insert overwrite table rlsf_prod.isbn_rank_assign_delta
select 
JRNL_BATCH_NAME,
row_number() OVER (
    ORDER BY JRNL_BATCH_NAME) as ranknumber  
from (select distinct   
JRNL_BATCH_NAME
from rlsf_prod.gl_master_parquet ) ll;