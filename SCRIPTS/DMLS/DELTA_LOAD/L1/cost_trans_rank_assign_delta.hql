with union_records as (
select   
transactionseoinventoryitemid as transactionseoinventoryitemid
from rlsf_prod.cost_trans_debit_delta
union
select inventory_item_id as transactionseoinventoryitemid from 
rlsf_prod.ohq_inv_as_of_date_with_layer_cost
)


insert overwrite table rlsf_prod.cost_trans_rank_assign_delta
select 
transactionseoinventoryitemid,
row_number() OVER (
    ORDER BY transactionseoinventoryitemid) as ranknumber  
from union_records ;
