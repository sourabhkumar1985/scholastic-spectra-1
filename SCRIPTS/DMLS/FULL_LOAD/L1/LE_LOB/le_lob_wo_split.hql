set decimal_v2=true;
insert overwrite table rlsf_prod.le_lob_wo_split

with org_code as (
select distinct organization_code, 
case when organization_code = 'SC1' then organization_code else 'US' end as channel_code
from rlsf_prod.le_lob_events
where event = 'Collation' and layer_quantity <> 0
)

select
eve.organization_code
,eve.row_num
--,org_code.channel_code
--,ch.country
,eve.item
,eve.short_id
,eve.transaction_type
,eve.reference
,eve.legacy_transaction_type
,eve.legacy_reason
,eve.transaction_source
,eve.source
,'' as gl_batch_desc
,'' as jrnl_batch_name
,'' as dr_jrnl_line_num
,'' as cr_jrnl_line_num
,'' as dr_account_num
,'' as cr_account_num
--,total_amount
--,(eve.layer_quantity * wo.wo_qty_percent) * eve.cost_unit as total_amount
,case when total_amount >= 0 then abs((eve.layer_quantity * wo.wo_qty_percent) * eve.cost_unit)
else (eve.layer_quantity * wo.wo_qty_percent) * eve.cost_unit end as total_amount
,eve.layer_quantity as base_layer_quantity
--,cast(round((eve.layer_quantity * wo.wo_qty_percent),1)  as decimal(38,6)) as layer_quantity
,cast(round(eve.layer_quantity * wo.wo_qty_percent) as decimal(38,6)) as layer_quantity
--,eve.layer_quantity * wo.wo_qty_percent as layer_quantity
--,cast(((cast(eve.layer_quantity as DECIMAL(38,18)) * cast(wo.wo_qty_percent as DECIMAL(38,18))) * cast(eve.cost_unit as DECIMAL(38,18))) as DECIMAL(38,6))as total_amount
--,cast((cast(eve.layer_quantity as DECIMAL(38,18)) * cast(wo.wo_qty_percent as DECIMAL(38,18))) as DECIMAL(38,6)) as layer_quantity
,eve.cost_unit
,eve.entered_dr
,eve.period
,eve.imt_creation_date
,eve.imt_creation_time
,eve.transaction_date
,eve.transaction_time
,'' as journal_creation_date
,'' as journal_creation_time
,'' as txn_transaction_id
,eve.rec_trxn_id
,eve.source_line
,eve.title
,eve.item_type
,eve.opus_item
,eve.isbn13
,case when eve.cr_account <> '1421000' then ch.legal_entity 
 else '' end as dr_legal_entity
,case when eve.cr_account <> '1421000' and ch.line_of_business ='0' then '000'
when eve.cr_account <> '1421000' and ch.line_of_business <> '0' then ch.line_of_business
 else '' end as dr_line_of_business
,if(eve.cr_account <> '1421000',eve.dr_account,'') as dr_account
,if(eve.cr_account <> '1421000',eve.dr_cost_center,'') as dr_cost_center
,if(eve.cr_account <> '1421000',eve.dr_product_line,'') as dr_product_line
,if(eve.cr_account <> '1421000',eve.dr_channel,'') as dr_channel
,if(eve.cr_account <> '1421000',eve.dr_loc,'') as dr_loc
,if(eve.cr_account <> '1421000',eve.dr_interco,'') as dr_interco
,case when eve.dr_account <> '1421000' and eve.cr_account = '1421000' then ch.legal_entity 
 else '' end as cr_legal_entity
,case when eve.dr_account <> '1421000' and eve.cr_account = '1421000' and ch.line_of_business ='0' then '000'
when eve.dr_account <> '1421000' and eve.cr_account = '1421000' and ch.line_of_business <> '0' then ch.line_of_business
 else '' end as cr_line_of_business
,if(eve.dr_account <> '1421000' and eve.cr_account = '1421000',eve.cr_account,'') as cr_account
,if(eve.dr_account <> '1421000' and eve.cr_account = '1421000',eve.cr_cost_center,'') as cr_cost_center
,if(eve.dr_account <> '1421000' and eve.cr_account = '1421000',eve.cr_product_line,'') as cr_product_line
,if(eve.dr_account <> '1421000' and eve.cr_account = '1421000',eve.cr_channel,'') as cr_channel
,if(eve.dr_account <> '1421000' and eve.cr_account = '1421000',eve.cr_loc,'') as cr_loc
,if(eve.dr_account <> '1421000' and eve.cr_account = '1421000',eve.cr_interco,'') as cr_interco
,eve.txn_external_system_ref_id
,eve.po_wo
,eve.destination_org
,eve.item_category
,eve.category_description
,eve.journal
,eve.accounting_class_code
,eve.po_wo_transaction_type
,eve.po_wo_transaction_source
,'LE/LOB' as lelob_source
,'WO_Split' as lelob_source_description
,eve.event
--,wo.major_channel
--,ch.major_channel
from rlsf_prod.le_lob_events eve
left join rlsf_prod.wo_split_percent wo on wo.work_order_number=eve.po_wo
left join 
(select case when country='Canada' then 'SC1'
else 'US' end as country,major_channel,
case when major_channel in ('CA','CABC','CATR','CMAG','CORR','ECOR','ENT','IRD','LEC',
'RECSHIP','SIG','SOFT','TFTR','TSP','AU','bc','CABD','CABF','CAED','CAFR','CARC','CAT',
'CAtr','CATR','CBC','CMG','CTR','ecat','ETFR','ETRS','EX','exp','EXP','EXPA',
'EXPORT','HMH','IN','jacket','Jackets','KLU','major_channel','MRK','sample','samples',
'SH','SSO','tr','TRADE','TRD','UK','uk','ww','MS','OTHER') then 'NA' 
else legal_entity end as legal_entity,
case when major_channel in ('CA','CABC','CATR','CMAG','CORR','ECOR','ENT','IRD','LEC',
'RECSHIP','SIG','SOFT','TFTR','TSP','AU','bc','CABD','CABF','CAED','CAFR','CARC','CAT',
'CAtr','CATR','CBC','CMG','CTR','ecat','ETFR','ETRS','EX','exp','EXP','EXPA',
'EXPORT','HMH','IN','jacket','Jackets','KLU','major_channel','MRK','sample','samples',
'SH','SSO','tr','TRADE','TRD','UK','uk','ww','MS','OTHER') then 'NA' 
else line_of_business end as line_of_business
from rlsf_prod.le_lob_channels) ch 
on ch.major_channel=wo.major_channel 
left join org_code  on org_code.organization_code=eve.organization_code
and org_code.channel_code=ch.country
where eve.event = 'Collation' and layer_quantity <> 0 and org_code.channel_code is not null
;