insert overwrite table rlsf_prod.le_lob_contra

with po_wo as (
select distinct powo from (
select po_number as powo from rlsf_prod.po_split_percent
where le_lob_forecast <>0
union all
select work_order_number as powo from rlsf_prod.wo_split_percent
where lob_forecast <>0) po_wo
)

select
organization_code ,
item ,
short_id ,
transaction_type ,
reference ,
legacy_transaction_type ,
legacy_reason ,
transaction_source ,
source ,
gl_batch_desc ,
jrnl_batch_name ,
dr_jrnl_line_num ,
cr_jrnl_line_num ,
dr_account_num ,
cr_account_num ,
total_amount * (-1) total_amount,
layer_quantity * (-1) as layer_quantity,
cost_unit ,
entered_dr ,
period ,
imt_creation_date ,
imt_creation_time ,
transaction_date ,
transaction_time ,
journal_creation_date ,
journal_creation_time ,
txn_transaction_id ,
rec_trxn_id ,
source_line ,
title ,
item_type ,
opus_item ,
isbn13 ,
dr_legal_entity ,
dr_line_of_business ,
dr_account ,
dr_cost_center ,
dr_product_line ,
dr_channel ,
dr_loc ,
dr_interco ,
cr_legal_entity ,
cr_line_of_business ,
cr_account ,
cr_cost_center ,
cr_product_line ,
cr_channel ,
cr_loc ,
cr_interco ,
txn_external_system_ref_id ,
po_wo ,
destination_org ,
item_category ,
category_description ,
journal ,
accounting_class_code ,
po_wo_transaction_type ,
po_wo_transaction_source ,
'LE/LOB' as lelob_source ,
'Contra' as lelob_source_description ,
event 
from rlsf_prod.le_lob_events eve
join po_wo po on po.powo=eve.po_wo 
where event in ('Receipt','Collation') and layer_quantity <> 0;