insert overwrite table rlsf_prod.le_lob
select
organization_code ,
item ,
short_id ,
transaction_type ,
reference ,
legacy_transaction_type ,
legacy_reason ,
transaction_source ,
source ,
gl_batch_desc ,
jrnl_batch_name ,
dr_jrnl_line_num ,
cr_jrnl_line_num ,
dr_account_num ,
cr_account_num ,
total_amount ,
layer_quantity ,
cost_unit ,
entered_dr ,
period ,
imt_creation_date ,
imt_creation_time ,
transaction_date ,
transaction_time ,
journal_creation_date ,
journal_creation_time ,
txn_transaction_id ,
rec_trxn_id ,
source_line ,
title ,
item_type ,
opus_item ,
isbn13 ,
dr_legal_entity ,
dr_line_of_business ,
dr_account ,
dr_cost_center ,
dr_product_line ,
dr_channel ,
dr_loc ,
dr_interco ,
cr_legal_entity ,
cr_line_of_business ,
cr_account ,
cr_cost_center ,
cr_product_line ,
cr_channel ,
cr_loc ,
cr_interco ,
txn_external_system_ref_id ,
po_wo ,
destination_org ,
item_category ,
category_description ,
journal ,
accounting_class_code ,
po_wo_transaction_type ,
po_wo_transaction_source ,
lelob_source ,
lelob_source_description ,
event 
from rlsf_prod.le_lob_events;

insert into table rlsf_prod.le_lob
select * from rlsf_prod.le_lob_contra;

insert into table rlsf_prod.le_lob

with po_tuned_qty as (
select base_layer_quantity,sum(layer_quantity),
max(layer_quantity) as max_qty,
max(layer_quantity) + (base_layer_quantity-sum(layer_quantity)) as tuned_qty
,row_num,item from rlsf_prod.le_lob_po_split
group by row_num,item,base_layer_quantity
)

select 
po.organization_code ,
--po.row_num ,
po.item ,
short_id ,
transaction_type ,
reference ,
legacy_transaction_type ,
legacy_reason ,
transaction_source ,
source ,
gl_batch_desc ,
jrnl_batch_name ,
dr_jrnl_line_num ,
cr_jrnl_line_num ,
dr_account_num ,
cr_account_num 
,case when total_amount >= 0 and po.layer_quantity=tn.max_qty then abs(tn.tuned_qty * po.cost_unit)
when total_amount < 0 and po.layer_quantity=tn.max_qty then (tn.tuned_qty * po.cost_unit)
else po.layer_quantity * po.cost_unit
end as total_amount 
,case when po.layer_quantity=tn.max_qty then tn.tuned_qty
else po.layer_quantity end as layer_quantity,
cost_unit ,
entered_dr ,
po.period ,
imt_creation_date ,
imt_creation_time ,
transaction_date ,
transaction_time ,
journal_creation_date ,
journal_creation_time ,
txn_transaction_id ,
po.rec_trxn_id ,
source_line ,
title ,
item_type ,
opus_item ,
isbn13 ,
dr_legal_entity ,
dr_line_of_business ,
dr_account ,
dr_cost_center ,
dr_product_line ,
dr_channel ,
dr_loc ,
dr_interco ,
cr_legal_entity ,
cr_line_of_business ,
cr_account ,
cr_cost_center ,
cr_product_line ,
cr_channel ,
cr_loc ,
cr_interco ,
txn_external_system_ref_id ,
po.po_wo ,
destination_org ,
item_category ,
category_description ,
journal ,
accounting_class_code ,
po_wo_transaction_type ,
po_wo_transaction_source ,
lelob_source ,
lelob_source_description ,
event 
from rlsf_prod.le_lob_po_split po
left join po_tuned_qty tn on tn.item=po.item and tn.row_num=po.row_num
;

insert into table rlsf_prod.le_lob

with wo_tuned_qty as (
select base_layer_quantity,sum(layer_quantity),
max(layer_quantity) as max_qty,
max(layer_quantity) + (base_layer_quantity-sum(layer_quantity)) as tuned_qty
,row_num,item from rlsf_prod.le_lob_wo_split
group by row_num,item,base_layer_quantity
)

select 
wo.organization_code ,
--wo.row_num ,
wo.item ,
short_id ,
transaction_type ,
reference ,
legacy_transaction_type ,
legacy_reason ,
transaction_source ,
source ,
gl_batch_desc ,
jrnl_batch_name ,
dr_jrnl_line_num ,
cr_jrnl_line_num ,
dr_account_num ,
cr_account_num 
,case when total_amount >= 0 and wo.layer_quantity=tn.max_qty then abs(tn.tuned_qty * wo.cost_unit)
when total_amount < 0 and wo.layer_quantity=tn.max_qty then (tn.tuned_qty * wo.cost_unit)
else wo.layer_quantity * wo.cost_unit
end as total_amount 
,case when wo.layer_quantity=tn.max_qty then tn.tuned_qty
else wo.layer_quantity end as layer_quantity,
cost_unit ,
entered_dr ,
wo.period ,
imt_creation_date ,
imt_creation_time ,
transaction_date ,
transaction_time ,
journal_creation_date ,
journal_creation_time ,
txn_transaction_id ,
wo.rec_trxn_id ,
source_line ,
title ,
item_type ,
opus_item ,
isbn13 ,
dr_legal_entity ,
dr_line_of_business ,
dr_account ,
dr_cost_center ,
dr_product_line ,
dr_channel ,
dr_loc ,
dr_interco ,
cr_legal_entity ,
cr_line_of_business ,
cr_account ,
cr_cost_center ,
cr_product_line ,
cr_channel ,
cr_loc ,
cr_interco ,
txn_external_system_ref_id ,
wo.po_wo ,
destination_org ,
item_category ,
category_description ,
journal ,
accounting_class_code ,
po_wo_transaction_type ,
po_wo_transaction_source ,
lelob_source ,
lelob_source_description ,
event 
from rlsf_prod.le_lob_wo_split wo
left join wo_tuned_qty tn on tn.item=wo.item and tn.row_num=wo.row_num
;