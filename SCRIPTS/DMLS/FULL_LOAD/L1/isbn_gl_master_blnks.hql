set DECIMAL_V2=false;

insert into table rlsf_prod.isbn_gl_master_blnks

with gl_mstr as (
select distinct gl_mstr.* from rlsf_prod.gl_master_parquet gl_mstr -- change the table name 
join rlsf_prod.isbn_rank_assign assign on assign.PERIOD = gl_mstr.PERIOD and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
)

select  
ORGANIZATION_CODE, gl_mstr.ITEM, o_item.short_id as SHORT_ID,
TRANSACTION_TYPE, REFERENCE, LEGACY_TRANSACTION_TYPE,
LEGACY_REASON, TRANSACTION_SOURCE,  SOURCE, GL_BATCH_DESC, gl_mstr.JRNL_BATCH_NAME, 
JRNL_LINE_NUM as DR_JRNL_LINE_NUM,
 '' as CR_JRNL_LINE_NUM,
concat(segment1,'.',segment2,'.',segment3,'.',segment4,'.',segment5,'.',segment6,'.',segment7,'.',segment8) as dr_account_num,'' as cr_account_num,
cast(entered_currency_amount as decimal(38,6)) as total_amount,
case when TRANSACTION_TYPE in ('ACQUISITION_COST_ADJUSTMENT','LAYER_COST_ADJUSTMENT','MANUAL_RECEIPT_COST_ADJUSTMENT') then 0
else
cast(LAYER_QUANTITY as decimal(38,6)) 
end as LAYER_QUANTITY,
case when TRANSACTION_TYPE in ('ACQUISITION_COST_ADJUSTMENT','LAYER_COST_ADJUSTMENT','MANUAL_RECEIPT_COST_ADJUSTMENT') then 0
else
cast (cast(entered_currency_amount as decimal(38,6)) / abs(cast(LAYER_QUANTITY as decimal(38,6))) as decimal (38,6)) 
end as cost_unit,
cast(gl_mstr.entered_dr as decimal(38,6)) as entered_dr,
gl_mstr.PERIOD, 
substr(IMT_CREATION_DATE, 1, 10) as IMT_CREATION_DATE,
substr(IMT_CREATION_DATE, 12, 19) as IMT_CREATION_TIME, 
substr(TRANSACTION_DATE, 1, 10) as TRANSACTION_DATE,
substr(TRANSACTION_DATE, 12, 19) as TRANSACTION_TIME,
substr(JOURNAL_CREATION_DATE, 1, 10) as JOURNAL_CREATION_DATE,
substr(substr(journal_creation_date, 12, 19), 1, 8) as JOURNAL_CREATION_Time,
TXN_TRANSACTION_ID, REC_TRXN_ID, SOURCE_LINE, 
o_item.title as title,
--case
--when inv_master.item_type like "%-%-%:%:%" then cast(substr(inv_master.item_type, 1, length(inv_master.item_type) - 20) as string) --else inv_master.item_type end as item_type,
o_item.item_type as item_type,
o_item.opus_num as opus_item,
o_item.isbn13 as isbn13,
gl_mstr.segment1 as dr_Legal_Entity, gl_mstr.segment2 as dr_Line_of_Business, gl_mstr.segment3 as dr_Account, gl_mstr.segment4 as dr_Cost_Center, gl_mstr.segment5 as dr_Product_Line,		  
gl_mstr.segment6 as dr_Channel, gl_mstr.segment7 as dr_Loc, gl_mstr.segment8 as dr_Interco,
'' as cr_Legal_Entity, '' as cr_Line_of_Business, '' as cr_Account, '' as cr_Cost_Center, '' as cr_Product_Line,		  
'' as cr_Channel, '' as cr_Loc, '' as cr_Interco,
txn_external_system_ref_id,
SOURCE_REFERENCE as PO_WO,
'' as Destination_org,
o_item.category_code as Item_category,
o_item.cat_description as Category_description,
gl_mstr.journal_name as Journal,
dist_cost_element_id,
ACCOUNTING_CLASS_CODE as dr_ACCOUNTING_CLASS_CODE,
'' as cr_ACCOUNTING_CLASS_CODE,
inv_transaction_type as po_wo_transaction_type, 
inv_transaction_source as po_wo_transaction_source
from gl_mstr -- change the table name 
left join rlsf_prod.inv_master_distinct_parquet inv_master on inv_master.oracle_item_id = gl_mstr.ITEM
left join rlsf_prod.oracle_items_parquet o_item on o_item.oracle_id = gl_mstr.ITEM
where distribution_dr_cr_sign = '' and entered_dr <> '' and entered_currency_amount <> '0'

union all

select  
ORGANIZATION_CODE, gl_mstr.ITEM, o_item.short_id as SHORT_ID,
TRANSACTION_TYPE, REFERENCE, LEGACY_TRANSACTION_TYPE,
LEGACY_REASON, TRANSACTION_SOURCE,  SOURCE,  GL_BATCH_DESC, gl_mstr.JRNL_BATCH_NAME, 
'' as DR_JRNL_LINE_NUM,
JRNL_LINE_NUM as CR_JRNL_LINE_NUM,
'' as dr_account_num,
concat(segment1,'.',segment2,'.',segment3,'.',segment4,'.',segment5,'.',segment6,'.',segment7,'.',segment8) as cr_account_num,
cast(entered_currency_amount as decimal(38,6)) as total_amount,
0 as LAYER_QUANTITY,
0 as cost_unit,
0 as entered_dr,
gl_mstr.PERIOD, 
substr(IMT_CREATION_DATE, 1, 10) as IMT_CREATION_DATE,
substr(IMT_CREATION_DATE, 12, 19) as IMT_CREATION_TIME, 
substr(TRANSACTION_DATE, 1, 10) as TRANSACTION_DATE,
substr(TRANSACTION_DATE, 12, 19) as TRANSACTION_TIME,
substr(JOURNAL_CREATION_DATE, 1, 10) as JOURNAL_CREATION_DATE,
substr(substr(journal_creation_date, 12, 19), 1, 8) as JOURNAL_CREATION_Time,
TXN_TRANSACTION_ID, REC_TRXN_ID, SOURCE_LINE, 
o_item.title as title,
--case
--when inv_master.item_type like "%-%-%:%:%" then cast(substr(inv_master.item_type, 1, length(inv_master.item_type) - 20) as string) --else inv_master.item_type end as item_type,
o_item.item_type as item_type,
o_item.opus_num as opus_item,
o_item.isbn13 as isbn13,
'' as dr_Legal_Entity, '' as dr_Line_of_Business, '' as dr_Account, '' as dr_Cost_Center, '' as dr_Product_Line,		  
'' as dr_Channel, '' as dr_Loc, '' as dr_Interco,
gl_mstr.segment1 as cr_Legal_Entity, gl_mstr.segment2 as cr_Line_of_Business, gl_mstr.segment3 as cr_Account, gl_mstr.segment4 as cr_Cost_Center, gl_mstr.segment5 as cr_Product_Line, gl_mstr.segment6 as cr_Channel, gl_mstr.segment7 as cr_Loc, gl_mstr.segment8 as cr_Interco,
txn_external_system_ref_id,
SOURCE_REFERENCE as PO_WO,
'' as Destination_org,
o_item.category_code as Item_category,
o_item.cat_description as Category_description,
gl_mstr.journal_name as Journal,
dist_cost_element_id ,
'' as dr_ACCOUNTING_CLASS_CODE,
ACCOUNTING_CLASS_CODE as cr_ACCOUNTING_CLASS_CODE,
inv_transaction_type as po_wo_transaction_type, 
inv_transaction_source as po_wo_transaction_source
from gl_mstr -- change the table name 
left join rlsf_prod.inv_master_distinct_parquet inv_master on inv_master.oracle_item_id = gl_mstr.ITEM
left join rlsf_prod.oracle_items_parquet o_item on o_item.oracle_id = gl_mstr.ITEM
where distribution_dr_cr_sign = '' and entered_cr <> '' and entered_currency_amount <> '0';