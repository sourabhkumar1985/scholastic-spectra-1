with txn_ct as (
select count(*), transaction_id,oracle_id,adjustment_reason,concat(trim(transaction_id),trim(oracle_id),trim(adjustment_reason)) txn_group from (
select distinct txn.transaction_id,txn.item_num,aa.oracle_id,txn.adjustment_reason,txn.transaction_date
from rlsf_prod.transaction_id_cloud txn
left join (select distinct oracle_id, isbn13, title from rlsf_prod.oracle_items_parquet)aa on trim(aa.isbn13)=trim(txn.isbn13)
)aa group by transaction_id,oracle_id,adjustment_reason having count(*)>1
)

INSERT OVERWRITE TABLE rlsf_prod.txn_cloud_gap

select txn.transaction_id,txn.item_num,aa.oracle_id, txn.isbn13, aa.title,txn.adjustment_reason,txn.transaction_date,sum(cast(update_quantity as decimal(38,6))) as qty_cloud,
concat(trim(txn.transaction_id),trim(aa.oracle_id),trim(txn.adjustment_reason)) as txn_grp 
from rlsf_prod.transaction_id_cloud txn
left join (select distinct oracle_id, isbn13, title from rlsf_prod.oracle_items_parquet)aa on trim(aa.isbn13)=trim(txn.isbn13)
left anti join txn_ct on txn_ct.txn_group=concat(trim(txn.transaction_id),trim(aa.oracle_id),trim(txn.adjustment_reason))
group by txn.transaction_id,txn.item_num,aa.oracle_id, txn.isbn13, aa.title,txn.adjustment_reason,txn.transaction_date,txn_grp
;

with txn_ct as (
select count(*), transaction_id,oracle_id,adjustment_reason,concat(trim(transaction_id),trim(oracle_id),trim(adjustment_reason)) txn_group from (
select distinct txn.transaction_id,txn.item_num,aa.oracle_id,txn.adjustment_reason,txn.transaction_date
from rlsf_prod.transaction_id_cloud txn
left join (select distinct oracle_id, isbn13, title from rlsf_prod.oracle_items_parquet)aa on trim(aa.isbn13)=trim(txn.isbn13)
)aa group by transaction_id,oracle_id,adjustment_reason having count(*)>1
)

INSERT into TABLE rlsf_prod.txn_cloud_gap

select txn.transaction_id,txn.item_num,aa.oracle_id, txn.isbn13, aa.title,txn.adjustment_reason,'' as transaction_date,sum(cast(update_quantity as decimal(38,6))) as qty_cloud,
concat(trim(txn.transaction_id),trim(aa.oracle_id),trim(txn.adjustment_reason)) as txn_grp 
from rlsf_prod.transaction_id_cloud txn
left join (select distinct oracle_id, isbn13, title from rlsf_prod.oracle_items_parquet)aa on trim(aa.isbn13)=trim(txn.isbn13)
join txn_ct on txn_ct.txn_group=concat(trim(txn.transaction_id),trim(aa.oracle_id),trim(txn.adjustment_reason))
group by txn.transaction_id,txn.item_num,aa.oracle_id, txn.isbn13, aa.title,txn.adjustment_reason,txn.transaction_date,txn_grp
;
