with isbn_ct as (
select count(*),organization_code,reference,item,concat(trim(organization_code),trim(reference),trim(item)) as isbn_group from (
select distinct organization_code,reference,short_id,item,isbn13,title,transaction_date
from rlsf_prod.isbn_activity_summary 
)aa group by organization_code,reference,item having count(*)>1
)

INSERT overwrite TABLE rlsf_prod.isbn_gap_po_ref

select organization_code,reference,short_id,item,isbn13,title,transaction_date, sum(qty) as qty_hadoop,
concat(trim(organization_code),trim(reference),trim(item)) as isbn_grp  
from rlsf_prod.isbn_activity_summary isbn 
left anti join isbn_ct on isbn_ct.isbn_group=concat(trim(isbn.organization_code),trim(isbn.reference),trim(isbn.item))
group by organization_code,reference,short_id,item,isbn13,title,transaction_date,isbn_grp
;

with isbn_ct as (
select count(*),organization_code,reference,item,concat(trim(organization_code),trim(reference),trim(item)) as isbn_group from (
select distinct organization_code,reference,short_id,item,isbn13,title,transaction_date
from rlsf_prod.isbn_activity_summary 
)aa group by organization_code,reference,item having count(*)>1
) 

INSERT into TABLE rlsf_prod.isbn_gap_po_ref

select isbn.organization_code,isbn.reference,short_id,isbn.item,isbn13,title,'' as transaction_date, sum(qty) as qty_hadoop,
concat(trim(isbn.organization_code),trim(isbn.reference),trim(isbn.item)) as isbn_grp  
from rlsf_prod.isbn_activity_summary isbn 
join isbn_ct on isbn_ct.isbn_group=concat(trim(isbn.organization_code),trim(isbn.reference),trim(isbn.item))
group by isbn.organization_code,isbn.reference,short_id,isbn.item,isbn13,title,isbn_grp
;