with po_ct as (
select count(*),cost_center,po_num,oracle_id,concat(trim(cost_center),trim(po_num),trim(oracle_id)) as po_group from (
select distinct po.cost_center,po.po_num,po.item_num,aa.oracle_id,po.isbn13,aa.title,po.transaction_date
from rlsf_prod.po_cloud po
left join (select distinct oracle_id,isbn13,title from rlsf_prod.oracle_items_parquet where isbn13<>'')aa on trim(po.isbn13)=trim(aa.isbn13)
)aa group by cost_center,po_num,oracle_id having count(*)>1
)

INSERT overwrite table rlsf_prod.po_cloud_gap

select po.cost_center,po.po_num,po.item_num,aa.oracle_id,po.isbn13,aa.title,po.transaction_date, sum(cast(quantity as decimal(38,6))) as qty_cloud,
concat(trim(po.cost_center),trim(po.po_num),trim(aa.oracle_id)) as po_grp 
from rlsf_prod.po_cloud po
left join (select distinct oracle_id,isbn13,title from rlsf_prod.oracle_items_parquet where isbn13<>'')aa on trim(po.isbn13)=trim(aa.isbn13)
left anti join po_ct on po_ct.po_group=concat(trim(po.cost_center),trim(po.po_num),trim(aa.oracle_id))
group by po.cost_center,po.po_num,po.item_num,aa.oracle_id,po.isbn13,aa.title,po.transaction_date,po_grp
;


with po_ct as (
select count(*),cost_center,po_num,oracle_id,concat(trim(cost_center),trim(po_num),trim(oracle_id)) as po_group from (
select distinct po.cost_center,po.po_num,po.item_num,aa.oracle_id,po.isbn13,aa.title,po.transaction_date
from rlsf_prod.po_cloud po
left join (select distinct oracle_id,isbn13,title from rlsf_prod.oracle_items_parquet where isbn13<>'')aa on trim(po.isbn13)=trim(aa.isbn13)
)aa group by cost_center,po_num,oracle_id having count(*)>1
)

INSERT into table rlsf_prod.po_cloud_gap

select po.cost_center,po.po_num,po.item_num,aa.oracle_id,po.isbn13,aa.title,'' as transaction_date, sum(cast(quantity as decimal(38,6))) as qty_cloud,
concat(trim(po.cost_center),trim(po.po_num),trim(aa.oracle_id)) as po_grp 
from rlsf_prod.po_cloud po
left join (select distinct oracle_id,isbn13,title from rlsf_prod.oracle_items_parquet where isbn13<>'')aa on trim(po.isbn13)=trim(aa.isbn13)
join po_ct on po_ct.po_group=concat(trim(po.cost_center),trim(po.po_num),trim(aa.oracle_id))
group by po.cost_center,po.po_num,po.item_num,aa.oracle_id,po.isbn13,aa.title,transaction_date,po_grp
;