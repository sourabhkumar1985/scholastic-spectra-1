-- we need consider all the transactions both from isbn and gmrd and hence we would be doing 3 inserts

-- first insert gives all the transactions which are common between both 
insert overwrite table rlsf_prod.gmrd_isbn_rev_ref_details
select 
gmrd.system_of_origin as source ,
gmrd.order_number as ref_number ,
case when gmrd.system_of_origin='OTC' then split_part(gmrd.order_number,'-',1)
else gmrd.order_number end as proforma_ref ,
gmrd.isbn_13 as isbn13,
isbn.title as title ,
sum(cast(gmrd.revenue as decimal(38,6)))  as rev_amt ,
sum(cast(gmrd.units as decimal(38,6))) as rev_qty ,
sum(isbn.cop_amt) as cop_amt ,
sum(isbn.cop_qty) as cop_qty ,
gmrd.fiscal_period as period ,
--'' as transaction_date ,
substr(gmrd.transaction_date_time,1,10) as transaction_date , -- based on jay reply we need to change it
'Match' as status ,
isbn.legacy_reason as legacy_reason ,
isbn.legacy_trans_type ,
isbn_Legal_Entity ,
isbn_Line_of_Business ,
isbn_Account ,
isbn_Cost_Center ,
isbn_Product_Line ,
isbn_Channel ,
isbn_Loc ,
isbn_Interco ,
split_part(bu.oracle_bu,'.',1) as gmrd_Legal_Entity ,     
split_part(bu.oracle_bu,'.',2) as gmrd_Line_of_Business , 
case 
when system_of_origin='BCOE' and (
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) <> ''
and  
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) is not null
)
then split_part(acc_2.oracle_accnt,'.',1) else 
split_part(acc.oracle_accnt,'.',1) end as gmrd_Account ,      
split_part(bu.oracle_bu,'.',3) as gmrd_Cost_Center ,      
split_part(bu.oracle_bu,'.',4) as gmrd_Product_Line ,     
split_part(bu.oracle_bu,'.',5) as gmrd_Channel ,          
split_part(bu.oracle_bu,'.',6) as gmrd_Loc ,
case 
when system_of_origin='BCOE' and (
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) <> ''
and  
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) is not null
)
then split_part(acc_2.oracle_accnt,'.',2) else 
split_part(acc.oracle_accnt,'.',2) end as gmrd_Interco            
from rlsf_prod.gmrd_revenue_extract gmrd
left join rlsf_prod.gmrd_oracle_bu bu on trim(split_part(gmrd.account_number,'.',1))=trim(bu.jde_bu)
left join rlsf_prod.gmrd_oracle_account acc on trim(split_part(gmrd.account_number,'.',2))=trim(acc.jde_accnt)
left join rlsf_prod.gmrd_oracle_account acc_2 on 
concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3)))=trim(acc_2.jde_accnt)
join rlsf_prod.isbn_activity_agg isbn on isbn.ref_number = gmrd.order_number and isbn.isbn13 = gmrd.isbn_13 
and isbn.period = gmrd.fiscal_period
--and lower(isbn.period) = lower(concat(split_part(fiscal_period, '-',1),'-', substr(split_part(fiscal_period, '-',2),3,4)))-- remove once confirmed and change isbn table name
--and isbn.transaction_date=substr(gmrd.transaction_date_time,1,10) -- based on jay reply, we need to add or completely remove it
--where gmrd.fiscal_period='JUN-2020' and isbn.period='Jun-20' -- this has to be changed and make sure both column values are mathcing
group by 
source ,
ref_number ,
proforma_ref ,
isbn13,
title ,
period ,
transaction_date ,
status ,
legacy_reason ,
isbn.legacy_trans_type ,
isbn_Legal_Entity ,
isbn_Line_of_Business ,
isbn_Account ,
isbn_Cost_Center ,
isbn_Product_Line ,
isbn_Channel ,
isbn_Loc ,
isbn_Interco ,
gmrd_Legal_Entity ,     
gmrd_Line_of_Business , 
gmrd_Account ,      
gmrd_Cost_Center ,      
gmrd_Product_Line ,     
gmrd_Channel ,          
gmrd_Loc ,
gmrd_Interco;




-- second insert will give all the records whihc are present in gmrd but missing in isbn
insert into table rlsf_prod.gmrd_isbn_rev_ref_details
with only_gmrd as (
select gmrd.* from 
rlsf_prod.gmrd_revenue_extract gmrd
left anti join rlsf_prod.gmrd_isbn_rev_ref_details det on det.ref_number=gmrd.order_number and det.isbn13 = gmrd.isbn_13 and det.period=gmrd.fiscal_period
--where gmrd.fiscal_period='JUN-2020'
) 

select 
gmrd.system_of_origin as source ,
gmrd.order_number as ref_number ,
case when gmrd.system_of_origin='OTC' then split_part(gmrd.order_number,'-',1)
else gmrd.order_number end as proforma_ref ,
gmrd.isbn_13 as isbn13,
o_itm.title as title ,
sum(cast(gmrd.revenue as decimal(38,6)))  as rev_amt ,
sum(cast(gmrd.units as decimal(38,6))) as rev_qty ,
0 as cop_amt ,
0 as cop_qty ,
gmrd.fiscal_period as period ,
--'' as transaction_date ,
substr(gmrd.transaction_date_time,1,10) as transaction_date ,
'Cop Missing' as status ,
'' as legacy_reason ,
'' as legacy_trans_type ,
'000' as isbn_Legal_Entity ,
'000' as isbn_Line_of_Business ,
'000' as isbn_Account ,
'000' as isbn_Cost_Center ,
'000' as isbn_Product_Line ,
'000' as isbn_Channel ,
'000' as isbn_Loc ,
'000' as isbn_Interco ,
split_part(bu.oracle_bu,'.',1) as gmrd_Legal_Entity ,     
split_part(bu.oracle_bu,'.',2) as gmrd_Line_of_Business , 
case 
when system_of_origin='BCOE' and (
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) <> ''
and  
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) is not null
)
then split_part(acc_2.oracle_accnt,'.',1) else 
split_part(acc.oracle_accnt,'.',1) end as gmrd_Account ,       
split_part(bu.oracle_bu,'.',3) as gmrd_Cost_Center ,      
split_part(bu.oracle_bu,'.',4) as gmrd_Product_Line ,     
split_part(bu.oracle_bu,'.',5) as gmrd_Channel ,          
split_part(bu.oracle_bu,'.',6) as gmrd_Loc ,              
case 
when system_of_origin='BCOE' and (
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) <> ''
and 
split_part(concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3))),'.',2) is not null
)
then split_part(acc_2.oracle_accnt,'.',2) else 
split_part(acc.oracle_accnt,'.',2) end as gmrd_Interco 
from only_gmrd gmrd
left join rlsf_prod.gmrd_oracle_bu bu on trim(split_part(gmrd.account_number,'.',1))=trim(bu.jde_bu)
left join rlsf_prod.gmrd_oracle_account acc on trim(split_part(gmrd.account_number,'.',2))=trim(acc.jde_accnt)
left join rlsf_prod.gmrd_oracle_account acc_2 on 
concat(trim(split_part(gmrd.account_number,'.',2)),'.',trim(split_part(gmrd.account_number,'.',3)))=trim(acc_2.jde_accnt)
left join rlsf_prod.oracle_items_parquet o_itm on o_itm.isbn13 = gmrd.isbn_13
group by 
source,
ref_number,
proforma_ref,
isbn13,
title,
period,
transaction_date,
status,
gmrd_Legal_Entity,
gmrd_Line_of_Business,
gmrd_Account,
gmrd_Cost_Center,
gmrd_Product_Line,
gmrd_Channel,
gmrd_Loc,
gmrd_Interco;


-- third insert will give all the records whihc are present in isbn but missing in gmrd

insert into table rlsf_prod.gmrd_isbn_rev_ref_details
with only_isbn as (
select isbn.* from 
rlsf_prod.isbn_activity_agg isbn
left anti join rlsf_prod.gmrd_isbn_rev_ref_details det on det.ref_number=isbn.ref_number and det.isbn13 = isbn.isbn13 and isbn.period = det.period
--lower(isbn.period) = lower(concat(split_part(det.period, '-',1),'-', substr(split_part(det.period, '-',2),3,4)))
--where isbn.period='Jun-20' 
) 

select 
'AISBN' as source ,
isbn.ref_number as ref_number ,
split_part(isbn.ref_number,'-',1) as proforma_ref ,
isbn.isbn13 as isbn13,
isbn.title as title ,
0 as rev_amt ,
0 as rev_qty ,
isbn.cop_amt as cop_amt,
isbn.cop_qty as cop_qty ,
isbn.period as period , --isbn.period as period , please make sure yu remove this and make it consistent with gmrd values
'' as transaction_date ,
'Rev Missing' as status ,
isbn.legacy_reason as legacy_reason ,
isbn.legacy_trans_type as legacy_trans_type ,
isbn_Legal_Entity ,
isbn_Line_of_Business ,
isbn_Account ,
isbn_Cost_Center ,
isbn_Product_Line ,
isbn_Channel ,
isbn_Loc ,
isbn_Interco ,
'000' as gmrd_Legal_Entity ,    
'000' as gmrd_Line_of_Business ,
'000' as gmrd_Account ,         
'000' as gmrd_Cost_Center ,     
'000' as gmrd_Product_Line ,    
'000' as gmrd_Channel ,         
'000' as gmrd_Loc ,             
'000' as gmrd_Interco           
from only_isbn isbn
;