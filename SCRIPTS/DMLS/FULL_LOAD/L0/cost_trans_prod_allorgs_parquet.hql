INVALIDATE METADATA rlsf_prod.cost_trans_prod_allorgs;

INSERT OVERWRITE TABLE rlsf_prod.cost_trans_prod_allorgs_parquet
select * from rlsf_prod.cost_trans_prod_allorgs;

truncate rlsf_prod.cost_trans_prod_allorgs;