INVALIDATE METADATA rlsf_prod.s3_oracle_items;

INSERT OVERWRITE TABLE rlsf_prod.oracle_items_parquet
select 
oracle_id String
,title String
,isbn13 String
,inventory_item_flag String
,item_type String
,inventory_asset_flag String
,costing_enabled_flag String
,short_id String
,mdm_id String
,creation_dt String
,inventory_item_id String
,opus_num String
,bf_num String

 from rlsf_prod.s3_oracle_items;