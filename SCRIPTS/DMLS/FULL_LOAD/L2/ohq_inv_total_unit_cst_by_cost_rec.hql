WITH maxdate_filter
AS (
	SELECT transactionseoinventoryitemid
		,cstlayercostspeorectrxnid
		,transactionseoinventoryorgid
		,max(transactionseocreationdate) AS maxdate
		,cstlayercostspeocosttransactiontype
	FROM rlsf_prod.ohq_inv_unit_cst_by_cost_rec
	GROUP BY transactionseoinventoryitemid
		,cstlayercostspeorectrxnid
		,transactionseoinventoryorgid
		,cstlayercostspeocosttransactiontype
	)
INSERT INTO TABLE rlsf_prod.ohq_inv_total_unit_cst_by_cost_rec
SELECT cost.transactionseoinventoryitemid AS trans_eo_inventory_item_id
	,cost.cstlayercostspeorectrxnid AS lc_p_eo_rec_trans_id
	,max(cost.transactionseocreationdate) AS date_
	,cost.transactionseoinventoryorgid AS oracle_id
	,sum(cost.cost_unit) AS cost_unit
FROM rlsf_prod.ohq_inv_unit_cst_by_cost_rec cost
JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = cost.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
JOIN maxdate_filter fil ON fil.transactionseoinventoryitemid = cost.transactionseoinventoryitemid
	AND fil.cstlayercostspeorectrxnid = cost.cstlayercostspeorectrxnid
	AND fil.transactionseoinventoryorgid = cost.transactionseoinventoryorgid
	AND cost.transactionseocreationdate = fil.maxdate
	AND cost.cstlayercostspeocosttransactiontype = fil.cstlayercostspeocosttransactiontype
GROUP BY trans_eo_inventory_item_id
	,lc_p_eo_rec_trans_id
	,oracle_id;