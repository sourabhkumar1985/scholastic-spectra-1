insert overwrite  table rlsf_prod.gmrd_isbn_rev_isbn13_details
select
isbn13 ,
title ,
sum(rev_amt) as rev_amt ,
sum(rev_qty) as rev_qty ,
sum(cop_amt) as cop_amt ,
sum(cop_qty) as cop_qty ,
period ,
legacy_reason ,
legacy_trans_type ,
if(isbn_Legal_Entity is null ,'000',isbn_Legal_Entity)  as isbn_Legal_Entity,
if(isbn_Line_of_Business is null ,'000',isbn_Line_of_Business) as isbn_Line_of_Business ,
if(isbn_Account is null ,'000',isbn_Account) as isbn_Account ,
if(isbn_Cost_Center is null ,'000', isbn_Cost_Center) as isbn_Cost_Center,
if(isbn_Product_Line is null ,'000', isbn_Product_Line) as isbn_Product_Line ,
if(isbn_Channel is null ,'000', isbn_Channel) as isbn_Channel ,
if(isbn_Loc is null ,'000',isbn_Loc) as isbn_Loc ,
if(isbn_Interco is null ,'000',isbn_Interco) as isbn_Interco ,
if(gmrd_Legal_Entity is null ,'000',gmrd_Legal_Entity) as gmrd_Legal_Entity ,
if(gmrd_Line_of_Business is null ,'000',gmrd_Line_of_Business) as gmrd_Line_of_Business ,
if(gmrd_Account is null ,'000', gmrd_Account)  as gmrd_Account,
if(gmrd_Cost_Center is null ,'000', gmrd_Cost_Center) as gmrd_Cost_Center,
if(gmrd_Product_Line is null ,'000', gmrd_Product_Line) as gmrd_Product_Line ,
if(gmrd_Channel is null ,'000',gmrd_Channel) as gmrd_Channel ,
if(gmrd_Loc is null ,'000',gmrd_Loc) as gmrd_Loc ,
if(gmrd_Interco is null ,'000',gmrd_Interco) as gmrd_Interco 
from 
rlsf_prod.gmrd_isbn_rev_ref_details
group by 
isbn13 ,
title ,
period ,
legacy_reason ,
legacy_trans_type ,
isbn_Legal_Entity ,
isbn_Line_of_Business ,
isbn_Account ,
isbn_Cost_Center ,
isbn_Product_Line ,
isbn_Channel ,
isbn_Loc ,
isbn_Interco ,
gmrd_Legal_Entity ,
gmrd_Line_of_Business ,
gmrd_Account ,
gmrd_Cost_Center ,
gmrd_Product_Line ,
gmrd_Channel ,
gmrd_Loc ,
gmrd_Interco ;