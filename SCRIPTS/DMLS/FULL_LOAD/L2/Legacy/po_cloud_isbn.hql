set decimal_v2=false;
INSERT OVERWRITE TABLE rlsf_prod.po_cloud_isbn

select 
po_cld.cost_center,po_cld.po_num as po,po_cld.item_num as short_id,po_cld.oracle_id as item,po_cld.isbn13,po_cld.title,po_cld.transaction_date,
'Matching' as po_status,
cast(po_cld.qty_cloud as int) qty_cloud,
cast(isbn.qty_hadoop as int) as qty_hadoop,
cast((po_cld.qty_cloud - isbn.qty_hadoop) as int) as qty_variance, 
if(po_cld.qty_cloud=isbn.qty_hadoop,'Matching','Not Matching') as qty_status
from rlsf_prod.po_cloud_gap po_cld
join rlsf_prod.isbn_gap_po isbn on po_cld.po_grp=isbn.isbn_grp
;

INSERT into TABLE rlsf_prod.po_cloud_isbn

with po_cld as (
select * from rlsf_prod.po_cloud_gap po_cld
left anti join rlsf_prod.isbn_gap_po isbn on po_cld.po_grp=isbn.isbn_grp
)

select 
po_cld.cost_center,po_cld.po_num as po,po_cld.item_num as short_id,po_cld.oracle_id as item,po_cld.isbn13,po_cld.title,po_cld.transaction_date,
'Matching' as po_status,
cast(po_cld.qty_cloud as int) qty_cloud,
cast(isbn.qty_hadoop as int) as qty_hadoop,
cast((po_cld.qty_cloud - isbn.qty_hadoop) as int) as qty_variance, 
if(po_cld.qty_cloud=isbn.qty_hadoop,'Matching','Not Matching') as qty_status
from po_cld
join rlsf_prod.isbn_gap_po_ref isbn on po_cld.po_grp=isbn.isbn_grp
;


INSERT into TABLE rlsf_prod.po_cloud_isbn

select 
po_cld.cost_center,po_cld.po_num as po,po_cld.item_num as short_id,po_cld.oracle_id as item,po_cld.isbn13,po_cld.title,transaction_date,
'NA Hadoop' as po_status,
case when po_cld.qty_cloud is null then 0
else cast(po_cld.qty_cloud as int) 
end as qty_cloud,
0 as qty_hadoop,
case when po_cld.qty_cloud is null then 0
else cast(po_cld.qty_cloud as int) 
end as qty_variance, 
'NA Hadoop' as qty_status
from rlsf_prod.po_cloud_gap po_cld
left anti join rlsf_prod.po_cloud_isbn po_isbn on po_cld.po_grp=concat(trim(po_isbn.cost_center),trim(po_isbn.po),trim(po_isbn.item))
;

INSERT into TABLE rlsf_prod.po_cloud_isbn

select 
isbn.organization_code,isbn.po_wo as po,isbn.short_id,isbn.item,isbn.isbn13,isbn.title,transaction_date,
'NA Legacy' as po_status,
0 as qty_cloud,
case when isbn.qty_hadoop is null then 0
else cast(isbn.qty_hadoop as int) 
end as qty_hadoop,
case when isbn.qty_hadoop is null then 0
else cast(isbn.qty_hadoop as int) 
end as qty_variance, 
'NA Legacy' as qty_status
from rlsf_prod.isbn_gap_po isbn
left anti join rlsf_prod.po_cloud_isbn po_isbn on concat(trim(po_isbn.cost_center),trim(po_isbn.po),trim(po_isbn.item))=isbn.isbn_grp
;