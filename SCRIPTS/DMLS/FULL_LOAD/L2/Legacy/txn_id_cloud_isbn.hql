set decimal_v2=false;
INSERT OVERWRITE TABLE rlsf_prod.txn_id_cloud_isbn
select 
isbn.txn_external_system_ref_id,isbn.short_id as short_id,isbn.item as item,isbn.isbn13,isbn.title,isbn.legacy_reason as legacy_reason,transaction_date,
'NA Legacy' as txn_id_status,
0 as qty_cloud,
case when isbn.qty_hadoop is null then 0
else cast(isbn.qty_hadoop as int) 
end as qty_hadoop,
case when isbn.qty_hadoop is null then 0
else cast(isbn.qty_hadoop as int) 
end as qty_variance,
'NA Legacy' as qty_status
from rlsf_prod.isbn_gap_txn isbn
left anti join rlsf_prod.txn_cloud_gap txn_cld on txn_cld.txn_grp=isbn.isbn_grp

union

select 
txn_cld.transaction_id,txn_cld.item_num as short_id,txn_cld.oracle_id as item,txn_cld.isbn13,txn_cld.title,txn_cld.adjustment_reason as legacy_reason,transaction_date,
'NA Hadoop' as txn_id_status,
case when txn_cld.qty_cloud is null then 0
else cast(txn_cld.qty_cloud as int) 
end as qty_cloud,
0 as qty_hadoop,
case when txn_cld.qty_cloud is null then 0
else cast(txn_cld.qty_cloud as int) 
end as qty_variance,
'NA Hadoop' as qty_status
from rlsf_prod.txn_cloud_gap txn_cld
left anti join rlsf_prod.isbn_gap_txn isbn on txn_cld.txn_grp=isbn.isbn_grp

union

select 
txn_cld.transaction_id,txn_cld.item_num as short_id,txn_cld.oracle_id as item,txn_cld.isbn13,txn_cld.title,txn_cld.adjustment_reason as legacy_reason,txn_cld.transaction_date,
if(txn_cld.transaction_id=isbn.txn_external_system_ref_id,'Matching','Not Matching') as txn_id_status,
cast(txn_cld.qty_cloud as int) as qty_cloud,
cast(isbn.qty_hadoop as int) as qty_hadoop,
cast((txn_cld.qty_cloud-isbn.qty_hadoop) as int) as qty_variance,
if(txn_cld.qty_cloud=isbn.qty_hadoop,'Matching','Not Matching') as qty_status
from rlsf_prod.txn_cloud_gap txn_cld
join rlsf_prod.isbn_gap_txn isbn on txn_cld.txn_grp=isbn.isbn_grp
;