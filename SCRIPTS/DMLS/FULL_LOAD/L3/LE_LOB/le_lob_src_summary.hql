INSERT OVERWRITE TABLE rlsf_prod.le_lob_src_summary

with le_lob_summary as (
--total quantity wms adjustment--
select 
item, 
isbn13,
replace(title,',','') as title, 
source,
legal_entity as dr_legal_entity, 
line_of_business as dr_line_of_business, 
final_quantity as layer_quantity
from rlsf_prod.le_lob_wms_adjustment

union all

--total quantity for 101-000 where transaction_source != WMS ADJUSTMENTS--
select 
item,
isbn13,
replace(title,',','') as title,
source,
dr_legal_entity,
dr_line_of_business,
sum(layer_quantity) as layer_quantity
from rlsf_prod.le_lob
where event='' and trim(transaction_source)<>'WMS ADJUSTMENTS'
group by
item,
isbn13,
title,
source,
dr_legal_entity,
dr_line_of_business

union all

--total quantity for 101-000 where transaction_source = WMS ADJUSTMENTS but has no corresponding split--
select 
eve.item,
eve.isbn13,
replace(eve.title,',','') as title,
eve.source,
eve.dr_legal_entity, 
eve.dr_line_of_business, 
sum(eve.layer_quantity) as layer_quantity
from rlsf_prod.le_lob eve
left anti join 
( select distinct
item,
isbn13,
title,
--source,
substr(dr_legal_entity,1,1) as le
from rlsf_prod.le_lob_wms_detail
where substr(dr_legal_entity,1,1) in ('1','5') and qty_percent<>0) wms_msng on wms_msng.item=eve.item and wms_msng.isbn13=eve.isbn13 and 
wms_msng.title=eve.title and wms_msng.le=substr(eve.dr_legal_entity,1,1) --and wms_msng.source=eve.source
where eve.event='' and trim(eve.transaction_source)='WMS ADJUSTMENTS'
group by
eve.item,
eve.isbn13,
eve.title,
eve.source,
eve.dr_legal_entity, 
eve.dr_line_of_business

)

select 
item,
isbn13,
replace(title,',','') as title,
source,
dr_legal_entity as legal_entity,
des.legal_entity_description as le_description,
dr_line_of_business as line_of_business,
des.line_of_business_description as lob_description,
sum(layer_quantity) as layer_quantity,
substr(cast(now() as string),1,10) as run_date
from le_lob_summary summ
left join
(select distinct legal_entity, line_of_business,legal_entity_description,line_of_business_description  from le_lob_channels) des
on des.legal_entity=summ.dr_legal_entity and des.line_of_business=summ.dr_line_of_business
group by
item,
isbn13,
title,
source,
dr_legal_entity,
dr_line_of_business,
run_date,
des.line_of_business_description,
des.legal_entity_description
;


