INSERT overwrite TABLE rlsf_prod.ohq_current_bi_reconcile_report_compare
SELECT DISTINCT a.oracle_item AS hadoop_item_id
	,a.cost_center AS hadoop_cost_center
	,a.rec_trans_id AS Hadoop_Layer
	,o.isbn13 AS ISBN13
	,o.short_id AS border_item
	,o.title AS description
	,o.opus_num AS opus_item
	,o.inventory_item_flag AS inv
	,o.costing_enabled_flag AS costed
	,o.inventory_asset_flag AS asset
	,o.item_type
	,o.Category_code
	,b.Category_Name
	,o.cat_description
	,o.catalog_code
	,b.item_code AS cloud_item_id
	,om.oicode AS cloud_cost_center
	,b.rec_trxn_id AS cloud_Layer
	,round(a.tot_qty, 2) hadoop_tot_qty
	,round(b.quantity, 2) cloud_quantity
	,CASE 
		WHEN a.oracle_item = b.item_code
			AND round(a.tot_qty, 2) = round(b.quantity, 2)
			THEN 'Matched'
		WHEN a.oracle_item IS NULL
			AND a.cost_center IS NULL
			AND a.rec_trans_id IS NULL
			THEN 'NA – Hadoop'
		WHEN b.item_code IS NULL
			AND b.rec_trxn_id IS NULL
			AND b.organization_id IS NULL
			THEN 'NA - Cloud'
		ELSE 'Not Matched'
		END quantity_status
	,(abs(round(a.tot_qty, 2)) - abs(round(b.quantity, 2))) diff_qty
	,abs((abs(round(a.tot_qty, 2)) - abs(round(b.quantity, 2)))) abs_diff_qty
	,round(a.unit_cost, 2) AS Hadoop_Unit_cost
	,round(b.unit_cost, 2) AS cloud_unit_cost
	,CASE 
		WHEN a.oracle_item = b.item_code
			AND a.unit_cost = b.unit_cost
			THEN 'Matched'
		WHEN a.oracle_item IS NULL
			AND a.cost_center IS NULL
			AND a.rec_trans_id IS NULL
			THEN 'NA – Hadoop'
		WHEN b.item_code IS NULL
			AND b.rec_trxn_id IS NULL
			AND b.organization_id IS NULL
			THEN 'NA - Cloud'
		ELSE 'Not Matched'
		END Unit_cost_Status
	,round(a.tot_amount, 2) hadoop_tot_amount
	,round(b.value, 2) cloud_amount
	,CASE 
		WHEN a.oracle_item = b.item_code
			AND round(a.tot_amount, 2) = round(b.value, 2)
			THEN 'Matched'
		WHEN a.oracle_item IS NULL
			AND a.cost_center IS NULL
			AND a.rec_trans_id IS NULL
			THEN 'NA – Hadoop'
		WHEN b.item_code IS NULL
			AND b.rec_trxn_id IS NULL
			AND b.organization_id IS NULL
			THEN 'NA - Cloud'
		ELSE 'Not Matched'
		END amount_status
	,(abs(round(a.tot_amount, 2)) - abs(round(b.value, 2))) diff_amount
	,abs((round(a.tot_amount, 2) - round(b.value, 2))) abs_diff_amount
FROM rlsf_prod.ohq_current_summary a
FULL OUTER JOIN rlsf_prod.bi_reconcile_report b ON trim(a.oracle_item) = trim(b.item_code)
	--AND trim(a.rec_trans_id) = trim(b.rec_trxn_id)
LEFT JOIN rlsf_prod.oracle_items_parquet o ON trim(o.oracle_id) = trim(a.oracle_item)
LEFT JOIN rlsf_prod.org_master_parquet om ON trim(om.oiorgid) = trim(b.organization_id)
WHERE a.cost_center IN (
		'EXU'
		,'CGS'
		,'BRY'
		,'BNP'
		,'BNG'
		,'BDR'
		);