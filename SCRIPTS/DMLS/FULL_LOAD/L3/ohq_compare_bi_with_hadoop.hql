insert overwrite table rlsf_prod.ohq_compare_bi_with_hadoop
with layer_cost as (
select distinct * from rlsf_prod.ohq_inv_as_of_date_with_layer_cost
)

select 
bi.oracle_item as oracle_item_number,
bi.org_id as inventory_organization,
CAST(cast(ohqty as double) AS DECIMAL(38,6)) as bi_extract_qty,
sum(total_qty) as hadoop_extract_qty,
hadoop.inventory_item_id as hadoop_inventory_item_id,
hadoop.org_id as hadoop_org_id,
case 
when CAST(cast(ohqty as double) AS DECIMAL(38,6))  - sum(total_qty) = 0 then 'Matched'
else 
'Not Matched' end as diff_state,
CAST((cast(cast(ohqty as double) AS DECIMAL(38,6)) - sum(total_qty)) as decimal(38,6)) as diff_value,
abs(CAST((cast(cast(ohqty as double) AS DECIMAL(38,6)) - sum(total_qty)) as decimal(38,6))) as diff_value_ABS,
if(inv.item_type like "%-%-%:%:%",substr(inv.item_type,1,length(inv.item_type)-20),inv.item_type) as item_type
from rlsf_prod.sch_bi_extract_parquet bi
join layer_cost hadoop on hadoop.oracle_item =bi.oracle_item and hadoop.cost_center = bi.org_id
left join rlsf_prod.inv_master_distinct_parquet inv on bi.oracle_item = inv.oracle_item_id
group by 
oracle_item_number,
Hadoop_inventory_item_id,
hadoop_org_id,
inventory_organization,
bi_extract_qty,
item_type;


--only from OHQ--

insert into table rlsf_prod.ohq_compare_bi_with_hadoop

with layer_cost as (
select distinct ohq.* from rlsf_prod.ohq_inv_as_of_date_with_layer_cost ohq
left anti join rlsf_prod.ohq_compare_bi_with_hadoop rec
on ohq.oracle_item = rec.oracle_item_number and ohq.cost_center = rec.inventory_organization
)

select 
ohq.oracle_item as oracle_item_number,
ohq.cost_center as inventory_organization,
0 as bi_extract_qty,
sum(ohq.total_qty) as hadoop_extract_qty,
ohq.inventory_item_id as hadoop_inventory_item_id,
org_id as hadoop_org_id,
'NA-Cloud' as diff_state,
sum(ohq.total_qty) as diff_value,
abs(sum(ohq.total_qty)) as diff_value_ABS,
if(inv.item_type like "%-%-%:%:%",substr(inv.item_type,1,length(inv.item_type)-20),inv.item_type) as item_type
from layer_cost ohq
left join rlsf_prod.inv_master_distinct_parquet inv on ohq.oracle_item = inv.oracle_item_id
group by 
oracle_item_number,
Hadoop_inventory_item_id,
hadoop_org_id,
inventory_organization,
bi_extract_qty,
item_type;


--only from BI--

insert into table rlsf_prod.ohq_compare_bi_with_hadoop

with bi_ext as (
select bi.* from rlsf_prod.sch_bi_extract_parquet bi
left anti join rlsf_prod.ohq_compare_bi_with_hadoop rec on
rec.oracle_item_number =bi.oracle_item and rec.inventory_organization = bi.org_id
)

select 
bi.oracle_item as oracle_item_number,
bi.org_id as inventory_organization,
CAST(cast(bi.ohqty as double) AS DECIMAL(38,6)) as bi_extract_qty,
0 as hadoop_extract_qty,
'' as hadoop_inventory_item_id,
'' as hadoop_org_id,
'NA-Hadoop' as diff_state,
CAST(cast(bi.ohqty as double) AS DECIMAL(38,6)) as diff_value,
abs(CAST(cast(bi.ohqty as double) AS DECIMAL(38,6))) as diff_value_ABS,
if(inv.item_type like "%-%-%:%:%",substr(inv.item_type,1,length(inv.item_type)-20),inv.item_type) as item_type
from bi_ext bi
left join rlsf_prod.inv_master_distinct_parquet inv on bi.oracle_item = inv.oracle_item_id