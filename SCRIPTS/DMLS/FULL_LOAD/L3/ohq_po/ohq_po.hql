insert overwrite table rlsf_prod.ohq_po

with gl as
( select distinct inventory_org_id,rec_trxn_id,inventory_item_id,source_reference,inv_transaction_type
from rlsf_prod.gl_master_hist
where distribution_dr_cr_sign = 'DR' and inv_transaction_type in ('Purchase Order Receipt','Purchase Order Receipt Adjustment',
'Work in Process Product Completion') )
,
recp as (
select * from gl
where inv_transaction_type in ('Purchase Order Receipt' ) )
, recp_adj as (
select * from recp
union
select * from gl
left anti join recp on gl.inventory_org_id=recp.inventory_org_id and gl.rec_trxn_id=recp.rec_trxn_id and 
gl.inventory_item_id=recp.inventory_item_id and gl.source_reference=recp.source_reference
where gl.inv_transaction_type in ('Purchase Order Receipt Adjustment','Work in Process Product Completion' ) )


select 
oracle_item, isbn13, border_item, opus_id, cost_center, org_id, lc_p_eo_rec_trans_id, 
ohq.inventory_item_id as inventory_item_id, total_qty, unit_cost, total_amount, 
recp_adj.source_reference as po_number, 
recp_adj.inv_transaction_type as transaction_type_name,
layer_date, date_, run_date, as_of_date
from rlsf_prod.ohq_inv_as_of_date_with_layer_cost ohq
left join recp_adj on ohq.org_id=recp_adj.inventory_org_id and ohq.lc_p_eo_rec_trans_id=recp_adj.rec_trxn_id and 
ohq.inventory_item_id=recp_adj.inventory_item_id

;