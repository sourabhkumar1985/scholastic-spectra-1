set DECIMAL_V2=false;

truncate rlsf_prod.gl_master_hist_2;

insert overwrite TABLE rlsf_prod.gl_master_hist_2 

select distribution_line_id ,
txn_transaction_id ,
txn_creation_date ,
gl.txn_external_system_ref_id ,
gl.distribution_id ,
'' as dist_cost_txn_type ,
event_id ,
rec_trxn_id ,
distribution_gl_date ,
round(cast(entered_currency_amount as decimal(38,6)),2) as currency,
distribution_dr_cr_sign ,
dist_cost_element_id ,
sla_code_combination_id ,
ae_header_id ,
gl_transfer_date ,
case when lyr.distribution_id=gl.distribution_id then '0' 
--when accounting_class_code='OFFSET' then '0'
else layer_quantity 
end as layer_quantity,
case 
--when ext.txn_external_system_ref_id=gl.txn_external_system_ref_id and ext.distribution_id=gl.distribution_id
--then ext.period
when substr(txn_creation_date,6,2)='01' then concat('Jan-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='02' then concat('Feb-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='03' then concat('Mar-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='04' then concat('Apr-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='05' then concat('May-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='06' then concat('Jun-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='07' then concat('Jul-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='08' then concat('Aug-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='09' then concat('Sep-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='10' then concat('Oct-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='11' then concat('Nov-',substr(txn_creation_date,3,2))
when substr(txn_creation_date,6,2)='12' then concat('Dec-',substr(txn_creation_date,3,2))
else gl.period
end period,
jrnl_batch_name ,
gl_batch_desc ,
journal_creation_date ,
jrnl_line_num ,
entered_cr ,
entered_dr ,
accounted_dr ,
accounted_cr ,
ledger_name ,
txn_num ,
segment1 ,
segment2 ,
segment3 ,
segment4 ,
segment5 ,
segment6 ,
segment7 ,
segment8 ,
je_batch_id ,
accounting_class_code ,
transaction_date ,
imt_creation_date ,
legacy_reason ,
legacy_transaction_type ,
source ,
source_line ,
item ,
source_reference ,
reference ,
transaction_source ,
case when lyr.distribution_id=gl.distribution_id then 'Cost Adjustment' 
else transaction_type end as transaction_type ,
organization_code ,
inventory_item_id ,
inventory_org_id ,
cst_inv_transaction_id ,
short_id ,
journal_name ,
inv_transaction_type ,
inv_transaction_source
from rlsf_prod.gl_master_parquet gl
left join rlsf_prod.gl_layer_cost_parquet lyr on lyr.distribution_id=gl.distribution_id 
--left join ext on ext.txn_external_system_ref_id=gl.txn_external_system_ref_id and ext.distribution_id=gl.distribution_id
where gl.period='' and gl.journal_name='' and round(abs(cast(gl.entered_currency_amount as decimal(38,6)))) =0;

--round(abs(cast(gl.entered_currency_amount as decimal(38,6)))) =0;

insert into TABLE rlsf_prod.gl_master_hist_2 

select distribution_line_id ,
txn_transaction_id ,
txn_creation_date ,
txn_external_system_ref_id ,
gl.distribution_id ,
'' as dist_cost_txn_type ,
event_id ,
rec_trxn_id ,
distribution_gl_date ,
case when entered_currency_amount='' and entered_cr='' then cast(entered_dr as decimal(38,2))
when entered_currency_amount='' and entered_dr='' then cast(entered_cr as decimal(38,2))*(-1)
else round(cast(entered_currency_amount as decimal(38,6)),2) end as currency,
distribution_dr_cr_sign ,
dist_cost_element_id ,
sla_code_combination_id ,
ae_header_id ,
gl_transfer_date ,
case when lyr.distribution_id=gl.distribution_id then '0' 
--when accounting_class_code='OFFSET' then '0'
else layer_quantity 
end as layer_quantity,
period ,
jrnl_batch_name ,
gl_batch_desc ,
journal_creation_date ,
jrnl_line_num ,
entered_cr ,
entered_dr ,
accounted_dr ,
accounted_cr ,
ledger_name ,
txn_num ,
segment1 ,
segment2 ,
segment3 ,
segment4 ,
segment5 ,
segment6 ,
segment7 ,
segment8 ,
je_batch_id ,
accounting_class_code ,
transaction_date ,
imt_creation_date ,
legacy_reason ,
legacy_transaction_type ,
source ,
source_line ,
item ,
source_reference ,
reference ,
transaction_source ,
case when lyr.distribution_id=gl.distribution_id then 'Cost Adjustment' 
else transaction_type end as transaction_type ,
organization_code ,
inventory_item_id ,
inventory_org_id ,
cst_inv_transaction_id ,
short_id ,
journal_name ,
inv_transaction_type ,
inv_transaction_source
from rlsf_prod.gl_master_parquet gl
left join rlsf_prod.gl_layer_cost_parquet lyr on lyr.distribution_id=gl.distribution_id
where period<>'' and gl.ledger_name<>'';