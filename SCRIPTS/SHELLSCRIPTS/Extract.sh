#!/bin/sh
JAVA_HOME=/usr/java/jdk1.8.0_181-cloudera
PATH=$PATH:$JAVA_HOME/bin

LOG=Extract-$(date +%s%N).log
echo `date` >> /tmp/${LOG}
counter=0
counterstart=`expr $counter`
counterend=`expr $counterstart + 30`
while true
do
                if hdfs dfs -test -e /sch/trigger/wftrigger.txt ;
                then
                cd /home/schuser/di
                        echo "Running Mysql Load program " >> /tmp/${LOG}
                        `java -cp $(hadoop classpath):/home/schuser/di/di-0.0.1-SNAPSHOT-jar-with-dependencies.jar:/home/schuser/di/oracle.cloud.storage.api-13.1.12.jar com.rl.dataload.FileLoad >>/tmp/${LOG} 2>&1`
                        if [ $? -ne 0 ];
                        then
                                        echo "mysql load program failed " >> /tmp/${LOG}
                                        exit 1
                        fi
                        `java -cp $(hadoop classpath):/home/schuser/di/di-0.0.1-SNAPSHOT-jar-with-dependencies.jar:/home/schuser/di/oracle.cloud.storage.api-13.1.12.jar com.rl.di.OracleDI >>/tmp/${LOG}2>&1`
                        if [ $? -ne 0 ];
                        then
                                        echo "extraction program failed " >> /tmp/${LOG}
                                        exit 1
                        fi
                        `java -cp $(hadoop classpath):/home/schuser/di/java -cp di-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.rl.email.ReportEmail >>/tmp/${LOG}2>&1`

                        hadoop fs -touchz /sch/trigger/tfile
                        echo " successfull ran both mysql and extraction "
                        break
                else
                echo " sleeping for 1 minute"
                counterstart=`expr $counterstart + 1`
                if [ $counterstart -gt $counterend ]
                then
                        echo "exiting program as the wf trigger has not recived since 30 mins"
                        echo 'We havent recived the trigger from the delta worflow SCH_DL_UMB_WF. Please check if the worklfow has completed successfully or rerun from thefrom the failed step if there are any failures. The workflow can be monitored in oozie schedule with the name SCH_DL_UMB_COORD' | mailx -s 'Oracle Extract jobs exited as no Trigger recieved from SCH_DL_UMB_WF [report Workflow]' kumaryas@scholastic.com,sgb-consultant@scholastic.com,sjayavinod-consultant@scholastic.com,techops-middlewaresupport@scholastic.com,techops-operations@scholastic.com
                break
                fi
                sleep 60
                fi
done

hadoop fs -put /tmp/${LOG} /sch/logs
rm /tmp/${LOG}