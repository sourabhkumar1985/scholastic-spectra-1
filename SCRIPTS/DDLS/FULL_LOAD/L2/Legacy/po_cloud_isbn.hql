DROP TABLE IF EXISTS rlsf_prod.po_cloud_isbn;
CREATE EXTERNAL TABLE rlsf_prod.po_cloud_isbn (
cost_center STRING,
po STRING,
short_id STRING,
item STRING,
isbn13 STRING,
title STRING,
transaction_date STRING,
po_status STRING,
qty_cloud INT,
qty_hadoop INT,
qty_variance INT,
qty_status STRING
) STORED AS PARQUET LOCATION '/sch/l2/po_cloud_isbn';