DROP TABLE IF EXISTS rlsf_prod.ohq_inv_total_qty_by_cost_rec_po;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_total_qty_by_cost_rec_po (
layer_qty_sum DECIMAL(38,6),
trans_eo_inventory_item_id STRING,
lc_p_eo_rec_trans_id STRING,
date_ STRING,
oracle_id STRING,
po_number STRING,
transaction_type_name STRING
) STORED AS PARQUET LOCATION '/sch/l2/ohq_po/ohq_inv_total_qty_by_cost_rec_po';
