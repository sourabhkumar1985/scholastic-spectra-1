DROP TABLE IF EXISTS rlsf_prod.ohq_compare_bi_with_hadoop_agg;
CREATE EXTERNAL TABLE rlsf_prod.ohq_compare_bi_with_hadoop_agg (
description string,
total string
) STORED AS PARQUET LOCATION '/sch/l3/ohq_compare_bi_with_hadoop_agg' ;