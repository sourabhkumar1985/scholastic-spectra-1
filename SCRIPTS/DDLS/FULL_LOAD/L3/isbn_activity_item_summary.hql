DROP TABLE IF EXISTS rlsf_prod.isbn_activity_item_summary;
CREATE EXTERNAL TABLE rlsf_prod.isbn_activity_item_summary (
item STRING,
oiorgid STRING,
organization_code STRING,
gl_batch_desc STRING,
isbn13 STRING,
qty decimal(38,6),
period STRING
) STORED AS PARQUET LOCATION '/sch/l3/isbn_activity_item_summary';