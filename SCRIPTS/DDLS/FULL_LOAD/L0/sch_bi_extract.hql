DROP TABLE IF EXISTS rlsf_prod.sch_bi_extract;
CREATE EXTERNAL TABLE rlsf_prod.sch_bi_extract (
	oracle_item STRING
	,org_id STRING
	,ohqty STRING
	) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/sch_bi_extract';