DROP TABLE

IF EXISTS rlsf_prod.inv_master_distinct;
	CREATE EXTERNAL TABLE rlsf_prod.inv_master_distinct (
		oracle_item_id STRING
		,inventory_item_id STRING
		,DESCRIPTION string
		,ITEM_TYPE string
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/inv_master_distinct';