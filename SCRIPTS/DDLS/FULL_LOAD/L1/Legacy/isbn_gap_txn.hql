DROP TABLE IF EXISTS rlsf_prod.isbn_gap_txn;
CREATE EXTERNAL TABLE rlsf_prod.isbn_gap_txn (
txn_external_system_ref_id STRING,
short_id STRING,
item STRING,
isbn13 STRING,
title STRING,
legacy_reason STRING,
transaction_date STRING,
qty_hadoop decimal(38,6),
isbn_grp STRING
) STORED AS PARQUET LOCATION '/sch/l1/isbn_gap_txn';