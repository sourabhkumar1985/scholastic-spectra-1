drop table if exists rlsf_prod.cost_trans_isbn_lite;
CREATE EXTERNAL TABLE rlsf_prod.cost_trans_isbn_lite (
   organization_code STRING, 
   item STRING, 
   transaction_quantity decimal(38,6), 
   transaction_date STRING, 
   transaction_type STRING, 
   reference STRING, 
   legacy_reason STRING, 
   short_id STRING, 
   transaction_source_name STRING, 
   legacy_transaction_type STRING, 
   source STRING, 
   source_reference STRING, 
   source_line STRING, 
   imt_creation_date STRING) 
 STORED AS parquet LOCATION '/sch/l1/cost_trans_isbn_lite' ;