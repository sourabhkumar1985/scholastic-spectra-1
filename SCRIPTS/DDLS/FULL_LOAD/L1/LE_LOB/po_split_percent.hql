CREATE EXTERNAL TABLE rlsf_prod.po_split_percent (
po_header_id STRING,
po_status  STRING,
po_number STRING,
po_creation_date STRING,
po_last_update_date STRING,
document_style STRING,
po_line_id STRING,
po_line_quantity STRING,
po_loc_qty decimal(38,6), 
le_lob_forecast decimal(38,6), 
major_channel STRING,
po_qty_percent decimal(38,6)
) STORED AS PARQUET LOCATION '/sch/l1/po_split_percent';
