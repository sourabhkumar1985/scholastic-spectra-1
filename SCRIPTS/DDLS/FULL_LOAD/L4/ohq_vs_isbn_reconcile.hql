CREATE EXTERNAL TABLE rlsf_prod.ohq_vs_isbn_reconcile ( 
oracle_item STRING, 
title STRING, 
inv_item_id STRING, 
cost_center STRING, 
org_id STRING, 
ohq_layer STRING, 
activity_layer STRING, 
layer_status STRING, 
sum_qty_activity decimal(38,6), 
sum_qty_ohq decimal(38,6), 
diff_qty decimal(38,6), 
qty_status STRING, 
Run_Date STRING, 
run_Time STRING
)  STORED AS PARQUET LOCATION '/sch/l4/ohq_vs_isbn_reconcile';