DROP TABLE IF EXISTS rlsf_prod.isbn_compare_ledger_with_hadoop;
CREATE EXTERNAL TABLE rlsf_prod.isbn_compare_ledger_with_hadoop (
Batch_Name string,
DR_Hadoop_Total_Amount decimal(38,6),
DR_Ledger_Total_Amount string,
Diff_State string,
Diff_Value string
 ) STORED AS PARQUET LOCATION '/sch/l4/isbn_compare_ledger_with_hadoop';