DROP TABLE IF EXISTS rlsf_prod.lite_isbn_reconcile;
CREATE EXTERNAL TABLE rlsf_prod.lite_isbn_reconcile (
txn_id STRING,
txn_id_status STRING,
lite_qty int,
isbn_qty int,
qty_status STRING
) STORED AS PARQUET LOCATION '/sch/l4/lite_isbn_reconcile';