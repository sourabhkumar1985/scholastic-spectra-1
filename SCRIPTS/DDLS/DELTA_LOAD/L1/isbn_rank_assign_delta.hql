DROP TABLE IF EXISTS rlsf_prod.isbn_rank_assign_delta;
CREATE EXTERNAL TABLE rlsf_prod.isbn_rank_assign_delta (   JRNL_BATCH_NAME STRING,
   ranknumber BIGINT ) STORED AS PARQUET LOCATION '/sch/l1/isbn_rank_assign_delta';
