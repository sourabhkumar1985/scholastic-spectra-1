DROP TABLE

IF EXISTS rlsf_prod.material_transactions;
	CREATE EXTERNAL TABLE rlsf_prod.material_transactions (
		txn_id STRING
		,txn_qty DOUBLE
		,organization_id STRING
		,item_id STRING
		,transaction_date STRING
		,imt_creation_date STRING
		,last_update_date STRING
		,source STRING
		,legacy_transaction_type STRING
		,legacy_reason STRING
		,reference STRING
		,source_line STRING
		,costed_flag STRING
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
		WITH SERDEPROPERTIES('field.delim' = '|', 'serialization.format' = '|') STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/material_transactions';