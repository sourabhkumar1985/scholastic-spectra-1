DROP TABLE

IF EXISTS rlsf_prod.inventory_orgs;
	CREATE EXTERNAL TABLE rlsf_prod.inventory_orgs (
organization_id string,
organization_name string,
organization_code string,
set_of_books_id string,
chart_of_accounts_id string,
currency_code string,
period_set_name string,
inventory_enabled_flag string,
business_unit_name string,
business_unit_id string,
legal_entity string,
organization_type string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/inventory_orgs';