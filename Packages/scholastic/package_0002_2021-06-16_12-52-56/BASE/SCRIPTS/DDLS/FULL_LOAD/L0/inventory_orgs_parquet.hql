DROP TABLE

IF EXISTS rlsf_prod.inventory_orgs_parquet;
	CREATE EXTERNAL TABLE rlsf_prod.inventory_orgs_parquet (
	organization_id STRING
	,organization_name STRING
	,organization_code STRING
	,set_of_books_id STRING
	,chart_of_accounts_id STRING
	,currency_code STRING
	,period_set_name STRING
	,inventory_enabled_flag STRING
	,business_unit_name STRING
	,business_unit_id STRING
	,legal_entity STRING
	,organization_type STRING
	) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l0/inventory_orgs_parquet';