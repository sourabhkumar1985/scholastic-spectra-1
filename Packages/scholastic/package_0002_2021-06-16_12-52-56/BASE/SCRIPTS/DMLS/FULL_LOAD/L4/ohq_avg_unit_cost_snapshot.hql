SET DECIMAL_V2 = false;

INSERT overwrite rlsf_prod.ohq_avg_unit_cost_snapshot
SELECT cost_center
	,oracle_item
	,isbn13
	,replace(title, ',', '') AS title
	,opus_id
	,border_item
	,inventory_item_id
	,org_id
	,cast(total_amount AS DECIMAL(38, 2))
	,cast(qty AS bigint)
	,CASE 
		WHEN total_amount = 0.000000
			THEN 0.00
		ELSE abs(cast(ifnull(sum(round(total_amount, 2) / round(qty, 2)), 0) AS DECIMAL(38, 2)))
		END AS avg_unit_cost
	,as_of_date
	,mdm_id
	,short_id
	,category
FROM (
	SELECT cost_center
		,oi.oracle_id AS oracle_item
		,po.isbn13
		,oi.title
		,opus_id
		,border_item
		,po.inventory_item_id
		,org_id
		,sum(total_amount) AS total_amount
		,sum(total_quantity) AS qty
		,as_of_date
		,oi.mdm_id
		,oi.short_id
		,category
	FROM rlsf_prod.ohq_po po
	LEFT JOIN rlsf_prod.oracle_items_parquet oi ON trim(oi.inventory_item_id) = trim(po.inventory_item_id)
	GROUP BY cost_center
		,oi.oracle_id
		,po.isbn13
		,oi.title
		,opus_id
		,border_item
		,po.inventory_item_id
		,org_id
		,as_of_date
		,oi.mdm_id
		,oi.short_id
		,category
	) a
GROUP BY cost_center
	,oracle_item
	,isbn13
	,title
	,opus_id
	,border_item
	,inventory_item_id
	,org_id
	,total_amount
	,qty
	,as_of_date
	,mdm_id
	,short_id
	,category;