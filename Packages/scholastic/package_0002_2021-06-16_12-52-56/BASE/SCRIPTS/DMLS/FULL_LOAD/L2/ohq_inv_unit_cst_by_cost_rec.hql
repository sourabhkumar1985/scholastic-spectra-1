SET decimal_v2 = false;

WITH only_receipt
AS (
	SELECT DISTINCT deb.transactionseoinventoryitemid
		,deb.transactionseoinventoryorgid
		,deb.cstlayercostspeorectrxnid
	FROM rlsf_prod.cost_trans_debit deb -- change table name accordingly
	WHERE cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT AS DECIMAL(38, 6)) <> 0
		AND deb.cstlayercostspeocosttransactiontype = 'RECEIPT'
	)
INSERT INTO TABLE rlsf_prod.ohq_inv_unit_cst_by_cost_rec
SELECT DISTINCT deb.transactionseoinventoryitemid
	,deb.transactionseoinventoryorgid
	,deb.cstlayercostspeorectrxnid
	,cast(deb.layer_unit_cost AS DECIMAL(38, 6)) AS cost_unit
	,deb.transactionseocreationdate AS transactionseocreationdate
	,deb.costdistributionlineseocostelementid
	,deb.distributionlineid
	,deb.cstlayercostspeocosttransactiontype
FROM rlsf_prod.cost_trans_debit deb -- change table name accordingly
	left anti
JOIN only_receipt recp ON recp.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND recp.transactionseoinventoryorgid = deb.transactionseoinventoryorgid
	AND recp.cstlayercostspeorectrxnid = deb.cstlayercostspeorectrxnid
JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
WHERE cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT AS DECIMAL(38, 6)) > 0
	AND cast(costdistributionseolayerquantity AS DECIMAL(38, 6)) > 0
	AND deb.cstlayercostspeocosttransactiontype = 'ADJUST'
	AND deb.cstlayercostspeorectrxnid IN (
		SELECT DISTINCT cstlayercostspeorectrxnid
		FROM (
			SELECT cstlayercosttransid
				,cstlayercostspeorectrxnid
				,sum(cast(costdistributionseolayerquantity AS DECIMAL(38, 6))) AS layer_unit_qty
			FROM rlsf_prod.cost_trans_debit
			GROUP BY cstlayercosttransid
				,cstlayercostspeorectrxnid
			) aa
		WHERE cast(layer_unit_qty AS DECIMAL(38, 6)) > 0
		);

SET decimal_v2 = false;

INSERT INTO TABLE rlsf_prod.ohq_inv_unit_cst_by_cost_rec
SELECT DISTINCT deb.transactionseoinventoryitemid
	,deb.transactionseoinventoryorgid
	,deb.cstlayercostspeorectrxnid
	,
	--cast((cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) / cast(deb.COSTDISTRIBUTIONSEOLAYERQUANTITY as --decimal(38,6))) as decimal(38,6)) as cost_unit,
	cast(deb.layer_unit_cost AS DECIMAL(38, 6)) AS cost_unit
	,deb.transactionseocreationdate AS transactionseocreationdate
	,deb.costdistributionlineseocostelementid
	,deb.distributionlineid
	,deb.cstlayercostspeocosttransactiontype
FROM rlsf_prod.cost_trans_debit deb -- change table name accordingly
	left anti
JOIN rlsf_prod.ohq_inv_unit_cst_by_cost_rec un_cst ON un_cst.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND un_cst.transactionseoinventoryorgid = deb.transactionseoinventoryorgid
	AND un_cst.cstlayercostspeorectrxnid = deb.cstlayercostspeorectrxnid
JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
WHERE cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT AS DECIMAL(38, 6)) <> 0
	AND cast(costdistributionseolayerquantity AS DECIMAL(38, 6)) > 0
	AND deb.cstlayercostspeocosttransactiontype IN (
		'ADJUST'
		,'RECEIPT'
		);
	--and deb.cstlayercostspeorectrxnid in (select distinct cstlayercostspeorectrxnid from 
	--(select cstlayercostspeorectrxnid,sum(cast(costdistributionseolayerquantity as decimal (38,6))) as layer_unit_qty 
	--from rlsf_prod.cost_trans_debit group by cstlayercostspeorectrxnid) aa
	--where  cast(layer_unit_qty as decimal(38,6)) >0)