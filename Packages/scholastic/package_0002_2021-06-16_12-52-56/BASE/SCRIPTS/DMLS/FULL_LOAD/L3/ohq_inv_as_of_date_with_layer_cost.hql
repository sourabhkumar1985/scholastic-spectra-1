INSERT INTO TABLE rlsf_prod.ohq_inv_as_of_date_with_layer_cost
SELECT DISTINCT ohq.oracle_item AS oracle_item
	,ohq.isbn13 AS isbn13
	,ohq.border_item AS border_item
	,ohq.opus_id AS opus_id
	,ohq.cost_center AS cost_center
	,ohq.org_id
	,org.organization_name
	,ohq.lc_p_eo_rec_trans_id
	,ohq.trans_eo_inventory_item_id AS inventory_item_id
	,layer_qty_sum AS total_qty
	,layer_cost_sum AS unit_cost
	,total_amount AS total_amount
	,layer_date AS layer_date
	,substr(layer_date, 1, 4) AS year_
	,CASE 
		WHEN substr(layer_date, 6, 2) = '01'
			THEN 'Jan'
		WHEN substr(layer_date, 6, 2) = '02'
			THEN 'Feb'
		WHEN substr(layer_date, 6, 2) = '03'
			THEN 'Mar'
		WHEN substr(layer_date, 6, 2) = '04'
			THEN 'Apr'
		WHEN substr(layer_date, 6, 2) = '05'
			THEN 'May'
		WHEN substr(layer_date, 6, 2) = '06'
			THEN 'Jun'
		WHEN substr(layer_date, 6, 2) = '07'
			THEN 'Jul'
		WHEN substr(layer_date, 6, 2) = '08'
			THEN 'Aug'
		WHEN substr(layer_date, 6, 2) = '09'
			THEN 'Sep'
		WHEN substr(layer_date, 6, 2) = '10'
			THEN 'Oct'
		WHEN substr(layer_date, 6, 2) = '11'
			THEN 'Nov'
		WHEN substr(layer_date, 6, 2) = '12'
			THEN 'Dec'
		ELSE ''
		END AS month_
	,ohq.date_
	,ohq.run_date
	,ohq.as_of_date
FROM rlsf_prod.ohq_inv_onhand_qty_cost_by_layer ohq
LEFT JOIN rlsf_prod.inventory_orgs_parquet org ON org.organization_code = ohq.cost_center
JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = ohq.trans_eo_inventory_item_id
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
JOIN (
	SELECT org_id
		,lc_p_eo_rec_trans_id
		,trans_eo_inventory_item_id
		,max(date_) AS maxdate
	FROM rlsf_prod.ohq_inv_onhand_qty_cost_by_layer
	GROUP BY org_id
		,lc_p_eo_rec_trans_id
		,trans_eo_inventory_item_id
	) maxdate ON ohq.org_id = maxdate.org_id
	AND ohq.lc_p_eo_rec_trans_id = maxdate.lc_p_eo_rec_trans_id
	AND ohq.trans_eo_inventory_item_id = maxdate.trans_eo_inventory_item_id
	AND ohq.date_ = maxdate.maxdate;
	--where layer_qty_sum > 0