#!/bin/sh

#
# DEPLOYEMENT SHELL SCRIPT
#

HOSTNAME=$(hostname)
CONFIG=config.properties

REPLACECONFIG=""
KEYTAB=""
PRINCIPAL=""
PROJECTDIR=""


CURRDATE=$(date)
echo -e "\e[33m ------------------------------------------------------------------------------------------------------\e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
echo -e "\e[33m!!!!!!!!!!!!!!!!PROCESSING DATE :\e[37m" $CURRDATE | tee -a  ../../LOG/JOBLOG/execute.log

if [ -f ../../dblog ]
then
	rm ../../dblog
fi
DEPUSER=$(grep 'PREPARED' readme.txt | cut  -c13-)
DEPMAIL=$(grep 'EMAIL:' readme.txt | cut  -c7-)
SVN=$(grep 'SVN REVESION' readme.txt | cut  -c14-)
PACKAGENAME=$(basename "$PWD")
PROJECT=$(grep 'PROJECT:' $CONFIG | cut  -c9-)

echo -e "\e[36m DEPLOYEMENT REQUESTED BY: $DEPUSER \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
echo -e "\e[36m EMAIL: $DEPMAIL \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
echo -e "\e[36m SVN REVESION: $SVN \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
echo -e "\e[36m PROCESSING PACKAGE: $PACKAGENAME \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log




if [ ! -d ../../LOG/JOBLOG ]
then
	mkdir -p ../../LOG/JOBLOG
fi

if [ ! -d ../../LOG/REPORT ] 
then 
	mkdir -p ../../LOG/REPORT
fi

cat readme.txt > ../../readme.txt

if [ -d FINAL ]
then
	rm -rf FINAL
fi

mkdir FINAL


if [[ $HOSTNAME == *"ip-10-130-13-233"* ]] 
then
	echo -e "\e[32m PROCESSING FOR DEV \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
	SYSTEM=DEV
fi

if [[ $HOSTNAME == *"ip-10-131-13-199"* ]] 
then
	echo -e "\e[32m PROCESSING FOR PROD \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
	SYSTEM=PROD
fi

function errorlog(){
	MESSAGE=$@
	echo -e "\e[31m <ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR> \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
	echo -e "\e[31m "$MESSAGE "\e[37m"| tee -a  ../../LOG/JOBLOG/execute.log
	echo -e "\e[31m SCRIPT IS EXITING PLEASE CONTACT :  ${DEPUSER} AT  ${DEPMAIL}\e[37m"  | tee -a  ../../LOG/JOBLOG/execute.log
	echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "FAIL" "|" $MESSAGE >> ../../LOG/REPORT/report.csv
	echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "FAIL" "|" $MESSAGE > ../../dblog
	echo -e "\e[31m <ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR> \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
	cd ../../
	java -jar postgreswriter.jar $SYSTEM $PROJECT
	exit 4

}



currpath=$(pwd)
currentpackagename=$(basename $currpath)
IFS="_"
set -- $currentpackagename
currentpackagenum=$2
IFS=""

if [ $currentpackagenum -ne 0 ] 
 then
	previouspackage=$(tail -1 ../../LOG/REPORT/report.csv)
	IFS="|"
	set -- $previouspackage
	prevpackagename=$4
	IFS="_"
	set -- $prevpackagename
	previouspackenum=$2
	IFS=""
	diffnum=`expr $currentpackagenum - 1`
	echo "Previous package number is $previouspackenum"
	echo "Current  package number is $currentpackagenum"
	if [ $previouspackenum -eq $currentpackagenum  ]
	then
		echo -e "\e[35m Previus deployed package is same as current package \e[37m"|| tee -a  ../../LOG/JOBLOG/execute.log
	elif [ $diffnum -ne  $previouspackenum ]
	then
		echo -e "\e[31m Current packege num is "$currentpackagenum " and previous package num is "$previouspackenum "\e[37m"| tee -a  ../../LOG/JOBLOG/execute.log
		echo -e "\e[31m Missing packeges are " $$currentpackagenum - $previouspackenum "\e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
		echo -e "\e[31m Deploy missig packages ..Deployment will ABORT!!!!!!!!!! \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
		echo -e "\e[31m <ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR> \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
		echo -e "\e[31m MISSING PACKCAGES FOUND... \e[37m"| tee -a  ../../LOG/JOBLOG/execute.log
		MESSAGE="MISSING PACKCAGES FOUND..."
		echo -e "\e[31m SCRIPT IS EXITING PLEASE CONTACT :  ${DEPUSER} AT  ${DEPMAIL}\e[37m"  | tee -a  ../../LOG/JOBLOG/execute.log
		echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "FAIL" "|" $MESSAGE > ../../dblog
		echo -e "\e[31m <ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR><ERROR> \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log
		cd ../../
		java -jar postgreswriter.jar $SYSTEM $PROJECT	
		exit 4
		
		
	fi
fi




if [ ${#SYSTEM} -lt 2 ]
	then
	errorlog "SCRIPT IS RUNNING FROM UNKOWN ENVIROINMENT..SCRIPT WILL EXIT"
	exit 4
fi


if [ -f $CONFIG ] 
then

	if [ $SYSTEM == "DEV" ]
	then
		PROJECTDIR=$(grep 'DEVPROJDIR:' $CONFIG | cut  -c12-)
		REPLACECONFIG=$(grep 'DEVCONFIG:' $CONFIG | cut  -c11-)
	fi	
	
  if [ $SYSTEM == "PROD" ]
  then
		PROJECTDIR=$(grep 'PRODPROJDIR:' $CONFIG | cut  -c13-)
		REPLACECONFIG=$(grep 'PRODCONFIG:' $CONFIG | cut  -c12-)
   fi	
else
  
  echo "$CONFIG not found." | tee -a  ../../LOG/JOBLOG/execute.log
  echo ""
fi

echo -e "\e[32m Running replace for ${SYSTEM}.. \e[37m" | tee -a  ../../LOG/JOBLOG/execute.log

sh createhql.sh BASE FINAL $REPLACECONFIG
RC=$?

if [ $RC -ne 0 ]
then
	
	errorlog "REPLACE FAILED..FOR SYSTEM ${SYSTEM}"
	echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "SUCCESS" "|" "DEPLOYMENT SUCCESS" >> ../../LOG/REPORT/report.csv
	echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "SUCCESS" "|" "DEPLOYMENT SUCCESS" > ../../dblog
	exit 4
fi

echo "REPLACING FOR ${SYSTEM} COMPLETED " | tee -a  ../../LOG/JOBLOG/execute.log

echo "ADDING FILE  to HDFS STARTS...PROJECT DIR IS :$PROJECTDIR" | tee -a  ../../LOG/JOBLOG/execute.log

#source ~/.bashrc
#FILESI=$(find FINAL/ -type f )
find FINAL/ -type f  > filelist


while read line
do

	FILETOLOAD=$line
	DNAME=`dirname $line`
	echo "DIRNAME IS " $DNAME
	FILEDIRNAME=$(echo $DNAME | sed -e 's/FINAL\///g' | sed -e 's/SCRIPTS\///g')
	echo "FILEDIRNAME:" $FILEDIRNAME
	HDFSPATH=$PROJECTDIR"/"$FILEDIRNAME
	echo "HDFS PATH is :" $HDFSPATH
	
	#if [ $FILEDIRNAME != "WORKFLOWS" ]
	
	if [[ $FILEDIRNAME != "OOZIE" ]] && [[ $FILEDIRNAME != "SHELLSCRIPTS" ]] 
	then
	
		hadoop fs -test -d ${HDFSPATH}
		if [ $? != 0 ]
		then 
			echo  "CREATEING DIR $HDFSPATH in HDFS " | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir -p ${HDFSPATH}
		fi
		echo "!!!!!!!!!!!!!!!!COMAMD TO EXECUTE: hadoop fs -put -f ${FILETOLOAD} ${HDFSPATH}" | tee -a  ../../LOG/JOBLOG/execute.log
		sed -i -e 's/\r//g' ${FILETOLOAD}
		hadoop fs -put -f ${FILETOLOAD} ${HDFSPATH}
		RC=$?
		if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM}" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
		fi
	fi
done < filelist


#
# PUT OOZIE WORKFLOWS for Archival WF
#

if [ -d FINAL/SCRIPTS/OOZIE/ARCHIVE_LOAD/WFS ]
then
 hadoop fs -test -d $PROJECTDIR/OOZIE/ARCHIVE_LOAD/WFS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE WF" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/OOZIE/ARCHIVE_LOAD/WFS
 fi

hadoop fs -put -f FINAL/SCRIPTS/OOZIE/ARCHIVE_LOAD/WFS/* 	$PROJECTDIR/OOZIE/ARCHIVE_LOAD/WFS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi

fi

# PUT OOZIE WORKFLOWS for FULL LOAD WF


if [ -d FINAL/SCRIPTS/OOZIE/FULL_LOAD/WFS ]
then
 hadoop fs -test -d $PROJECTDIR/OOZIE/FULL_LOAD/WFS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE WF" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/OOZIE/FULL_LOAD/WFS
 fi

hadoop fs -put -f FINAL/SCRIPTS/OOZIE/FULL_LOAD/WFS/* 	$PROJECTDIR/OOZIE/FULL_LOAD/WFS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi

fi

# PUT OOZIE WORKFLOWS for DELTA LOAD WF


if [ -d FINAL/SCRIPTS/OOZIE/DELTA_LOAD/WFS ]
then
 hadoop fs -test -d $PROJECTDIR/OOZIE/DELTA_LOAD/WFS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE WF" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/OOZIE/DELTA_LOAD/WFS
 fi

hadoop fs -put -f FINAL/SCRIPTS/OOZIE/DELTA_LOAD/WFS/* 	$PROJECTDIR/OOZIE/DELTA_LOAD/WFS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi

fi

#
# PUT OOZIE ARCHIVAL COORD
#

if [ -d FINAL/SCRIPTS/OOZIE/ARCHIVE_LOAD/COORDS ]
then
hadoop fs -test -d $PROJECTDIR/OOZIE/ARCHIVE_LOAD/COORDS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE COORD" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/OOZIE/ARCHIVE_LOAD/COORDS
 fi

hadoop fs -put -f FINAL/SCRIPTS/OOZIE/ARCHIVE_LOAD/COORDS/* 	$PROJECTDIR/OOZIE/ARCHIVE_LOAD/COORDS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi
	
fi

# PUT OOZIE full COORD
#

if [ -d FINAL/SCRIPTS/OOZIE/FULL_LOAD/COORDS ]
then
hadoop fs -test -d $PROJECTDIR/OOZIE/FULL_LOAD/COORDS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE COORD" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/OOZIE/FULL_LOAD/COORDS
 fi

hadoop fs -put -f FINAL/SCRIPTS/OOZIE/FULL_LOAD/COORDS/* 	$PROJECTDIR/OOZIE/FULL_LOAD/COORDS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi
	
fi

# PUT OOZIE full COORD
#

if [ -d FINAL/SCRIPTS/OOZIE/DELTA_LOAD/COORDS ]
then
hadoop fs -test -d $PROJECTDIR/OOZIE/DELTA_LOAD/COORDS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE COORD" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/OOZIE/DELTA_LOAD/COORDS
 fi

hadoop fs -put -f FINAL/SCRIPTS/OOZIE/DELTA_LOAD/COORDS/* 	$PROJECTDIR/OOZIE/DELTA_LOAD/COORDS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi
	
fi


if [ -d FINAL/SCRIPTS/SHELLSCRIPTS ]
then
hadoop fs -test -d $PROJECTDIR/SHELLSCRIPTS
 if [ $? != 0 ]
	then 
			echo  "CREATEING DIR $HDFSPATH in HDFS FOR OOZIE COORD" | tee -a  ../../LOG/JOBLOG/execute.log
			hadoop fs -mkdir  $PROJECTDIR/SHELLSCRIPTS
 fi
sed -i -e 's/\r//g' FINAL/SCRIPTS/SHELLSCRIPTS/* 
hadoop fs -put -f FINAL/SCRIPTS/SHELLSCRIPTS/* 	$PROJECTDIR/SHELLSCRIPTS
	if [ $RC -ne 0 ]	
		then
			errorlog "HADOOP PUT FAILED FOR SYSTEM ${SYSTEM} FOR WORKFLOWS" | tee -a  ../../LOG/JOBLOG/execute.log
		exit 4
	fi
	
fi


echo "DEPLOYMENT COMPLETE EXCEPT FOR WORKFLOWS..PLEASE COPY THE WORKFLOWS FROM FINAL DIRECTORY" | tee -a  ../../LOG/JOBLOG/execute.log
echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "SUCCESS" "|" "DEPLOYMENT SUCCESS" >> ../../LOG/REPORT/report.csv
echo $DEPUSER "|" $CURRDATE "|" $SYSTEM "|" $PACKAGENAME "|" $SVN "|" "SUCCESS" "|" "DEPLOYMENT SUCCESS" > ../../dblog
cd ../../
java -jar postgreswriter.jar $SYSTEM $PROJECT