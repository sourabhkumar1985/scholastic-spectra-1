DROP TABLE IF EXISTS rlsf_prod.cost_trans_debit_delta;
CREATE EXTERNAL TABLE rlsf_prod.cost_trans_debit_delta (   costdistributionlineseocostelementid STRING,
   distributionlineid STRING,
   transactionseoinventoryitemid STRING,
   costdistributionseolayerquantity STRING,
   transactionseoinventoryorgid STRING,
   transactionseocreationdate STRING,
   transactionseocstinvtransactionid STRING,
   cstlayercostspeorectrxnid STRING,
   cstlayercostspeocosttransactiontype STRING,
   costdistributionlineseoenteredcurrencyamount STRING,
   cstlayercostspeounitcost STRING ) STORED AS PARQUET LOCATION '/sch/l1/cost_trans_debit_delta';
   
