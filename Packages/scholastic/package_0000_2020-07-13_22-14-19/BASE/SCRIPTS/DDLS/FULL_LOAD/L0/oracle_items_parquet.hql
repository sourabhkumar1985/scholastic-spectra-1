DROP TABLE

IF EXISTS rlsf_prod.oracle_items_parquet;
	CREATE EXTERNAL TABLE rlsf_prod.oracle_items_parquet (
		oracle_id String
		,title String
		,isbn13 String
		,inventory_item_flag String
		,item_type String
		,inventory_asset_flag String
		,costing_enabled_flag String
		,short_id String
		,mdm_id String
		,creation_dt String
		,inventory_item_id String
		,opus_num String
		,bf_num String
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l0/oracle_items_parquet';