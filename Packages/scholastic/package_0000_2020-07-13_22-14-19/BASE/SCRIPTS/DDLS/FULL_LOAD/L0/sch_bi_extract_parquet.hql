DROP TABLE IF EXISTS rlsf_prod.sch_bi_extract_parquet;
CREATE EXTERNAL TABLE rlsf_prod.sch_bi_extract_parquet (
	oracle_item STRING
	,org_id STRING
	,ohqty STRING
	) stored as parquet location 'hdfs://nameservice1/sch/l0/sch_bi_extract_parquet';