DROP TABLE

IF EXISTS rlsf_prod.inv_master_distinct_parquet;
	CREATE EXTERNAL TABLE rlsf_prod.inv_master_distinct_parquet (
		oracle_item_id STRING
		,inventory_item_id STRING
		,DESCRIPTION string
		,ITEM_TYPE string
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l0/inv_master_distinct_parquet';