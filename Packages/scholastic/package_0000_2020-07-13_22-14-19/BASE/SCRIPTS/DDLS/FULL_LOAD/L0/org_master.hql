DROP TABLE

IF EXISTS rlsf_prod.org_master;
	CREATE EXTERNAL TABLE rlsf_prod.org_master (
		oiorgid STRING
		,oidescr STRING
		,oicode STRING
		,oitype STRING
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/org_master';