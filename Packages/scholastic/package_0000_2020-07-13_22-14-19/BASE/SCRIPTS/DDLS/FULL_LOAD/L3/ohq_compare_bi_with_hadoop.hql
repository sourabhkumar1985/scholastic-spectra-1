DROP TABLE IF EXISTS rlsf_prod.ohq_compare_bi_with_hadoop;
CREATE EXTERNAL TABLE rlsf_prod.ohq_compare_bi_with_hadoop (   oracle_item_number STRING,
   inventory_organization STRING,
   bi_extract_qty DECIMAL(38,6),   hadoop_extract_qty DECIMAL(38,6),   hadoop_inventory_item_id STRING,
   hadoop_org_id STRING,
   diff_state STRING,
   diff_value DECIMAL(38,6),   diff_value_abs DECIMAL(38,6) ) 
   STORED AS PARQUET LOCATION '/sch/l3/ohq_compare_bi_with_hadoop' ;