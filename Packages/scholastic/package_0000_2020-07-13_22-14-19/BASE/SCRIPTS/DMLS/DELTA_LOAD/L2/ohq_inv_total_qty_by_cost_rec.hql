insert into table rlsf_prod.ohq_inv_total_qty_by_cost_rec
select 
sum(costdistributionseolayerquantity) as layer_qty_sum,
cost.transactionseoinventoryitemid as trans_eo_inventory_item_id,
cstlayercostspeorectrxnid as lc_p_eo_rec_trans_id,
transactionseocreationdate as date_,
transactionseoinventoryorgid as oracle_id
from rlsf_prod.ohq_inv_qty_by_cost_rec cost
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = cost.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
group by trans_eo_inventory_item_id,
date_,oracle_id,lc_p_eo_rec_trans_id;
