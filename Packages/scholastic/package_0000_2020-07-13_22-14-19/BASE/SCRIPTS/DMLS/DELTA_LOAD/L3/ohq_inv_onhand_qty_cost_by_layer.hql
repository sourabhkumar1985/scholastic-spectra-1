insert into table rlsf_prod.ohq_inv_onhand_qty_cost_by_layer
select 
ohq.date_ ,
ohq.org_id ,
items.oracle_id as oracle_item ,
items.isbn13 as isbn13 ,
items.short_id as border_item ,
items.opus_num as opus_id ,
ohq.lc_p_eo_rec_trans_id ,
ohq.layer_qty_sum ,
if (cost.cost_unit is null,0,cost.cost_unit ) as layer_cost_sum ,
if (cost.cost_unit is null,0,cast((layer_qty_sum * cost.cost_unit) as decimal(38,6))) as  total_amount ,
if(cost.date_ is null,'',cost.date_) as layer_date ,
ohq.trans_eo_inventory_item_id ,
org.oicode as cost_center,
config.run_date as run_date,
config.as_of_date as as_of_date 
from rlsf_prod.ohq_inv_oh_qty_by_cost_rec ohq 
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = ohq.trans_eo_inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
left join rlsf_prod.ohq_inv_total_unit_cst_by_cost_rec cost on ohq.lc_p_eo_rec_trans_id=cost.lc_p_eo_rec_trans_id 
and ohq.trans_eo_inventory_item_id=cost.trans_eo_inventory_item_id and ohq.org_id = cost.oracle_id 
left outer join rlsf_prod.inv_master_distinct_parquet  dist on dist.inventory_item_id = ohq.trans_eo_inventory_item_id
left outer join rlsf_prod.oracle_items_parquet items on items.oracle_id=dist.oracle_item_id
left outer join rlsf_prod.org_master_parquet org on trim(org.oiorgid) = trim(ohq.org_id)
cross join rlsf_prod.ohq_config_date config;
