INVALIDATE METADATA rlsf_prod.s3_org_master;

INSERT OVERWRITE TABLE rlsf_prod.org_master_parquet
select 
oiorgid 
,oidescr 
,oicode 
,oitype 
 from rlsf_prod.s3_org_master;