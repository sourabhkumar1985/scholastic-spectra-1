INSERT OVERWRITE TABLE rlsf_prod.s3_org_master partition(run_date)
select *, cast(now() as string) from rlsf_prod.org_master_parquet;