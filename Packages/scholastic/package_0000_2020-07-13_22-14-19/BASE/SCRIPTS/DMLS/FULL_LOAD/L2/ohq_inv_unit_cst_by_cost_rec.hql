insert into table rlsf_prod.ohq_inv_unit_cst_by_cost_rec
select distinct 
deb.transactionseoinventoryitemid,
deb.transactionseoinventoryorgid,
deb.cstlayercostspeorectrxnid,
cast((cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT as decimal(38,6)) / cast(deb.COSTDISTRIBUTIONSEOLAYERQUANTITY as decimal(38,6)))  as decimal(38,6)) as cost_unit, 
deb.transactionseocreationdate as transactionseocreationdate,
deb.costdistributionlineseocostelementid,
deb.distributionlineid
from  rlsf_prod.cost_trans_debit deb  -- change table name accordingly
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where (deb.cstlayercostspeocosttransactiontype = "ADJUST" or deb.cstlayercostspeocosttransactiontype= "RECEIPT")
and cast(deb.costdistributionlineseoenteredcurrencyamount as double) <> 0 
and cast((cast(deb.costdistributionseolayerquantity as double) * cast(cstlayercostspeounitcost as double)) as decimal(32,4)) =
cast(deb.costdistributionlineseoenteredcurrencyamount as decimal(32,4));
