insert into table rlsf_prod.ohq_inv_qty_by_cost_rec 
select distinct 
deb.transactionseoinventoryitemid,
cast(deb.costdistributionseolayerquantity as decimal(38,6)) as costdistributionseolayerquantity,
deb.transactionseoinventoryorgid,
deb.transactionseocreationdate as  transactionseocreationdate,
deb.transactionseocstinvtransactionid,
deb.cstlayercostspeorectrxnid
from  rlsf_prod.cost_trans_debit deb  
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where deb.costdistributionlineseocostelementid='300000073384740'   
and (deb.cstlayercostspeocosttransactiontype = "ISSUE" or deb.cstlayercostspeocosttransactiontype= "RECEIPT");
