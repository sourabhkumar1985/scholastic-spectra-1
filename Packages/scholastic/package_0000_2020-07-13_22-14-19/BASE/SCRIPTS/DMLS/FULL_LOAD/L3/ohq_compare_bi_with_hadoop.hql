with layer_cost as (
select distinct * from rlsf_prod.ohq_inv_as_of_date_with_layer_cost
)

insert overwrite table rlsf_prod.ohq_compare_bi_with_hadoop
select 
bi.oracle_item as oracle_item_number,
bi.org_id as inventory_organization,
CAST(cast(ohqty as double) AS DECIMAL(38,6)) as bi_extract_qty,
sum(total_qty) as hadoop_extract_qty,
hadoop.inventory_item_id as hadoop_inventory_item_id,
hadoop.org_id as hadoop_org_id,
case 
when sum(total_qty) is null then 'NA'
when CAST(cast(ohqty as double) AS DECIMAL(38,6))  - sum(total_qty) = 0 then 'Matched'
else 
'Not Matched' end as diff_state,
CAST((cast(cast(ohqty as double) AS DECIMAL(38,6)) - sum(total_qty)) as decimal(38,6)) as diff_value,
abs(CAST((cast(cast(ohqty as double) AS DECIMAL(38,6)) - sum(total_qty)) as decimal(38,6))) as diff_value_ABS
from rlsf_prod.sch_bi_extract_parquet bi
left join layer_cost hadoop on hadoop.oracle_item =bi.oracle_item and hadoop.cost_center = bi.org_id
group by 
oracle_item_number,Hadoop_inventory_item_id,hadoop_org_id,
inventory_organization,
bi_extract_qty;