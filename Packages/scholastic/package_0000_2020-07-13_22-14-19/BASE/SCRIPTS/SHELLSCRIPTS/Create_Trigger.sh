#!/bin/bash

export PYTHON_EGG_CACHE=./myeggs

if hdfs dfs -test -e /sch/trigger/wftrigger.txt ; 
then
	echo " trigger already present "
	if hdfs dfs -test -e /sch/trigger/tfile;
				then 
					hadoop fs -rmr /sch/trigger/tfile
					RC=$?
					if [ $RC -eq 0 ];
					then
						echo "java flag has been deleted" 
					else 
						echo " java flag has not been deleted"
					fi
	else
		echo " java trigger already deleted"
	fi 
	
else 
	hadoop fs -touchz /sch/trigger/wftrigger.txt
	RC=$?
		if [ $RC -eq 0 ];
		then
				echo "flag has been created" 
				if hdfs dfs -test -e /sch/trigger/tfile;
				then 
					hadoop fs -rmr /sch/trigger/tfile
					RC=$?
					if [ $RC -eq 0 ];
					then
						echo "java flag has been deleted" 
					else 
						echo " java flag has not been deleted"
					fi
				fi 	
		else 
				echo " java flag has not been created"
				exit 1
		fi 
fi 