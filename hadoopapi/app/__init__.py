"""
base init file
"""
# pylint:disable=wrong-import-position
from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
from app.views import hadoop_apis
from app.views import oracle_apis
from . import apis
