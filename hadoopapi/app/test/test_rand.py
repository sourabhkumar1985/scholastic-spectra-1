from random import seed
from random import randint
from datetime import datetime


seed(1)
value = randint(0, 100000)
dt = datetime.today().strftime('%Y%m%d%H%M%S')

print(value)
print(dt)

jsonfile  = f'/tmp/{value}{dt}_j.json'
outfile = f'/tmp/{value}{dt}_o.log'

print(jsonfile)
print(outfile)