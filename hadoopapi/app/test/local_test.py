import os
import json
from flask import request, make_response, jsonify, abort
import stat
with os.scandir('/home/python/basedir/multi/') as entries:
    output = []

    for entry in entries:
        fileStatsObj = os.stat('/home/python/basedir/multi/' + entry.name)
        last_modified_time = (fileStatsObj[stat.ST_MTIME])
        last_access_time = (fileStatsObj[stat.ST_ATIME])
        size = (fileStatsObj[stat.ST_SIZE])
        x = {'name': entry.name, 'size': size, 'last_modified_time':last_modified_time, 'last_access_time':last_access_time}
        output.append(x)
    print(output)
    print(jsonify(output))