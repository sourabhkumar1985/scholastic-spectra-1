package com.relevance.search.rest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.relevance.prism.rest.BaseResource;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SolrMaster;
import com.relevance.search.service.SolrMasterService;

/**
 * All Solrmaster services has defined here.
 *
 */
@Path("/solrmaster")
public class SolrMasterResource extends BaseResource {

	private final SolrMasterService solrMstSer = new SolrMasterService();

	/**
	 * to get the solrmaster json string.
	 * 
	 * @return the Response
	 * @throws AppException
	 * @throws IOException
	 */
	@GET
	@Path("/getsolrmasterlist")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getSolrMasterListValue() throws AppException, IOException {
		String fileName = "D://SolrMasterReport.csv";
		final String fieldSeparator = ",";
		final String rowSeparator = "\n";

		List<SolrMaster> solrList = solrMstSer.getSolrMasterList();

		FileWriter writer = new FileWriter(fileName);
		String header = "key,components,docnum,manufacturer,relateditems,source,title,userupdated,smallimagepath,imagepath,filepath,cspeciality, categoryl0, lables, rank, date_1, deleted, partnumber, finished_goods_number,component_number,notes,imageset,clicked,downloaded,emailed,revision";
		writer.append(header);
		writer.append(rowSeparator);
		for (SolrMaster solrMaster : solrList) {
			writer.append(solrMaster.key);
			writer.append(fieldSeparator);
			writer.append(solrMaster.components);
			writer.append(fieldSeparator);
			writer.append(solrMaster.docnum);
			writer.append(fieldSeparator);
			writer.append(solrMaster.manufacturer);
			writer.append(fieldSeparator);
			writer.append(solrMaster.relateditems);
			writer.append(fieldSeparator);
			writer.append(solrMaster.source);
			writer.append(fieldSeparator);
			writer.append(solrMaster.title);
			writer.append(fieldSeparator);
			writer.append(solrMaster.userupdated);
			writer.append(fieldSeparator);
			writer.append(solrMaster.smallimagepath);
			writer.append(fieldSeparator);
			writer.append(solrMaster.imagepath);
			writer.append(fieldSeparator);
			writer.append(solrMaster.filepath);
			writer.append(fieldSeparator);
			writer.append(solrMaster.cspeciality);
			writer.append(fieldSeparator);
			writer.append(solrMaster.categoryl0);
			writer.append(fieldSeparator);
			writer.append(solrMaster.lables);
			writer.append(fieldSeparator);
			writer.append(solrMaster.rank);
			writer.append(fieldSeparator);
			writer.append(solrMaster.date_1);
			writer.append(fieldSeparator);
			writer.append(solrMaster.imageset);
			writer.append(fieldSeparator);
			writer.append(solrMaster.partnumber);
			writer.append(fieldSeparator);
			writer.append(solrMaster.finished_goods_number);
			writer.append(fieldSeparator);
			writer.append(solrMaster.component_number);
			writer.append(fieldSeparator);
			writer.append(solrMaster.deleted);
			writer.append(fieldSeparator);
			writer.append(solrMaster.notes);
			writer.append(fieldSeparator);
			writer.append(solrMaster.revision);
			writer.append(fieldSeparator);
			writer.append(solrMaster.clicked);
			writer.append(fieldSeparator);
			writer.append(solrMaster.downloaded);
			writer.append(fieldSeparator);
			writer.append(solrMaster.emailed);
			writer.append(rowSeparator);
		}
		writer.flush();
		writer.close();
		File file = new File(fileName);
		ResponseBuilder response = Response.ok((Object) file);
		response.header("Content-Disposition", "attachment; filename=\"solr_file.csv\"");
		return response.build();

	}

	/**
	 * Updates Categories of solrmaster
	 * 
	 * @return the json on successful/unsuccessful updation of Categories
	 */
	@POST
	@Path("/updateCategories")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateSolrMasterCategories(@FormParam("rules") String rules,@FormParam("condition") String condition) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			solrMstSer.updateCategories(rules, condition);
			response = "{\"message\":\"Categories have been updated successfully. \"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	/**
	 * Updates CSpeciality of solrmaster
	 * 
	 * @return the json message on successful/unsuccessful updation of
	 *         CSpeciality
	 */
	@POST
	@Path("/updateCSpeciality")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateSolrMasterCSpeciality(@FormParam("rules") String rules,@FormParam("condition") String condition) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			//solrMstSer.updateCSpecialityCompany(rules);
			solrMstSer.updateCSpecialityTitle(rules, condition);
			response = "{\"message\":\"CSpecialities have been updated successfully. \"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Path("/applyCategoryRules")
	@Produces(MediaType.APPLICATION_JSON)
	public String applyCategoryRules(){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			solrMstSer.applyCategoryRules();
			response = "{\"message\":\"Rules have been applied successfully for Categories. \"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Path("/applyCspecialityRules")
	@Produces(MediaType.APPLICATION_JSON)
	public String applyCspecialityRules(){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			solrMstSer.applyCspecialityRules();
			response = "{\"message\":\"Rules have been applied successfully for Cspeciality. \"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Path("/clearDraftCategory")
	@Produces(MediaType.APPLICATION_JSON)
	public String clearDraftCategory(){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			solrMstSer.clearDraftCategory();
			response = "{\"message\":\"Drafts have been cleared successfully for Categories. \"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Path("/clearDraftCspeciality")
	@Produces(MediaType.APPLICATION_JSON)
	public String clearDraftCspeciality(){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			solrMstSer.clearDraftCspeciality();
			response = "{\"message\":\"Drafts have been cleared successfully for Cspeciality. \"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
