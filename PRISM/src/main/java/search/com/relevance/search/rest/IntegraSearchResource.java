package com.relevance.search.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.data.Shortlist;
import com.relevance.prism.data.UserSecondaryRole;
import com.relevance.prism.rest.BaseResource;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.BOM;
import com.relevance.search.data.IntegraAgilePart;
import com.relevance.search.data.QueryParam;
import com.relevance.search.service.InstrumentCatalogServiceLocator;
import com.relevance.search.service.InstrumentSearchService;
import com.relevance.search.service.IntegraSearchService;
import com.relevance.search.util.SearchCache;


@Path("/integrasearch")
@Consumes("application/javascript")
public class IntegraSearchResource extends BaseResource{
	
	@Context
	ServletContext context;

	public IntegraSearchResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/searchservice/")
	public String searchData(@FormParam("key") String key,
			@FormParam("facet") String facet,
			@FormParam("start") String start,
			@FormParam("rows") String rows,
			@FormParam("type") String type,
			@FormParam("searchInDisplaycolumns") String searchInDisplaycolumns,
			@FormParam("solrcollectionname") String solrcollectionname,@Context HttpServletRequest req) throws AppException{
		long startTime = System.currentTimeMillis();
		long endTime = 0L;
		String secondaryRolesString = null;
		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults = null;
		List<UserSecondaryRole> userSecondaryRolesList = new ArrayList<>();
		
		try {
			HttpSession session = req.getSession(true);
			//List<UserSecondaryRole> userSecondaryRolesList = userService.getDataSourcesForUser(E2emfConstants.qfindRole, username);
			HashMap<String, String> secondaryRolesMap = new HashMap<>();
			secondaryRolesMap.put("QFIND_PRODUCT_CATALOG", "product_catalog");
			secondaryRolesMap.put("QFIND_AGILE", "agile");
			secondaryRolesMap.put("QFIND_LABEL", "label");
			secondaryRolesMap.put("QFIND_ITEM_MASTER", "item_master");
			StringBuilder secondaryRoles = new StringBuilder("datasource:");
			for(UserSecondaryRole userSecondaryRole : userSecondaryRolesList){
				secondaryRoles.append(secondaryRolesMap.get(userSecondaryRole.getDataSource())).append(" OR datasource:");
			}
			
			IntegraSearchService integraSearchService = new IntegraSearchService();

			Log.Info("Search Param recieved is " + key);					

			if (facet != null && facet.equalsIgnoreCase("All") && (start == null || Integer.parseInt(start) == 0) && (type == null || !type.equalsIgnoreCase("detailed")) && (key != null && !key.trim().equalsIgnoreCase(""))) {

				SearchLog searchLog = new SearchLog();
				
				searchLog
				.setUserID(session.getAttribute("userName").toString());
		searchLog.setUserName(session.getAttribute("userDisplayName")
				.toString());
				searchLog.setDisplayQuery(key);
				searchLog.setJsonQuery("{key : \""+key+"\"}");
				searchLog.setType("History");
				searchLog.setModule("Q Find");
				E2emfAppUtil.logSearchQuery(searchLog);
			}
			
			SearchParam searchParam = new  SearchParam();
			searchParam.setKey(DataObfuscator.deObfuscateToken(key.replaceAll("\"", " ").replaceAll("'", " ").trim()));
			searchParam.setFacet(facet);
			searchParam.setStart(start);
			searchParam.setRows(rows);
			searchParam.setType(type);
			//searchParam.setSource(source);
			searchParam.setSolrCollectionName(solrcollectionname);
			//if(solrcollectionname.toLowerCase().contains(E2emfConstants.integraApp.toLowerCase())){
			if(!userSecondaryRolesList.isEmpty()){
				searchParam.setApp(E2emfConstants.integraApp);
			}else{
				searchParam.setApp(E2emfConstants.pakFindApp);
			}
			//if(solrcollectionname.toLowerCase().contains(E2emfConstants.integraApp.toLowerCase())){
			if(!userSecondaryRolesList.isEmpty()){
				secondaryRolesString = secondaryRoles.substring(0, secondaryRoles.length()-15);
			}
			searchParam.setSecondaryRoles(secondaryRolesString);
			String obfuscateFlag = E2emfAppUtil.getAppProperty(E2emfConstants.DATA_OBFUSCATION);
			searchResults = (String) integraSearchService.getJsonObject(searchParam,
					"searchService",searchInDisplaycolumns, obfuscateFlag);
			
			searchResults = DataObfuscator.obfuscate(searchResults);
			endTime = System.currentTimeMillis();
		} catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getAgileParts")
	public String getAgileParts(@FormParam("level") String level,
			@FormParam("partno") String partno,
			@FormParam("children") String children,
			@FormParam("description") String description,
			@FormParam("rowid") String rowId,@Context HttpServletRequest req) throws AppException{
		
		long startTime = System.currentTimeMillis();
		long endTime = 0L;		
		Log.Info("Recieved Request getAgileParts with param : " + description);

		String result = null;
		IntegraSearchService integraSearchService;
		try{
			integraSearchService = new IntegraSearchService();
			IntegraAgilePart part = new IntegraAgilePart();
			int lvl = 0;
			int row = 0;
			
			try{
				lvl = Integer.parseInt(level);				
			}catch(Exception ex) {
				//Intentionally do nothing
			}
			try{
				row = Integer.parseInt(rowId);		
			}catch(Exception ex) {
				//Intentionally do nothing
			}
			if(SearchCache.JsonResultsCache.containsKey("integraBOM") && SearchCache.JsonResultsCache.get("integraBOM") != null && !SearchCache.JsonResultsCache.get("integraBOM").isEmpty()) {
				result = SearchCache.JsonResultsCache.get("integraBOM");
			} else {
				part.setLevel(lvl);
				part.setRowId(row);
				part.setPartNumber(partno);
				part.setDescription(description);
				result = integraSearchService.getAgile(part);
				SearchCache.JsonResultsCache.put("integraBOM", result);
			}			
		} catch (Exception ex) {			
			result = PrismHandler.handleException(ex);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");		
		return result;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchSuggestions")
	public String getSearchSuggestions(@javax.ws.rs.QueryParam("type") String varX,@javax.ws.rs.QueryParam("query") String query,@javax.ws.rs.QueryParam("source") String source, @javax.ws.rs.QueryParam("solrcollectionname") String solrcollectionname) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Slob searchMasterDatanew Request with param " + varX	+ " searchParam " + query);

		String masterDataJson = null;

		try {
			SearchParam searchParam = new  SearchParam();
			searchParam.setKey(query);
			searchParam.setSolrCollectionName(solrcollectionname);
			IntegraSearchService integraSearchService = new IntegraSearchService();
			masterDataJson = integraSearchService.getSearchSuggestion(searchParam);

			endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception getSlobMasterData() of the MasterData with queryParams "
					+ e);
		}

		Log.Info("Returning Slob MasterData Json to presentation, total Time taken is "	+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime));

		return masterDataJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/bookmarksearch/")
	public String bookmarksearch(@FormParam("key") String key,
			@FormParam("displayQuery") String displayQuery,
			@FormParam("bookmarkname") String bookmarkname,
			@Context HttpServletRequest req) throws Exception {
		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults = null;

		Gson gson = new Gson();
		QueryParam queryParam = gson.fromJson(key, QueryParam.class);

		if (queryParam.getFacet() != null && queryParam.getStart() != null
				&& Integer.parseInt(queryParam.getStart()) == 0) {
			SearchLog searchLog = new SearchLog();
			HttpSession session = req.getSession(true);
			searchLog.setUserID(session.getAttribute("userName").toString());
			searchLog.setUserName(session.getAttribute("userDisplayName")
					.toString());
			searchLog.setJsonQuery(key);
			searchLog.setDisplayQuery(displayQuery);
			searchLog.setType("Bookmark");
			searchLog.setModule("Q-Find");
			searchLog.setBookmarkName(bookmarkname);
			searchResults = E2emfAppUtil.logSearchQuery(searchLog);
		}

		return searchResults;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getsearchhistory")
	public String getSearchHisory(@FormParam("type") String type,
			@Context HttpServletRequest req) {
		String json = null;
		SearchLog searchQuery = new SearchLog();
		List<SearchLog> searchHistory = new ArrayList<SearchLog>();
		try {
			HttpSession session = req.getSession(true);
			searchQuery.setUserID(session.getAttribute("userName").toString());
			searchQuery.setType(type);
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			searchHistory = service.getSearchHistory(searchQuery, null, null);
			Gson gson = new Gson();
			json = gson.toJson(searchHistory);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception ex) {
			Log.Error("Exception while retrieving the user list "
					+ ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getshortlist/")
	public String getshortlist(@FormParam("module") String module,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + module);

		String searchResults = null;
		String userName = null;
		String userId = null;

		try {
			Log.Info("Search Param recieved is " + module);
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			List<Shortlist> shortlistsArray = E2emfAppUtil.getShortlist(userId,
					userName, "Qfind");
			Gson gson = new Gson();
			searchResults = gson.toJson(shortlistsArray);
			endTime = System.currentTimeMillis();
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/shortlist/")
	public String shortlist(@FormParam("solrid") String id,
			@FormParam("shortlistall") String shortlistall,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + id);

		String searchResults = null;
		String userName = null;
		String userId = null;

		try {
			Log.Info("Search Param recieved is " + id);
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			E2emfAppUtil.shortlist(userId, userName, id, "Qfind",
					shortlistall);
			endTime = System.currentTimeMillis();
			List<Shortlist> shortlistsArray = E2emfAppUtil.getShortlist(userId,
					userName, "Qfind");
			Gson gson = new Gson();
			searchResults = gson.toJson(shortlistsArray);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBOM")
	public String getBOM(@FormParam("key") String key, @FormParam("table") String table, @FormParam("source") String source, @FormParam("dataBase") String dataBase) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		BOM bom = null;
		String response = null;
		try {
			IntegraSearchService integraSearchService = new IntegraSearchService();
			bom = integraSearchService.getBOM(key, table, source, dataBase);
			Gson gson = new Gson();
			response = gson.toJson(bom);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBOMIds")
	public String getBOMIds() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		List<String> infoList = new ArrayList<>();
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();
			infoList = integraSearchService.getBOMIds();
			Gson gson = new Gson();
			response = gson.toJson(infoList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	 

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateCatalogs")		
	  public String UpdateFileStructure(@FormParam("tableName") String tableName,@FormParam("dbName") String dbName,@FormParam("columnName") String columnName,@FormParam("primaryKeys") String primaryKeys,@FormParam("value") String value,@FormParam("primaryKeyColumnName") String primaryKeyColumnName,@FormParam("keyValueMap") String keyValueMap) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		boolean values = false;
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();
			
			if(tableName == null || dbName == null || columnName == null || primaryKeys == null || value == null || primaryKeyColumnName == null) {
				throw new Exception("All values are not present");
			}
		
			values = integraSearchService.UpdateFileStructure(tableName, dbName, columnName, primaryKeys, value, primaryKeyColumnName, keyValueMap);
			Gson gson = new Gson();
			response = "{\"response\":\""+values+"\"}";
			response = gson.toJson(response);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());		
		return response;		
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/genericUpdate")		
	  public String genericUpdate(@FormParam("query") String query, @FormParam("source") String source, @FormParam("dataBase") String dataBase, @FormParam("queries") String queries) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		boolean values = false;
		List<String> queryList = null;
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();
			Gson gson;
			if(queries != null && !"".equalsIgnoreCase(queries)) {
				queryList = new LinkedList<>(Arrays.asList(queries.split(",")));
			}
			values = integraSearchService.genericUpdate(query, source, dataBase, queryList);
			gson = new Gson();
			response = "{\"response\":\""+values+"\"}";
			response = gson.toJson(response);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());		
		return response;		
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getCategories")
	public String getCategories(@FormParam("source") String source,@FormParam("tableName") String tableName,@FormParam("dbName") String dbName,@FormParam("columnName") String columnName, @FormParam("regEx") String regEx, @FormParam("_union") boolean _union) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		List<String> values = null;
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();
			values = integraSearchService.getIntegraCategories(source,dbName, tableName, columnName, regEx, _union);
			Gson gson = new Gson();
			response = gson.toJson(values);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());		
		return response;	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBrands")
	public String getIntegraBrands(@FormParam("tableName") String tableName,@FormParam("dbName") String dbName) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, String> categories = null;
		String response = null;
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();			
			categories = integraSearchService.getIntegraBrands(dbName, tableName);
			Gson gson = new Gson();
			response = gson.toJson(categories);
		} catch(Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMasterData")
	public String getMasterData(@FormParam("key") String key,
			@FormParam("dbName") String dbName) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, List<String>> masterDatas = null;
		String response = null;
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();			
			masterDatas = integraSearchService.getMasterData(key, dbName);
			Gson gson = new Gson();
			response = gson.toJson(masterDatas);
		} catch(Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Path("/getFullImgFromURL")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("image/png")
	public Response getFullImage(@FormParam("path") String path) {
		URL url = null;
		byte[] imageData = null;
		try {
			url = new URL(path);
		} catch (MalformedURLException e1) {
			PrismHandler.handleException(e1);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		
		try {
		  is = url.openStream ();
		  byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
		  int n;

		  while ( (n = is.read(byteChunk)) > 0 ) {
		    baos.write(byteChunk, 0, n);
		  }
		  imageData = baos.toByteArray();
		}
		catch (IOException e) {
		  PrismHandler.handleException(e);
		  // Perform any other exception handling that's appropriate.
		}
		finally {
		  if (is != null) { 
			  try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		} }
		}
		return Response.ok(imageData).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMasterDataDetails")
	public String getMasterDataDetails(@FormParam("key") String key,
			@FormParam("dbName") String dbName) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, List<String>> masterDatas = null;
		String response = null;
		try{
			IntegraSearchService integraSearchService = new IntegraSearchService();			
			masterDatas = integraSearchService.getMasterDataDetails(key, dbName);
			Gson gson = new Gson();
			response = gson.toJson(masterDatas);
		} catch(Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
