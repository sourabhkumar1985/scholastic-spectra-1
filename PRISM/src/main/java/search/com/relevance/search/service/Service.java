package com.relevance.search.service;

import com.relevance.prism.util.AppException;

	/**
	 * 
	 * E2emf Service Interface 
	 *
	 */
	public interface Service {
		
		public String getJsonObject();
		
		public Object getJsonObject(Object... obj) throws AppException;	
		public String generateJsonString(Object... obj);
		
		public String getSearchSuggestion(Object... obj) throws AppException;
	}
