package com.relevance.search.data;

import java.util.List;

public class NGramAnalyserStored {

	private List<NGramEntry> nGramAnalyser;

	public List<NGramEntry> getnGramAnalyser() {
		return nGramAnalyser;
	}

	public void setnGramAnalyser(List<NGramEntry> nGramAnalyser) {
		this.nGramAnalyser = nGramAnalyser;
	}
	
	
}
