package com.relevance.search.data;

import java.util.ArrayList;

public class SmartFindDocumentList {
	ArrayList<SmartFindDocument> smartFindDocumentList;

	public ArrayList<SmartFindDocument> getSmartFindDocumentList() {
		return smartFindDocumentList;
	}

	public void setSmartFindDocumentList(
			ArrayList<SmartFindDocument> smartFindDocumentList) {
		this.smartFindDocumentList = smartFindDocumentList;
	}
}
