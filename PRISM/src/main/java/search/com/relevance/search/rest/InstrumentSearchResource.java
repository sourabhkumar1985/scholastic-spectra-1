package com.relevance.search.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.prism.data.ClinicalSpeciality;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.Shortlist;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.search.data.InstrumentCatalogView;
import com.relevance.search.data.QueryParam;
import com.relevance.search.data.SmartFindAuditDocument;
import com.relevance.search.data.SmartFindDocument;
import com.relevance.search.data.SmartFindDocumentList;
import com.relevance.search.data.SmartFindMasterView;
import com.relevance.search.service.InstrumentCatalogServiceLocator;
import com.relevance.search.service.InstrumentSearchService;

@Path("/instrumentcatalog")
public class InstrumentSearchResource {

	@Context
	ServletContext context;

	public InstrumentSearchResource(@Context ServletContext value) {
		this.context = value;
		// System.out.println("Conext:" + this.context);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/bookmarksearch/")
	public String bookmarksearch(@FormParam("key") String key,
			@FormParam("displayQuery") String displayQuery,
			@FormParam("bookmarkname") String bookmarkname,
			@Context HttpServletRequest req) throws Exception {
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults = null;

		Gson gson = new Gson();
		QueryParam queryParam = gson.fromJson(key, QueryParam.class);

		if (queryParam.getQueryItems() != null && queryParam.getStart() != null
				&& Integer.parseInt(queryParam.getStart()) == 0) {
			SearchLog searchLog = new SearchLog();
			HttpSession session = req.getSession(true);
			searchLog.setUserID(session.getAttribute("userName").toString());
			searchLog.setUserName(session.getAttribute("userDisplayName")
					.toString());
			searchLog.setJsonQuery(key);
			searchLog.setDisplayQuery(displayQuery);
			searchLog.setType("Bookmark");
			searchLog.setModule("Smart Find");
			searchLog.setBookmarkName(bookmarkname);
			searchResults = E2emfAppUtil.logSearchQuery(searchLog);
		}

		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/instrumentsearch/")
	public String searchData(@FormParam("key") String key,
			@FormParam("displayQuery") String displayQuery,
			@FormParam("facet") String facet, @FormParam("start") String start,
			@FormParam("rows") String rows, @FormParam("source") String source,
			@FormParam("type") String type,
			@FormParam("searchField") String searchField,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults = null;

		try {

			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");

			Log.Info("Search Param recieved is " + key);

			/*
			 * QueryParam queryParam = new QueryParam();
			 * queryParam.setFacet(facet); queryParam.setStart(start);
			 * queryParam.setRows(rows); queryParam.setSource(source);
			 * 
			 * ArrayList<QueryItem> queryItems = new ArrayList<QueryItem>();
			 * QueryItem queryItem = new QueryItem(); queryItem.setKey(key);
			 * queryItem.setType(type); queryItem.setField(searchField);
			 * queryItems.add(queryItem);
			 */

			// queryParam.setQueryItems(queryItems);

			Gson gson = new Gson();
			QueryParam queryParam = gson.fromJson(key, QueryParam.class);

			if (queryParam.getQueryItems() != null
					&& queryParam.getStart() != null
					&& Integer.parseInt(queryParam.getStart()) == 0
					&& displayQuery != "" && displayQuery != null
					&& displayQuery != "null") {

				SearchLog searchLog = new SearchLog();
				HttpSession session = req.getSession(true);
				searchLog
				.setUserID(session.getAttribute("userName").toString());
		searchLog.setUserName(session.getAttribute("userDisplayName")
				.toString());
				searchLog.setJsonQuery(key);
				searchLog.setDisplayQuery(displayQuery);
				searchLog.setType("History");
				searchLog.setModule("Smart Find");
				E2emfAppUtil.logSearchQuery(searchLog);
			}
			searchResults = (String) service.getJsonObject(queryParam,
					"searchService");
			searchResults = DataObfuscator.obfuscate(searchResults);
			endTime = System.currentTimeMillis();

			// Log.Info("Search Json fetched is \n " + searchResults);

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/catalogTagUpdate/")
	public String catalogTagUpdate(@FormParam("solrid") String id,
			@FormParam("title") String title,
			@FormParam("docnum") String docnum,
			@FormParam("manufacturer") String manufacturer,
			@FormParam("components") String components,
			@FormParam("categoryl0") String categoryl0,
			@FormParam("cspeciality") String cspeciality,
			@FormParam("tags") String facet, 
			@FormParam("helpful") int helpful,
			@FormParam("partnumber") String partnumber,
			@FormParam("componentnumber") String componentnumber,
			@FormParam("finishedgoodsnumber") String finishedgoodsnumber,
			@FormParam("notes") String notes,
			@FormParam("mlocation") String mlocation,
			@Context HttpServletRequest req)
			throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + id);

		String searchResults = null;

		try {
			HttpSession session = req.getSession();
			String userName = session.getAttribute("userName").toString();
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			URI contextUrl = URI.create(req.getRequestURL().toString()).resolve(req.getContextPath());
			System.out.println("Context URL" + contextUrl);
			// Log.Info("Search Param recieved is " + key);
			/* Gson gson = new Gson(); */
			if(contextUrl.toString().contains("sandbox") || contextUrl.toString().contains("192.168.101.42")){
				System.out.println("Sandbox.relevancelab.com");
				searchResults = (String) service.catalogTagUpdate(id, facet,
						helpful, title, docnum, manufacturer, components,
						categoryl0, cspeciality, null, partnumber, componentnumber, finishedgoodsnumber, notes, "operator", userName, mlocation);
			}else{
				System.out.println("DEV OR QA OR PROD");
				searchResults = (String) service.catalogTagUpdate(id, facet,
						helpful, title, docnum, manufacturer, components,
						categoryl0, cspeciality, null, partnumber, componentnumber, finishedgoodsnumber, notes, "user", userName, mlocation);
			}
			endTime = System.currentTimeMillis();

			// Log.Info("Search Json fetched is \n " + searchResults);

		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/shortlist/")
	public String shortlist(@FormParam("solrid") String id,
			@FormParam("shortlistall") String shortlistall,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + id);

		String searchResults = null;
		String userName = null;
		String userId = null;

		try {
			Log.Info("Search Param recieved is " + id);
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			E2emfAppUtil.shortlist(userId, userName, id, "SmartFind",
					shortlistall);
			endTime = System.currentTimeMillis();
			List<Shortlist> shortlistsArray = E2emfAppUtil.getShortlist(userId,
					userName, "SmartFind");
			Gson gson = new Gson();
			// String json = gson.toJson(queryResponse.getResponseHeader());

			searchResults = gson.toJson(shortlistsArray);
			// Log.Info("Search Json fetched is \n " + searchResults);

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getshortlist/")
	public String getshortlist(@FormParam("module") String module,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + module);

		String searchResults = null;
		String userName = null;
		String userId = null;

		try {
			Log.Info("Search Param recieved is " + module);
			HttpSession session = req.getSession(true);
			//System.out.println("Session Obj Instrument Search Resource : " + session.getAttributeNames());
			//Enumeration<String> enumr = session.getAttributeNames();
			//while(enumr.hasMoreElements()){
			//	System.out.println("Attribute Name :" + enumr.nextElement());
			//}
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			System.out.println("User Name From Instrument Search Resource : " + userId);
			System.out.println("User Display Name From Instrument Search Resource : " + userName);
			//System.out.println("Servlet Context : " + context);
			//UserView userView = (UserView) context.getAttribute("userview");
			//System.out.println("User Name From Context - Instrument Search Resource : " + userView.getUserName());
			//System.out.println("User Display Name From Context - Instrument Search Resource : " + userView.getUserDisplayName());
			List<Shortlist> shortlistsArray = E2emfAppUtil.getShortlist(userId,
					userName, "SmartFind");
			Gson gson = new Gson();
			// String json = gson.toJson(queryResponse.getResponseHeader());

			searchResults = gson.toJson(shortlistsArray);
			endTime = System.currentTimeMillis();

			// Log.Info("Search Json fetched is \n " + searchResults);

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/smartfindmasterview/")
	public String getSmartFindMasterViewDetails() {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request slob profile view to fetch Smart Find Master Details ");

		String json = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		try {
			InstrumentSearchService service = new InstrumentSearchService();
			ArrayList<SmartFindMasterView> viewList = service
					.getSmartFindMasterViewObject();

			json = gson.toJson(viewList);
			json = DataObfuscator.obfuscate(json);
			endTime = System.currentTimeMillis();

			if (json != null) {
				Log.Info("Smart Find Master View JSON created having size : "
						+ json.length());
			}

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in slob profile view for slob:"
					+ e);
		}
		// }
		Log.Info("Returning slob profile view,  total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		// Log.Info("Slob View JSON created : " + slobDetailedViewJson);
		return json;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/nGramAnalyser/")
	public String nGramAnalyser(@FormParam("gram") int gram,
			@FormParam("key") String key) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;
		String searchResults = null;
		Gson gson = new Gson();
		QueryParam queryParam = gson.fromJson(key, QueryParam.class);
		if(queryParam.getQueryItems().size() == 0){			
			Object nGramAnalyserText = context.getAttribute("nGramAnalyserText");
			if(nGramAnalyserText != null){
				searchResults = nGramAnalyserText.toString();
			}			
		}else{
			try {				
				InstrumentSearchService service = InstrumentCatalogServiceLocator.getServiceInstance("search");
				searchResults = (String) service.getNGramAnalsyer(gram, queryParam,false);
				endTime = System.currentTimeMillis();
			} catch (AppException appe) {
				Log.Error(appe);
				return appe.toString();
			} catch (Exception e) {
				Log.Error("Exception in searchService " + e);
			}
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/nGramAnalyserStored/")
	public String nGramAnalyserStoredResults(@FormParam("gram") int gram,
			@FormParam("source") String source,
			@FormParam("category") String category,
			@FormParam("subcategory") String subcategory,
			@FormParam("range") int range,
			@FormParam("isdictionarywords") boolean isdictionarywords
			) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;
		String searchResults = null;
		try {
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			searchResults = (String) service.getNGramAnalsyerStoredResults(gram, source,category,subcategory,range, isdictionarywords);
			endTime = System.currentTimeMillis();
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	
	@GET
	@Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	@Path("/ngramexcel/")
	public Response downloadNGramReportToExcel(
			@javax.ws.rs.QueryParam("gram") int gram,
			@javax.ws.rs.QueryParam("key") String key) throws IOException,
			AppException {

		int rowNum = 1;
		String fileName = "Smartfind-nGram.xls";
		StreamingOutput streamOutput = null;
		List<String> keysList = null;
		List<String> keysListHeader = null;
		String rootArrayElement = null;
		String report = null;
		JSONArray jsonArray = null;
		///UserService userService = new UserService();
		Gson gson = new Gson();

		try {
			/*long startTime = System.currentTimeMillis();
			long endTime = 0L;*/
			//String searchResults = null;
			QueryParam queryParam = gson.fromJson(key, QueryParam.class);
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			report = (String) service.getNGramAnalsyer(gram, queryParam,false);
			if (report != null) {
				if (report.startsWith("[")) {
					jsonArray = new JSONArray(report);
				} else {
					JSONObject reportJSONObj = new JSONObject(report);
					Iterator rootElement = reportJSONObj.keys();
					while (rootElement.hasNext()) {
						rootArrayElement = rootElement.next().toString();
					}
					jsonArray = reportJSONObj.getJSONArray(rootArrayElement);
				}

				JSONObject jsonObj1 = jsonArray.getJSONObject(0);

				keysListHeader = new ArrayList<String>();
				Iterator keys1 = jsonObj1.keys();
				while (keys1.hasNext()) {
					keysListHeader.add(keys1.next().toString());
				}

				final HSSFWorkbook hwb = new HSSFWorkbook();
				HSSFSheet sheet = hwb.createSheet("Report");

				HSSFRow row = sheet.createRow(rowNum);

				CellStyle style = hwb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);

				for (int colheader = 0; colheader < keysListHeader.size(); colheader++) {
					HSSFCell cell = row.createCell(colheader);
					cell.setCellStyle(style);
					cell.setCellValue(keysListHeader.get(colheader));
				}

				for (int i = 0; i < jsonArray.length(); i++) {
					rowNum++;
					keysList = new ArrayList<String>();
					JSONObject jsonObj = jsonArray.getJSONObject(i);
					Iterator keys = jsonObj.keys();
					while (keys.hasNext()) {
						keysList.add(jsonObj.getString(keys.next().toString()));
					}

					HSSFRow nextRow = sheet.createRow(rowNum);
					for (int record = 0; record < keysList.size(); record++) {
						nextRow.createCell(record).setCellValue(
								keysList.get(record));
					}
				}

				streamOutput = new StreamingOutput() {
					public void write(OutputStream output) throws IOException,
							WebApplicationException {
						try {
							hwb.write(output);
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};

			}// end of if
			else {
				Log.Error("name parameter  is null");
				return null;
			}

		} catch (Exception e) {
			Log.Error("Exception in downloadMaterialReportToExcel :"
					+ e);
		}

		ResponseBuilder response = Response.ok((Object) streamOutput);
		response.header("Content-Disposition", "attachment; filename="
				+ fileName);
		return response.build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchSuggestions")
	public String getSlobMasterData(
			@javax.ws.rs.QueryParam("type") String varX,
			@javax.ws.rs.QueryParam("query") String searchParam,
			@javax.ws.rs.QueryParam("source") String source) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Slob searchMasterDatanew Request with param " + varX
				+ " searchParam " + searchParam);

		String masterDataJson = null;

		try {

			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			masterDataJson = (String) service.getSearchSuggestion(varX,
					searchParam, source);

			this.context.setAttribute("searchresults", masterDataJson);
			endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception getSlobMasterData() of the MasterData with queryParams "
					+ e);
		}

		Log.Info("Returning Slob MasterData Json to presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime));

		this.context.removeAttribute("searchresults");

		return masterDataJson;
	}

	@GET
	@Produces({
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
			MediaType.APPLICATION_JSON })
	@Path("/materialreportexcel/")
	public Response downloadMaterialReportToExcel(
			@javax.ws.rs.QueryParam("query") String key) throws IOException,
			AppException {
		/*long startTime = System.currentTimeMillis();
		long endTime = 0L;*/

		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults = null;

		try {

			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");

			Log.Info("Search Param recieved is " + key);

			/*
			 * QueryParam queryParam = new QueryParam();
			 * queryParam.setFacet(facet); queryParam.setStart(start);
			 * queryParam.setRows(rows); queryParam.setSource(source);
			 * 
			 * ArrayList<QueryItem> queryItems = new ArrayList<QueryItem>();
			 * QueryItem queryItem = new QueryItem(); queryItem.setKey(key);
			 * queryItem.setType(type); queryItem.setField(searchField);
			 * queryItems.add(queryItem);
			 */

			// queryParam.setQueryItems(queryItems);

			Gson gson = new Gson();
			QueryParam queryParam = gson.fromJson(key, QueryParam.class);
			queryParam.setStart("0");
			queryParam.setRows("1000");

			searchResults = (String) service.getJsonObject(queryParam,
					"searchService");

			this.context.setAttribute("searchresults", searchResults);

			//endTime = System.currentTimeMillis();

			// Log.Info("Search Json fetched is \n " + searchResults);

		} catch (AppException appe) {
			Log.Error(appe);
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		int rowNum = 1;
		String fileName = "SmartFindSearchReport.xls";
		StreamingOutput streamOutput = null;
		String report = null;
		JSONObject jsonObj = null;
		JSONArray jsonArray = null;
		//UserService userService = new UserService();
		// Gson gson = new Gson();

		try {

			if (this.context.getAttribute("searchresults") != null) {
				report = this.context.getAttribute("searchresults").toString();
			}

			if (report != null) {
				if (report.startsWith("[")) {
					jsonArray = new JSONArray(report);
				} else {
					JSONObject reportJSONObj = new JSONObject(report);

					jsonObj = reportJSONObj.getJSONObject("search");
					jsonArray = jsonObj.getJSONArray("result");
				}

				List<String> keysListHeader = new ArrayList<String>();

				keysListHeader.add("Doc Number");
				keysListHeader.add("Title");
				keysListHeader.add("Material/Components");
				keysListHeader.add("Manufacturer");
				keysListHeader.add("Part Number");
				keysListHeader.add("Component Number");
				keysListHeader.add("Finished Goods Number");
				keysListHeader.add("Manufacturer Location");
				
				final HSSFWorkbook hwb = new HSSFWorkbook();
				HSSFSheet sheet = hwb.createSheet("search");

				HSSFRow row = sheet.createRow(rowNum);

				CellStyle style = hwb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);

				for (int colheader = 0; colheader < keysListHeader.size(); colheader++) {
					HSSFCell cell = row.createCell(colheader);
					cell.setCellStyle(style);
					cell.setCellValue(keysListHeader.get(colheader));
				}

				for (int i = 0; i < jsonArray.length(); i++) {
					List<String> keysList = new ArrayList<String>();
					rowNum++;
					jsonObj = jsonArray.getJSONObject(i);

					keysList.add(jsonObj.getString("docnum"));

					String strTitle = jsonObj.getString("title");
					String str1 = strTitle.replaceAll("<em>", "");
					String str2 = str1.replaceAll("</em>", "");

					keysList.add(str2);

					keysList.add(jsonObj.getString("components"));
					keysList.add(jsonObj.getString("manufacturer"));
					keysList.add(jsonObj.getString("partnum"));
					keysList.add(jsonObj.getString("compnum"));
					keysList.add(jsonObj.getString("fingood"));
					keysList.add(jsonObj.getString("mlocation"));
					
					HSSFRow nextRow = sheet.createRow(rowNum);
					for (int record = 0; record < keysList.size(); record++) {
						nextRow.createCell(record).setCellValue(
								keysList.get(record));
					}
				}

				streamOutput = new StreamingOutput() {
					public void write(OutputStream output) throws IOException,
							WebApplicationException {
						try {
							hwb.write(output);
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};

			}// end of if
			else {
				Log.Error("name parameter  is null");
				return null;
			}

		} catch (Exception e) {
			Log.Error("Exception in downloadMaterialReportToExcel :"
					+ e);
		}

		ResponseBuilder response = Response.ok((Object) streamOutput);
		response.header("Content-Disposition", "attachment; filename="
				+ fileName);
		return response.build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getsearchhistory")
	public String getSearchHisory(@FormParam("type") String type,
			@Context HttpServletRequest req) {
		String json = null;
		SearchLog searchQuery = new SearchLog();
		List<SearchLog> searchHistory = new ArrayList<SearchLog>();
		try {
			HttpSession session = req.getSession(true);
			searchQuery.setUserID(session.getAttribute("userName").toString());
			searchQuery.setType(type);
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			searchHistory = service.getSearchHistory(searchQuery, null, null);
			Gson gson = new Gson();
			json = gson.toJson(searchHistory);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception ex) {
			Log.Error("Exception while retrieving the user list "
					+ ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/instrumentsearchl2view/")
	public String getInstrumentSearchL2Details(
			@FormParam("system") String system,
			@FormParam("indexstatus") String indexstatus,
			@FormParam("category") String category,
			@FormParam("ftype") String ftype,
			@FormParam("isduplicate") String isduplicate,
			@FormParam("ocrstatus") String ocrstatus,
			@FormParam("patternmatchingstatus") String patternmatchingstatus,
			@FormParam("pngconverted") String pngconverted,
			@FormParam("spellcheckstatus") String spellcheckstatus,
			@FormParam("subcategory") String subcategory,
			@FormParam("threadprocessed") String threadprocessed,
			@FormParam("titlefound") String titlefound,
			@FormParam("componentsfound") String componentsfound,
			@FormParam("docnumfound") String docnumfound,
			@FormParam("manufacturerfound") String manufacturerfound) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request for Instrument Search L2 View to fetch Instrument Search L2 View Details.");

		String instrumentSearchL2ViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "instrumentsearchl2view";
		try {
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			ArrayList<SmartFindMasterView> smartFindL2DetailsList = service
					.getSmartFindL2Details(system, indexstatus, category,
							ftype, isduplicate, ocrstatus,
							patternmatchingstatus, pngconverted,
							spellcheckstatus, subcategory, threadprocessed,
							titlefound, componentsfound, docnumfound,
							manufacturerfound);
			InstrumentCatalogView instrumentCatalogView = new InstrumentCatalogView();
			instrumentCatalogView.setSmartFindL2View(smartFindL2DetailsList);
			instrumentSearchL2ViewJson = gson.toJson(instrumentCatalogView);
			// instrumentSearchL2ViewJson =
			// DataObfuscator.obfuscate(emeaPOProfileL2ViewJson);
			endTime = System.currentTimeMillis();

			if (instrumentSearchL2ViewJson != null) {
				Log.Info("Instrument Search L2 View JSON created having size : "
						+ instrumentSearchL2ViewJson.length());
				this.context.setAttribute(key, instrumentSearchL2ViewJson);
			}

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in Instrument Search L2 View."
					+ e);
		}
		Log.Info("Returning Instrument Search L2 View,  total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		// Log.Info("Instrument Search L2 View JSON created : " +
		// instrumentSearchL2ViewJson);
		return instrumentSearchL2ViewJson;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getreleateddocments/")
	public String getRelatedDocuments(@FormParam("title") String title)
			throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + title);

		String searchResults = null;
		try {

			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");

			Log.Info("Search Param recieved is " + title);

			searchResults = (String) service.getRelatedDocuments(title);
			searchResults = DataObfuscator.obfuscate(searchResults);
			endTime = System.currentTimeMillis();

			// Log.Info("Search Json fetched is \n " + searchResults);

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getclinicalspeciality/")
	public String getClinicalSpeliality() throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request getClinicalSpeliality");
		String searchResults = null;
		List<ClinicalSpeciality> clinicalSpecialityList = new ArrayList<ClinicalSpeciality>();
		try {
			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			clinicalSpecialityList = service.getClinicalSpeliality(E2emfConstants.ALL);
			Gson gson = new Gson();
			searchResults = gson.toJson(clinicalSpecialityList);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);			
		}
		Log.Info("Returning clinicalspeliality Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addclinicalspeciality")
	public String addClinicalSpeciality(@FormParam("company") String company, 
						@FormParam("clinicalSpeciality") String clinic 
						){
		String responseMessage = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			responseMessage = instrumentSearchService.insertClinicalSpeciality(company, clinic);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the Category to the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateclinicalspeciality")
	public String updateClinicalSpeciality(@FormParam("company") String company,
						@FormParam("clinicalSpeciality") String clinic, 
						@FormParam("id") String id
						){
		String responseMessage = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			responseMessage = instrumentSearchService.updateClinicalSpeciality(company, clinic, id);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while Updating the Category in the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteclinicalspeciality")
	public String deleteClinicalSpeciality(@FormParam("company") String company
						){
		String responseMessage = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			responseMessage = instrumentSearchService.deleteClinicalSpeciality(company);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while deleting the Category from the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/refreshNgram")
	public String refreshNgram() throws AppException {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Refreshing nGram ");

		String masterDataJson = null;

		try {

			InstrumentSearchService service = InstrumentCatalogServiceLocator
					.getServiceInstance("search");
			masterDataJson = (String) service.refreshNgram();

			this.context.setAttribute("searchresults", masterDataJson);
			endTime = System.currentTimeMillis();

		} catch (Exception e) {
			Log.Error("Exception getSlobMasterData() of the MasterData with queryParams "
					+ e);
		}

		Log.Info("Returning Slob MasterData Json to presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime));

		this.context.removeAttribute("searchresults");

		return masterDataJson;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/dobulkupdate")
	public String doBulkUpdate(@FormParam("request") String  request,  
			@Context HttpServletRequest req){
		System.out.println("request from UI : "  +  request);
		String responseMessage = null;
		SmartFindDocumentList smartFindDocumentList   = new SmartFindDocumentList();
		Gson gson = new Gson();
		smartFindDocumentList = gson.fromJson(request, SmartFindDocumentList.class);
		String testString = gson.toJson(smartFindDocumentList);
		System.out.println("SmartFindDocumentList" + testString);
		HttpSession session = req.getSession(true);
		String userName = session.getAttribute("userName").toString();
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			responseMessage = instrumentSearchService.doBulkUpdate(smartFindDocumentList, userName);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while adding the Category to the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}

	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updatedeletestatus")
	public String updateDeleteStatusInPostGresTable(@FormParam("id") String id, @FormParam("status") String status 
						){
		String responseMessage = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			//responseMessage = instrumentSearchService.updateDeleteStatusInPostGresTable(id);
			responseMessage = (String) instrumentSearchService.catalogTagUpdate(id, null, 0, null, null, null, null, null, null, status, null, null, null, null, null,"ADMIN", null);
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while updating delete status in the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/incrementactioncount")
	public String incrementActionCount(@FormParam("id") String solrid, @FormParam("action") String action){
		String responseMessage = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			responseMessage = (String) instrumentSearchService.incrementActionCount(solrid, action);;
			return responseMessage;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while Incrementing Count status in the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getsmartfindauditinformation")
	public String getSmartFindAuditInformation(@FormParam("id") String solrid) throws AppException, SQLException{
		ArrayList<SmartFindAuditDocument> smartFindAuditInformationList = null;
		String json = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			smartFindAuditInformationList = (ArrayList<SmartFindAuditDocument>) instrumentSearchService.getSmartFindAuditInformation(solrid);
			Gson gson = new Gson();
			json = gson.toJson(smartFindAuditInformationList);
			return json;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving Audit Information from the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}			
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getsmartfindbackupinformation")
	public String getSmartFindBackupInformation(@FormParam("id") String solrid) throws AppException, SQLException{
		ArrayList<SmartFindDocument> smartFindBackupInformationList = null;
		String json = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			smartFindBackupInformationList = (ArrayList<SmartFindDocument>) instrumentSearchService.getSmartFindBackupInformation(solrid);
			Gson gson = new Gson();
			json = gson.toJson(smartFindBackupInformationList);
			return json;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving Backup Information from the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getimagesetlist")
	public String getImageSetList() throws AppException{
		ArrayList<SmartFindDocument> smartFindImageSetList = null;
		String json = null;
		try{
			InstrumentSearchService instrumentSearchService = new InstrumentSearchService();
			smartFindImageSetList = (ArrayList<SmartFindDocument>) instrumentSearchService.getImageSetList();
			Gson gson = new Gson();
			json = gson.toJson(smartFindImageSetList);
			return json;
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}
		catch(Exception ex){
			Log.Error("Exception while retrieving ImageSet List from the DB." + ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}	
	}
}
