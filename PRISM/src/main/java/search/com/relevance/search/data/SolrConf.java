package com.relevance.search.data;

import java.util.ArrayList;

public class SolrConf 
{
	private ArrayList<SolrEntityConf> confList;
	//private String srviceName;

/*	public String getSrviceName() {
		return srviceName;
	}

	public void setSrviceName(String srviceName) {
		this.srviceName = srviceName;
	}*/

	public ArrayList<SolrEntityConf> getConfList() {
		return confList;
	}

	public void setConfList(ArrayList<SolrEntityConf> confList) {
		this.confList = confList;
	}
	
	public SolrEntityConf getSolrEntityConf()
	{
		if (confList != null)
		{
			
			return confList.get(0);
		
		}
		
		return null;
	}
	
}
