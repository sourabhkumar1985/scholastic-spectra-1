package com.relevance.search.data;


public class FacetCountAndResults {
	java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>> facetMapList;
	Search search;
	public java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>> getFacetMapList() {
		return facetMapList;
	}
	public void setFacetMapList(
			java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>> facetMapList) {
		this.facetMapList = facetMapList;
	}
	public Search getSearch() {
		return search;
	}
	public void setSearch(Search search) {
		this.search = search;
	}
}
