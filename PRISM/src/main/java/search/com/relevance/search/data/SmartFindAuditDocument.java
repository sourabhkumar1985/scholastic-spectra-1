package com.relevance.search.data;

public class SmartFindAuditDocument {

	private String id;
	private String solrid;
	private String fromId;
	private String toId;
	private String userWhoUpdated;
	private String dateWithTimeStamp;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSolrid() {
		return solrid;
	}
	public void setSolrid(String solrid) {
		this.solrid = solrid;
	}
	public String getFromId() {
		return fromId;
	}
	public void setFromId(String fromId) {
		this.fromId = fromId;
	}
	public String getToId() {
		return toId;
	}
	public void setToId(String toId) {
		this.toId = toId;
	}
	public String getUserWhoUpdated() {
		return userWhoUpdated;
	}
	public void setUserWhoUpdated(String userWhoUpdated) {
		this.userWhoUpdated = userWhoUpdated;
	}
	public String getDateWithTimeStamp() {
		return dateWithTimeStamp;
	}
	public void setDateWithTimeStamp(String dateWithTimeStamp) {
		this.dateWithTimeStamp = dateWithTimeStamp;
	}	
}
