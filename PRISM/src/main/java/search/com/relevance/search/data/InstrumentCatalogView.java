package com.relevance.search.data;

import java.util.ArrayList;

public class InstrumentCatalogView {

	private ArrayList<SmartFindMasterView> smartFindL2View;

	public ArrayList<SmartFindMasterView> getSmartFindL2View() {
		return smartFindL2View;
	}

	public void setSmartFindL2View(ArrayList<SmartFindMasterView> smartFindL2View) {
		this.smartFindL2View = smartFindL2View;
	}
	
	
}
