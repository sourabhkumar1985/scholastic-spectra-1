package com.relevance.search.service;

import java.util.List;

import com.relevance.prism.dao.AppPropertiesDao;
import com.relevance.prism.data.Category;
import com.relevance.prism.data.ClinicalSpeciality;
import com.relevance.prism.service.BaseService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.dao.InstrumentSearchDao;
import com.relevance.search.dao.SolrMasterDao;
import com.relevance.search.data.SolrMaster;

/**
 * Holds the services for the solrmaster
 *
 */
public class SolrMasterService extends BaseService {

	private final SolrMasterDao solrMasterDao = new SolrMasterDao();
	private final InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();

	public List<SolrMaster> getSolrMasterList() throws AppException {
		List<SolrMaster> solrMasterList = null;
		try {
			Log.Info("getSolrMasterList() method called on SolrMasterService");
			solrMasterList = solrMasterDao.getSolrMasterList();
			return solrMasterList;
		} catch (Exception e) {
			Log.Error("Exception while fetching SolrMaster List " + e.getMessage());
			Log.Error(e);
		}
		return solrMasterList;

	}

	/**
	 * updates categol0 for the solrmaster
	 * 
	 * @throws Exception
	 */
	public void updateCategories(String rules, String condition) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			final AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			final List<Category> categories = appPropertiesDao.getCategoriesList(rules);
			solrMasterDao.updateSolrMasterCategories(categories, condition);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	/**
	 * updates the cspecility of solrmaster depending on the company.
	 * 
	 * @throws Exception
	 */
	public void updateCSpecialityCompany(String rules) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			//solrMasterDao.cleanCSPeciality();
			final List<ClinicalSpeciality> cspecialities = instrumentSearchDao
					.getClinicalSpeciality(rules);
			if (cspecialities != null && !cspecialities.isEmpty()) {
				solrMasterDao.updateSolrMasterCSpeciality(cspecialities, null);
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	/**
	 * update the cspeciality of solrmaster depending on the title.
	 * 
	 * @throws Exception
	 */
	public void updateCSpecialityTitle(String rules, String condition) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			final List<ClinicalSpeciality> cspecialities = solrMasterDao
					.getClinicalSpecialityTitles(rules);
			if (cspecialities != null && !cspecialities.isEmpty()) {
				solrMasterDao.updateSolrMasterCSpeciality(cspecialities, condition);
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	public void applyCategoryRules() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
				solrMasterDao.applyCategoryRules();
			}catch (Exception e) {
				PrismHandler.handleException(e, true);
			}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	public void applyCspecialityRules() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
				solrMasterDao.applyCspecialityRules();
			}catch (Exception e) {
				PrismHandler.handleException(e, true);
			}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	public void clearDraftCategory() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
				solrMasterDao.clearDraftCategory();
			}catch (Exception e) {
				PrismHandler.handleException(e, true);
			}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	public void clearDraftCspeciality() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
				solrMasterDao.clearDraftCspeciality();
			}catch (Exception e) {
				PrismHandler.handleException(e, true);
			}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
}