package com.relevance.search.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.data.Category;
import com.relevance.prism.data.ClinicalSpeciality;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SolrMaster;

/**
 * DAO layer for the solrmaster
 *
 */
public class SolrMasterDao extends BaseDao {

	public List<SolrMaster> getSolrMasterList() throws SQLException, AppException {
		List<SolrMaster> solrList = new ArrayList<>();
		Connection connection = null;
		SolrMaster solrToStore = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			String query = "select * from solrmaster";
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				solrToStore = new SolrMaster();
				solrToStore.setKey(rs.getString(1));
				solrToStore.setComponents(rs.getString(2));
				solrToStore.setDocnum(rs.getString(3));
				solrToStore.setManufacturer(rs.getString(4));
				solrToStore.setRelateditems(rs.getString(5));
				solrToStore.setSource(rs.getString(6));
				solrToStore.setTitle(rs.getString(7));
				solrToStore.setUserupdated(rs.getString(8));
				solrToStore.setSmallimagepath(rs.getString(9));
				solrToStore.setImagepath(rs.getString(10));
				solrToStore.setFilepath(rs.getString(11));
				solrToStore.setCspeciality(rs.getString(12));
				solrToStore.setCategoryl0(rs.getString(13));
				solrToStore.setLables(rs.getString(14));
				solrToStore.setRank(rs.getString(15));
				solrToStore.setDate_1(rs.getString(16));
				solrToStore.setImageset(rs.getString(17));
				solrToStore.setPartnumber(rs.getString(18));
				solrToStore.setFinished_goods_number(rs.getString(19));
				solrToStore.setComponent_number(rs.getString(20));
				solrToStore.setDeleted(rs.getString(21));
				solrToStore.setNotes(rs.getString(22));
				solrToStore.setRevision(rs.getString(23));
				solrToStore.setClicked(rs.getString(24));
				solrToStore.setDownloaded(rs.getString(25));
				solrToStore.setEmailed(rs.getString(26));

				solrList.add(solrToStore);
			}

		} catch (SQLException e) {
			Log.Error(e);
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}
		return solrList;
	}

	public Connection getPostGresDBConnection() throws AppException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		return dbUtil.getPostGresConnection();
	}

	/**
	 * updates solrmaster by updateQuery
	 * 
	 * @param categories
	 * @throws Exception
	 */
	public void updateSolrMasterCategories(final List<Category> categories, String condition) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		try 
		{
			Log.Info("Updating Categories in Solrmaster");
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			//final String query = "update solrmaster set draftcategory = '';";
			
			//stmt.executeUpdate(query);
			//stmt.addBatch(query);
			//stmt.executeBatch();
			if (categories != null && !categories.isEmpty()) 
			{
				for (final Category category : categories) 
				{
					if (category.getIsEnabled())
					{
						final String updateQuery = getCategoryUpdateQuery(category, condition);
						if (updateQuery != null && !updateQuery.isEmpty()) 
						{
							stmt.executeUpdate(updateQuery);
							//stmt.addBatch(updateQuery);
							//stmt.executeBatch();
						}
					}
				}
			}
			//updateManualCategories(con);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	public void applyCategoryRules() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		try {
			Log.Info("applying Rules for Categories in Solrmaster");
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			//final String categoryUpdateQuery = "update solrmaster set cat_approved = cat_approved || cat_draft || ";
			
			StringBuilder categoryUpdateQuery = new StringBuilder();
			categoryUpdateQuery.append("update solrmaster set ");
			categoryUpdateQuery.append("cat_approved =  COALESCE(NULLIF(cat_approved,''),'') || cat_draft");
			//categoryUpdateQuery.append(" || ';'");
			categoryUpdateQuery.append(",");
			categoryUpdateQuery.append("cat_approved_rule = COALESCE(NULLIF(cat_approved_rule,''),'') || cat_draft_rule,index = 'true'");
			//categoryUpdateQuery.append(" || ';'");
			categoryUpdateQuery.append(" where ");
			categoryUpdateQuery.append(" cat_draft <> '' and cat_draft is not NULL");
				
			stmt.executeUpdate(categoryUpdateQuery.toString());
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	public void applyCspecialityRules() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		try {
			Log.Info("applying Rules for Clinical Specialities in Solrmaster");
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			//final String cspecialityUpdateQuery = "update solrmaster set  cs_approved = cs_draft";
			
			StringBuilder cspecialityUpdateQuery = new StringBuilder();
			cspecialityUpdateQuery.append("update solrmaster set ");
			cspecialityUpdateQuery.append("cs_approved = cs_draft");
			cspecialityUpdateQuery.append(",");
			cspecialityUpdateQuery.append("cs_approved_rule = cs_draft_rule,index = 'true'");
			cspecialityUpdateQuery.append(" where ");
			cspecialityUpdateQuery.append(" cs_draft <> '' and cs_draft is not NULL");
			
			stmt.executeUpdate(cspecialityUpdateQuery.toString());
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void clearDraftCategory() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		try {
			Log.Info("Clearing Draft Categories in Solrmaster");
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			//final String categoryUpdateQuery = "update solrmaster set cat_draft = ''";
			StringBuilder categoryUpdateQuery = new StringBuilder();
			categoryUpdateQuery.append("update solrmaster set ");
			categoryUpdateQuery.append("cat_draft = NULL, cat_draft_rule = NULL, index = 'false'");
			categoryUpdateQuery.append(" where ");
			categoryUpdateQuery.append(" cat_draft <> '' AND cat_draft is not NULL");
			stmt.executeUpdate(categoryUpdateQuery.toString());
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	public void clearDraftCspeciality() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		try {
			Log.Info("Clearing Draft Cspecialities in Solrmaster");
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			//final String cspecialityUpdateQuery = "update solrmaster set cs_draft = ''";
			StringBuilder cspecialityUpdateQuery = new StringBuilder();
			cspecialityUpdateQuery.append("update solrmaster set ");
			cspecialityUpdateQuery.append("cs_draft = NULL, cs_draft_rule = NULL, index = 'false'");
			cspecialityUpdateQuery.append(" where ");
			cspecialityUpdateQuery.append(" cs_draft <> '' and cs_draft is not NULL");
			stmt.executeUpdate(cspecialityUpdateQuery.toString());
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	private String getCategoryUpdateQuery(final Category category, String condition) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder updateQuery = new StringBuilder();
		try {
			if (category.getIsEnabled()) {
				final String pCondition = category.getCondition();
				final Boolean pUserOverride = category.getIsOverride();
				if (pCondition != null && !pCondition.isEmpty()) {
					updateQuery.append(getQueryString(category.getCategory().trim(), category.getRule()));
					if (pUserOverride) 
					{
						updateQuery.append(pCondition);
						if(condition != null && !condition.equalsIgnoreCase("")) {
							updateQuery.append(" AND ");
							updateQuery.append(condition);
						}
						//updateQuery.append(";");
					} else 
					{
						updateQuery.append("userupdated <> 'user' and (");
						updateQuery.append("(" + pCondition + ")");
						if(condition != null && !condition.equalsIgnoreCase("")) {
							updateQuery.append(" AND ");
							updateQuery.append(condition);
						}
					}
				}

			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return updateQuery.toString();
	}

	private String getQueryString(String category, String ruleName) {
		StringBuilder query = new StringBuilder();
		query.append("update solrmaster set ");
		query.append("cat_draft = COALESCE(NULLIF(cat_draft,''),'') || '");
		query.append(category);
		query.append("' || ';'");
		query.append(",");
		query.append("cat_draft_rule = COALESCE( NULLIF(cat_draft_rule,''),'') || '");
		query.append(ruleName);
		query.append("' || ';'");
		query.append(" where ");
		
		return query.toString();
	}

	public List<ClinicalSpeciality> getClinicalSpecialityTitles(String rules) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		
		if(rules != null && rules.equalsIgnoreCase(E2emfConstants.ALL)){
			query = new StringBuilder("select * from sf_cs_title order by priority desc;");
		}
		else if(rules != null && rules.length() > 0){
			query = new StringBuilder("select * from sf_cs_title").append(" where rule in ('").append(rules).append("') order by priority desc");
		}
		
		List<ClinicalSpeciality> clinicalSpecialityList = new ArrayList<>();
		ClinicalSpeciality clinicalSpeciality = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(query.toString());

			if (rs != null) {
				while (rs.next()) {
					clinicalSpeciality = new ClinicalSpeciality();
					clinicalSpeciality.setCompany(rs.getString("company"));
					clinicalSpeciality.setClinicalSpeciality(rs.getString("cspeciality"));
					clinicalSpeciality.setId(rs.getString("id"));
					clinicalSpeciality.setCondition(rs.getString("condition"));
					clinicalSpeciality.setIsEnabled(rs.getBoolean("enabled"));
					clinicalSpeciality.setIsOverride(rs.getBoolean("user_override"));
					clinicalSpeciality.setPriority(rs.getString("priority"));
					clinicalSpeciality.setRule(rs.getString("rule"));
					clinicalSpeciality.setTitle(rs.getString("title"));
					clinicalSpecialityList.add(clinicalSpeciality);
				}
			}

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return clinicalSpecialityList;
	}
	
	
	public void updateSolrMasterCSpeciality(final List<ClinicalSpeciality> cspecialities, String condition)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		try {
			con = getPostGresDBConnection();
			stmt = con.createStatement();
			 
			//final String query =  "update solrmaster set draftcs = '';";
			//stmt.executeUpdate(query);
			 
			for (final ClinicalSpeciality cspeciality : cspecialities) 
			{
				if (cspeciality.getIsEnabled() && cspeciality.getClinicalSpeciality() != null && !("Others")
						.equalsIgnoreCase(cspeciality.getClinicalSpeciality().trim())) 
				{
					final String updateQuery = getCSpecialityUpdateQuery(cspeciality, condition);
					if (updateQuery != null && !updateQuery.isEmpty()) 
					{
						stmt.executeUpdate(updateQuery);
						//stmt.addBatch(updateQuery);
						//stmt.executeBatch();
					}
				}
			}
			
			//final String updateQuery = "update solrmaster set cspeciality = 'Others' where cspeciality = ''";
			//stmt.addBatch(updateQuery);
			//stmt.executeBatch();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());

	}

	private String getCSpecialityUpdateQuery(final ClinicalSpeciality cspeciality, String condition)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder updateQuery = new StringBuilder();
		try {
			if (cspeciality.getIsEnabled()) {
				updateQuery.append(" update solrmaster set cs_draft = ");
				updateQuery.append("'");
				updateQuery.append(cspeciality.getClinicalSpeciality());
				updateQuery.append("'");
				updateQuery.append(", ");
				updateQuery.append(" cs_draft_rule = ");
				updateQuery.append("'");
				updateQuery.append(cspeciality.getRule());
				updateQuery.append("'");
				updateQuery.append(" where ");
				final String pCondition = cspeciality.getCondition();				
				updateQuery.append("(" + pCondition);
				updateQuery.append(")");
				if(condition != null && !condition.equalsIgnoreCase("")) {
					updateQuery.append(" AND ");
					updateQuery.append(condition);
				}
				
				if (!cspeciality.getIsOverride()) {
					updateQuery.append(" and userupdated <> 'user'");
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return updateQuery.toString();
	}
	
	/*
	private void updateManualCategories(final Connection con) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement stmt = null;
		try {
			stmt = con.createStatement();
			final String customCategory1 = "Customs/MTOs";
			final String customCategory2 = "Registration";
			final String anspach = "ANSPACH";
			final String doclocator = "DOCLOCATOR";
			final String updateQuery1 = getQueryString(customCategory1)
					.concat(" docnum like '698%'");
			final String updateQuery2 = getQueryString(customCategory2)
					.concat(" docnum like '%reg%'");
			final String updateQuery3 = getQueryString(anspach)
					.concat(" (lower(source) = '" + doclocator.toLowerCase()
							+ "' and (title is null or title = '')) or (lower(manufacturer) = '"
							+ anspach.toLowerCase() + "')");
			final String updateQuery4 = "update solrmaster set categoryl0 = 'Title Not Found' where title is null or title=''";
			final String updateQuery5 = "update solrmaster set categoryl0 = 'Unclassified Instruments' where (title is not null or title!='') and categoryl0 = ''";

			stmt.executeUpdate(updateQuery1);
			stmt.executeUpdate(updateQuery2);
			stmt.executeUpdate(updateQuery3);
			stmt.executeUpdate(updateQuery4);
			stmt.executeUpdate(updateQuery5);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(null, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());

	}
	*/
}
