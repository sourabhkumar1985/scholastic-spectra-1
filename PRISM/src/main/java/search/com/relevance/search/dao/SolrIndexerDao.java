package com.relevance.search.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.common.SolrInputDocument;

import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.dao.DataAnalyserDao;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SolrColumnConf;
import com.relevance.search.data.SolrConf;
import com.relevance.search.data.SolrEntityConf;

public class SolrIndexerDao extends BaseDao {

	public String indexData(SolrConf solrConf, String filterColumnName, String filterColumnType, String filterColumnValue) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ConcurrentUpdateSolrServer server = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		SolrEntityConf solrEntityConf;
		String tableName;
		ArrayList<SolrColumnConf> solrColumnConfList;
		int count = 0;
		DataAnalyserDao dataanalyser = new DataAnalyserDao();
		ResultSetMetaData  rsmd = null;
		String endQuery = "";		
		boolean isFilterPresent = false;
		try {
			if((filterColumnName != null && !filterColumnName.equalsIgnoreCase("")) && (filterColumnValue != null && !filterColumnValue.equalsIgnoreCase(""))) {
				if(filterColumnType == null || filterColumnType.equalsIgnoreCase("")) {
					filterColumnType = "text";
				}
				if(filterColumnType.equalsIgnoreCase("boolean")) {
				  if(filterColumnValue.equalsIgnoreCase("true")) {
					  endQuery += " " + filterColumnName + " is not null and " + filterColumnName + " is true";
				  } else {
					  endQuery += " " + filterColumnName + " is not null and " + filterColumnName + " is false";
				  }
				} else if(filterColumnType.equalsIgnoreCase("number")) {
					endQuery += " " + filterColumnName + " is not null and " + filterColumnName + " = " + filterColumnValue;
				} else {
					endQuery += " " + filterColumnName + " is not null and " + filterColumnName + " = '" + filterColumnValue + "'";
				}				
			}
			solrEntityConf = solrConf.getSolrEntityConf();
			tableName = solrEntityConf.getName();
			solrColumnConfList = solrEntityConf.getColumns();
			String query = "select * from " + tableName + " limit 1;"; //Get First Row
			server = new ConcurrentUpdateSolrServer(E2emfAppUtil.getAppProperty(solrEntityConf.getSolrCollection()), 1000, 10);
			con = dataanalyser.getDatabaseConnection(E2emfAppUtil.getAppProperty(solrEntityConf.getDbType()), E2emfAppUtil.getAppProperty(solrEntityConf.getDbName()));
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			rsmd = rs.getMetaData();
			if(rsmd != null && filterColumnName != null && !filterColumnName.equalsIgnoreCase("")) {
				for(int i= 1 ; i <=rsmd.getColumnCount(); i++) {
					if(rsmd.getColumnName(i).equalsIgnoreCase(filterColumnName)) {
						isFilterPresent = !isFilterPresent;
						break;
					}
				}
			}	
			con.close();
			rs.close();
			stmt.close();
			query = (!isFilterPresent) ? "select * from " + tableName + ";" : "select * from " + tableName + " where " + endQuery + " ; "; 
			
			server = new ConcurrentUpdateSolrServer(E2emfAppUtil.getAppProperty(solrEntityConf.getSolrCollection()), 1000, 10);
			con = dataanalyser.getDatabaseConnection(E2emfAppUtil.getAppProperty(solrEntityConf.getDbType()), E2emfAppUtil.getAppProperty(solrEntityConf.getDbName()));
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				count++;
				//String docnum = rs.getString("docnum");
				//if(docnum.equalsIgnoreCase("DWG-1007325_B")){
					SolrInputDocument solrDoc = new SolrInputDocument();
					for (SolrColumnConf s : solrColumnConfList) {	
						if (validate(s.getIndex())) {	
							/*if (("int").equalsIgnoreCase(s.getType())) {
								solrDoc.addField(s.getName(),
										rs.getInt(s.getIndex()));
							} else if (("float").equalsIgnoreCase(s.getType())) {
								solrDoc.addField(s.getName(),
										rs.getFloat(s.getIndex()));
							}*/ 
							if("number".equalsIgnoreCase(s.getType())){
								solrDoc.addField(s.getName(), rs.getInt(s.getIndex()));
							}
							else {
								solrDoc.addField(s.getName(), validateNull(rs.getString(s.getIndex())));
							}
						} 
						else if(("date").equalsIgnoreCase(s.getType())){
							solrDoc.addField(s.getName(), new java.util.Date());
						}
						else {
							solrDoc.setField(s.getName(), "");
						}
					}
					server.add(solrDoc);
					if ((count % 10000) == 0) {
						server.commit();
					}
				}
			//}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			if (server != null) {
				server.commit();
				//server.shutdown();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		String message = "{\"message\":\" ";
	//	if (count > 0){
			message +=  + count + " Records are Indexed\"";
	//	}		
		message += "}";
		return message;
	}

	private boolean validate(String index) {
		boolean valid = false;
		if (index != null && index.trim().length() > 0) {
			valid = true;
		}

		return valid;
	}

	private String validateNull(String value) {
		String returnStr = "";
		if (value != null && value.length() >= 1) {
			returnStr = value;
		}
		return returnStr;

	}
}
