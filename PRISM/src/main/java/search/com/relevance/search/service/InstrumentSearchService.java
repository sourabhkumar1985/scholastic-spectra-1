package com.relevance.search.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
//import org.apache.solr.client.solrj.impl.StreamingUpdateSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
//import org.apache.solr.client.solrj.response.PivotField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.relevance.prism.dao.NGramDao;
import com.relevance.prism.data.ClinicalSpeciality;
import com.relevance.prism.data.NGram;
import com.relevance.prism.data.NGramList;
import com.relevance.search.data.QueryItem;
import com.relevance.search.data.QueryParam;
import com.relevance.search.data.SmartFindAuditDocument;
import com.relevance.search.data.SmartFindDocument;
import com.relevance.search.data.SmartFindDocumentList;
import com.relevance.search.data.SmartFindMasterView;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.util.WriteBackUpdater;
import com.relevance.search.dao.InstrumentSearchDao;
import com.relevance.search.dao.SmartFindAuditDao;
import com.relevance.search.data.FacetCountAndResults;
import com.relevance.search.data.Search;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.service.BaseService;

public class InstrumentSearchService extends BaseService{
	//E2emfAppUtil appUtil;
	PrismDbUtil dbUtil;
	static String solarUrl = "SOLR_SERVER_URL";
	static String solarInstanceName = "SOLR_SERVER_INSTANCE_NAME";

	static String solarServerInsanceUrl = null;
	//private StreamingUpdateSolrServer server = null;
	private String solrUrl;
	private SolrInputDocument thisdoc = null;
	final String ALLFIELDS = "rawdata";
	ConcurrentUpdateSolrServer server = null;
	String solrTableString = null;
	String hbaseZookeperQuorum = null;
	String hbaseMaster = null;
	String cdhVersion = null;
	public InstrumentSearchService() {
		String solarInstanceName = E2emfConstants.solarInstrumentCatalogInstanceName;
		
		
		
		try {
			//appUtil = new E2emfAppUtil();
			solrUrl = E2emfAppUtil.getAppProperty(solarInstanceName);
			solrTableString = E2emfAppUtil.getAppProperty(E2emfConstants.HBASE_SOLR_TABLE);
			hbaseZookeperQuorum = E2emfAppUtil.getAppProperty(E2emfConstants.HBASE_ZOOKEPER_QUORUM);
			hbaseMaster = E2emfAppUtil.getAppProperty(E2emfConstants.HBASE_ZOOKEPER_QUORUM);
			cdhVersion = E2emfAppUtil.getAppProperty(E2emfConstants.CDH_VERSION);
			System.out.println("CDH Vesion : " + cdhVersion);
			Log.Info("CDH Vesion : " + cdhVersion);
			System.setProperty("javax.net.ssl.trustStore",
					E2emfAppUtil.getAppProperty("truststore"));
			System.setProperty("javax.net.ssl.trustStorePassword",
					E2emfAppUtil.getAppProperty("password"));
			System.setProperty("java.security.auth.login.config",
					E2emfAppUtil.getAppProperty("jaas"));
			System.setProperty("java.security.krb5.conf",
					E2emfAppUtil.getAppProperty("krb5"));
			
			server = new ConcurrentUpdateSolrServer(solrUrl, 100, 5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.Error(e);
		}
		dbUtil = new PrismDbUtil();
	}

	public String catalogTagUpdate(String id, String tags, int helpfulCount,String title,String docnum,String manufacturer,String components,String categoryl0,String cspeciality, String custom1, String partnumber, String componentnumber, String finishedgoodsnumber, String notes, String userupdated, String userWhoUpdated, String mlocation)
			throws AppException {
		String responseMessage = null;
		try {
			// String searchResultJson = null;
			HttpSolrServer  solarServer = null;
			QueryResponse queryResponse = null;
			ModifiableSolrParams parameters = new ModifiableSolrParams();
			solarServer = getSolarServerConnection();
			
			//HBase Connection 
			//Configuration conf = HBaseConfiguration.create();
			//conf.set("hbase.zookeeper.quorum", hbaseZookeperQuorum);
			//conf.set("hbase.master", hbaseMaster);
			//HTable solrTable = new HTable(conf, solrTableString);
			
			parameters.set("q", "id:"+ "\"" +id+ "\"");  
			Log.Info("param value: " +parameters.get("q"));
		
			if (solarServer != null) {
				queryResponse = solarServer.query(parameters);
				Log.Info("Recieved response from solar successfully ");
			} else {
				Log.Error("Unable to connect to solarServer...");
			}
			Gson gson = new Gson();
			String jsonelement = gson.toJson(queryResponse.getResults());
			JSONArray jsonArray = new JSONArray(jsonelement);
			JSONObject jsonObject = jsonArray.getJSONObject(0);

			thisdoc = new SolrInputDocument();
			thisdoc.addField("id", jsonObject.getString("id"));
			thisdoc.addField("rawdata", jsonObject.getString("rawdata"));
			String category = null;
			if (categoryl0 != null && categoryl0 != ""){
				if(custom1 != null && custom1.equalsIgnoreCase("false")){
					category = WriteBackUpdater.getCategoryInPostGresTable(id);			
					thisdoc.addField("categoryl0", category);
				}else{
					thisdoc.addField("categoryl0", categoryl0);
				}			
			}
			else{
				if(custom1 != null && custom1.equalsIgnoreCase("false")){
					category = WriteBackUpdater.getCategoryInPostGresTable(id);
					thisdoc.addField("categoryl0", category);
				}else{
					categoryl0 = jsonObject.getString("categoryl0");
					thisdoc.addField("categoryl0", categoryl0);
				}				
			}
			if (cspeciality != null && cspeciality != ""){
				thisdoc.addField("cspeciality", cspeciality);
			}
			else{
				cspeciality = jsonObject.getString("cspeciality");
				thisdoc.addField("cspeciality", jsonObject.getString("cspeciality"));
			}
			thisdoc.addField("categoryl1", jsonObject.getString("categoryl1"));
			thisdoc.addField("categoryl2", jsonObject.getString("categoryl2"));
			thisdoc.addField("categoryl3", jsonObject.getString("categoryl3"));
			thisdoc.addField("categoryl4", jsonObject.getString("categoryl4"));
			thisdoc.addField("categoryl5", jsonObject.getString("categoryl5"));
			thisdoc.addField("categoryl6", jsonObject.getString("categoryl6"));
			thisdoc.addField("categoryl7", jsonObject.getString("categoryl7"));
			thisdoc.addField("categoryl8", jsonObject.getString("categoryl8"));
			thisdoc.addField("categoryl9", jsonObject.getString("categoryl9"));
			thisdoc.addField("categoryl10", jsonObject.getString("categoryl10"));
			//thisdoc.addField("categoryl1", jsonObject.getString("categoryl1"));
			thisdoc.addField("smallimagepath", jsonObject.getString("smallimagepath"));
			thisdoc.addField("imagepath", jsonObject.getString("imagepath"));
			thisdoc.addField("filepath", jsonObject.getString("filepath"));
			/*if (tags != null && tags != "")
			thisdoc.addField("tags", tags);
		else
			thisdoc.addField("tags", jsonObject.getString("tags"));
		if (helpfulCount == 0) {
			thisdoc.addField("helpfulcnt", jsonObject.getInt("helpfulcnt"));
			thisdoc.addField("totcnt", jsonObject.getInt("totcnt"));
		} else if (helpfulCount == 1) {
			thisdoc.addField("helpfulcnt",
					jsonObject.getInt("helpfulcnt") + 1);
			thisdoc.addField("totcnt", jsonObject.getInt("totcnt") + 1);
		} else {
			thisdoc.addField("helpfulcnt", jsonObject.getInt("helpfulcnt"));
			thisdoc.addField("totcnt", jsonObject.getInt("totcnt") + 1);
		}*/
			if (components != null && components != ""){
				thisdoc.addField("components", components);
			}
			else{
				components = jsonObject.getString("components");
				thisdoc.addField("components", jsonObject.getString("components"));
			}
			if (title != null && title != ""){
				thisdoc.addField("title", title);
				thisdoc.addField("title2", title);
				thisdoc.addField("original",title);
				thisdoc.addField("numbersyn",title);
			}
			else{
				title = jsonObject.getString("title");
				 thisdoc.addField("title", jsonObject.getString("title"));
				    thisdoc.addField("title2", jsonObject.getString("title2"));
				    thisdoc.addField("original", jsonObject.getString("original"));
				    thisdoc.addField("numbersyn",jsonObject.getString("numbersyn"));
			}
			if (docnum != null && docnum != ""){
				thisdoc.addField("docnum", docnum);
			}
			else{
				docnum = jsonObject.getString("docnum");
				thisdoc.addField("docnum", jsonObject.getString("docnum"));
			}
			if (manufacturer != null && manufacturer != ""){
				thisdoc.addField("manufacturer", manufacturer);
			}
			else{
				manufacturer = jsonObject.getString("manufacturer");
				thisdoc.addField("manufacturer", jsonObject.getString("manufacturer"));
			}
			thisdoc.addField("relateditems", jsonObject.getString("relateditems"));
			thisdoc.addField("lables", jsonObject.getString("lables"));
			thisdoc.addField("rank", jsonObject.getInt("rank"));
			thisdoc.addField("moddate", new java.util.Date());
		   
			thisdoc.addField("source", jsonObject.getString("source"));
			if(custom1 != null && custom1.equalsIgnoreCase("true") && category == null){
				thisdoc.addField("custom1", custom1);
				thisdoc.removeField("categoryl0");
				thisdoc.addField("categoryl0", "Obsolete");
			}else if(custom1 != null && custom1.equalsIgnoreCase("false") && category != null){
				thisdoc.addField("custom1", custom1);				
			}
			else{
			   thisdoc.addField("custom1", jsonObject.getString("custom1"));
			}
			   thisdoc.addField("custom2", jsonObject.getString("custom2"));
			   thisdoc.addField("custom3", jsonObject.getString("custom3"));
			   thisdoc.addField("custom4", jsonObject.getString("custom4"));
			   thisdoc.addField("custom5", jsonObject.getString("custom5"));
			   thisdoc.addField("custom6", jsonObject.getString("custom6"));
			   thisdoc.addField("custom7", jsonObject.getString("custom7"));
			   thisdoc.addField("imageset", jsonObject.getString("imageset"));
			   //thisdoc.addField("userupdated", jsonObject.getString("userupdated"));
			//thisdoc.addField("original", jsonObject.getString("original"));
			   if(notes != null){
				   thisdoc.addField("notes", notes);
			   }else{
				   thisdoc.addField("notes", jsonObject.getString("notes"));
			   }
			   if(partnumber != null){
				   thisdoc.addField("partnum", partnumber);
			   }else{
				   thisdoc.addField("partnum", jsonObject.getString("partnum"));
			   }
			   if(componentnumber != null){
				   thisdoc.addField("compnum", componentnumber);
			   }else{
				   thisdoc.addField("compnum", jsonObject.getString("compnum"));
			   }
			   if(finishedgoodsnumber != null){
				   thisdoc.addField("fingood", finishedgoodsnumber);
			   }else{
				   thisdoc.addField("fingood", jsonObject.getString("fingood"));
			   }
			   if(userupdated != null){
				   thisdoc.addField("userupdated", userupdated);
			   }else{
				   thisdoc.addField("userupdated", jsonObject.getString("userupdated"));
			   }
			   thisdoc.addField("revision", jsonObject.getString("revision"));
			   thisdoc.addField("docnumrev", jsonObject.getString("docnumrev"));
			   if(jsonObject.has("pngimage") && !jsonObject.isNull("pngimage")){
				   thisdoc.addField("pngimage", jsonObject.getString("pngimage"));
			   }
			   if(jsonObject.has("jpgimage") && !jsonObject.isNull("jpgimage")){
				   thisdoc.addField("jpgimage", jsonObject.getString("jpgimage"));
			   }
			   if(mlocation != null & (jsonObject.has("mlocation") && !jsonObject.isNull("mlocation"))){
				   thisdoc.addField("mlocation", mlocation);
			   }else{
			   		if(jsonObject.has("mlocation") && !jsonObject.isNull("mlocation")){
			   			thisdoc.addField("mlocation", jsonObject.getString("mlocation"));
			   		}
			   }
			//System.out.println(thisdoc);
			try {
				long startTime = System.currentTimeMillis();
				long endTime = 0L;
				server.add(thisdoc);
				server.commit();
				endTime = System.currentTimeMillis();
				Log.Info("Updated Masterdata in Solr, time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime));
			} catch (SolrServerException e) {
				Log.Error(e);
			}
			//added code for Postgres table update / Insert
			try {
				long startTime = System.currentTimeMillis();
				long endTime = 0L;
				//WriteBackUpdater.upDateSolrTable(solrTable, id, title, docnum, manufacturer, components, categoryl0,"true");
				if(custom1 != null && custom1.equalsIgnoreCase("true")){
					responseMessage = updateObsoleteStatusInPostGresTable(id, "true");
				}else if(custom1 != null && custom1.equalsIgnoreCase("false")){
					responseMessage = updateObsoleteStatusInPostGresTable(id, "false");
				}else{
					SmartFindAuditDao smartFindAuditDao = new SmartFindAuditDao();
					SmartFindDocument oldSmartFindDocument = smartFindAuditDao.getSmartFindDocument(id);
					WriteBackUpdater.updatePostGresTable(id, title, docnum, manufacturer, components, userupdated, categoryl0, cspeciality, partnumber, componentnumber, finishedgoodsnumber, notes, userWhoUpdated, mlocation);
					SmartFindDocument newSmartFindDocument = smartFindAuditDao.getSmartFindDocument(id);
					if(oldSmartFindDocument != null){
						responseMessage = smartFindAuditDao.addCatalogUpdateDetailsInAuditTable(id, userWhoUpdated, oldSmartFindDocument, newSmartFindDocument);
					}
				}
				Log.Info("Successfully Inserted/Updated User Updation in PostGres Table");				
				endTime = System.currentTimeMillis();
				Log.Info("Updated Masterdata in Postgres, time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime));
			} catch (Exception e) {
				Log.Error("Exception in updating PostGres table for master data." + e);
			}
		} catch (SolrServerException e) {
			Log.Error("Exception in getting Solar Server Connection."
					+ e);
			Log.Error("thisdoc value :" + thisdoc);
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);

		} catch (Exception e) {
			Log.Error("Exception Fetching Search Details." + e);
			Log.Error("thisdoc value :" + thisdoc);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		}
		return responseMessage;
	}
	
	public String getRelatedDocuments(Object... obj) throws AppException{
		String title = null;
		String resultJson = null;
		if(obj != null){
			title = (String) obj[0];
			try {
				// String searchResultJson = null;
				HttpSolrServer  solarServer = null;
				QueryResponse queryResponse = null;
				ModifiableSolrParams parameters = new ModifiableSolrParams();
				solarServer = getSolarServerConnection();
							
				//parameters.set("q", "title:*" + title + "*");
				//parameters.set("q", "title:" + title);
				parameters.set("q", title);
				parameters.set("qf","title");
				parameters.set("defType", "edismax");
				parameters.set("rows", "10");
				if (solarServer != null) {
					queryResponse = solarServer.query(parameters);
					Log.Info("Recieved response from solar successfully ");
				} else {
					Log.Error("Unable to connect to solarServer...");
				}
				Gson gson = new Gson();
				SolrDocumentList results = queryResponse.getResults();
				resultJson = gson.toJson(results);
				
				//JSONArray jsonArray = new JSONArray(jsonelement);
				//JSONObject jsonObject = jsonArray.getJSONObject(0);
				//resultJson = getSearchSuggestion(null, title, null);
				return resultJson;
			} catch (SolrServerException e) {
				Log.Error("Exception in getting Solar Server Connection."
						+ e);
	
			} catch (AppException appe) {
				Log.Error("Exception Fetching Search Details." + appe);
			}catch (Exception e) {
				Log.Error("Exception Fetching Search Details." + e);
				if (e instanceof NullPointerException) {
					throw new AppException("NullPointerException",
							"No Indexed Data Available in Collection.", 3,
							e.getCause(), true);
				}
			}			
		}
		return resultJson;
	}
	
	public String getJsonObject(Object... obj) throws AppException {
		String searchResultJson = null;
		HttpSolrServer  solarServer = null;
		QueryResponse queryResponse = null;

		QueryParam queryParam = null;
		String searchCriteria = null;

		if (obj != null && obj.length > 1) {
			queryParam = (QueryParam) obj[0];
			searchCriteria = ((String) obj[1]).trim();
			Log.Info("Searching initiated for " + searchCriteria);
		}
		if(searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService") && queryParam.getQueryItems() == null) {
			  try {
				  Gson gson = new Gson();
				  java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>> facetMapList = new java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>>();
				  facetMapList.put("cs", getFacetWiseResult("*:*","cspeciality"));
				  facetMapList.put("category", getFacetWiseResult("*:*","categoryl0"));
				  searchResultJson = gson.toJson(facetMapList);
			  }
			  catch(Exception e){
					Log.Error("Exception Fetching JSON Data." + e);
					if(e instanceof NullPointerException){
						throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
					}
					throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
			}		
		}
		else if (searchCriteria != null
				&& searchCriteria.equalsIgnoreCase("searchService")) {
			
			solarServer = getSolarServerConnection();
			Gson gson = new Gson();
			ModifiableSolrParams parameters = buildSearchParams(queryParam,false);
			FacetCountAndResults facetCountAndResults = new FacetCountAndResults();
			if (queryParam.getSource() == null){
			  java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>> facetMapList = new java.util.LinkedHashMap<String, java.util.LinkedHashMap<String, String>>();
			  facetMapList.put("cs",  getFacetWiseResult(parameters.get("q"),"cspeciality"));
			  facetMapList.put("category", getFacetWiseResult(parameters.get("q"),"categoryl0"));
			  facetCountAndResults.setFacetMapList(facetMapList);
			}
			try {
				if (solarServer != null) {
					System.out.println("Search Parameters : " + parameters);
					queryResponse = solarServer.query(parameters);
					Log.Info("Recieved response from solar successfully ");
				} else {
					Log.Error("Unable to connect to solarServer...");
				}
				Search search = new Search();
				
				
				
				SolrDocumentList results = queryResponse.getResults();
				Map<String, Map<String, List<String>>> highlightingMap =  queryResponse.getHighlighting();
				
				if (highlightingMap != null && highlightingMap.size() > 0)//&& searchParam.getSearchField() != null
				{
					for (SolrDocument doc : results)
					{
						Map<String, List<String>> map = highlightingMap.get(doc.getFieldValue("id"));
						
						for(String fieldName : doc.getFieldNames())
						{
							if (map != null && map.size() > 0)
							{
								//List<String> highlightValues = map.get(searchParam.getSearchField());
								List<String> highlightValues = map.get(fieldName);
								if (highlightValues != null && highlightValues.size() > 0)
								{
									//doc.setField(searchParam.getSearchField(), highlightValues.get(0));
									doc.setField(fieldName, highlightValues.get(0));
								}
							}
						}
					}
				}
				
				search.setResult(queryResponse.getResults());
				
				//Not required since highlighting is updated in actual content as per the code block above
				search.setHighlighting(queryResponse.getHighlighting());
				search.setTotalCount(queryResponse.getResults().getNumFound());
				if (queryParam.getQueryItems() != null && queryParam.getQueryItems().size() > 0)
				{
					search.setInputQuery(queryParam.getQueryItems().get(0).getKey());
				}
				else
				{
					search.setInputQuery("");
				}
				search.setSolrQuery(parameters.get("q"));
				facetCountAndResults.setSearch(search);
				searchResultJson = gson.toJson(facetCountAndResults);

				// searchResultJson = parseJSONStr(json);
				// System.out.println("parsedJSONOutPut:" + searchResultJson);
				// Log.Info("parsedJSONOutPut:" +searchResultJson);

			} catch (SolrServerException e) {
				Log.Error("Exception in getting Solar Server Connection."
						+ e);
				throw new AppException(e.getMessage(),
						"Error while processing the request.", 2, e.getCause(),
						true);

			} catch (Exception e) {
				Log.Error("Exception Fetching Search Details."
						+ e);
				if (e instanceof NullPointerException) {
					throw new AppException("NullPointerException",
							"No Indexed Data Available in Collection.", 3,
							e.getCause(), true);
				}
				throw new AppException(e.getMessage(),
						"Error while processing the request.", 2, e.getCause(),
						true);
			}
		}

		return searchResultJson;
	}

	private String buildCondition(QueryItem queryItem)
	{
		StringBuilder searchStr = new StringBuilder();
		String key = queryItem.getKey();
		String searchType = null;
		String searchQuery = null;
		String searchField = null;
		boolean isSearchAllFields = false;
		
		if (queryItem.getField() != null && ALLFIELDS.equalsIgnoreCase(queryItem.getField()))
		{
			searchField = ALLFIELDS;
			isSearchAllFields = true;
		}
		else if (queryItem.getField() != null && queryItem.getField().length() > 0)
		{
			searchField = queryItem.getField();
		}
		else
		{
			searchField = "title";
		}
		
		if (queryItem.getType() != null && queryItem.getType().length() > 0)
		{
			searchType = queryItem.getType();
		}
		else
		{
			searchType = "all";
		}
		
		
		if ("exact".equalsIgnoreCase(searchType))
		{
			String delim = "\"";
			if (key != null && key.contains("*"))
			{
				delim = "";
			}
			//String delimiter = "*";
			
			searchStr.append(searchField);
			searchStr.append(":");
			searchStr.append(delim);
			searchStr.append(key);
			searchStr.append(delim);
			
			searchQuery = searchStr.toString();
		}
		else
		{	String[] tokens = null;
			if(queryItem.getField().equalsIgnoreCase("imageset")){
				tokens = new String[1];
				tokens[0] = queryItem.getKey();
			}else{
				tokens = key.split(" ");
			}
			//String conjunction = " OR ";
			
			if ("none".equalsIgnoreCase(searchType))
			{
				searchField = "-" + searchField;
				//conjunction = " AND ";
			}
			
			//Using HashMap to remove duplicate words in search
			HashMap<String, String> tokenMap = new HashMap<String, String>();
			StringBuilder condition = null;
			
			ArrayList<String> tokensList = new ArrayList<String>(Arrays.asList(tokens));
			for (String token : tokensList)
			{
				condition = new StringBuilder();
				//String token = tokens[i];
				
				String delimiter = "\"";
				
				if (token != null && token.length() > 0)
				{
					
					if (token.contains("*"))
					{
						delimiter = "";
					}
					if (!"none".equalsIgnoreCase(searchType))
						condition.append("(");
					
					condition.append(searchField);
					condition.append(":");
					condition.append(delimiter);
					condition.append(token);
					condition.append(delimiter);
					
					if (!"none".equalsIgnoreCase(searchType))
						condition.append(")");
					
					tokenMap.put(token, condition.toString());
					
				}
			}
			
			
			
			/*ArrayList<String> updatedTokensList = new ArrayList<String>();
			int index = 0;
			
			//If string is like 1.1 mm ==> search for 1.1mm (without space too)
			for (String token : tokensList)
			{
				index = tokensList.indexOf(token);
				
				if (token != null && index > 0 && "mm".equalsIgnoreCase(token))
				{
					updatedTokensList.add(tokensList.get(index - 1) + "mm");
					updatedTokensList.remove(tokensList.get(index - 1));
				}
				else
				{
					updatedTokensList.add(token);
				}
			}
			
			
			for (String token : updatedTokensList)
			{
				condition = new StringBuilder();
				//String token = tokens[i];
				
				String delimiter = "\"";
				
				if (token != null && token.length() > 0)
				{
					
					if (token.contains("*"))
					{
						delimiter = "";
					}
					
					
					if (token.length() > 2 && (token.endsWith("mm") || token.endsWith("MM")))
					{
						String tokenWithDotZero = null;
						String tokenWithoutDotZero = null;
						
						//eg 2.0mm
						if (token.contains(".0"))
						{
							tokenWithDotZero = token;
							tokenWithoutDotZero = token.replaceAll(".0", "");
						}
						else
						{
							if (token.endsWith("mm"))
								tokenWithDotZero = token.replaceAll("mm", ".0mm");
							else
								tokenWithDotZero = token.replaceAll("MM", ".0mm");
							tokenWithoutDotZero = token;
						}
						
						condition.append("(");
						
						condition.append(searchField);
						condition.append(":");
						condition.append(delimiter);
						condition.append(tokenWithDotZero);
						condition.append(delimiter);
						
						condition.append(conjunction);
						
						condition.append(searchField);
						condition.append(":");
						condition.append(delimiter);
						condition.append(tokenWithDotZero.replaceAll("mm", " mm"));
						condition.append(delimiter);
						
						condition.append(conjunction);

						condition.append(searchField);
						condition.append(":");
						condition.append(delimiter);
						condition.append(tokenWithoutDotZero);
						condition.append(delimiter);
						
						condition.append(conjunction);

						condition.append(searchField);
						condition.append(":");
						condition.append(delimiter);
						condition.append(tokenWithoutDotZero.replaceAll("mm", " mm"));
						condition.append(delimiter);
						
						condition.append(")");
						
						//token = token.replaceAll("mm", " mm");
						//token = token.replaceAll("MM", " mm");
						//String[] subTokens = token.split(" ");
						//for (int j = 0; j < subTokens.length; j++)
						//{
							//String subToken = subTokens[j];
							//if (subToken != null && subToken.length() > 0)
							//{
								//remove .0 from the key string
								//if (subToken.endsWith(".0"))
								//{
								//	subToken = subToken.replaceAll(".0", "");
								//}
							//}
							
						//}
						
						
						tokenMap.put(token, condition.toString());
						
						
					}
					else
					{
						//remove .0 from the key string
						if (token.endsWith(".0"))
						{
							condition.append("(");
							
							condition.append(searchField);
							condition.append(":");
							condition.append(delimiter);
							condition.append(token);
							condition.append(delimiter);
							
							condition.append(conjunction);
							
							condition.append(searchField);
							condition.append(":");
							condition.append(delimiter);
							condition.append(token.replaceAll(".0", ""));
							condition.append(delimiter);
							
							condition.append(")");
						}
						else
						{
							condition.append("(");
							
							condition.append(searchField);
							condition.append(":");
							condition.append(delimiter);
							condition.append(token);
							condition.append(delimiter);
							
							condition.append(")");
						}
						tokenMap.put(token, condition.toString());
					}
				}
			}*/
			
		
			
			for (Map.Entry<String, String> entry : tokenMap.entrySet())
		    {
				String subCondition = entry.getValue();
				
				searchStr.append(subCondition);
				
			
				if ("any".equalsIgnoreCase(searchType))
					searchStr.append(" OR ");
				else if ("all".equalsIgnoreCase(searchType))
					searchStr.append(" AND ");
				else if ("none".equalsIgnoreCase(searchType))
					searchStr.append(" AND ");
				
			}
			
			//To remove last OR or AND keyword
			if ("any".equalsIgnoreCase(searchType) && searchStr.lastIndexOf(" OR ") > 0)
				searchQuery = searchStr.substring(0, searchStr.lastIndexOf(" OR "));
			else if ("all".equalsIgnoreCase(searchType) && searchStr.lastIndexOf(" AND ") > 0)
				searchQuery = searchStr.substring(0, searchStr.lastIndexOf(" AND "));
			else if ("none".equalsIgnoreCase(searchType) && searchStr.lastIndexOf(" AND ") > 0)
				searchQuery = searchStr.substring(0, searchStr.lastIndexOf(" AND "));
		}
		
		if (isSearchAllFields)
		{
			//For all fields search based on ranking parameter
			searchQuery = searchQuery.replaceAll(ALLFIELDS + ":", "");
		}
		
		
		return searchQuery;
		

	}
	
	private ModifiableSolrParams buildSearchParams(QueryParam queryParam,boolean getFacetWiseSearch)
	{
		ModifiableSolrParams parameters = new ModifiableSolrParams();
		StringBuilder searchQuery = new StringBuilder();
		StringBuilder fieldList = new StringBuilder();
		String searchFields = null;
		String paramQuery = null;
		String outputField = null;
		boolean isAllFieldExists = false;
		String condition = " AND ";
		if (queryParam.getCondition() != null && queryParam.getCondition().equalsIgnoreCase("or"))
			condition = " OR ";
		
		if (queryParam.getOutputField() != null && !queryParam.getOutputField().equalsIgnoreCase(""))
			outputField = queryParam.getOutputField() ;
		if (queryParam.getQueryItems() != null)
		{
			for (QueryItem queryItem : queryParam.getQueryItems())
			{
				if(queryItem.getField().equalsIgnoreCase("original") & !queryItem.getType().equalsIgnoreCase("none")){
					String query = buildCondition(queryItem);
					searchQuery.append("(");
					searchQuery.append(query);
					searchQuery.append(")");
					
					/*searchQuery.append("OR");
					
					searchQuery.append("(");
					searchQuery.append(query.replaceAll("original", "title"));
					searchQuery.append(")");
					*/
					searchQuery.append(" OR ");
										
					searchQuery.append("(");
					searchQuery.append(query.replaceAll("original", "numbersyn"));
					searchQuery.append(")");
				}
				else{
					if (!queryItem.getType().equalsIgnoreCase("none"))
						searchQuery.append("(");
					searchQuery.append(buildCondition(queryItem));
					if (!queryItem.getType().equalsIgnoreCase("none"))
						searchQuery.append(")");
				}
				
				searchQuery.append(condition);
				
				if (ALLFIELDS.equalsIgnoreCase(queryItem.getField()))
				{
					isAllFieldExists = true;
				}
				else
				{
					fieldList.append(queryItem.getField());
					fieldList.append(",");
				}
			
			}
			
			//To remove last AND keyword
			if (searchQuery.lastIndexOf(condition) > 0)
				paramQuery = searchQuery.substring(0, searchQuery.lastIndexOf(condition));
			else
				paramQuery = searchQuery.toString();
			
			//To remove last comma
			if (fieldList.lastIndexOf(",") > 0)
				searchFields = fieldList.substring(0, fieldList.lastIndexOf(","));
			else
				searchFields = fieldList.toString();
			
		}
		
		
		
		parameters.set("q", paramQuery);
		
		// parameters.set("wt", "xml");//Not required since converting from
		// the object directly
		parameters.set("start", queryParam.getStart());
		parameters.set("rows", queryParam.getRows());
		if (getFacetWiseSearch){
			parameters.set("facet", "true");
			parameters.set("facet.field", queryParam.getFacet());
		} else {
		//To show only selected output fields
			if (outputField== null){
				parameters.set("fl","id,title,docnum,components,manufacturer,smallimagepath,filepath,imagepath,categoryl0,original,cspeciality,partnum,compnum,fingood,notes,custom1,custom2,custom3,custom4,custom6,imageset,pngimage,jpgimage,mlocation");
				parameters.set("hl","true");//highlighting is true
			} else{
				parameters.set("fl",outputField);
			}
			
			if (isAllFieldExists)
			{
				parameters.set("qf","title^5 numbersyn^80 original^10000 components docnum manufacturer partnum compnum fingood imageset docnumrev custom3 custom4 custom6");
				parameters.set("hl.fl", "title,original,components,docnum,manufacturer,score,partnum,compnum,fingood,imageset,docnumrev");
				parameters.set("defType", "edismax");
			}
			else
			{
				parameters.set("hl.fl", searchFields);
			}
		}//for all conditions need to filter the document which are deleted
		//parameters.set("fq", "!custom1:true");
		return parameters;
	}
	
	
	public String getNGramAnalsyer(int gram,QueryParam queryParam,boolean allRecords) throws AppException {
		String searchResultJson = null;
	     HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;
		Iterator<Map.Entry<String, Integer>> it ;
		NGramList nGramView = new NGramList();
		List<NGram> nGramList = new ArrayList<NGram>();
		NGramDao nGramDao = new NGramDao();
		//SearchParam searchParam = null;
		Map<String, Integer> nGramHashMap = new HashMap<String, Integer>();
		Map<String, Integer> nGramHashMapTemp = null;
		solarServer = getSolarServerConnection();
		//StringBuilder searchStr = new StringBuilder();
		ModifiableSolrParams parameters = new ModifiableSolrParams();
		if (queryParam.getQueryItems() != null && queryParam.getQueryItems().size() > 0){
			parameters = buildSearchParams(queryParam,false);
		}else{
			parameters = new ModifiableSolrParams();
			parameters.set("q", "*:*");
			parameters.set("defType", "edismax");
			parameters.set("fl", queryParam.getOutputField());
			parameters.set("start", queryParam.getStart());
			parameters.set("rows", queryParam.getRows());
		}
		try {
			if (solarServer != null) {
				queryResponse = solarServer.query(parameters);
				Log.Info("Recieved response from solar successfully ");
			} else {
				Log.Error("Unable to connect to solarServer...");
			}
			Gson gson = new Gson();
			SolrDocumentList results = queryResponse.getResults();
			ListIterator<SolrDocument> iter = results.listIterator();
	        while (iter.hasNext()) {
	            SolrDocument doc = iter.next();
	            Map<String, Collection<Object>> values = doc.getFieldValuesMap();

	            Iterator<String> names = doc.getFieldNames().iterator();
	            while (names.hasNext()) {
	                String name = names.next();
	                Collection<Object> vals = values.get(name);
	                Iterator<Object> valsIter = vals.iterator();
	                while (valsIter.hasNext()) {
	                    Object obj = valsIter.next();
	                    nGramHashMapTemp = new HashMap<String, Integer>();
	                    nGramHashMapTemp = nGramDao.nGramAnalyserHasMap(obj.toString(),gram);
	                    	it = nGramHashMapTemp.entrySet()
	        						.iterator();
	        			while (it.hasNext()) {
	        				Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
	        						.next();
	        				if (nGramHashMap.containsKey(pairs.getKey())){
	        					int tempValue = nGramHashMap.get(pairs.getKey());
	        					nGramHashMap.put(pairs.getKey(), tempValue+pairs.getValue());
	        				} else {
	        					nGramHashMap.put(pairs.getKey(), pairs.getValue());
	        				}
	        			}
	                }
	            }
	        }
	        it = nGramHashMap.entrySet()
					.iterator();
		while (it.hasNext()) {
			Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
					.next();
			NGram nGram = new NGram();
			nGram.setText(pairs.getKey());
			nGram.setSize(pairs.getValue());
			nGramList.add(nGram);
		}
		Collections.sort(nGramList);
		if (nGramList.size()>100 && allRecords == false){
			List<NGram> subItems = new ArrayList<NGram>(nGramList.subList(0, 100));
			nGramView.setNGramAnalyser(subItems);
		}else
			nGramView.setNGramAnalyser(nGramList);
		
		searchResultJson = gson.toJson(nGramView);
			/*String jsonelement = gson.toJson(queryResponse.getResults());
			JSONArray jsonArray = new JSONArray(jsonelement);
			
			String nGramFilePath = E2emfConstants.solarInstrumentCatalogNGramFilePath;
			nGramFilePath = E2emfAppUtil.getAppProperty(nGramFilePath);
			//String line;
			BufferedReader reader = new BufferedReader(new FileReader(nGramFilePath));

				// Loop over lines in the file and print them.
				while (true) {
				    String line = reader.readLine();
				    if (line == null) {
					break;
				    }
				    searchStr.append(" ").append(line);
				}

				// Close the BufferedReader.
			reader.close();
			
			NGramView nGramView = nGramDao.nGramAnalyserHasMap(searchStr.toString(),gram);
			searchResultJson = gson.toJson(nGramView);*/
			// searchResultJson = parseJSONStr(json);
			// System.out.println("parsedJSONOutPut:" + searchResultJson);
			// Log.Info("parsedJSONOutPut:" +searchResultJson);

		} catch (Exception e) {
			Log.Error("Exception Fetching Search Details." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		}
		return searchResultJson;
	}

	
	/*public String performNgram(int gram,QueryParam queryParam,boolean allRecords) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String result = null;
		
		try 
		{
			
		NGramDao nGramDao = new NGramDao();
		String query = "select title from solrmaster where cat_approved is null";
		result = nGramDao.performNgram(gram, queryParam, query, allRecords);
			

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}*/
	/*private String parseJSONStr(String jsonStr) throws AppException {

		org.json.JSONArray jsonArr = null;
		String outputJSONStr = "";
		// List<String> outputList = new ArrayList<String>();

		try {

			jsonArr = new org.json.JSONArray(jsonStr);
			int length = jsonArr.length();
			// System.out.println("length:" + length);
			outputJSONStr = "[";

			for (int i = 0; i < length; i++) {
				JSONObject jsonObj = (JSONObject) jsonArr.get(i);
				outputJSONStr = outputJSONStr.concat("{\"key\":\"").concat(
						jsonObj.getString("id").concat("\","));
				outputJSONStr = outputJSONStr.concat(jsonObj
						.getString("output").substring(1));
				if (i != length - 1) {
					outputJSONStr = outputJSONStr.concat(",");
				}
			}
			outputJSONStr = outputJSONStr.concat("]");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			Log.Error("Exception parsing the JSON String." + e.getStackTrace());
			Log.Error(e);
			e.printStackTrace();
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		}
		return outputJSONStr;
	}
*/
	
	public String getSearchSuggestion(String type,String key,String source) throws AppException{
		String searchResultJson = null;
		HttpSolrServer  solarServer = null;
		QueryResponse queryResponse = null;

		/*QueryParam queryParam = null;
		String searchCriteria = null;*/
		//key = "drill";
	/*	if (obj != null && obj.length > 1) {
			queryParam = (QueryParam) obj[0];
			searchCriteria = ((String) obj[1]).trim();
			Log.Info("Searching initiated for " + searchCriteria);
		}*/
		
		 ModifiableSolrParams parameters = new ModifiableSolrParams();	 
		  //parameters.set("q", "rawdata:*" + searchParam.getKey().replaceAll(" ", "*") + "*");//Search only in all data column
		  
		  parameters.set("spellcheck.q", key.toString());
		  parameters.set("defType", "edismax");	
		  //parameters.set("wt", "velocity");//Not required since converting from the object directly
		  parameters.set("qt", "/suggest");
		  //parameters.set("rows", "8");
		  //parameters.set("v.template", "suggest");
		/*  parameters.set("qf", "rawdata rank^2 keywords");
		  parameters.set("facet", "true");
		  parameters.set("facet.field", "facet");
		  parameters.set("facet.sort", "rank+asc");*/
		  //parameters.set("fq", "facet:"+queryFilterVal);	 
		  solarServer = getSolarServerConnection();
		  try {
			  if(solarServer != null){
				  queryResponse = solarServer.query(parameters);  
				  Log.Info("Recieved response from solar successfully "  );
			  }else{
				  Log.Error("Unable to connect to solarServer...");
			  }
			  Gson gson = new Gson();
			  //String json = gson.toJson(queryResponse.getResponseHeader());

			  String json = gson.toJson(queryResponse.getSpellCheckResponse().getSuggestions());
			  /*String json2 = gson.toJson(queryResponse.getResponse());
			  String json3 = gson.toJson(queryResponse.getTermsResponse());
			  String json4 = gson.toJson(queryResponse.getResults());
			  java.util.LinkedHashMap<String, String> facetMap = new java.util.LinkedHashMap<String, String>();
			  
			  //First for loop is for fieldname as "facet"
			  if (queryResponse != null && queryResponse.getFacetFields() != null)
			  {
				  for(FacetField facetField : queryResponse.getFacetFields())
				  {
						  for(Count facetFieldCount : facetField.getValues())
						  {
							  facetMap.put(facetFieldCount.getName(), "" + facetFieldCount.getCount());
						  }
				  }
			  }*/
			  //String json = gson.toJson(facetMap);
			  searchResultJson = json;
		  }
		  catch (SolrServerException e) {					  
			  	Log.Error("Exception in getting Solar Server Connection." + e);
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);

		  }
		  catch(Exception e){
				Log.Error("Exception Fetching JSON Data." + e);
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
		}

		return searchResultJson;
	}
	private HttpSolrServer  getSolarServerConnection()
			throws AppException {
		HttpSolrServer  serverCon = null;
		String solarInstanceName = E2emfConstants.solarInstrumentCatalogInstanceName;
		solarInstanceName = E2emfAppUtil.getAppProperty(solarInstanceName);
		try {
			serverCon = new HttpSolrServer(solarInstanceName);
		} catch (Exception e) {
			Log.Error("Exception connecting to solr server."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		}

		return serverCon;
	}
	private  java.util.LinkedHashMap<String, String> getFacetWiseResult(String key, String facetFieldName) throws AppException{
		HttpSolrServer  solarServer = null;
		 solarServer = getSolarServerConnection();
		 QueryResponse queryResponse = null;
		  try {
			  SolrQuery parameters = new SolrQuery();
			  parameters.add("q",key);
			  parameters.set("start", "0");
			  parameters.set("rows", "0");
			  parameters.set("defType", "edismax");
			  parameters.set("facet", "true");
			  parameters.set("facet.field",facetFieldName);
			  parameters.set("qf","title^5 numbersyn^80 original^10000 components docnum manufacturer docnumrev");
			  parameters.set("facet.sort", "index");
			  parameters.set("facet.limit", "4000");
			  //parameters.set("fq","!custom1:true");
			  if(solarServer != null){
				  queryResponse = solarServer.query(parameters);  
				  Log.Info("Recieved response from solar successfully ");
			  }else{
				  Log.Error("Unable to connect to solarServer...");
			  }
			  java.util.LinkedHashMap<String, String> facetMap = new java.util.LinkedHashMap<String, String>();
			  
			  //First for loop is for fieldname as "facet"
			  if (queryResponse != null && queryResponse.getFacetFields() != null)
			  {
				  for(FacetField facetField : queryResponse.getFacetFields())
				  {
						  for(Count facetFieldCount : facetField.getValues())
						  {
							  facetMap.put(facetFieldCount.getName(), "" + facetFieldCount.getCount());
						  }
				  }
			  }
			  return facetMap;
		  }
		  catch (SolrServerException e) {					  
			  	Log.Error("Exception in getting Solar Server Connection." + e);
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);

		  }
		  catch(Exception e){
				Log.Error("Exception Fetching JSON Data." + e);
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
		}
	}
	
	public ArrayList<SmartFindMasterView> getSmartFindMasterViewObject(Object... obj) throws AppException
	{
			ArrayList<SmartFindMasterView> viewList = null;
			try{
				InstrumentSearchDao dao = new InstrumentSearchDao();
				viewList = dao.getSmartFindMasterDetails(obj);
			}catch(AppException appe){
				throw appe;
			} catch(Exception e) {
				Log.Error("Exception fetching profile in the Instrument Search Service getSmartFindMasterViewObject()" + e);
			}
			return viewList;
	}
	
	
	public List<SearchLog> getSearchHistory(SearchLog searchQuery, String orderBy, String orderStyle) throws AppException{
		List<SearchLog> searchHisory = new ArrayList<SearchLog>();
		try{
			Log.Info("getSearchHistory method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			searchHisory = instrumentSearchDao.getSearchHistory(searchQuery, orderBy, orderStyle);
			return searchHisory;
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while fetching SearchHistory " + e);
		}
		return searchHisory;
	}
	
	public ArrayList<SmartFindMasterView> getSmartFindL2Details(Object... obj) throws AppException {
		ArrayList<SmartFindMasterView> smartFindL2DetailsList = new ArrayList<SmartFindMasterView>();
		try{
			Log.Info("getSmartFindL2Details method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			smartFindL2DetailsList = instrumentSearchDao.getSmartFindL2Details(obj);
			return smartFindL2DetailsList;
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while fetching SmartFind L2 Details " + e);
		}
		return smartFindL2DetailsList;
	}
	
	public List<ClinicalSpeciality> getClinicalSpeliality(String rules) throws AppException {
	List<ClinicalSpeciality> clinicalSpecialityList = new ArrayList<ClinicalSpeciality>(); 
	try{
			Log.Info("getSmartFindL2Details method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			clinicalSpecialityList = instrumentSearchDao.getClinicalSpeciality(rules);
	}catch(AppException appe){
		throw appe;
	}catch(Exception e){
		Log.Error("Exception while fetching SmartFind L2 Details " + e);
	}
	return clinicalSpecialityList;
	}
	
	
	//Clinical Speciality - Insert, Update, Delete
	
	public String insertClinicalSpeciality(Object... obj) throws AppException{
		ClinicalSpeciality clinicalspeciality = new ClinicalSpeciality();
		String responseMessage = null;
		try{
			Log.Info("insertClinicalSpeciality method called on Instrumental Search service");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			clinicalspeciality.setCompany(obj[0].toString());
			clinicalspeciality.setClinicalSpeciality(obj[1].toString());
			responseMessage = instrumentSearchDao.insertClinicalSpeciality(clinicalspeciality);
			Log.Info("ClinicalSpeciality inserted in DB.");
			return responseMessage;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while inserting ClinicalSpeciality in DB " + e);
		}
		System.out.println("Service" +responseMessage);
		return responseMessage;
	}
	
	public String updateClinicalSpeciality(Object... obj) throws AppException{
		ClinicalSpeciality clinicalspeciality = new ClinicalSpeciality();
		String responseMessage = null;
		try{
			Log.Info("updateClinicalSpeciality() method called on Instrument Search service");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			clinicalspeciality.setCompany(obj[0].toString());
			clinicalspeciality.setClinicalSpeciality(obj[1].toString());
			clinicalspeciality.setId(obj[2].toString());
			
			responseMessage = instrumentSearchDao.updateClinicalSpeciality(clinicalspeciality);
			Log.Info("ClinicalSpeciality Updated in DB.");
			return responseMessage;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while Updating ClinicalSpeciality in DB " + e);
		}
		return responseMessage;
	}
	
	public String deleteClinicalSpeciality(Object... obj) throws AppException{
		ClinicalSpeciality clinicalspeciality = new ClinicalSpeciality();
		String responseMessage = null;
		try{
			Log.Info("deleteClinicalSpeciality method called on Instrument Search service");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			
			clinicalspeciality.setCompany(obj[0].toString());
					
			responseMessage = instrumentSearchDao.deleteClinicalSpeciality(clinicalspeciality);
			Log.Info("ClinicalSpeciality deleted in DB.");
			return responseMessage;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while deleting ClinicalSpeciality from DB " + e);
		}
		return responseMessage;
	}
	

	public String refreshNgram() throws AppException {
		String resultStatus=null;
		try{
			Log.Info("refreshNgram method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			resultStatus = instrumentSearchDao.refreshNgram();
			return null;
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while fetching SearchHistory " + e);
		}
		return resultStatus;
	}

	public String getNGramAnalsyerStoredResults(int gram, String source,
			String category, String subcategory, int range, boolean isDictionaryWords) throws AppException {
		String resultStatus=null;
		try{
			Log.Info("getNGramAnalsyerStoredResults method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			resultStatus = instrumentSearchDao.getNGramAnalsyerStoredResults(gram, source,category,subcategory, range, isDictionaryWords);
			return resultStatus;
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while fetching SearchHistory " + e);
		}
		return null;
	}

	
		
	public String doBulkUpdate(SmartFindDocumentList smartFindDocumentList, String userName) throws AppException{
		try{
			Log.Info("doBulkUpdate() method called on InstrumentSearchService");
			for(SmartFindDocument smartFindDocument : smartFindDocumentList.getSmartFindDocumentList()){
				long startTime = System.currentTimeMillis();
				long endTime = 0L;
				String id = smartFindDocument.getId();
				//String originalTitle = smartFindDocument.getOriginalTitle();
				String correctedTitle = smartFindDocument.getCorrectedTitle();
				catalogTagUpdate(id, null, 0, correctedTitle, null, null, null, null, null, null, null, null, null, null, "user", userName, null);
				endTime = System.currentTimeMillis();
				Log.Info("Updated Masterdata in Solr and Postgres, total time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime));
			}
			return "{\"message\":\""+ smartFindDocumentList.getSmartFindDocumentList().size() + " " + "Docments are corrected." +"\"}";
		}catch(Exception e){
			Log.Error("Exception while doing Bulk Update."  + e);
		}
		
		return "{\"message\":\""+ smartFindDocumentList.getSmartFindDocumentList().size() + " " + "Docments are corrected." +"\"}";
	}

	
	public String updateObsoleteStatusInPostGresTable(Object... obj) throws AppException{
		String responseMessage = null;
		try{
			Log.Info("updateDeleteStatusInPostGresTable method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			String id = obj[0].toString();
			String status = obj[1].toString();
			responseMessage = instrumentSearchDao.updateObsoleteStatusInPostGresTable(id, status);
			Log.Info("delete status updated in Postgres DB.");
			return responseMessage;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while updating delete status in DB " + e);
		}
		System.out.println("Service" +responseMessage);
		return responseMessage;
	}
	
	public String incrementActionCount(String solrid, String action) throws AppException{
		long numberOfTimes = 0;
		try {
			// String searchResultJson = null;
			HttpSolrServer solarServer = null;
			QueryResponse queryResponse = null;
			ModifiableSolrParams parameters = new ModifiableSolrParams();
			solarServer = getSolarServerConnection();
			
			parameters.set("q", "id:*" + solrid + "*");
			if (solarServer != null) {
				queryResponse = solarServer.query(parameters);
				Log.Info("Recieved response from solar successfully ");
			} else {
				Log.Error("Unable to connect to solarServer...");
			}
			Gson gson = new Gson();
			String jsonelement = gson.toJson(queryResponse.getResults());
			JSONArray jsonArray = new JSONArray(jsonelement);
			JSONObject jsonObject = jsonArray.getJSONObject(0);

			thisdoc = new SolrInputDocument();
			thisdoc.addField("id", jsonObject.getString("id"));
			thisdoc.addField("rawdata", jsonObject.getString("rawdata"));
			thisdoc.addField("categoryl0", jsonObject.getString("categoryl0"));
			thisdoc.addField("categoryl1", jsonObject.getString("categoryl1"));
			thisdoc.addField("cspeciality", jsonObject.getString("cspeciality"));
			thisdoc.addField("categoryl2", jsonObject.getString("categoryl2"));
			thisdoc.addField("categoryl3", jsonObject.getString("categoryl3"));
			thisdoc.addField("categoryl4", jsonObject.getString("categoryl4"));
			thisdoc.addField("categoryl5", jsonObject.getString("categoryl5"));
			thisdoc.addField("categoryl6", jsonObject.getString("categoryl6"));
			thisdoc.addField("categoryl7", jsonObject.getString("categoryl7"));
			thisdoc.addField("categoryl8", jsonObject.getString("categoryl8"));
			thisdoc.addField("categoryl9", jsonObject.getString("categoryl9"));
			thisdoc.addField("categoryl10", jsonObject.getString("categoryl10"));
			thisdoc.addField("smallimagepath", jsonObject.getString("smallimagepath"));
			thisdoc.addField("imagepath", jsonObject.getString("imagepath"));
			thisdoc.addField("filepath", jsonObject.getString("filepath"));
			thisdoc.addField("components", jsonObject.getString("components"));
			thisdoc.addField("title", jsonObject.getString("title"));
			thisdoc.addField("title2", jsonObject.getString("title2"));
			thisdoc.addField("original", jsonObject.getString("original"));
			thisdoc.addField("numbersyn",jsonObject.getString("numbersyn"));
			thisdoc.addField("docnum", jsonObject.getString("docnum"));
			thisdoc.addField("manufacturer", jsonObject.getString("manufacturer"));
			thisdoc.addField("relateditems", jsonObject.getString("relateditems"));
			thisdoc.addField("lables", jsonObject.getString("lables"));
			thisdoc.addField("rank", jsonObject.getInt("rank"));
			thisdoc.addField("moddate", new java.util.Date());
			thisdoc.addField("source", jsonObject.getString("source"));
			thisdoc.addField("custom1", jsonObject.getString("custom1"));
			if(action.equalsIgnoreCase("click")){
				String clicked = jsonObject.getString("custom2");
				if(clicked == null || clicked.equalsIgnoreCase("") || clicked == ""){
					numberOfTimes = 1;
					thisdoc.addField("custom2", "1");
					thisdoc.addField("custom3", jsonObject.getString("custom3"));
					thisdoc.addField("custom4", jsonObject.getString("custom4"));
				}else{
					long clickedInLong = Long.valueOf(clicked).longValue();
					clickedInLong = clickedInLong + 1;
					numberOfTimes = clickedInLong;
					thisdoc.addField("custom2", clickedInLong);
					thisdoc.addField("custom3", jsonObject.getString("custom3"));
					thisdoc.addField("custom4", jsonObject.getString("custom4"));
				}
			}
			if(action.equalsIgnoreCase("download")){
				String downloaded = jsonObject.getString("custom3");
				if(downloaded == null || downloaded.equalsIgnoreCase("") || downloaded == ""){
					numberOfTimes = 1;
					thisdoc.addField("custom3", "1");
					thisdoc.addField("custom2", jsonObject.getString("custom2"));
					thisdoc.addField("custom4", jsonObject.getString("custom4"));
				}else{
					long downloadedInLong = Long.valueOf(downloaded).longValue();
					downloadedInLong = downloadedInLong + 1;
					numberOfTimes = downloadedInLong;
					thisdoc.addField("custom3", downloadedInLong);
					thisdoc.addField("custom2", jsonObject.getString("custom2"));
					thisdoc.addField("custom4", jsonObject.getString("custom4"));
				}
			}
			if(action.equalsIgnoreCase("email")){
				String emailed = jsonObject.getString("custom4");
				if(emailed == null || emailed.equalsIgnoreCase("") || emailed == ""){
					numberOfTimes = 1;
					thisdoc.addField("custom4", "1");
					thisdoc.addField("custom2", jsonObject.getString("custom2"));
					thisdoc.addField("custom3", jsonObject.getString("custom3"));
				}else{
					long emailedInLong = Long.valueOf(emailed).longValue();
					emailedInLong = emailedInLong + 1;
					numberOfTimes = emailedInLong;
					thisdoc.addField("custom4", emailedInLong);
					thisdoc.addField("custom2", jsonObject.getString("custom2"));
					thisdoc.addField("custom3", jsonObject.getString("custom3"));
				}
			}			
			thisdoc.addField("custom5", jsonObject.getString("custom5"));
			thisdoc.addField("custom6", jsonObject.getString("custom6"));
			thisdoc.addField("custom7", jsonObject.getString("custom7"));
			thisdoc.addField("notes", jsonObject.getString("notes"));
			thisdoc.addField("partnum", jsonObject.getString("partnum"));
			thisdoc.addField("compnum", jsonObject.getString("compnum"));
			thisdoc.addField("fingood", jsonObject.getString("fingood"));
			thisdoc.addField("userupdated", jsonObject.getString("userupdated"));
			thisdoc.addField("imageset", jsonObject.getString("imageset"));
			//thisdoc.addField("notes", jsonObject.getString("notes"));
			//thisdoc.addField("partnum", jsonObject.getString("partnum"));
			//thisdoc.addField("compnum", jsonObject.getString("compnum"));
			thisdoc.addField("revision", jsonObject.getString("revision"));
			thisdoc.addField("docnumrev", jsonObject.getString("docnumrev"));
			if(jsonObject.has("pngimage") && !jsonObject.isNull("pngimage")){
			   thisdoc.addField("pngimage", jsonObject.getString("pngimage"));
			}
			if(jsonObject.has("jpgimage") && !jsonObject.isNull("jpgimage")){
			   thisdoc.addField("jpgimage", jsonObject.getString("jpgimage"));
			}
			if(jsonObject.has("mlocation") && !jsonObject.isNull("mlocation")){
	   			thisdoc.addField("mlocation", jsonObject.getString("mlocation"));
	   		}
			//System.out.println(thisdoc);
			try {
				long startTime = System.currentTimeMillis();
				long endTime = 0L;
				server.add(thisdoc);
				server.commit();
				endTime = System.currentTimeMillis();
				Log.Info("Updated Masterdata in Solr, time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime));
			} catch (SolrServerException e) {
				Log.Error(e);
			}
			//added code for Postgres table update / Insert
			try {
				long startTime = System.currentTimeMillis();
				long endTime = 0L;
				WriteBackUpdater.incrementActionCountInPostGresTable(solrid, action, numberOfTimes);
				Log.Info("Successfully Incremented Count in PostGres Table");
				endTime = System.currentTimeMillis();
				Log.Info("Updated Increment Data in Postgres, time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime));
			} catch (AppException e) {
				Log.Error("AppException in Incrementing PostGres table for master data." + e);
			} catch (Exception e) {
				Log.Error("Exception in Incrementing PostGres table for master data." + e);
			}
		} catch (SolrServerException e) {
			Log.Error("Exception in getting Solar Server Connection."
					+ e);
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);

		} catch (Exception e) {
			Log.Error("Exception Fetching Search Details." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3,
						e.getCause(), true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		}
		return "{\"message\":\"successfully incremented action in Solr and in Postgres table\"}";
	}
	
	public ArrayList<SmartFindAuditDocument> getSmartFindAuditInformation(Object... obj) throws AppException, SQLException{
		ArrayList<SmartFindAuditDocument> smartFindAuditDocumentList = null;
		try{
			Log.Info("getSmartFindAuditInformation method called on InstrumentSearchService");
			SmartFindAuditDao smartFindAuditDao = new SmartFindAuditDao();
			String id = obj[0].toString();
			smartFindAuditDocumentList = smartFindAuditDao.getSmartFindAuditInformation(id);
			Log.Info("Retrieved Smart Find Audit Information from Postgres DB.");
			return smartFindAuditDocumentList;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while retrieving Smart Find Audit Information from DB " + e);
		}
		//System.out.println("Service" +smartFindAuditDocumentList);
		return smartFindAuditDocumentList;
	}
	
	public ArrayList<SmartFindDocument> getSmartFindBackupInformation(Object... obj) throws AppException, SQLException{
		ArrayList<SmartFindDocument> smartFindDocumentList = null;
		try{
			Log.Info("getSmartFindBackupInformation method called on InstrumentSearchService");
			SmartFindAuditDao smartFindAuditDao = new SmartFindAuditDao();
			String id = obj[0].toString();
			smartFindDocumentList = smartFindAuditDao.getSmartFindBackupInformation(id);
			Log.Info("Retrieved Smart Find Backup Information from Postgres DB.");
			return smartFindDocumentList;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while retrieving Smart Find Backup Information from DB " + e);
		}
		//System.out.println("Service" +smartFindAuditDocumentList);
		return smartFindDocumentList;
	}
	
	public ArrayList<SmartFindDocument> getImageSetList() throws AppException{
		ArrayList<SmartFindDocument> smartFindDocumentList = null;
		try{
			Log.Info("getImageSetList method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			smartFindDocumentList = instrumentSearchDao.getImageSetList();
			Log.Info("Retrieved Smart Find ImageSet List from Postgres DB.");
			return smartFindDocumentList;		
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while retrieving Smart Find ImageSet List from DB " + e);
		}
		//System.out.println("Service" +smartFindAuditDocumentList);
		return smartFindDocumentList;
	}
}
