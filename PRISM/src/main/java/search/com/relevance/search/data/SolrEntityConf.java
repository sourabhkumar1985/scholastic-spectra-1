package com.relevance.search.data;

import java.util.ArrayList;

public class SolrEntityConf 
{
	private String name;
	private String keywords;
	private String source;
	private String dbType;
	private String dbName;
	private String solrCollection;
	private ArrayList<SolrColumnConf> columns;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}


	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getSolrCollection() {
		return solrCollection;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public void setSolrCollection(String solrCollection) {
		this.solrCollection = solrCollection;
	}
	public ArrayList<SolrColumnConf> getColumns() {
		return columns;
	}
	public void setColumns(ArrayList<SolrColumnConf> columns) {
		this.columns = columns;
	}
}
