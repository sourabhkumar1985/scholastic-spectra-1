package com.relevance.search.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;

@Path("/integrawebsearch")
public class IntegraWebResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getinstrumentslist")
	public String getAgileParts(@FormParam("key") String key) throws AppException{
		
		Log.Info("Recieved Request getAgileParts with param : " + key);

		return "{\"message\":\"successfully returned - " +  key + "\"}";
	}
}
