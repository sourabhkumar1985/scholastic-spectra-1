package com.relevance.search.data;

import java.util.List;

public class IntegraAgilePart {

	private int rowId;
	private int level;
	private String description;
	private String partNumber;
	private String parent;
	private String key;	
	private String partType;
	private String rev;
	private String revReleaseDate;
	private String functionalTerm;
	private String bomItmtype;
	private String bomIncorpDate;
	
	public String getPartType() {
		return partType;
	}
	public void setPartType(String partType) {
		this.partType = partType;
	}
	public String getRev() {
		return rev;
	}
	public void setRev(String rev) {
		this.rev = rev;
	}
	public String getRevReleaseDate() {
		return revReleaseDate;
	}
	public void setRevReleaseDate(String revReleaseDate) {
		this.revReleaseDate = revReleaseDate;
	}
	public String getFunctionalTerm() {
		return functionalTerm;
	}
	public void setFunctionalTerm(String functionalTerm) {
		this.functionalTerm = functionalTerm;
	}
	public String getBomItmtype() {
		return bomItmtype;
	}
	public void setBomItmtype(String bomItmtype) {
		this.bomItmtype = bomItmtype;
	}
	public String getBomIncorpDate() {
		return bomIncorpDate;
	}
	public void setBomIncorpDate(String bomIncorpDate) {
		this.bomIncorpDate = bomIncorpDate;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

	private List<IntegraAgilePart> forwardChildren;
	private List<IntegraAgilePart> backwardParent;
	private List<IntegraAgilePart> integraAgileParts;	
	private List<IntegraAgilePart> children;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public int getRowId() {
		return rowId;
	}
	
	public List<IntegraAgilePart> getChildren() {
		return children;
	}
	public void setChildren(List<IntegraAgilePart> children) {
		this.children = children;
	}
	public List<IntegraAgilePart> getForwardChildren() {
		return forwardChildren;
	}
	public void setForwardChildren(List<IntegraAgilePart> forwardChildren) {
		this.forwardChildren = forwardChildren;
	}
	public List<IntegraAgilePart> getBackwardParent() {
		return backwardParent;
	}
	public void setBackwardParent(List<IntegraAgilePart> backwardParent) {
		this.backwardParent = backwardParent;
	}
	public List<IntegraAgilePart> getIntegraAgileParts() {
		return integraAgileParts;
	}
	public void setIntegraAgileParts(List<IntegraAgilePart> integraAgileParts) {
		this.integraAgileParts = integraAgileParts;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
