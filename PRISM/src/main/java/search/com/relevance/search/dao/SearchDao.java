package com.relevance.search.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.dao.E2emfDao;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.IntegraAgilePart;
/**
 * 
 * E2emf Search Data Access Object  
 *
 */
public class SearchDao extends BaseDao implements E2emfDao {
	
    //static Connection con = null;
    //static Statement stmt = null;
	
	 Connection con = null;
     Statement stmt = null;
    
    
    @Override
	public E2emfBusinessObject getBusinessObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfBusinessObject getBusinessObject(Object... obj) {
	
		return null;
	}

	@Override
	public List<FlowData> getFlowDataList(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeQuery(String query) throws AppException {
		
		Log.Info("ExecuteQuery called with Query \n " + query);
		E2emfView e2emfView = null;
		String nodeLevelJson = "";
		Gson gson = new Gson();
		try{
			//if(con == null)
				con = dbUtil.getHiveConnection (E2emfConstants.e2emfDB);		
			
			if(con != null){
				
				//if(stmt == null)
				   stmt = con.createStatement();
				
			    ResultSet rs = stmt.executeQuery(query);
			    Log.Info("Resultset fetched with size " + rs.getFetchSize());
			    
			    if(rs != null ){
			    	
			    	//e2emfView = dbUtil.convert(rs);
			    	nodeLevelJson = gson.toJson(e2emfView);	
			    }
			    	
			    } else {
			    	Log.Info("Connection object is null");
			    }
			}catch(SQLException sql){
				Log.Error("SQLException executing Query." + sql);
				throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
			}catch(Exception e){
				Log.Error("Exception in executing Query." + e);
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
			}finally{
				if(con != null)
					try {
						con.close();
						//stmt.close();
						//con = null;
						//stmt = null;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						Log.Error(e);
						throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
					}
					
			 }
					
		
		return nodeLevelJson;
	}


	@Override
	public String executeQuery(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public E2emfView getLevel2ViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public E2emfView getLevel3ViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public E2emfView getDefaultViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	public GoodsHistoryList getGoodsHistory(Object... obj){
		return null;
	}


	@Override
	public MaterialReportList getMaterialReport(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}


	/*@Override
	public E2emfMaterialFlowData getE2emfMaterialFlowData(String source)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}*/


	@Override
	public E2emfBusinessObject getActions(Object[] obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public E2emfBusinessObject getRoles(Object[] obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public E2emfBusinessObject getPrimaryRoles(Object[] obj)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<String> getSecondarySearchAppRoles(String database, String query, String userName)  throws Exception {
		StringBuilder queryBuilder = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<String> result = null;
		try{
			result = new ArrayList<>();
			con = dbUtil.getPostGresConnection(database);
			queryBuilder.append(query);
			queryBuilder.append(" where username = ?;");
			stmt = con.prepareStatement(queryBuilder.toString());
			stmt.setString(1, userName);
			rs = stmt.executeQuery();
			if(rs.next()) {
				do{
					result.add(rs.getString(1));
				} while(rs.next());
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
	
	/**
	 * 
	 * @param part
	 * @return
	 * @throws Exception
	 */
	public IntegraAgilePart getAgileParts(IntegraAgilePart part) throws SQLException, AppException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		con = null;
		String baseQuery = "";	
		try{
			con = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
			baseQuery = "select parent, materialnumber, description from integra_bom_oracle";
			if(part.getDescription() != null && !part.getDescription().trim().equalsIgnoreCase("")){
				ps = con.prepareStatement(baseQuery + " where description = ?;");
				ps.setString(1, part.getDescription());
			} else if(part.getRowId() != 0) {
				ps = con.prepareStatement(baseQuery + " where rowid = ?;");
				ps.setInt(1, part.getRowId());
			} else if(part.getPartNumber() != null && !part.getPartNumber().trim().equalsIgnoreCase("")){
				ps = con.prepareStatement(baseQuery + " where materialnumber = ?;");
				ps.setString(1, part.getPartNumber());
			} else {
				ps = con.prepareStatement(baseQuery);
			}
			if(ps != null) {
				rs = ps.executeQuery();
					if(rs.next()) {
						List<IntegraAgilePart> parts = new ArrayList<>();
						IntegraAgilePart agilePart;
						do{							
							agilePart = new IntegraAgilePart();
							agilePart.setParent(rs.getString(1));
//							agilePart.set(rs.getString(2));
							agilePart.setKey(rs.getString(2));
							agilePart.setDescription(rs.getString(3));							
//							agilePart.setKey(rs.getString(3));
//							agilePart.setDescription(rs.getString(4));
//							agilePart.setRowId(rs.getInt(5));
//							agilePart.setPartType(rs.getString(6));
//							agilePart.setRev(rs.getString(7));
//							agilePart.setRevReleaseDate(rs.getString(8));
//							agilePart.setFunctionalTerm(rs.getString(9));
//							agilePart.setBomIncorpDate(rs.getString(10));
							parts.add(agilePart);							
						}while(rs.next());
						part.setIntegraAgileParts(parts);
					}
			}
			
			
		}catch(Exception ex) {
			Log.Error(ex);
		}finally{
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			con.close();
		}
		return part;
	}
	
	public boolean deleteSearchLog(int id) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean response = false;
		PreparedStatement ps = null;
		con = null;
		StringBuilder baseQuery;
		try{
			baseQuery = new StringBuilder("DELETE from search_log where id = ?");
			con = dbUtil.getPostGresConnection();
			ps = con.prepareStatement(baseQuery.toString());
			ps.setInt(1, id);
			int res = ps.executeUpdate();
			response = (res > 0) ? true : false;
		}catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}finally{			
			if (ps != null) {
				ps.close();
			}
			if(con != null) {
				con.close();
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
