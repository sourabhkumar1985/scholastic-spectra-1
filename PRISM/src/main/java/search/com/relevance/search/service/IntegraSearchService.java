package com.relevance.search.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.service.BaseService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.dao.InstrumentSearchDao;
import com.relevance.search.dao.IntegraSearchDao;
import com.relevance.search.dao.SearchDao;
import com.relevance.search.data.BOM;
import com.relevance.search.data.IntegraAgilePart;

/**
 * @author E2emf Search Service
 * 
 */
public class IntegraSearchService extends BaseService {

	// E2emfAppUtil appUtil;
	PrismDbUtil dbUtil;

	static String solarUrl = "SOLR_SERVER_URL";
	static String solarInstanceName = "SOLR_SERVER_INSTANCE_NAME";

	static String solarServerInsanceUrl = null;

	private String searchType;

	public IntegraSearchService() {

		// appUtil = new E2emfAppUtil();
		dbUtil = new PrismDbUtil();
	}

	public String getJsonObject() {
		return null;
	}

	public String getJsonObject(Object... obj) throws AppException {
		String searchResultJson = null;
		HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;
		searchType = E2emfAppUtil.getAppProperty(E2emfConstants.e2emfSearchType);

		SearchParam searchParam = null;
		String searchCriteria = null;
		boolean searchOutputColumns = true;
		// String obfuscateFlag = null;
		try {
			if (obj != null && obj.length > 1) {
				searchParam = (SearchParam) obj[0];
				searchCriteria = ((String) obj[1]).trim();

				if (obj.length >= 3 && obj[2] != null) {
					searchOutputColumns = Boolean.parseBoolean((String) obj[2]);
					/*
					 * if (obj[3] != null) obfuscateFlag = (String) obj[3];
					 */
				}

				Log.Info("Searching initiated for " + searchCriteria);
			} /*
				 * else { searchCriteria = (String) obj[0]; Log.Info(
				 * "Searching initiated for " + searchCriteria); }
				 */

			StringBuilder searchStr = new StringBuilder();
			if (searchParam.getKey() != null
					&& searchParam.getKey().toString().equalsIgnoreCase("")) {
				searchStr.append("*");
			} else if ("1".equalsIgnoreCase(searchType)) {
				searchStr.append("rawdata:*");
				searchStr.append(searchParam.getKey().replaceAll(" ", "*"));
				searchStr.append("*");
			} else if (searchOutputColumns) {
				// Replace Special character like - with "+" with AND condition
				String searchWords = searchParam.getKey();
				searchWords = searchWords.replaceAll("-", "+");

				// parameters.set("q", "output:*" +
				// searchParam.getKey().replaceAll(" ", "* OR output:*") + "*");
				searchStr.append("output:");
				searchStr.append("*");
				searchStr.append(searchWords.replaceAll(" ", "* OR output:*"));
				searchStr.append("*");
				searchStr = new StringBuilder(
						searchStr.toString().replaceAll("\\+", "* AND output:*"));
			} else {
				// parameters.set("q", "rawdata:*" +
				// searchParam.getKey().replaceAll(" ", "* OR rawdata:*") +
				// "*");//Search only in all data column
				// commented for QFIND Search Configuration
				/*
				 * searchStr.append("rawdata:"); searchStr.append("\"");
				 * searchStr.append(searchParam.getKey().replaceAll(" ",
				 * "\" OR rawdata:\"")); searchStr.append("\""); searchStr = new
				 * StringBuilder(searchStr.toString().replaceAll( "\\+",
				 * "\" AND rawdata:\""));
				 */
				searchStr.append(searchParam.getKey());
			}

			if (searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService")
					&& searchParam.getFacet() == null) {

				solarServer = getSolarServerConnection(searchParam.getSolrCollectionName());

				ModifiableSolrParams parameters = new ModifiableSolrParams();
				// parameters.set("q", "rawdata:*" +
				// searchParam.getKey().replaceAll(" ", "*") + "*");//Search
				// only in all data column
				if (searchParam.getType() != null && searchParam.getType().equalsIgnoreCase("or")) {
					parameters.set("q", searchStr.toString().trim());
				} else if (searchStr.toString().trim().length() > 1) {
					if (searchStr.toString().contains("[")) {
						String startString = searchStr.toString().trim().split("\\[")[0];
						String endString = searchStr.toString().trim().split("\\[")[1];
						String finalString = "\""
								+ startString.trim().replaceAll("\\s+", "\" AND \"") + "\""
								+ " AND [" + endString;
						parameters.set("q", finalString);
					} else {
						parameters.set("q",
								"\"" + searchStr.toString().trim().replaceAll("\\s+", "\" AND \"")
										+ "\"");
					}
				}
				parameters.set("defType", "edismax");
				// parameters.set("wt", "xml");//Not required since converting
				// from the object directly
				// parameters.set("start", "0");
				// parameters.set("rows", "0");
				if (searchParam.getType() != null && "similar".equalsIgnoreCase(searchParam.getType())){
					parameters.set("qf","catalogdesc3");
				}else{
					if(searchParam.getApp().equals(E2emfConstants.integraApp)){
						parameters.set("qf",
								"id catalognumber1^40 catalognumber2 catalogdesc1^20 catalogdesc2 catalogdesc6 catalogdesc7 catalogdesc4 catalogdesc5 component_sku_number^40 title category subcategory components");
					} else {
						parameters.set("qf", "section2 content"); 
					}
					
				}
				
				parameters.set("facet", "true");
				// parameters.set("facet.field", "facet");
				// parameters.set("facet.sort", "rank+asc");
				
				parameters.set("indent", "true");
				
				parameters.set("wt", "json");
				
				
				
				
				// parameters.set("fq", "facet:"+queryFilterVal);
				if(searchParam.getApp().equals(E2emfConstants.integraApp)){
					//parameters.set("fq", searchParam.getSecondaryRoles());
					parameters.set("sort", "score desc,ranksrc asc");
					parameters.set("lowercaseOperators", "true");
					parameters.set("hl.simple.pre", "<em>");
					parameters.set("hl.fl", "catalogdesc1");
					parameters.set("hl.simple.post", "</em>");
					parameters.set("stopwords", "true");
					parameters.set("q.alt", "*:*");
					parameters.set("hl", "true");
					parameters.set("facet.field", "datasource");
					String fq = searchParam.getSecondaryRoles();
					if(searchParam.getKey() != null && searchParam.getKey().trim().equalsIgnoreCase("")) {
						String e = "manufacturer:\"JARIT\"";							
						parameters.set("fq", new String[]{fq, e});
					}else {
						parameters.set("fq", fq);
					}
				} else {
					parameters.set("facet.field", "section");
				}
				try {
					if (solarServer != null) {
						queryResponse = solarServer.query(parameters);
						System.out.println("Search Parameters : " + parameters.toString());
						Log.Info("Recieved response from solar successfully ");
					} else {
						Log.Error("Unable to connect to solarServer...");
					}

					Gson gson = new Gson();
					java.util.LinkedHashMap<String, String> facetMap = new java.util.LinkedHashMap<String, String>();

					// First for loop is for fieldname as "facet"
					if (queryResponse != null && queryResponse.getFacetFields() != null) {
						for (FacetField facetField : queryResponse.getFacetFields()) {
							for (Count facetFieldCount : facetField.getValues()) {
								facetMap.put(facetFieldCount.getName(),
										"" + facetFieldCount.getCount());
							}
						}
					}
					String json = gson.toJson(facetMap);
					searchResultJson = json;
				} catch (SolrServerException e) {
					Log.Error("Exception in getting Solar Server Connection." + e);
					throw new AppException(e.getMessage(), "Error while processing the request.", 2,
							e.getCause(), true);

				} catch (Exception e) {
					Log.Error("Exception Fetching JSON Data." + e);
					if (e instanceof NullPointerException) {
						throw new AppException("NullPointerException",
								"No Indexed Data Available in Collection.", 3, e.getCause(), true);
					}
					throw new AppException(e.getMessage(), "Error while processing the request.", 2,
							e.getCause(), true);
				}

			}
			// To implement detailed search
			else if (searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService")
					&& searchParam.getFacet() != null) {

				solarServer = getSolarServerConnection(searchParam.getSolrCollectionName());

				ModifiableSolrParams parameters = new ModifiableSolrParams();
				// parameters.set("q", "rawdata:*" +
				// searchParam.getKey().replaceAll(" ", "*") + "*");
				if (searchParam.getType() != null && searchParam.getType().equalsIgnoreCase("or")) {
					parameters.set("q", searchStr.toString().trim());
				} else if (searchStr.toString().trim().length() > 1) {
					if (searchStr.toString().contains("[")) {
						String startString = searchStr.toString().trim().split("\\[")[0];
						String endString = searchStr.toString().trim().split("\\[")[1];

						String finalString = "\""
								+ startString.trim().replaceAll("\\s+", "\" AND \"") + "\""
								+ " AND [" + endString;
						parameters.set("q", finalString);					
					} else {
						if(searchParam.getApp().equals(E2emfConstants.integraApp)){
							parameters.set("q",
									"\"" + searchStr.toString().trim().replaceAll("\\s+", "\" AND \"")
											+ "\"");
						} else {
							parameters.set("q",
									"\"" + searchStr.toString().trim().replaceAll("\\s+", "\" AND \"")
											+ "\"");
							//parameters.set("q", searchStr.toString().trim().replaceAll("\\s+", "+"));
						}
						
					}
				} else {
					if(searchParam.getKey().equalsIgnoreCase("")) {
						parameters.set("q", "*");	
					}
				}
				/*
				 * if(searchStr.toString().trim().length() > 1){
				 * parameters.set("q","\"" +
				 * searchStr.toString().trim().replaceAll("\\s+", "\" AND \""
				 * )+"\""); parameters.set("q", searchStr.toString().trim()); }
				 */
				parameters.set("defType", "edismax");
				// parameters.set("wt", "xml");//Not required since converting
				// from the object directly
				// parameters.set("start", "0");
				// parameters.set("rows", "0");				
				if (searchParam.getType() != null && "similar".equalsIgnoreCase(searchParam.getType())){
					parameters.set("qf","catalogdesc3");
				}else if(searchParam.getType() != null && "detailed".equalsIgnoreCase(searchParam.getType())){
					parameters.set("q", searchStr.toString());
					parameters.set("qf","id");
				}else{
					if(searchParam.getApp().equals(E2emfConstants.integraApp)){
						parameters.set("qf",
								"id catalognumber1^40 catalognumber2 catalogdesc1^20 catalogdesc2 catalogdesc6 catalogdesc7 catalogdesc4 catalogdesc5 component_sku_number^40 title category subcategory components");
					}else {
						parameters.set("qf", "section2 content");
					}
					
				}
				// parameters.set("facet.field", "facet");
				// parameters.set("facet.sort", "rank+asc");
				
				parameters.set("indent", "true");
				
				parameters.set("wt", "json");
				
				// parameters.set("fq", "facet:"+queryFilterVal);
				if (searchParam.getFacet().equalsIgnoreCase("all")) {
					// parameters.set("fq", "datasource:" +
					// searchParam.getFacet());
					if(searchParam.getApp().equals(E2emfConstants.integraApp)){
						parameters.set("sort", "score desc,ranksrc asc");
						parameters.set("lowercaseOperators", "true");
						parameters.set("facet.field", "datasource");
						parameters.set("hl", "true");
						parameters.set("hl.simple.post", "</em>");
						parameters.set("stopwords", "true");
						parameters.set("q.alt", "*:*");
						parameters.set("hl.simple.pre", "<em>");
						parameters.set("hl.fl", "catalogdesc1");
						parameters.set("start", searchParam.getStart());
						parameters.set("rows", searchParam.getRows());
						
						String fq = searchParam.getSecondaryRoles();
						if(searchParam.getKey() != null && searchParam.getKey().trim().equalsIgnoreCase("")) {
							String e = "manufacturer:\"JARIT\"";							
							parameters.set("fq", new String[]{fq, e});
						}else {
							parameters.set("fq", fq);
						}						
						System.out.println("FQ :" + fq);
					}
				} else {
					if(searchParam.getApp().equals(E2emfConstants.integraApp)){
						parameters.set("sort", "score desc,ranksrc asc");
						parameters.set("lowercaseOperators", "true");
						parameters.set("facet.field", "datasource");
						parameters.set("hl", "true");
						parameters.set("hl.simple.post", "</em>");
						parameters.set("stopwords", "true");
						parameters.set("q.alt", "*:*");
						parameters.set("hl.simple.pre", "<em>");
						parameters.set("hl.fl", "catalogdesc1");
						parameters.set("start", searchParam.getStart());
						parameters.set("rows", searchParam.getRows());
						
						String[] dataSources = searchParam.getSecondaryRoles().split("OR");
						for (String dataSource : dataSources) {
							if (dataSource.contains(searchParam.getFacet())) {
								String fq = dataSource;
								if(searchParam.getKey() != null && searchParam.getKey().trim().equalsIgnoreCase("")) {
									String e = "manufacturer:\"JARIT\"";							
									parameters.set("fq", new String[]{fq, e});
								}else {
									parameters.set("fq", fq);
								}									
								System.out.println("FQ :" + fq);
								break;
							}
						}
					} else {
						parameters.set("hl", "true");
						parameters.set("hl.simple.post", "</em>");
						parameters.set("hl.simple.pre", "<em>");
						parameters.set("facet.field", "section");
					}
				}

				// new collection type settings
				if ("1".equalsIgnoreCase(searchType)) {
					parameters.set("group", "true");
					parameters.set("group.field", "key");
					parameters.set("group.main", "true");
				}

				try {
					if (solarServer != null) {
						queryResponse = solarServer.query(parameters);
						System.out.println("Search Parameters : " + parameters.toString());
						Log.Info("Recieved response from solar successfully ");
					} else {
						Log.Error("Unable to connect to solarServer...");
					}

					Gson gson = new Gson();
					String json = gson.toJson(queryResponse.getResults());
					final JSONArray jsonArray = new JSONArray(json);
					final JSONArray array = new JSONArray();
					final int length = jsonArray.length();
					final String titleAttr = (searchParam.getApp().equals(E2emfConstants.integraApp)) ? "title" : "section";
					for (int i = 0; i < length; i++) {
						final JSONObject jsonObj = (JSONObject) jsonArray.get(i);
						final String title = getDisplayTitle(jsonObj.getString(titleAttr),
								searchStr.toString().trim());
						jsonObj.put(titleAttr, title);
						array.put(jsonObj);
					}
					searchResultJson = array.toString();
					// searchResultJson = parseJSONStr(json);
					// searchResultJson = json;
					// System.out.println("parsedJSONOutPut:" +
					// searchResultJson);
					// Log.Info("parsedJSONOutPut:" +searchResultJson);

				} catch (SolrServerException e) {
					Log.Error("Exception in getting Solar Server Connection." + e);
					throw new AppException(e.getMessage(), "Error while processing the request.", 2,
							e.getCause(), true);

				} catch (Exception e) {
					Log.Error("Exception Fetching Search Details." + e);
					if (e instanceof NullPointerException) {
						throw new AppException("NullPointerException",
								"No Indexed Data Available in Collection.", 3, e.getCause(), true);
					}
					throw new AppException(e.getMessage(), "Error while processing the request.", 2,
							e.getCause(), true);
				}
			}
		} catch (Exception e) {
			Log.Error("Exception Fetching JSON Data." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3, e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2,
					e.getCause(), true);
		}
		return searchResultJson;

	}

	private String parseJSONStr(String jsonStr) throws AppException {

		org.json.JSONArray jsonArr = null;
		String outputJSONStr = "";
		// List<String> outputList = new ArrayList<String>();

		try {

			jsonArr = new org.json.JSONArray(jsonStr);
			int length = jsonArr.length();
			// System.out.println("length:" + length);
			outputJSONStr = "[";

			for (int i = 0; i < length; i++) {
				JSONObject jsonObj = (JSONObject) jsonArr.get(i);
				outputJSONStr = outputJSONStr.concat("{\"key\":\"")
						.concat(jsonObj.getString("id").concat("\","));
				String output = jsonObj.getString("output");
				outputJSONStr = outputJSONStr.concat(output.substring(1));
				if (i != length - 1) {
					outputJSONStr = outputJSONStr.concat(",");
				}
			}
			outputJSONStr = outputJSONStr.concat("]");
		} catch (JSONException e) {
			Log.Error(e);
		} catch (Exception e) {
			Log.Error("Exception parsing the JSON String." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3, e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2,
					e.getCause(), true);
		}
		return outputJSONStr;
	}

	public String getSearchSuggestion(Object... obj) throws AppException {
		String searchResultJson = null;
		HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;

		SearchParam searchParam = null;
		if (obj != null && obj.length >= 1) {
			searchParam = (SearchParam) obj[0];
		}

		ModifiableSolrParams parameters = new ModifiableSolrParams();
		parameters.set("spellcheck.q", searchParam.getKey());
		parameters.set("defType", "edismax");
		parameters.set("qt", "/suggest");

		solarServer = getSolarServerConnection(searchParam.getSolrCollectionName());
		try {
			if (solarServer != null) {
				queryResponse = solarServer.query(parameters);
				Log.Info("Recieved response from solar successfully ");
			} else {
				Log.Error("Unable to connect to solarServer...");
			}
			Gson gson = new Gson();
			String json = gson.toJson(queryResponse.getSpellCheckResponse().getSuggestions());
			searchResultJson = json;
		} catch (SolrServerException e) {
			Log.Error("Exception in getting Solar Server Connection." + e);
			throw new AppException(e.getMessage(), "Error while processing the request.", 2,
					e.getCause(), true);

		} catch (Exception e) {
			Log.Error("Exception Fetching JSON Data." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3, e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2,
					e.getCause(), true);
		}
		return searchResultJson;
	}

	private HttpSolrServer getSolarServerConnection(String solrcollectionname) throws AppException {
		HttpSolrServer serverCon = null;
		String solrCollectionUrl = E2emfAppUtil.getAppProperty(solrcollectionname);
		/*
		 * String solarInstanceName= null; if(solarServerInsanceUrl == null){
		 */
		// solarUrl = appUtil.getAppProperty(E2emfConstants.solarUrl);
		/*
		 * if (Source.equalsIgnoreCase("sap")) solarInstanceName =
		 * E2emfConstants.solarSAPInstanceName; else if
		 * (Source.equalsIgnoreCase("jde")) solarInstanceName =
		 * E2emfConstants.solarJDEInstanceName; else if
		 * (Source.equalsIgnoreCase("gp")) solarInstanceName =
		 * E2emfConstants.solarGPInstanceName; else if
		 * (Source.equalsIgnoreCase("apac")) solarInstanceName =
		 * E2emfConstants.solarAPACInstanceName; else if
		 * (Source.equalsIgnoreCase("bcs")) solarInstanceName =
		 * E2emfConstants.solarBCSInstanceName; if(solarInstanceName!= null) {
		 * solarInstanceName = E2emfAppUtil.getAppProperty(solarInstanceName); }
		 */
		// solarServerInsanceUrl = solarUrl+solarInstanceName+"/";
		/* } */

		try {
			serverCon = new HttpSolrServer(solrCollectionUrl);
		} catch (Exception e) {
			Log.Error("Exception connecting to solr server." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						"No Indexed Data Available in Collection.", 3, e.getCause(), true);
			}
			throw new AppException(e.getMessage(), "Error while processing the request.", 2,
					e.getCause(), true);
		}

		return serverCon;
	}

	public String getDisplayTitle(final String title, final String queryString) {
		String outputString = "";
		if (title != null && !title.isEmpty() && queryString != null && !queryString.isEmpty()) {
			final String inputText = title.toLowerCase();
			final String inputQueryString = queryString.toLowerCase();
			if (inputText.contains(inputQueryString)) {
				final int extract = 100;
				final int inputLength = title.length();
				final int queryLength = queryString.length();
				final int queryIndex = inputText.indexOf(inputQueryString);
				final int postStart = queryIndex + queryLength;
				final int postEnd = postStart + extract;
				final int preStart = queryIndex - extract;
				final int preEnd = queryIndex;
				String preString = null;
				String postString = null;
				if (queryIndex >= extract) {
					preString = title.substring(preStart, preEnd);
				} else {
					preString = title.substring(0, queryIndex);
				}
				if ((inputLength - queryLength > extract) && (title.length() > postEnd)) {
					postString = title.substring(postStart, postEnd);
				} else {
					postString = title.substring(postStart, inputLength);
				}
				outputString = preString.concat(queryString).concat(postString).concat(".....");
				System.out.println(outputString);

			} else {
				System.out.println("No Matching Found...");
			}
		} else {
			System.out.println("Title and the query string cannot be null or empty...");
		}
		return outputString;
	}
	
	public String getAgile(IntegraAgilePart part) throws AppException, Exception {		
		SearchDao dao;
		String result = "";
		try {		
			dao = new SearchDao();
			part = dao.getAgileParts(part);
			
			Gson gson = new Gson();			
			String json = gson.toJson(part);
			result = json;
			
		} catch (Exception e) {
			result = PrismHandler.handleException(e, true);
		}		
		return result;
	}
	
	public String generateJsonString(Object... obj) {
		return null;
	}

	
	public String getActions(Object... arg0) throws AppException {
		return null;
	}

	
	public E2emfBusinessObject getBusinessObject() throws AppException {
		return null;
	}

	
	public E2emfBusinessObject getBusinessObject(Object... arg0) throws AppException {
		return null;
	}

	
	public GoodsHistoryList getGoodsHistoryViewObject(Object... arg0) throws AppException {
		return null;
	}

	
	public MaterialReportList getMaterialReportViewObject(Object... arg0) throws AppException {
		return null;
	}

	
	public String getPrimaryRoles(Object... arg0) throws AppException {
		return null;
	}

	
	public String getRoles(Object... arg0) throws AppException {
		return null;
	}

	
	public E2emfView getViewObject(Object... arg0) throws AppException {
		return null;
	}

	
	public E2emfView getViewObject(E2emfBusinessObject arg0) {
		return null;
	}

	public List<SearchLog> getSearchHistory(SearchLog searchQuery) throws AppException{
		List<SearchLog> searchHisory = new ArrayList<SearchLog>();
		try{
			Log.Info("getSearchHistory method called on InstrumentSearchService");
			InstrumentSearchDao instrumentSearchDao = new InstrumentSearchDao();
			searchHisory = instrumentSearchDao.getSearchHistory(searchQuery, null, null);
			return searchHisory;
		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while fetching SearchHistory " + e);
		}
		return searchHisory;
	}
	public BOM getBOM(String key, String tableName, String source, String dataBase) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<IntegraAgilePart> skuList = null;
		BOM bom = null;
		try {
			bom = new BOM();
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			skuList = integraSearchDao.getBOM(key,"materialnumber","forward", tableName, source, dataBase);
			if(skuList != null && !skuList.isEmpty()) {				
				bom.setBackward(skuList.get(0));
			}
			skuList = integraSearchDao.getBOM(key,"materialnumber","backward", tableName, source, dataBase);
			if(skuList != null && !skuList.isEmpty()) {
				bom.setForward(skuList.get(0));				
			}			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return bom;
	}
	
	public List<String> getBOMIds() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> skuList = null;
		try {
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			skuList = integraSearchDao.getBOMIds();
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return skuList;
	}

	public boolean UpdateFileStructure(String tableName, String dbName,
			String columnName, String primaryKeys, String value,
			String primaryKeyColumnName, String keyValueMap) throws Exception {
		// TODO Auto-generated method stub
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean keyUpdate = false;
		try {
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			keyUpdate = integraSearchDao.updateFileStructure(tableName, dbName, columnName, primaryKeys, value, primaryKeyColumnName, keyValueMap);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return keyUpdate;
	}
	
	
	public boolean genericUpdate(String query, String source,String dataBase, List<String> queries) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean keyUpdate = false;
		try {
			if(queries != null && !queries.isEmpty()) {
				query = queries.get(0);
				queries.remove(0);
			}
			if(query == null && "".equalsIgnoreCase(query)) {
				throw new Exception("Query not present");
			}
			query = new String(DatatypeConverter.parseBase64Binary(query), "UTF-8");
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			keyUpdate = integraSearchDao.genericUpdate(query, source, dataBase);
			if(queries != null && !queries.isEmpty()){
				genericUpdate(query, source, dataBase, queries);
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return keyUpdate;
	}
	
	public List<String> getIntegraCategories(String source,String dbName, String tableName, String columnName, String regEx, boolean _union) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> categories = null;
		try{
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			dbName = (dbName == null || dbName.equalsIgnoreCase("")) ? "QFIND" : dbName;
			tableName = (tableName == null || tableName.equalsIgnoreCase("")) ? "solrmaster_catalog_base" : tableName;
			columnName = (columnName == null || columnName.equalsIgnoreCase("")) ? "category" : columnName;
			categories = integraSearchDao.getIntegraCategories(source, dbName, tableName, columnName, regEx, _union);
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return categories;
	}
	
	public HashMap<String, String> getIntegraBrands(String dbName, String tableName) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, String> categories = null;
		try{
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			dbName = (dbName == null || dbName.equalsIgnoreCase("")) ? "QFIND" : dbName;
			tableName = (tableName == null || tableName.equalsIgnoreCase("")) ? "integra_brands" : tableName;
			categories = integraSearchDao.getIntegraBrands(dbName, tableName);
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return categories;
	}
	
	public HashMap<String, List<String>> getMasterData(String key, String dbName) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, List<String>> masterDatas = null;
		try{
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			masterDatas = integraSearchDao.getMasterData(key, dbName);
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return masterDatas;
	}
	
	public HashMap<String, List<String>> getMasterDataDetails(String key, String dbName) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, List<String>> masterDatas = null;
		try{
			IntegraSearchDao integraSearchDao = new IntegraSearchDao();
			masterDatas = integraSearchDao.getMasterDataDetails(key, dbName);
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return masterDatas;
	}
}
