package com.relevance.search.data;

import org.apache.solr.common.SolrDocumentList;
import java.util.Map;
import java.util.List;

public class Search {

	private String inputQuery;
	private String solrQuery;
	private Map<String, Map<String,List<String>>> highlighting;
	
	public Map<String, Map<String, List<String>>> getHighlighting() {
		return highlighting;
	}
	public void setHighlighting(Map<String, Map<String, List<String>>> highlighting) {
		this.highlighting = highlighting;
	}
	public String getInputQuery() {
		return inputQuery;
	}
	public void setInputQuery(String inputQuery) {
		this.inputQuery = inputQuery;
	}
	public String getSolrQuery() {
		return solrQuery;
	}
	public void setSolrQuery(String solrQuery) {
		this.solrQuery = solrQuery;
	}
	
	
	public SolrDocumentList result;
	public SolrDocumentList getResult() {
		return result;
	}
	public void setResult(SolrDocumentList result) {
		this.result = result;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public long totalCount;
}
