package com.rl.cms.refresh;

import java.util.List;

public class Documents {

	private List<FileJSONData> docs;

	public List<FileJSONData> getDocs() {
		return docs;
	}

	public void setDocs(List<FileJSONData> docs) {
		this.docs = docs;
	}
	
	
}
