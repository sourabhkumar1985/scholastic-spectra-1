package com.rl.cms.refresh;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.sql.Date;

public class Meta {

	private String timestamp;
	private String timestamp_as_date;
	
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
		this.timestamp_as_date = convertTime(Long.parseLong(timestamp));
	}
	
	public String getTimestamp_as_date() {
		return timestamp_as_date;
	}

	public void setTimestamp_as_date(String timestamp_as_date) {
		this.timestamp_as_date = timestamp_as_date;
	}

	public String convertTime(long time){
	    Date date = new Date(time);
	    Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
	    return format.format(date);
	}
	
}
