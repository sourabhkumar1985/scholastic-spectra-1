package com.rl.cms.refresh;

import java.sql.Timestamp;

public class CMSAction {
	public CMSAction(){
		
	}
	private String action;
	private Timestamp date;
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	
	
}
