package com.rl.cms.refresh;

import com.rl.cms.refresh.File;

public class Relationships {
	private File file;
	private Thumbnail thumbnail;
	
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public Thumbnail getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}	
}
