package com.relevance.prism.data;

import java.util.List;

public class ProfileDetails {
	private String config;
	String dataQuery;
	private List<ProfileView> views;
	private EmailNotification emailNotification;
	private List<ProfileAction> profileActions;
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public String getDataQuery() {
		return dataQuery;
	}
	public void setDataQuery(String dataQuery) {
		this.dataQuery = dataQuery;
	}
	public List<ProfileView> getViews() {
		return views;
	}
	public void setViews(List<ProfileView> views) {
		this.views = views;
	}
	public EmailNotification getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}
	public List<ProfileAction> getProfileActions() {
		return profileActions;
	}
	public void setProfileActions(List<ProfileAction> profileActions) {
		this.profileActions = profileActions;
	}
}
