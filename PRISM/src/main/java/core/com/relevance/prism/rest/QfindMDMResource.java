package com.relevance.prism.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.dao.QfindMDMDao;
import com.relevance.prism.data.Attribute;
import com.relevance.prism.data.Audit;
import com.relevance.prism.data.MasterData;
import com.relevance.prism.data.QfindAuditData;
import com.relevance.prism.service.QfindMDMService;
import com.relevance.prism.service.UserService;
import com.relevance.prism.util.PrismHandler;


@Path("/qfindmdm")
public class QfindMDMResource extends BaseResource{
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getallattributes")
	public String getAllAttributes() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Attribute> response = (new QfindMDMService()).getAllattribute();
		Gson gson = new Gson();
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gson.toJson(response);
		
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getsearchedattribute")
	public String getAttribute(@FormParam("attributename") String attriutename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Attribute response = null;
		response = (new QfindMDMService()).getAttribute(attriutename);
		Gson gson = new Gson();
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gson.toJson(response);
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addattribute")
	public String addAttribute(@FormParam("attributename") String attriutename,@FormParam("rulename") String rulename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		response = (new QfindMDMService()).addAttribute(attriutename,rulename);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateattribute")
	public String updateAttribute(@FormParam("attributename") String attributename,@FormParam("rulename") String rulename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		response = (new QfindMDMService()).updateAttribute(attributename,rulename);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteattribute")
	public String deleteAttribute(@FormParam("attributename") String attributename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		response = (new QfindMDMService()).deleteAttribute(attributename);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getattributeslist")
	public String getAllItemNumbers()
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<MasterData> response = null;
		String res = null;
		try {
			response = (new QfindMDMService()).getAllAttributeList();
			Gson gson = new Gson();
			res = gson.toJson(response);
		} catch (Exception ex) {
			res = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return res;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addAttributeLog")
	public String addLogData(@FormParam("item_number")String item_number, @FormParam("attributename")String attributename,@FormParam("new_value") String new_value,@Context HttpServletRequest req)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			response = (new QfindMDMService()).addAttributeLog(item_number, attributename, new_value, "abc");
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}


	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getlogdata")
	public String getLogData(@FormParam("item_number")String item_number, @FormParam("attributename")String attributename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Audit> response = null;
		String res = null;
		try {
			response = (new QfindMDMService()).getLogDetails(item_number, attributename);
			Gson gson = new Gson();
			res = gson.toJson(response);
		} catch (Exception ex) {
			res = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return res;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getauditdata")
	public String getAllAuditData(@FormParam("itemnumberlist")String itemnumberlist)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<MasterData> response = null;
		String res = null;
		try {
			response = (new QfindMDMService()).getAuditData(itemnumberlist);
			Gson gson = new Gson();
			res = gson.toJson(response);
		} catch (Exception ex) {
			res = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return res;
	}
	
}
