package com.relevance.prism.data;

import java.util.ArrayList;

public class PrimaryKey {

	private ArrayList<String> primaryKeyNamesList;

	public ArrayList<String> getPrimaryKeyNamesList() {
		return primaryKeyNamesList;
	}

	public void setPrimaryKeyNamesList(ArrayList<String> primaryKeyNamesList) {
		this.primaryKeyNamesList = primaryKeyNamesList;
	}	
	
}
