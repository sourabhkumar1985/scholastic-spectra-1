package com.relevance.prism.data;

import java.util.ArrayList;

public class FieldAndDescriptionList {
	private ArrayList<FieldAndDescription> fieldAndDescriptionList;

	public ArrayList<FieldAndDescription> getFieldAndDescriptionList() {
		return fieldAndDescriptionList;
	}

	public void setFieldAndDescriptionList(
			ArrayList<FieldAndDescription> fieldAndDescriptionList) {
		this.fieldAndDescriptionList = fieldAndDescriptionList;
	}
}