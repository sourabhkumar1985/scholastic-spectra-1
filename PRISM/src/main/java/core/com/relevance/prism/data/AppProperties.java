package com.relevance.prism.data;

import java.util.HashMap;

public class AppProperties 
{
	private HashMap<String, AppProperty> prop;
	
	public HashMap<String, AppProperty> getProp() {
		return prop;
	}

	public void setProp(HashMap<String, AppProperty> prop) {
		this.prop = prop;
	}

}
