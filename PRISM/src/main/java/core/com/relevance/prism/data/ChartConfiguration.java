package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChartConfiguration {
	private Map<String, ArrayList<ChartProperty>> chartProperty;
	private List<Charts> charts;
	public Map<String, ArrayList<ChartProperty>> getChartProperty() {
		return chartProperty;
	}
	public void setChartProperty(Map<String, ArrayList<ChartProperty>> chartProperty) {
		this.chartProperty = chartProperty;
	}
	public List<Charts> getCharts() {
		return charts;
	}
	public void setCharts(List<Charts> chartslist) {
		this.charts = chartslist;
	}
}
