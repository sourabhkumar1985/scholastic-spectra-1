package com.relevance.prism.data;

public class Charts {
	private String chartName;
	private String displayName;

	public String getChartName() {
		return chartName;
	}
	
	public void setChartName(String chartName) {
		this.chartName = chartName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	

}
