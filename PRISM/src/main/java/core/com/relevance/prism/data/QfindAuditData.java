package com.relevance.prism.data;

import java.sql.Date;

public class QfindAuditData {

	private String item_number;
	private String attribute_name;
	private String old_value;
	private String new_value;
	private String who_modified;
	private Date when_modified;
	private int serial_no;

	public String getItem_number() {
		return item_number;
	}
	public void setItem_number(String item_number) {
		this.item_number = item_number;
	}
	public String getAttribute_name() {
		return attribute_name;
	}
	public void setAttribute_name(String attribute_name) {
		this.attribute_name = attribute_name;
	}
	public String getOld_value() {
		return old_value;
	}
	public void setOld_value(String old_value) {
		this.old_value = old_value;
	}
	public String getNew_value() {
		return new_value;
	}
	public void setNew_value(String new_value) {
		this.new_value = new_value;
	}
	public String getWho_modified() {
		return who_modified;
	}
	public void setWho_modified(String who_modified) {
		this.who_modified = who_modified;
	}
	
	public Date getWhen_modified() {
		return when_modified;
	}
	public void setWhen_modified(Date when_modified) {
		this.when_modified = when_modified;
	}
	public int getSerial_no() {
		return serial_no;
	}
	public void setSerial_no(int serial_no) {
		this.serial_no = serial_no;
	}
	
	
	
}
