package com.relevance.prism.data;


public class Report{

	public String id;
	public String name;
	public String description;
	public String query;
	public String param;
	public String source;
	public int roleid;	
	public String dbname;
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}



	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}



	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}



	/**
	 * @return the param
	 */
	public String getParam() {
		return param;
	}



	/**
	 * @param param the param to set
	 */
	public void setParam(String param) {
		this.param = param;
	}



	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}



	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}



	/**
	 * @return the roleid
	 */
	public int getRoleid() {
		return roleid;
	}



	/**
	 * @param roleid the roleid to set
	 */
	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	/**
	 * @return the dbname
	 */
	public String getDbname() {
		return dbname;
	}


	/**
	 * @param dbname the dbname to set
	 */
	public void setDbname(String dbname) {
		this.dbname = dbname;
	}


	@Override
	public String toString(){
		return "Report ID : "+ id + " Name : "+ name + " Description : " + description + " Query : " + query + " Param : " + param + " Source : " + source + " Role ID " + roleid;
	}
	
}
