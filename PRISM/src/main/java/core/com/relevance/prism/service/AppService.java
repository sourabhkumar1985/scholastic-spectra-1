/**
 * 
 */
package com.relevance.prism.service;

import com.relevance.prism.dao.AppDao;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

/**
 * @author skhan71
 *
 */
public class AppService extends BaseService {

	

	public String getAllowedAppListForUser(String userName) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = ""; 
		try {
			Log.Info("getAllowedAppListForUser method on  App Properties service");
			AppDao appPropDao = new AppDao();
			response = appPropDao.getAllowedAppListForUser(userName);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;		
	}
	
	public boolean updateLastUsedAppByUser(String userName, String appName ) throws Exception{
		Boolean status =  false;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());		
		try {
			Log.Info("updateLastUsedAppByUser method on  App Properties service");
			AppDao daoAppProp = new AppDao();
			status = daoAppProp.updateLastUsedAppByUser(userName, appName);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			return status;
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());		 
		return status;		
	}
		

	public String getLastUsedAppOfUser(String userName) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = ""; 
		try {
			Log.Info("getAllowedAppListForUser method on  App Properties service");
			AppDao appPropDao = new AppDao();
			response = appPropDao.getLastUsedAppOfUser(userName);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;		
	}
	
	
}
