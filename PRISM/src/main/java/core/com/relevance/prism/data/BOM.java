package com.relevance.prism.data;

import com.relevance.prism.data.CrystalAgilePart;

public class BOM {
	private CrystalAgilePart forward;
	private CrystalAgilePart backward;
	public CrystalAgilePart getForward() {
		return forward;
	}
	public void setForward(CrystalAgilePart forward) {
		this.forward = forward;
	}
	public CrystalAgilePart getBackward() {
		return backward;
	}
	public void setBackward(CrystalAgilePart backward) {
		this.backward = backward;
	}
}
