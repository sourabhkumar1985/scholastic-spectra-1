package com.relevance.prism.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.service.Service;
import com.relevance.prism.service.ServiceLocator;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.data.SearchParam;

@Path("/tokenAnalysis")
@Consumes("application/javascript")
public class TokenAnalysisResource extends BaseResource{
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/search/")
	public String searchData(@FormParam("key") String key,
			@FormParam("facet") String facet, @FormParam("start") String start,
			@FormParam("rows") String rows) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;

		try {

			Service service = ServiceLocator.getServiceInstance("tokenAnalysis");

			Log.Info("Search Param recieved is " + key);
			/* Gson gson = new Gson(); */
			SearchParam searchParam = new SearchParam();
			searchParam.setKey(key);
			searchParam.setFacet(facet);
			searchParam.setStart(start);
			searchParam.setRows(rows);
			
			response = (String) service.getJsonObject(searchParam,
					"tokenAnalysisService");
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/visualization/")
	public String chartData(@FormParam("key") String key,
			@FormParam("facet") String facet, @FormParam("start") String start,
			@FormParam("rows") String rows,@FormParam("type") String type) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;

		try {

			Service service = ServiceLocator.getServiceInstance("tokenAnalysis");

			Log.Info("Search Param recieved is " + key);
			/* Gson gson = new Gson(); */
			SearchParam searchParam = new SearchParam();
			searchParam.setKey(key);
			searchParam.setFacet(facet);
			searchParam.setStart(start);
			searchParam.setRows(rows);
			searchParam.setType(type);
			response = (String) service.getJsonObject(searchParam,
					"tokenAnalysisVisualizationService");

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

}
