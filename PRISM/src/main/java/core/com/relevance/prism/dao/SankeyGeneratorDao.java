package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.data.SankeyConfig;
import com.relevance.prism.data.SankeyFilter;
import com.relevance.prism.data.SankeyLink;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;

public class SankeyGeneratorDao extends BaseDao{

	Connection con = null;
	Connection conPost = null;
	Statement stmt = null;
	Statement stmtPost = null;
	ResultSet rsPost = null;
	
	public HashMap<String, String> getLookUpValues(String source, String database, String lookupQuery) throws SQLException, NullPointerException, Exception
	{
		String query = null;
		String paramCon = null;
		ResultSet rs = null;
		HashMap<String, String> lookUpMap = new HashMap<String, String>();
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		
		try 
		{
			query = "select name from app_properties where name ='" + database + "' or (value like '%jdbc%' and value like '%"
					+ database + "%')";
			conPost = dbUtil.getPostGresConnection();
			stmtPost = conPost.createStatement();
			rsPost = stmtPost.executeQuery(query);
			if (rsPost != null) {
				while (rsPost.next()) {
					paramCon = rsPost.getString("name");
				}
			}
			if ("HIVE".equalsIgnoreCase(source)) {
				con = dbUtil.getHiveConnection(paramCon);
				query = "show tables";
			} else {
				con = dbUtil.getPostGresConnection(paramCon);
				query = "SELECT table_name as name FROM information_schema.tables where table_schema = 'public' ORDER BY table_schema,table_name ";
			}
	
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) 
			{
				lookUpMap.put(rs.getString(0), rs.getString(1));
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, stmt);
			PrismHandler.handleFinally(conPost, rsPost, stmtPost);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return lookUpMap;
	}
	
	public List<MasterItem> getSearchSuggestions(String q,String source, String database, String lookupQuery) throws SQLException, NullPointerException, Exception {
//		String query = null;
//		String paramCon = null;
//		Boolean isPresent= false;
		ResultSet rs = null;
		//Set<MasterItem> suggestionsList = new HashSet<MasterItem>();
		LinkedHashSet<MasterItem> suggestionsList = new LinkedHashSet<MasterItem>(); 
		LinkedHashSet<MasterItem> selectedList = new LinkedHashSet<MasterItem>();
		List<MasterItem> aList = new ArrayList<MasterItem>(); 
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
//			query = "select name from app_properties where name ='" + database + "' or (value like '%jdbc%' and value like '%"
//					+ database + "%')";
//			conPost = dbUtil.getPostGresConnection();
//			stmtPost = conPost.createStatement();
//			rsPost = stmtPost.executeQuery(query);
//			while (rsPost.next()) {
//				paramCon = rsPost.getString("name");
//			}
			 String[] arrOfStr =lookupQuery.split("\\|");
			if ("HIVE".equalsIgnoreCase(source)) {
				con = dbUtil.getHiveConnection(database);
			} else {
				con = dbUtil.getPostGresConnection(database);				
			}
			int len= arrOfStr.length;
			stmt = con.createStatement();
			if(len>1){
				ResultSet rs1 = null;
				rs1 = stmt.executeQuery(arrOfStr[1]);
				int columnCount1 = rs1.getMetaData().getColumnCount();
				while (rs1.next()) {
					MasterItem masterPojo = new MasterItem();
					masterPojo.setId(rs1.getString(1));
					masterPojo.setName(rs1.getString(2));
					if(columnCount1>2){
						masterPojo.setCount(rs1.getString(3));
					}
					selectedList.add(masterPojo);
				}
				rs1.close();
			}
			int listLen=selectedList.size();
			
			if(listLen <100 || q!= null && !"".equalsIgnoreCase(q.trim())){
				lookupQuery = arrOfStr[0];
				if (q!= null && !"".equalsIgnoreCase(q.trim())){
					lookupQuery  = lookupQuery.replace("<Q>", q.toLowerCase());
					if (q.toLowerCase().contains("blank") || q.toLowerCase().contains("null") || q.toLowerCase().contains("other") || q.toLowerCase().contains("empty")){
						MasterItem masterPojo = new MasterItem();
						masterPojo.setId("null");
						masterPojo.setName("Empty/Others");
						suggestionsList.add(masterPojo);
						masterPojo = new MasterItem();
						masterPojo.setId("not null");
						masterPojo.setName("Exclude Empty/Others");
						suggestionsList.add(masterPojo);
					}
				}else{
					lookupQuery  = lookupQuery.replace("<Q>","");
				}
				if(lookupQuery.indexOf(" LIMIT ") == -1){
					lookupQuery += " Limit 100";
				}
				
				rs = stmt.executeQuery(lookupQuery);
				int columnCount = rs.getMetaData().getColumnCount();
				while (rs.next()) {
					MasterItem masterPojo = new MasterItem();
					masterPojo.setId(rs.getString(1));
					masterPojo.setName(rs.getString(2));
					if(columnCount>2){
						masterPojo.setCount(rs.getString(3));
					}
					suggestionsList.add(masterPojo);
				}
			}
			
			if( listLen == 100 && q == null && "".equalsIgnoreCase(q.trim())){
				suggestionsList =selectedList;
			}else if(listLen>0){
				suggestionsList.addAll(selectedList);//removeAll(suggestionsList);
			}
		aList.addAll(suggestionsList);
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, stmt);
			//PrismHandler.handleFinally(conPost, rsPost, stmtPost);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return aList;
	}
	
	public  List<FlowData> getJSON(String source, String database,SankeyConfig sankeyConfig) throws SQLException, NullPointerException, Exception{
		String query = null;
		String paramCon = null;
		Integer noOfThreadPool;
		List<FlowData> flowDataList = new ArrayList<>();
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<FlowData> flowDataListTemp = null;
		try {
			query = "select name from app_properties where name ='" + database + "' or (value like '%jdbc%' and value like '%"
					+ database + "%')";
			conPost = dbUtil.getPostGresConnection();
			stmtPost = conPost.createStatement();
			rsPost = stmtPost.executeQuery(query);
			if (rsPost != null) {
				while (rsPost.next()) {
					paramCon = rsPost.getString("name");
				}
			}
			if(E2emfAppUtil.getAppProperty(E2emfConstants.THREAD_POOL_COUNT) != null){
				noOfThreadPool = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.THREAD_POOL_COUNT));
			}else{
				noOfThreadPool = 25;
			}
			ExecutorService executor = Executors.newFixedThreadPool(noOfThreadPool);
			List<Future<List<FlowData>>> list = new ArrayList<>();
			for(SankeyLink sConfig : sankeyConfig.getSankeyLinkList()){
				Future<List<FlowData>> future = executor
						.submit(new SankeyQueryExcecuter(source, paramCon, sConfig, sankeyConfig));
				list.add(future); 
			}
			for (Future<List<FlowData>> fut : list) {
				flowDataListTemp = fut.get();
				for (FlowData flowData : flowDataListTemp){
					flowDataList.add(flowData);
				}
			}
			
		}  catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conPost, rsPost, stmtPost);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return flowDataList;
	}

}
