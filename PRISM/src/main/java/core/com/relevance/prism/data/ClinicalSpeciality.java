package com.relevance.prism.data;

public class ClinicalSpeciality {

	String company;
	String clinicalSpeciality;
	String id;
	private Boolean isEnabled;
	private Boolean isOverride;
	private String title;
	private String rule;
	private String condition;
	private String priority;

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getClinicalSpeciality() {
		return clinicalSpeciality;
	}

	public void setClinicalSpeciality(String clinicalSpeciality) {
		this.clinicalSpeciality = clinicalSpeciality;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Boolean getIsOverride() {
		return isOverride;
	}

	public void setIsOverride(Boolean isOverride) {
		this.isOverride = isOverride;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

}
