package com.relevance.prism.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.apache.pdfbox.pdmodel.PDDocument;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.relevance.prism.dao.MailDao;
import com.rl.cms.refresh.Attributes;
import com.rl.cms.refresh.CMSAction;
import com.rl.cms.refresh.Content;
import com.rl.cms.refresh.Documents;
import com.rl.cms.refresh.FileDetails;
import com.rl.cms.refresh.FileJSONData;
import com.rl.cms.refresh.MasterData;
//import java.time.ZonedDateTime;
//import java.time.format.DateTimeFormatter;

public class QFindFileDownloader {
	
	// Constants
	public static final String TOKEN = "token";
	public static final String JSON_CONTENT_TYPE = "application/vnd.api+json";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String ATTRIBUTES = "attributes";
	public static final String TYPE = "type";
	public static final String DATA = "data";
	public static final String AUTHENTICATE = "authenticate";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_LENGTH = "Content-Length";
	public static final String ACCEPT = "Accept";
	public static final String AUTHORIZATION = "Authorization";
	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final int BUFFER_SIZE = 4096;
	//public static final String BASEURL = "https://api.sales.integralife.com/";
	public static final String BASEURL=E2emfAppUtil.getAppProperty(E2emfConstants.INTEGRA_BASEURL);
	
	private PrismDbUtil dbUtil = null;
	private List<FileJSONData> fileDetailsList = null;
	private Map<Integer, String> filePathList = null;
	public QFindFileDownloader(){
		dbUtil = new PrismDbUtil();
		fileDetailsList = new ArrayList<FileJSONData>();
		filePathList = new HashMap<Integer, String>();
	} 
	
	public List<FileJSONData> getFileDetailsList() {
		return fileDetailsList;
	}

	public void setFileDetailsList(List<FileJSONData> fileDetailsList) {
		this.fileDetailsList = fileDetailsList;
	}

	public boolean makeConnectionAndDownloadFiles() throws Exception{

		final JSONObject jObject = new JSONObject();
		final JSONObject attr = new JSONObject();
		JSONObject data = new JSONObject();
		try {
			String cms_userid=E2emfAppUtil.getAppProperty(E2emfConstants.INTEGRA_CMS_USERID);
			String cms_password=E2emfAppUtil.getAppProperty(E2emfConstants.INTEGRA_CMS_PASSWORD);
			jObject.put(EMAIL, cms_userid);
			jObject.put(PASSWORD, cms_password);
			attr.put(ATTRIBUTES, jObject);
			attr.put(TYPE, AUTHENTICATE);
			data.put(DATA, attr);

			//final String baseURL = "https://api.sales.integralife.com/";
			final String authURL = BASEURL.concat(AUTHENTICATE);
			// Get the token
			URL url = new URL(authURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(POST);
			conn.setRequestProperty(CONTENT_TYPE, JSON_CONTENT_TYPE);
			final Integer length = data.toString().length();
			conn.setRequestProperty(CONTENT_LENGTH, length.toString());
			conn.setRequestProperty(ACCEPT, JSON_CONTENT_TYPE);
			OutputStream os = conn.getOutputStream();
			os.write(data.toString().getBytes());
			os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			String output = getStringFromInputStream(conn.getInputStream());
			data = new JSONObject(output);
			final JSONObject attrOutput = (JSONObject) ((JSONObject) data.get(DATA))
					.get(ATTRIBUTES);
			final String token = attrOutput.getString(TOKEN);
			conn.disconnect();
			java.util.Date currentdate = new java.util.Date();
			int numberOfDays = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.INTEGRA_CMS_NO_OF_DAYS));
			int date = currentdate.getDate() - numberOfDays;
			int month = currentdate.getMonth() + 1;
			int year = currentdate.getYear() + 1900;
			//String strDate = "2017-01-17 00:00:00.000 UTC";
			String strDate = ""+year+"-"+month+"-"+date+ " 00:00:00.000 UTC";
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz");
		    long epochTimeStamp = dfm.parse(strDate).getTime()/1000;
		    Log.Info("Epoch TimeStamp : " + epochTimeStamp);
			
			final String syncURL = BASEURL.concat("changes/content/" + epochTimeStamp);
			url = new URL(syncURL);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(GET);

			conn.setRequestProperty(CONTENT_TYPE, JSON_CONTENT_TYPE);
			conn.setRequestProperty(ACCEPT, JSON_CONTENT_TYPE);
			conn.setRequestProperty(AUTHORIZATION, "Bearer " + token);
			
			output = getStringFromInputStream(conn.getInputStream());
			Log.Info("\n The Response is : \n" + output);
			Gson gson = new Gson();
			MasterData masterdata = gson.fromJson(output, MasterData.class);
			List<Content> dataList = masterdata.getData();
			List<Content> includedList = masterdata.getIncluded();
			List<Integer> fileIds = new ArrayList<Integer>();
			Map<String, String> fileNameMap = new HashMap<String, String>();
			if(dataList != null && dataList.size()>0){
				Log.Info("data list size : " + dataList.size());
				for(Content content : dataList){
					Log.Info("content Attribute Name : " + content.getAttributes().getName());
					if(content.getAttributes().getFilename() != null && content.getAttributes().getFilename().toLowerCase().endsWith("pdf")){
						fileIds.add(Integer.valueOf(content.getId()));
						fileNameMap.put(content.getId(), content.getAttributes().getFilename());
					}					
				}
			}
			if(includedList != null && includedList.size()>0){
				Log.Info("included list size : " + includedList.size());
				for(Content content : includedList){
					Log.Info("Included Attribute Name : " + content.getAttributes().getDisplay_name());
					if(content.getAttributes().getFilename() != null && content.getAttributes().getFilename().toLowerCase().endsWith("pdf")){
						fileIds.add(Integer.valueOf(content.getId()));
						fileNameMap.put(content.getId(), content.getAttributes().getFilename());
					}
				}
			}
			if(fileIds != null && fileIds.size() > 0){
				downloadFiles(token, fileIds,fileNameMap);
				getMetaData(token, fileIds);
				conn.disconnect();
				return true;
			}else{
				conn.disconnect();
				return false;
			}			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(null, null, null);
		}
		return false;
	}

	private String getStringFromInputStream(InputStream is) throws Exception{

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			if (is != null) {
				br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

	public void downloadFiles(String token, List<Integer> fileIds, Map<String, String> fileNameMap) throws Exception {URL url = null;
	HttpURLConnection conn = null;
	Log.Info("Number of Files to be Downloaded : " + fileIds.size());
	try {
		for (Integer fileId : fileIds) {
			final String syncURL = BASEURL.concat("files/" + fileId);
			url = new URL(syncURL);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(GET);

			conn.setRequestProperty(CONTENT_TYPE, JSON_CONTENT_TYPE);
			conn.setRequestProperty(ACCEPT, JSON_CONTENT_TYPE);
			conn.setRequestProperty(AUTHORIZATION, "Bearer " + token);
			int responseCode = conn.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				String fileName = "";
				String disposition = conn.getHeaderField("Content-Disposition");
				String contentType = conn.getContentType();
				int contentLength = conn.getContentLength();
				if (disposition != null) {
					int index = disposition.indexOf("filename=");
					if (index > 0) {
						fileName = disposition.substring(index + 10, disposition.length() - 1);
					}
				} else {
					fileName = "sample.pdf";
				}
				Log.Info("Content-Type = " + contentType);
				Log.Info("Content-Disposition = " + disposition);
				Log.Info("Content-Length = " + contentLength);
				Log.Info("fileName = " + fileName);
				InputStream inputStream = conn.getInputStream();
				String dowonloadDirectory = E2emfAppUtil.getAppProperty(E2emfConstants.QFIND_DELTA_DOWNLOAD_DIRECTORY);
				if(fileNameMap.get(String.valueOf(fileId)) != null){
					String fileNameInNumbers = fileNameMap.get(String.valueOf(fileId)).toString();
					FileOutputStream outputStream = new FileOutputStream(dowonloadDirectory + fileNameInNumbers);
					filePathList.put(fileId, dowonloadDirectory + fileNameInNumbers);
					int bytesRead = -1;
					byte[] buffer = new byte[BUFFER_SIZE];
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						outputStream.write(buffer, 0, bytesRead);
					}
					outputStream.close();
					inputStream.close();
					Log.Info("File " + fileName + " downloaded");
					//fileUploadToPrincetonServer(dowonloadDirectory, fileNameInNumbers);
					fileUploadToQfindServer(dowonloadDirectory, fileNameInNumbers);
				}				
			} else {
				Log.Info("Not able to reach the API for id " + fileId );
			}
			conn.disconnect();
		}

	} catch(Exception ex){
		PrismHandler.handleException(ex, true);
	}
	finally {
		PrismHandler.handleFinally(null, null, null);
	}
	}
	
	private void writeFile(InputStream is, String fileName) throws Exception{

		BufferedReader br = null;
		File file = new File("C:\\QFind\\"+ fileName);
		OutputStream output = new FileOutputStream(file);
		byte[] buffer = new byte[4096];
		int n = - 1;
		try {
				br = new BufferedReader(new InputStreamReader(is));
				while ( (n = is.read(buffer)) != -1) 
				{
				    output.write(buffer, 0, n);
				}
				output.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void getMetaData(String token, List<Integer> fileIds) throws Exception{
		URL url = null;
		HttpURLConnection conn = null;
		Log.Info("Number of Files : " + fileIds.size());
		try {
			for (Integer fileId : fileIds) {
				final String syncURL = BASEURL.concat("files/" + fileId + "/info");
				url = new URL(syncURL);
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod(GET);

				conn.setRequestProperty(CONTENT_TYPE, JSON_CONTENT_TYPE);
				conn.setRequestProperty(ACCEPT, JSON_CONTENT_TYPE);
				conn.setRequestProperty(AUTHORIZATION, "Bearer " + token);
				int responseCode = conn.getResponseCode();
				if (responseCode != HttpURLConnection.HTTP_INTERNAL_ERROR) {
					String output = getStringFromInputStream(conn.getInputStream());
					System.out.println("\n The Response is : \n" + output);
					Gson gson = new Gson();
					FileDetails fileDetails = gson.fromJson(output, FileDetails.class);
					FileJSONData fileJsonData = new FileJSONData();
					fileJsonData.setType(fileDetails.getData().getType());
					fileJsonData.setId(fileDetails.getData().getId());
					fileJsonData.setDisplay_name(fileDetails.getData().getAttributes().getDisplay_name());
					fileJsonData.setCreated_at(fileDetails.getData().getAttributes().getCreated_at());
					fileJsonData.setUpdated_at(fileDetails.getData().getAttributes().getUpdated_at());
					String deletedAt = fileDetails.getData().getAttributes().getDeleted_at();
					if(deletedAt != null){
						fileJsonData.setDeleted_at(deletedAt);
					}else{
						fileJsonData.setDeleted_at(null);
					}
					String fileName = fileDetails.getData().getAttributes().getFilename();
					if(fileName != null){
						fileJsonData.setFilename(fileName);
					}else{
						fileJsonData.setFilename("");
					}
					double fileSize = fileDetails.getData().getAttributes().getSize();
					if(fileSize > 0){
						fileJsonData.setSize(fileSize);
					}else{
						fileJsonData.setSize(0);
					}
					Log.Info("File Display Name : " + fileDetails.getData().getAttributes().getDisplay_name());
					//Log.Info("File Name matched with CMS : " + fileDetails.getData().getAttributes().getName());    // Nagaraj added
					insertRecordsInPostgresTable(fileDetails, fileJsonData);
				}else {
					Log.Info("No Information available for id " + fileId );
				}
				conn.disconnect();
			}			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(null, null, null);
		}	
	}
	
	public String convertTime(long time){
	    Date date = new Date(time);
	    Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
	    return format.format(date);
	}
	
	public void insertRecordsInPostgresTable(FileDetails fileDetails, FileJSONData fileJsonData) throws Exception{
		Connection conn = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		try{
			Attributes attributes = fileDetails.getData().getAttributes();
			conn = getPostGresDBConnection();
			stmt = conn.createStatement();
			String fileName = attributes.getFilename();
			if(fileName.toLowerCase().endsWith("pdf")){
				rs = stmt.executeQuery("select * from qfind_pdf_categories where str_file_name = '" + attributes.getFilename() + "'");
				if(rs.next()){
					pstmt = conn.prepareStatement("insert into qfind_pdf_categories (str_file_type,str_pages,str_file_name,str_filedisplayname, status, date) values (?,?,?,?,?,?)");
					pstmt.setString(1, attributes.getType());
					int pageCount = 0;
					String id = fileDetails.getData().getId();
					String filePath = filePathList.get(Integer.valueOf(id).intValue());
					System.out.println("ID : " + id + " " + "File Name : " + fileName + " " + "File Path : " + filePath);
					if(fileDetails.getData().getId() != null && filePathList.get(Integer.valueOf(fileDetails.getData().getId())) != null){
						File file = new File(filePath);
						pageCount = countPages(file);
					}					
					pstmt.setString(2, String.valueOf(pageCount));
					pstmt.setString(3, attributes.getFilename());
					pstmt.setString(4, attributes.getDisplay_name());
					UpdateDocumentAttributes updateDocumentAttributes = new UpdateDocumentAttributes();// Nagaraj Added
					CMSAction cmsAction = updateDocumentAttributes.getCMSAction(fileDetails.getData().getId(),"U");
					if(cmsAction.getAction().equalsIgnoreCase("D")){
						pstmt.setString(5, "D");
						pstmt.setTimestamp(6, new java.sql.Timestamp(cmsAction.getDate().getTime()));
					}else{
						pstmt.setString(5, "U");
						pstmt.setTimestamp(6, new java.sql.Timestamp(cmsAction.getDate().getTime()));
					}
					//java.util.Date date = new java.util.Date();
					pstmt.executeUpdate();					
					FileJSONData fileJsonDataWithFlags = updateDocumentAttributes.updateDocumentAttributes(fileDetails.getData().getId(),attributes.getFilename(), fileJsonData, "U");
					fileDetailsList.add(fileJsonDataWithFlags);
				}else{
					pstmt = conn.prepareStatement("insert into qfind_pdf_categories (str_file_type,str_pages,str_file_name,str_filedisplayname, status, date) values (?,?,?,?,?,?)");
					pstmt.setString(1, attributes.getType());
					int pageCount = 0;
					String id = fileDetails.getData().getId();
					String filePath = filePathList.get(Integer.valueOf(id).intValue());
					System.out.println("ID : " + id + " " + "File Path : " + filePath);
					if(filePath != null && filePath.length() > 0){
						if(fileDetails.getData().getId() != null && filePathList.get(Integer.valueOf(fileDetails.getData().getId())) != null){
							File file = new File(filePath);
							pageCount = countPages(file);
						}
						pstmt.setString(2, String.valueOf(pageCount));
						pstmt.setString(3, attributes.getFilename());
						pstmt.setString(4, attributes.getDisplay_name()); // Nagaraj Added					
						UpdateDocumentAttributes updateDocumentAttributes = new UpdateDocumentAttributes();// Nagaraj Added
						CMSAction cmsAction = updateDocumentAttributes.getCMSAction(fileDetails.getData().getId(),"A");
						if(cmsAction.getAction() != null && cmsAction.getAction().equalsIgnoreCase("A")){						
							pstmt.setString(5, "A");
							pstmt.setTimestamp(6, new java.sql.Timestamp(cmsAction.getDate().getTime()));
						}else{
							pstmt.setString(5, "NA");
							pstmt.setTimestamp(6, new java.sql.Timestamp(cmsAction.getDate().getTime()));
						}
						//java.util.Date date = new java.util.Date();
						//pstmt.setTimestamp(6, new java.sql.Timestamp(date.getTime()));
						pstmt.executeUpdate();
						FileJSONData fileJsonDataWithFlags = updateDocumentAttributes.updateDocumentAttributes(fileDetails.getData().getId(),attributes.getFilename(), fileJsonData, "A");
						fileDetailsList.add(fileJsonDataWithFlags);
					}
				}
			}
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conn, rs, pstmt);
		}	
	}
	
	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}
	
	private int countPages(File file) throws Exception{
	     /*RandomAccessFile raf = new RandomAccessFile(file, "r");
	     RandomAccessFileOrArray pdfFile = new RandomAccessFileOrArray(
	          new RandomAccessSourceFactory().createSource(raf));
	     PdfReader reader = new PdfReader(pdfFile, new byte[0]);
	     int pages = reader.getNumberOfPages();
	     reader.close();*/
		//PDDocument doc = PDDocument.load(file);
		//int pages = doc.getNumberOfPages();
		//doc.close();
	    return 0;
	  }
	
	public File getEmailAttachment(org.json.JSONArray dataJson,
			String fileName, String fileformat) {
		File file = null;
		MailDao mailerDao = new MailDao();
		if ("csv".equalsIgnoreCase(fileformat))
			file = mailerDao.convertJsonToCsv(dataJson, fileName);
		if ("xlsx".equalsIgnoreCase(fileformat))
			file = mailerDao.convertJsonToXlsx(dataJson, fileName);
		return file;
	}
	
	public JSONArray getJSONArrayOfContents(List<FileJSONData> fileDetailsList) throws Exception{
		try{
			//JSONArray fileJSONArray = new JSONArray(fileDetailsList);
			Documents documents = new Documents();
			documents.setDocs(fileDetailsList);
			Gson gson = new Gson();
			String output = gson.toJson(documents);
			JSONObject jsonOutput = new JSONObject(output);
			JSONArray docs = jsonOutput.getJSONArray("docs");
			return docs;
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return null;
	}
	public static void fileUploadToPrincetonServer(String localFileLocation, String fileName){
		String username=E2emfAppUtil.getAppProperty(E2emfConstants.PRINCETON_USERNAME);
		String host=E2emfAppUtil.getAppProperty(E2emfConstants.PRINCETON_HOST);
		String password=E2emfAppUtil.getAppProperty(E2emfConstants.PRINCETON_PASSWORD);
		String remotefileLocation=E2emfAppUtil.getAppProperty(E2emfConstants.PRINCETON_PDF_FILE_LOCATION);
		//File localfile = new File("/apps2/INTEGRA");
		//File[] listOfFiles = localfile.listFiles();
		//String filename = null;
	    JSch jsch = new JSch();
	    Session session = null;
	    try {
	          session = jsch.getSession(username, host, 22);
	          session.setConfig("StrictHostKeyChecking", "no");
	          session.setPassword(password);
	          session.connect();

	          Channel channel = session.openChannel("sftp");
	          channel.connect();
	          ChannelSftp sftpChannel = (ChannelSftp) channel;
	          if(fileName.endsWith(".pdf")){
		    	  sftpChannel.put(localFileLocation + fileName, remotefileLocation+"/"+fileName);
		      }else{
		    	  System.out.println("File is not a PDF File!!!");
		      }
	          System.out.println("Files uploaded to Princeton Server successfully...!!!");
	          sftpChannel.exit();
	          session.disconnect();
	     } catch (JSchException e) {
	          e.printStackTrace();  
	     } catch (SftpException e) {
	          e.printStackTrace();
	     }
	}
	
	public static void fileUploadToQfindServer(String localFileLocation, String fileName)
	{
		String host=E2emfAppUtil.getAppProperty(E2emfConstants.QFIND_REMOTE_HOST);
		String username=E2emfAppUtil.getAppProperty(E2emfConstants.QFIND_REMOTE_USERNAME);
		String password=E2emfAppUtil.getAppProperty(E2emfConstants.QFIND_REMOTE_PASSWORD);
		String remotefilelocation=E2emfAppUtil.getAppProperty(E2emfConstants.QFIND_REMOTE_PDF_FILE_LOCATION);
		//File localfile = new File("/apps2/INTEGRA");
		//File[] listOfFiles = localfile.listFiles();
		//String filename = null;
		 JSch jsch = new JSch();
		    Session session = null;
		    try {
		          session = jsch.getSession(username, host, 22);
		          session.setConfig("StrictHostKeyChecking", "no");
		          session.setPassword(password);
		          session.connect();
		          Channel channel = session.openChannel("sftp");
		          channel.connect();
		          ChannelSftp sftpChannel = (ChannelSftp) channel;
		          if(fileName.endsWith(".pdf")){
			    	  sftpChannel.put(localFileLocation + fileName, remotefilelocation+"/"+fileName);
			      }else{
			    	  System.out.println("File is not a PDF File!!!");
			      }
		          System.out.println("Files uploaded to Soft Link Path successfully...!!!");
		          sftpChannel.exit();
		          session.disconnect();
		     } catch (JSchException e) {
		          e.printStackTrace();  
		     } catch (SftpException e) {
		          e.printStackTrace();
		     }
	}
}
