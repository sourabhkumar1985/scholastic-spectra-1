package com.relevance.prism.util;

public class EmailConstants {

	public static final String DAILY = "daily";
	public static final String WEEKLY = "weekly";
	public static final String MONTHLY = "monthly";
	
}
