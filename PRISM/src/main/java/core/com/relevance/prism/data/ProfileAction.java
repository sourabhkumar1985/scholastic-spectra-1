package com.relevance.prism.data;

public class ProfileAction {
	private int id;
	private int profileID;
	private String name;
	private String fieldMapping;
	private String linkedProfileKey;
	private String linkedProfileName;
	private String type;
	private String actionType;
	private boolean enabled;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFieldMapping() {
		return fieldMapping;
	}
	public void setFieldMapping(String fieldMapping) {
		this.fieldMapping = fieldMapping;
	}
	public int getProfileID() {
		return profileID;
	}
	public void setProfileID(int profileID) {
		this.profileID = profileID;
	}
	public String getLinkedProfileKey() {
		return linkedProfileKey;
	}
	public void setLinkedProfileKey(String linkedProfileKey) {
		this.linkedProfileKey = linkedProfileKey;
	}
	public String getLinkedProfileName() {
		return linkedProfileName;
	}
	public void setLinkedProfileName(String linkedProfileName) {
		this.linkedProfileName = linkedProfileName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
