package com.relevance.prism.service;

import com.relevance.prism.dao.DataCorrectionsDao;
import com.relevance.prism.data.NGramList;

public class DataCorrectionService {

	public NGramList getUnCorrectedData() throws Exception {
		DataCorrectionsDao correctionsDao = new DataCorrectionsDao();
		return correctionsDao.getUnCorrectedData();

	}

	public NGramList getCorrectedData() throws Exception {
		DataCorrectionsDao correctionsDao = new DataCorrectionsDao();
		return correctionsDao.getCorrectedData();

	}
}
