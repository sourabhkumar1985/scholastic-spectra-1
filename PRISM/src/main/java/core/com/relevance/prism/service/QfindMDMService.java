package com.relevance.prism.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.relevance.prism.dao.QfindMDMDao;
import com.relevance.prism.dao.RuleAttributeMasterDataDao;
import com.relevance.prism.dao.UserDao;
import com.relevance.prism.data.Attribute;
import com.relevance.prism.data.Audit;
import com.relevance.prism.data.MasterData;
import com.relevance.prism.data.QfindAuditData;
import com.relevance.prism.data.User;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;


public class QfindMDMService extends BaseService{
	public List<Attribute> getAllattribute() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Attribute> response=new ArrayList<Attribute>();
		try {
			Log.Info("MDM service called ");
			response = (new QfindMDMDao()).getAllattribute();
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	public Attribute getAttribute(String attributename) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Attribute response=new Attribute();
		try {
			Log.Info("MDM service called ");
			response = (new QfindMDMDao()).getAttribute(attributename);
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	public String addAttribute(String attributename,String rulename) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean response= false;
		try {
			Log.Info("MDM service called ");
			response = (new QfindMDMDao()).addAttribute(attributename,rulename);
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if(response==true)
		return "{\"message\": \"Attribute added successfully .\"}";
		else
		return "{\"message\": \"cannot add.\"}";
	}
	public String updateAttribute(String attributename, String rulename) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean response= false;
		try {
			Log.Info("MDM service called ");
			response = (new QfindMDMDao()).updateAttribute(attributename,rulename);
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if(response==true)
		return "{\"message\": \"Attribute updated successfully .\"}";
		else
		return "{\"message\": \"cannot update.\"}";
		
	}
	public String deleteAttribute(String attributename) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean response= false;
		try {
			Log.Info("MDM service called ");
			response = (new QfindMDMDao()).deleteAttribute(attributename);
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if(response==true)
		return "{\"message\": \"Attribute deleted successfully .\"}";
		else
		return "{\"message\": \"cannot update.\"}";
	}
	
	
	public List<MasterData> getAllAttributeList() throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<MasterData> response= null;
		try {
			Log.Info("MDM service called ");
			response = (new RuleAttributeMasterDataDao()).getItemNumbers();
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	
	public String addAttributeLog(String item_number, String attributename,String new_value,String userName)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		boolean response = false;
		try {
			Log.Info("MDM service called ");
			response = (new RuleAttributeMasterDataDao()).addDataIntoLogTable(item_number, attributename,new_value,userName);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response == true)
			return "{\"message\": \"Attribute added successfully .\"}";
		else
			return "{\"message\": \"cannot add.\"}";

	}
	
	
	public List<Audit> getLogDetails(String item_number,String attributname) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Audit> response=null;
		try {
			Log.Info("MDM service called ");
			
			response = (new RuleAttributeMasterDataDao()).getAttributeLog(item_number,attributname);
			
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	public List<MasterData> getAuditData(String itemnumberlist) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<MasterData> response=null;
		try {
			Log.Info("MDM service called ");
			response = (new RuleAttributeMasterDataDao()).getDataFromMasterData(itemnumberlist);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
}
