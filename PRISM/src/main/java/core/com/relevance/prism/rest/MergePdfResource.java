package com.relevance.prism.rest;

import java.io.File;
import java.sql.Timestamp;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.common.io.Files;
import com.relevance.prism.util.MergePdfUtil;
import com.relevance.prism.util.PrismHandler;
@Path("/pdf")
public class MergePdfResource extends BaseResource {
	
	@Context
	ServletContext context;

	public MergePdfResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	/*@POST
    @Path("/mergepdf")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/pdf")
    public Response merge(@FormParam("urllist") String urllist) {
        //System.out.println("Start");
        ResponseBuilder response = null ;
        try {
        	String [] urls = urllist.split(",");
        	String filePath = MergePdfUtil.merge(urls);
        	File file = new File(filePath);
			response = Response.ok((Object) file, "application/pdf");
            response.header("Content-Disposition", "inline; filename=" + file.getName());
            //response.header("Set-Cookie", "fileDownload=true; path=/");
            return response.build();
        } catch (Exception ex) {
        	Log.Error("Exception-1 Occurred While Downloading pdf File..." + ex.getMessage());
            ex.printStackTrace();
        }
        //System.out.println("End");
        return response.build();
    }*/
	
	
	@POST
    @Path("/mergepdf")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
    public String merge(@FormParam("urllist") String urllist, @Context HttpServletRequest req)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//String contextPath = context.getContextPath();
		String urlPath = req.getRequestURL().toString();
		int e2emfPathIndex = urlPath.indexOf("/E2EMF");
		String finalUrlPath = urlPath.substring(0, e2emfPathIndex);
		String contextPath = context.getRealPath(File.separator);
		String pdfFolderPath = null;
		String finalMergedPdfPath = null;
		String response = null;
        try {
        	String [] urls = urllist.split(",");
        	String filePath = MergePdfUtil.merge(urls);
        	File file = new File(filePath);
			pdfFolderPath = contextPath + "web/pdf/temp/";
			File pdfFolder = new File(pdfFolderPath);
			if(!pdfFolder.exists()){
				pdfFolder.mkdirs();
			}
			java.util.Date date= new java.util.Date();
			String fileName = new Timestamp(date.getTime()).toString().replace(" ", "_").replace(":", "_").replace(".", "_") + "_" + file.getName();
			Files.copy(file, new File(pdfFolderPath + fileName));
			finalMergedPdfPath = finalUrlPath+ "/E2EMF/pdf/temp/" + fileName;
			response = "{\"mergedPdfFilePath\": \""+ finalMergedPdfPath +"\"}";
        }catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
    }
}
