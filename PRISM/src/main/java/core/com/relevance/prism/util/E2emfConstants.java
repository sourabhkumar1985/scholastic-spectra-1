package com.relevance.prism.util;
/**
 * 
 * E2emf Application constants  
 *
 */
public class E2emfConstants {
	
		//Enable Cache
		public static final String enableCache = "EnableCache";
		
		// For automated test suite
		public static final String suiteSource = "SuiteSource";
		public static final String suiteDB = "SuiteDB";
		public static final String suiteQuery = "SuiteQuery";
		public static final String emailFrom = "EmailFrom";
	   //DB Constants
		public static final String hiveUrl = "CONNECTION_URL_HIVE";
		public static final String gbtHiveUrl = "CONNECTION_URL_GBT_HIVE";
		//public static final String hiveUrl = "CONNECTION_URL_HIVE";
		public static final String hivedriverUrl = "CONNECTION_DRIVER_HIVE";
		public static final String hiveUser = "HIVE_USER_NAME";
		public static final String hivePwd =  "HIVE_USER_PWD";
		public static final String e2emfDB = "E2EMF_DB";
		public static final String gbtDB = "GBT_DB";
		public static final String slobDB = "SLOB_DB";
		public static final String emeaDB = "EMEA_DB";		
		public static final String emeaWRK = "EMEA_WRK";
		public static final String emeaSTG = "EMEA_STG";
		public static final String p2020DB = "P2020_DB";
		public static final String kitkatDB = "KITKAT_DB";
		
		public static final String qfindDB="QFIND_DB";

		public static final String smartFindDB = "SF_DB";
		public static final String apacDB = "APAC_DB";
		public static final String lpfgDB = "LPFG_DB";
		public static final String ppvReportPath = "PPV_REPORT_FILEPATH";
		//public static final String ppaDB = "PPA_DB";
		public static final String securityDB = "SECURITY_DB";
		public static final String e2emfSearchType = "E2EMF_SEARCH_TYPE";
		public static final String gbtSearchType = "GBT_SEARCH_TYPE";
		
		public static final String QUERY_LIMIT = "QUERY_LIMIT";
		
		//Login config
		public static final String loginLogo = "LOGIN_LOGO";
		public static final String loginFavicon = "LOGIN_FAVICON";
		public static final String loginContent = "LOGIN_CONTENT";
		
		//PPA Item match
		public static final String ppaEnableGenericMatch = "PPA_ENABLE_GENERIC_MATCH";
		
		//Constants for reading Build Info
		public static final String revisionNo = "Revision";
		public static final String lastChangeDate = "Last";
		
		
		public static final String release = "RELEASE";
		
		public static final String jiraUrl = "JIRA_SERVER_URL";
		public static final String jiraUser = "JIRA_USER";
		public static final String jiraPwd = "JIRA_PASSWORD";	
		
		public static final String solarInstrumentCatalogInstanceName = "SOLR_SERVER_INSTRUMENT_CATALOG_INSTANCE_NAME";
		public static final String solarInstrumentCatalogNGramFilePath = "INSTRUMENT_CATALOG_NGRAM_FILEPATH";
		public static final String solarUrl = "SOLR_SERVER_URL";
		public static final String solarTokenAnalysisUrl = "SOLR_SERVER_URL_TOKEN_ANALYSIS";
		public static final String solarSAPInstanceName = "SOLR_SERVER_SAP_INSTANCE_NAME";
		public static final String solarJDEInstanceName = "SOLR_SERVER_JDE_INSTANCE_NAME";
		public static final String solarGPInstanceName = "SOLR_SERVER_GP_INSTANCE_NAME";
		public static final String solarInstanceTokenAnalysisName = "SOLR_SERVER_INSTANCE_NAME_TOKEN_ANALYSIS";
		public static final String postGresDBContext = "E2EMFPostGres";
		public static final String MYSQL_DB_CONTEXT = "E2EMFPostGres";
		
		public static final String postgresUrl = "CONNECTION_URL_POSTGRES";
		public static final String postgresdriverUrl = "CONNECTION_DRIVER_POSTGRES";
		public static final String postgresUser = "POSTGRES_USER_NAME";
		public static final String postgresPwd =  "POSTGRES_USER_PWD";
		
		public static final String phoenixUrl = "CONNECTION_URL_PHOENIX";
		public static final String phoenixdriverUrl = "CONNECTION_DRIVER_PHOENIX";
		public static final String phoenixUser = "PHOENIX_USER_NAME";
		public static final String phoenixPwd =  "PHOENIX_USER_PWD";
		
		public static final String usePostGresDb = "USE_POSTGRESQL_DB";
		
		public static final String qfind_db="QFINDSPELL";
		
		public static final String dataObfuscation = "DATA_OBFUSCATION"; 
		public static final String seacrhHistoryCountCutoff = "SEARCH_HISTORY_CUTOFF";
		
		//SolarSearch Constants PurchaseOrder SAP
		public static final String POH1 = "POH1";
		public static final String POH2 = "POH2";
		public static final String POD1 = "POD1";
		public static final String POD2 = "POD2";
		public static final String POD3 = "POD3";
		public static final String POD4 = "POD4";
		
		//SolarSearch Constants PurchaseOrder JDE
		public static final String POH1JDE = "POH1JDE";
		public static final String POH2JDE = "POH2JDE";
		public static final String POD1JDE = "POD1JDE";
		public static final String POD2JDE = "POD2JDE";
		public static final String POD3JDE = "POD3JDE";
		public static final String POD4JDE = "POD4JDE";
		
		
		public static final String PONetPrice = "PONetPrice";
		public static final String POQty = "POQty";
		public static final String POItem = "POItem";
		
		
		//SolarSearch Constants SalesOrder SAP
		public static final String SOSalesDoc = "SOSalesDoc";
		public static final String SOItem = "SOItem";
		
		public static final String SOMaterialNum = "SOMaterialNum";
		public static final String SOMaterial = "SOMaterial";
		public static final String SONetValue = "SONetValue";
		public static final String SOOrderQty = "SOOrderQty";
		
		//SolarSearch Constants SalesOrder JDE
		public static final String SOSalesDocJDE = "SOSalesDocJDE";
		public static final String SOItemJDE = "SOItemJDE";
		
		public static final String SOMaterialNumJDE = "SOMaterialNumJDE";
		public static final String SOMaterialJDE = "SOMaterialJDE";
		public static final String SONetValueJDE = "SONetValueJDE";
		public static final String SOOrderQtyJDE = "SOOrderQtyJDE";
		
		
		//SolarSearch Constants PREQ EBKN related fields
		public static final String PR1 = "PR1";
		public static final String PR2 = "PR2";
		public static final String PR3 = "PR3";
		
		
		//SolarSearch Constants PREQ EBAN related fields
		public static final String EBANPR1 = "EBANPR1";
		public static final String EBANPR2 = "EBANPR2";
		public static final String EBANPR3 = "EBANPR3";
		public static final String EBANPR4 = "EBANPR4";
		
		//SolarSearch Constants KNA1 related fields SAP
		public static final String KNACust1 = "KNACust1";
		public static final String KNACust2 = "KNACust2";		
		public static final String KNACust3 = "KNACust3";		
		public static final String KNACust4 = "KNACust4";
		public static final String KNACust5 = "KNACust5";
		
		//SolarSearch Constants KNA1 related fields JDE
		public static final String KNACust1JDE = "KNACust1JDE";
		public static final String KNACust2JDE = "KNACust2JDE";		
		public static final String KNACust3JDE = "KNACust3JDE";		
		public static final String KNACust4JDE = "KNACust4JDE";
		public static final String KNACust5JDE = "KNACust5JDE";
		
		//SolarSearch Constants KNB1 related fields
		public static final String KNBCust1 = "KNBCust1";
		public static final String KNBCust2 = "KNBCust2";
		
		//SolarSearch Constants LFA1 related fields SAP
		public static final String LFA1 = "LFA1";
		public static final String LFA2 = "LFA2";
		public static final String LFA3 = "LFA3";		
		public static final String LFA4 = "LFA4";
		public static final String LFA5 = "LFA5";
		
		//SolarSearch Constants LFA1 related fields JDE
		public static final String LFA1JDE = "LFA1JDE";
		public static final String LFA2JDE = "LFA2JDE";
		public static final String LFA3JDE = "LFA3JDE";		
		public static final String LFA4JDE = "LFA4JDE";
		public static final String LFA5JDE = "LFA5JDE";
		
		//SolarSearch Constants MAKT related fields SAP
		public static final String MAKT1 = "MAKT1";
		public static final String MAKT2 = "MAKT2";
		
		//SolarSearch Constants MAKT related fields JDE
		public static final String MAKT1JDE = "MAKT1JDE";
		public static final String MAKT2JDE = "MAKT2JDE";
		
		//SolarSearch Constants MARA related fields SAP
		public static final String MARA1 = "MARA1";
		public static final String MARA2 = "MARA2";
		public static final String MARA3 = "MARA3";
		
		//SolarSearch Constants MARA related fields JDE
		public static final String MARA1JDE = "MARA1JDE";
		public static final String MARA2JDE = "MARA2JDE";
		public static final String MARA3JDE = "MARA3JDE";
		
		//SolarSearch Constants MARC related fields
		public static final String MARC1 = "MARC1";
		public static final String MARC2 = "MARC2";
		
		
		//Search Summary
		public static final String SEARCH_SUMMARY_EKKO_QUERY = "SEARCH_EKKO";
		public static final String SEARCH_SUMMARY_EKPO_QUERY = "SEARCH_EKKO";
		
		
		public static final String SUB_CONTRACTING = "SUBCONTRACTING";
		public static final String DROP_SHIP = "DROPSHIP";
		public static final String EXTERNAL_PROCESSING = "EXTERNAL PROCESSING";	
		public static final String NORMAL_PURCHASING = "NORMAL PURCHASING";
		public static final String CONSIGMENT= "CONSIGNMENT";
		public static final String DIRECT_SALES= "DIRECT SALES";
		public static final String STO= "STO";
		public static final String INTERCOMPANY_DROPSHIP= "INTERCOMPANY DROPSHIP";
		
		public static final String EXTERNAL_VENDOR = "EXTERNAL VENDOR";
		public static final String INTERNAL_VENDOR = "AFFILIATE";
		public static final String MF_PLANT = "MF_PLANT";
		public static final String DC_PLANT = "DC_PLANT";
		public static final String CUSTOMER = "CUSTOMER";
		
		//PurchaseOrder Constants
		public static final String COMPANY_CODE = "COMPANY_CODE";
		public static final String PLANT = "PLANT";
		public static final String PURCHASE_ORG = "MF_PLANT";
		public static final String DOC_TYPE = "DOC_TYPE";
		public static final String PAY_TERM = "PAY_TERM";
		
		public static final String COMPANYCODE_LIST = "COMPANYCODE_LIST";
		public static final String PLANT_LIST = "PLANT_LIST";
		public static final String PURCHASEORG_LIST = "PURCHASEORG_LIST";
		public static final String DOCTYPE_LIST = "DOCTYPE_LIST";
		public static final String PAYTERM_LIST = "PAYTERM_LIST";
		
		public static final String COMPANYCODE_PLANT_QUERY = "COMPANYCODE_PLANT";
		public static final String PLANT_PURCHASEORG_QUERY = "PLANT_PURCHASEORG";
		public static final String PURCHASEORG_DOCTYPE_QUERY = "PURCHASEORG_DOCTYPE";
		public static final String DOCTYPE_PAYTERM_QUERY = "DOCTYPE_PAYTERM";
		
		public static final String COMPANYCODE_PLANT_FLOWLIST = "COMPANYCODE_PLANT";
		public static final String PLANT_PURCHASEORG_FLOWLIST = "PLANT_PURCHASEORG";
		public static final String PURCHASEORG_DOCTYPE_FLOWLIST = "PURCHASEORG_DOCTYPE";
		public static final String DOCTYPE_PAYTERM_FLOWLIST = "DOCTYPE_PAYTERM";
		
		public static final String FROM_COMPANYCODE = "COMPANYCODE";
		public static final String FROM_PLANT = "PLANT";
		public static final String FROM_PURCHASEORG = "PURCHASEORG";
		public static final String FROM_DOCTYPE = "DOCTYPE";
		public static final String FROM_PAYTERM = "PAYTERM";
		
		
		public static final String TO_PLANT = "PLANT";
		public static final String TO_PURCHASEORG = "PURCHASEORG";
		public static final String TO_DOCTYPE = "DOCTYPE";
		public static final String TO_PAYTERM = "PAYTERM";
		
		
		public static final String COMPANYCODE_PLANT_LINKTYPE = "COMPANYCODE_TO_PLANT";
		public static final String PLANT_PURCHASEORG_LINKTYPE = "PLANT_TO_PURCHASEORG";
		public static final String PURCHASEORG_DOCTYPE_LINKTYPE = "PURCHASEORG_TO_DOCTYPE";
		public static final String DOCTYPE_PAYTERM_LINKTYPE = "DOCTYPE_TO_PAYTERM";
		
		//Material Level2 Constants
		public static final String VENDOR_L2 = "VENDOR_L2";
		public static final String PLANT_L2 = "PLANT_L2";
		public static final String RECIEVING_PLANT_L2 = "RECIEVING_PLANT_L2";
		public static final String COMPANY_CODE_L2 = "COMPANY_CODE_L2";
		public static final String SOLDTOPARTY_L2 = "SOLDTOPARTY_L2";
		//Link Constants
		public static final String L2_TIER_12 = "L2_TIER_12";
		public static final String L2_TIER_23IP = "L2_TIER_23IP";
		public static final String L2_TIER_23CC = "L2_TIER_23CC";
		public static final String L2_TIER_24 = "L2_TIER_24"; 
		public static final String L2_TIER_34 = "L2_TIER_34";
		
		public static final String FORM_DATA_DECRYPT_LIST = "FORM_DATA_DECRYPT_LIST";
		public static final String DATA_OBFUSCATION = "DATA_OBFUSCATION";
		
		public static final String HBASE_SOLR_TABLE = "HBASE_SOLR_TABLE";
		public static final String HBASE_ZOOKEPER_QUORUM = "HBASE_ZOOKEPER_QUORUM";
		public static final String HBASE_MASTER = "HBASE_MASTER";
		
		public static final String CDH_VERSION = "CDH_VERSION";
		
		public static final String SMARTFIND_EMAIL_ID = "SMARTFIND_EMAIL_ID";
		public static final String SMARTFIND_EMAIL_PASSWORD = "SMARTFIND_EMAIL_PASSWORD";
		public static final String SMTP_HOST = "SMTP_HOST";
		public static final String CONFIDENTIALITY_NOTE = "CONFIDENTIALITY_NOTE";
		 
		//Prod cassa-email
		public static final String CASSA_SOURCE_EMAIL_ID ="CASSA_SOURCE_EMAIL_ID";
		public static final String CASSA_EMAIL_PASSWORD ="CASSA_EMAIL_PASSWORD";
		public static final String CASSA_SMTP_HOST ="CASSA_SMTP_HOST";
		public static final String CASSA_TO_EMAIL_ID ="CASSA_TO_EMAIL_ID";
		public static final String CASSA_EMAIL_SUBJECT ="CASSA_EMAIL_SUBJECT";
		public static final String CASSA_EMAIL_GETDATA_PAGINATION_BODY ="CASSA_EMAIL_GETDATA_PAGINATION_BODY";
		public static final String CASSA_EMAIL_GETDATA_BODY="CASSA_EMAIL_GETDATA_BODY";
		public static final String prod_env ="prod_env";
		
		
		public static final String ROLE_ADMIN = "ROLE_ADMIN";
		
		public static final String solarAPACInstanceName = "SOLR_SERVER_APAC_INSTANCENAME";
		
		public static final String qfindRole = "ROLE_QFIND";
		
		public static final String integraApp = "INTEGRA";
		
		public static final String pakFindApp = "PAKFIND";
		
		public static final String ALL = "ALL";
		
		public static final String DIGICASE_DB = "DIGICASE_DB";
		
		public static final String IDMP_DB = "IDMP";
		
		public static final String excelDownloadLimit = "ExcelDownloadLimit";
		
		public static final String csvDownloadLimit = "CsvDownloadLimit";
		
		public static final String QFIND_DELTA_DOWNLOAD_DIRECTORY = "QFIND_DELTA_DOWNLOAD_DIRECTORY"; 
		
		public static final String QFIND_CUSTOM_QUERY = "QFIND_CUSTOM_QUERY";
		
		public static final String INTEGRA_BASEURL="INTEGRA_BASEURL";
		public static final String INTEGRA_CMS_USERID="INTEGRA_CMS_USERID";
		public static final String INTEGRA_CMS_PASSWORD="INTEGRA_CMS_PASSWORD";
		public static final String INTEGRA_CMS_NO_OF_DAYS="INTEGRA_CMS_NO_OF_DAYS";
		public static final String THREAD_POOL_COUNT ="THREAD_POOL_COUNT";
		
		//Princeton Server Details
		public static final String PRINCETON_USERNAME="PRINCETON_USERNAME";
		public static final String PRINCETON_HOST="PRINCETON_HOST";
		public static final String PRINCETON_PASSWORD="PRINCETON_PASSWORD";
		public static final String PRINCETON_XML_FILE_LOCATION="PRINCETON_XML_FILE_LOCATION";
		public static final String PRINCETON_PDF_FILE_LOCATION="PRINCETON_PDF_FILE_LOCATION";
		
		//Qfind Server Details
		public static final String QFIND_REMOTE_HOST="QFIND_REMOTE_HOST";
		public static final String QFIND_REMOTE_USERNAME="QFIND_REMOTE_USERNAME";
		public static final String QFIND_REMOTE_PASSWORD="QFIND_REMOTE_PASSWORD";
		public static final String QFIND_REMOTE_PDF_FILE_LOCATION="QFIND_REMOTE_PDF_FILE_LOCATION";
		
		//QA oracle DB details
		public static final String QA_ORACLE_DRIVER_CLASS="QA_ORACLE_DRIVER_CLASS";
		public static final String QA_ORACLE_HOST="QA_ORACLE_HOST";
		public static final String QA_ORACLE_USERNAME="QA_ORACLE_USERNAME";
		public static final String QA_ORCALE_PASSWORD="QA_ORCALE_PASSWORD";
		
		//QA postgres DB Details
		public static final String QA_POSTGRES_DRIVER_CLASS="QA_POSTGRES_DRIVER_CLASS";
		public static final String QA_POSTGRES_HOST="QA_POSTGRES_HOST";
		public static final String QA_POSTGRES_USERNAME="QA_POSTGRES_USERNAME";
		public static final String QA_POSTGRES_PASSWORD="QA_POSTGRES_PASSWORD";
		
		
		//Mysql DB details with SSH tunnel
		public static final String SSH_HOSTNAME="SSH_HOSTNAME";
		public static final String SSH_USERNAME="SSH_USERNAME";
		public static final String SSH_PASSWORD="SSH_PASSWORD";
		public static final String SSH_PORT="SSH_PORT";
		public static final String SSH_HOST="SSH_HOST";
		public static final String MYSQL_HOSTNAME="MYSQL_HOSTNAME";
		public static final String MYSQL_REMOTE_PORT="MYSQL_REMOTE_PORT";
		public static final String MYSQL_LOCAL_PORT="MYSQL_LOCAL_PORT";
		public static final String MYSQL_USERNAME="MYSQL_USERNAME";
		public static final String MYSQL_PASSWORD="MYSQL_PASSWORD";
		public static final String MYSQL_DBNAME="MYSQL_DBNAME";
		public static final String MYSQL_DB_DRIVER="MYSQL_DB_DRIVER";
}
