package com.relevance.prism.data;

public class Comment {
	
	private int id;
	private String profile_name;
	private String commentPanelId;
	private String userName;
	private String comment;	
	private String commentTimeStamp;
	private String userImage;
	private String filterQuery;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProfileName() {
		return profile_name;
	}
	public void setProfileName(String profile_name) {
		this.profile_name = profile_name;
	}
	public String getCommentPanelId() {
		return commentPanelId;
	}
	public void setCommentPanelId(String sectionName) {
		this.commentPanelId = sectionName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommentTimeStamp() {
		return commentTimeStamp;
	}
	public void setCommentTimeStamp(String commentTimeStamp) {
		this.commentTimeStamp = commentTimeStamp;
	}
	public String getUserImage() {
		return userImage;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public String getFilterQuery() {
		return filterQuery;
	}
	public void setFilterQuery(String filterQuery) {
		this.filterQuery = filterQuery;
	}	
}
