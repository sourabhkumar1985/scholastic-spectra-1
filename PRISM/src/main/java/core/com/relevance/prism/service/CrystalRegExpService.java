package com.relevance.prism.service;

import com.relevance.prism.dao.CrystalRegExpDao;

import com.relevance.prism.util.PrismHandler;

public class CrystalRegExpService extends BaseService {
	public String insertRegExpersion(String source, String database, String table, String type, String regex) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CrystalRegExpDao regExp = new CrystalRegExpDao();
		String resultString = null;
		try {
			resultString = regExp.insertRegExp(source, database, table, type, regex);
		} catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultString;
	}

}
