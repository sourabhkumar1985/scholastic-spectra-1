package com.relevance.prism.data;

public class Relationship {
	private String kNumber;
	private String kNumberFilledInternally;
	private String originalApplicant;
	private String receviedDate;
	private String clearedDate;
	private String productCodes;
	private String deviceClass;
	private String deviceListing;
	private String deviceListingStatus;

	public String getkNumber() {
		return kNumber;
	}

	public void setkNumber(String kNumber) {
		this.kNumber = kNumber;
	}

	public String getkNumberFilledInternally() {
		return kNumberFilledInternally;
	}

	public void setkNumberFilledInternally(String kNumberFilledInternally) {
		this.kNumberFilledInternally = kNumberFilledInternally;
	}

	public String getOriginalApplicant() {
		return originalApplicant;
	}

	public void setOriginalApplicant(String originalApplicant) {
		this.originalApplicant = originalApplicant;
	}

	public String getReceviedDate() {
		return receviedDate;
	}

	public void setReceviedDate(String receviedDate) {
		this.receviedDate = receviedDate;
	}

	public String getClearedDate() {
		return clearedDate;
	}

	public void setClearedDate(String clearedDate) {
		this.clearedDate = clearedDate;
	}

	public String getProductCodes() {
		return productCodes;
	}

	public void setProductCodes(String productCodes) {
		this.productCodes = productCodes;
	}

	public String getDeviceClass() {
		return deviceClass;
	}

	public void setDeviceClass(String deviceClass) {
		this.deviceClass = deviceClass;
	}
	
	public String getDeviceListing() {
		return deviceListing;
	}

	public void setDeviceListing(String deviceListing) {
		this.deviceListing = deviceListing;
	}

	public String getDeviceListingStatus() {
		return deviceListingStatus;
	}

	public void setDeviceListingStatus(String deviceListingStatus) {
		this.deviceListingStatus = deviceListingStatus;
	}
}
