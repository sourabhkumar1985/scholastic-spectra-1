package com.relevance.prism.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.AdhocReport;
import com.relevance.prism.service.AdhocReportService;
import com.relevance.prism.util.PrismHandler;

@Path("/adhocReport")
public class AdhocReportResource extends BaseResource{
	
	@Context
	ServletContext context;

	public AdhocReportResource(@Context ServletContext value) {
		this.context = value;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getadhocReports")
	public String getAdhocReports() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdhocReportService adhocReportService = new AdhocReportService();
			List<AdhocReport> adhocReportList = adhocReportService.getAdhocReports();
			Gson gson = new Gson();
			response = gson.toJson(adhocReportList);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getadhocReport")
	public String getAdhocReport(@FormParam("id") long id ) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			AdhocReportService adhocReportService = new AdhocReportService();
			AdhocReport adhocReport = adhocReportService.getAdhocReport(id);
			Gson gson = new Gson();
			response = gson.toJson(adhocReport);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/createadhocReport")
	public String createAdhocReport(@FormParam("adhocReportRequest") String adhocReportRequest) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		AdhocReport adhocReport = null;
		try{
			Gson gson = new Gson();
			adhocReport = gson.fromJson(adhocReportRequest, AdhocReport.class);	
			AdhocReportService adhocReportService = new AdhocReportService();
			response = adhocReportService.createAdhocReport(adhocReport);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateadhocReport")
	public String updateAdhocReport(@FormParam("adhocReportRequest") String adhocReportRequest) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		AdhocReport adhocReport = null;
		try{
			Gson gson = new Gson();
			adhocReport = gson.fromJson(adhocReportRequest, AdhocReport.class);	
			AdhocReportService adhocReportService = new AdhocReportService();
			response = adhocReportService.updateAdhocReport(adhocReport);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
