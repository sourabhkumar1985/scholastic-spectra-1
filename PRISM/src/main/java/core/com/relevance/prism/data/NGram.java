package com.relevance.prism.data;
public class NGram implements Comparable<NGram>{

	String text;
	int size;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	@Override
    public int compareTo(NGram o) {
        return o.size > this.size ? 1 : (o.size < this.size ? -1 : 0);
    }
}
