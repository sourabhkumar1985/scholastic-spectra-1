package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.rl.cms.refresh.CMSAction;
import com.rl.cms.refresh.FileJSONData;

public class UpdateDocumentAttributes extends BaseUtil{
	private PrismDbUtil dbUtil = null;
	private Connection con = null;
	private Connection postgresConn = null;
	private Session session = null;
	public UpdateDocumentAttributes(){
		dbUtil = new PrismDbUtil();
	}
	public FileJSONData updateDocumentAttributes(String fileId, String fileName, FileJSONData fileJsonData, String flag) throws Exception{
		try {

			String strSshUser = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_USERNAME); // SSH loging username
			String strSshPassword = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_PASSWORD); // SSH login password
			//String strSshHost = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_HOST); // hostname or ip
																// or
																// SSH server
			String strSshHost = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_HOSTNAME);
			int nSshPort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.SSH_PORT)); // remote SSH host port number
			String strRemoteHost =  E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_HOSTNAME); // hostname or
												// ip of
												// your
												// database
												// server
			int nLocalPort = Integer.parseInt( E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_LOCAL_PORT)); // local port number use to bind SSH tunnel
			int nRemotePort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_REMOTE_PORT)); // remote port number of your database
			String strDbUser = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_USERNAME); // database loging username
			String strDbPassword = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_PASSWORD); // database login password
			String dataBase = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_DBNAME);
			//int count = 0;
			String sql = "update qfind_pdf_categories set web_flag = ?,qfind_flag = ?, str_foldername = ?, str_category = ?, str_filedisplayname = ? where str_file_name = ?";
			PreparedStatement pst = null;
			postgresConn = getPostGresDBConnection();
			session = doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);

			Class.forName(E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_DB_DRIVER));
			con = DriverManager.getConnection("jdbc:mysql://localhost:" + nLocalPort + "/" + dataBase,
					strDbUser, strDbPassword);
			System.out.println("mysql connencted");
			Statement stmt = con.createStatement();
			ResultSet rs = null;
			if(flag.equalsIgnoreCase("A")){
				rs = stmt.executeQuery("select file_id,web_access,qfind_access,promotion,name, pathname, created_at, deleted_at from integra2015.content where file_id = " + fileId);
			}
			if(flag.equalsIgnoreCase("U")){
				rs = stmt.executeQuery("select file_id,web_access,qfind_access,promotion,name, pathname, updated_at, deleted_at from integra2015.content where file_id = " + fileId);
			}
			pst = postgresConn.prepareStatement(sql);
			boolean T = true;
			boolean F = false;
			while (rs.next()) {
				if (rs.getInt("web_access") == 1) {
					pst.setBoolean(1, T);
					fileJsonData.setWeb_flag("true");
				} else {
					pst.setBoolean(1, F);
					fileJsonData.setWeb_flag("false");
				}
				if (rs.getInt("qfind_access") == 1) {
					fileJsonData.setQfind_flag("true");
					pst.setBoolean(2, T);
				} else {
					pst.setBoolean(2, F);
					fileJsonData.setQfind_flag("false");
				}				
				pst.setString(3, rs.getString("pathname"));
				pst.setString(4, rs.getString("promotion")); // Nagaraj added for category
				pst.setString(5, rs.getString("name"));      // Nagaraj added for CMS file name
				if(flag.equalsIgnoreCase("A")){
					fileJsonData.setDate(rs.getTimestamp("created_at"));
					fileJsonData.setDeleted(false);
					fileJsonData.setAction("A");
				}									
				if(rs.getString("deleted_at") != null){
					if(flag.equalsIgnoreCase("U")){
						fileJsonData.setDate(rs.getTimestamp("deleted_at"));
						fileJsonData.setDeleted(true);
						fileJsonData.setAction("D");
					}					
				}else{
					if(flag.equalsIgnoreCase("U")){
						fileJsonData.setDate(rs.getTimestamp("updated_at"));
						fileJsonData.setDeleted(false);
						fileJsonData.setAction("U");
					}
				}
				pst.setString(6, fileName);
				// Nagaraj added
				pst.executeUpdate(); 
				//System.out.println(count++);				
			}
			return fileJsonData;
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			con.close();
			postgresConn.close();
			session.disconnect();
			PrismHandler.handleFinally(null, null, null);
		}
		return fileJsonData;
	}

	public CMSAction getCMSAction(String fileId, String flag) throws Exception{
		try {
			Date result = null;
			String strSshUser = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_USERNAME); // SSH loging username
			String strSshPassword = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_PASSWORD); // SSH login password
			//String strSshHost = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_HOST); // hostname or ip
																// or
																// SSH server
			String strSshHost = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_HOSTNAME);
			int nSshPort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.SSH_PORT)); // remote SSH host port number
			String strRemoteHost = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_HOSTNAME); // hostname or
												// ip of
												// your
												// database
												// server
			int nLocalPort = Integer.parseInt( E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_LOCAL_PORT)); // local port number use to bind SSH tunnel
			int nRemotePort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_REMOTE_PORT)); // remote port number of your database
			String strDbUser = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_USERNAME); // database loging username
			String strDbPassword = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_PASSWORD); // database login password
			String dataBase = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_DBNAME);
			session = doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);
			Class.forName(E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_DB_DRIVER));
			con = DriverManager.getConnection("jdbc:mysql://localhost:" + nLocalPort + "/" + dataBase, strDbUser, strDbPassword);
			System.out.println("mysql connencted");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select created_at, updated_at, deleted_at from integra2015.content where file_id = " + fileId);
			CMSAction cmsAction = new CMSAction();
			while (rs.next()) {
				if(flag.equalsIgnoreCase("A")){
					cmsAction.setAction("A");
					cmsAction.setDate(rs.getTimestamp("created_at"));
				}
				if(flag.equalsIgnoreCase("U")){
					if(rs.getDate("deleted_at") != null){
						cmsAction.setAction("D");
						cmsAction.setDate(rs.getTimestamp("deleted_at"));
					}else{
						cmsAction.setAction("U");
						cmsAction.setDate(rs.getTimestamp("updated_at"));
					}
				}
			}
			return cmsAction;
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			con.close();
			session.disconnect();
			PrismHandler.handleFinally(null, null, null);
		}
		return null;
	}
	
	private static Session doSshTunnel(String strSshUser, String strSshPassword, String strSshHost, int nSshPort,
			String strRemoteHost, int nLocalPort, int nRemotePort) throws JSchException {
		final JSch jsch = new JSch();
		Session session = jsch.getSession(strSshUser, strSshHost, 22);
		session.setPassword(strSshPassword);
		//String boundaddress ="0.0.0.0";
		
		final Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);

		session.connect();
		session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
		return session;
	}
	
	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}
}
