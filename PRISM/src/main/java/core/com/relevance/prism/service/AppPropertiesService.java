package com.relevance.prism.service;

import java.util.List;
import java.util.Map;

import com.relevance.prism.dao.AppPropertiesDao;
import com.relevance.prism.data.AppProperties;
import com.relevance.prism.data.AppProperty;
import com.relevance.prism.data.Category;
import com.relevance.prism.data.CharacterWordItem;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.FormData;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class AppPropertiesService extends BaseService {

	public AppProperties getProperties() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperties appProperties = null;
		try{
			Log.Info("getProperties method called on AppPropertiesService");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			appProperties = appPropertiesDao.getProperties();		
			Log.Info("Fetched app properties from AppPropertiesService");
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		
		return appProperties;
	}
	
	/*
	public String updateDataObfuscation(Object... obj) throws Exception{
		String responseMessage = null;
		try{
			Log.Info("updateDataObfuscation() method called on DataObfuscationService");
			AppPropertiesDao dataObfuscationDao = new AppPropertiesDao();
			DataObfuscationConfig dataObfuscationConfig = new DataObfuscationConfig();
			dataObfuscationConfig.enabled = Long.parseLong(obj[0].toString());
			responseMessage = dataObfuscationDao.updateDataObfuscation(dataObfuscationConfig);
			return responseMessage;
		}catch(Exception appe){
			throw appe;
		}catch(Exception e){
			Log.Error("Exception while updating Data Obfuscation in DB " + e.getMessage());
			Log.Error(e);
		}
		return responseMessage;
	}
	*/
	
	public Map<String, FormData> getFormDataMap() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, FormData> formDataMap = null;
		try{
			Log.Info("getFormDataMap() method called on AppPropertiesService");
			AppPropertiesDao dataObfuscationDao = new AppPropertiesDao();
			formDataMap = dataObfuscationDao.getFormDataMap();
			return formDataMap;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return formDataMap;
	}
	
	
	public Map<String, CharacterWordItem> getObfuscationWordMap(boolean reverse) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, CharacterWordItem> wordMap = null;
		try{
			Log.Info("getObfuscationWordMap() method called on AppPropertiesService");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			wordMap = appPropertiesDao.getObfuscationWordMap(reverse);
			return wordMap;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return wordMap;
	}
	
	
	public Map<Character, CharacterWordItem> getObfuscationCharMap(boolean reverse) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<Character, CharacterWordItem> charMap = null;
		try{
			Log.Info("getObfuscationCharMap() method called on AppPropertiesService");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			charMap = appPropertiesDao.getObfuscationCharMap(reverse);
			return charMap;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return charMap;
	}
	
	public String insertFormData(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		FormData formData = new FormData();
		String responseMessage = null;
		try{
			Log.Info("insertFormData method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			formData.setName(obj[0].toString());
			if(obj[1].toString().equalsIgnoreCase("1")){
				formData.setObfuscate(true);
			} else {
				formData.setObfuscate(false);
			}
			
			if(obj[2].toString().equalsIgnoreCase("1")){
				formData.setDeObfuscate(true);
			} else {
				formData.setDeObfuscate(false);
			}			
						
			formData.setRemarks(obj[3].toString());
			formData.setObfType(Integer.parseInt(obj[4].toString()));
			
			responseMessage = appPropertiesDao.insertFormData(formData);
			Log.Info("Form Data inserted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String insertObfWords(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CharacterWordItem obfWords = new CharacterWordItem();
		String responseMessage = null;
		try{
			Log.Info("insertFormData method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			obfWords.setWordFrom(obj[0].toString());
			obfWords.setWordTo(obj[1].toString());
			
			
			responseMessage = appPropertiesDao.insertObfWords(obfWords);
			Log.Info("Obf words inserted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String insertAppProperties(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperty appProps = new AppProperty();
		String responseMessage = null;
		try{
			Log.Info("insertFormData method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			appProps.setName(obj[0].toString());
			appProps.setValue(obj[1].toString());
			appProps.setRemarks(obj[2].toString());
			appProps.setType(obj[3].toString());
			responseMessage = appPropertiesDao.insertAppProps(appProps);
			Log.Info("apps props inserted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String updateFormData(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		FormData formData = new FormData();
		String responseMessage = null;
		try{
			Log.Info("updateFormData() method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			formData.setName(obj[0].toString());
			if(obj[1].toString().equalsIgnoreCase("1")){
				formData.setObfuscate(true);
			} else {
				formData.setObfuscate(false);
			}
			
			if(obj[2].toString().equalsIgnoreCase("1")){
				formData.setDeObfuscate(true);
			} else {
				formData.setDeObfuscate(false);
			}			
						
			formData.setRemarks(obj[3].toString());
			formData.setObfType(Integer.parseInt(obj[4].toString()));
			
			responseMessage = appPropertiesDao.updateFormData(formData);
			Log.Info("Form Data Updated in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String updateObfWords(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CharacterWordItem obfWords = new CharacterWordItem();
		String responseMessage = null;
		try{
			Log.Info("updateObfWords() method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			obfWords.setWordFrom(obj[0].toString());
			obfWords.setWordTo(obj[1].toString());
			
			
			responseMessage = appPropertiesDao.updateObfWords(obfWords);
			Log.Info("Obf words updated in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String updateAppProperties(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperty appProperties = new AppProperty();
		String responseMessage = null;
		try{
			Log.Info("updateAppProperties() method called on ReportsService");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			appProperties.setName((String) obj[0]);
			appProperties.setValue((String) obj[1]);
			appProperties.setRemarks((String) obj[2]);
			appProperties.setType((String) obj[3]);	
			responseMessage = appPropertiesDao.updateAppProps(appProperties);
			Log.Info("Updated App Properties in table.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	
	}
	
	public String deleteFormData(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		FormData formData = new FormData();
		String responseMessage = null;
		try{
			Log.Info("deleteFormData method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			formData.setName(obj[0].toString());
						
			responseMessage = appPropertiesDao.deleteFormData(formData);
			Log.Info("Form Data deleted DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteObfWords(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CharacterWordItem obfWords = new CharacterWordItem();
		String responseMessage = null;
		try{
			Log.Info("deleteObfWords method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			obfWords.setWordFrom(obj[0].toString());
			
			responseMessage = appPropertiesDao.deleteObfWords(obfWords);
			Log.Info("Obf words delted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteAppProperties(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperty appProps = new AppProperty();
		String responseMessage = null;
		try{
			Log.Info("deleteAppProperties method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			appProps.setName(obj[0].toString());
					
			responseMessage = appPropertiesDao.deleteAppProps(appProps);
			Log.Info("apps props deleted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public List<Category> getCategoriesList() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Category> categoriesList = null;
		try{
			Log.Info("getCategoriesList() method called on AppPropertiesService");
			AppPropertiesDao dataObfuscationDao = new AppPropertiesDao();
			categoriesList = dataObfuscationDao.getCategoriesList(E2emfConstants.ALL);
			return categoriesList;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return categoriesList;
	}
	
	public String insertCategory(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Category category = new Category();
		String responseMessage = null;
		try{
			Log.Info("insertCategory method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			category.setParent(obj[0].toString());
			category.setParentId(obj[1].toString());
			category.setCategory(obj[2].toString());
			category.setKeywords(obj[3].toString());
					
			responseMessage = appPropertiesDao.insertCategory(category);
			Log.Info("Category inserted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String updateCategory(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Category category = new Category();
		String responseMessage = null;
		try{
			Log.Info("updateCategory() method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			if (obj[0] != null)
				category.setParent(obj[0].toString());
			if (obj[1] != null)
				category.setParentId(obj[1].toString());
			category.setCategoryId(obj[2].toString());
			category.setCategory(obj[3].toString());
			category.setKeywords(obj[4].toString());
			
			responseMessage = appPropertiesDao.updateCategory(category);
			Log.Info("Category Updated in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteCategory(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Category categoryId = new Category();
		String responseMessage = null;
		try{
			Log.Info("deleteCategory method called on App Properties service");
			AppPropertiesDao appPropertiesDao = new AppPropertiesDao();
			
			categoryId.setCategoryId(obj[0].toString());
					
			responseMessage = appPropertiesDao.deleteCategory(categoryId);
			Log.Info("Category deleted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
}
