package com.relevance.prism.data;

import com.relevance.prism.data.Row;


public class Query {

	private String query;
	private Row defaultValues;
	
	public Row getDefaultValues() {
		return defaultValues;
	}
	public void setDefaultValues(Row defaultValues) {
		this.defaultValues = defaultValues;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	
	
}
