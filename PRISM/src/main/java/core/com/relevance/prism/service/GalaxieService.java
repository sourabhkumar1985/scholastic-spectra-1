package com.relevance.prism.service;

import java.util.List;

import com.relevance.prism.dao.GalaxieDao;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SmartFindDocument;

public class GalaxieService extends BaseService {
	
	public List<SmartFindDocument> getDetailsByPartNumber(String partNumber)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<SmartFindDocument> smartFindDocumentList = null;
		try {
			GalaxieDao galaxieDao = new GalaxieDao();
			smartFindDocumentList = galaxieDao.getDetailsByPartNumber(partNumber);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return smartFindDocumentList;
	}

}
