package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.relevance.prism.data.Traceability;
import com.relevance.prism.data.TraceabilityTree;
import com.relevance.prism.util.PrismHandler;

public class TraceabilityDao extends BaseDao {
	private Connection con;
	private Statement stmt;
	private String child;
	private String parent;
	private List<String> suportingColumnList = new ArrayList<>();
	private StringBuilder query;
	private String direction;
	private String fieldName;

	public List<TraceabilityTree> getTraceability(String source,
			String database, String table, Traceability traceability)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserDao dataAnalyserDao;
		String suportingColumns = null;
		String key;
		List<TraceabilityTree> traceabilityTreeList = null;
		try {
			query = new StringBuilder();
			suportingColumns = traceability.getSuportingColumns();
			key = traceability.getKey();
			child = traceability.getChild();
			parent = traceability.getParent();
			direction = traceability.getDirection();
			if ("forward".equalsIgnoreCase(direction))
				fieldName = parent;
			else if (parent != null)
				fieldName = child;

			query.append("SELECT ").append(child).append(" , ").append(parent);
			if (suportingColumns != null && !suportingColumns.trim().isEmpty())
				query.append(" , ").append(suportingColumns);

			query.append(" FROM ").append(table);

			if (suportingColumns != null && !suportingColumns.trim().isEmpty())
				suportingColumnList = Arrays
						.asList(suportingColumns.split(","));
			dataAnalyserDao = new DataAnalyserDao();
			con = dataAnalyserDao.getDatabaseConnection(source, database);
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);

			traceabilityTreeList = recursiveTraceability(key, 0);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return traceabilityTreeList;
	}

	private List<TraceabilityTree> recursiveTraceability(String fieldValue,
			int level) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet rs = null;
		TraceabilityTree traceabilityTree;
		List<TraceabilityTree> traceabilityTreeList = new ArrayList<>();
		HashMap<String, String> suportingColumnMap = new HashMap<>();
		StringBuilder recursiveQuery = new StringBuilder();
		try {
			if (level > 10)
				return traceabilityTreeList;
			
			recursiveQuery.append(query);
			if (fieldValue != null && !fieldValue.isEmpty()) {
				recursiveQuery.append(" WHERE ").append(fieldName).append(" = '")
						.append(fieldValue).append("'");
			} else {
				recursiveQuery.append(" WHERE ").append(parent).append(" is NULL");
			}
			System.out.println(recursiveQuery);
			rs = stmt.executeQuery(recursiveQuery.toString());
			while (rs.next()) {
				traceabilityTree = new TraceabilityTree();
				String childValue = rs.getString(child);
				String parentValue = rs.getString(parent);
				if ("forward".equalsIgnoreCase(direction))
					traceabilityTree.setChildren(recursiveTraceability(
							childValue, level+1));
				else if (parent != null)
					traceabilityTree.setChildren(recursiveTraceability(
							parentValue, level+1));
				traceabilityTree.setParent(parentValue);
				traceabilityTree.setKey(childValue);
				
				for (String suportingColumn : suportingColumnList) {
					suportingColumnMap.put(suportingColumn,
							rs.getString(suportingColumn));
				}
				traceabilityTree.setSupportingColumns(suportingColumnMap);
				traceabilityTreeList.add(traceabilityTree);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
			PrismHandler.handleFinally(con, rs, stmt);
		} finally {
			PrismHandler.handleFinally(null, rs, null);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return traceabilityTreeList;
	}
	
	public Map<String,String> getBOM(String key,
			String direction)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserDao dataAnalyserDao;
		ResultSet rs = null;
		StringBuilder query = new StringBuilder();
		HashMap<String,String> output = new HashMap<>();
		try {
			dataAnalyserDao = new DataAnalyserDao();
			con = dataAnalyserDao.getDatabaseConnection("postgres", "SolenisPostGres");
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			query.append("SELECT * FROM bom_output WHERE key='").append(key).append("'");
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				output.put(rs.getString("direction"), rs.getString("value"));
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return output;
	}
}
