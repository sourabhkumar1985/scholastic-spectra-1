package com.relevance.prism.data;

 
public class PrismException {
	
	String message = "FAILURE";
	String exceptionType = null;
	String originalExceptionMessage = null;
	String customMessage = null;
	String isException = "false";
	String className = null;
	String methodName = null;
	int lineNumber = 0;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getExceptionType() {
		return exceptionType;
	}
	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}
	public String getOriginalExceptionMessage() {
		return originalExceptionMessage;
	}
	public void setOriginalExceptionMessage(String originalExceptionMessage) {
		this.originalExceptionMessage = originalExceptionMessage;
	}
	public String getCustomMessage() {
		return customMessage;
	}
	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}
	public String getIsException() {
		return isException;
	}
	public void setIsException(String isException) {
		this.isException = isException;
	}	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public int getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
		
}


