package com.relevance.prism.data;

public class DistanceObject {

	public int type;
	public String typeDesc;
	public String typeCategory;
	public double distance;
	public String distnaceSupplier;
	public String algorithm;
	public float priceDiff;
	public int  weightage;
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getTypeDesc() {
		return setDefaultvalue(typeDesc);
	}
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	public String getTypeCategory() {
		return setDefaultvalue(typeCategory);
	}
	public void setTypeCategory(String typeCategory) {
		this.typeCategory = typeCategory;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public String getDistnaceSupplier() {
		return setDefaultvalue(distnaceSupplier);
	}
	public void setDistnaceSupplier(String distnaceSupplier) {
		this.distnaceSupplier = distnaceSupplier;
	}
	public String getAlgorithm() {
		return setDefaultvalue(algorithm);
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public float getPriceDiff() {
		return priceDiff;
	}
	public void setPriceDiff(float priceDiff) {
		this.priceDiff = priceDiff;
	}
	public int getWeightage() {
		return weightage;
	}
	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}
	public String setDefaultvalue(String obj){
		if (obj == null || obj.equalsIgnoreCase("null"))
			obj =  "Not Available";
		return obj;
	}
}
