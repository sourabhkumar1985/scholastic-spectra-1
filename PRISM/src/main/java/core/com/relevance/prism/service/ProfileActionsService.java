package com.relevance.prism.service;

import java.util.List;

import com.relevance.prism.dao.ProfileActionsDao;
import com.relevance.prism.data.ProfileAction;
import com.relevance.prism.util.PrismHandler;

public class ProfileActionsService extends BaseService{
	public List<ProfileAction> getProfileActions(int profileID, int id,String profileIds) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ProfileAction> profileActionList = null;
		try {
			ProfileActionsDao profileActionsDao = new ProfileActionsDao();
			profileActionList = profileActionsDao.getProfileActions(profileID,id,profileIds);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return profileActionList;
	}
	public List<ProfileAction> saveProfileAction(ProfileAction profileAction) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ProfileAction> profileActionList = null;
		try {
			ProfileActionsDao profileActionsDao = new ProfileActionsDao();
			profileActionList = profileActionsDao.saveProfileAction(profileAction);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return profileActionList;
	}
}
