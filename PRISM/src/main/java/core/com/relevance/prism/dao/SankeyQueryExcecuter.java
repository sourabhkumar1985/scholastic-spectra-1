package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.SankeyConfig;
import com.relevance.prism.data.SankeyFilter;
import com.relevance.prism.data.SankeyLink;
import com.relevance.prism.util.PrismHandler;

public class SankeyQueryExcecuter extends BaseDao implements Callable<List<FlowData>> {
	private String source;
	private String paramCon;
	private SankeyLink sConfig;
	private SankeyConfig sankeyConfig;

	public SankeyQueryExcecuter(String source, String paramCon, SankeyLink sConfig, SankeyConfig sankeyConfig) {
		this.source = source;
		this.paramCon = paramCon;
		this.sConfig = sConfig;
		this.sankeyConfig = sankeyConfig;
	}

	@Override
	public List<FlowData> call() throws Exception {
		List<FlowData> nGramHashMapTemp = null;
		try {
			nGramHashMapTemp = executeQuery(source, paramCon, sConfig, sankeyConfig);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return nGramHashMapTemp;
	}

	public List<FlowData> executeQuery(String source, String paramCon, SankeyLink sConfig, SankeyConfig sankeyConfig)
			throws Exception {
		Connection con = null;
		Statement stmt = null;
		String query = null;
		StringBuilder sb = null;
		ArrayList<StringBuilder> conditionList = null;
		final String AND_STR = " AND ";
		ResultSet rs = null;
		List<FlowData> flowDataList = new ArrayList<>();
		try {
			if ("HIVE".equalsIgnoreCase(source)) {
				con = dbUtil.getHiveConnection(paramCon);
			} else {
				con = dbUtil.getPostGresConnection(paramCon);
			}
			if (con == null)
				return flowDataList;
			stmt = con.createStatement();
			query = sConfig.getQuery();

			if (sankeyConfig.getSankeyFilterMap() != null && sConfig.getFilterKeyList() != null) {
				conditionList = new ArrayList<>();
				for (String filterKey : sConfig.getFilterKeyList()) {
					SankeyFilter sf = sankeyConfig.getSankeyFilterMap().get(filterKey);
					if (SankeyFilter.FIELD_TYPE_DATE.equalsIgnoreCase(sf.getFieldType())) {
						if (sf.getValue1() != null && sf.getValue1().length() > 0) {
							sb = new StringBuilder();
							sb.append("unix_timestamp(" + sf.getSearchField() + ", 'MM/dd/yyyy')");
							sb.append(" >= ");
							sb.append("unix_timestamp('" + sf.getValue1() + "', 'MM/dd/yyyy')");
							conditionList.add(sb);

						}

						if (sf.getValue2() != null && sf.getValue2().length() > 0) {
							sb = new StringBuilder();
							sb.append("unix_timestamp(" + sf.getSearchField() + ", 'MM/dd/yyyy')");
							sb.append(" <= ");
							sb.append("unix_timestamp('" + sf.getValue2() + "', 'MM/dd/yyyy')");
							conditionList.add(sb);
						}
					} else if (SankeyFilter.FIELD_TYPE_NUM.equalsIgnoreCase(sf.getFieldType())) {
						if (sf.getValue1() != null && sf.getValue1().length() > 0) {
							sb = new StringBuilder();
							sb.append(sf.getSearchField());
							sb.append(" >= ");
							sb.append(sf.getValue1());
							conditionList.add(sb);
						}

						if (sf.getValue2() != null && sf.getValue2().length() > 0) {
							sb = new StringBuilder();
							sb.append(sf.getSearchField());
							sb.append(" <= ");
							sb.append(sf.getValue2());
							conditionList.add(sb);
						}
					} else {
						if (sf.getValue1() != null && sf.getValue1().length() > 0) {
							sb = new StringBuilder();
							String searchField = sf.getSearchField();
							if (searchField.indexOf("<Q>") > -1) {
								searchField = searchField.replace("<Q>", sf.getValue1());
								sb.append(searchField);
							} else {
								sb.append(sf.getSearchField());
								sb.append(" in ( ");
								sb.append(sf.getValue1());
								sb.append(") ");
							}
							conditionList.add(sb);
						}
					}
				}
				StringBuilder consolidatedCondList = new StringBuilder();
				for (StringBuilder cond : conditionList) {
					consolidatedCondList.append(cond.toString());
					consolidatedCondList.append(AND_STR);
				}
				consolidatedCondList.append(" (1 = 1) ");
				query = query.replace("<FILTER>", consolidatedCondList.toString());
			}
			rs = stmt.executeQuery(query);
			if (rs != null) {
				while (rs.next()) {
					FlowData flowData = new FlowData();
					flowData.setFromCode(rs.getString("source_code"));
					flowData.setFromName(rs.getString("source_name"));
					flowData.setFromType(rs.getString("source_type"));
					flowData.setToCode(rs.getString("target_code"));
					flowData.setToName(rs.getString("target_name"));
					flowData.setToType(rs.getString("target_type"));
					flowData.setLinkValue(rs.getDouble("link_value"));
					if (query.contains("link_value_suffix")) {
						flowData.setLinkValueSuffix(rs.getString("link_value_suffix"));
					} else {
						flowData.setLinkValueSuffix(null);
					}
					if (query.contains("link_value_prefix")) {
						flowData.setLinkValuePrefix(rs.getString("link_value_prefix"));
					} else {
						flowData.setLinkValuePrefix(null);
					}
					flowData.setLinkType(rs.getString("link_type"));
					if (query.contains("link_tooltip")) {
						String toolTip = rs.getString("link_tooltip");
						flowData.setLinkToolTip(toolTip);
					} else {
						flowData.setLinkToolTip(null);
					}
					flowData.setFrom(sConfig.getFromStack());
					flowData.setTo(sConfig.getToStack());
					flowData.setFromColor(sConfig.getFromStackColor());
					flowData.setToColor(sConfig.getToStackColor());
					flowData.setLinkColor(sConfig.getLinkColor());
					flowDataList.add(flowData);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return flowDataList;
	}
}
