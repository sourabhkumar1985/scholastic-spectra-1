package com.relevance.prism.dao;

import java.util.List;

import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
/**
 * 
 * E2emf Data Access Interface  
 *
 */
public interface E2emfDao {
	
	
	public String executeQuery(String nodeLevelQuery) throws Exception;
	
	public String executeQuery(Object... obj) throws Exception;
	
	public List<FlowData> getFlowDataList(Object... obj);

	public E2emfBusinessObject getBusinessObject();
	
	public E2emfBusinessObject getBusinessObject(Object... obj) throws Exception;
	
	public E2emfView getDefaultViewDetails(Object... obj) throws Exception;
	
	public GoodsHistoryList getGoodsHistory(Object... obj) throws Exception;

	public E2emfView getLevel2ViewDetails(Object... obj) throws Exception;
	
	public E2emfView getLevel3ViewDetails(Object... obj) throws Exception;
	
	public MaterialReportList getMaterialReport(Object... obj) throws Exception;
	
	public E2emfBusinessObject getActions(Object[] obj) throws Exception;

	public E2emfBusinessObject getRoles(Object[] obj) throws Exception;

	public E2emfBusinessObject getPrimaryRoles(Object[] obj) throws Exception;
	
}
