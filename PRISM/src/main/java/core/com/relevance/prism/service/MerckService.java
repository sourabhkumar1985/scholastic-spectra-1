package com.relevance.prism.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import com.relevance.prism.dao.MerckDao;
import com.relevance.prism.dao.NGramDao;
import com.relevance.prism.data.BOM;
import com.relevance.prism.data.CrystalAgilePart;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;

public class MerckService extends BaseService {
	//E2emfAppUtil appUtil;
	PrismDbUtil dbUtil;
	Connection con = null;
	public MerckService() throws Exception{
		//appUtil = new E2emfAppUtil();	
		dbUtil = new PrismDbUtil();
	}

	
	public BOM getCrystalBOM(String key, String database) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<CrystalAgilePart> skuList = null;
		BOM bom = null;
		try {
			bom = new BOM();
			MerckDao merckDao = new MerckDao();
			con = dbUtil.getPostGresConnection(database);
			skuList = merckDao.getBOMCrystal(con, key,3);
			if(skuList != null && !skuList.isEmpty()) {				
				bom.setForward(skuList.get(0));
			}
			merckDao = new MerckDao();
			skuList = merckDao.getBackBOMCrystal(con, key,2);
			if(skuList != null && !skuList.isEmpty()) {
				bom.setBackward(skuList.get(0));	
			}			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		finally {
			PrismHandler.handleFinally(con, null, null);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return bom;
	}
	
	public List<String> getMongoDBCollectionList(String database) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> collectionList = null;
		try {
			MerckDao merckDao = new MerckDao();
			collectionList = merckDao.getMongoDBCollectionList(database);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return collectionList;
	}
	
	public String[] getMongoDBConnection(String database, String collection) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String[] result = {"false"};	
		try {
			MerckDao merckDao = new MerckDao();
			result = merckDao.getMongoDBConnection(database, collection);	
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return result;
	}
	
	public List<String> executeMongoQuery(String database, String collection, String query, String options) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> documentList =new ArrayList<>();
		try {
			MerckDao merckDao = new MerckDao();
			documentList = merckDao.executeMongoQuery(database,collection,query, options);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return documentList;
	}
	
	public String updateDuplicate(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("getNgramFilter method called on NGramService");
			MerckDao merckDao = new MerckDao();
			
			responseMessage = merckDao.updateDuplicate(obj);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
}
