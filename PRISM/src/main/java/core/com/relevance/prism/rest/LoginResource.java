package com.relevance.prism.rest;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.Role;
import com.relevance.prism.service.UserService;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;

/**
 * 
 * E2emf GlobalBatchResource 
 *
 */
@Path("/login")
public class LoginResource extends BaseResource {	
	
	@Context
	ServletContext context;
	//private static E2emfAppUtil appUtil = null;
	
	public LoginResource(@Context ServletContext value){
		this.context = value;
		//appUtil = new E2emfAppUtil();
		// System.out.println("Conext:" + this.context);
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)	
	@Path("/getappinfo")
	public String getBuildInformation() throws IOException,Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response =  null;
		try{
		/*String jenkinsBuild = null;
		String svnRevision = null;
		
		//jenkinsBuild = Manifests.read("Implementation-Build1");
		//svnRevision = Manifests.read("Implementation-SvnRevision");
		
		Properties prop = new Properties();
		prop.load(context.getResourceAsStream("/META-INF/MANIFEST.MF"));
		//System.out.println("All attributes:" + prop.stringPropertyNames());
		//System.out.println(prop.getProperty("{whatever attribute you want}"));
		
		jenkinsBuild = prop.getProperty("Implementation-Build1");
		svnRevision = prop.getProperty("Implementation-SvnRevision");
		
		Log.Info("jenkins Build : " + jenkinsBuild);
		Log.Info("Svn Revision : " + svnRevision);
		
		String release = appUtil.getAppProperty(E2emfConstants.release);
		
		return release+"-"+jenkinsBuild +"(Rev# "+svnRevision+")" ;*/
		
		/*StringBuilder buildInfo = new StringBuilder();
		buildInfo.append(E2emfAppUtil.getAppProperty(E2emfConstants.release));
		buildInfo.append("-");
		buildInfo.append(E2emfAppUtil.getAppVersionProperty(E2emfConstants.revisionNo));
		buildInfo.append("(");
		buildInfo.append(E2emfAppUtil.getAppVersionProperty(E2emfConstants.lastChangeDate));
		buildInfo.append(")");
		return buildInfo.toString();*/

		HashMap<String, String> appInfo = new HashMap<String, String>();
		appInfo.put(E2emfConstants.release, E2emfAppUtil.getAppProperty(E2emfConstants.release));
		appInfo.put(E2emfConstants.revisionNo, E2emfAppUtil.getAppVersionProperty(E2emfConstants.revisionNo));
		appInfo.put(E2emfConstants.lastChangeDate, E2emfAppUtil.getAppVersionProperty(E2emfConstants.lastChangeDate));
		//appInfo.put(E2emfConstants.loginLogo, E2emfAppUtil.getAppProperty(E2emfConstants.loginLogo));
		//appInfo.put(E2emfConstants.loginFavicon, E2emfAppUtil.getAppProperty(E2emfConstants.loginFavicon));
		//appInfo.put(E2emfConstants.loginContent, E2emfAppUtil.getAppProperty(E2emfConstants.loginContent));
		
		Gson gson = new Gson();
		response = gson.toJson(appInfo);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
		
	}	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getlogincontent")
	public String getLoginContent(@FormParam("appname") String appname) throws IOException,Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response =  null;
		try{
			UserService userService = new UserService();
			Role role = userService.getRole(appname);
			Gson gson = new Gson();
			response = gson.toJson(role);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
		
	}	
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)	
	@Path("/getappversion")
	public String getAppVersion() throws IOException ,Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response =  null;
		try{
			response = E2emfAppUtil.getAppVersionProperty(E2emfConstants.revisionNo);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;

	}	
	
}
