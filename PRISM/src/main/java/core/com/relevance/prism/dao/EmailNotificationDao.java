package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.json.XML;

import com.relevance.prism.data.DataAnalyserView;
import com.relevance.prism.data.EmailNotification;
import com.relevance.prism.data.ProfileConfig;
import com.relevance.prism.data.ProfileView;
import com.relevance.prism.data.TableDetails;
import com.relevance.prism.service.DataAnalyserService;
import com.relevance.prism.service.MailService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class EmailNotificationDao extends BaseDao{
	
	/*public static void main(String args[]){
		try{
			String inputString = "{\"infile\": [{\"field1\": 11,\"field2\": 12,\"field3\": 13},{\"field1\": 21,\"field2\": 22,\"field3\": 23},{\"field1\": 31,\"field2\": 32,\"field3\": 33}]}";
			convertJSONToCSV(inputString);
		}catch(Exception e){
			Log.Error(e);
		}
	}*/
	
	public String saveEmailNotification(EmailNotification emailNotification, String path) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String responseMessge = null;
		StringBuilder query = new StringBuilder();
		String createdBy = null;
		int profileId;
		int viewId;
		int notificationId =0;
		PreparedStatement ps = null;
		ProfileConfig profileConfig = null;
		try{
			profileId = emailNotification.getProfileId();
			createdBy = emailNotification.getCreatedBy();
			viewId =  emailNotification.getViewId();
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			query.append("select id from email_notifications where profile_id=")
			.append(profileId)
			.append(" and view_id=").append(viewId)
			.append(" and created_by ='").append(createdBy).append("'");
			rs = stmt.executeQuery(query.toString());
			if (rs.next()) {
				notificationId = rs.getInt(1);
			}
			if (notificationId == 0){
				query = new StringBuilder("INSERT INTO email_notifications(name, dist_email, profile_id,created_by, content, view_id,subject, frequency, frequency_day,enabled,file_format,app_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)");
			}else{
				query = new StringBuilder("UPDATE email_notifications SET name=?, dist_email=?, profile_id=?, profile_name=?, content=?, view_id=?, subject=?, frequency=?, frequency_day=?, enabled=?, file_format=? ,app_name=?  ,modified_on=? WHERE id = ?");
				
			}
			ps = con.prepareStatement(query.toString());
			ps.setString(1, emailNotification.getName());
			ps.setString(2, emailNotification.getDistEmail());
			ps.setInt(3, emailNotification.getProfileId());
			ps.setString(4, emailNotification.getCreatedBy());
			ps.setString(5, emailNotification.getContent());
			ps.setInt(6, emailNotification.getViewId());
			ps.setString(7, emailNotification.getSubject());
			ps.setString(8, emailNotification.getFrequency());
			ps.setInt(9, emailNotification.getFrequencyDay());
			ps.setBoolean(10, emailNotification.isEnabled());
			ps.setString(11, emailNotification.getFileformat());
			ps.setString(12, emailNotification.getAppName());
			if (notificationId != 0){
				 java.util.Date date= new java.util.Date();
				ps.setTimestamp(13,  new Timestamp(date.getTime()));
				ps.setInt(14, notificationId);
			}
			ps.addBatch();
			ps.executeBatch();
			if (("once").equalsIgnoreCase(emailNotification.getFrequency())){
				profileConfig = new ProfileConfig();
				profileConfig.setId(profileId);
				profileConfig.setViewID(viewId);
				DataAnalyserService dataAnalyserService = new DataAnalyserService();
				//ProfileView profileView = getProfileViewDetails(viewId, con);
				List<TableDetails> tableDetailsList = dataAnalyserService.getProfileTableDetails(emailNotification);
                MailService mailService = new MailService();
                String profile=" ";
				if( emailNotification.getViewId() !=0){
					path+= "?viewId$=$" + emailNotification.getViewId();
				}
				profile=emailNotification.getContent() +  " <br />  <br /> <br /> <a href='"+ path + "' > Click here </a> " + " &nbsp;" + " to view the application";
				mailService.sendEmailNotifications(tableDetailsList, profile, emailNotification.getSubject(), emailNotification.getDistEmail(),emailNotification.getFileformat());				
			
			}
			responseMessge = "{\"message\":\"Email Notification saved successfully.\"}";
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally { 
			if(ps!= null){
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessge;
	}
	
	public String deleteEmailNotification(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		EmailNotification param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (EmailNotification) obj[0];
			Log.Info("Deleting the Email Notification from DB : " + param);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String deleteQuery = "delete from email_notifications where id = "	+ param.getId();
				stmt.executeUpdate(deleteQuery);
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}	
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"Successfully deleted the Email Notification from DB\"}";
	}
	
	public String convertJSONToXML(String jsonStr) throws Exception{
		String xml = null;
		try{
			JSONObject json = new JSONObject(jsonStr);
			xml = XML.toString(json);
		}catch(Exception e){
			Log.Error(e);
		}
		return xml;
	}
	
	/*public void convertJSONToCSV(JSONArray data) throws Exception{
		try{
		     //JSONObject response = new JSONObject(jsonStr);
		     String tDir = System.getProperty("java.io.tmpdir");
			 String filePath = tDir + File.separator + "mismatches.csv";
		     //JSONArray docs = response.getJSONArray("infile");
		     File file=new File(filePath);
		     System.out.println("File Path : " + file.getAbsolutePath());
		     String csv = CDL.toString(data);
		     FileUtils.writeStringToFile(file, csv);
		}catch(Exception e){
			Log.Error(e);
		}
	}*/
	
	public List<EmailNotification> getDistinctEmailNotificationsAndFilters(String appName) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<EmailNotification> notificationsList = null;		
		try{
			notificationsList = new ArrayList<>();
			conn = getPostGresDBConnection();
			stmt = conn.createStatement();
			//query = "select distinct notification_id, notification_name, notification_dist, profile_id, profile_name from email_notification";
			//query = "select filters as filters, a.name, b.notification_id, b.notification_name, b.notification_dist, b.profile_id, b.profile_name, b.notification_text from profile_views a inner join email_notifications b on a.profile_id = b.profile_id order by b.notification_id";
			//query = "select filters as filters, a.name, a.filter_query, b.view_id, b.id, b.dist_email, b.profile_id, b.profile_name, b.subject , b.content, b.frequency, b.frequency_day, b.created_by, b.modified_on, b.enabled,b.file_format,b.app_name  from profile_views a inner join email_notifications b on a.id = b.view_id and b.app_name='" + appName + "' order by b.id";
			query = "select view_id, id, dist_email, profile_id, profile_name, subject , content, frequency, frequency_day, created_by, modified_on, enabled,file_format,app_name  from email_notifications order by id";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				EmailNotification emailNotification = new EmailNotification();				
				emailNotification.setId(rs.getInt("id"));
				//emailNotification.setName(rs.getString("name"));
				emailNotification.setDistEmail(rs.getString("dist_email"));
				emailNotification.setProfileId(rs.getInt("profile_id"));
				emailNotification.setProfileName(rs.getString("profile_name"));
				emailNotification.setSubject(rs.getString("subject"));
				//emailNotification.setFilters(rs.getString("filters"));
				emailNotification.setViewId(rs.getInt("view_id"));
				emailNotification.setContent(rs.getString("content"));
				emailNotification.setFrequency(rs.getString("frequency"));
				emailNotification.setFrequencyDay(rs.getInt("frequency_day"));
				emailNotification.setCreatedBy(rs.getString("created_by"));
				emailNotification.setModifiedOn(rs.getTimestamp("modified_on"));
				emailNotification.setEnabled(rs.getBoolean("enabled"));
				//emailNotification.setFilterQuery(rs.getString("filter_query"));
				emailNotification.setFileformat(rs.getString("file_format"));
				emailNotification.setAppName(rs.getString("app_name"));
				notificationsList.add(emailNotification);
			}			
			return notificationsList;
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conn, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return notificationsList;
	}
	public List<EmailNotification> getDistinctEmailNotificationsAndFilters() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<EmailNotification> notificationsList = null;		
		try{
			notificationsList = new ArrayList<>();
			conn = getPostGresDBConnection();
			stmt = conn.createStatement();
			//query = "select distinct notification_id, notification_name, notification_dist, profile_id, profile_name from email_notification";
			//query = "select filters as filters, a.name, b.notification_id, b.notification_name, b.notification_dist, b.profile_id, b.profile_name, b.notification_text from profile_views a inner join email_notifications b on a.profile_id = b.profile_id order by b.notification_id";
			//query = "select filters as filters, a.name, a.filter_query, b.view_id, b.id, b.dist_email, b.profile_id, b.profile_name, b.subject , b.content, b.frequency, b.frequency_day, b.created_by, b.modified_on, b.enabled,b.file_format,b.app_name  from profile_views a inner join email_notifications b on a.id = b.view_id  order by b.id";
			query = "select view_id, id, dist_email, profile_id, profile_name, subject , content, frequency, frequency_day, created_by, modified_on, enabled,file_format,app_name from  email_notifications order by id";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				EmailNotification emailNotification = new EmailNotification();			
				emailNotification.setId(rs.getInt("id"));
				//emailNotification.setName(rs.getString("name"));
				emailNotification.setDistEmail(rs.getString("dist_email"));
				emailNotification.setProfileId(rs.getInt("profile_id"));
				emailNotification.setProfileName(rs.getString("profile_name"));
				emailNotification.setSubject(rs.getString("subject"));
				//emailNotification.setFilters(rs.getString("filters"));
				emailNotification.setViewId(rs.getInt("view_id"));
				emailNotification.setContent(rs.getString("content"));
				emailNotification.setFrequency(rs.getString("frequency"));
				emailNotification.setFrequencyDay(rs.getInt("frequency_day"));
				emailNotification.setCreatedBy(rs.getString("created_by"));
				emailNotification.setModifiedOn(rs.getTimestamp("modified_on"));
				emailNotification.setEnabled(rs.getBoolean("enabled"));
				//emailNotification.setFilterQuery(rs.getString("filter_query"));
				emailNotification.setFileformat(rs.getString("file_format"));
				emailNotification.setAppName(rs.getString("app_name"));
				notificationsList.add(emailNotification);
			}			
			return notificationsList;
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conn, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return notificationsList;
	}
	public List<EmailNotification> getAllEmailNotifications() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<EmailNotification> notificationsList = null;
		try{
			notificationsList = new ArrayList<>();
			conn = getPostGresDBConnection();
			stmt = conn.createStatement();
			query = "select id, name, dist_email, profile_id, profile_name, content, view_id, subject, frequency, frequency_day, created_by, modified_on, enabled, file_format,app_name from email_notifications";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				EmailNotification emailNotification = new EmailNotification();
				emailNotification.setId(rs.getInt("id"));
				emailNotification.setName(rs.getString("name"));
				emailNotification.setDistEmail(rs.getString("dist_email"));
				emailNotification.setProfileId(rs.getInt("profile_id"));
				emailNotification.setProfileName(rs.getString("profile_name"));
				emailNotification.setContent(rs.getString("content"));
				emailNotification.setViewId(rs.getInt("view_id"));
				emailNotification.setSubject(rs.getString("subject"));
				emailNotification.setFrequency(rs.getString("frequency"));
				emailNotification.setFrequencyDay(rs.getInt("frequency_day"));
				emailNotification.setCreatedBy(rs.getString("created_by"));
				emailNotification.setModifiedOn(rs.getTimestamp("modified_on"));
				emailNotification.setEnabled(rs.getBoolean("enabled"));
				emailNotification.setFileformat(rs.getString("file_format"));
				emailNotification.setAppName(rs.getString("app_name"));
				notificationsList.add(emailNotification);
			}
			return notificationsList;
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conn, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return notificationsList;
	}
	
	public EmailNotification getEmailNotification(ProfileView profileView) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuilder query = null;
		EmailNotification emailNotification =null;
		try{
			conn = getPostGresDBConnection();
			stmt = conn.createStatement();
			query = new StringBuilder("select id, name, dist_email, profile_id, profile_name, content, view_id, subject, frequency, frequency_day, created_by, modified_on, enabled,content,file_format,app_name  from email_notifications");
			query.append(" WHERE profile_id=").append(profileView.getProfileId());
			//query.append(" AND created_by='").append(profileView.getCreatedBy()).append("'");
			query.append(" AND view_id=").append(profileView.getId());
			rs = stmt.executeQuery(query.toString());
			if(rs.next()){
				emailNotification = new EmailNotification();
				emailNotification.setId(rs.getInt("id"));
				emailNotification.setName(rs.getString("name"));
				emailNotification.setDistEmail(rs.getString("dist_email"));
				emailNotification.setProfileId(rs.getInt("profile_id"));
				emailNotification.setProfileName(rs.getString("profile_name"));
				emailNotification.setContent(rs.getString("content"));
				emailNotification.setViewId(rs.getInt("view_id"));
				emailNotification.setSubject(rs.getString("subject"));
				emailNotification.setFrequency(rs.getString("frequency"));
				emailNotification.setFrequencyDay(rs.getInt("frequency_day"));
				emailNotification.setCreatedBy(rs.getString("created_by"));
				emailNotification.setModifiedOn(rs.getTimestamp("modified_on"));
				emailNotification.setEnabled(rs.getBoolean("enabled"));
				emailNotification.setContent(rs.getString("content"));
				emailNotification.setFileformat(rs.getString("file_format"));
				emailNotification.setAppName(rs.getString("app_name"));
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conn, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return emailNotification;
	}
	
	public ProfileView getProfileViewDetails(int viewid, Connection conn) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		ProfileView view = new ProfileView();
		try{
			stmt = conn.createStatement();
			query = "select distinct id, profile_id, name, filters, created_by, created_on, view_setting, modified_by, modified_on, isdefault, ispublic, app, filter_query, from profile_views where id = " + viewid + "limit 1";
			rs = stmt.executeQuery(query);			
			DateFormat df = new SimpleDateFormat("d MMM yy hh:mm aaa");
			while(rs.next()){
				view.setId(rs.getInt("id"));
				view.setProfileId(rs.getInt("profile_id"));
				view.setName(rs.getString("name"));
				view.setViewFilters(rs.getString("filters"));
				view.setCreatedBy(rs.getString("created_by"));
				view.setCreatedOn(df.format(rs.getTimestamp("modified_on")));
				view.setPublic(rs.getBoolean("ispublic"));
				view.setDefault(rs.getBoolean("isdefault"));
				view.setViewType(rs.getBoolean("viewtype"));
				view.setFilterQuery(rs.getString("filter_query"));
			}
			return view;
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(conn, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return view;
	}
	
	private Connection getPostGresDBConnection() throws AppException, SQLException {
		Connection connection = dbUtil.getPostGresConnection();
		return connection;
	}
	
	
}
