package com.relevance.prism.data;

public class ProfileConfig {
	private int id;
	private String ids;
	private int viewID;
	private String profileKey;
	private String source; 
	private String database;
	private String table; 
	private String query; 
	private String columns; 
	private String columnConfigurations;
	private String filter;
	private String defaultFilter;
	private Integer orderby; 
	private String orderType;
	private String pagenumber; 
	private String pagesize; 
	private String searchValue;
	private boolean groupByEnabled;
	private String summaryEnabled;
	private FieldList description;
	private String createdBy;
	private String displayname;
	private String config;
	private String summary;
	private String fact;
	private String customFacts;
	private String remarks;
	private String type;
	private int stackCount;
	private String additionalconfig;
	private String notificationFilter;
	private String masterData;
	private String actionId;
	private String app;
	private Integer secondOrderBy;
	private String secondOrderType;
	private boolean cacheDisabled;
	
	public boolean isCacheDisabled() {
		return cacheDisabled;
	}
	public void setCacheDisabled(boolean cacheDisabled) {
		this.cacheDisabled = cacheDisabled;
	}
	public Integer getSecondOrderBy() {
		return secondOrderBy;
	}
	public void setSecondOrderBy(Integer secondOrderBy) {
		this.secondOrderBy = secondOrderBy;
	}
	public String getSecondOrderType() {
		return secondOrderType;
	}
	public void setSecondOrderType(String secondOrderType) {
		this.secondOrderType = secondOrderType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public int getViewID() {
		return viewID;
	}
	public void setViewID(int viewID) {
		this.viewID = viewID;
	}
	public String getProfileKey() {
		return profileKey;
	}
	public void setProfileKey(String profileKey) {
		this.profileKey = profileKey;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getColumns() {
		return columns;
	}
	public void setColumns(String columns) {
		this.columns = columns;
	}
	public String getColumnConfigurations() {
		return columnConfigurations;
	}
	public void setColumnConfigurations(String columnConfigurations) {
		this.columnConfigurations = columnConfigurations;
	}
	public String getDefaultFilter() {
		return defaultFilter;
	}
	public void setDefaultFilter(String defaultFilter) {
		this.defaultFilter = defaultFilter;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public Integer getOrderby() {
		return orderby;
	}
	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getPagenumber() {
		return pagenumber;
	}
	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}
	public String getPagesize() {
		return pagesize;
	}
	public void setPagesize(String pagesize) {
		this.pagesize = pagesize;
	}
	public String getSearchValue() {
		return searchValue;
	}
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public boolean isGroupByEnabled() {
		return groupByEnabled;
	}
	public void setGroupByEnabled(boolean groupByEnabled) {
		this.groupByEnabled = groupByEnabled;
	}
	public String getSummaryEnabled() {
		return summaryEnabled;
	}
	public void setSummaryEnabled(String summaryEnabled) {
		this.summaryEnabled = summaryEnabled;
	}
	public FieldList getDescription() {
		return description;
	}
	public void setDescription(FieldList description) {
		this.description = description;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getFact() {
		return fact;
	}
	public void setFact(String fact) {
		this.fact = fact;
	}
	public String getCustomFacts() {
		return customFacts;
	}
	public void setCustomFacts(String customFacts) {
		this.customFacts = customFacts;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getStackCount() {
		return stackCount;
	}
	public void setStackCount(int stackCount) {
		this.stackCount = stackCount;
	}
	public String getAdditionalconfig() {
		return additionalconfig;
	}
	public void setAdditionalconfig(String additionalconfig) {
		this.additionalconfig = additionalconfig;
	}
	public String getNotificationFilter() {
		return notificationFilter;
	}
	public void setNotificationFilter(String notificationFilter) {
		this.notificationFilter = notificationFilter;
	}
	public String getMasterData() {
		return masterData;
	}
	public void setMasterData(String masterData) {
		this.masterData = masterData;
	}
	public String getActionId() {
		return actionId;
	}
	public void setActionId(String actionId) {
		this.actionId = actionId;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
}
