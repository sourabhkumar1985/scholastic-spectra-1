package com.relevance.prism.service;

import java.util.ArrayList;

import com.relevance.prism.dao.SecurityProfileDao;
import com.relevance.prism.data.SecurityProfile;
import com.relevance.prism.util.PrismHandler;

public class SecurityProfileService extends BaseService {

	public ArrayList<SecurityProfile> getSecurityProfileViewDetails(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<SecurityProfile> securityViewList = null;
		try{
			SecurityProfileDao securityProfileDao = new SecurityProfileDao();
			securityViewList = securityProfileDao.getSecurityViewDetails(obj);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return securityViewList;
	}	
}
