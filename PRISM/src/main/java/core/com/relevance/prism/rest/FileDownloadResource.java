package com.relevance.prism.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.relevance.prism.util.PrismHandler;

/**
 * 
 * @author mbose4
 * This REST Resource class is used to download Files from Server
 */
@Path("/filedownload")
public class FileDownloadResource extends BaseResource{
	/**
	 * 
	 * @param svgcontent
	 * @return
	 */
	@POST
	@Path("/converthtmltopdf")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/pdf")
	public Response convertSvgToPdf(@FormParam("content") String svgcontent) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try{
			String tDir = System.getProperty("java.io.tmpdir");
			String pdfPath = tDir + File.separator + "svgoutput.pdf";
			String svgFileLocation = tDir + File.separator + "svginput.svg";
			System.out.println("Output File Path : " + pdfPath);
			File svgTempFile = new File(svgFileLocation);
			File pdfTempFile = new File(pdfPath);
			svgTempFile.delete();
			pdfTempFile.delete();
			// save it
			writeToFile(svgcontent, svgFileLocation);
			//SvgToPdfConverter svgToPdfConverter = new SvgToPdfConverter();
			//svgToPdfConverter.createPdf(svgFileLocation, pdfPath);
			File pdfFile = new File(pdfPath);
			ResponseBuilder response = Response.ok((Object) pdfFile);
			response.header("Content-Disposition","attachment; filename=svgoutput.pdf");
			return response.build();
		}catch(Exception ex){
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}
	/**
	 * 
	 * @param svgContent
	 * @param uploadedFileLocation
	 */
	private void writeToFile(String svgContent,
			String uploadedFileLocation) {
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			try {
				OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
				out.write(svgContent.getBytes());				
				out.flush();
				out.close();				
			}catch(Exception ex){
				PrismHandler.handleException(ex);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		}
}
