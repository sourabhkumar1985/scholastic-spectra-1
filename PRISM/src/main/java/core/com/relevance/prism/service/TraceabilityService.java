package com.relevance.prism.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.relevance.prism.dao.TraceabilityDao;
import com.relevance.prism.data.Traceability;
import com.relevance.prism.data.TraceabilityView;
import com.relevance.prism.util.PrismHandler;

public class TraceabilityService extends BaseService {
	public TraceabilityView getTraceability(String source, String database,
			String table, Traceability traceability) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		TraceabilityView traceabilityView = null;
		String direction;
		try {
			traceabilityView = new TraceabilityView();
			TraceabilityDao traceabilityDao = new TraceabilityDao();
			direction = traceability.getDirection();
			if (direction == null || direction.isEmpty()
					|| "forward".equalsIgnoreCase(direction)) {
				traceability.setDirection("forward");
				traceabilityView
						.setForwardTraceability(traceabilityDao
								.getTraceability(source, database, table,
										traceability));
			}

			if (direction == null || direction.isEmpty()
					|| "backward".equalsIgnoreCase(direction)) {
				traceability.setDirection("backward");
				traceabilityView
						.setBackwardTraceability(traceabilityDao
								.getTraceability(source, database, table,
										traceability));
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return traceabilityView;
	}
	
	public  Map<String,String> getBOM(String key,String direction) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		 Map<String,String> traceabilityView = null;
		try {
			traceabilityView = new HashMap<>();
			TraceabilityDao traceabilityDao = new TraceabilityDao();
			traceabilityView = traceabilityDao.getBOM(key,direction);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return traceabilityView;
	}
}
