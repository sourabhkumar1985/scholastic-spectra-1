package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SpectraRuleEngine extends BaseUtil{
	
	public String fireRules(String [] idsList, String project){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection spectraDBConnection = null;
		Connection projectDBConnection = null;
		Statement spectraRuleMappingStatement = null;
		Statement spectraParameterStatement = null;
		Statement projectColumnValueStatement = null;
		Statement ruleStatement = null;
		Statement ruleExecutionStatement = null;
		ResultSet spectraRuleMappingResultSet = null;
		ResultSet spectraParameterResultSet = null;
		ResultSet projectColumnValueResultSet = null;
		ResultSet ruleResultSet = null;
		try{
			spectraDBConnection = getSPECTRAPostGresDBConnection();
			projectDBConnection = getProjectPostGresDBConnection("Postgres", E2emfConstants.IDMP_DB);
			ruleExecutionStatement = projectDBConnection.createStatement();
			spectraRuleMappingStatement = spectraDBConnection.createStatement();
			spectraRuleMappingResultSet = spectraRuleMappingStatement.executeQuery("select mapping_id, table_name, column_name, rule_id, filter, key from spectra_rule_mapping where project = '"+ project +"' order by rule_id asc");
			ruleStatement = spectraDBConnection.createStatement();
			String methodName = null;
			String ruleText = null;
			while(spectraRuleMappingResultSet.next()){
				int ruleMappingId = spectraRuleMappingResultSet.getInt("mapping_id");
				String columnName = spectraRuleMappingResultSet.getString("column_name");
				String tableName = spectraRuleMappingResultSet.getString("table_name");
				String filter = spectraRuleMappingResultSet.getString("filter");
				String ruleId = spectraRuleMappingResultSet.getString("rule_id");
				String key = spectraRuleMappingResultSet.getString("key");
				
				ruleResultSet = ruleStatement.executeQuery("select rule_name, rule_text from rules where id = '" + ruleId + "' and project = '"+ project +"'");
				while(ruleResultSet.next()){
					methodName = ruleResultSet.getString("rule_name");
					ruleText = ruleResultSet.getString("rule_text");
				}
				String parameterQuery = "select table_name, column_name, filter, key from spectra_rule_parameter where project = '"+ project +"' and mapping_id = '" + ruleMappingId + "' order by sequence asc";
				spectraParameterStatement = spectraDBConnection.createStatement();
				spectraParameterResultSet = spectraParameterStatement.executeQuery(parameterQuery);
				Map<String,List<String>> parameterValueMap = new HashMap<>();
				int parameterCount = 0;
				while(spectraParameterResultSet.next()){
					parameterCount = parameterCount + 1;
					String parameterTableName = spectraParameterResultSet.getString("table_name");
					String parmeterColumnName = spectraParameterResultSet.getString("column_name");
					String parameterFilter = spectraParameterResultSet.getString("filter");
					String parameterKey = spectraParameterResultSet.getString("key");
					for(String keyVaue : idsList){						 
						if(key != null){								
							String columnValueQuery = "select " + parmeterColumnName + " from " + parameterTableName+ " where "+ parameterFilter + " and " + parameterKey + " = '" + keyVaue +"'";
							projectColumnValueStatement = projectDBConnection.createStatement();
							projectColumnValueResultSet = projectColumnValueStatement.executeQuery(columnValueQuery);
							if(projectColumnValueResultSet.next()){
								do{
									String columnValue = projectColumnValueResultSet.getString(parmeterColumnName);
									if(columnValue == null){
										if(parameterValueMap.containsKey(keyVaue)){
											parameterValueMap.get(keyVaue).add("empty");
										}else{
											List<String> list = new ArrayList<String>();
											list.add("empty");
											parameterValueMap.put(keyVaue, list);
										}
									}
									if(columnValue != null){
										if(parameterValueMap.containsKey(keyVaue)){
											parameterValueMap.get(keyVaue).add(columnValue);
										}else{
											List<String> list = new ArrayList<String>();
											list.add(columnValue);
											parameterValueMap.put(keyVaue, list);
										}										
									}
								}while(projectColumnValueResultSet.next());
							}							
						}						
					}					
				}
				if(!parameterValueMap.isEmpty()){
					Set<String> parameterValueSet = parameterValueMap.keySet();
					for(String keyValue : parameterValueSet){
						List<String> parameterValueListFromMap = parameterValueMap.get(keyValue);
						List<String> targetList = new ArrayList<>();
						for(String targetValueString : parameterValueListFromMap){
							if(targetValueString.contains(",")){
								targetList.add(targetValueString.replaceAll(",", ";"));
							}else{
								targetList.add(targetValueString);
							}							
						}
						Log.Info("List Size : " + targetList.size() + targetList);
						if(targetList.size() == 1 && targetList.get(0).equalsIgnoreCase(keyValue)){
							targetList.add("empty");
							Object output = GroovyRunner.getResult(ruleText, methodName, targetList.toArray());
							if(!output.toString().equalsIgnoreCase("empty")){
								String updateQuery = "update " + tableName + " set " + columnName + " = '" + output + "' where "+ key + "='" + keyValue + "' and " + filter;
								Log.Info("Update Query : " + updateQuery);
								System.out.println("Update Query : " + updateQuery);
								ruleExecutionStatement.executeUpdate(updateQuery);
							}
						}
						if(targetList.size() == parameterCount){
							Object output = GroovyRunner.getResult(ruleText, methodName, targetList.toArray());
							if(!output.toString().equalsIgnoreCase("empty")){
								String updateQuery = "update " + tableName + " set " + columnName + " = '" + output + "' where "+ key + "='" + keyValue + "' and " + filter;
								Log.Info("Update Query : " + updateQuery);
								System.out.println("Update Query : " + updateQuery);
								ruleExecutionStatement.executeUpdate(updateQuery);
							}
						}
						if(targetList.size() == (parameterCount+1)){
							targetList.remove(0);
							Object output = GroovyRunner.getResult(ruleText, methodName, targetList.toArray());
							if(!output.toString().equalsIgnoreCase("empty")){
								String updateQuery = "update " + tableName + " set " + columnName + " = '" + output + "' where "+ key + "='" + keyValue + "' and " + filter;
								Log.Info("Update Query : " + updateQuery);
								System.out.println("Update Query : " + updateQuery);
								ruleExecutionStatement.executeUpdate(updateQuery);
							}
						}	
						if(targetList.size() == (parameterCount+2)){
							targetList.remove(0);
							targetList.remove(1);
							Object output = GroovyRunner.getResult(ruleText, methodName, targetList.toArray());
							if(!output.toString().equalsIgnoreCase("empty")){
								String updateQuery = "update " + tableName + " set " + columnName + " = '" + output + "' where "+ key + "='" + keyValue + "' and " + filter;
								Log.Info("Update Query : " + updateQuery);
								System.out.println("Update Query : " + updateQuery);
								ruleExecutionStatement.executeUpdate(updateQuery);
							}
						}
						if(targetList.size() == (parameterCount+3)){
							targetList.remove(0);
							targetList.remove(1);
							targetList.remove(2);
							Object output = GroovyRunner.getResult(ruleText, methodName, targetList.toArray());
							if(!output.toString().equalsIgnoreCase("empty")){
								String updateQuery = "update " + tableName + " set " + columnName + " = '" + output + "' where "+ key + "='" + keyValue + "' and " + filter;
								Log.Info("Update Query : " + updateQuery);
								System.out.println("Update Query : " + updateQuery);
								ruleExecutionStatement.executeUpdate(updateQuery);
							}
						}
						if(targetList.size() >= (parameterCount+4)){
							Set<String> targetSet = new HashSet<>();
							targetSet.addAll(targetList);
							//targetList.remove(0);
							//targetList.remove(1);
							//targetList.remove(2);
							//targetList.remove(3);
							if(targetSet.size() == 1){
								targetSet.add("empty");
							}
							Object output = GroovyRunner.getResult(ruleText, methodName, targetSet.toArray());
							if(!output.toString().equalsIgnoreCase("empty")){
								String updateQuery = "update " + tableName + " set " + columnName + " = '" + output + "' where "+ key + "='" + keyValue + "' and " + filter;
								Log.Info("Update Query : " + updateQuery);
								System.out.println("Update Query : " + updateQuery);
								ruleExecutionStatement.executeUpdate(updateQuery);
							}
						}
					}
				}
			}
		}catch (Exception je) {
			Log.Error("Exception Occurred : " + je);
        }
		finally{
			try{
				//ruleExecutionStatement.close();
				projectDBConnection.close();
				spectraDBConnection.close();
			}catch (SQLException je) {
				Log.Error("Exception Occurred : " + je);
	        }			
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"Rules are fired Sucessfully. And the Master Records are updated Sucessfully.\"}";
	}
	
	/**
	 * 
	 * @param env
	 * @param paramCon
	 * @return
	 * @throws AppException
	 * @throws SQLException
	 */
	private static Connection getProjectPostGresDBConnection(String env, String paramCon) throws AppException, SQLException {
		Connection connection = null;
		PrismDbUtil dbUtil = new PrismDbUtil();
		if (env.equalsIgnoreCase("HIVE")) {				
			connection = dbUtil.getHiveConnection(paramCon);
			
		} else {
			connection = dbUtil.getPostGresConnection(paramCon);
			
		}	
		return connection;
	}
	
	/**
	 * 
	 * @param env
	 * @param paramCon
	 * @return
	 * @throws AppException
	 * @throws SQLException
	 */
	private static Connection getSPECTRAPostGresDBConnection() throws AppException, SQLException {
		Connection connection = null;
		PrismDbUtil dbUtil = new PrismDbUtil();
		connection = dbUtil.getPostGresConnection();
		return connection;
	}
}
