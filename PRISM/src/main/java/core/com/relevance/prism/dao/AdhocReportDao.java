package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.data.AdhocReport;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class AdhocReportDao extends BaseDao {
	public List<AdhocReport> getAdhocReports() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;		
		List<AdhocReport> adhocReportList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			AdhocReport adhocReport = null;
			String adhocReportQuery = "SELECT id, name, description, query, param, source, roleid, dbname FROM report";
		System.out.println(adhocReportQuery);
			rs = stmt.executeQuery(adhocReportQuery);
			while (rs.next()) {
				adhocReport = new AdhocReport();
				adhocReport.setId(rs.getLong("id"));
				adhocReport.setName(rs.getString("name"));
				adhocReport.setDescription(rs.getString("description"));
				adhocReport.setQuery(rs.getString("query"));
				adhocReport.setParam(rs.getString("param"));
				adhocReport.setSource(rs.getString("source"));
				adhocReport.setRoleId(rs.getLong("roleid"));
				adhocReport.setDbName(rs.getString("dbname"));
				adhocReportList.add(adhocReport);
			}

		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}					
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return adhocReportList;
	}
	
	public AdhocReport getAdhocReport(long id) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;		
		AdhocReport adhocReport = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String adhocReportQuery = "SELECT id, name, description, query, param, source, roleid, dbname FROM report where id = '" + id +"'";
			System.out.println(adhocReportQuery);
			rs = stmt.executeQuery(adhocReportQuery);
			while (rs.next()) {
				adhocReport = new AdhocReport();
				adhocReport.setId(rs.getLong("id"));
				adhocReport.setName(rs.getString("name"));
				adhocReport.setDescription(rs.getString("description"));
				adhocReport.setQuery(rs.getString("query"));
				adhocReport.setParam(rs.getString("param"));
				adhocReport.setSource(rs.getString("source"));
				adhocReport.setRoleId(rs.getLong("roleid"));
				adhocReport.setDbName(rs.getString("dbname"));
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}					
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return adhocReport;
	}
	
	public String createAdhocReport(AdhocReport adhocReport) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		StringBuilder query = new StringBuilder();
		if (adhocReport != null) {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			Log.Info("Inserting  Values for AdhocReport Object : " + adhocReport);
			try {
				query = new StringBuilder("INSERT INTO report(name, description, query, param, source, roleid, dbname) VALUES (?, ?, ?, ?, ?, ?, ?)");
				ps = connection.prepareStatement(query.toString());
				ps.setString(1, adhocReport.getName());
				ps.setString(2, adhocReport.getDescription());
				ps.setString(3, adhocReport.getQuery());
				ps.setString(4, adhocReport.getParam());
				ps.setString(5, adhocReport.getSource());
				ps.setLong(6, adhocReport.getRoleId());
				ps.setString(7, adhocReport.getDbName());
				ps.addBatch();
				ps.executeBatch();
				ps.close();
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}					
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted AdhocReport in table\"}";
	}
	
	public String updateAdhocReport(AdhocReport adhocReport) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		StringBuilder query = new StringBuilder();
		if (adhocReport != null) {
			connection = dbUtil.getPostGresConnection();
			Log.Info("Updating the AdhocReport in DB : " + adhocReport);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				query = new StringBuilder("UPDATE report SET name=?, description=?, query=?, param=?, source=?, roleid=?, dbname=?  WHERE id=?");
				ps = connection.prepareStatement(query.toString());
				ps.setString(1, adhocReport.getName());
				ps.setString(2, adhocReport.getDescription());
				ps.setString(3, adhocReport.getQuery());
				ps.setString(4, adhocReport.getParam());
				ps.setString(5, adhocReport.getSource());
				ps.setLong(6, adhocReport.getRoleId());
				ps.setString(7, adhocReport.getDbName());
				ps.setLong(8, adhocReport.getId());
				ps.addBatch();
				ps.executeBatch();
				ps.close();
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}					
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return "{\"message\" : \"successfully updated AdhocReports in DB.\"}";
	}
}
