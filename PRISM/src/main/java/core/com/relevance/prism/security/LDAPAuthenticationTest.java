package com.relevance.prism.security;

import java.net.UnknownHostException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import jcifs.UniAddress;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;

/*
 * Created on Nov 12, 2014
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author lkodavat
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LDAPAuthenticationTest {
	
	public boolean login(String username, String password){
		Hashtable<String, String> env = new Hashtable<String, String>(11);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://192.168.105.11:389/");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, "uid=" + username + ",ou=system");
		env.put(Context.SECURITY_CREDENTIALS, password);
		boolean result = false;
		try {
			// Create initial context
			DirContext ctx = new InitialDirContext(env);
			// Close the context when we're done
			result = true;
			ctx.close();

		} catch (NamingException e) {
			System.out.println(e);
			result = false;
		} finally {
			if (result) {
				System.out.println("Success");
			} else {
				System.out.println("Failure");
			}
		}
		System.out.println("Login authentication result   :"+ result);
		return result;
	}
	
	public static void main(String[] args) {
		boolean Result = false;
		   try{
			   //System properties
//			   System.setProperty("jcifs.resolveOrder", "LMHOSTS,WINS,DNS");
			   System.setProperty("jcifs.resolveOrder", "WINS,DNS");
			   System.setProperty("jcifs.lmCompatibility", "2");
			   System.setProperty("jcifs.netbios.hostname", "ADG_SAMPLECODE");


		
			   //Login Information and validations
			   String jnjmsusername = "MKUMAR57";//"LKODAVAT";
			   String jnjmsdomain = "NA";//Values coming like {ap,eu,la,na and jnj} na.jnj.com
			   String password = "";//"Venkata@12";//enter password
			   if (jnjmsdomain != null  && jnjmsusername != null && password != null) {
				   jnjmsdomain = jnjmsdomain.toString().trim();
				   jnjmsusername = jnjmsusername.toString().trim();
				   password = password.toString().trim();
				   jnjmsdomain = jnjmsdomain.toLowerCase();
				   //	if the domain is provided with the “.jnj.com” suffix,
				   //	remove .jnj.com
				   if (jnjmsdomain.equalsIgnoreCase("jnj.com")){//for removing .com from jnj.com domain
					   jnjmsdomain = jnjmsdomain.replaceAll(".com","").trim();
				   }else if (jnjmsdomain.endsWith(".jnj.com")) {//for removing .jnj.com from na.jnj.com
					   jnjmsdomain = jnjmsdomain.substring(0, jnjmsdomain.length() - 8);
				   }
				   //	get the UniAddress
				   UniAddress domainController = null;
				   if (jnjmsdomain.equalsIgnoreCase("jnj")){
					   jnjmsdomain = jnjmsdomain.toUpperCase();
//					   domainController = UniAddress.getByName("10.36.108.28");
//					   manager.reportSuccess("domainController if"+domainController);
				   }else{
//					   manager.reportSuccess("jnjmsdomain   :"+jnjmsdomain);
					   if(jnjmsdomain.equalsIgnoreCase("ap")){
						   domainController = UniAddress.getByName("10.36.108.36");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }else if(jnjmsdomain.equalsIgnoreCase("eu")){
						   domainController = UniAddress.getByName("10.36.108.34");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }else if(jnjmsdomain.equalsIgnoreCase("la")){
						   domainController = UniAddress.getByName("10.36.108.32");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }else if(jnjmsdomain.equalsIgnoreCase("na")){
						   domainController = UniAddress.getByName("10.36.108.29");
//						   jnjmsdomain = jnjmsdomain.toUpperCase();
					   }

				   }		

				   //	Create the NtlmPasswordAuthentication credentials object
				   jcifs.smb.NtlmPasswordAuthentication credentials = new jcifs.smb.NtlmPasswordAuthentication(jnjmsdomain, jnjmsusername, password);
//				   manager.reportSuccess(" domain   :"+credentials.getDomain());
				   //	submit the authentication attempt
				   jcifs.smb.SmbSession.logon(domainController, credentials);
				   //String userName = UUID.
				   //wdContext.currentContextElement().setMessage("Login Success");
				   Result = true;
			   }else{
				   System.out.println("Please Enter login details");
			   }
			   //	   convert the passed-in domain to lower case
		   }catch (UnknownHostException ue) {
			   Result = false;
			   System.out.println("Domain Controller IP address not being resolved"+ ue);
		   } catch (SmbAuthException authE) {
			   //	handle the case of a bad username / password
			   Result = false;
			   System.out.println("Incorrect Username/Password or Domain" + authE);
			  
		   } catch (SmbException e) {
			   //  Auto-generated catch block
			   Result = false;
			   System.out.println("SmbException" + e);
			   
		   }catch (Exception e) {
			   Result = false;
			   System.out.println("Exception" + e);
			  
		   }
		System.out.println("Login authentication result   :"+ Result);
		
		
	}
}
