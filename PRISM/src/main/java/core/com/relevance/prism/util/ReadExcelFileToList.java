package com.relevance.prism.util;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;

public class ReadExcelFileToList {
	private static int updateCount = 0;
	private static int insertCount = 0;
	public static String readExcelData(String fileName, String env, String db, String table) throws NullPointerException, AppException, IOException{
		updateCount = 0;
		insertCount = 0;
		ArrayList<String> columnNamesList = new ArrayList<String>();
		Connection connection = null;
		Connection queryConnection = null;
		Statement stmt = null;		
		try {
			String paramCon = null;
			connection = getPostGresDBConnection();
			//String dbquery = "select name from app_properties where value like '%jdbc%' and value like '%" + db + "%'";
			String dbquery = "select name from app_properties where name ='" + db + "' or (value like '%jdbc%' and value like '%"
					+ db + "%')";
			Statement stmtPost = connection.createStatement();
			ResultSet rsPost = stmtPost.executeQuery(dbquery);
			if (rsPost != null) {
				while (rsPost.next()) {
					paramCon = rsPost.getString("name");
				}
			}
			queryConnection = getDBConnection(env, paramCon);
			stmt = queryConnection.createStatement();
			// Create the input stream from the xlsx/xls file
			FileInputStream fis = new FileInputStream(fileName);
			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if (fileName.toLowerCase().endsWith("xlsx")) {
				workbook = new XSSFWorkbook(fis);
			} else if (fileName.toLowerCase().endsWith("xls")) {
				workbook = new HSSFWorkbook(fis);
			}
			// Get the number of sheets in the xlsx file
			int numberOfSheets = workbook.getNumberOfSheets();
			// loop through each of the sheets
			for (int i = 0; i < numberOfSheets; i++) {
				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);
				// every sheet has rows, iterate over them
				Iterator<Row> rowIterator = sheet.iterator();
				int rowcount = 0;
				while (rowIterator.hasNext()) {
					//String name = "";
					//String shortCode = "";
					// Get the row object
					Row row = rowIterator.next();
					// Every row has columns, get the column iterator and
					// iterate over them
					rowcount = rowcount + 1;
					if(rowcount == 1){
						//continue;
						for(int cn = 0; cn < row.getLastCellNum(); cn++) {
							Cell cell = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
							columnNamesList.add(cell.getStringCellValue().trim());
						}
						continue;
					}
					//Iterator<Cell> cellIterator = row.cellIterator();
					//while (cellIterator.hasNext()) {
					//for(int cn = 0; cn < row.getLastCellNum(); cn++) {
						addExcelDataToTable(stmt, row, table, columnNamesList);
					//} // end of cell iterator
				} // end of rows iterator
			}// end of sheets for loop
			// close file input stream 
			fis.close();
		} catch (IOException ex) {
			Log.Error("Exception while Reading from XLs Workbook and creating SmartFindDocument List" + ex);
		}catch (SQLException sql) {
			Log.Error("Exception while inserting the Table Entries in table."
					+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception while inserting the Table Entries in table."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (connection != null) {
				try {
					if(stmt != null){
						stmt.close();
					}					
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}
		Log.Info("File Upload : " + insertCount + " Number of Records are inserted in "+ table +" table.");
		Log.Info("File Upload : " + updateCount + " Number of Records are updated in "+ table +" table.");
		//return "{\"message\":\"successfully inserted entries in solrmaster table.\"}";
		return "{\"message\":\" " + insertCount + " Number of Records are inserted and" + updateCount +" Number of Records are updated in table.";		
	}
	
	public static String readCSVData(String fileName, String env, String db, String table, char separator) throws NullPointerException, AppException, IOException{
		updateCount = 0;
		insertCount = 0;
		ArrayList<String> columnNamesList = new ArrayList<String>();
		HashMap<String, Integer> columnNamesMap = new HashMap<String, Integer>();
		Connection connection = null;
		Connection queryConnection = null;
		Statement stmt = null;		
		try {
			String paramCon = null;
			connection = getPostGresDBConnection();
			//String dbquery = "select name from app_properties where value like '%jdbc%' and value like '%" + db + "%'";
			String dbquery = "select name from app_properties where name ='" + db + "' or (value like '%jdbc%' and value like '%"
					+ db + "%')";
			Statement stmtPost = connection.createStatement();
			ResultSet rsPost = stmtPost.executeQuery(dbquery);
			if (rsPost != null) {
				while (rsPost.next()) {
					paramCon = rsPost.getString("name");
				}
			}
			queryConnection = getDBConnection(env, paramCon);
			stmt = queryConnection.createStatement();
			// Create the input stream from the xlsx/xls file
			//Build reader instance       
			//Read data.csv       
			//Default seperator is comma       
			//Default quote character is double quote       
			//Start reading from line number 2 (line numbers start from zero)       
			CSVReader reader = new CSVReader(new FileReader(fileName), separator , '"' , 0);               
			//Read CSV line by line and use the string array as you want       
			String[] row;
			int rowcount = 0;
			StringBuffer createTableQuery = new StringBuffer("create table " + table + "(");
			while ((row = reader.readNext()) != null) {
				rowcount = rowcount + 1;
				if(rowcount == 1){
					if (row != null) {             
						//Verifying the read data here						
						for(int cn = 0; cn < row.length ; cn++) {
							if(cn < (row.length -1)){
								createTableQuery.append(row[cn].trim() + " text, ");
								columnNamesList.add(row[cn].trim());
							}else{
								createTableQuery.append(row[cn].trim() + " text)");
								columnNamesList.add(row[cn].trim());
							}
						}         
					}
					//ResultSet rs = stmt.executeQuery("select * from " + table );
					DatabaseMetaData dbm = connection.getMetaData();
					ResultSet tables = dbm.getTables(null, null, table, null);
					if (tables.next()) {
					  // Table exists
						Log.Info("Table Already Exists");
						System.out.println("Table Already Exists");
					}
					else {
					  // Table does not exist
						Log.Info("Create Table Query " + createTableQuery.toString());
						System.out.println("Create Table Query " + createTableQuery.toString());
						stmt.executeUpdate(createTableQuery.toString());
					}
					
					connection.close();
					connection = getPostGresDBConnection();
					DatabaseMetaData dbmd = connection.getMetaData();
					ResultSet columns = dbmd.getColumns(null, null, table, "%");
					if(columns != null){
						while(columns.next()) {
					        // get the info from the resultset (eg the java.sql.Types value):
					        int dataType = columns.getInt("DATA_TYPE");
					        String columnName = columns.getString("COLUMN_NAME");
					        columnNamesMap.put(columnName, Integer.valueOf(dataType));
					        Log.Info("Column Name : " + columnName + " Column Type : " + dataType);
					        System.out.println("Column Name : " + columnName + " Column Type : " + dataType);
					    }
					}
					continue;
				}				
				if (row != null) {             
					//Verifying the read data here             
					//System.out.println(Arrays.toString(nextLine));
					addCSVDataToTable(stmt, row, table, columnNamesList, columnNamesMap);
				}        
			}
			if (reader != null){
				try {
					stmt.close();
					reader.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		} catch (IOException ex) {
			Log.Error("Exception while Reading from CSV Workbook and creating Rows List" + ex);
		}catch (SQLException sql) {
			Log.Error("Exception while inserting the Table Entries in table."
					+ sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception while inserting the Table Entries in table."
					+ e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			throw new AppException(newmessage,
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (connection != null) {
				try {
					if(stmt != null){
						stmt.close();
					}	
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}
			}
		}
		Log.Info("File Upload : " + insertCount + " Number of Records are inserted in "+ table +" table.");
		Log.Info("File Upload : " + updateCount + " Number of Records are updated in "+ table +" table.");
		//return "{\"message\":\"successfully inserted entries in solrmaster table.\"}";
		return "{\"message\":\" " + insertCount + " Number of Records are inserted and" + updateCount +" Number of Records are updated in "+ table +" table.";
	}
	
	public static void addCSVDataToTable(Statement stmt, String [] row, String table, ArrayList<String> columnNamesList, HashMap<String, Integer> columnNamesMap)throws AppException, SQLException{		
		String selectQuery = null;
		String cell = row[0];
		//String regex = "\\d+";
		//if(cell.matches(regex)){
		if(columnNamesMap.get(columnNamesList.get(0)).intValue() == 4){
			selectQuery = "select * from " + table +" where "+ columnNamesList.get(0) + " = " + cell.trim();
		}else{
			selectQuery = "select * from " + table +" where "+ columnNamesList.get(0) + " = '" + cell.trim() + "'";
		}
		Log.Info("Select Query : " + selectQuery.toString());
		System.out.println("Select Query : " + selectQuery.toString());
		ResultSet rs = stmt.executeQuery(selectQuery);
		if(rs.next()){
			updateCount++;
			StringBuffer updateQuery = new StringBuffer("update " + table + " set ");
			for(int cn = 0; cn < columnNamesList.size(); cn++) {
				if(cn < (columnNamesList.size()-1)){
					String columnValue = row[cn];
					//if(columnValue.matches(regex)){
					if(columnNamesMap.get(columnNamesList.get(cn)).intValue() == 4){
						updateQuery.append(columnNamesList.get(cn) + " = " + columnValue.trim() + ", ");
					}else{
						updateQuery.append(columnNamesList.get(cn) + " = '" + columnValue.trim() + "', ");
					}					
				}else{
					String columnValue = row[cn];
					//if(columnValue.matches(regex)){
					if(columnNamesMap.get(columnNamesList.get(cn)).intValue() == 4){
						updateQuery.append(columnNamesList.get(cn) + " = " + columnValue.trim() + " ");
					}
					else{
						updateQuery.append(columnNamesList.get(cn) + " = '" + columnValue.trim() + "' ");
					}		
				}
			}
			//if(cell.matches(regex)){
			if(columnNamesMap.get(columnNamesList.get(0)).intValue() == 4){
				updateQuery.append(" where "+ columnNamesList.get(0) + " = " + cell.trim());				
			}
			else{
				updateQuery.append(" where "+ columnNamesList.get(0) + " = '" + cell.trim() + "'");
			}	
			//updateQuery.append(" where "+ columnNamesList.get(0) + " = " + keyValue);
			Log.Info("Update Query : " + updateQuery.toString());
			System.out.println("Update Query : " + updateQuery.toString());
			stmt.executeUpdate(updateQuery.toString());
		}else{
			insertCount++;
			StringBuffer insertQuery = new StringBuffer("insert into " + table + "(");
			for(int i = 0; i < columnNamesList.size(); i++) {
				if(i < (columnNamesList.size()-1)){
					insertQuery.append(columnNamesList.get(i));
					insertQuery.append(", ");
				}else{
					insertQuery.append(columnNamesList.get(i));
				}
			}
			insertQuery.append(") values (");
			for(int j = 0; j < columnNamesList.size(); j++) {				
				if(j < (columnNamesList.size()-1)){
					String columnValue = row[j];
					//if(columnValue.matches(regex)){
					if(columnNamesMap.get(columnNamesList.get(j)).intValue() == 4){
						insertQuery.append(columnValue.trim() + ",");						
					}else{
						insertQuery.append("'" + columnValue.trim() + "',");
					}
				}else{
					String columnValue = row[j];
					//if(columnValue.matches(regex)){
					if(columnNamesMap.get(columnNamesList.get(j)).intValue() == 4){
						insertQuery.append(columnValue.trim());						
					}else{
						insertQuery.append("'" + columnValue.trim() + "'");
					}
				}
			}
			insertQuery.append(")");
			Log.Info("Insert query : " + insertQuery.toString());
			System.out.println("Insert query : " + insertQuery.toString());
			stmt.executeUpdate(insertQuery.toString());					
		}		
	}
	
	public static void addExcelDataToTable(Statement stmt, Row row, String table, ArrayList<String> columnNamesList)throws AppException, SQLException{		
		String selectQuery = null;
		Cell cell = row.getCell(0, Row.CREATE_NULL_AS_BLANK);
		String keyStringValue = null;
		String keyNumberValue = null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			keyStringValue = cell.getStringCellValue().trim();
			selectQuery = "select * from " + table +" where "+ columnNamesList.get(0) + " = '" + cell.getStringCellValue().trim() + "'";
			break;
		case Cell.CELL_TYPE_NUMERIC:
			selectQuery = "select * from " + table +" where "+ columnNamesList.get(0) + " = " + new DataFormatter().formatCellValue(cell);
			keyNumberValue = new DataFormatter().formatCellValue(cell);
			break;
		}
		Log.Info("Select Query : " + selectQuery.toString());
		System.out.println("Select Query : " + selectQuery.toString());
		ResultSet rs = stmt.executeQuery(selectQuery);
		if(rs.next()){
			updateCount++;
			StringBuffer updateQuery = new StringBuffer("update " + table + " set ");
			for(int cn = 0; cn < columnNamesList.size(); cn++) {
				if(cn < (columnNamesList.size()-1)){
					Cell columnValue = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
					switch (columnValue.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						updateQuery.append(columnNamesList.get(cn) + " = '" + columnValue.getStringCellValue().trim() + "', ");
						break;
					case Cell.CELL_TYPE_NUMERIC:
						updateQuery.append(columnNamesList.get(cn) + " = " + new DataFormatter().formatCellValue(columnValue) + ", ");
						break;
					}		
				}else{
					Cell columnValue = row.getCell(cn, Row.CREATE_NULL_AS_BLANK);
					switch (columnValue.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						updateQuery.append(columnNamesList.get(cn) + " = '" + columnValue.getStringCellValue().trim() + "' ");
						break;
					case Cell.CELL_TYPE_NUMERIC:
						updateQuery.append(columnNamesList.get(cn) + " = " + new DataFormatter().formatCellValue(columnValue) + " ");
						break;
					}		
				}
			}
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				updateQuery.append(" where "+ columnNamesList.get(0) + " = '" + keyStringValue + "'");
				break;
			case Cell.CELL_TYPE_NUMERIC:
				updateQuery.append(" where "+ columnNamesList.get(0) + " = " + keyNumberValue);
				break;
			}	
			//updateQuery.append(" where "+ columnNamesList.get(0) + " = " + keyValue);
			Log.Info("Update Query : " + updateQuery.toString());
			System.out.println("Update Query : " + updateQuery.toString());
			stmt.executeUpdate(updateQuery.toString());
		}else{
			insertCount++;
			StringBuffer insertQuery = new StringBuffer("insert into " + table + "(");
			for(int i = 0; i < columnNamesList.size(); i++) {
				if(i < (columnNamesList.size()-1)){
					insertQuery.append(columnNamesList.get(i));
					insertQuery.append(", ");
				}else{
					insertQuery.append(columnNamesList.get(i));
				}
			}
			insertQuery.append(") values (");
			for(int j = 0; j < columnNamesList.size(); j++) {				
				if(j < (columnNamesList.size()-1)){
					Cell columnValue = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
					switch (columnValue.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						insertQuery.append("'" + columnValue.getStringCellValue().trim() + "',");
						break;
					case Cell.CELL_TYPE_NUMERIC:
						insertQuery.append(new DataFormatter().formatCellValue(columnValue) + ",");
						break;
					}
				}else{
					Cell columnValue = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
					switch (columnValue.getCellType()) {
					case Cell.CELL_TYPE_STRING:
						insertQuery.append("'" + columnValue.getStringCellValue().trim() + "'");
						break;
					case Cell.CELL_TYPE_NUMERIC:
						insertQuery.append(new DataFormatter().formatCellValue(columnValue));
						break;
					}
				}
			}
			insertQuery.append(")");
			Log.Info("Insert query : " + insertQuery.toString());
			System.out.println("Insert query : " + insertQuery.toString());
			stmt.executeUpdate(insertQuery.toString());					
		}		
	}
	
	public static Connection getPostGresDBConnection() throws AppException, SQLException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Connection connection = dbUtil.getPostGresConnection();
		return connection;
	}
	
	public static Connection getDBConnection(String env, String paramCon) throws AppException, SQLException {
		Connection connection = null;
		PrismDbUtil dbUtil = new PrismDbUtil();
		if (env.equalsIgnoreCase("HIVE")) {				
			connection = dbUtil.getHiveConnection(paramCon);
			
		} else {
			connection = dbUtil.getPostGresConnection(paramCon);
			
		}	
		return connection;
	}	
}