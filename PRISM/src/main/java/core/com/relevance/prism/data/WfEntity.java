package com.relevance.prism.data;

import java.util.Date;

/**
 *
 * @author: Shawkath Khan
 * @Created_Date: Feb 17, 2016
 * @Purpose: Entity for Workflow Entity	
 * @Modified_By: 
 * @Modified_Date:  
 */

public class WfEntity {

	private String entityId;
	private String state;
	private String user;
	private Date lastUpdated;
	private String comments;
	
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
