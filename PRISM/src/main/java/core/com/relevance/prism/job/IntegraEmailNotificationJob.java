package com.relevance.prism.job;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.relevance.prism.dao.MailDao;
import com.relevance.prism.service.MailService;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.QFindFileDownloader;
import com.rl.cms.refresh.FileDetails;
import com.rl.cms.refresh.FileJSONData;

public class IntegraEmailNotificationJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext context) {
		try {

			System.out.println("--------------------------------------------------------------------");
			System.out.println("Starting Job " + context.getJobDetail().getFullName() + " at " + context.getFireTime());
			JobDetail jobDetail = context.getJobDetail();
			System.out.println(context.getJobDetail().getFullName() + " at " + context.getJobRunTime() + ", key: " + jobDetail.getKey());
			System.out.println(context.getJobDetail().getFullName()	+ "'s next scheduled time: " + context.getNextFireTime());
			System.out.println("--------------------------------------------------------------------");
			QFindFileDownloader qFindFileDownloader = new QFindFileDownloader();
			boolean result = qFindFileDownloader.makeConnectionAndDownloadFiles();
			if(result){
				MailService mailService = new MailService();
				String mailContent = "CMS Data has been modified or refreshed. So please review.";
				String subject = "Q-Find: Integra App updates";
				String app = "INTEGRA";
				List<FileJSONData> fileDetailList = qFindFileDownloader.getFileDetailsList();
				JSONArray jsonArray = qFindFileDownloader.getJSONArrayOfContents(fileDetailList);
				File file = qFindFileDownloader.getEmailAttachment(jsonArray, "QFind_Delta_Data", "csv");
				mailService.sendEmail(mailContent, subject, app, file);
			}
		} catch (Exception e) {
			Log.Error("Exception Occured....." + e);
		}
	}
	
	
}
