package com.relevance.prism.dao;

import com.relevance.prism.util.PrismDbUtil;

public class BaseDao {
	protected PrismDbUtil dbUtil = null;
	protected BaseDao() {
		dbUtil = new PrismDbUtil();
	}
	protected static String getCurrentMethodName() {
		if (Thread.currentThread().getStackTrace() != null
				&& Thread.currentThread().getStackTrace().length > 2)
			return Thread.currentThread().getStackTrace()[2].getMethodName();
		else
			return "UnknownMethod";
	}
	protected static String getClassName() {
		if (Thread.currentThread().getStackTrace() != null
				&& Thread.currentThread().getStackTrace().length > 2)
			return Thread.currentThread().getStackTrace()[2].getClassName();
		else
			return "UnknownClass";
	}
}
