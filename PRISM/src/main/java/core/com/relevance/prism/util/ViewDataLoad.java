package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;

public class ViewDataLoad extends BaseUtil {
	
	/**
	 * Description : Program to insert oracle views data to postgres DB
	 * 
	 * @authors : Mallikarjun Mundasi
	 */
	
	private static final int BATCH_SIZE = 1000;

	//DB properties
	public static String oracle_driverclass = E2emfAppUtil.getAppProperty(E2emfConstants.QA_ORACLE_DRIVER_CLASS);
	public static String oracle_hostname = E2emfAppUtil.getAppProperty(E2emfConstants.QA_ORACLE_HOST);
	public static String oracle_username = E2emfAppUtil.getAppProperty(E2emfConstants.QA_ORACLE_USERNAME);
	public static String oracle_password = E2emfAppUtil.getAppProperty(E2emfConstants.QA_ORCALE_PASSWORD);

	public static String postgres_driverclass = E2emfAppUtil.getAppProperty(E2emfConstants.QA_POSTGRES_DRIVER_CLASS);
	public static String postgres_hostname = E2emfAppUtil.getAppProperty(E2emfConstants.QA_POSTGRES_HOST);
	public static String postgres_username = E2emfAppUtil.getAppProperty(E2emfConstants.QA_POSTGRES_USERNAME);
	public static String postgres_password = E2emfAppUtil.getAppProperty(E2emfConstants.QA_POSTGRES_PASSWORD);

	// Connect QA Oracle DB
	public Connection getOracleDBConnetion() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		System.out.println("inside getOracleDBConnetion() method...");
		Connection conn = null;
		try {
			Class.forName(oracle_driverclass);
			conn = DriverManager.getConnection(oracle_hostname, oracle_username, oracle_password);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		System.out.println("Got Oracle connection !!");
		return conn;
	}

	// Connect QA Postgres DB
	public Connection getPostGresDBConnection() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		System.out.println("inside getPostGresDBConnection() method...");
		Connection conn = null;
		try {
			Class.forName(postgres_driverclass);
			conn = DriverManager.getConnection(postgres_hostname, postgres_username, postgres_password);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		System.out.println("Got postgreSQL connection !!");
		return conn;
	}

	// Insert data from item attribute view to postgres table
	public boolean insertDataForItemAttributeView() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		System.out.println("inside insertDataForItemAttributeView() method...");
		boolean result = false;
		Connection oracle_connection = null;
		ResultSet rs = null;
		ResultSet rs1=null;
		Connection postgres_connection = null;
		Statement stmt=null;
		PreparedStatement pst = null;
		Statement st=null;
		int rowcount = 0;
		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		Timestamp last_update_date = new Timestamp(currentDate.getTime());
		System.out.println(last_update_date.toString());
		// Oracle view data checks last week data
		String sql="select \"Item\",\"DESCRIPTION\",\"Main/UOM:Primary\","
				+ "\"Main/User Item Type\",\"Main/Item Status\",\"Organization\","
				+ "\"DFF/Integra Manufacturer Name\",\"Purchasing/Default Buyer\",\"Purchasing/List Price\","
				+ "\"OM/Default Shipping Org\",\"DFF/Country of Origin\",\"Physical Attribute/Weight:UOM\",\"Physical Attribute/Weight\","
					+ "\"PA/Dimension:UOM\",\"PA/Dimension:Length\",\"PA/Dimension:Width\",\"PA/Dimension:Height\" from XX_ITEM_ATTRIBUTES_V "
				+ " WHERE SYSDATE-7 <= \"LAST UPDATE DATE\" AND \"Organization\"='MST' order by \"LAST UPDATE DATE\"";
		
		//insert into postgres table query
		String insert_sql ="INSERT INTO SOLRMASTER_ITEM(KEY,ITEM_NUMBER,TITLE,DATASOURCE,USERUPDATED,UOM_CODE,ITEM_TYPE,ITEMSTATUS,"
				+ "ORG_CODE,MANUFACTURER,RELATEDITEMS,PRODUCT_BRAND,PURCHASING_DEFAULT_BUYER,"
				+ "	PURCHASING_LIST_PRICE,OM_DEFAULT_SHIPPING_ORG,COO,FLOATF,MDM_FLAG,LAST_UPDATE_DATE,PHYSICAL_ATTRIBUTE_WEIGHT_UOM,"
				+ "PHYSICAL_ATTRIBUTE_WEIGHT,PA_DIMENSION_UOM,PA_DIMENSION_LENGTH,PA_DIMENSION_WIDTH,PA_DIMENSION_HEIGHT) "
				+ "	VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";		
		try {
			oracle_connection = getOracleDBConnetion();
			stmt = oracle_connection.createStatement();
			postgres_connection=getPostGresDBConnection();
			
			rs = stmt.executeQuery(sql);
				System.out.println("ResultSet created from oracle item master view..");
			if (!rs.next()) { //checks result set
				result = false;
				System.out.println("No data");
			} else {
				System.out.println("Loading of Item attribute view data starts...");
				while (rs.next()) {
					rowcount++;
					//System.out.println(rs.getString(1).toString());
					String itemno_check="select item_number from solrmaster_item where item_number='"+rs.getString(1)+"'";
					st=postgres_connection.createStatement();
					rs1=st.executeQuery(itemno_check);
					pst = postgres_connection.prepareStatement(insert_sql);
					if(!rs1.next())
					{
						insertDataForSolrmasterItem(rs,rowcount, postgres_connection, pst, oracle_connection);
						result=true;
					}
					else{
							String delete_query="delete from solrmaster_item where item_number='"+rs1.getString(1)+"'";
							boolean query_result=st.execute(delete_query);
							insertDataForSolrmasterItem(rs,rowcount, postgres_connection, pst, oracle_connection);
							result=true;
					}
				}	
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(postgres_connection, rs, stmt);
			PrismHandler.handleFinally(oracle_connection, null, pst);
			PrismHandler.handleFinally(null, rs1, st);
		}
		return result;
	}

	public void insertDataForSolrmasterItem(ResultSet rs,int rowcount,Connection postgres_connection,PreparedStatement pst,Connection oracle_connection) throws Exception
	{
		String key_Item = "ITM_";
		String datasource = "ITEM_MASTER";
		String userupdated = null;
		String relateditems = null;
		String product_brand = null;
		double floatf = 0.0;
		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		Timestamp last_update_date = new Timestamp(currentDate.getTime());
		try {
			String key = key_Item + rs.getString(1).replaceAll("\'", "\\'");
			String item_number = rs.getString(1).replaceAll("\'", "\\'");
			String title = rs.getString(2).replaceAll("\'", "\\'");
			String uom_code = rs.getString(3);
			String item_type = rs.getString(4);
			String item_status = rs.getString(5);
			String org_code = rs.getString(6);
			String manufacturer_name = rs.getString(7);
			String purchasing_default_buyer = rs.getString(8);
			String purchasing_list_price = rs.getString(9);
			String om_default_shipping_org = rs.getString(10);
			String def_country_of_origin = rs.getString(11);
			String physical_attribute_weight_uom=rs.getString(12);
			String pa_dimension_uom=rs.getString(14);

			// pst = postgres_connection.prepareStatement(insert_sql);
			//postgres_connection.setAutoCommit(false); // set auto commit to
														// false

			pst.setString(1, key);
			pst.setString(2, item_number);
			pst.setString(3, title);
			pst.setString(4, datasource);
			pst.setString(5, userupdated);
			pst.setString(6, uom_code);
			pst.setString(7, item_type);
			pst.setString(8, item_status);
			pst.setString(9, org_code);
			pst.setString(10, manufacturer_name);
			pst.setString(11, relateditems);
			pst.setString(12, product_brand);
			pst.setString(13, purchasing_default_buyer);
			pst.setString(14, purchasing_list_price);
			pst.setString(15, om_default_shipping_org);
			pst.setString(16, def_country_of_origin);
			pst.setDouble(17, floatf);

			boolean T = true;
			boolean F = false;
			if (item_type.startsWith("Surgical Kit") || item_type.startsWith("Equipment")
					|| item_type.startsWith("Finished Good") || item_type.startsWith("Tools/Instruments")
					|| item_type.startsWith("Kit")) {
				pst.setBoolean(18, T);
			} else {
				pst.setBoolean(18, F);
			}
			pst.setTimestamp(19, last_update_date);
			pst.setString(20,physical_attribute_weight_uom);
			if(rs.getInt(13)==0){
				pst.setNull(21, java.sql.Types.INTEGER);
			}else{
				pst.setInt(21,rs.getInt(13));
			}
			pst.setString(22,pa_dimension_uom);
			if(rs.getInt(15)==0)
			{
				pst.setNull(23, java.sql.Types.INTEGER);
			}else{
				pst.setInt(23,rs.getInt(15));
			}
			if(rs.getInt(16)==0)
			{
				pst.setNull(24, java.sql.Types.INTEGER);
			}else{
				pst.setInt(24,rs.getInt(16));
			}
			if(rs.getInt(17)==0)
			{
				pst.setNull(25, java.sql.Types.INTEGER);
			}else{
				pst.setInt(25,rs.getInt(17));
			}
			
			pst.addBatch(); // add to batch
			System.out.println(rowcount);
			// checks no. of rows if more than batch size
			if (rowcount % BATCH_SIZE == 0) {
				pst.executeBatch();
			}else{
				pst.executeBatch();
			}
			//postgres_connection.commit();
		}catch(Exception e) {
			PrismHandler.handleException(e, true);
		} 
	}

	
	// Insert data from BOM attribute view to postgres table
	public boolean insertDataForBOMview() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		System.out.println("inside insertDataForBOMview() method...");
		Connection oracle_connection = null;
		ResultSet rs = null;
		Connection postgres_connection = null;
		PreparedStatement pst=null;
		Statement stmt=null;
		Statement stmt1=null;
		ResultSet rs1=null;
		int rowcount = 0;
		boolean result = false;
		
		String query="select ASSEMBLY_ITEM_ID,ORGANIZATION_ID,ORGANIZATION_CODE,ITEM,ITEM_DESCRIPTION,ITEM_SEQ,"
				+ "OPERATION_SEQ,COMPONENT,COMPONENT_DESCRIPTION,COMPONENT_QUANTITY,PLANNING_FACTOR,"
				+ "COMPONENT_YIELD_FACTOR,INCLUDE_IN_COST_ROLLUP,PRIMARY_UNIT_OF_MEASURE,PRIMARY_UOM_CODE,"
				+ "LAST_UPDATE_DATE from XXBOM_ATTRIBUTES_V WHERE SYSDATE-7 <= LAST_UPDATE_DATE ORDER BY LAST_UPDATE_DATE";

		 //String delete_query="delete from bom_details_oracle";

		String insert_sql="INSERT INTO BOM_DETAILS_ORACLE(ASSEMBLY_ITEM_ID,ORGANIZATION_ID,ORGANIZATION_CODE,ITEM,ITEM_DESCRIPTION,ITEM_SEQ,"
				+ "OPERATION_SEQ,COMPONENT,COMPONENT_DESCRIPTION,COMPONENT_QUANTITY,PLANNING_FACTOR,COMPONENT_YIELD_FACTOR,INCLUDE_IN_COST_ROLLUP,"
				+ "	PRIMARY_UNIT_OF_MEASURE,PRIMARY_UOM_CODE,LAST_UPDATE_DATE) "
				+ "	VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			oracle_connection = getOracleDBConnetion();
			postgres_connection = getPostGresDBConnection();
			stmt = oracle_connection.createStatement();
			Statement st=postgres_connection.createStatement();
			rs = stmt.executeQuery(query);
			System.out.println("ResultSet created from oracle BOM master view..");
			if (!rs.next()) {
				result = false;
				System.out.println("No data");
			} else {
				System.out.println("Loading of BOM view data starts...");
				//boolean res=st.execute(delete_query);
				while (rs.next()) {
					rowcount++;
					String itemno_check="select item,item_seq from bom_details_oracle where item='"+rs.getString(4)+"'and item_seq='"+rs.getString(6)+"'";
					stmt1=postgres_connection.createStatement();
					rs1=stmt1.executeQuery(itemno_check);
					pst = postgres_connection.prepareStatement(insert_sql);
					if(!rs1.next())
					{
						insertDataForBOM(rs,rowcount, postgres_connection, pst, oracle_connection);
						result=true;
					}
					else{
							String delete_query="delete from bom_details_oracle where item='"+rs1.getString(1)+"'and item_seq='"+rs1.getString(2)+"'";
							boolean query_result=st.execute(delete_query);
							insertDataForBOM(rs,rowcount, postgres_connection, pst, oracle_connection);
							result=true;
					}
					
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(oracle_connection, rs, stmt);
			PrismHandler.handleFinally(postgres_connection, null, pst);
			PrismHandler.handleFinally(null, rs1, stmt1);
		}
		return result;
	}
	
	
	public void insertDataForBOM(ResultSet rs,int rowcount,Connection postgres_connection,PreparedStatement pst,Connection oracle_connection) throws Exception
	{
		try {
			String ASSEMBLY_ITEM_ID = rs.getString(1);
			String ORGANIZATION_ID = rs.getString(2);
			String ORGANIZATION_CODE = rs.getString(3);
			String ITEM = rs.getString(4);
			String ITEM_DESCRIPTION = rs.getString(5);
			String ITEM_SEQ = rs.getString(6);
			String OPERATION_SEQ = rs.getString(7);
			String COMPONENT = rs.getString(8);
			String COMPONENT_DESCRIPTION = rs.getString(9);
			String COMPONENT_QUANTITY = rs.getString(10);
			String PLANNING_FACTOR = rs.getString(11);
			String COMPONENT_YIELD_FACTOR = rs.getString(12);
			String INCLUDE_IN_COST_ROLLUP = rs.getString(13);
			String PRIMARY_UNIT_OF_MEASURE = rs.getString(14);
			String PRIMARY_UOM_CODE = rs.getString(15);
			String LAST_UPDATE_DATE = rs.getString(16);

			// pst = postgres_connection.prepareStatement(insert_sql);
			//postgres_connection.setAutoCommit(false);

			pst.setString(1, ASSEMBLY_ITEM_ID);
			pst.setString(2, ORGANIZATION_ID);
			pst.setString(3, ORGANIZATION_CODE);
			pst.setString(4, ITEM);
			pst.setString(5, ITEM_DESCRIPTION);
			pst.setString(6, ITEM_SEQ);
			pst.setString(7, OPERATION_SEQ);
			pst.setString(8, COMPONENT);
			pst.setString(9, COMPONENT_DESCRIPTION);
			pst.setString(10, COMPONENT_QUANTITY);
			pst.setString(11, PLANNING_FACTOR);
			pst.setString(12, COMPONENT_YIELD_FACTOR);
			pst.setString(13, INCLUDE_IN_COST_ROLLUP);
			pst.setString(14, PRIMARY_UNIT_OF_MEASURE);
			pst.setString(15, PRIMARY_UOM_CODE);
			pst.setString(16, LAST_UPDATE_DATE);

			pst.addBatch();

			System.out.println(rowcount);
			if (rowcount % BATCH_SIZE == 0) {
				pst.executeBatch();
			} else  {
				pst.executeBatch();
			}
			//postgres_connection.commit();
		}catch(Exception e) {
			PrismHandler.handleException(e, true);
		}
	}
	
	
	
	/*public static void main(String[] args) throws Exception {
		try {
			ViewDataLoad viewDataLoad = new ViewDataLoad();
			viewDataLoad.insertDataForBOMview();
			viewDataLoad.insertDataForItemAttributeView();
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
	}*/
}
