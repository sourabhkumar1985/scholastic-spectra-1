package com.relevance.prism.security;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;

public class ReadLDAPUserAttributes 
{ 
  static DirContext ldapContext; 
  public static void main (String[] args) throws NamingException 
  { 
    try 
    { 
      System.out.println("Début du test Active Directory"); 
      //String userName = "nshivaru";
      //String domainController = "10.36.108.29";

      Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11); 
      ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory"); 
      //ldapEnv.put(Context.PROVIDER_URL,  "ldap://societe.fr:389"); 
      ldapEnv.put(Context.PROVIDER_URL,  "ldap://ldap-idv.psso.its.jnj.com:389/"); 
      //ldapEnv.put(Context.PROVIDER_URL,  "ldap://jjedsdir.jnj.com:389/o=jnj,dc=jjedsed,dc=jnj,dc=com");
      //ldapEnv.put(Context.SECURITY_AUTHENTICATION, "Simple"); 
      //ldapEnv.put(Context.SECURITY_PRINCIPAL, "cn=administrateur,cn=users,dc=societe,dc=fr"); 
      //ldapEnv.put(Context.SECURITY_PRINCIPAL,userName); 
      //ldapEnv.put(Context.SECURITY_CREDENTIALS, "Test@1234");  // Enter NT Password here
      //ldapEnv.put(Context.SECURITY_PROTOCOL, "ssl"); 
      //ldapEnv.put(Context.SECURITY_PROTOCOL, "simple"); 
      ldapContext = new InitialDirContext(ldapEnv); 
      
      //dc=na,dc=jnj,dc=com

      // Create the search controls          
      SearchControls searchCtls = new SearchControls(); 

      //Specify the attributes to return 
      String returnedAtts[]={"sn","CN","givenName", "Title","mail","jnjMSUserName","samAccountName","employeeType","mobile","jnjMSUPN"}; 
      searchCtls.setReturningAttributes(returnedAtts); 

      //Specify the search scope 
      searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE); 

      //specify the LDAP search filter 
      //String searchFilter = "(&(objectClass=user)(givenName=nshivaru))"; 
      String searchFilter = "(&(sn=*)(objectClass=user)(jnjMSUserName=msrivat1))";

      //Specify the Base for the search 
      String searchBase = "o=jnj"; //"dc=dom,dc=fr";    dc=ad,dc=my-domain,dc=com
      //initialize counter to total the results 
      int totalResults = 0; 

      // Search for objects using the filter 
      NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls); 

      //Loop through the search results 
      while (answer.hasMoreElements()) 
      { 
        SearchResult sr = (SearchResult)answer.next(); 

        totalResults++; 
     
        Attributes attrs = sr.getAttributes(); 
       
        	 System.out.println("sn:" + attrs.get("sn"));
        	 System.out.println("CN:" + attrs.get("CN"));
        	 System.out.println("employeeType:" + attrs.get("employeeType"));
             System.out.println("mobile:" + attrs.get("mobile"));
             System.out.println("mail:" + attrs.get("mail"));             
             System.out.println("Email:" + attrs.get("givenName"));             
             System.out.println("jnjMSUserName:" + attrs.get("jnjMSUserName"));             
             System.out.println("jnjMSUserName:" + attrs.get("workforceID"));             
             System.out.println("Title:" + attrs.get("Title"));
             
             String CN = attrs.get("givenName").toString()+" "+ attrs.get("sn").toString();
             
             UserAttributes userattribs =  new UserAttributes();
             userattribs.setNtId(attrs.get("jnjMSUserName").toString());
             userattribs.setCn(CN);
             String title = (attrs.get("Title") != null) ? attrs.get("Title").toString().replaceAll("Title: ", "") : "";
             userattribs.setTitle(title);
             userattribs.setCompany("Johnson & Johnson");
             userattribs.setDepartment("Medical Devies");
             userattribs.setEmail(attrs.get("mail").toString());       
             
             
             //String WWID = attrs.get("workforceID").toString();
             String commonName = (attrs.get("givenName") != null) ? attrs.get("givenName").toString().replaceAll("givenName: ", "") : " ";
 	        commonName += (attrs.get("sn") != null) ? " "+ attrs.get("sn").toString().replaceAll("sn: ", "") : "";
 	        
 	       String WWID = (attrs.get("CN") != null) ? attrs.get("CN").toString().replaceAll("CN: ", "") : " ";
 	        
 	       UserAttributes updatedUsrAttrs = connectToLDAPAndRetrieveUserAttributes(commonName,WWID,userattribs);
 	        
 	        System.out.println("Company Name :" +updatedUsrAttrs.getCompany());
 	        System.out.println("Department Desc :" +updatedUsrAttrs.getDepartment());
             
             //return userattribs;
       
      } 

      System.out.println("Total results: " + totalResults); 
      ldapContext.close(); 
    } 
    catch (Exception e) 
    { 
      System.out.println(" Search error: " + e); 
      e.printStackTrace(); 
      System.exit(-1); 
    } 
  } 
  
  public static UserAttributes connectToLDAPAndRetrieveUserAttributes(String cn, String wwid, UserAttributes userAttributes ) throws Exception {
		Hashtable<String, String> env = new Hashtable<String, String>(11);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://jjedsdir.jnj.com:389/o=jnj,dc=jjedsed,dc=jnj,dc=com");  
		//env.put(Context.PROVIDER_URL, "ldap://ldap-idv.psso.its.jnj.com:389/o=jnj,dc=jjedsed,dc=jnj,dc=com");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		UserAttributes userAttributesUpdated = null;
		DirContext context = null;
		//boolean result = false;
		try {
			// Create initial context
			context = new InitialDirContext(env);
			//Attributes attributes = context.getAttributes("CN=LINDA NASH,OU=75519,OU=Employees");
			//Attributes attributes = context.getAttributes("CN=MADHAN BOSE,OU=702178450,OU=Partners");
			Attributes attributes = context.getAttributes("CN="+cn+",OU="+wwid+",OU=Employees");	
			//if()
			// Print the answer
			userAttributesUpdated =  printAttrs(attributes,userAttributes);
			//result = true;
			context.close();
		} catch (NamingException e) {
			//System.out.println(e);
			//result = false;
			Attributes attributes = context.getAttributes("CN="+cn+",OU="+wwid+",OU=Partners");
			userAttributesUpdated =  printAttrs(attributes,userAttributes);
			context.close();
		} 
		//System.out.println("Login authentication result   :" + result);
		return userAttributesUpdated;
	}
  
	public static UserAttributes printAttrs(Attributes attrs, UserAttributes userAttributes ) throws Exception {
		if (attrs == null) {
			System.out.println("No attributes");
		} else {
			
			 System.out.println("sn:" + attrs.get("sn"));
        	 System.out.println("employeeType:" + attrs.get("employeeType"));
             System.out.println("mobile:" + attrs.get("mobile"));
             System.out.println("Email:" + attrs.get("jnjMSUPN"));
             
            String deptDesc = (attrs.get("jnjDepartmentDescription") != null) ? attrs.get("jnjDepartmentDescription").toString().replaceAll("jnjDepartmentDescription: ", "") : "";
            String legalEntity = (attrs.get("legalEntityCategory") != null) ? attrs.get("legalEntityCategory").toString().replaceAll("legalEntityCategory: ", "") : "";
            
            String postgresURL = E2emfAppUtil.getAppProperty("EvidencePostgres");
            String postgresUser = E2emfAppUtil.getAppProperty("EvidencePostgresUser");
            String postgresPwd = E2emfAppUtil.getAppProperty("EvidencePostgresPwd");
            
            Connection conn = getPostGresConnection(postgresURL,postgresUser,postgresPwd);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select name from jjeds_legalentity where legalentitycode='"+legalEntity+"'");
            String companyName = null;
            while(rs.next()){
            	companyName = rs.getString("name");
            }
             userAttributes.setDepartment(deptDesc);
             userAttributes.setCompany(companyName);
             
             
          /* Print each attribute */
		/*	try {
				  for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) 
				{
					Attribute attr = (Attribute) ae.next();
					System.out.println("attribute: " + attr.getID());
					
					 //print each value 
					for (NamingEnumeration e = attr.getAll(); e.hasMore(); System.out.println("value: " + e.next()));
				}
			} catch (NamingException e) {
				e.printStackTrace();
			}*/
		}
		 return userAttributes;
	}
  
  
  public static UserAttributes getLDAPUserDetails(String username) {
	  UserAttributes usrAttrs = null;
	  String commonName = "";
	  String ntId= "";
	  String title = "";
	  String companyName = "";
	  String dept = "";
	  String email = "";
	  UserAttributes updatedUsrAttrs = null;
	  
	  try {
		  Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11); 
	      ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	      ldapEnv.put(Context.PROVIDER_URL,  "ldap://ldap-idv.psso.its.jnj.com:389/");
	      ldapContext = new InitialDirContext(ldapEnv);
	      
	      SearchControls searchCtls = new SearchControls();
	      
	      //Specify the attributes to return 
	      String returnedAtts[]={"sn","CN","workforceID","givenName", "Title","mail","jnjMSUserName","samAccountName","employeeType","mobile","jnjMSUPN"}; 
	      searchCtls.setReturningAttributes(returnedAtts); 

	      //Specify the search scope 
	      searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE); 
	      StringBuilder searchFilter = new StringBuilder("(&(sn=*)(objectClass=user)");
	      searchFilter.append("(jnjMSUserName=");
	      searchFilter.append(username + "))");
	      
	      //String searchFilter = "(&(sn=*)(objectClass=user)(jnjMSUserName=jtrinh1))";
	      //Specify the Base for the search 
	      String searchBase = "o=jnj"; //"dc=dom,dc=fr";    dc=ad,dc=my-domain,dc=com
	      //initialize counter to total the results 
	      // Search for objects using the filter 
	      NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter.toString(), searchCtls); 
	      
	    //Loop through the search results 
	      while (answer.hasMoreElements()) 
	      { 
	        SearchResult sr = (SearchResult)answer.next(); 
	     
	        Attributes attrs = sr.getAttributes(); 
		         
	        commonName = (attrs.get("givenName") != null) ? attrs.get("givenName").toString().replaceAll("givenName: ", "") : " ";
	        commonName += (attrs.get("sn") != null) ? " "+ attrs.get("sn").toString().replaceAll("sn: ", "") : "";
	        	ntId = (attrs.get("jnjMSUserName") != null) ? attrs.get("jnjMSUserName").toString().replaceAll("jnjMSUserName: ", "") : "";
	        	title = (attrs.get("Title") != null) ? attrs.get("Title").toString().replaceAll("Title: ", "") : "";
	        	title = (title == "") ? "Vendor" : title;
	        	email = (attrs.get("mail") != null) ? attrs.get("mail").toString().replaceAll("mail: ", "") : "";
	        	
	        usrAttrs =  new UserAttributes();
	        usrAttrs.setNtId(ntId);
	        usrAttrs.setCn(commonName);
	        usrAttrs.setTitle(title);
	        usrAttrs.setCompany(companyName);
	        usrAttrs.setDepartment(dept);
	        usrAttrs.setEmail(email); 
	        
	        String WWID = (attrs.get("CN") != null) ? attrs.get("CN").toString().replaceAll("CN: ", "") : " ";
	        
	        updatedUsrAttrs = connectToLDAPAndRetrieveUserAttributes(commonName,WWID,usrAttrs);
	        
	        System.out.println("Company Name :" +updatedUsrAttrs.getCompany());
	        System.out.println("Department Desc :" +updatedUsrAttrs.getDepartment());
	             	       
	      } 
	      ldapContext.close(); 
	  } catch (Exception ex) {
		  //System.out.println(" Search error: " + ex); 
	     // ex.printStackTrace();
	  }
	  return updatedUsrAttrs;
  }
  
  private static Connection getPostGresConnection(String dbURL, String username, String password) throws Exception
		{
			
	        Connection connection = null;
	          try {

	               Class.forName("org.postgresql.Driver"); 
	               connection = DriverManager.getConnection(dbURL,username, password);               
	               System.out.println("Using Postgress connection...");

	        } catch (Exception ee) {
	               System.out.println("Exception in getting data from batch table "  + ee.getMessage());
	               System.out.println("Exception" + ee.getMessage());
	               System.out.println("Exception in detail" + ee);
	               ee.printStackTrace();
	               System.out.println(ee.getMessage());
	               throw ee;
	        }       

	        if (connection != null) {
	               //System.out.println("You made it, take control your database now!");
	               System.out.println("You made it, take control your database now!");
	        } else {
	               //System.out.println("Failed to make connection!");
	               System.out.println("Failed to make connection!");
	        }

	        return connection;
	 }
}
