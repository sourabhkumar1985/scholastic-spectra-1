package com.relevance.prism.listener;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.relevance.prism.data.AppProperties;
import com.relevance.prism.service.AppPropertiesService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.FormData;
import com.relevance.prism.util.Log;

public class E2emfContextListener implements ServletContextListener {
	
	ServletContext context;
	AppPropertiesService appPropertiesService = null;
	AppProperties appProperties = null; 

	@Override
	public void contextInitialized(ServletContextEvent contextEvent) 
	{
		Log.Info("contextInitialized() " + context);
		if(!Log.isLoggerInitialized)
		    Log.initialize();
	
		context = contextEvent.getServletContext();
		Log.Info("contextInitialized, fetching profile and material details to be made available to app ");
		appPropertiesService = new AppPropertiesService();
		
		try{
			if(context != null)
			{
				appProperties = appPropertiesService.getProperties();
				E2emfAppUtil.initialize(appProperties);
			
				Map<String, FormData> formDataMap = appPropertiesService.getFormDataMap();
				E2emfAppUtil.setFormDataMap(formDataMap);
				
				
				E2emfAppUtil.setWordMap(appPropertiesService.getObfuscationWordMap(false));
				E2emfAppUtil.setRevWordMap(appPropertiesService.getObfuscationWordMap(true));
				
				E2emfAppUtil.setCharMap(appPropertiesService.getObfuscationCharMap(false));
				E2emfAppUtil.setRevCharMap(appPropertiesService.getObfuscationCharMap(true));
				
				
				
		 } else {
			 Log.Info("This should never be executed...");
			 
		
		 }	
		}catch(AppException e){
			Log.Error(e);
			Log.Error("Exception, fetching profile and material details to be made available to app ");
		}catch(Exception e){
			Log.Error(e);
			Log.Error("Exception, fetching profile and material details to be made available to app ");
		}
		
		
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent contextEvent) {
			
		context = contextEvent.getServletContext();
		Log.Info("contextDestroyed() called before container shutdown, removing objects from context ...");	
		
		if(context != null){			
			context.removeAttribute("e2emfsapnetval");
			context.removeAttribute("e2emfsapmaterialcount");
			context.removeAttribute("e2emfjdenetval");
			context.removeAttribute("e2emfjdematerialcount");
			context.removeAttribute("poprofilesapdefault");
			context.removeAttribute("poprofilejdedefault");
			context.removeAttribute("poprofilegpdefault");
			context.removeAttribute("soprofilesapdefault");
			context.removeAttribute("soprofilejdedefault");
			context.removeAttribute("soprofilegpdefault");
			context.removeAttribute("systemmapsapdefault");
			context.removeAttribute("systemmapjdedefault");
			context.removeAttribute("systemmapgpdefault");
			context.removeAttribute("manualglsapdefault");
			context.removeAttribute("manualgljdedefault");
			context.removeAttribute("manualglgpdefault");
			context.removeAttribute("pricingsapdefault");
			context.removeAttribute("pricingjdedefault");
			context.removeAttribute("pricinggpdefault");
			
		}		
		
	}

}
