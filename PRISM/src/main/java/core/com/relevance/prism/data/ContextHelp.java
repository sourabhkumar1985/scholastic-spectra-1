package com.relevance.prism.data;

public class ContextHelp {
	public long id;
	public String question;
	public String answer;
	public String screen;
	//public String role[];
	public String role;
	//public String enable_disable;
	public long enabled;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getScreen() {
		return screen;
	}
	public void setScreen(String screen) {
		this.screen = screen;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	/*public String getEnable_disable() {
		return enable_disable;
	}
	public void setEnable_disable(String enable_disable) {
		this.enable_disable = enable_disable;
	}*/
	public long getEnabled() {
		return enabled;
	}
	public void setEnabled(long enabled) {
		this.enabled = enabled;
	}
	
	
}
