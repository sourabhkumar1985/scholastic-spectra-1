package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WriteBackUpdater {
	/*public static void main(String args[]){
		try{
			Configuration conf = HBaseConfiguration.create();
			conf.set("hbase.zookeeper.quorum", "rl-jj-app");
			conf.set("hbase.master", "rl-jj-master:60000");
			HTable solrTable = new HTable(conf, "hbasesolr");
			//upDateSolrTable(solrTable, "ADPT_--PSB-6653-001_c.pdf", "\\x5CN", "--PSB-6653-001_c", "\\x5CN", "\\x5CN" , "false");
			upDateSolrTable(solrTable, "D0002764-00-1", "test", "D0002764-00-1", "test", "test" , "true","drillbits","others");
		}catch(Exception e){
			System.out.println("Exception occurred in Write Back Operation : " + e.getMessage());
		}
	}*/
	
	/*public static void upDateSolrTable(HTable solrtable, String rowkey, String title, String docNum, String manufacturer, String components, String userUpdated,String category,String cspeciality) throws IOException{
		 
		 //conf.set("hbase.zookeeper.quorum", zkquorem);
		 //conf.set("hbase.master", hmaster);
		 Put data = null;
		 Get g = new Get(rowkey.getBytes());
		 Result r = solrtable.get(g);
		 if (!r.isEmpty()){//if data is available get the data from the hbase table and update it
			 data = new Put(r.getRow());
		 }else{//if the data is not available add the data to the table
			 data = new Put(rowkey.getBytes());
		 }
			 data.add("cf".getBytes(),"category".getBytes(),category.getBytes());
			 data.add("cf".getBytes(),"components".getBytes(),components.getBytes());
			 data.add("cf".getBytes(),"componentsfound".getBytes(),r.getValue("cf".getBytes(), "componentsfound".getBytes()));
			 data.add("cf".getBytes(),"cspeciality".getBytes(),r.getValue("cf".getBytes(), "cspeciality".getBytes()));
			 data.add("cf".getBytes(),"date1".getBytes(),r.getValue("cf".getBytes(), "date1".getBytes()));
			 data.add("cf".getBytes(),"docnum".getBytes(), docNum.getBytes());
			 data.add("cf".getBytes(),"docnumfound".getBytes(),r.getValue("cf".getBytes(), "docnumfound".getBytes()));
			 data.add("cf".getBytes(),"filepath".getBytes(),r.getValue("cf".getBytes(), "filepath".getBytes()));
			 data.add("cf".getBytes(),"id".getBytes(),r.getValue("cf".getBytes(), "id".getBytes()));
			 data.add("cf".getBytes(),"imagepath".getBytes(),r.getValue("cf".getBytes(), "imagepath".getBytes()));
			 data.add("cf".getBytes(),"manufacturer".getBytes(),manufacturer.getBytes());
			 data.add("cf".getBytes(),"manufacturerFound".getBytes(),r.getValue("cf".getBytes(), "manufacturerFound".getBytes()));
			 data.add("cf".getBytes(),"moddate".getBytes(),r.getValue("cf".getBytes(), "moddate".getBytes()));
			 data.add("cf".getBytes(),"relateditems".getBytes(),r.getValue("cf".getBytes(), "relateditems".getBytes()));
			 data.add("cf".getBytes(),"secondarycomponents".getBytes(),r.getValue("cf".getBytes(), "secondarycomponents".getBytes()));
			 data.add("cf".getBytes(),"secondarytitle".getBytes(),r.getValue("cf".getBytes(), "secondarytitle".getBytes()));
			 data.add("cf".getBytes(),"smallimagepath".getBytes(),r.getValue("cf".getBytes(), "smallimagepath".getBytes()));
			 data.add("cf".getBytes(),"source".getBytes(),r.getValue("cf".getBytes(), "source".getBytes()));
			 data.add("cf".getBytes(),"subcategory".getBytes(),r.getValue("cf".getBytes(), "subcategory".getBytes()));
			 data.add("cf".getBytes(),"title".getBytes(),title.getBytes());
			 data.add("cf".getBytes(),"titlefound".getBytes(),r.getValue("cf".getBytes(), "titlefound".getBytes()));
			 data.add("cf".getBytes(),"userupdated".getBytes(),userUpdated.getBytes());
			 
			 solrtable.put(data);
			 
			 //System.out.println("Category : " + Bytes.toString(r.getValue("cf".getBytes(), "filepath".getBytes())));
			 Log.Info("Updated the Table with User Input.....");
			 Log.Info("Document ID : " + rowkey);
			 Log.Info("Doc Num : " + Bytes.toString(r.getValue("cf".getBytes(), "docnum".getBytes())));
			 solrtable.close();
}*/
	
	public static void updatePostGresTable(String rowkey, String title, String docNum, String manufacturer, String components, String userUpdated,String category,String cspeciality, String partnumber, String componentnumber, String finishedgoodsnumber, String notes, String userWhoUpdated, String mlocation) throws NullPointerException, AppException{
		
		Connection connection = null;
		Statement stmt = null;
		try{
			String date = getSystemDate();
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			String updateQuery = "update solrmaster set title ='"	+ title + "', docnum ='" + docNum + "', manufacturer ='"+  manufacturer +"', components ='" + components + "', userupdated ='"	+ userUpdated +"', categoryl0 ='"	+ category +"', cspeciality='"+ cspeciality + "', component_number='"+ componentnumber +"', finished_goods_number='"+ finishedgoodsnumber + "', partnumber='"+ partnumber +"', notes='"+ notes + "', datetimestamp='"+ date + "', userwhoupdated='"+ userWhoUpdated + "', mlocation='"+ mlocation +"' where key like '%" + rowkey + "%'";
			//String docNumUpdateQuery = "update solrmaster set docnum ='"	+ docNum + "' where key1 = '" + rowkey + "'";
			//String manufacturerUpdateQuery = "update solrmaster set manufacturer ='"	+ manufacturer + "' where key1 = '" + rowkey + "'";
			//String componentsUpdateQuery = "update solrmaster set components ='"	+ components + "' where key1 = '" + rowkey + "'";
			//String userUpdatedUpdateQuery = "update solrmaster set userupdated ='"	+ userUpdated + "' where key1 = '" + rowkey + "'";
			//String categoryUpdateQuery = "update solrmaster set category ='"	+ category + "' where key1 = '" + rowkey + "'";
			//String smallimagepath = "";
			//String imagepath ="";
			//String filepath = "";
			//String cspeciality = "";
			//String category = "";
			Log.Info("Postgres Solr Update Querty : " + updateQuery);
			stmt.executeUpdate(updateQuery);
			//stmt.executeUpdate(docNumUpdateQuery);
			//stmt.executeUpdate(manufacturerUpdateQuery);
			//stmt.executeUpdate(componentsUpdateQuery);
			//stmt.executeUpdate(userUpdatedUpdateQuery);
			//stmt.executeUpdate(categoryUpdateQuery);			
		}catch(AppException appe){
			Log.Error("AppException while updating solr master table." + appe);
		}catch (SQLException sql) {
			Log.Error("SQLException while updating solr master table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while updating solr master table." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					if(stmt != null){
						stmt.close();
					}	
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		
	}
	
	public static void incrementActionCountInPostGresTable(String solrid, String action, long numberoftimes)  throws AppException{
		Connection connection = null;
		Statement stmt = null;
		String updateQuery = null;
		try{
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			if(action.equalsIgnoreCase("click")){
				updateQuery = "update solrmaster set  clicked ="	+ numberoftimes + " where key like '%" + solrid + "%'";
			}
			if(action.equalsIgnoreCase("download")){
				updateQuery = "update solrmaster set  downloaded ="	+ numberoftimes + " where key like '%" + solrid + "%'";
			}
			if(action.equalsIgnoreCase("email")){
				updateQuery = "update solrmaster set  emailed ="	+ numberoftimes + " where key like '%" + solrid + "%'";
			}
			Log.Info("Postgres Solr Update Querty : " + updateQuery);
			stmt.executeUpdate(updateQuery);					
		}catch(AppException appe){
			Log.Error("AppException while updating solr master table." + appe);
		}catch (SQLException sql) {
			Log.Error("SQLException while updating solr master table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while updating solr master table." + e.getStackTrace());
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					if(stmt != null){
						stmt.close();
					}	
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
	}
	
	public static String getCategoryInPostGresTable(String solrid)  throws NullPointerException, AppException{
		Connection connection = null;
		Statement stmt = null;
		String categoryQuery = null;
		ResultSet rs = null;
		String category = null;
		try{
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			categoryQuery = "select categoryl0 from solrmaster where key = '" + solrid + "'";
			Log.Info("Postgres Solr Update Querty : " + categoryQuery);
			rs = stmt.executeQuery(categoryQuery);
			if(rs != null){
				while(rs.next()){
					category = rs.getString("categoryl0");
				}
			}
			return category;					
		}catch(AppException appe){
			Log.Error("AppException while selecting category from solr master table." + appe);

		}catch (SQLException sql) {
			Log.Error("SQLException while selecting category from solr master table." + sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 1 ,sql.getCause(), true);
		} catch (Exception e) {
			Log.Error("Exception while selecting category from solr master table." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n", ".");
			throw new AppException(newmessage,"Error while processing the request.", 2 ,e.getCause(), true);
		} finally {
			if (connection != null) {
				try {
					if(stmt != null){
						stmt.close();
					}	
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
			}
		}
		return category;
	}
	public static Connection getPostGresDBConnection() throws AppException, SQLException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Connection connection = dbUtil.getPostGresConnection();		
		return connection;
	}
	
	private static String getSystemDate(){
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
		Date date = new Date();
		return dateFormat.format(date);
		
	}
}