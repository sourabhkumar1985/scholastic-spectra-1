				package com.relevance.prism.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.PrimaryKey;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.FileUploadUtil;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/file")
public class FileUploadResource extends BaseResource {
	@POST
	@Path("/upload") 
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String uploadExcelFile(		
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail,
		@FormDataParam("env") String env,
		@FormDataParam("db") String db,
		@FormDataParam("table") String table,
		@FormDataParam("primarykeyslist") String primarykeyslist,
		@FormDataParam("configcolumns") String configcolumns, @Context HttpServletRequest req,
		@FormDataParam("defaultValueConfig") String defaultValueConfig){
		long startTime = System.currentTimeMillis();
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//String uploadedFileLocation = "C:/Madhan/" + fileDetail.getFileName();
		//String uploadedFileLocation = "d://uploaded/" + fileDetail.getFileName();
		String response = null;
		String userName = "";
		String userDisplayName = "";
		String tDir = System.getProperty("java.io.tmpdir");
		String uploadedFileLocation = tDir + File.separator + fileDetail.getFileName();
		System.out.println("File Path : " + uploadedFileLocation);
		File tempFile = new File(uploadedFileLocation);
		tempFile.delete();
		// save it
		writeToFile(uploadedInputStream, uploadedFileLocation);
		try{
			FileUploadUtil fileUploadUtil = new FileUploadUtil();
			/*ArrayList<String> primaryKeyNamesList = new ArrayList<>();
			primaryKeyNamesList.add("id");
			primaryKeyNamesList.add("name");*/
			Gson gson = new Gson();
			HttpSession session = req.getSession(true);
			userName = session.getAttribute("userName").toString();
			userDisplayName = session.getAttribute("userDisplayName").toString();
			PrimaryKey primaryKeys = gson.fromJson(primarykeyslist, PrimaryKey.class);
			response = fileUploadUtil.readAndUploadExcelData(uploadedFileLocation, env, db, table, primaryKeys.getPrimaryKeyNamesList(),configcolumns, userName, userDisplayName,defaultValueConfig);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		long endTime = System.currentTimeMillis();
		Log.Info("Inserting Records in Table Completed, total Time taken is  " +  E2emfAppUtil.getTotalElapsedTime(startTime, endTime) + " seconds");
		return response;
	}
	
	@POST
	@Path("/uploadcsv")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String uploadCSVFile(		
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail,
		@FormDataParam("env") String env,
		@FormDataParam("db") String db,
		@FormDataParam("table") String table,
		@FormDataParam("separator") String separator,
		@FormDataParam("primarykeyslist") String primarykeyslist,
		@FormDataParam("configcolumns") String configcolumns, @Context HttpServletRequest req) {
		long startTime = System.currentTimeMillis();
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//String uploadedFileLocation = "C:/Madhan/" + fileDetail.getFileName();
		//String uploadedFileLocation = "d://uploaded/" + fileDetail.getFileName();
		String response = null;
		String tDir = System.getProperty("java.io.tmpdir");
		String uploadedFileLocation = tDir + File.separator + fileDetail.getFileName();
		String userName = "";
		String userDisplayName = "";
		System.out.println("File Path : " + uploadedFileLocation);
		File tempFile = new File(uploadedFileLocation);
		tempFile.delete();
		// save it
		writeToFile(uploadedInputStream, uploadedFileLocation);
		try{
			FileUploadUtil fileUploadUtil = new FileUploadUtil();
			/*ArrayList<String> primaryKeyNamesList = new ArrayList<>();
			primaryKeyNamesList.add("id");
			primaryKeyNamesList.add("name");*/
			Gson gson = new Gson();
			HttpSession session = req.getSession(true);
			userName = session.getAttribute("userName").toString();
			userDisplayName = session.getAttribute("userDisplayName").toString();
			PrimaryKey primaryKeys = gson.fromJson(primarykeyslist, PrimaryKey.class);
			response = fileUploadUtil.readAndUploadCSVData(uploadedFileLocation, env, db, table, separator.charAt(0),primaryKeys.getPrimaryKeyNamesList(),configcolumns);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		long endTime = System.currentTimeMillis();
		Log.Info("Inserting Records in Table Completed, total Time taken is  " +  E2emfAppUtil.getTotalElapsedTime(startTime, endTime) + " seconds");
		return response;
	}
	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];
			//out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();				
		}catch(Exception ex){
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}
	
	@POST
	@Path("/uploadCSVToTable")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String uploadCSVToTable(		
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail,
		@FormDataParam("db") String db, 
		@FormDataParam("table") String table,		
		@FormDataParam("columnnames") String columnnames,@Context HttpServletRequest req 
		) {
		long startTime = System.currentTimeMillis();
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String userName = "";
		String response = null;
		String tDir = System.getProperty("java.io.tmpdir");
		String uploadedFileLocation = tDir + File.separator + fileDetail.getFileName();
		System.out.println("File Path : " + uploadedFileLocation);
		File tempFile = new File(uploadedFileLocation);
		tempFile.delete();
		// save it
		writeToFile(uploadedInputStream, uploadedFileLocation);
		try{
			HttpSession session = req.getSession(true);
			userName = session.getAttribute("userName").toString();
			FileUploadUtil fileUploadUtil = new FileUploadUtil();
			response = fileUploadUtil.uploadExcelData(uploadedFileLocation, db, table, columnnames, userName);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		long endTime = System.currentTimeMillis();
		Log.Info("Inserting Records in Table Completed, total Time taken is  " +  E2emfAppUtil.getTotalElapsedTime(startTime, endTime) + " seconds");
		return response;
	}
}	