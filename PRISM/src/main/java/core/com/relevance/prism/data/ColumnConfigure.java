package com.relevance.prism.data;

public class ColumnConfigure {
	
	private String groupbyaggregate;
	private String summaryaggregate;
	private String columnName;
	
	public String getGroupbyaggregate() {
		return groupbyaggregate;
	}
	public void setGroupbyaggregate(String groupbyaggregate) {
		this.groupbyaggregate = groupbyaggregate;
	}
	public String getSummaryaggregate() {
		return summaryaggregate;
	}
	public void setSummaryaggregate(String summaryaggregate) {
		this.summaryaggregate = summaryaggregate;
	}
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
}
