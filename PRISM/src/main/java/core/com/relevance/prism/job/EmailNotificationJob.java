package com.relevance.prism.job;

import java.util.Map;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.relevance.prism.service.MailService;
import com.relevance.prism.util.Log;

public class EmailNotificationJob extends QuartzJobBean {
	
	
	@Override
	protected void executeInternal(JobExecutionContext context) {
		try {
			 System.out.println("--------------------------------------------------------------------");
			         System.out.println("Starting Job " + context.getJobDetail().getFullName() + " at " + context.getFireTime());
			         JobDetail jobDetail = context.getJobDetail();
			         System.out.println(context.getJobDetail().getFullName() + " at " + context.getJobRunTime() + ", key: " + jobDetail.getKey());
			         System.out.println(context.getJobDetail().getFullName() + "'s next scheduled time: " + context.getNextFireTime());
			         System.out.println("--------------------------------------------------------------------");
			          

			Map<String,Object> mapData=context.getMergedJobDataMap();
			String appName=(String)mapData.get("appName");
			MailService mailService = new MailService();
			mailService.sendEmailNotifications(appName);
		} catch (Exception e) {
			Log.Error("Exception Occured....." + e);
		}
	}  

}
