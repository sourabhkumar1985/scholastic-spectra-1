package com.relevance.prism.data;

import com.relevance.prism.data.E2emfBusinessObject;

/**
 * 
 * E2emf Search Business Object  
 *
 */
public class SearchParam extends E2emfBusinessObject
{

	private String key;
	private String facet;
	private String start;
	private String rows;
	private String type;
	private String source;
	private String searchField;
	private String solrCollectionName;
	private String secondaryRoles;
	private String app;
	
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getFacet() {
		return facet;
	}
	public void setFacet(String facet) {
		this.facet = facet;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getRows() {
		return rows;
	}
	public void setRows(String rows) {
		this.rows = rows;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSolrCollectionName() {
		return solrCollectionName;
	}
	public void setSolrCollectionName(String solrCollectionName) {
		this.solrCollectionName = solrCollectionName;
	}
	public String getSecondaryRoles() {
		return secondaryRoles;
	}
	public void setSecondaryRoles(String secondaryRoles) {
		this.secondaryRoles = secondaryRoles;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
	
}
