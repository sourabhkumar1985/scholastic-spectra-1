package com.relevance.prism.dao;

/**
* Test desc
* @author: Shawkath Khan & Manikantan
* @Created_Date: Feb 17, 2016
* @Purpose: Workflow Engine Methods
* @Modified_By: 
* @Modified_Date:  
*/

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import com.relevance.prism.data.WfEntity;
import com.relevance.prism.data.WfTransition;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class WorkflowDao extends BaseDao{
	
	Connection con = null;
	Statement stmt = null;

	String workflow_db = null;
	
	public WorkflowDao() {
		workflow_db = E2emfAppUtil.getAppProperty(E2emfConstants.emeaDB);
	}
	
	public WfEntity getEntity(String entityId) throws SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		ResultSet rs = null;
		String query = null;
		WfEntity entity = null;
		try 
		{
			//con = dbUtil.getPostGresConnection(workflow_db);
			con = dbUtil.getPostGresConnection();
			Log.Debug("getEntity().. Connecting to DB..." +  workflow_db);
			statment = con.createStatement();
			query = "select entity_id, state, username, lastupdated, comments from wf_entity where lower(entity_id) = '" + entityId.toLowerCase() + "' limit 1"; 
			Log.Debug("Executing query..." +  query);
			System.out.println(query);
			rs = statment.executeQuery(query);
			//ResultSetMetaData rsmd = rs.getMetaData();
			entity = new WfEntity(); 
			
			if (rs != null) 
			{
				if (rs.next()) 
				{
					entity.setEntityId(rs.getString("entity_id"));
					entity.setState(rs.getString("state"));
					entity.setUser(rs.getString("username"));
					entity.setLastUpdated(rs.getTimestamp("lastupdated"));
					entity.setComments(rs.getString("comments"));
				}
			}
			
			rs.close();
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return entity;
	}
	
	public ArrayList<WfEntity>  getEntityList() throws  SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<WfEntity> wfEntityList = new ArrayList<WfEntity>();
		try 
		{
			//con = dbUtil.getPostGresConnection(workflow_db);
			con = dbUtil.getPostGresConnection();
			Log.Debug("getEntity().. Connecting to DB..." +  workflow_db);
			statment = con.createStatement();
			query = "select entity_id, state, username, lastupdated, comments from wf_entity order by entity_id"; 
			Log.Debug("Executing query..." +  query);
			System.out.println(query);
			rs = statment.executeQuery(query);
			if (rs != null) 
			{
				while(rs.next()) 
				{
					WfEntity wfEntity = new WfEntity();
					wfEntity.setEntityId(rs.getString("entity_id"));
					wfEntity.setState(rs.getString("state"));
					wfEntity.setUser(rs.getString("username"));
					wfEntity.setLastUpdated(rs.getTimestamp("lastupdated"));
					wfEntity.setComments(rs.getString("comments"));
					wfEntityList.add(wfEntity);
				}
			}
			rs.close();
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return wfEntityList;		
	}
	
	public ArrayList<WfTransition>  getTransitionList() throws SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<WfTransition> wfTransitionList = new ArrayList<WfTransition>();
		try 
		{
			//con = dbUtil.getPostGresConnection(workflow_db);
			con = dbUtil.getPostGresConnection();
			Log.Debug("getEntity().. Connecting to DB..." +  workflow_db);
			statment = con.createStatement();
			query = "select key, current_state, action, role, target_state,displayorder from wf_transition order by displayorder"; 
			Log.Debug("Executing query..." +  query);
			System.out.println(query);
			rs = statment.executeQuery(query);
			if (rs != null) 
			{
				while(rs.next()) 
				{
					WfTransition trans = new WfTransition(); 
					trans.setKey(rs.getInt("key"));
					trans.setCurrentState(rs.getString("current_state"));
					trans.setAction(rs.getString("action"));
					trans.setRole(rs.getString("role"));
					trans.setTargetState(rs.getString("target_state"));
					trans.setDisplayOrder(rs.getInt("displayOrder"));
					wfTransitionList.add(trans);
				}
			}
			rs.close();
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return wfTransitionList;		
	}
	
	public String addToWorkflow(WfEntity entity) throws SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		try {
			//call dao to insert record into wf_entity table
			String entityId = entity.getEntityId();
			String startState = entity.getState();
			String userId = entity.getUser();
			String lastupdated = new Date().toString(); 
			String comments = entity.getComments();
			con = dbUtil.getPostGresConnection();
			statment = con.createStatement();
			Log.Debug("addToWorkflow().. Connecting to DB..." +  workflow_db);
			String query = "insert into wf_entity (entity_id, state, username, lastupdated, comments) values ('"+ entityId + "','" + startState + "','" + userId +  "','" + lastupdated + "','" + comments + "')"; 
			Log.Debug("Executing query..." +  query);
			statment.executeUpdate(query);
		} 
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, null, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());			
		return "{\"message\":\"Successfully inserted the entity\"}";
	}
	
	public ArrayList<WfTransition> getNextTransitions(WfEntity entity, String userRole) throws SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		ResultSet rs = null;
		ArrayList<WfTransition> transList = new ArrayList<WfTransition>(); 
		try 
		{
			//con = dbUtil.getPostGresConnection(workflow_db);
			con = dbUtil.getPostGresConnection();
			Log.Debug("getNextTransitions().. Connecting to DB..." +  workflow_db);
			statment = con.createStatement();
			String query = "select key, current_state, action, role, target_state from wf_transition where current_state = '" + entity.getState() + "' and role = '" + userRole + "'"; 
			Log.Debug("Executing query..." +  query);
			System.out.println(query);
			rs = statment.executeQuery(query);
			
			if (rs != null) 
			{
				while (rs.next()) 
				{
					WfTransition trans = new WfTransition(); 
					trans.setKey(rs.getInt("key"));
					trans.setCurrentState(rs.getString("current_state"));
					trans.setAction(rs.getString("action"));
					trans.setRole(rs.getString("role"));
					trans.setTargetState(rs.getString("target_state"));
					
					transList.add(trans);
				}
			}
			
			rs.close();
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());		
		return transList;
	}
	
	public String applyNextTransition(WfEntity entity, WfTransition trans, String user, String comments) throws SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		ResultSet rs = null;
		try 
		{
			//con = dbUtil.getPostGresConnection(workflow_db);
			con = dbUtil.getPostGresConnection();
			Log.Debug("applyNextTransition().. Connecting to DB..." +  workflow_db);
			statment = con.createStatement();
			
			String query = "update wf_entity set state = '" + trans.getTargetState() + "' where entity_id = '" + entity.getEntityId() + "'"; 
			//update user id, timestamp, comments
			
			Log.Debug("Executing query..." +  query);
			statment.executeUpdate(query);
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());		
		return "{\"message\":\"Successfully added the next transition\"}";
	}
	
	public boolean updateWorkFlowStatus(String entityid, String status,String updatedby,String comments) throws Exception{
		Connection con = null;
		Statement stmt=null;
		ResultSet rs=null;				
		try {
			Log.Info("updateStatus() method called on CaseStatusDao");
			String query = "UPDATE wf_entity  set  state='"+ status +"' WHERE entity_id='"+ entityid +"'";
			 con = dbUtil.getPostGresConnection();
			 stmt =  con.createStatement();
			 stmt.executeUpdate(query);
			 String logsQuery = "INSERT INTO wf_entity_audit(entity_id, state, username, comments) VALUES ('"+entityid+"','"+status+"','"+updatedby+"','"+comments+"')";
			 stmt.executeUpdate(logsQuery);
		}catch (Exception ex) {
			PrismHandler.handleException(ex, true);
			return false;
		}finally{
			PrismHandler.handleFinally(con, rs, stmt);
		}		
		return true;		
	}	
	
	public ArrayList<WfEntity>  getWorkFlowAuditList(String entityid) throws  SQLException, NullPointerException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement statment = null;
		ResultSet rs = null;
		String query = null;
		ArrayList<WfEntity> wfEntityList = new ArrayList<WfEntity>();
		try 
		{
			con = dbUtil.getPostGresConnection();
			Log.Debug("getEntity().. Connecting to DB..." +  workflow_db);
			statment = con.createStatement();
			query = "select entity_id, state, username, lastupdated, comments from wf_entity_audit where entity_id in('"+entityid+"')"; 
			Log.Debug("Executing query..." +  query);
			System.out.println(query);
			rs = statment.executeQuery(query);
			if (rs != null) 
			{
				while(rs.next()) 
				{
					WfEntity wfEntity = new WfEntity();
					wfEntity.setEntityId(rs.getString("entity_id"));
					wfEntity.setState(rs.getString("state"));
					wfEntity.setUser(rs.getString("username"));
					wfEntity.setLastUpdated(rs.getTimestamp("lastupdated"));
					wfEntity.setComments(rs.getString("comments"));
					wfEntityList.add(wfEntity);
				}
			}
			rs.close();
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, statment);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return wfEntityList;		
	}
}
