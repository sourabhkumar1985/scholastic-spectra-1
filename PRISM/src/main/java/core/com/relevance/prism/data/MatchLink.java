package com.relevance.prism.data;

import java.util.ArrayList;

public class MatchLink {

	private String name;
	private Row srcRow;
	private ArrayList<Row> tgtRows;
	private int linkPriority;
	private DistanceObject distance;
	
	public MatchLink()
	{
		name = null;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DistanceObject getDistance() {
		return distance;
	}
	public void setDistance(DistanceObject distance) {
		this.distance = distance;
	}
	public Row getSrcRow() {
		return srcRow;
	}
	public void setSrcRow(Row srcRow) {
		this.srcRow = srcRow;
	}
	public ArrayList<Row> getTgtRows() {
		return tgtRows;
	}
	public void setTgtRows(ArrayList<Row> tgtRows) {
		this.tgtRows = tgtRows;
	}
	public int getLinkPriority() {
		return linkPriority;
	}
	public void setLinkPriority(int linkPriority) {
		this.linkPriority = linkPriority;
	}
}
