package com.relevance.prism.job;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import com.relevance.prism.service.MailService;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.ViewDataLoad;

public class ViewDataLoadNotificationJob extends QuartzJobBean {
	
	/**
	 * Description : Program to run data load job for oracle views to postgres DB and send mail
	 * 
	 * @authors : Mallikarjun Mundasi
	 */

	@Override
	protected void executeInternal(JobExecutionContext context) {
		try {
			// Job Details
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Starting Job " + context.getJobDetail().getFullName() + " at " + context.getFireTime());
			JobDetail jobDetail = context.getJobDetail();
			System.out.println(context.getJobDetail().getFullName() + " at " + context.getJobRunTime() + ", key: "
					+ jobDetail.getKey());
			System.out.println(
					context.getJobDetail().getFullName() + "'s next scheduled time: " + context.getNextFireTime());
			System.out.println("--------------------------------------------------------------------");
			ViewDataLoad viewDataLoad = new ViewDataLoad();
			boolean item_result = viewDataLoad.insertDataForItemAttributeView();
			boolean bom_result = viewDataLoad.insertDataForBOMview();
			/*boolean item_result = ViewDataLoad.insertDataForItemAttributeView();
			boolean bom_result = ViewDataLoad.insertDataForBOMview();*/
			if (item_result) {
				MailService mailService = new MailService();
				String mailContent = "Item attribute view data has been loaded";
				String subject = "INTEGRA ITEM ATTRIBUTE VIEW DATA LOAD";
				String app = "INTEGRA";
				mailService.sendEmail(mailContent, subject, app, null); // sends
																		// mail
																		// to
																		// recipients
																		// with
																		// subject
																		// line.
			}
			if (bom_result) //
			{
				MailService mailService = new MailService();
				String mailContent = "BOM attribute view data has been loaded";
				String subject = "INTEGRA BOM ATTRIBUTE VIEW DATA LOAD";
				String app = "INTEGRA";
				mailService.sendEmail(mailContent, subject, app, null);
			}
		} catch (Exception e) {
			Log.Error("Exception Occured....." + e);
		}
	}
}
