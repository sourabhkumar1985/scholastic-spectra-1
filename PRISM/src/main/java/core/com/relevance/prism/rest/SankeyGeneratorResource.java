package com.relevance.prism.rest;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.data.Sankey;
import com.relevance.prism.data.SankeyConfig;
import com.relevance.prism.service.SankeyGeneratorService;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

@Path("/sankeyGenerator")
public class SankeyGeneratorResource extends BaseResource {

	@Context
	ServletContext context;

	public SankeyGeneratorResource(@Context ServletContext value) {
		this.context = value;
		// System.out.println("Conext:" + this.context);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getJSON")
	public String getJSON(@FormParam("config") String request, @FormParam("source") String source,
			@FormParam("database") String database) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		Sankey sankey = new Sankey();
		SankeyConfig sankeyConfig = new SankeyConfig();
		Gson gson = new Gson();
		String configString = "";
		try {

			sankeyConfig = gson.fromJson(request, SankeyConfig.class);
			configString = gson.toJson(sankeyConfig);
			int requestHashCode = configString.hashCode();
			Object requestObject = this.context.getAttribute("PRISM_" + Integer.toString(requestHashCode));
			if (!"true".equalsIgnoreCase(E2emfAppUtil.getAppProperty(E2emfConstants.enableCache))
					|| requestObject == null) {
				SankeyGeneratorService sankeyGeneratorService = new SankeyGeneratorService();
				sankey = sankeyGeneratorService.getJSON(source, database, sankeyConfig);
				response = gson.toJson(sankey);
				this.context.setAttribute("PRISM_" + Integer.toString(requestHashCode), response);
			} else {
				response = requestObject.toString();
			}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filterLookUp")
	public String getLookupData(@QueryParam("source") String source, @QueryParam("database") String database,
			@QueryParam("lookupQuery") String lookupQuery) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());

		Log.Info("Recieved filterLookUp Request with param " + source + " , " + database + ", " + lookupQuery);
		String response = null;
		try {
			Gson gson = new Gson();
			SankeyGeneratorService sankeyGeneratorService = new SankeyGeneratorService();
			HashMap<String, String> lookupValues = sankeyGeneratorService.getLookUpValues(source, database,
					lookupQuery);
			response = gson.toJson(lookupValues);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/suggestionsLookUp")
	public String getSearchSuggestions(@QueryParam("q") String q, @QueryParam("source") String source,
			@QueryParam("database") String database, @QueryParam("lookupQuery") String lookupQuery) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Recieved Suggestions LookUp Request with param " + source + " , " + database + ", " + lookupQuery);
		String response = null;
		String configString = "";
		try {
			Gson gson = new Gson();
			SankeyGeneratorService sankeyGeneratorService = new SankeyGeneratorService();
			HashMap<String, String> param = new HashMap<String, String>();
			param.put("query", q);
			param.put("source", source);
			param.put("database", database);
			param.put("lookupQuery", lookupQuery);
			configString = gson.toJson(param);
			int requestHashCode = configString.hashCode();
			Object requestObject = this.context.getAttribute("PRISM_" + Integer.toString(requestHashCode));
			if (!"true".equalsIgnoreCase(E2emfAppUtil.getAppProperty(E2emfConstants.enableCache))
					|| requestObject == null) {
				List<MasterItem> suggestionValues = sankeyGeneratorService.getSearchSuggestions(q, source, database,
						lookupQuery);
				response = gson.toJson(suggestionValues);
				this.context.setAttribute("PRISM_" + Integer.toString(requestHashCode), response);
			} else {
				response = requestObject.toString();
			}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
