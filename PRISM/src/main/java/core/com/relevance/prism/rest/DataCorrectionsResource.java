package com.relevance.prism.rest;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.service.DataCorrectionService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.data.NGramList;

@Path("/dataCorrections")
public class DataCorrectionsResource extends BaseResource{
	@Context
	ServletContext context;

	public DataCorrectionsResource(@Context ServletContext value) {
		this.context = value;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDataUnCorrectedWords")
	public String getUnCorrectedData() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {

			DataCorrectionService dataCorrectionService = new DataCorrectionService();
			NGramList gramView = dataCorrectionService.getUnCorrectedData();
			Gson gson = new Gson();
			response = gson.toJson(gramView);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getDataCorrectedWords")
	public String getCorrectedData() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {

			DataCorrectionService dataCorrectionService = new DataCorrectionService();
			NGramList gramView = dataCorrectionService.getCorrectedData();
			Gson gson = new Gson();
			response = gson.toJson(gramView);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
