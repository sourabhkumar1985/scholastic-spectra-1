package com.relevance.prism.service;
import com.relevance.prism.dao.SolenisDao;
import com.relevance.prism.util.PrismHandler;

public class SolenisService extends BaseService{
	//getDataFromExcel
		public String processcpusimulation(String uploadedFileLocation,String type) throws Exception{
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			String response = null;
			try {
				SolenisDao solenisDao = new SolenisDao();
				response = solenisDao.processcpusimulation(uploadedFileLocation,type);
			}catch(Exception e){
				PrismHandler.handleException(e, true);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			return response;
		}
}
