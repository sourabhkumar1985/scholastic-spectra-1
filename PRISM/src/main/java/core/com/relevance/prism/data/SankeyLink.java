package com.relevance.prism.data;

import java.util.ArrayList;

public class SankeyLink {
	
	private String key;
	private int fromStack;
	private int toStack;
	private String fromStackColor;
	private String toStackColor;
	private String query;
	private String linkColor;
	private int limit;
	private ArrayList<String> filterKeyList;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public ArrayList<String> getFilterKeyList() {
		return filterKeyList;
	}
	public void setFilterKeyList(ArrayList<String> filterKeyList) {
		this.filterKeyList = filterKeyList;
	}
	
	public int getFromStack() {
		return fromStack;
	}
	public void setFromStack(int fromStack) {
		this.fromStack = fromStack;
	}
	public int getToStack() {
		return toStack;
	}
	public void setToStack(int toStack) {
		this.toStack = toStack;
	}
	public String getFromStackColor() {
		return fromStackColor;
	}
	public void setFromStackColor(String fromStackColor) {
		this.fromStackColor = fromStackColor;
	}
	public String getToStackColor() {
		return toStackColor;
	}
	public void setToStackColor(String toStackColor) {
		this.toStackColor = toStackColor;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getLinkColor() {
		return linkColor;
	}
	public void setLinkColor(String linkColor) {
		this.linkColor = linkColor;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
}
