package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.data.ContextHelp;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class ContextHelpDao extends BaseDao {
	
	public String insertFaq(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String returnMessage = null;
		Statement stmt = null;
		ContextHelp param = null;
		Connection connection = null;
		if (obj != null) {
			param =(ContextHelp) obj[0];
			Log.Info("Inserting  Values for ContextHelp Object : " + param);
			try {
				String insertQuery = "insert into faq (question, answer, screen, role, enabled) values('"
						+ param.getQuestion()
						+ "','"
						+ param.getAnswer()
						+ "','"
						+ param.getScreen()
						+ "','"
						+ param.getRole()
						+ "','"
						+ param.getEnabled()
						+ "')";
				
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				System.out.println(insertQuery);
				stmt.executeUpdate(insertQuery);
				returnMessage =  "{\"message\":\"successfully inserted faq in table\"}";
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}				
		}
		else {
			Log.Error("Input obj is null");
			returnMessage = "{\"message\":\"Failed to  inserted faq in table\"}";
		}		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return returnMessage;
	}
		
	public String updateFaq(Object... obj) throws Exception {		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ContextHelp param = null;
		String returnMessage = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (ContextHelp) obj[0];
			Log.Info("Updating the faq in DB : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String deleteQueryForEnabled = "update faq set question='"
						+ param.getQuestion() + "',answer='"
						+ param.getAnswer() + "',screen='"
						+ param.getScreen() + "',role='"
						+ param.getRole() + "',enabled='"
						+ param.getEnabled() +"' where id = "
						+ param.id ;
				stmt.executeUpdate(deleteQueryForEnabled);
				returnMessage = "{\"message\" : \"successfully updated faq in DB.\"}";
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}				
		}
		else {
			Log.Error("Input obj is null");
			returnMessage = "{\"message\" : \"Failed to  updated faq in DB.\"}";
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return returnMessage;
	}
	
	public String deleteFaq(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String returnMessage = null;
		ContextHelp param = null;
		Statement stmt = null;
		Connection connection = null;
		if (obj != null) {
			param = (ContextHelp) obj[0];
			Log.Info("Deleting the faq from DB : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String deleteFaqQuery="delete from faq where id = "
						+param.getId();				
				stmt.executeUpdate(deleteFaqQuery);
				returnMessage = "{\"message\" : \"successfully deleted the faq from DB\"}";
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}	
		}
		else {
			Log.Error("Input Object is null");
			returnMessage = "{\"message\" : \"failed to  deleted the faq from DB\"}";
		}		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return returnMessage;
	}

	public ContextHelp getFaq(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());		
		ContextHelp param = null;
		ContextHelp contextHelp = null;
		Statement stmt = null;
		Connection connection = null;
		if (obj != null) {
			contextHelp = new ContextHelp();
			param = (ContextHelp) obj[0];
			Log.Info("Fetching ContextHelp Object for : " + param);
			try {
				String query = "select id, question, answer, screen, role, enabled from faq where id="
						+ param.id;
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
			System.out.println(query);
				ResultSet rs = stmt.executeQuery(query);

				if (rs != null) {
					while (rs.next()) {
						contextHelp.setId(rs.getInt("id"));
						contextHelp.setQuestion(rs.getString("question"));
						contextHelp.setAnswer(rs.getString("answer"));
						contextHelp.setScreen(rs.getString("screen"));
						contextHelp.setRole(rs.getString("role"));
						contextHelp.setEnabled(rs.getLong("enabled"));
					}
				}				
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}	
		}
		else {
			Log.Error("Input object is null");
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return contextHelp;
	}
		
	public List<ContextHelp> getFaqList(String role) throws Exception {
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ContextHelp> faqList = new ArrayList<>();
		ContextHelp contextHelpToStore = null;
		Statement stmt  = null;
		Connection connection = null;
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		try {
			query.append("select * from faq");
			if (role != null && !role.isEmpty()){
				query.append(" where ");
				query.append("concat(concat(',',role),',') LIKE '%,").append(role).append(",%'");
			}
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				contextHelpToStore = new ContextHelp();
				contextHelpToStore.setId(rs.getLong("id"));
				contextHelpToStore.setQuestion(rs.getString("question"));
				contextHelpToStore.setAnswer(rs.getString("answer"));
				contextHelpToStore.setScreen(rs.getString("screen"));
				contextHelpToStore.setRole(rs.getString("role"));
				contextHelpToStore.setEnabled(rs.getLong("enabled"));
				faqList.add(contextHelpToStore);
			}
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return faqList;
	}	
}
