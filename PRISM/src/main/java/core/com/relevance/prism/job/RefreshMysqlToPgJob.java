package com.relevance.prism.job;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.relevance.prism.util.Log;
import com.relevance.prism.util.RefreshMysqlToPG;

public class RefreshMysqlToPgJob extends QuartzJobBean {
	@Override
	protected void executeInternal(JobExecutionContext context) {
		try {

			System.out.println("--------------------------------------------------------------------");
			System.out.println("Starting Job " + context.getJobDetail().getFullName() + " at " + context.getFireTime());
			JobDetail jobDetail = context.getJobDetail();
			System.out.println(context.getJobDetail().getFullName() + " at " + context.getJobRunTime() + ", key: "
					+ jobDetail.getKey());
			System.out.println(
					context.getJobDetail().getFullName() + "'s next scheduled time: " + context.getNextFireTime());
			System.out.println("--------------------------------------------------------------------");
			RefreshMysqlToPG refreshMysqlToPG = new RefreshMysqlToPG();
			refreshMysqlToPG.refreshMysql();
		} catch (Exception e) {
			Log.Error("Exception Occured....." + e);
		}
	}

}
