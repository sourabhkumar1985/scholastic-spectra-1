package com.relevance.prism.data;

/**
 *
 * @author: Shawkath Khan
 * @Created_Date: Feb 17, 2016
 * @Purpose: Entity for Workflow Role
 * @Modified_By: 
 * @Modified_Date:  
 */

public class WfRole {

	private String role;
	private String description;
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
