package com.relevance.prism.data;

import java.util.HashMap;

public class E2emfField {
	
	private String fieldName = null;
	private String aggregateValue = null;
	private String numeric = null;
	
	private HashMap<String,String> groupByMap = null;
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}	
	public String getAggregateValue() {
		return aggregateValue;
	}
	public void setAggregateValue(String aggregateValue) {
		this.aggregateValue = aggregateValue;
	}
	public HashMap<String, String> getGroupByMap() {
		return groupByMap;
	}
	public void setGroupByMap(HashMap<String, String> groupByMap) {
		this.groupByMap = groupByMap;
	}
	public String getNumeric() {
		return numeric;
	}
	public void setNumeric(String numeric) {
		this.numeric = numeric;
	}	
}
