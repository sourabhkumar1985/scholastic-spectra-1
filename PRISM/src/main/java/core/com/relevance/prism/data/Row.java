package com.relevance.prism.data;

import java.util.HashMap;

public class Row 
{
	private HashMap<String, String> row;
	private boolean matched;

	public Row()
	{
		row = new HashMap<String, String>();
	}
	
	public boolean isMatched() {
		return matched;
	}
	public void setMatched(boolean matched) {
		this.matched = matched;
	}
	public HashMap<String, String> getRow() {
		return row;
	}

	public void setRow(HashMap<String, String> row) {
		this.row = row;
	}
}
