package com.relevance.prism.security;

import org.springframework.security.core.GrantedAuthority;

public enum SecurityContextAuthority implements GrantedAuthority {
	
	ROLE_ADMIN;
	 
    @Override
    public String getAuthority() {
        return name();
    }   

}
