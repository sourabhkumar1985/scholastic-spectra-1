package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.relevance.prism.data.NGram;
import com.relevance.prism.data.NGramConfig;
import com.relevance.prism.data.NGramList;
import com.relevance.prism.service.NGramAlgorithm;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class NGramDao extends BaseDao {

	HashMap<String, Integer> ngrams;
	HashMap<String, Integer> unigrams = new HashMap<>();
	String thetext;
	String cutoff;
	String punc;
	String[] previous = new String[] { "", "", "", "", "" };
	int total = 0;
	String gram;
	String current = "";
	String[] unImportantWords = new String[] { "please", "i", "me", "my",
			"myself", "we", "us", "our", "ours", "ourselves", "you", "your",
			"yours", "yourself", "yourselves", "he", "him", "his", "himself",
			"she", "her", "hers", "herself", "it", "its", "itself", "they",
			"them", "their", "theirs", "themselves", "what", "which", "who",
			"whom", "whose", "this", "that", "these", "those", "am", "is",
			"are", "was", "were", "be", "been", "being", "have", "has", "had",
			"having", "do", "does", "did", "doing", "will", "would", "should",
			"can", "could", "ought", "i'm", "you're", "he's", "she's", "it's",
			"we're", "they're", "i've", "you've", "we've", "they've", "i'd",
			"you'd", "he'd", "she'd", "we'd", "they'd", "i'll", "you'll",
			"he'll", "she'll", "we'll", "they'll", "isn't", "aren't", "wasn't",
			"weren't", "hasn't", "haven't", "hadn't", "doesn't", "didn't",
			"won't", "wouldn't", "shan't", "shouldn't", "can't", "cancouldn't",
			"mustn't", "let's", "that's", "who's", "what's", "here's",
			"there's", "when's", "where's", "why's", "how's", "a", "an", "the",
			"and", "but", "if", "or", "because", "as", "until", "while", "of",
			"at", "by", "for", "with", "about", "against", "between", "into",
			"through", "during", "before", "after", "above", "below", "to",
			"from", "up", "upon", "down", "in", "out", "on", "off", "over",
			"under", "again", "further", "then", "once", "here", "there",
			"when", "where", "why", "how", "all", "any", "both", "each", "few",
			"more", "most", "other", "some", "such", "no", "nor", "only",
			"own", "same", "so", "than", "too", "very", "say", "says", "said",
			"shall", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b",
			"c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
			"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "\n", "\\n" };

	public NGramList nGramAnalyser(String input, int gram) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		NGramList nGramView = new NGramList();
		try {
			ngrams = new HashMap<>();
			processLine(gram, input);
			List<NGram> nGramList = new ArrayList<>();
			Iterator<Map.Entry<String, Integer>> it;
			if (gram == 1) {
				it = unigrams.entrySet().iterator();
			} else {
				it = ngrams.entrySet().iterator();
			}
			while (it.hasNext()) {
				Map.Entry<String, Integer> pairs = it.next();
				NGram nGram = new NGram();
				nGram.setText(pairs.getKey());
				nGram.setSize(pairs.getValue());
				nGramList.add(nGram);
			}
			Collections.sort(nGramList);
			if (nGramList.size() > 100) {
				List<NGram> subItems = new ArrayList<>(
						nGramList.subList(0, 100));
				nGramView.setNGramAnalyser(subItems);
			} else
				nGramView.setNGramAnalyser(nGramList);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return nGramView;
	}

	public Map<String, Integer> nGramAnalyserHasMap(String input, int gram)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, Integer> response = null;
		try {
			ngrams = new HashMap<>();
			unigrams = new HashMap<>();

			processLine(gram, input);
			if (gram == 1) {
				response = unigrams;
			} else {
				response = ngrams;
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	private static String processWord(String x) {
		return x.replaceAll("[|,.;:!?<>%=\\\"”/]", "");
	}

	public Map<String, Integer> performNgram(NGramConfig nGramConfig)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Iterator<Map.Entry<String, Integer>> it;
		Map<String, Integer> nGramHashMap = new HashMap<>();
		Map<String, Integer> nGramHashMapTemp = null;
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuilder query;
		String whereCondition;
		try {
			query = new StringBuilder();
			whereCondition = nGramConfig.getWhereCondition();
			DataAnalyserDao dataAnalyserDao = new DataAnalyserDao();
			con = dataAnalyserDao.getDatabaseConnection(
					nGramConfig.getSource(), nGramConfig.getDatabase());
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			query.append("SELECT ").append(nGramConfig.getField())
					.append(" FROM ").append(nGramConfig.getTable());
			if (whereCondition != null && !whereCondition.isEmpty())
				query.append(" ").append(whereCondition);
			rs = stmt.executeQuery(query.toString());
			int gramValue = nGramConfig.getGram();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			List<Future<Map<String, Integer>>> list = new ArrayList<>();
			while (rs.next()) {
				Future<Map<String, Integer>> future = executor
						.submit(new NGramAlgorithm(rs.getString(1), gramValue));
				list.add(future); 
			} 
			for (Future<Map<String, Integer>> fut : list) {
				nGramHashMapTemp = fut.get();
				it = nGramHashMapTemp.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Integer> pairs = it.next();
					Integer tempValue = nGramHashMap.get(pairs.getKey());
					if (tempValue != null) {
						nGramHashMap.put(pairs.getKey(),
								tempValue + pairs.getValue());
					} else {
						nGramHashMap.put(pairs.getKey(), pairs.getValue());
					}
				}
			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		return nGramHashMap;
	}

	public void processLine(int n, String line) throws Exception {
		try {
			boolean found;
			String[] words = line.split("[\\s+]");
			for (String word : words) {
				if (word.trim() != "") {
					String processedWord = word.trim();
					if (!"".equals(processedWord)
							&& !Arrays.asList(unImportantWords).contains(
									processedWord.toLowerCase())) {
						if (unigrams.containsKey(processedWord))
							unigrams.put(processedWord,
									unigrams.get(processedWord) + 1);
						else
							unigrams.put(processedWord, 1);

						gram = "";
						total++;
						found = false;
						for (int i = n - 2; i >= 0; i--) {
							if (previous[i].trim() != "") {
								found = true;
								if (gram.trim() == "")
									gram = previous[i].trim();
								else
									gram += " " + previous[i].trim();
							}
						}
						if (processedWord != "" && found) {
							if (gram.trim() == "")
								gram = processedWord;
							else
								gram += " " + processedWord;

							if (gram.trim().split(" ").length == n) {
								gram = gram.toLowerCase();
								if (ngrams.containsKey(gram))
									ngrams.put(gram, ngrams.get(gram) + 1);
								else
									ngrams.put(gram, 1);
							}
						}
						previous[4] = previous[3];
						previous[3] = previous[2];
						previous[2] = previous[1];
						previous[1] = previous[0];

						previous[0] = processedWord;
					}
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
	}
	
	public List<NGramList> getNgramFilter(NGramList param) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
//		Integer profileId;
		String ngramType;
		List<NGramList> nGramList = new ArrayList<>();
		try {
			userName = param.getuserName();
//			profileId = param.getProfileId();
			ngramType = param.getNgramType();
			
			StringBuilder query = new StringBuilder(
					"select ngram_id, ngram_key, "+((!ngramType.equals("stopwords"))? "ngram_value, ":"" )+"ngram_type, user_name from ngram_filter_"+ngramType+" ");
//			if (profileId > 0) {
//				query.append("where profile_id='").append(profileId).append("'");
//			} else if (userName != null) {
				query.append("where user_name='").append(userName).append("'");
//			}
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				NGramList ngram = new NGramList();
				int ngramId  = rs.getInt("ngram_id");
				String ngramkey = rs.getString("ngram_key");
				
				if(!ngramType.equals("stopwords")){
					String ngramValue = rs.getString("ngram_value");
					ngram.setNgramValue(ngramValue);
				}

				ngram.setNgramID(ngramId);
				ngram.setNgramKey(ngramkey);
				ngram.setNgramType(ngramType);
				ngram.setUserName(userName);
//				ngram.setProfileId(profileId);
				
				nGramList.add(ngram);
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return nGramList;
	}
	
	public String addNgramFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		PreparedStatement pstmt = null;
		int reponse = 0;
		ResultSet rs = null;
		if (obj != null) {
			Log.Info("Inserting  Values for ngram_filter_"+obj[4]+" Object:" + obj);
			try {
				connection = getPostGresDBConnection();
				String inertQuery =  null;
				
				if(!obj[4].equals("stopwords")){
					inertQuery = "insert into ngram_filter_"+obj[4]+" (ngram_key,ngram_value,ngram_type,user_name) values(?,?,?,?)";
				} else {
					inertQuery = "insert into ngram_filter_"+obj[4]+" (ngram_key,ngram_type,user_name) values(?,?,?)";
				}
				pstmt = connection.prepareStatement(inertQuery,Statement.RETURN_GENERATED_KEYS);
	            pstmt.setString(1, (String) obj[2]);
	            if(!obj[4].equals("stopwords")){
	            	pstmt.setString(2, (String) obj[3]);
		            pstmt.setString(3, (String) obj[4]);
		            pstmt.setString(4, (String) obj[0]);
//		            pstmt.setInt(5, (Integer) obj[1]);
	            } else {
		            pstmt.setString(2, (String) obj[4]);
		            pstmt.setString(3, (String) obj[0]);
//		            pstmt.setInt(4, (Integer) obj[1]);
	            }
	            pstmt.executeUpdate();
	            rs = pstmt.getGeneratedKeys();
	            if(rs != null && rs.next()){
	                System.out.println("Generated Emp Id: "+rs.getInt(1));
	                reponse = rs.getInt(1);
	            }
	 
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, pstmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"response\":"+reponse+"}";
	}
	
	public String updateNgramFilter(Object...obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Integer ngramId = null;
		String ngramKey = null;
		String ngramValue = null;
		String ngramType = null;
		Connection connection = null;
		Statement stmt = null;
		try {
			ngramId = (Integer) obj[0];
			ngramKey = (String) obj[1];
			ngramValue = (String) obj[2];
			ngramType = (String) obj[3];
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			String updateQuery = null;
			if(!ngramType.equals("stopwords")){
				updateQuery = "update ngram_filter_"+ngramType+" set ngram_key='" + ngramKey + "', ngram_value='" +ngramValue+"' where ngram_id="+ ngramId +"";
			} else {
				updateQuery = "update ngram_filter_"+ngramType+" set ngram_key='" + ngramKey + "' where ngram_id="+ ngramId +"";
			}
			stmt.executeUpdate(updateQuery);			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated Field in DB.\"}";
	}
	
	public String deleteNgramFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Integer ngramId = null;
		String ngramType = null;
		Connection connection = null;
		Statement stmt = null;
			try {
				ngramId = (Integer) obj[0];
				ngramType = (String) obj[1];
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String tableAndFieldsDeleteQuery = "delete from ngram_filter_"+ngramType+" where ngram_id = " + ngramId +"" ;
				stmt.executeUpdate(tableAndFieldsDeleteQuery);
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			return "{\"message\" : \"successfully deleted the Field from DB\"}";
	}
	
	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}
}
