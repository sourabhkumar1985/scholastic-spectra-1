package com.relevance.prism.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.ProfileAction;
import com.relevance.prism.data.ProfileView;
import com.relevance.prism.service.ProfileActionsService;
import com.relevance.prism.util.PrismHandler;

@Path("/profileActions")
public class ProfileActionsResource  extends BaseResource{
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getProfileActions")
	public String getProfileActions(@FormParam("profileID") int profileID,@FormParam("id") int id,@FormParam("profileIds") String profileIds) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ProfileAction> profileActionList = null;
		String response = null;
		try {
			ProfileActionsService profileActionsService = new ProfileActionsService();
			profileActionList = profileActionsService.getProfileActions(profileID,id,profileIds);
			Gson gson = new Gson();
			response = gson.toJson(profileActionList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/saveProfileAction")
	public String saveProfileAction(@FormParam("action") String action) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ProfileAction profileAction;
		List<ProfileAction> profileActionList;
		String response = null;
		try {
			Gson gson = new Gson();
			profileAction = gson.fromJson(action, ProfileAction.class);
			ProfileActionsService profileActionsService = new ProfileActionsService();
			profileActionList = profileActionsService.saveProfileAction(profileAction);
			response = gson.toJson(profileActionList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
