package com.relevance.prism.service;

import com.relevance.prism.dao.RegulatoryAffairDao;
import com.relevance.prism.util.PrismHandler;

public class RegulatoryAffairService extends BaseService {
	public String generateMapping() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseList = null;
		try {
			RegulatoryAffairDao regulatoryAffairDao = new RegulatoryAffairDao();
			databaseList = regulatoryAffairDao.generateMapping();
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return databaseList;
	}
	
	public String generateOracleItemMapping() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String databaseList = null;
		try {
			RegulatoryAffairDao regulatoryAffairDao = new RegulatoryAffairDao();
			databaseList = regulatoryAffairDao.generateOracleItemMapping();
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return databaseList;
	}
}
