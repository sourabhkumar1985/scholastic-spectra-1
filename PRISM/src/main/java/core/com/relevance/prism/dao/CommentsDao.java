package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import com.relevance.prism.data.Comment;
import com.relevance.prism.data.CommentLog;
import com.relevance.prism.util.PrismHandler;

public class CommentsDao extends BaseDao {
	public String insertComment(Comment comment) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String response = null;
		String insertQuery = null;
		Boolean result = false;
		try {
			con = dbUtil.getPostGresConnection();
			insertQuery = "INSERT INTO comments( profile_name, commentpanel_id, comment_content, user_name, user_image, filter_query, log_time)VALUES ( ?, ?, ?, ?, ?, ?,now());"; 
			ps = (PreparedStatement) con.prepareStatement(insertQuery.toString());
			ps.setString(1, comment.getProfileName());
			ps.setString(2, comment.getCommentPanelId());
			ps.setString(3, comment.getComment());
			ps.setString(4, comment.getUserName());
			ps.setString(5, comment.getUserImage());
			ps.setString(6, comment.getFilterQuery());
			ps.executeUpdate();  
			response = "{\"message\":\"successfully inserted comment in table\"}";  
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally{
			PrismHandler.handleFinally(con, rs, ps);
		}
		return response;
	}
	
	public List<Comment> getComments(Comment comment) throws Exception {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuilder query = null;
		List<Comment> commentList = null;
		try {
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			query = new StringBuilder("SELECT id, profile_name, commentpanel_id, comment_content, user_name, log_time,user_image FROM comments where 1=1 ");
			if(comment.getCommentPanelId() != null){
				query.append(" and commentpanel_id = '").append(comment.getCommentPanelId()).append("'");
			}
			query.append(" and profile_name = '").append(comment.getProfileName());
			query.append("' and filter_query = '").append(comment.getFilterQuery());
			query.append("' order by log_time;");
			rs = stmt.executeQuery(query.toString());
			commentList = new ArrayList<>();
			while (rs.next()) {
				comment = new Comment();
				comment.setId(rs.getInt("id"));
				comment.setProfileName(rs.getString("profile_name"));
				comment.setCommentPanelId(rs.getString("commentpanel_id"));
				comment.setUserName(rs.getString("user_name"));
				comment.setComment(rs.getString("comment_content"));
				comment.setCommentTimeStamp(rs.getString("log_time"));
				comment.setUserImage(rs.getString("user_image"));
				commentList.add(comment);
			}			
		} catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally{
			PrismHandler.handleFinally(con, rs, stmt);
		}		
		return commentList;
	}
	
	public List<CommentLog> getLastRead(Comment comment,boolean setLastRead) throws Exception {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		StringBuilder query = null;
		String lastRead = null;
		List<CommentLog> commentLogList = null;
		try {
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			
			query = new StringBuilder("SELECT last_read,commentpanel_id  FROM comment_user_read_logs where user_name='");  
			query.append(comment.getUserName()).append("'");
			if(comment.getCommentPanelId() != null){
				query.append(" and commentpanel_id = '").append(comment.getCommentPanelId()).append("'");
			}
			query.append(" and profile_name = '").append(comment.getProfileName()).append("'");
			query.append(" and filter_query='").append(comment.getFilterQuery()).append("';");
			rs = stmt.executeQuery(query.toString());
			commentLogList = new ArrayList<>();
			while (rs.next()) {
				CommentLog commentLog = new CommentLog();
				lastRead = rs.getString("last_read");
				commentLog.setLastread(rs.getString("last_read"));
				commentLog.setCommentPanelId(rs.getString("commentpanel_id"));                    
				commentLogList.add(commentLog);
			}
			if(setLastRead) {
				if(lastRead != null) {
					query = new StringBuilder("UPDATE comment_user_read_logs   SET last_read=now() where user_name='");  
					query.append(comment.getUserName()).append("' and commentpanel_id='").append(comment.getCommentPanelId());
					query.append("' and profile_name = '").append(comment.getProfileName());
					query.append("' and filter_query = '").append(comment.getFilterQuery());
					query.append("';");
					stmt.executeUpdate(query.toString());
				}else {
					query = new StringBuilder("INSERT INTO comment_user_read_logs( user_name, commentpanel_id, profile_name, filter_query, last_read)    VALUES ('");  
					query.append(comment.getUserName()).append("','").append(comment.getCommentPanelId());
					query.append("','").append(comment.getProfileName());
					query.append("','").append(comment.getFilterQuery());
					query.append("',now());");
					stmt.executeUpdate(query.toString());	
				}
			}
		}catch(Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally{
			PrismHandler.handleFinally(con, rs, stmt);
		}	
		return commentLogList;
	}
}
