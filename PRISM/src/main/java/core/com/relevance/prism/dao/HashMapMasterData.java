package com.relevance.prism.dao;

import java.util.HashMap;

public class HashMapMasterData {
	
	private static HashMap<String, String> hm1=new HashMap<String,String>();
	private static HashMap<String, String> hm2=new HashMap<String,String>();
	private static HashMap<String, String> hm3=new HashMap<String,String>();
	//private static HashMap<String, String> hm4=new HashMap<String,String>();
	
	
	public static void initiatlize(){		
		
		//columns from solrmaster_label table
		hm1.put("catalog_no", "item_number");
		hm1.put("brand", "brand_type");
		hm1.put("description", "title");
		hm1.put("major_variant", "major_variant");
		hm1.put("shape", "shape");
		hm1.put("composition", "composition");
		/*hm1.put("", "instrument");
		hm1.put("", "size_qual");
		hm1.put("", "size");*/
		hm1.put("country_of_origin","country_of_origin");
		
		//columns from spreadsheet_datasource table	
		hm3.put("catalog_no", "catalog");
		hm3.put("brand","brand");
		hm3.put("specialty_use", "specialty_use");
		hm3.put("description", "instrument_description");
		hm3.put("physician", "physician_developer_name");
		hm3.put("type", "instr_type");
		hm3.put("shape","shape");
		hm3.put("blade","blade");
		hm3.put("grade","grade");
		hm3.put("finish","finish");
		hm3.put("composition","composition");
		hm3.put("disposable_reusable", "usage");
		hm3.put("overall_length_meter", "overall_length");
		hm3.put("working_length","working_length");
		hm3.put("tip_jaw_width_1","tip_width_1");
		hm3.put("tip_jaw_length_1","tip_length_1");
		hm3.put("tip_width_2","tip_width_2");
		hm3.put("tip_length_2","tip_length_2");
		hm3.put("angle","angle");
		hm3.put("spread_open","spread_open");
		hm3.put("bite","bite");
		hm3.put("ga_fr_size","ga_fr_size");
		hm3.put("size_style","size_style");
		hm3.put("jaws_blades","jaws_blades");

		//columns from solrmaster_catalog table
		hm2.put("catalog_no", "item_number");
		hm2.put("brand","product_brand");
		hm2.put("specialty_use", "category");
		hm2.put("description", "title");
		hm2.put("first_level_folder", "franchise");
		hm2.put("category", "category");
	}
	
	public static HashMap<String, String> getDataFromLabelTable()
	{
		return hm1;
	}
	
	public static HashMap<String, String> getDataFromCatalogTable()
	{
		return hm2;
	}
	
	public static HashMap<String, String> getDataFromSpreadsheetTable()
	{
		return hm3;
	}
	
	/*public static HashMap<String, String> getDataFromItemTable()
	{
		return hm4;
	}*/
	
}
