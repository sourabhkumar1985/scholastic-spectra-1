package com.relevance.prism.rest;

import java.io.File;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.util.ZipUtil;

@Path("/download")
public class DownloadResource extends BaseResource{

	@POST
    @Path("/downloadzip")
    @Produces("application/zip")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)	
    public Response download(@FormParam("urllist") String urllist) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
        //System.out.println("Start");
        ResponseBuilder response = null ;
        try {
        	String [] urls = urllist.split(",");
        	String filePath = ZipUtil.createZipFile(urls);
        	File file = new File(filePath);
			response = Response.ok((Object) file, "application/zip");
            response.header("Content-Disposition", "attachment; filename=" + file.getName());
            response.header("Set-Cookie", "fileDownload=true; path=/");
            return response.build();
        }catch(Exception ex){
			PrismHandler.handleException(ex, false);
		}
        PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
        //System.out.println("End");
        return response.build();
    }	
}
