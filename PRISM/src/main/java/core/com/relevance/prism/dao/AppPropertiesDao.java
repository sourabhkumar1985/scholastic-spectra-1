package com.relevance.prism.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.relevance.prism.data.AppProperties;
import com.relevance.prism.data.AppProperty;
import com.relevance.prism.data.Category;
import com.relevance.prism.data.CharacterWordItem;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.FormData;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class AppPropertiesDao extends BaseDao {

	public AppProperties getProperties() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperties appProperties = null;
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String selectQuery = "select name, value,type,remarks from app_properties";
			rs = stmt.executeQuery(selectQuery);

			appProperties = new AppProperties();
			appProperties.setProp(new HashMap<String, AppProperty>());

			if (rs != null) {
				while (rs.next()) {
					AppProperty pi = new AppProperty();
					pi.setKey(rs.getString("name"));
					pi.setValue(rs.getString("value"));
					pi.setType(rs.getString("type"));
					pi.setRemarks(rs.getString("remarks"));
					appProperties.getProp().put(pi.getKey(), pi);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return appProperties;
	}

	public Map<String, FormData> getFormDataMap() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, FormData> formDataMap = new HashMap<>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String dataObfuscationSelectQuery = "select property_name, obfuscation_status, deobfuscation_status, obf_type, remarks from form_data";
			rs = stmt.executeQuery(dataObfuscationSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					String propertyName = rs.getString("property_name");
					int obfuscationStatus = rs.getInt("obfuscation_status");
					int deobfuscationStatus = rs.getInt("deobfuscation_status");
					int obfType = rs.getInt("obf_type");
					String remarks = rs.getString("remarks");
					FormData formData = new FormData();
					formData.setName(propertyName);
					formData.setObfuscate(obfuscationStatus > 0);
					formData.setDeObfuscate(deobfuscationStatus > 0);
					formData.setObfType(obfType);
					formData.setRemarks(remarks);
					formDataMap.put(propertyName, formData);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return formDataMap;
	}

	public Map<String, CharacterWordItem> getObfuscationWordMap(boolean reverse) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, CharacterWordItem> wordMap = new HashMap<>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String dataObfuscationSelectQuery = "select fromword, toword from obf_words";
			rs = stmt.executeQuery(dataObfuscationSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					String from = rs.getString("fromword");
					String to = rs.getString("toword");
					CharacterWordItem wi = new CharacterWordItem();
					wi.setWordFrom(from);
					wi.setWordTo(to);
					if (reverse)
						wordMap.put(to, wi);
					else
						wordMap.put(from, wi);

				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return wordMap;
	}

	public Map<Character, CharacterWordItem> getObfuscationCharMap(boolean reverse)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<Character, CharacterWordItem> charMap = new HashMap<>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String dataObfuscationSelectQuery = "select fromchar, tochar from obf_characters";
			rs = stmt.executeQuery(dataObfuscationSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					String from = rs.getString("fromchar");
					String to = rs.getString("tochar");
					CharacterWordItem ci = new CharacterWordItem();
					ci.setCharacterFrom(from.charAt(0));
					ci.setCharacterTo(to.charAt(0));
					if (reverse)
						charMap.put(ci.getCharacterTo(), ci);
					else
						charMap.put(ci.getCharacterFrom(), ci);

				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return charMap;
	}

	public String insertFormData(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		FormData param = null;
		Connection connection = null;
		Statement stmt = null;
		int obfuscate = 0;
		int deobfuscate = 0;

		if (obj != null) {
			param = (FormData) obj[0];
			Log.Info("Inserting  Values for Form Data Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				if (param.isObfuscate()) {
					obfuscate = 1;
				} else {
					obfuscate = 0;
				}

				if (param.isDeObfuscate()) {
					deobfuscate = 1;
				} else {
					deobfuscate = 0;
				}

				String inertFormDataQuery = "insert into form_data (property_name, obfuscation_status, deobfuscation_status, remarks, obf_type) values("
						+ "'" + param.getName() + "'," + obfuscate + "," + deobfuscate + ",'"
						+ param.getRemarks() + "'," + param.getObfType() + ")";
				stmt.executeUpdate(inertFormDataQuery);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted Form Data in DB.\"}";
	}

	public String insertObfWords(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CharacterWordItem param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (CharacterWordItem) obj[0];
			Log.Info("Inserting  Values for Obf Words Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String inertFormDataQuery = "insert into obf_words (fromword, toword) values(" + "'"
						+ param.getWordFrom() + "'," + "'" + param.getWordTo() + "'" + ")";
				stmt.executeUpdate(inertFormDataQuery);

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted Obfuscation Words Data in DB.\"}";
	}

	public String insertAppProps(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperty param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (AppProperty) obj[0];
			Log.Info("Inserting  Values for app properties Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String inertFormDataQuery = "insert into app_properties (name,value,remarks,type) values("
						+ "'" + param.getName() + "'," + "'" + param.getValue() + "'," + "'"
						+ param.getRemarks() + "','" + param.getType() + "'" + ")";
				stmt.executeUpdate(inertFormDataQuery);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted App Properties in DB.\"}";
	}

	public String updateFormData(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		FormData param = null;
		Connection connection = null;
		int obfuscate = 0;
		int deobfuscate = 0;
		Statement stmt = null;
		if (obj != null) {
			param = (FormData) obj[0];
			Log.Info("Updating  Values for Form Data Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				if (param.isObfuscate()) {
					obfuscate = 1;
				} else {
					obfuscate = 0;
				}

				if (param.isDeObfuscate()) {
					deobfuscate = 1;
				} else {
					deobfuscate = 0;
				}

				String updateQueryForObfucate = "update form_data set obfuscation_status = "
						+ obfuscate + " where property_name = '" + param.getName() + "'";
				String updateQueryForDeObfuscate = "update form_data set deobfuscation_status = "
						+ deobfuscate + " where property_name = '" + param.getName() + "'";
				String updateQueryForRemarks = "update form_data set remarks = '"
						+ param.getRemarks() + "' where property_name = '" + param.getName() + "'";
				String updateQueryForObfType = "update form_data set obf_type = "
						+ param.getObfType() + " where property_name = '" + param.getName() + "'";

				stmt.executeUpdate(updateQueryForObfucate);
				stmt.executeUpdate(updateQueryForDeObfuscate);
				stmt.executeUpdate(updateQueryForRemarks);
				stmt.executeUpdate(updateQueryForObfType);

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully updated Form Data in DB.\"}";
	}

	public String updateObfWords(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CharacterWordItem param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (CharacterWordItem) obj[0];
			Log.Info("Updating  Values for Obf Words Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String updateQueryForWord = "update obf_words set toword = '" + param.getWordTo()
						+ "' where fromword = '" + param.getWordFrom() + "'";

				stmt.executeUpdate(updateQueryForWord);
				return "{\"message\":\"successfully Updated Obfucation Word Data in DB.\"}";
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully Updated Obfucation Word Data in DB.\"}";
	}

	public String updateAppProps(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperty param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (AppProperty) obj[0];
			Log.Info("Updating  Values for App Properties Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String updateQueryForValue = "update app_properties set value = '"
						+ param.getValue() + "', remarks='" + param.getRemarks() + "', type='"
						+ param.getType() + "' where name = '" + param.getName() + "'";

				stmt.executeUpdate(updateQueryForValue);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully updated App Properties in table\"}";
	}

	public String deleteFormData(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		FormData param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (FormData) obj[0];
			Log.Info("Inserting  Values for Form Data Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String deleteQueryForFormData = "delete from form_data where property_name = '"
						+ param.getName() + "'";
				stmt.executeUpdate(deleteQueryForFormData);

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully deleted Form Data in DB.\"}";
	}

	public String deleteObfWords(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		CharacterWordItem param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (CharacterWordItem) obj[0];
			Log.Info("Deleting Obf Words Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String deleteQueryForWord = "delete from obf_words where fromword = '"
						+ param.getWordFrom() + "'";
				stmt.executeUpdate(deleteQueryForWord);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully deleted Obf Word in DB.\"}";
	}

	public String deleteAppProps(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AppProperty param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (AppProperty) obj[0];
			Log.Info("Deleting app properties Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String deleteAppPropertiesQuery = "delete from app_properties where name = '"
						+ param.getName() + "'";
				stmt.executeUpdate(deleteAppPropertiesQuery);
				return "{\"message\":\"successfully Deleted App Properties from DB.\"}";
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully Deleted App Properties from DB.\"}";
	}

	public List<Category> getCategoriesList(String rules) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<Category> categoryList = new ArrayList<>();
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String categoriesSelectQuery = null;
			if(rules != null && rules.equalsIgnoreCase(E2emfConstants.ALL)){
				categoriesSelectQuery = "select a.category_id as category_id, a.category as category, a.keywords as keywords, "
						+ "a.parent_id as parent_id, b.category as parent, a.rule as rule,a.condition as condition, a.user_override as user_override, "
						+ "a.enabled as enabled from sf_categories as a "
						+ "left outer join sf_categories as b on b.category_id = a.parent_id;"; // where a.category !='Unclassified Instruments'
			}
			else if(rules != null && rules.length() > 0){
				categoriesSelectQuery = "select a.category_id as category_id, a.category as category, a.keywords as keywords, "
						+ "a.parent_id as parent_id, b.category as parent, a.rule as rule,a.condition as condition, a.user_override as user_override, "
						+ "a.enabled as enabled from sf_categories as a "
					+ "left outer join sf_categories as b on b.category_id = a.parent_id where a.category !='Unclassified Instruments' and a.rule in ('" + rules +"')";
			}			
			rs = stmt.executeQuery(categoriesSelectQuery);
			if (rs != null) {
				while (rs.next()) {
					String categoryId = rs.getString("category_id");
					String categoryName = rs.getString("category");
					String keywords = rs.getString("keywords");
					String parentId = rs.getString("parent_id");
					String parent = rs.getString("parent");
					String rule = rs.getString("rule");
					String condition = rs.getString("condition");
					Boolean isOverride = rs.getBoolean("user_override");
					Boolean isEnabled = rs.getBoolean("enabled");
					Category category = new Category();
					category.setCategoryId(categoryId);
					category.setCategory(categoryName);
					category.setKeywords(keywords);
					category.setParentId(parentId);
					category.setParent(parent);
					category.setRule(rule);
					category.setCondition(condition);
					category.setIsEnabled(isEnabled);
					category.setIsOverride(isOverride);
					categoryList.add(category);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return categoryList;
	}

	public String insertCategory(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Category param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (Category) obj[0];
			Log.Info("Inserting  Values for Category Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String inertCategoryQuery = "insert into sf_categories (parent, parent_id, category, keywords) values("
						+ " '" + param.getParent() + "','" + param.getParentId() + "','"
						+ param.getCategory() + "','" + param.getKeywords() + "')";
				stmt.executeUpdate(inertCategoryQuery);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted Category in DB.\"}";
	}

	public String updateCategory(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Category param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (Category) obj[0];
			Log.Info("Updating  Values for Category Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String pid = param.getParentId();
				if (pid != null) {
					String updateQueryForParent = "update sf_categories set parent = '"
							+ param.getParent() + "', parent_id = '" + param.getParentId()
							+ "', category = '" + param.getCategory() + "', keywords = '"
							+ param.getKeywords() + "' where category_id = '"
							+ param.getCategoryId() + "'";

					stmt.executeUpdate(updateQueryForParent);
				} else {
					String updateQueryForCategory = "update sf_categories set category = '"
							+ param.getCategory() + "', keywords = '" + param.getKeywords()
							+ "' where category_id = '" + param.getCategoryId() + "'";

					stmt.executeUpdate(updateQueryForCategory);
				}

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully updated Category in DB.\"}";
	}

	public String deleteCategory(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Category param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (Category) obj[0];
			Log.Info("Inserting  Values for Category Object : " + param);
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();

				String deleteQueryForCategory = "delete from sf_categories where category_id = "
						+ param.getCategoryId();

				stmt.executeUpdate(deleteQueryForCategory);

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully deleted Category in DB.\"}";
	}
	
	public String getPropertyName() throws Exception
    {
		String key = null;
		Connection con = null;
		ResultSet rs = null;
		Statement stmt = null;
		//dbUtil dbUtil = new DbUtil();
		try {
			con = dbUtil.getPostGresConnection();		
		    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);		
		    String sql = "select * from app_properties where name = 'Licence_key'";
				
			rs = stmt.executeQuery(sql);
			
			while(rs.next()){
				key = rs.getString("value");
			}
		} catch (AppException e1) {
			PrismHandler.handleException(e1, true);
		}	
		
		finally{
			PrismHandler.handleFinally(con, null, stmt);
		}
		
		return key;
        
    }

}
