package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;

public class GoodsHistoryList {

	public List<GoodsHistory> getAggregatedGoodsHistoryList() {
		return aggregatedGoodsHistoryList;
	}

	public void setAggregatedGoodsHistoryList(
			List<GoodsHistory> aggregatedGoodsHistoryList) {
		this.aggregatedGoodsHistoryList = aggregatedGoodsHistoryList;
	}

	private List<GoodsHistory> aggregatedGoodsHistoryList = new ArrayList<GoodsHistory>();
}
