package com.relevance.prism.data;

import java.util.List;

public class CrystalAgilePart {

	private String materialNumber;
	private String parent;
	private String plant;	
	private String materialName;			
	private String parentName;
	private String materialPlant;
	private String parentPlant;
	private List<CrystalAgilePart> children;

	
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public List<CrystalAgilePart> getChildren() {
		return children;
	}
	public void setChildren(List<CrystalAgilePart> children) {
		this.children = children;
	}
	
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getMaterialPlant() {
		return materialPlant;
	}
	public void setMaterialPlant(String materialPlant) {
		this.materialPlant = materialPlant;
	}
	public String getParentPlant() {
		return parentPlant;
	}
	public void setParentPlant(String parentPlant) {
		this.parentPlant = parentPlant;
	}
	
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getMaterialNumber();
	}
}
