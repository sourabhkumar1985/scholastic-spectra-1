package com.relevance.prism.service;

import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.util.SpectraRuleEngine;

public class SpectraRulesService extends BaseService{

	public String runRules(String [] idsList, String project)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		SpectraRuleEngine spectraRuleEngine = new SpectraRuleEngine();
		String response = spectraRuleEngine.fireRules(idsList, project);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
