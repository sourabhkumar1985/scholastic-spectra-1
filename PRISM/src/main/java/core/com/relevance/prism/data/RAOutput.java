package com.relevance.prism.data;

public class RAOutput {
	private int id;
	private int dCount;
	private int kCount;
	private String gudidDnumber;
	private String gudidKnumber;
	private String gudidProductCode;
	private String sheetDnumber;
	private String sheetKnumber;
	private String sheetProductCode;
	private String dnumberMatchStatus;
	private String knumberMatchStatus;
	private String productCodeMatchStatus;
	private String sheetKnumberFilledInternally;
	private String sheetOriginalApplicant;
	private String sheetReceviedDate;
	private String sheetVlearedDate;
	private String sheetDeviceClass;
	private String sheetdeviceListingStatus;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getdCount() {
		return dCount;
	}

	public void setdCount(int dCount) {
		this.dCount = dCount;
	}

	public int getkCount() {
		return kCount;
	}

	public void setkCount(int kCount) {
		this.kCount = kCount;
	}

	public String getGudidDnumber() {
		return gudidDnumber;
	}

	public void setGudidDnumber(String gudidDnumber) {
		this.gudidDnumber = gudidDnumber;
	}

	public String getGudidKnumber() {
		return gudidKnumber;
	}

	public void setGudidKnumber(String gudidKnumber) {
		this.gudidKnumber = gudidKnumber;
	}

	public String getGudidProductCode() {
		return gudidProductCode;
	}

	public void setGudidProductCode(String gudidProductCode) {
		this.gudidProductCode = gudidProductCode;
	}

	public String getSheetDnumber() {
		return sheetDnumber;
	}

	public void setSheetDnumber(String sheetDnumber) {
		this.sheetDnumber = sheetDnumber;
	}

	public String getSheetKnumber() {
		return sheetKnumber;
	}

	public void setSheetKnumber(String sheetKnumber) {
		this.sheetKnumber = sheetKnumber;
	}

	public String getSheetProductCode() {
		return sheetProductCode;
	}

	public void setSheetProductCode(String sheetProductCode) {
		this.sheetProductCode = sheetProductCode;
	}

	public String getDnumberMatchStatus() {
		return dnumberMatchStatus;
	}

	public void setDnumberMatchStatus(String dnumberMatchStatus) {
		this.dnumberMatchStatus = dnumberMatchStatus;
	}

	public String getKnumberMatchStatus() {
		return knumberMatchStatus;
	}

	public void setKnumberMatchStatus(String knumberMatchStatus) {
		this.knumberMatchStatus = knumberMatchStatus;
	}

	public String getProductCodeMatchStatus() {
		return productCodeMatchStatus;
	}

	public void setProductCodeMatchStatus(String productCodeMatchStatus) {
		this.productCodeMatchStatus = productCodeMatchStatus;
	}

	public String getSheetKnumberFilledInternally() {
		return sheetKnumberFilledInternally;
	}

	public void setSheetKnumberFilledInternally(String sheetKnumberFilledInternally) {
		this.sheetKnumberFilledInternally = sheetKnumberFilledInternally;
	}

	public String getSheetOriginalApplicant() {
		return sheetOriginalApplicant;
	}

	public void setSheetOriginalApplicant(String sheetOriginalApplicant) {
		this.sheetOriginalApplicant = sheetOriginalApplicant;
	}

	public String getSheetReceviedDate() {
		return sheetReceviedDate;
	}

	public void setSheetReceviedDate(String sheetReceviedDate) {
		this.sheetReceviedDate = sheetReceviedDate;
	}

	public String getSheetVlearedDate() {
		return sheetVlearedDate;
	}

	public void setSheetVlearedDate(String sheetVlearedDate) {
		this.sheetVlearedDate = sheetVlearedDate;
	}

	public String getSheetDeviceClass() {
		return sheetDeviceClass;
	}

	public void setSheetDeviceClass(String sheetDeviceClass) {
		this.sheetDeviceClass = sheetDeviceClass;
	}

	public String getSheetdeviceListingStatus() {
		return sheetdeviceListingStatus;
	}

	public void setSheetdeviceListingStatus(String sheetdeviceListingStatus) {
		this.sheetdeviceListingStatus = sheetdeviceListingStatus;
	}
}
