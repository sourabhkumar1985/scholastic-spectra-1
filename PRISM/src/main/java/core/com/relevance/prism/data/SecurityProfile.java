package com.relevance.prism.data;

public class SecurityProfile {

	private String source;
	private String sourceRole;
	private String sourceTcodesCount;
	private String sourceTcodes;
	private String target;
	private String targetRole;
	private String targetTcodesCount;
	private String targetTcodes;
	private String matchCount;
	private String matchedTcodes;
	private String rankPercentage;
	private String rank;
	private String type;
	private String noMatchCountSourceToTarget;
	private String noMatchTcodesSourceToTarget;
	private String noMatchCountTargetToSource;
	private String noMatchTcodesTargetToSource;
	private String percentageMatchSourceToTarget;
	private String percentageMatchTargetToSource;
	private String roleType;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSourceRole() {
		return sourceRole;
	}
	public void setSourceRole(String sourceRole) {
		this.sourceRole = sourceRole;
	}
	public String getSourceTcodesCount() {
		return sourceTcodesCount;
	}
	public void setSourceTcodesCount(String sourceTcodesCount) {
		this.sourceTcodesCount = sourceTcodesCount;
	}
	public String getSourceTcodes() {
		return sourceTcodes;
	}
	public void setSourceTcodes(String sourceTcodes) {
		this.sourceTcodes = sourceTcodes;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getTargetRole() {
		return targetRole;
	}
	public void setTargetRole(String targetRole) {
		this.targetRole = targetRole;
	}
	public String getTargetTcodesCount() {
		return targetTcodesCount;
	}
	public void setTargetTcodesCount(String targetTcodesCount) {
		this.targetTcodesCount = targetTcodesCount;
	}
	public String getTargetTcodes() {
		return targetTcodes;
	}
	public void setTargetTcodes(String targetTcodes) {
		this.targetTcodes = targetTcodes;
	}
	public String getMatchCount() {
		return matchCount;
	}
	public void setMatchCount(String matchCount) {
		this.matchCount = matchCount;
	}
	public String getMatchedTcodes() {
		return matchedTcodes;
	}
	public void setMatchedTcodes(String matchedTcodes) {
		this.matchedTcodes = matchedTcodes;
	}
	public String getRankPercentage() {
		return rankPercentage;
	}
	public void setRankPercentage(String rankPercentage) {
		this.rankPercentage = rankPercentage;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getNoMatchCountSourceToTarget() {
		return noMatchCountSourceToTarget;
	}
	public void setNoMatchCountSourceToTarget(String noMatchCountSourceToTarget) {
		this.noMatchCountSourceToTarget = noMatchCountSourceToTarget;
	}
	public String getNoMatchTcodesSourceToTarget() {
		return noMatchTcodesSourceToTarget;
	}
	public void setNoMatchTcodesSourceToTarget(String noMatchTcodesSourceToTarget) {
		this.noMatchTcodesSourceToTarget = noMatchTcodesSourceToTarget;
	}
	public String getNoMatchCountTargetToSource() {
		return noMatchCountTargetToSource;
	}
	public void setNoMatchCountTargetToSource(String noMatchCountTargetToSource) {
		this.noMatchCountTargetToSource = noMatchCountTargetToSource;
	}
	public String getNoMatchTcodesTargetToSource() {
		return noMatchTcodesTargetToSource;
	}
	public void setNoMatchTcodesTargetToSource(String noMatchTcodesTargetToSource) {
		this.noMatchTcodesTargetToSource = noMatchTcodesTargetToSource;
	}
	public String getPercentageMatchSourceToTarget() {
		return percentageMatchSourceToTarget;
	}
	public void setPercentageMatchSourceToTarget(
			String percentageMatchSourceToTarget) {
		this.percentageMatchSourceToTarget = percentageMatchSourceToTarget;
	}
	public String getPercentageMatchTargetToSource() {
		return percentageMatchTargetToSource;
	}
	public void setPercentageMatchTargetToSource(
			String percentageMatchTargetToSource) {
		this.percentageMatchTargetToSource = percentageMatchTargetToSource;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	
}
