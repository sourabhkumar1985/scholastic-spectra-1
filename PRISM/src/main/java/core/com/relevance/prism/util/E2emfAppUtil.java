package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.relevance.prism.data.AppProperties;
import com.relevance.prism.data.CharacterWordItem;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.Shortlist;
import com.relevance.prism.rest.BaseResource;

/**
 * E2emf Application Utility Class
 * 
 */
public class E2emfAppUtil  extends BaseResource {

	private static AppProperties appProperties;

	private static final String VERSION_FILE = "Build.txt";

	private static Map<String, FormData> formDataMap;

	private static boolean isAppUtilInitialized = false;

	private static boolean isVersionInitialized = false;

	private static Properties appVersion = new Properties();

	private static Map<String, CharacterWordItem> wordMap;

	private static Map<String, CharacterWordItem> revWordMap;

	private static Map<Character, CharacterWordItem> charMap;

	private static Map<Character, CharacterWordItem> revCharMap;

	public static Map<Character, CharacterWordItem> getCharMap() {
		return charMap;
	}

	public static void setCharMap(Map<Character, CharacterWordItem> charMap) {
		E2emfAppUtil.charMap = charMap;
	}

	public static Map<Character, CharacterWordItem> getRevCharMap() {
		return revCharMap;
	}

	public static void setRevCharMap(
			Map<Character, CharacterWordItem> revCharMap) {
		E2emfAppUtil.revCharMap = revCharMap;
	}

	public static AppProperties getAppProperties() {
		return appProperties;
	}

	public static Map<String, CharacterWordItem> getWordMap() {
		return wordMap;
	}

	public static void setWordMap(Map<String, CharacterWordItem> wordMap) {
		E2emfAppUtil.wordMap = wordMap;
	}

	public static Map<String, CharacterWordItem> getRevWordMap() {
		return revWordMap;
	}

	public static void setRevWordMap(Map<String, CharacterWordItem> revWordMap) {
		E2emfAppUtil.revWordMap = revWordMap;
	}

	public static Map<String, FormData> getFormDataMap() {
		return formDataMap;
	}

	public static void setFormDataMap(Map<String, FormData> formDataMap) {
		E2emfAppUtil.formDataMap = formDataMap;
	}

	private E2emfAppUtil() {

	}

	private static void loadAppVersion() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
			appVersion.load(E2emfAppUtil.class.getClassLoader()
					.getResourceAsStream(VERSION_FILE));
			// appProps.load(this.getClass().getClassLoader().getSystemResourceAsStream(propFile));
			Log.Info("Loaded App Version Successfully... ");
			isVersionInitialized = true;

		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());


	}

	public static void reinitialize(AppProperties prop) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
	
		Log.Info("Reinitializing Application Properties");
		appProperties = prop;
		Log.Info("Application Properties Reinitialized");
		} catch (Exception ex) {
			PrismHandler.handleException(ex,true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	public static void initialize(AppProperties prop) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {
		if (isAppUtilInitialized) {
			Log.Info("Already initialized Application Properties");
			return;
		}

		Log.Info("Initializing Application Properties");

		if (!Log.isLoggerInitialized)
			Log.initialize();

		if (!isVersionInitialized)
			loadAppVersion();


			appProperties = prop;
			isAppUtilInitialized = true;
			Log.Info("Loaded AppProperties sucessfully... ");

		} catch (Exception ex) {
			PrismHandler.handleException(ex,true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	public static String getAppProperty(String key){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		if (key == null)
			response = null;
		else if (appProperties.getProp().get(key) == null)
			response = null;
		else
			response = appProperties.getProp().get(key).getValue();
		} catch (Exception ex) {
			response =PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String getAppVersionProperty(String key) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			response = appVersion.getProperty(key, "");
		} catch (Exception ex) {
			response=PrismHandler.handleException(ex,true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String getLookUpNode(String[] splitStr)throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String selectedNode = null;
		String response = null;
		try {
		
		// String[] splitStr = varx.split("\\s");
		if (splitStr[0].equalsIgnoreCase("0")) {
			selectedNode = "bukrs";
		} else if (splitStr[0].equalsIgnoreCase("1")) {
			selectedNode = "werks";
		} else if (splitStr[0].equalsIgnoreCase("2")) {
			selectedNode = "purorggrp";
		} else if (splitStr[0].equalsIgnoreCase("3")) {
			selectedNode = "purdoctype";
		} else if (splitStr[0].equalsIgnoreCase("4")) {
			selectedNode = "paymentterm";
		}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		response =  selectedNode + "," + splitStr[1] + "," + splitStr[2];
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;

	}

	public static String getLookUpNodeConnector(String[] splitStrFirstSet,
			String[] splitStrSecondSet) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		String selectedNode = null;
		String selectedNode1 = null;

		try {
	
		
		// String[] splitStr = varX.split(",");
		// String firstSet = splitStr[0];
		// String secondSet = splitStr[1];

		// String[] splitStrFirstSet = firstSet.split("\\s");

		if (splitStrFirstSet[0].equalsIgnoreCase("0")) {
			selectedNode = "bukrs";
		} else if (splitStrFirstSet[0].equalsIgnoreCase("1")) {
			selectedNode = "werks";
		} else if (splitStrFirstSet[0].equalsIgnoreCase("2")) {
			selectedNode = "purorggrp";
		} else if (splitStrFirstSet[0].equalsIgnoreCase("3")) {
			selectedNode = "purdoctype";
		} else if (splitStrFirstSet[0].equalsIgnoreCase("4")) {
			selectedNode = "paymentterm";
		}

		// String[] splitStrSecondSet = secondSet.split("\\s");

		if (splitStrSecondSet[0].equalsIgnoreCase("0")) {
			selectedNode1 = "bukrs";
		} else if (splitStrSecondSet[0].equalsIgnoreCase("1")) {
			selectedNode1 = "werks";
		} else if (splitStrSecondSet[0].equalsIgnoreCase("2")) {
			selectedNode1 = "purorggrp";
		} else if (splitStrSecondSet[0].equalsIgnoreCase("3")) {
			selectedNode1 = "purdoctype";
		} else if (splitStrSecondSet[0].equalsIgnoreCase("4")) {
			selectedNode1 = "paymentterm";
		}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		response =  selectedNode + "@" + splitStrFirstSet[1] + "," + selectedNode1
				+ "@" + splitStrSecondSet[1] + "," + splitStrSecondSet[2];
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String getKeyValue(String key, String value) throws Exception{
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		String q = "\"";

		StringBuilder sb = new StringBuilder();

		sb.append(q);
		sb.append(key);
		sb.append(q);
		sb.append(":");
		sb.append(q);
		sb.append(value);
		sb.append(q);

		response =  sb.toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String removeSpecialChar(String inStr, char specialChar)throws Exception{
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		Log.Info("Removing special character " + specialChar + " from string "
				+ inStr);
		StringBuilder strBuilder = new StringBuilder();
		char[] rmString = inStr.toCharArray();
		for (int i = 0; i < rmString.length; i++) {
			if (rmString[i] == specialChar) {

			} else {
				strBuilder.append(rmString[i]);
			}
		}
		response =  strBuilder.toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static long getTotalElapsedTime(long startTime, long endTime){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		long elapsedTimeinSeconds = 0;
		try {
		elapsedTimeinSeconds = (endTime - startTime) / 1000;
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return elapsedTimeinSeconds;

	}

	public static String getDateFromJulianDateFormat(String julianDate) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		// String j = "113029";
		Date date = null;
		try {
			date = new SimpleDateFormat("Myydd").parse(julianDate);
		} catch (ParseException e) {
			Log.Error("Exception in parsing date:" + e.getMessage());
		}

		if (date != null) {
			response = new SimpleDateFormat("dd/MM/yyyy").format(date);
		}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String getSimpleDateFormat(String dateString) throws Exception{
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		Date date = null;
		try {
			date = new SimpleDateFormat("mm/dd/yyyy").parse(dateString);
		} catch (ParseException e) {
			Log.Error("Exception in parsing date:" + e.getMessage());
		}

		response = new SimpleDateFormat("mm/dd/yyyy").format(date);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String getSimpleDateFormatYYYYMM(String dateString) throws Exception{

		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		Date date = null;
		try {
			date = new SimpleDateFormat("mm/dd/yyyy").parse(dateString);
		} catch (ParseException e) {
			Log.Error("Exception in parsing date:" + e.getMessage());
		}

		response = new SimpleDateFormat("MM").format(date) + "/"
				+ new SimpleDateFormat("yyyy").format(date);
		// System.out.println(convertedDate);

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex,true);
		}
		
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static int getJulianDateFromDate(String regularDate) throws Exception{

		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		int resultJulian = 0;
		try {

		
		if (regularDate.length() > 0) {
			/* Days of month */
			int[] monthValues = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,
					31 };

			String dayS, monthS, yearS;
			dayS = regularDate.substring(0, 2);
			monthS = regularDate.substring(3, 5);
			yearS = regularDate.substring(6, 10);

			/* Convert to Integer */
			int day = Integer.valueOf(dayS);
			int month = Integer.valueOf(monthS);
			int year = Integer.valueOf(yearS);

			// Leap year check
			if (year % 4 == 0) {
				monthValues[1] = 29;
			}
			// Start building Julian date
			String julianDate = "1";
			// last two digit of year: 2012 ==> 12
			julianDate += yearS.substring(2, 4);

			int julianDays = 0;
			for (int i = 0; i < month - 1; i++) {
				julianDays += monthValues[i];
			}
			julianDays += day;

			if (String.valueOf(julianDays).length() < 2) {
				julianDate += "00";
			}
			if (String.valueOf(julianDays).length() < 3) {
				julianDate += "0";
			}

			julianDate += String.valueOf(julianDays);
			resultJulian = Integer.valueOf(julianDate);
		}
		} catch (Exception ex) {
			PrismHandler.handleException(ex,true);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultJulian;

	}

	public static String getSimpleDateFormatMMDDYYYY(String dateString) throws Exception{

		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
		Date date = null;
		try {
			date = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
		} catch (ParseException e) {
			Log.Error("Exception in parsing date:" + e.getMessage());
		}

		response = new SimpleDateFormat("MM/dd/yyyy").format(date);
		} catch (Exception ex) {
			PrismHandler.handleException(ex,true);
		}
		
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static String logSearchQuery(SearchLog searchLog) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		Connection connection = null;
		Statement stmt = null;
		try {
		
		String searchHistoryQuery = "SELECT count(*) as historycount FROM search_log";
		String deleteSearchHistoryQuery = "DELETE FROM search_log WHERE ID IN(SELECT ID FROM search_log";
		String totalIdQuery = "SELECT max(id) as id from search_log;";
		StringBuilder where = new StringBuilder(" where ");
		StringBuilder orderby = new StringBuilder(" ORDER BY logdate LIMIT 1");
		int historycount = 0;
		if (!searchLog.getUserID().equalsIgnoreCase(null)
				&& !searchLog.getUserID().equalsIgnoreCase("NULL")) {
			where.append(" userid = '").append(searchLog.getUserID())
					.append("' and ");
		}
		if (!searchLog.getType().equalsIgnoreCase(null)
				&& !searchLog.getType().equalsIgnoreCase("NULL")) {
			where.append(" type = '").append(searchLog.getType()).append("'");
		}
		StringBuilder queryToExecute = new StringBuilder(searchHistoryQuery)
				.append(where);
		StringBuilder deleteQueryToExecute = new StringBuilder(
				deleteSearchHistoryQuery).append(where).append(orderby)
				.append(")");
		// ResultSet rs = stmt1.executeQuery(queryToExecute.toString());
		PrismDbUtil dbUtil = new PrismDbUtil();
		connection = dbUtil.getPostGresConnection();
		
		int seacrhHistoryCountCutoff = 50;
		if (E2emfAppUtil
				.getAppProperty(E2emfConstants.seacrhHistoryCountCutoff) != null
				|| E2emfAppUtil
						.getAppProperty(E2emfConstants.seacrhHistoryCountCutoff) != "")
			seacrhHistoryCountCutoff = Integer.parseInt(E2emfAppUtil
					.getAppProperty(E2emfConstants.seacrhHistoryCountCutoff));

			stmt = connection.createStatement();
			System.out.println(queryToExecute.toString());
			ResultSet rs = stmt.executeQuery(queryToExecute.toString());
			if (rs != null) {
				while (rs.next()) {
					historycount = rs.getInt("historycount");
					if (historycount >= seacrhHistoryCountCutoff) {
						stmt.execute(deleteQueryToExecute.toString());
						break;
					}
					;
				}
			}
			rs.close();
			stmt.close();
			//To Get Max Id
			stmt = connection.createStatement();
			System.out.println(totalIdQuery);
			rs = stmt.executeQuery(totalIdQuery);
			if(rs.next()) {
				historycount = rs.getInt("id");
			}
			
			String insertQuery = "insert into search_log (userid, username,display_query,json_query,type,module,bookmarkname) values('"
					+ searchLog.getUserID()
					+ "','"
					+ searchLog.getUserName()
					+ "','"
					+ searchLog.getDisplayQuery()
					+ "','"
					+ searchLog.getJsonQuery()
					+ "','"
					+ searchLog.getType()
					+ "','" + searchLog.getModule() 
					+ "','"
					+ searchLog.getBookmarkName()
					+ "')";

			stmt.executeUpdate(insertQuery);
			response =
			 "{\"message\":\"Bookmark added successfully\", \"count\" : \""+(historycount +1)+"\"}";
		} catch (Exception ex) {
			response =PrismHandler.handleException(ex);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	};

	public static String shortlist(String userId, String userName,
			String solrID, String module,String shortlistall) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
	
		Connection connection = null;
		String searchHistoryQuery = "SELECT count(*) as shortlistcount FROM sf_shortlist";
		String deleteSearchHistoryQuery = "DELETE FROM sf_shortlist";
		StringBuilder where = new StringBuilder(" where ");

		if (!userId.equalsIgnoreCase(null) && !userId.equalsIgnoreCase("NULL")) {
			where.append(" userid = '").append(userId).append("' and ");
		}
		if (!module.equalsIgnoreCase(null) && !module.equalsIgnoreCase("NULL")) {
			where.append(" module = '").append(module).append("'");
		}
		if (solrID != null && !solrID.equalsIgnoreCase(null) && !solrID.equalsIgnoreCase("NULL")) {
			where.append(" and refid in ('").append(solrID).append("')");
		} else {
			where.append(" and 1 = 0");
		}
		
		PrismDbUtil dbUtil = new PrismDbUtil();
		connection = dbUtil.getPostGresConnection();
		Statement stmt;
		try {
			stmt = connection.createStatement();
			if (shortlistall != null && shortlistall.equalsIgnoreCase("true")){
				StringBuilder deleteQueryToExecute = new StringBuilder(
						deleteSearchHistoryQuery).append(where);
				stmt.execute(deleteQueryToExecute.toString());
				String[] solrIDs = solrID.split(",");
				for (int i =0; i<solrIDs.length;i++){
					String insertQuery = "insert into sf_shortlist (userid, username,refid,module) values('"
							+ userId
							+ "','"
							+ userName
							+ "','"
							+ solrIDs[i]
							+ "','" + module + "')";
					stmt.executeUpdate(insertQuery);
				}
			} else {
				StringBuilder queryToExecute = new StringBuilder(searchHistoryQuery)
				.append(where);
				System.out.println(queryToExecute.toString());
				ResultSet rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {
					while (rs.next()) {
						int historycount = rs.getInt("shortlistcount");
						if (historycount != 0) {
							StringBuilder deleteQueryToExecute = new StringBuilder(
									deleteSearchHistoryQuery).append(where);
							stmt.execute(deleteQueryToExecute.toString());
						} else {
							String insertQuery = "insert into sf_shortlist (userid, username,refid,module) values('"
									+ userId
									+ "','"
									+ userName
									+ "','"
									+ solrID
									+ "','" + module + "')";
							stmt.executeUpdate(insertQuery);
						}
					}
				}
			}
		} catch (SQLException sql) {
			Log.Error("SQLException while inserting search log details in table."
					+ sql.getStackTrace());
			Log.Error(sql);
			String message = sql.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			return newmessage;
		} catch (Exception e) {
			Log.Error("Exception while inserting user details in table."
					+ e.getStackTrace());
			Log.Error(e);
			String message = e.getMessage();
			String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",
					".");
			return newmessage;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
				}
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	};
	public static List<Shortlist> getShortlist(String userId, String userNamem, String module) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		String searchHistoryQuery = "SELECT userid,username,refid,module,date,\"Id\" as id FROM sf_shortlist";
		StringBuilder where = new StringBuilder(" where ");

		if (!userId.equalsIgnoreCase(null) && !userId.equalsIgnoreCase("NULL")) {
			where.append(" userid = '").append(userId).append("' and ");
		}
		if (!module.equalsIgnoreCase(null) && !module.equalsIgnoreCase("NULL")) {
			where.append(" module = '").append(module).append("'");
		}
		StringBuilder orderby = new StringBuilder(" ORDER BY date desc");
		StringBuilder queryToExecute = new StringBuilder(searchHistoryQuery)
				.append(where).append(orderby);
		PrismDbUtil dbUtil = new PrismDbUtil();
		connection = dbUtil.getPostGresConnection();
		Statement stmt;
		List<Shortlist> ShortlistArray = new ArrayList<Shortlist>();
		try {
			stmt = connection.createStatement();
			System.out.println(queryToExecute.toString());
			ResultSet rs = stmt.executeQuery(queryToExecute.toString());
			if (rs != null) {
				while (rs.next()) {
					Shortlist shortlist = new Shortlist();
					shortlist.setUserid(rs.getString("userid"));
					shortlist.setUsername(rs.getString("username"));
					shortlist.setRefid(rs.getString("refid"));
					shortlist.setModule(rs.getString("module"));
					shortlist.setDate(rs.getString("date"));
					//shortlist.setId(rs.getInt("id"));
					ShortlistArray.add(shortlist);
				}
			}
		} catch (SQLException sql) {
			Log.Error("SQLException while inserting search log details in table."
					+ sql.getStackTrace());
			Log.Error(sql);
			//String message = sql.getMessage();
			//String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",	".");
		} catch (Exception e) {
			Log.Error("Exception while inserting user details in table."
					+ e.getStackTrace());
			Log.Error(e);
			//String message = e.getMessage();
			//String newmessage = message.replaceAll("\"", "\'").replaceAll("\n",	".");
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					Log.Error(e);
				}
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return ShortlistArray;
	};
	public static int parseInt(String number) {
		  try {
		    return Integer.parseInt(number);
		  } catch (NumberFormatException e) {
		    return 0;
		  }
	}
	public static float parseFloat(String number) {
		  try {
		    return Float.parseFloat(number);
		  } catch (NumberFormatException e) {
		    return 0;
		  }
	}
	public static double parseDouble(String number) {
		  try {
		    return  Double.parseDouble(number);
		  } catch (NumberFormatException e) {
		    return 0;
		  }
	}
	public static java.sql.Date parseDate(String date) {
		  try {
		    return  new java.sql.Date(new java.util.Date(date).getTime());
		  } catch (Exception e) {
		    return  new java.sql.Date(Calendar.getInstance().getTime().getTime());
		  }
	}
	public static Timestamp parseTimestamp(String timestamp) throws ParseException {
		  try {
			  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = dateFormat.parse(timestamp);
				long time = date.getTime();
				return new Timestamp(time);
		  } catch (Exception e) {
		    return new Timestamp(System.currentTimeMillis());
		  }
	}
}
