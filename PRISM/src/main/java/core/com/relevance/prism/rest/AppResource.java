/**
 * 
 */
package com.relevance.prism.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.service.AppPropertiesService;
import com.relevance.prism.service.AppService;
import com.relevance.prism.util.Log;

/**
 * @author skhan71
 *
 */

@Path("/app")
public class AppResource extends BaseResource {
 

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getallowedapplistforuser")
	public String getAllowedAppListForUser(@FormParam("username") String username) throws Exception{
		AppService svcAppProp = new AppService();
		String response = svcAppProp.getAllowedAppListForUser(username);			
		return response;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updatelastusedapp")
	public String updateLastUsedAppByUser(@FormParam("username") String username, @FormParam("lastusedapp") String lastusedapp ) throws Exception{
		String response = "";
		Boolean status =  false;
		Log.Info("updateLastUsedAppByUser method on  App Properties service");
		AppService svcAppProp = new AppService();
		status = svcAppProp.updateLastUsedAppByUser(username, lastusedapp);
		response = "{ \"isUpdated\"  :\""+status+"\"} "; 
		return response; 
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getlastusedappofuser")
	public String getLastUsedAppOfUser(@FormParam("username") String username) throws Exception{
		AppService svcAppProp = new AppService();
		String response = svcAppProp.getLastUsedAppOfUser(username);			
		return response;
	}
	
}
