package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.relevance.prism.data.Attribute;
import com.relevance.prism.data.Role;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;

public class QfindMDMDao extends BaseDao {
	public List<Attribute> getAllattribute() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<Attribute> attList = new ArrayList<Attribute>();
		Attribute attribute = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery("select * from attribute_rule");
			while (rs.next()) {
				attribute = new Attribute();
				attribute.setAttribute_name(rs.getString("attribute_name"));
				attribute.setRule_name(rs.getString("rule_name"));
				attList.add(attribute);
			}
			for(Attribute a:attList){
				System.out.println(a.getAttribute_name());
			}

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}

		return attList;
	}
	
	public HashMap<String, String> getAllattributeMap() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, String> attList = new HashMap<String, String>();
		Attribute attribute = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery("select * from attribute_rule");
			while (rs.next()) {
				attribute = new Attribute();
				String attibute_name = rs.getString("attribute_name");
				String rule_name = rs.getString("rule_name");
				attList.put(attibute_name, rule_name);

			}
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}

		return attList;
	}
	public Attribute getAttribute(String attributename) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Attribute attribute = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			rs = stmt
					.executeQuery("select * from attribute_rule where attribute_name = '" + attributename + "'");
			while (rs.next()) {
				attribute = new Attribute();
				attribute.setAttribute_name(rs.getString("attribute_name"));
				attribute.setRule_name(rs.getString("rule_name"));
			}

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}

		return attribute;
	}

	public boolean addAttribute(String attributename, String rulename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		boolean success = false;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			stmt.executeUpdate("insert into attribute_rule values('"
					+ attributename + "','" + rulename + "')");
			success = true;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			success = false;
		} finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}

		return success;
	}

	public boolean updateAttribute(String attributename, String rulename)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		boolean success = false;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			stmt.executeUpdate("update attribute_rule set rule_name= '"
					+ rulename + "' where attribute_name= '" + attributename+"'");
			success = true;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			success = false;
		} finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}

		return success;
	}

	public boolean deleteAttribute(String attributename) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		boolean success = false;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			stmt.executeUpdate("delete from attribute_rule where attribute_name= '" + attributename + "'");
			success = true;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			success = false;
		} finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}

		return success;
	}

	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}

}
