package com.relevance.prism.data;

import java.util.ArrayList;

public class FieldList {
	private ArrayList<Field> descriptionList;

	public ArrayList<Field> getDescriptionList() {
		return descriptionList;
	}

	public void setDescriptionList(
			ArrayList<Field> descriptionList) {
		this.descriptionList = descriptionList;
	}
}