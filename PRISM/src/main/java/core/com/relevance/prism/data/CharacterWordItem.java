package com.relevance.prism.data;

public class CharacterWordItem 
{
	
	private Character characterFrom;
	private Character characterTo;
	private String wordFrom;
	private String wordTo;
	
	public Character getCharacterFrom() {
		return characterFrom;
	}
	public void setCharacterFrom(Character characterFrom) {
		this.characterFrom = characterFrom;
	}
	public Character getCharacterTo() {
		return characterTo;
	}
	public void setCharacterTo(Character characterTo) {
		this.characterTo = characterTo;
	}
	public String getWordFrom() {
		return wordFrom;
	}
	public void setWordFrom(String wordFrom) {
		this.wordFrom = wordFrom;
	}
	public String getWordTo() {
		return wordTo;
	}
	public void setWordTo(String wordTo) {
		this.wordTo = wordTo;
	}
	
	
	

}
