package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.relevance.prism.data.CrystalAgilePart;
import com.relevance.prism.util.PrismHandler;
public class MerckDao extends BaseDao {
	int counter = 10;
	
	public List<CrystalAgilePart> getBOMCrystal(Connection con, String matNumber,int level) throws Exception {
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		counter--;
		CrystalAgilePart matInfo  = null;
		List<CrystalAgilePart> matInfoList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Statement stmt = null;				
		try {			
			query.append("SELECT parent, plant,materialnumber, material_plant, parent_plant, material_name, parent_name FROM crystal_bom");
			if (matNumber != null && !matNumber.isEmpty()){
				query.append(" WHERE parent_plant = '").append(matNumber).append("' order by parent, plant,materialnumber, material_plant, parent_plant, material_name, parent_name");
			}
			level--;
			if (con != null && level>=0) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					String parent  = rs.getString("parent");
					String materialNumber = rs.getString("materialnumber");
					String materialPlant = rs.getString("material_plant");
					String parentPlant = rs.getString("parent_plant");
					String materialName = rs.getString("material_name");
					String parentName = rs.getString("parent_name");
					String plant = rs.getString("plant");
										
					matInfo = new CrystalAgilePart();
					
					matInfo.setChildren(getBOMCrystal(con,materialPlant,level));
					
					matInfo.setParent(parent);
					matInfo.setPlant(plant);
					matInfo.setMaterialNumber(materialNumber);
					matInfo.setMaterialName(materialName);
					matInfo.setParentName(parentName);
					matInfoList.add(matInfo);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(null, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return matInfoList;
	}
	
	public List<CrystalAgilePart> getBackBOMCrystal(Connection con, String matNumber,int level) throws Exception
	{
		if (matNumber != null && !matNumber.isEmpty()){
			System.out.println("IN : "+matNumber);
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			counter--;
			CrystalAgilePart matInfo  = null;
			List<CrystalAgilePart> matNodeList = new ArrayList<>();
			StringBuilder query = new StringBuilder();
			
			ResultSet rs = null;
			Statement stmt = null;				
			try {			
	
				query.append("SELECT parent, plant,materialnumber, material_plant, parent_plant, material_name, parent_name FROM crystal_bom  WHERE material_plant ='");
				query.append(matNumber).append("' order by parent, plant,materialnumber, material_plant, parent_plant, material_name, parent_name DESC");
				
				level--;
				if (con != null && level>=0) {
					stmt = con.createStatement(	java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
					rs = stmt.executeQuery(query.toString()); //has all parent in parents prop
					/*List<String> parentIds=new ArrayList<>();
					System.out.print(" parentIDs  :");
					while (rs.next()) {					
						if(rs.getString("parent_plant")!=null){
							parentIds.add(rs.getString("parent_plant"));
							System.out.print(" "+rs.getString("parent_plant")+" ");
						}
					}System.out.println("");
					
					stmt = con.createStatement(	java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
					rs = stmt.executeQuery(query.toString()); //has all parent in parents prop				
					if(rs.next()){

						System.out.println("node : "+rs.getString("materialnumber"));
						String parent  = rs.getString("parent");					
						String materialNumber = rs.getString("materialnumber");
						String materialName = rs.getString("material_name");
						String parentName = rs.getString("parent_name");
						String plant = rs.getString("plant");
											
						matInfo = new CrystalAgilePart();
						matInfo.setParent(parent);
						matInfo.setPlant(plant);
						matInfo.setMaterialNumber(materialNumber);
						matInfo.setMaterialName(materialName);
						matInfo.setParentName(parentName);
						
						matNodeList.add(matInfo);
						level--;
						if(level>=0)
						{							
							for(String parentPlant : parentIds) {//parents to a node
								
								List<CrystalAgilePart>	parentList = getBackBOMCrystal(con,parentPlant,level);							
								if(parentList!=null && parentList.size()>0){
									if(matInfo.getChildren()==null){
										matInfo.setChildren(new ArrayList<CrystalAgilePart>() );
									}
									matInfo.getChildren().addAll(parentList);
								}
							}		
						}
					}*/
					while (rs.next()) {
						String parent  = rs.getString("parent");
						String materialNumber = rs.getString("materialnumber");
						String materialPlant = rs.getString("material_plant");
						String parentPlant = rs.getString("parent_plant");
						String materialName = rs.getString("material_name");
						String parentName = rs.getString("parent_name");
						String plant = rs.getString("plant");
											
						matInfo = new CrystalAgilePart();
						
						matInfo.setChildren(getBackBOMCrystal(con,parentPlant,level));
							
						matInfo.setParent(parent);
						matInfo.setPlant(plant);
						matInfo.setMaterialNumber(materialNumber);
						matInfo.setMaterialName(materialName);
						matInfo.setParentName(parentName);
						matNodeList.add(matInfo);
				}
				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(null, rs, stmt);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			System.out.println("Out "+matInfo+" with :"+matNodeList);
			return matNodeList;
		}else
			return null;
	}
	
	public List<String> getMongoDBCollectionList(String database) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		MongoClient mongoClient = null;
		List<String> collectionList = new ArrayList<>();		
		StringBuilder query = new StringBuilder();
		String ipHeader = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {						
				con = dbUtil.getPostGresConnection();
				query.append("SELECT value FROM app_properties WHERE name='").append("mongo_"+database).append("';");
				if (con != null) {
					stmt = con.createStatement();
					rs = stmt.executeQuery(query.toString());
					while (rs.next()) {
						ipHeader = rs.getString("value");	
					}
				}				
			mongoClient = new MongoClient(new MongoClientURI(ipHeader));  
			MongoDatabase mDatabase = mongoClient.getDatabase(database);
			MongoIterable <String> collections = mDatabase.listCollectionNames();
			for (String collectionName: collections) {
				collectionList.add(collectionName);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			mongoClient.close();
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return collectionList;
	}
	
	public String[] getMongoDBConnection(String database, String collection) throws Exception  {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		MongoClient mongoClient = null;
		boolean result = false;
		StringBuilder query = new StringBuilder();
		String ipHeader = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		Set<String> collectionKeySet=new HashSet<String>();
		List<String> res = new ArrayList<String>();
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT value FROM app_properties WHERE name='").append("mongo_"+database).append("';");
			if (con != null) {
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					ipHeader = rs.getString("value");	
				}
			}
			mongoClient = new MongoClient(new MongoClientURI(ipHeader));  
			MongoDatabase mDatabase = mongoClient.getDatabase(database);
			MongoCollection<Document> mCollection = mDatabase.getCollection(collection);
			if(mCollection==null){
				result=false;
				res.add("false");
			}	
			MongoCursor<Document> cursor = mCollection.find(new BasicDBObject("filename","28062-0.xlsx")).iterator();
			
			while (cursor.hasNext()) {				
				Document doc=cursor.next();
				JSONParser parser = new JSONParser(); 
				JSONObject jsondoc = (JSONObject) parser.parse(doc.toJson());								
				for(Object key:jsondoc.keySet())
					collectionKeySet.add(key.toString().trim());
			}
			res.add("true");
			res.addAll(collectionKeySet);			
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			mongoClient.close();
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return res.toArray(new String[0]);
	}
	
	public List<String> executeMongoQuery(String database, String collection, String query, String options) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		MongoClient mongoClient = null;
		List<String> documentList =new ArrayList<>();
		StringBuilder querysource = new StringBuilder();
		String ipHeader = null;
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {			
			con = dbUtil.getPostGresConnection();
			querysource.append("SELECT value FROM app_properties WHERE name='").append("mongo_"+database).append("';");
			if (con != null) {
				stmt = con.createStatement();
				rs = stmt.executeQuery(querysource.toString());
				while (rs.next()) {
					ipHeader = rs.getString("value");	
				}
			}
			
			mongoClient = new MongoClient(new MongoClientURI(ipHeader));  
			MongoDatabase mDatabase = mongoClient.getDatabase(database);
			MongoCollection<Document> mCollection = mDatabase.getCollection(collection);
			BasicDBObject andQuery = new BasicDBObject();
			
			JSONParser parser = new JSONParser(); 
			JSONObject json = (JSONObject) parser.parse(query);
			JSONArray querylist=(JSONArray) json.get("querylist");
			List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
			Iterator it=querylist.iterator();
			while(it.hasNext())
			{	
				JSONObject j=(JSONObject) it.next();
				String key = j.get("key").toString();
				String value = j.get("value").toString();					
				String regexp = j.get("regexp").toString();					
			
				if(regexp.equals("True")){
					obj.add(new BasicDBObject(key,new BasicDBObject("$regex", value).append("$options", "i")));					
				}else
				{
					obj.add(new BasicDBObject(key,value));		
				}
			}
			
			andQuery.put("$and", obj);			
			
			MongoCursor<Document> cursor = mCollection.find(andQuery).iterator();
			while (cursor.hasNext()) {
				documentList.add(cursor.next().toJson());
			}
					
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			mongoClient.close();
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return documentList;
	}
	
	public String updateDuplicate(Object...obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Integer seqNo = null;
		String status = null;
		String groupId = null;
		Connection connection = null;
		Statement stmt = null;
		try {
			seqNo = (Integer) obj[0];
			status = (String) obj[1];
			groupId = (String) obj[2];
			connection = dbUtil.getPostGresConnection("KALLIK");
			stmt = connection.createStatement();
			String updateQuery = null;
			if(status.equals("neutral")) {
				updateQuery = "update duplicate_data set status='Duplicate' where dd_idgroup='"+groupId+"';";
				updateQuery += "update duplicate_data set status='Winner' where dd_seqno="+ seqNo +" and dd_idgroup='"+groupId+"'";
			} else {
				if(status.equals("like")){
					updateQuery = "update duplicate_data set status='Winner' where dd_seqno="+ seqNo +" and dd_idgroup='"+groupId+"'";
				} else {
					updateQuery = "update duplicate_data set status='Duplicate' where dd_seqno="+ seqNo +" and dd_idgroup='"+groupId+"'";
				}
			}
			stmt.executeUpdate(updateQuery);			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated Field in DB.\"}";
	}
	
}
