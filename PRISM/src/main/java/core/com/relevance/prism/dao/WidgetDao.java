package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.data.Widget;

public class WidgetDao  extends BaseDao  {

	Connection connection = null;
	Statement stmt = null;
	
	public WidgetDao() {
		
	}
	
	public Widget getWidgetByName(String widgetName)  throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		stmt = null;
		ResultSet rs = null;
		StringBuilder query = null;
		Widget widgetResponse = null;
		try {
			connection = dbUtil.getPostGresConnection();
			Log.Debug("getWidgetByName().. Connecting to DB...");
			
			query = new StringBuilder("select * from widgets_ui where name = '"+widgetName+"'");
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query.toString());
			if(rs.next()) {
				widgetResponse = new Widget();
				widgetResponse.setId(rs.getInt("id"));
				widgetResponse.setLogic(rs.getString("logic"));
				widgetResponse.setName(rs.getString("name"));
				widgetResponse.setView(rs.getString("view"));
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		
		return widgetResponse;
	}
}
