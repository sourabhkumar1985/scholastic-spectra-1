package com.relevance.prism.data;

import java.util.ArrayList;

public class Sankey {
	
	public ArrayList<Stack> nodes;
	public ArrayList<Link> links;
	public ArrayList<Stack> getNodes() {
		return nodes;
	}
	public void setNodes(ArrayList<Stack> nodes) {
		this.nodes = nodes;
	}
	public ArrayList<Link> getLinks() {
		return links;
	}
	public void setLinks(ArrayList<Link> links) {
		this.links = links;
	}
}
