package com.relevance.prism.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.relevance.prism.dao.NGramDao;
import com.relevance.prism.data.NGram;
import com.relevance.prism.data.NGramConfig;
import com.relevance.prism.data.NGramList;
import com.relevance.prism.util.Lemmatizer;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class NGramService extends BaseService {
	public NGramList nGramAnalyser(NGramConfig nGramConfig) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		NGramList nGramView = new NGramList();
		String inputString;
		List<NGram> nGramList = new ArrayList<>();
		try {
			NGramDao nGramDao = new NGramDao();
			inputString = nGramConfig.getInputString();
			if (inputString != null && !inputString.isEmpty()){
				nGramView = nGramDao.nGramAnalyser(nGramConfig.getInputString(),nGramConfig.getGram());
			}else{
				 Iterator<Map.Entry<String, Integer>> it ;
				Map<String, Integer> nGramHashMap = nGramDao.performNgram(nGramConfig);
				it = nGramHashMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Integer> pairs = it.next();
					NGram nGram = new NGram();
					nGram.setText(pairs.getKey());
					nGram.setSize(pairs.getValue());
					nGramList.add(nGram);
				}
				Collections.sort(nGramList);
				if (nGramList.size()>100 && !nGramConfig.isAllRecords()){
					List<NGram> subItems = new ArrayList<>(nGramList.subList(0, 100));
					nGramView.setNGramAnalyser(subItems);
				}else
					nGramView.setNGramAnalyser(nGramList);
			}
		} catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return nGramView;
	}
	
	public NGramList nGramAnalyserNLP(NGramConfig nGramConfig) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		NGramList nGramView = new NGramList();
		String inputString;
		List<NGram> nGramList = new ArrayList<>();
		Lemmatizer lemmatizer = new Lemmatizer();
		try {
			NGramDao nGramDao = new NGramDao();
			inputString = nGramConfig.getInputString();
			if (inputString != null && !inputString.isEmpty()){
				nGramView = nGramDao.nGramAnalyser(nGramConfig.getInputString(),nGramConfig.getGram());
			}else{
				 Iterator<Map.Entry<String, Integer>> it ;
				Map<String, Integer> nGramHashMap = nGramDao.performNgram(nGramConfig);
				it = nGramHashMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Integer> pairs = it.next();
					NGram nGram = new NGram();
					nGram.setText(pairs.getKey());
					nGram.setSize(pairs.getValue());
					nGramList.add(nGram);
				}
				Collections.sort(nGramList);
				if (nGramList.size()>100 && !nGramConfig.isAllRecords()){
					 List<NGram> subItems = new ArrayList<>(nGramList.subList(0, 100));
					 for(NGram nGram:subItems)
					      nGram.setText(lemmatizer.getLemma(nGram.getText()).trim());     
					 nGramView.setNGramAnalyser(subItems);
				} else
					nGramView.setNGramAnalyser(nGramList);
			}
		} catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return nGramView;
	}
	
	public List<NGramList> getNgramFilter(NGramList ngram) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<NGramList> nGramList = null;
		try {
			Log.Info("getNgramFilter method called on NGramService");
			NGramDao nGramDao = new NGramDao();
			
			nGramList = nGramDao.getNgramFilter(ngram);
			Log.Info("Fetched ngram object from DAO " + ngram.getuserName());

		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return nGramList;
	}
	
	public String addNgramFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("getNgramFilter method called on NGramService");
			NGramDao nGramDao = new NGramDao();
			
			responseMessage = nGramDao.addNgramFilter(obj);
			return responseMessage;
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String updateNgramFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("getNgramFilter method called on NGramService");
			NGramDao nGramDao = new NGramDao();
			
			responseMessage = nGramDao.updateNgramFilter(obj);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteNgramFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			Log.Info("getNgramFilter method called on NGramService");
			NGramDao nGramDao = new NGramDao();
			
			responseMessage = nGramDao.deleteNgramFilter(obj);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		}

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
}
