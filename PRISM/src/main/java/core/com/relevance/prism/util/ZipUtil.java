package com.relevance.prism.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import jline.internal.Log;

import org.apache.commons.io.FileUtils;

public class ZipUtil {
	
	private static String zipFilePath = null;
	
	public static String createZipFile(String [] urllist){
		try {
			
			String tDir = System.getProperty("java.io.tmpdir");
			zipFilePath = tDir + File.separator + "Smartfind_Multidrawings.zip";
			File tempFile = new File(zipFilePath);
			tempFile.delete();
			FileOutputStream fos = new FileOutputStream(zipFilePath);
			ZipOutputStream zos = new ZipOutputStream(fos);

			for(String urlPath : urllist){
				URL url = new URL(urlPath);	            
	            //String fileName = urlPath.substring(urlPath.lastIndexOf(File.separator)+1);
				String fileName = urlPath.substring(urlPath.lastIndexOf("/")+1);
	            String path = tDir + fileName;
	            //File inputFile = new File(filePath);
	            File outPutfile = new File(path);
	            FileUtils.copyURLToFile(url, outPutfile);
				//addToZipFile(inputFile, fileName, zos);
				addToZipFile(outPutfile, fileName, zos);
			}
			zos.close();
			fos.close();
			return zipFilePath;
		} catch (FileNotFoundException ex) {
			Log.error("FileNotFoundException Occurred While Downloading zip File..." + ex);
		} catch (IOException ex) {
			Log.error("IOException Occurred While Downloading zip File..." + ex);
		}
		return zipFilePath;
	}
	
	public static void addToZipFile(File file, String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

		//System.out.println("Writing '" + fileName + "' to zip file");

		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}
}
