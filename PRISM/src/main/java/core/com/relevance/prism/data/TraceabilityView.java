package com.relevance.prism.data;

import java.util.List;

public class TraceabilityView {
	private List<TraceabilityTree> forwardTraceability; 
	private List<TraceabilityTree> backwardTraceability;
	public List<TraceabilityTree> getForwardTraceability() {
		return forwardTraceability;
	}
	public void setForwardTraceability(List<TraceabilityTree> forwardTraceability) {
		this.forwardTraceability = forwardTraceability;
	}
	public List<TraceabilityTree> getBackwardTraceability() {
		return backwardTraceability;
	}
	public void setBackwardTraceability(List<TraceabilityTree> backwardTraceability) {
		this.backwardTraceability = backwardTraceability;
	}
}
