package com.relevance.prism.util;


public class BaseUtil 
{
	protected BaseUtil(){
	}
	
	protected String getCurrentMethodName()
	{
		if (Thread.currentThread().getStackTrace() != null && Thread.currentThread().getStackTrace().length > 2)
			return Thread.currentThread().getStackTrace()[2].getMethodName();
		else
			return "UnknownMethod";
	}
	
	protected String getClassName()
	{
		if (Thread.currentThread().getStackTrace() != null && Thread.currentThread().getStackTrace().length > 2)
			return Thread.currentThread().getStackTrace()[2].getClassName();
		else
			return "UnknownClass";
	}

}
