package com.relevance.prism.data;

/**
 *
 * @author: Shawkath Khan
 * @Created_Date: Feb 17, 2016
 * @Purpose: Entity for Workflow Entity Audit 
 * @Modified_By: 
 * @Modified_Date:  
 */

public class WfEntityAudit {

	private String entity_id;
	private String state;
	private String user;
	private String lastUpdated;
	private String comments;
	private int key;
	
	
	public String getEntity_id() {
		return entity_id;
	}
	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	
	
}
