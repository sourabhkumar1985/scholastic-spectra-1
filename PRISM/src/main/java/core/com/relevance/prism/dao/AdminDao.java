package com.relevance.prism.dao;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.LookupValue;
import com.relevance.prism.data.Role;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;

public class AdminDao extends BaseDao{
	
	public String insertAction(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action param = null;
		int maxactionid = 0;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (obj != null) {
			param = (Action) obj[0];
			Log.Info("Inserting  Values for Action Object : " + param);
			try {
				String selectMaxQuery = "select max(action_id) from actions";
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				System.out.println(selectMaxQuery);
				rs = stmt.executeQuery(selectMaxQuery);
				while (rs.next()) {
					maxactionid = rs.getInt(1);
				}
				maxactionid = maxactionid + 1;
				String inertActionQuery = "insert into actions (action_id, action, display_name, parent,sequence, icon, enabled,sources) values("
						+ maxactionid
						+ ",'"
						+ param.getAction()
						+ "','"
						+ param.getDisplayname() 
						+ "',"
						+ param.getParent()
						+ ","
						+ param.getSequence()
						+",'"
						+ param.getIcon()
						+"',"
						+ param.getEnabled()
						+",'"
						+ param.getSource()
						+"'"
						+")";
				System.out.println(inertActionQuery);
			stmt.executeUpdate(inertActionQuery);
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return Integer.toString(maxactionid);
	}
	
	public Action updateAction(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action param = null;
		Action actionToStore = null;
		Action resultAction = null;
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		if (obj != null) {
			actionToStore = new Action();
			param = (Action) obj[0];
			Log.Info("Updating the Action in DB : " + param);
			actionToStore.setId(param.getId());
			actionToStore.setAction(param.getAction());
			actionToStore.setDisplayname(param.getDisplayname());
			actionToStore.setParent(param.getParent());
			actionToStore.setParentName(param.getParentName());
			actionToStore.setSequence(param.getSequence());
			actionToStore.setIcon(param.getIcon());
			actionToStore.setEnabled(param.getEnabled());
			actionToStore.setSource(param.getSource());
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String selectActionQuery = "select a.action_id, a.action, a.display_name, a.parent,b.display_name as parent_name, a.sequence, a.icon, a.enabled,a.sources from actions a inner join actions b on b.action_id = a.parent where a.action_id=" + actionToStore.getId();
				String updateQueryForAction = "update actions set action='"	+ actionToStore.getAction() 
						+ "', display_name='"+ actionToStore.getDisplayname()
						+ "', parent="+ actionToStore.getParent()
						+ ", sequence="+ actionToStore.getSequence()
						+ ", icon='"+ actionToStore.getIcon()
						+ "', enabled="+ actionToStore.getEnabled()
						+ ", sources='"+ actionToStore.getSource()
						+ "' where action_id = " + actionToStore.getId();	
				System.out.println(updateQueryForAction);
				stmt.executeUpdate(updateQueryForAction);
				System.out.println(selectActionQuery);
				rs = stmt.executeQuery(selectActionQuery);
				while (rs.next()) {
					resultAction = new Action(); 	
					resultAction.setId(Integer.parseInt(rs.getString("action_id")));
					resultAction.setAction(rs.getString("action"));
					resultAction.setDisplayname(rs.getString("display_name"));
					resultAction.setParent(Integer.parseInt(rs.getString("parent")));
					resultAction.setSequence(Integer.parseInt(rs.getString("sequence")));
					resultAction.setIcon(rs.getString("icon"));
					resultAction.setParentName(rs.getString("parent_name"));
					resultAction.setEnabled(Integer.parseInt(rs.getString("enabled")));
					resultAction.setSource(rs.getString("sources"));	
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultAction;		
	}
	
	public String deleteAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action param = null;
		Action actionToDelete = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			actionToDelete = new Action();
			param = (Action) obj[0];
			Log.Info("Deleting the user from DB : " + param);
			actionToDelete.setId(param.getId());
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				Action actionInDB = getAction(actionToDelete);
				if(actionInDB == null){
					return "{\"message\" : \"failure while deleting the action from DB\"}";
				}
				String actionDeleteQuery = "delete from actions where action_id = " + actionToDelete.getId() + "";
				System.out.println(actionDeleteQuery);
				stmt.executeUpdate(actionDeleteQuery);
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully deleted the action from DB\"}";
	}
	
	public List<Action> getActionList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Action> actionList = new ArrayList<>();
		Action actionToStore = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			//Properties prop = new Properties();
			//InputStream input = null;
			//input = new FileInputStream("E:\\git repo\\spetra-ui\\src\\main\\resources\\database.properties");
			// load a properties file
			//prop.load(input);
			String dbValue = "UNSIGNED";
			/*if(PrismDbUtil.isMysqlDatabase())
			{
				dbValue="UNSIGNED";

			}
			else
			{
				dbValue="int";
			}*/
			String query = "select action_id,action,ll.display_name,parent,parent_name,sequence,icon,enabled,ll.sources,b.id as refid from (select a.action_id as action_id, a.action as action, a.display_name as display_name, a.parent as parent,b.display_name as parent_name, a.sequence as sequence, a.icon as icon, a.enabled as enabled,a.sources as sources from actions a inner join actions b on b.action_id = a.parent order by a.action_id desc) ll left outer join profile_analyser b on CAST(b.actionid as "+dbValue+") = CAST(ll.action_id as "+dbValue+") ";

			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				actionToStore = new Action();
				actionToStore.setId(rs.getInt("action_id"));
				actionToStore.setAction(rs.getString("action"));
				actionToStore.setDisplayname(rs.getString("display_name"));
				actionToStore.setParent(rs.getInt("parent"));
				actionToStore.setSequence(rs.getInt("sequence"));
				actionToStore.setIcon(rs.getString("icon"));
				actionToStore.setEnabled(rs.getInt("enabled"));
				actionToStore.setParentName(rs.getString("parent_name"));
				actionToStore.setSource(rs.getString("sources"));
				actionToStore.setRefid(rs.getString("refid"));
				actionList.add(actionToStore);
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return actionList;
	}
	
	public Action getAction(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action actionToStore = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		Action param = null;
		if (obj != null) {
			param = (Action) obj[0];
			Log.Info("Inserting  Values for Role Object : " + param);
			try {
				String query = "select a.action_id, a.action, a.display_name, a.parent,b.display_name as parent_name, a.sequence, a.icon, a.enabled,a.sources from actions a inner join actions b on b.action_id = a.parent where a.action_id = " + param.getId();
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				System.out.println(query);
				rs = stmt.executeQuery(query);
				while (rs.next()) {
					actionToStore = new Action();
					actionToStore.setId(rs.getInt("action_id"));
					actionToStore.setAction(rs.getString("action"));
					actionToStore.setDisplayname(rs.getString("display_name"));
					actionToStore.setParent(rs.getInt("parent"));
					actionToStore.setSequence(rs.getInt("sequence"));
					actionToStore.setIcon(rs.getString("icon"));
					actionToStore.setEnabled(rs.getInt("enabled"));
					actionToStore.setParentName(rs.getString("parent_name"));
					actionToStore.setSource(rs.getString("sources"));
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return actionToStore;
	}
	
	public String insertRoleAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Role param = null;
		if (obj != null) {
			param = (Role) obj[0];
			Log.Info("Inserting  Values for Role Object : " + param);
			long max_role_action_id = 0;
			Connection connection = null;
			Statement stmt = null;
			ResultSet rs = null;
			String selectMaxQuery = "select max(role_action_id) from role_actions";
			try{
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				System.out.println(selectMaxQuery);
				rs = stmt.executeQuery(selectMaxQuery);
				while (rs.next()) {
					max_role_action_id = rs.getLong(1);
				}
				max_role_action_id = max_role_action_id + 1;
				String insertQuery = "insert into role_actions (role_action_id, role, action_id) values("
						+ max_role_action_id
						+ ",'"
						+ param.getRole()
						+ "',"
						+ param.getActionId() + ")";
				System.out.println(insertQuery);
				stmt.executeUpdate(insertQuery);
	
			}catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted role action\"}";
	}
	
	public String deleteRoleAction(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Statement stmt = null;
		Role param = null;
		if (obj != null) {
			param = (Role) obj[0];
			Log.Info("Inserting  Values for Role Object : " + param);
			Connection connection = null;
			try {
				connection = dbUtil.getPostGresConnection();
				stmt = connection.createStatement();
				String roleActionTableDeleteQuery = "delete from role_actions where role_action_id = " + param.getRoleActionId();
				System.out.println(roleActionTableDeleteQuery);
				stmt.executeUpdate(roleActionTableDeleteQuery);
	
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());	
		return "{\"message\" : \"successfully deleted the role_actions from DB\"}";
	}
	
	public List<Role> getRoleActionList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Role> roleList = new ArrayList<>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			String query = "select role_action_id,role,a.action_id,display_name from role_actions a inner join actions b on a.action_id = b.action_id order by role_action_id desc";
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				Role role = new Role();
				role.setRoleActionId(rs.getInt("role_action_id"));
				role.setRole(rs.getString("role"));
				role.setActionId(rs.getLong("action_id"));
				role.setActionName(rs.getString("display_name"));
				roleList.add(role);
			}			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return roleList;
	}
	
	public String insertLookupValue(String key, String displayname) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		try{
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String insertQuery = "insert into lookuptable (`key`, displayname) values("
					+ "'"
					+ key
					+ "','"
					+ displayname + "')";
			System.out.println(insertQuery);
			stmt.executeUpdate(insertQuery);
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted lookup value in lookup table\"}";
	}
	
	public String updateLookupValue(String key, String displayname) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		try{
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String updateQuery = "update lookuptable set displayname='"	+ displayname + "' where `key` = '" + key + "'";
			System.out.println(updateQuery);
			stmt.executeUpdate(updateQuery);
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully updated lookup value in lookup table\"}";
	}
	
	public String deleteLookupValue(String key) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		try{
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String updateQuery = "delete from lookuptable where `key` = '" + key + "'";
			System.out.println(updateQuery);
			stmt.executeUpdate(updateQuery);			
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully deleted lookup value from lookup table\"}";
	}
	
	public String getLookUpValueList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet rs = null;
		Connection connection = null;
		connection = dbUtil.getPostGresConnection();
		Statement stmt = connection.createStatement();
		Gson gson = new Gson();		
		List<LookupValue> hashTableList = new ArrayList<>();
		try {
			String roleQuery = "select `key`, displayName from lookuptable";
			System.out.println(roleQuery);
			rs = stmt.executeQuery(roleQuery);
			while (rs.next()) {	
				LookupValue hashTable = new LookupValue();
				hashTable.setKey(rs.getString("key"));
				hashTable.setDisplayName(rs.getString("displayName"));
				hashTableList.add(hashTable);
			}
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (!hashTableList.isEmpty())
			return gson.toJson(hashTableList);
		else 
			return null;
	}
	
	public Role getLogo(String name) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet rs = null;
		Connection connection = null;
		connection = dbUtil.getPostGresConnection();
		Statement stmt = connection.createStatement();
		Role applicationConfig = new Role();
		try {
			String roleQuery = "select logo,favicon from users inner join roles on  users.primaryrole = roles.name where users.username = '"+name+"' limit 1 ";
			System.out.println(roleQuery);
			rs = stmt.executeQuery(roleQuery);
			if (rs != null) {
				while (rs.next()) {	
					
					applicationConfig.setLogo(rs.getString("logo"));
					applicationConfig.setFavicon(rs.getString("favicon"));
				}
			}
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return applicationConfig;
	}
	
}
