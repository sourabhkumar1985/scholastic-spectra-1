package com.relevance.prism.data;

import java.util.List;

import org.json.JSONArray;

import com.google.gson.Gson;

public class DataTableView  implements E2emfView {

	JSONArray data;
	String recordsTotal;
	String recordsFiltered;
	JSONArray summaryData;
	String dataQuery;
	List<ProfileView> profileViews;
	EmailNotification emailNotification;
	List<ProfileAction> profileActions;
	public JSONArray getData() {
		return data;
	}
	public void setData(JSONArray data) {
		this.data = data;
	}
	public String getDataQuery() {
		return dataQuery;
	}
	public void setDataQuery(String dataQuery) {
		this.dataQuery = dataQuery;
	}
	public String getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(String recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public String getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(String recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public JSONArray getSummaryData() {
		return summaryData;
	}
	public void setSummaryData(JSONArray summaryData) {
		this.summaryData = summaryData;
	}
	public List<ProfileView> getProfileViews() {
		return profileViews;
	}
	public void setProfileViews(List<ProfileView> profileViews) {
		this.profileViews = profileViews;
	}
	public EmailNotification getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(EmailNotification emailNotification) {
		this.emailNotification = emailNotification;
	}
	public List<ProfileAction> getProfileActions() {
		return profileActions;
	}
	public void setProfileActions(List<ProfileAction> profileActions) {
		this.profileActions = profileActions;
	}
	@Override
	public String toString()
	{
		DataTableStringView view = new DataTableStringView();
		view.setData(this.data.toString());
		view.setSummaryData(this.summaryData.toString());
		view.setRecordsFiltered(this.recordsFiltered);
		view.setRecordsTotal(this.recordsTotal);
		Gson gson = new Gson();
		return gson.toJson(view);
	}
}
