package com.relevance.prism.service;


import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.swing.text.Keymap;

import com.relevance.prism.dao.AppPropertiesDao;

public class LicenseKeyService extends BaseService {
	static String qsync = "QSYNC";
	static String merck = "MERCK";
	boolean qsyncmerckstatus = false;
	private static Properties prop = null;	
	AppPropertiesDao dao = new AppPropertiesDao();
	Map <String,String> keyMap = null;
	
	public LicenseKeyService(){
		keyMap = new HashMap<String,String>();
		keyMap.put("0", "A");
		keyMap.put("E", "B");
		keyMap.put("5", "C");
		keyMap.put("I", "D");
		keyMap.put("D", "E");
		keyMap.put("3", "F");
		keyMap.put("H", "G");
		keyMap.put("P", "H");
		keyMap.put("6", "I");
		keyMap.put("C", "J");
		keyMap.put("9", "K");
		keyMap.put("G", "L");
		keyMap.put("1", "M");
		keyMap.put("Y", "N");
		keyMap.put("B", "O");
		keyMap.put("J", "P");
		keyMap.put("N", "Q");
		keyMap.put("7", "R");
		keyMap.put("T", "S");
		keyMap.put("F", "T");
		keyMap.put("O", "U");
		keyMap.put("2", "V");
		keyMap.put("V", "W");
		keyMap.put("X", "X");
		keyMap.put("8", "Y");
		keyMap.put("R", "Z");
		keyMap.put("Z", "0");
		keyMap.put("K", "1");
		keyMap.put("4", "2");
		keyMap.put("W", "3");
		keyMap.put("S", "4");
		keyMap.put("A", "5");
		keyMap.put("L", "6");
		keyMap.put("U", "7");
		keyMap.put("M", "8");
		keyMap.put("Q", "9");
	}
		 
	public String licenseCheck() throws Exception{
		String key = dao.getPropertyName().trim();
		System.out.println("Key:"+key);
		return getLicenseStatus(key);
		 
	}
	
	  public String getLicenseStatus(String key) throws Exception
	{				
		//String key = dao.getPropertyName();
		
		if(key == null || key.isEmpty()){
			System.out.println("the key is not valid or the key is empty");
			return "Invalid";
		}
		else {			
			String qsyncvalidate = qsync.concat(qsync);
			String merckvalidate = qsync.concat(merck);
			String qsyncfromkey = "";
			String merckfromkey = "";
			
			char [] charArray = key.toCharArray();
			  for(int i=0; i<charArray.length; i++)				 
			{
				if(i==0 || i==1 || i==2 || i==3 || i==7){
					qsyncfromkey += charArray[i];
				}
				if(i==8 || i==12 || i==13 || i==15 || i==16){
					merckfromkey += charArray[i];
				}				
			 }
			    System.out.println(qsyncfromkey);
			    System.out.println(merckfromkey);
						
			qsyncmerckstatus = LicenseKeyService.qsyncmerckValidate(qsyncfromkey,merckfromkey,keyMap);
		}
		
			if(qsyncmerckstatus){
				System.out.println("The key is : " + key);
				String maindate = LicenseKeyService.getrequiredfields(key);
				System.out.println("The encrypted date is:" + maindate);
				String decrypteddate = LicenseKeyService.decrypt(maindate, keyMap);
				
				String status = LicenseKeyService.verify(decrypteddate);
				System.out.println("The License key is : " + status);
				return status;
			}
			else{
				return "Invalid";
			}

	}
	
	public Properties getProp() {
		return prop;
	}
	
private static boolean qsyncmerckValidate(String qsyncfromkey, String merckfromkey, Map<String,String> keyMap) throws IOException{
		
		/*FileReader reader = new FileReader("Licencekey.properties");
		prop = new Properties();
		prop.load(reader);*/
		boolean validlic = false;
		String qsynctemp = "";
		String mercktemp = "";
		
		//String[] arr1 = qsyncfromkey.split("");
		//String[] arr2 = merckfromkey.split("");
		
		 char [] charArray1 = qsyncfromkey.toCharArray();
		 char [] charArray2 = merckfromkey.toCharArray();		
		
		    for(int i=0; i<charArray1.length; i++)
		  {			
			System.out.println(keyMap.get(String.valueOf(charArray1[i])));
			System.out.println(charArray1[i]);
			qsynctemp += keyMap.get(String.valueOf(charArray1[i]));
		  } 	 
		   for(int i=0; i<charArray2.length; i++)
		 {
			System.out.println(keyMap.get(String.valueOf(charArray2[i])));
			System.out.println(charArray2[i]);
			mercktemp += keyMap.get(String.valueOf(charArray2[i]));
		 } 
	
		if(qsynctemp.equals(qsync) && mercktemp.equals(merck)){
			validlic = true;
		}
		
		return validlic;
		
	}
	
	   private static String verify(String decrypteddate) throws ParseException
	{
		
		String status = "";
		Date date = new Date();
		SimpleDateFormat dateformater = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		//cal.add(Calendar.DATE, 1);
		String current_date = dateformater.format(cal.getTime());
		System.out.println("current date is : " + current_date);
		int counter1 = 0;
		String[] check_expireyear = decrypteddate.split("/");
		
		for(String expirecheckyyyy : check_expireyear){
			counter1++;
			if(counter1 == 3){
				if(Integer.parseInt(expirecheckyyyy) >= year){
					if(Integer.parseInt(expirecheckyyyy) == year){
						int counter2 = 0;
						String[] check_expiremonth = decrypteddate.split("/");
						for(String expirecheckmm : check_expiremonth){
							counter2++;
							if(counter2 == 2){
								if(Integer.parseInt(expirecheckmm) >= month){
									if(Integer.parseInt(expirecheckmm) == month){
										int counter3 = 0;
										String[] check_expireday = decrypteddate.split("/");
										for(String expirecheckdd : check_expireday){
											counter3++;
											if(counter3 == 1){
												if(Integer.parseInt(expirecheckdd) >= day){
													if(Integer.parseInt(expirecheckdd) == day){
														status = "Expired";
														break;
													}
													else{
														status = "Valid";
														break;
													}
												}
												else{
													status = "Expired";
													break;
												}
											}
										}
									}
									else{
										status = "Valid";
										break;
									}
								}
								else{
									status = "Expired";
									break;
								}
							}
						}
					}
					else{
						status = "Valid";
						break;
					}
				}
				else{
					status = "Expired";
					break;
				}
			}
		}
		
		return status;
		
	}
	
	private static String decrypt(String cryptodate,Map<String,String> keyMap) throws IOException{
		
	//	InputStream input = null;
	//	String filename = "Licencekey.properties";
	//	input = LicenseKeyService.class.getClassLoader().getResourceAsStream(filename);
		//if(input == null){
	      //   System.out.println("Sorry, unable to find " + filename);
		   //  return "";
		//}	
		//prop.load(input);
		
		String decryptdate = "";
	//	FileReader reader = new FileReader("Licencekey.properties");
	//	prop = new Properties();
	//	prop.load(reader);
		
	//	String[] arr = cryptodate.trim().split("");
		char [] arr = cryptodate.trim().toCharArray();
		
		   for(int i=0; i<arr.length; i++)
		 {				
				if(i == 2 || i == 5){
					decryptdate += "/";
				}
				else if(i == 6 || i == 7){
					decryptdate += arr[i];
				}
				else{
					//System.out.println("from decrypt - " + arr[i] + "from map - " + keyMap.get(arr[i]));
					decryptdate += keyMap.get(String.valueOf(arr[i]));
				}
		 }		
		
		System.out.println("The decrypted date is : " + decryptdate);		
		return decryptdate;		
	}
	
	private static String getrequiredfields(String valkey){
		String maindate = "";
		try{
			String[] seperate_fields = valkey.split("-");
			String datemm = null;
			String datedd = null;
			String dateyyyy = null;
            String yy = "20";
            
            for(int i=0; i<seperate_fields.length; i++){
            	if(i == 1){
            		datemm = seperate_fields[i];
            	}
            	if(i == 2){
            		datedd = seperate_fields[i];
            	}
            	if(i == 3){
            		dateyyyy = seperate_fields[i];
            	}
            }
            
            for(int i=0 ; i<datemm.length(); i++){
            	if(i<=1){
            		maindate += datemm.charAt(i);
            	}
            }
            maindate += "/";
            
            for(int i=0 ; i<datedd.length(); i++){
            	if(i<=1){
            		maindate += datedd.charAt(i);
            	}
            }
            maindate += "/";
            
            maindate += yy;
            for(int i=0 ; i<dateyyyy.length(); i++){
            	if(i>1){
            		maindate += dateyyyy.charAt(i);
            	}
            }
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		return maindate;
		
	}
	
	

}
