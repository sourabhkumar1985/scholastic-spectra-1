package com.relevance.prism.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.relevance.prism.service.SolenisService;
import com.relevance.prism.util.PrismHandler;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/solenis")
public class SolenisResource extends BaseResource{
	public Logger _logger = Logger.getLogger(this.getClassName());
	
	@POST
	@Path("/processcpusimulation")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String processcpusimulation(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,@FormDataParam("type") String type) throws IOException {
		String response = null;
		/*org.json.JSONArray profileConfigArray = null;
		JSONObject profileConfigObject = null;*/
		String tDir = System.getProperty("java.io.tmpdir");
		String uploadedFileLocation = tDir + File.separator + fileDetail.getFileName();
		try {
			File tempFile = new File(uploadedFileLocation);
			tempFile.delete();
			writeToFile(uploadedInputStream, uploadedFileLocation);
			SolenisService solenisService = new SolenisService();
			response = solenisService.processcpusimulation(uploadedFileLocation,type);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	private void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			try {
				OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
				int read = 0;
				byte[] bytes = new byte[1024];
				//out = new FileOutputStream(new File(uploadedFileLocation));
				while ((read = uploadedInputStream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				out.flush();
				out.close();				
			}catch(Exception ex){
				PrismHandler.handleException(ex);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		}
}
