package com.relevance.prism.dao;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.NumberFormat;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import com.google.gson.Gson;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.DataTable;
import com.relevance.prism.data.Report;
import com.relevance.prism.data.Role;
import com.relevance.prism.data.SecondaryRole;
import com.relevance.prism.data.User;
import com.relevance.prism.data.UserAdditionalDetails;
import com.relevance.prism.data.UserSecondaryRole;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class UserDao extends BaseDao {
	public Logger _logger = Logger.getLogger(this.getClassName());

	public Role getTheme(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Role role = null;
		String param = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		try {
			if (obj != null) {
				_logger.info("Fetching User Object for : " + param);
				if (obj.length > 0) {
					param = (String) obj[0];
					query = "select r.themename,r.menuontop,u.primaryrole,r.displayname,r.logo,r.favicon,r.icon,r.widget_class,r.widget_color, r.toprighticon from roles r inner join users u on r.name = u.primaryrole where username='"
							+ param + "'";
				}
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				System.out.println(query);
				rs = stmt.executeQuery(query);
				while (rs.next()) {
					role = new Role();
					role.setThemeName(rs.getString("themename"));
					role.setDisplayName(rs.getString("displayname"));

					role.setMenuOnTop(rs.getInt("menuontop"));
					role.setName(rs.getString("primaryrole"));
					role.setLogo(rs.getString("logo"));
					role.setFavicon(rs.getString("favicon"));
					role.setWidgetClass(rs.getString("widget_class"));
					role.setWidgetColor(rs.getString("widget_color"));
					role.setIcon(rs.getString("icon"));
					role.setTopRightIcon(rs.getString("topRightIcon"));

				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return role;
	}

	public String insertUser(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		String[] rolesArray = null;
		long maxUserRoleId = 0;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		UserAdditionalDetails additionalDetails = null;
		String insertQuery = "";
		boolean alreadyExists = false;
		if (obj != null) {
			param = (User) obj[0];
			rolesArray = param.getRolesAsArray();
			_logger.info("Inserting  Values for User Object : " + param);
			try {
				// added code to check for duplicate users
				String selectMaxQuery = "select max(user_role_id) from user_roles";
				String selectDuplicateQuery = "select username, status, remarks from users where username  = '"
						+ param.getUsername() + "'";

				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				System.out.println(selectDuplicateQuery);
				rs = stmt.executeQuery(selectDuplicateQuery);
				if (rs != null) {
					if (rs.next()) {
						String status = rs.getString("status");
						String username = rs.getString("username");
						if (status != null && status.equalsIgnoreCase("rejected")) {

							// throw new Exception("User is rejected due to " + rs.getString("remarks"));
//							insertQuery = "update users set status = 'Pending', remarks = '' where username = '" + rs.getString("username") + "'";
							insertQuery = "delete from users where username = '" + username + "'";
							stmt.close();
							connection.close();
							connection = getPostGresDBConnection();
							stmt = connection.createStatement();
							System.out.println(insertQuery);
							stmt.executeUpdate(insertQuery);
							if (param.getAdditionalDetails() != null) {
								insertQuery = "delete from user_additional_details where username = '" + username + "'";
								// stmt.close();
								// connection.close();
								// connection = getPostGresDBConnection();
								stmt = connection.createStatement();
								System.out.println(insertQuery);
								stmt.executeUpdate(insertQuery);
								insertQuery = "";
							}
							if (!rs.isClosed()) {
								rs.close();
							}
							if (!stmt.isClosed()) {
								stmt.close();
							}
							if (!connection.isClosed()) {
								connection.close();
							}
							// alreadyExists = true;
						} else if (status != null && status.equalsIgnoreCase("pending")) {
							throw new Exception("Pending User already exists");
						} else {
							throw new Exception("User already exists");
						}

					}

				}
				if (!alreadyExists) {
					rs.close();
					stmt.close();
					connection.close();
				}

				rs = null;
				stmt = null;
				connection = null;

				insertQuery = (!"".equalsIgnoreCase(insertQuery)) ? insertQuery
						: "insert into users (username, password, enabled, menuontop, primaryrole, emailid, timezone, firstname, lastname, dateformat,business_role, status, region, remarks) values ('"
								+ param.getUsername().toLowerCase() + "','" + param.getPassword() + "','"
								+ param.getEnabled() + "','" + param.getMenuOnTop() + "','" + param.getPrimaryRole()
								+ "','" + param.getEmail() + "','" + param.getTimezone() + "','" + param.getFirstName()
								+ "','" + param.getLastName() + "','" + param.getDateformat() + "','"
								+ param.getBusinessRole() + "','" + param.getStatus() + "','" + param.getRegion()
								+ "','" + param.getRemarks() + "')";
				if (param.getThemeName() != null && !param.getThemeName().isEmpty()) {
					insertQuery = (!"".equalsIgnoreCase(insertQuery)) ? insertQuery
							: "insert into users (username, password, enabled, themename, menuontop, primaryrole, emailid, timezone, firstname, lastname, dateformat,business_role, status, region, remarks) values('"
									+ param.getUsername().toLowerCase() + "','" + param.getPassword() + "','"
									+ param.getEnabled() + "','" + param.getThemeName() + "','" + param.getMenuOnTop()
									+ "','" + param.getPrimaryRole() + "','" + param.getEmail() + "','"
									+ param.getTimezone() + "','" + param.getFirstName() + "','" + param.getLastName()
									+ "','" + param.getDateformat() + "','" + param.getBusinessRole() + "','"
									+ param.getStatus() + "','" + param.getRegion() + "','" + param.getRemarks() + "')";
				}
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				stmt.executeUpdate(insertQuery);
				System.out.println(selectMaxQuery);
				rs = stmt.executeQuery(selectMaxQuery);
				while (rs.next()) {
					maxUserRoleId = rs.getLong(1);
				}
				if (!alreadyExists) {
					for (String role : rolesArray) {
						maxUserRoleId = maxUserRoleId + 1;
						String inertUserRoleQuery = "insert into user_roles (user_role_id, username, role) values("
								+ maxUserRoleId + ",'" + param.getUsername() + "','" + role + "')";
						System.out.println(inertUserRoleQuery);
						stmt.executeUpdate(inertUserRoleQuery);
					}
					// Check for user additional Details.
					if (param.getAdditionalDetails() != null) {
						additionalDetails = param.getAdditionalDetails();

						String deleteAdditionalQuery = "delete from user_additional_details where username = '"
								+ param.getUsername().toLowerCase() + "'";
						System.out.println(deleteAdditionalQuery);
						stmt.executeUpdate(deleteAdditionalQuery);

						String insertAdditionalUserQuery = "insert into user_additional_details (username, common_name, title,company_name, department, speciality) values (";
						insertAdditionalUserQuery += "'" + param.getUsername().toLowerCase() + "','"
								+ additionalDetails.getCommonName() + "','" + additionalDetails.getTitle() + "','"
								+ additionalDetails.getCompanyName() + "','" + additionalDetails.getDepartment()
								+ "', '" + additionalDetails.getSpeciality() + "');";
						System.out.println(insertAdditionalUserQuery);
						stmt.executeUpdate(insertAdditionalUserQuery);
					}
				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted user in table\"}";
	}

	public User getUser(User param) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		// User param = null;
		User userToStore = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
		// if (obj != null) {
		userToStore = new User();
		// param = (User) obj[0];
		// _logger.info("Fetching User Object for : " + param);
		try {
			userName = param.getUsername();
			Log.Info(userName + "  *************");
			StringBuilder query = new StringBuilder(
					"select username, firstname, lastname, emailid, timezone, dateformat, enabled, themename, menuontop, primaryrole,business_role,status,region,remarks from users ");
			if (userName != null) {
				query.append("where username='").append(userName).append("'");
			} else if (param.getId() > 0) {
				query.append("where id='").append(param.getId()).append("'");
			}

			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			Log.Info(query.toString() + "   ***************");
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				userToStore.setUsername(rs.getString("username"));
				userToStore.setPassword("********");
				userToStore.setFirstName(rs.getString("firstname"));
				userToStore.setLastName(rs.getString("lastname"));
				userToStore.setEmail(rs.getString("emailid"));
				userToStore.setTimezone(rs.getString("timezone"));
				userToStore.setDateformat(rs.getString("dateformat"));
				userToStore.setEnabled(rs.getLong("enabled"));
				userToStore.setThemeName(rs.getString("themename"));
				userToStore.setMenuOnTop(rs.getLong("menuontop"));
				userToStore.setPrimaryRole(rs.getString("primaryRole"));
				userToStore.setBusinessRole(rs.getString("business_role"));
				userToStore.setStatus(rs.getString("status"));
				userToStore.setRegion(rs.getString("region"));
				userToStore.setRemarks(rs.getString("remarks"));
			}
			rs.close();
			rs = stmt.executeQuery(
					"SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_name = 'user_additional_details') AS \"exists\";");
			if (rs.next() && rs.getBoolean("exists")) {
				rs.close();
				StringBuilder additionalDetailsQuery = new StringBuilder(
						"select common_name, title, company_name, department,user_remarks,status_remarks, speciality, last_login, notification_closed_date from user_additional_details");
				additionalDetailsQuery.append(" where username = '" + userToStore.getUsername() + "'");
				Log.Info(additionalDetailsQuery.toString() + "  ****************");
				rs = stmt.executeQuery(additionalDetailsQuery.toString());
				if (rs.next()) {
					UserAdditionalDetails additionalDetails = new UserAdditionalDetails();
					additionalDetails.setTitle(rs.getString("title"));
					additionalDetails.setCompanyName(rs.getString("company_name"));
					additionalDetails.setDepartment(rs.getString("department"));
					additionalDetails.setUserRemarks(rs.getString("user_remarks"));
					additionalDetails.setStatusRemarks(rs.getString("status_remarks"));
					additionalDetails.setSpeciality(rs.getString("speciality"));
					additionalDetails.setLastUpdatedDate(rs.getString("last_login"));
					additionalDetails.setNotificationCloedDate(rs.getString("notification_closed_date"));
					userToStore.setAdditionalDetails(additionalDetails);
				}
			}
			List<String> roles = new ArrayList<>();
			rs.close();
			StringBuilder roleQuery = new StringBuilder("select username, role from user_roles where username = '");
			roleQuery.append(userName).append("'");
			Log.Info(roleQuery.toString() + "  ***************");
			rs = stmt.executeQuery(roleQuery.toString());
			while (rs.next()) {
				roles.add(rs.getString("role"));
			}
			rs.close();
			userToStore.setRoles(roles);
			Log.Info(userToStore.getUsername() + "  ***************");
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		// }
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userToStore;
	}

	public String updateUserLastLoginDate(UserAdditionalDetails param) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
		try {
			userName = param.getUserName();
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(
					"SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_name = 'user_additional_details') AS \"exists\";");
			if (rs.next() && rs.getBoolean("exists")) {
				rs.close();
				StringBuilder query = new StringBuilder("update user_additional_details set last_login= now()")
						.append(" where username = '").append(userName).append("'");
				System.out.println(query.toString());
				stmt.executeUpdate(query.toString());
			}

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated user_additional_details in DB.\"}";
	}

	public User sendCredentialsToUser(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		User userToStore = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
		if (obj != null) {
			userToStore = new User();
			param = (User) obj[0];
			userName = param.getUsername();
			_logger.info("Fetching User Object for : " + param);
			try {
				StringBuilder query = new StringBuilder(
						"select username, password, emailid from users where username='").append(userName).append("'");
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				System.out.println(query.toString());
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					userToStore.setUsername(rs.getString("username"));
					userToStore.setPassword(rs.getString("password"));
					userToStore.setEmail(rs.getString("emailid"));

				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userToStore;
	}

	public String updateUser(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		Connection connection = null;
		Statement stmt = null;
		long maxUserRoleId = 0;
		String[] rolesArray = null;
		ResultSet rs = null;
		String userName;
		UserAdditionalDetails additionalDetails = null;
		if (obj != null) {
			param = (User) obj[0];
			userName = param.getUsername();
			rolesArray = param.getRolesAsArray();
			_logger.info("Updating the user in DB : " + param);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String deleteQueryForEnabled = "update users set status='" + param.getStatus() + "',remarks='"
						+ param.getRemarks() + "',region='" + param.getRegion() + "',enabled=" + param.getEnabled()
						+ ",menuontop=" + param.getMenuOnTop() + ",firstname='" + param.getFirstName() + "' ,lastname='"
						+ param.getLastName() + "' ,emailid='" + param.getEmail() + "' ,dateformat='"
						+ param.getDateformat() + "' ,timezone='" + param.getTimezone() + "' ,primaryrole='"
						+ param.getPrimaryRole() + "' ,business_role='" + param.getBusinessRole()
						+ "' where username = '" + userName + "'";
				if (param.getThemeName() != null && !param.getThemeName().isEmpty()) {
					String deleteQueryForTheme = "update users set themename='" + param.getThemeName()
							+ "' where username = '" + userName + "'";
					System.out.println(deleteQueryForTheme);
					stmt.executeUpdate(deleteQueryForTheme);
				}
				System.out.println(deleteQueryForEnabled);
				stmt.executeUpdate(deleteQueryForEnabled);

				String userRolesTableDeleteQuery = "delete from user_roles where username = '" + userName + "'";
				System.out.println(userRolesTableDeleteQuery);
				stmt.executeUpdate(userRolesTableDeleteQuery);
				String selectMaxQuery = "select max(user_role_id) from user_roles";
				System.out.println(selectMaxQuery);
				rs = stmt.executeQuery(selectMaxQuery);
				while (rs.next()) {
					maxUserRoleId = rs.getLong(1);
				}
				for (String role : rolesArray) {
					maxUserRoleId = maxUserRoleId + 1;
					String inertUserRoleQuery = "insert into user_roles (user_role_id, username, role) values("
							+ maxUserRoleId + ",'" + param.getUsername() + "','" + role + "')";
					System.out.println(inertUserRoleQuery);
					stmt.executeUpdate(inertUserRoleQuery);
				}
				// Check for user additional Details.
				if (param.getAdditionalDetails() != null) {
					additionalDetails = param.getAdditionalDetails();

					String updateAdditionalUserQuery = "update user_additional_details set" + " common_name = '"
							+ additionalDetails.getCommonName() + "', company_name = '"
							+ additionalDetails.getCompanyName() + "' , " + " department = '"
							+ additionalDetails.getDepartment() + "' , user_remarks = '"
							+ additionalDetails.getUserRemarks() + "' , " + "status_remarks = '"
							+ additionalDetails.getStatusRemarks() + "' , title = '" + additionalDetails.getTitle()
							+ "', speciality = '" + additionalDetails.getSpeciality() + "' where username = '"
							+ param.getUsername() + "';";
					System.out.println(updateAdditionalUserQuery);
					stmt.executeUpdate(updateAdditionalUserQuery);
				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated user in DB.\"}";
	}

	public String resetPassword(String username, String password, String newpassword) throws Exception {
		User param = null;
		String response = null;
		String currentPasswoord = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			StringBuilder query = new StringBuilder("select password from users where username in('" + username + "')");
			System.out.println(query.toString());
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				currentPasswoord = rs.getString("password");
			}
			rs.close();
			if (password.equalsIgnoreCase(currentPasswoord)) {
				query = new StringBuilder(
						"update users set password='" + newpassword + "' where username in ('" + username + "')");
				System.out.println(query.toString());
				stmt.executeUpdate(query.toString());
				response = "{\"success\" : \"Password reset successfully.\"}";
			} else {
				response = "{\"error\" : \"Password doesnot  match.\"}";
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		return response;
	}

	public String updateUserPrimaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
		if (obj != null) {
			param = (User) obj[0];
			_logger.info("Updating the user Primary role in DB : " + param);
			try {
				userName = param.getUsername();
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				StringBuilder query = new StringBuilder("update users set primaryrole='").append(param.getPrimaryRole())
						.append("' where username = '").append(userName).append("'");
				System.out.println(query.toString());
				stmt.executeUpdate(query.toString());

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated user in DB.\"}";
	}

	public String deleteUser(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		User userToStore = null;
		Connection connection = null;
		Statement stmt = null;
		String userName;
		ResultSet rs = null;
		if (obj != null) {
			userToStore = new User();
			param = (User) obj[0];
			userName = param.getUsername();
			_logger.info("Deleting the user from DB : " + param);
			userToStore.setUsername(userName);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String userRolesTableDeleteQuery = "delete from user_roles where username = '" + userName + "'";
				String usersTableDeleteQuery = "delete from users where username = '" + userName + "'";
				System.out.println(userRolesTableDeleteQuery);
				stmt.executeUpdate(userRolesTableDeleteQuery);
				System.out.println(usersTableDeleteQuery);
				stmt.executeUpdate(usersTableDeleteQuery);
				rs = stmt.executeQuery(
						"SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_name = 'user_additional_details') as \"exists\";");
				if (rs.next()) {
					rs.close();
					StringBuilder additionalDetailsDeleteQuery = new StringBuilder(
							"delete from user_additional_details where username = '" + userToStore.getUsername() + "'");
					System.out.println(additionalDetailsDeleteQuery.toString());
					stmt.executeUpdate(additionalDetailsDeleteQuery.toString());
					rs.close();
				}

			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully deleted the user from DB\"}";
	}

	public User insertUserRoles(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		User userToStore = null;
		String role = null;
		long maxUserRoleId = 0;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
		if (obj != null) {
			userToStore = new User();
			param = (User) obj[0];
			userName = param.getUsername();
			role = param.getRoles().get(0);
			_logger.info("Inserting  Values for User Roles : " + param);
			try {
				String selectMaxQuery = "select max(user_role_id) from user_roles";
				StringBuilder selectUserQuery = new StringBuilder(
						"select username, password, enabled,menuontop from users where username='").append(userName)
								.append("'");
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				System.out.println(selectMaxQuery);
				rs = stmt.executeQuery(selectMaxQuery);
				while (rs.next()) {
					maxUserRoleId = rs.getLong(1);
				}
				rs.close();
				maxUserRoleId = maxUserRoleId + 1;
				StringBuilder insertQuery = new StringBuilder(
						"insert into user_roles (user_role_id, username, role) values(").append(maxUserRoleId)
								.append(",'").append(param.getUsername()).append("','").append(role).append("')");
				System.out.println(insertQuery.toString());
				stmt.executeUpdate(insertQuery.toString());
				System.out.println(selectUserQuery.toString());
				rs = stmt.executeQuery(selectUserQuery.toString());
				while (rs.next()) {
					userToStore.setUsername(rs.getString("username").trim());
					userToStore.setPassword(rs.getString("password").trim());
					userToStore.setEnabled(rs.getLong("enabled"));
					userToStore.setMenuOnTop(rs.getLong("menuontop"));
				}
				rs.close();
				List<String> roles = new ArrayList<String>();
				StringBuilder roleQuery = new StringBuilder("select username, role from user_roles where username = '")
						.append(userName).append("'");
				System.out.println(roleQuery.toString());
				rs = stmt.executeQuery(roleQuery.toString());
				while (rs.next()) {
					roles.add(rs.getString("role").trim());
				}
				rs.close();
				userToStore.setRoles(roles);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userToStore;
	}

	public String deleteUserRoles(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		String[] roles = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (User) obj[0];
			roles = param.getRolesAsArray();
			_logger.info("Deleting  Roles for User : " + param.getUsername());
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				for (String role : roles) {
					String deleteUserRoleQuery = "delete from user_roles where username ='" + param.getUsername()
							+ "' and role ='" + role + "'";
					System.out.println(deleteUserRoleQuery);
					stmt.executeUpdate(deleteUserRoleQuery);
				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully role(s) deleted for the user from DB\"}";
	}

	public List<Role> getUserRoles(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		ArrayList<Role> roleList = new ArrayList<>();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String userName;
		if (obj != null) {
			param = (User) obj[0];
			userName = param.getUsername();
			_logger.info("selecting the Roles for User Object : " + param);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				StringBuilder roleQuery = new StringBuilder(
						"select displayname, name, favicon, logo, themename, menuontop,icon,displayorder,widget_class,widget_color from roles where name in (select distinct role from user_roles where username = '")
								.append(userName).append("') order by displayorder");
				System.out.println(roleQuery.toString());
				rs = stmt.executeQuery(roleQuery.toString());
				while (rs.next()) {
					Role role = new Role();
					role.setDisplayName(rs.getString("displayname"));
					role.setName(rs.getString("name"));
					role.setFavicon(rs.getString("favicon"));
					role.setLogo(rs.getString("logo"));
					role.setThemeName(rs.getString("themename"));
					role.setMenuOnTop(rs.getInt("menuontop"));
					role.setIcon(rs.getString("icon"));
					role.setDisplayOrder(rs.getInt("displayorder"));
					role.setWidgetClass(rs.getString("widget_class"));
					role.setWidgetColor(rs.getString("widget_color"));
					roleList.add(role);
				}
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, rs, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return roleList;
	}

	public List<Role> getRoles(ArrayList<Role> roles) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		List<Role> roleList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		Boolean isAdmin = false;
		String rolesString = "";
		if (roles != null) {
			for (Role item : roles) {
				String userRole = item.getName();
				if ("ROLE_ADMIN".equalsIgnoreCase(userRole)) {
					isAdmin = true;
					break;
				}
				if (!rolesString.isEmpty())
					rolesString += ",";
				rolesString += "'" + userRole + "'";
			}
		} else {
			isAdmin = true;
		}
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			Role role = null;
			StringBuilder roleQuery = new StringBuilder(
					"SELECT displayname, name, favicon, logo, themename, menuontop, login_content,icon,displayorder,widget_class,widget_color,fixedheader,fixednavigation,fixedribbon,sso, signup,parentrole from roles");
			if (!isAdmin)
				roleQuery.append(" WHERE name in (").append(rolesString).append(")");
			roleQuery.append(" ORDER BY displayorder");
			System.out.println(roleQuery.toString());
			rs = stmt.executeQuery(roleQuery.toString());
			while (rs.next()) {
				role = new Role();
				role.setDisplayName(rs.getString("displayname"));
				role.setName(rs.getString("name"));
				role.setFavicon(rs.getString("favicon"));
				role.setLogo(rs.getString("logo"));
				role.setThemeName(rs.getString("themename"));
				role.setMenuOnTop(rs.getInt("menuontop"));
				role.setLoginContent(rs.getString("login_content"));
				role.setIcon(rs.getString("icon"));
				role.setDisplayOrder(rs.getInt("displayorder"));
				role.setWidgetClass(rs.getString("widget_class"));
				role.setWidgetColor(rs.getString("widget_color"));
				role.setFixedHeader(rs.getInt("fixedheader"));
				role.setFixedNavigation(rs.getInt("fixednavigation"));
				role.setFixedRibbon(rs.getInt("fixedribbon"));
				role.setSso(rs.getInt("sso"));
				role.setSignUp(rs.getInt("signup"));
				role.setParentRole(rs.getString("parentrole"));
				roleList.add(role);
			}

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return roleList;
	}

	public Role getRole(String roleName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Role role = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			StringBuilder roleQuery = new StringBuilder(
					"select displayname, name, favicon, logo, themename, menuontop, login_content, displayOrder, widget_class, icon, widget_color, fixedheader, fixednavigation, fixedribbon, sso, signup,parentrole ,toprighticon from roles where name = '")
							.append(roleName).append("'");
			System.out.println(roleQuery.toString());
			rs = stmt.executeQuery(roleQuery.toString());
			if (rs.next()) {
				role = new Role();
				role.setDisplayName(rs.getString("displayname"));
				role.setName(rs.getString("name"));
				role.setFavicon(rs.getString("favicon"));
				role.setLogo(rs.getString("logo"));
				role.setThemeName(rs.getString("themename"));
				role.setMenuOnTop(rs.getInt("menuontop"));
				role.setLoginContent(rs.getString("login_content"));
				role.setDisplayOrder(rs.getInt("displayOrder"));
				role.setWidgetClass(rs.getString("widget_class"));
				role.setWidgetColor(rs.getString("widget_color"));
				role.setFixedHeader(rs.getInt("fixedheader"));
				role.setFixedNavigation(rs.getInt("fixednavigation"));
				role.setFixedRibbon(rs.getInt("fixedribbon"));
				role.setIcon(rs.getString("icon"));
				role.setSso(rs.getInt("sso"));
				role.setSignUp(rs.getInt("signup"));
				role.setParentRole(rs.getString("parentrole"));
				role.setTopRightIcon(rs.getString("toprighticon"));
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return role;
	}

	public String insertRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (User) obj[0];
			_logger.info("Inserting  Values for Role Object : " + param);
			try {
				String insertQuery = "insert into roles (displayname, name, favicon, logo,login_content, themeName, menuOnTop, displayOrder, widget_class, icon, widget_color, fixedheader, fixednavigation, fixedribbon, sso, signup, parentrole , toprighticon) values('"
						+ param.getDisplayname() + "','" + param.getName() + "','" + param.getFavicon() + "','"
						+ param.getLogo() + "','" + param.getLoginContent() + "','" + param.getThemeName() + "','"
						+ param.getMenuOnTop() + "','" + param.getDisplayOrder() + "','" + param.getWidgetClass()
						+ "','" + param.getIcon() + "','" + param.getWidgetColor() + "','" + param.getFixedHeader()
						+ "','" + param.getFixedNavigation() + "','" + param.getFixedRibbon() + "','" + param.getSso()
						+ "','" + param.getSignUp() + "','" + param.getParentRole() + "','" + param.getTopRightIcon()
						+ "')";
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				System.out.println(insertQuery);
				stmt.executeUpdate(insertQuery);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted role in table\"}";
	}

	public String updateRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (User) obj[0];
			_logger.info("Updating the role in DB : " + param);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String updateQueryForEnabled = "update roles set displayname= '" + param.getDisplayname()
						+ "',favicon= '" + param.getFavicon() + "',logo= '" + param.getLogo() + "',login_content= '"
						+ param.getLoginContent() + "',themeName= '" + param.getThemeName() + "',menuOnTop= '"
						+ param.getMenuOnTop() + "',displayOrder= '" + param.getDisplayOrder() + "',widget_class= '"
						+ param.getWidgetClass() + "',icon= '" + param.getIcon() + "',widget_color= '"
						+ param.getWidgetColor() + "',fixedheader= '" + param.getFixedHeader() + "',fixednavigation= '"
						+ param.getFixedNavigation() + "',fixedribbon= '" + param.getFixedRibbon() + "',sso= '"
						+ param.getSso() + "',signup= '" + param.getSignUp() + "',parentrole= '" + param.getParentRole()
						+ "',toprighticon= '" + param.getTopRightIcon() + "' where name = '" + param.getName() + "'";
				stmt.executeUpdate(updateQueryForEnabled);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated roles in DB.\"}";
	}

	public List<SecondaryRole> getSecondaryRoles(String email) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		List<SecondaryRole> secondaryRoleList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			SecondaryRole secondaryRole = null;
			StringBuilder roleQuery = new StringBuilder(
					"select section_id, section_name, facet_name,group_ids,group_names,section_delimeter from user_secondary_roles where email = '")
							.append(email).append("'");
			System.out.println(roleQuery.toString());
			rs = stmt.executeQuery(roleQuery.toString());
			if (!rs.next()) {
				rs.close();
				String distinctRoleQueryselect = "select distinct section_id, section_name, facet_name from user_secondary_roles";
				System.out.println(distinctRoleQueryselect);
				rs = stmt.executeQuery(distinctRoleQueryselect);
				while (rs.next()) {
					secondaryRole = new SecondaryRole();
					secondaryRole.setSectionId(rs.getInt("section_id"));
					secondaryRole.setSectionName(rs.getString("section_name"));
					secondaryRole.setFacetName(rs.getString("facet_name"));
					secondaryRole.setGroupIds("");
					secondaryRole.setGroupNames("");
					secondaryRole.setSectionDelimeter("");
					secondaryRoleList.add(secondaryRole);
				}
			} else {
				do {
					secondaryRole = new SecondaryRole();
					secondaryRole.setSectionId(rs.getInt("section_id"));
					secondaryRole.setSectionName(rs.getString("section_name"));
					secondaryRole.setFacetName(rs.getString("facet_name"));
					secondaryRole.setGroupIds(rs.getString("group_ids"));
					secondaryRole.setGroupNames(rs.getString("group_names"));
					secondaryRole.setSectionDelimeter(rs.getString("section_delimeter"));
					secondaryRoleList.add(secondaryRole);
				} while (rs.next());
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return secondaryRoleList;
	}

	public List<UserSecondaryRole> getUserSecondaryRoles(String app, String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		List<UserSecondaryRole> userSecondaryRoleList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			UserSecondaryRole userSecondaryRole = null;
			String userSecondaryRoleQuery = "select user_secondary_role_id, username, secondary_role, datasource from user_secondary_roles where username = '"
					+ username + "' and secondary_role in (select secondary_role from app_secondary_roles where app = '"
					+ app + "')";
			System.out.println(userSecondaryRoleQuery);
			rs = stmt.executeQuery(userSecondaryRoleQuery);
			while (rs.next()) {
				userSecondaryRole = new UserSecondaryRole();
				userSecondaryRole.setUserSecondaryRoleId(rs.getInt("user_secondary_role_id"));
				userSecondaryRole.setUserName(rs.getString("username"));
				userSecondaryRole.setSecondaryRole(rs.getString("secondary_role"));
				userSecondaryRole.setDataSource(rs.getString("datasource"));
				userSecondaryRoleList.add(userSecondaryRole);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userSecondaryRoleList;
	}

	public List<UserSecondaryRole> getDataSourcesForUser(String app, String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		List<UserSecondaryRole> userSecondaryRoleList = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			UserSecondaryRole userSecondaryRole = null;
			StringBuilder userSecondaryRoleQuery = new StringBuilder(
					"select user_secondary_role_id, username, secondary_role, datasource from user_secondary_roles where username = '")
							.append(username)
							.append("' and secondary_role in (select secondary_role from app_secondary_roles where app = '")
							.append(app).append("')");
			System.out.println(userSecondaryRoleQuery.toString());
			rs = stmt.executeQuery(userSecondaryRoleQuery.toString());
			while (rs.next()) {
				userSecondaryRole = new UserSecondaryRole();
				userSecondaryRole.setUserSecondaryRoleId(rs.getInt("user_secondary_role_id"));
				userSecondaryRole.setUserName(rs.getString("username"));
				userSecondaryRole.setSecondaryRole(rs.getString("secondary_role"));
				userSecondaryRole.setDataSource(rs.getString("datasource"));
				userSecondaryRoleList.add(userSecondaryRole);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userSecondaryRoleList;
	}

	public String insertUserSecondaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserSecondaryRole param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (UserSecondaryRole) obj[0];
			_logger.info("Inserting  Values for User Secondary Role Object : " + param);
			try {
				String insertQuery = "insert into user_secondary_roles (username, secondary_role, datasource) values('"
						+ param.getUserName() + "','" + param.getSecondaryRole() + "','" + param.getDataSource() + "')";
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				System.out.println(insertQuery);
				stmt.executeUpdate(insertQuery);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted User Secondary Role in table\"}";
	}

	public String updateUserSecondaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserSecondaryRole param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (UserSecondaryRole) obj[0];
			_logger.info("Updating the User Secondary Role in DB : " + param);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String updateQueryForUserSecondaryRole = "update user_secondary_roles set username = '"
						+ param.getUserName() + "',secondary_role= '" + param.getSecondaryRole() + "',datasource= '"
						+ param.getDataSource() + "' where user_secondary_role_id = " + param.getUserSecondaryRoleId();
				System.out.println(updateQueryForUserSecondaryRole);
				stmt.executeUpdate(updateQueryForUserSecondaryRole);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully updated User Secondary Role in DB.\"}";
	}

	public String deleteUserSecondaryRole(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UserSecondaryRole param = null;
		Connection connection = null;
		Statement stmt = null;
		if (obj != null) {
			param = (UserSecondaryRole) obj[0];
			_logger.info("Deleting the User Secondary Role in DB : " + param);
			try {
				connection = getPostGresDBConnection();
				stmt = connection.createStatement();
				String updateQueryForSecondaryRole = "delete from user_secondary_roles where user_secondary_role_id = "
						+ param.getUserSecondaryRoleId();
				System.out.println(updateQueryForSecondaryRole);
				stmt.executeUpdate(updateQueryForSecondaryRole);
			} catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			} finally {
				PrismHandler.handleFinally(connection, null, stmt);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\" : \"successfully deleted User Secondary Role from DB.\"}";
	}

	public List<User> getUserList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<User> userList = new ArrayList<>();
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		boolean isAdditionalTableExist = false;
		try {
			String query = "select a.username, a.firstname,a.emailid, a.lastname, a.enabled, a.themename,a.menuontop,a.business_role,a.status, a.remarks, a.region ,b.displayname as primaryrole from users a inner join roles b on a.primaryrole = b.name";
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();

			rs = stmt.executeQuery(
					"SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_name = 'user_additional_details') as \"exists\";");
			if (rs.next() && rs.getBoolean("exists")) {
				isAdditionalTableExist = true;
				query = "select a.username, a.firstname,a.emailid, a.lastname, a.enabled, a.themename,a.menuontop,a.business_role,a.status, a.remarks, a.region ,b.displayname as primaryrole , u.common_name, u.title, u.company_name, u.department, u.user_remarks, u.status_remarks, u.speciality from users a inner join roles b on a.primaryrole = b.name left join user_additional_details u on a.username = u.username;";
				rs.close();
			}
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				User userToStore = null;
				userToStore = new User();
				String userName = rs.getString("username");
				userToStore.setUsername(userName);
				userToStore.setPassword("********");
				String firstname = rs.getString("firstname");
				userToStore.setFirstName(firstname);
				String emailId = rs.getString("emailid");
				userToStore.setEmail(emailId);
				String lastname = rs.getString("lastname");
				userToStore.setLastName(lastname);
				userToStore.setEnabled(rs.getLong("enabled"));
				userToStore.setThemeName(rs.getString("themename"));
				userToStore.setMenuOnTop(rs.getLong("menuontop"));
				userToStore.setPrimaryRole(rs.getString("primaryrole"));
				userToStore.setBusinessRole(rs.getString("business_role"));
				userToStore.setStatus(rs.getString("status"));
				userToStore.setRegion(rs.getString("region"));
				userToStore.setRemarks(rs.getString("remarks"));
				if (isAdditionalTableExist) {
					UserAdditionalDetails additionalDetails = new UserAdditionalDetails();
					additionalDetails.setCommonName(rs.getString("common_name"));
					additionalDetails.setCompanyName(rs.getString("company_name"));
					additionalDetails.setDepartment(rs.getString("department"));
					additionalDetails.setStatusRemarks(rs.getString("status_remarks"));
					additionalDetails.setTitle(rs.getString("title"));
					additionalDetails.setUserRemarks(rs.getString("user_remarks"));
					additionalDetails.setSpeciality(rs.getString("speciality"));
					userToStore.setAdditionalDetails(additionalDetails);
				}
				userList.add(userToStore);
			}

			rs.close();

			stmt.close();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userList;
	}

	public String getUserRoleActionList(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User param = null;
		ResultSet rs = null;
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmt = connection.createStatement();
		Gson gson = new Gson();
		List<Action> menus = new ArrayList<>();
		String userName;
		param = (User) obj[0];

		_logger.info("selecting the Roles for User Object : " + param);
		try {
			if (obj != null) {
				userName = param.getUsername();
				List<String> roles = new ArrayList<>();

				StringBuilder roleQuery = new StringBuilder("select  primaryrole from users where username = '")
						.append(userName).append("'");
				System.out.println(roleQuery.toString());
				rs = stmt.executeQuery(roleQuery.toString());
				while (rs.next()) {
					roles.add(rs.getString("primaryrole"));
				}
				StringBuilder subroleQuery = new StringBuilder(
						"select name from roles inner join user_roles on roles.name = user_roles.role	where username = '")
								.append(userName).append("' and parentrole = '").append(roles.get(0)).append("'");
				ResultSet innerResultSet = stmt.executeQuery(subroleQuery.toString());
				while (innerResultSet.next()) {
					roles.add(innerResultSet.getString("name"));
				}
				if (innerResultSet != null)
					innerResultSet.close();
				menus = getActions(roles, userName);
			}
			if (!menus.isEmpty())
				return gson.toJson(menus);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}

	private List<Action> getActions(List<String> roles, String userName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet rsMenu = null;
		List<String> allRoles = new ArrayList<>();
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmtMenu = connection.createStatement();
		LinkedHashMap<Integer, Action> actionMap = new LinkedHashMap<>();
		Action root = null;
		try {

			StringBuilder allroleQuery = new StringBuilder("select role from user_roles where username = '")
					.append(userName).append("'");
			ResultSet innerResultSet = stmtMenu.executeQuery(allroleQuery.toString());
			while (innerResultSet.next()) {
				allRoles.add(innerResultSet.getString("role"));
			}
			StringBuilder businessRoleQuery = new StringBuilder("select business_role from users where username = '")
					.append(userName).append("'");
			ResultSet businessResultSet = stmtMenu.executeQuery(businessRoleQuery.toString());
			while (businessResultSet.next()) {
				allRoles.add(businessResultSet.getString("business_role"));
			}
			String allRolesString = StringUtils.join(allRoles, "','");
			String innerWidgetQuery = "select action from widget_action_config_logs where role in ('" + allRolesString
					+ "') and enabled = 0";
			String rolesString = StringUtils.join(roles, "','");

			StringBuilder menuQuery = new StringBuilder();

			menuQuery.append(
					"select distinct action,role_actions.action_id,display_name,icon,enabled,sequence,parent,sources from role_actions ");
			menuQuery.append("inner join actions ");
			menuQuery.append(" on role_actions.action_id = actions.action_id where enabled = 1 ");
			menuQuery.append(" and role in ('" + rolesString + "') and action not in (" + innerWidgetQuery
					+ ") order by parent,sequence");

			rsMenu = stmtMenu.executeQuery(menuQuery.toString());
			while (rsMenu.next()) {
				Action action = new Action();
				action.setAction(rsMenu.getString("action"));
				action.setDisplayname(rsMenu.getString("display_name"));
				action.setIcon(rsMenu.getString("icon"));
				action.setSequence(rsMenu.getInt("sequence"));
				action.setId(rsMenu.getInt("action_id"));
				action.setEnabled(rsMenu.getInt("enabled"));
				action.setParent(rsMenu.getInt("parent"));
				action.setSource(rsMenu.getString("sources"));
				actionMap.put(action.getId(), action);
			}

			// Root
			root = new Action();
			root.setId(0);
			root.setParent(-1);
			actionMap.put(root.getId(), root);

			addActionChildren(actionMap, root);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rsMenu, stmtMenu);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return root.getChildrens();
	}

	private void addActionChildren(LinkedHashMap<Integer, Action> mp, Action action) {
		Iterator<Entry<Integer, Action>> it = mp.entrySet().iterator();

		action.setChildrens(new ArrayList<Action>());
		while (it.hasNext()) {
			Entry<Integer, Action> pair = it.next();

			if (action.getId() == pair.getValue().getParent()) {
				action.getChildrens().add(pair.getValue());
				addActionChildren(mp, pair.getValue());
			}
		}
	}

	public String getUserPrimaryRole(String userName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		connection = getPostGresDBConnection();
		Statement stmt = connection.createStatement();
		ResultSet rs = null;
		String userPrimaryRole = null;
		try {
			StringBuilder userPrimaryRoleQuery = new StringBuilder("select primaryrole from users where username = '")
					.append(userName).append("'");
			System.out.println(userPrimaryRoleQuery.toString());
			rs = stmt.executeQuery(userPrimaryRoleQuery.toString());
			while (rs.next()) {
				userPrimaryRole = rs.getString("primaryrole");
			}

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return userPrimaryRole;
	}

	public Connection getPostGresDBConnection() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}

	public Report getReportQuery(Report report) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		try {
			query = "select name,description,query,param,source,roleid,dbname from report where id=" + report.id;
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				report.setName(rs.getString("name"));
				report.setDescription(rs.getString("description"));
				report.setQuery(rs.getString("query"));
				report.setParam(rs.getString("param"));
				report.setSource(rs.getString("source"));
				report.setRoleid(rs.getInt("roleid"));
				report.setDbname(rs.getString("dbname"));

			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return report;
	}

	public Report getReportQueryByReportName(Report report) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query = null;
		_logger.info("Fetching Theme Object for : ");
		try {

			_logger.info("Fetching getReportQuery ");
			query = "select id, name, description, query, param, source, roleid, dbname from report where name = '"
					+ report.name + "'";
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			System.out.println(query);
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				report.setId(rs.getString("id"));
				report.setName(rs.getString("name"));
				report.setDescription(rs.getString("description"));
				report.setQuery(rs.getString("query"));
				report.setParam(rs.getString("param"));
				report.setSource(rs.getString("source"));
				report.setRoleid(rs.getInt("roleid"));
				report.setDbname(rs.getString("dbname"));
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return report;
	}

	public ArrayList<LinkedHashMap<String, Object>> getReports(Report report) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<LinkedHashMap<String, Object>> reportdata = new ArrayList<LinkedHashMap<String, Object>>();
		_logger.info("Fetching getReports");
		try {
			String query = report.getQuery();

			if (report.getSource().equalsIgnoreCase("HIVE")) {
				connection = dbUtil.getHiveConnection(report.getDbname());
			} else {
				connection = dbUtil.getPostGresConnection(report.getDbname());
			}
			stmt = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			_logger.info("Query to create excel:" + query);
			rs = stmt.executeQuery(query);
			ArrayList<String> columnNames = new ArrayList<>();
			ArrayList<String> columnTypeNames = new ArrayList<>();
			ResultSetMetaData columns = rs.getMetaData();
			int i = 0;
			while (i < columns.getColumnCount()) {
				i++;
				columnNames.add(StringUtils.capitalize(columns.getColumnLabel(i)));
				columnTypeNames.add(StringUtils.capitalize(columns.getColumnTypeName(i)));
			}
			List<String> numbericType = new ArrayList<>();
			numbericType.add("double");
			numbericType.add("bigint");
			numbericType.add("int8");
			numbericType.add("float8");
			numbericType.add("decimal");
			while (rs.next()) {
				LinkedHashMap<String, Object> reportvalues = new LinkedHashMap<>();
				for (i = 0; i < columnNames.size(); i++) {
					if (rs.getString(columnNames.get(i).toLowerCase()) == null) {
						reportvalues.put(columnNames.get(i), " ");
					} else {
						if (numbericType.contains(columnTypeNames.get(i).toLowerCase())) {
							String numberInString = rs.getString(columnNames.get(i).toLowerCase());
							if (numberInString != null && !numberInString.trim().equalsIgnoreCase("")) {
								try {
									double numberinDouble = Double.parseDouble(numberInString);
									reportvalues.put(columnNames.get(i), numberinDouble);
								} catch (Exception e) {
									_logger.error(e);
									continue;
								} finally {

								}
							}
						} else {
							reportvalues.put(columnNames.get(i), rs.getString(columnNames.get(i).toLowerCase()));
						}
					}

				}
				reportdata.add(reportvalues);
			}
		} catch (Exception ex) {
			_logger.info("Eexcetption in generating excel:" + ex.getMessage());
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}

	public StreamingOutput getReportsAndGenerateExcel(Report report) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		int rowNum=0;
		StreamingOutput streamOutput = null;
		_logger.info("Fetching getReports");
		try {
			String query = report.getQuery();

			if (report.getSource().equalsIgnoreCase("HIVE")) {
				connection = dbUtil.getHiveConnection(report.getDbname());
			} else {
				connection = dbUtil.getPostGresConnection(report.getDbname());
			}
			stmt = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			_logger.info("Query to create excel:" + query);
			rs = stmt.executeQuery(query);
			final SXSSFWorkbook hwb = new SXSSFWorkbook();
			SXSSFSheet sheet = hwb.createSheet("Report");
			SXSSFRow row = sheet.createRow(rowNum);
			CellStyle style = hwb.createCellStyle();
			CellStyle cs = hwb.createCellStyle();
			CellStyle cs1 = hwb.createCellStyle();
			
			    XSSFColor color = new XSSFColor(new java.awt.Color(95,153,206));//79, 138, 188//87, 170, 238
		       ((XSSFCellStyle) style).setFillForegroundColor(color);
		        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        XSSFFont font = (XSSFFont) hwb.createFont();
		        font.setColor(new XSSFColor( new java.awt.Color(255, 255, 255)));
		        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		        style.setFont(font);
		        XSSFColor color1 = new XSSFColor(new java.awt.Color(218,234,247));
		       ((XSSFCellStyle) cs).setFillForegroundColor(color1);
		        cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        cs.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		        cs.setBorderTop(HSSFCellStyle.BORDER_THIN);
		        cs.setBottomBorderColor(IndexedColors.SKY_BLUE.getIndex());
		        cs.setTopBorderColor(IndexedColors.SKY_BLUE.getIndex());
		        XSSFColor color2 = new XSSFColor(new java.awt.Color(253,255,255));//79, 138, 188//87, 170, 238
			    ((XSSFCellStyle) cs1).setFillForegroundColor(color2);
		        cs1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		        cs1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		        cs1.setBorderRight(HSSFCellStyle.BORDER_THIN);
		        cs1.setLeftBorderColor(IndexedColors.GREY_25_PERCENT .getIndex());
			    cs1.setRightBorderColor(IndexedColors.GREY_25_PERCENT .getIndex());
			    ResultSetMetaData rsmd=rs.getMetaData();
			    int columnCount=rsmd.getColumnCount();
			    for(int i=1;i<=columnCount;i++)
			      {
			    	  SXSSFCell cell = row.createCell(i-1);
			    	  cell.setCellStyle(style);
			          cell.setCellValue(rs.getMetaData().getColumnName(i));
			      }
			    
			    // if(rs.next()) {
			    	
			    	while (rs.next())
			        {  	
			    		rowNum++;
			        SXSSFRow nextRow = sheet.createRow( rowNum);
			    		for(int j=1;j<=columnCount;j++)
			            {  
			    			System.out.println(rs.getMetaData().getColumnType(j));
			    			System.out.println(rs.getMetaData().getColumnTypeName(j));
			    			if((rs.getMetaData().getColumnType(j) == Types.VARCHAR)){
			    				SXSSFCell dataCell=nextRow.createCell(j-1);
				    			dataCell.setCellValue(rs.getString(rs.getMetaData().getColumnName(j)));
				    			 if(rowNum%2==0) {
				    				 dataCell.setCellStyle(cs);
				    			 }else
				    			 {
				    				 dataCell.setCellStyle(cs1);
				    			 }
			    			}
			    			else{
			    				SXSSFCell dataCell=nextRow.createCell(j-1);
			    				 if(rowNum%2==0) {
				    				 dataCell.setCellStyle(cs);
				    			 }else
				    			 {
				    				 dataCell.setCellStyle(cs1);
				    			 }
			    					dataCell.setCellValue(Double.parseDouble(rs.getString(rs.getMetaData().getColumnName(j))));
			    				 
			    			}
			    	     }
			       }
			    //}
				if(rowNum == 0) {
					style = hwb.createCellStyle();
					int columnsLength = columnCount;
					int lengthOfCol = columnsLength - 1;
					style.setAlignment(CellStyle.ALIGN_CENTER);
					row = sheet.createRow(1);
					SXSSFCell cell = row.createCell(0);
					cell.setCellValue("Data not found");
					cell.setCellStyle(style);
					sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, lengthOfCol));
				}

				streamOutput = new StreamingOutput() {
					public void write(OutputStream output) throws IOException, WebApplicationException {
						try {
							hwb.write(output);
							hwb.close();
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};
		        
		} catch (Exception ex) {
			_logger.info("Eexcetption in generating excel:" + ex.getMessage());
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return streamOutput;
	}
	
	public File getReportsAndGenerateCsv(Report report ,DataTable datatableExport) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		_logger.info("Fetching getReports");
		String fileName = datatableExport.getDisplayName() + ".csv";
		fileName = fileName.replace(",", "");
		File file = new File(fileName);
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		
		try {
			String query = report.getQuery();

			if (report.getSource().equalsIgnoreCase("HIVE")) {
				connection = dbUtil.getHiveConnection(report.getDbname());
			} else {
				connection = dbUtil.getPostGresConnection(report.getDbname());
			}
			stmt = connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			_logger.info("Query to create excel:" + query);
			rs = stmt.executeQuery(query);
			int columnNames = rs.getMetaData().getColumnCount();
			for (int j = 1; j < (columnNames + 1); j++) {
				out.append(rs.getMetaData().getColumnName(j));
				if (j < columnNames)
					out.append(",");
				else
					out.append("\r\n");
			}
			while (rs.next()) {
				for (int k = 1; k < (columnNames + 1); k++) {
					out.append(rs.getString(k));
					if (k < columnNames)
						out.append(",");
					else
						out.append("\r\n");
				}
			}

			

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			if(out!=null) {
			out.close();
			}
			PrismHandler.handleFinally(connection, rs, stmt);
	}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return file;

	}

	public ArrayList<LinkedHashMap<String, String>> getReports(Report report, String env, String paramCon, String table,
			String whereCondition) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		_logger.info("Fetching getReports");
		ArrayList<LinkedHashMap<String, String>> reportdata = new ArrayList<>();
		try {

			String query = report.getQuery();
			if (env.equalsIgnoreCase("HIVE")) {
				connection = dbUtil.getHiveConnection(paramCon);
				query = "SELECT * FROM " + table;
				if (whereCondition != null && !whereCondition.trim().isEmpty())
					query += " " + whereCondition;
			} else {
				connection = dbUtil.getPostGresConnection(paramCon);
				query = "SELECT * FROM " + table;
				if (whereCondition != null && !whereCondition.trim().isEmpty())
					query += " " + whereCondition;
			}
			stmt = connection.createStatement();
			System.out.println(query);
			rs = stmt.executeQuery(query);
			ArrayList<String> columnNames = new ArrayList<>();
			ArrayList<String> columnTypeNames = new ArrayList<>();
			if (rs != null) {
				ResultSetMetaData columns = rs.getMetaData();
				int i = 0;
				while (i < columns.getColumnCount()) {
					i++;
					columnNames.add(StringUtils.capitalize(columns.getColumnName(i)));
					columnTypeNames.add(StringUtils.capitalize(columns.getColumnTypeName(i)));
				}

				while (rs.next()) {
					LinkedHashMap<String, String> reportvalues = new LinkedHashMap<String, String>();
					for (i = 0; i < columnNames.size(); i++) {
						if (rs.getString(columnNames.get(i).toLowerCase()) == null) {
							reportvalues.put(columnNames.get(i), " ");
						} else {
							if (columnTypeNames.get(i).equalsIgnoreCase("double")
									|| columnTypeNames.get(i).equalsIgnoreCase("bigint")) {
								String numberInString = rs.getString(columnNames.get(i).toLowerCase());
								double numberinDouble = Double.valueOf(numberInString).doubleValue();
								NumberFormat defaultFormat = NumberFormat.getInstance();
								defaultFormat.setMinimumFractionDigits(2);
								defaultFormat.setMaximumFractionDigits(10);
								reportvalues.put(columnNames.get(i), defaultFormat.format(numberinDouble));
							} else {
								reportvalues.put(columnNames.get(i), rs.getString(columnNames.get(i).toLowerCase()));
							}
						}

					}
					reportdata.add(reportvalues);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}

	public List<Report> getReportQueryGrid() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		String query = null;
		List<Report> reportList = null;
		_logger.info("Fetching Theme Object for : ");
		try {

			_logger.info("Fetching getReportQuery ");
			query = "select id,name,description,query,param,source,roleid from report where roleid='1'";
			connection = getPostGresDBConnection();
			stmt = connection.createStatement();
			System.out.println(query);
			rs = stmt.executeQuery(query);
			reportList = new ArrayList<>();
			while (rs.next()) {
				Report report = new Report();

				report.setId(rs.getString("id"));
				report.setName(rs.getString("name"));
				report.setDescription(rs.getString("description"));
				report.setQuery(rs.getString("query"));
				report.setParam(rs.getString("param"));
				report.setSource(rs.getString("source"));
				report.setRoleid(rs.getInt("roleid"));

				reportList.add(report);

			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportList;
	}

	public List<LinkedHashMap<String, String>> getColumnValueList(String env, String db, String table,
			String whereCondition) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String query = null;
		String paramCon = null;
		Connection con = null;
		Statement stmtPost = null;
		ResultSet rsPost = null;
		ArrayList<LinkedHashMap<String, String>> reportdata = null;
		try {
			query = "select name from app_properties where name ='" + db + "' or (value like '%jdbc%' and value like '%"
					+ db + "%')";
			con = dbUtil.getPostGresConnection();
			stmtPost = con.createStatement();
			System.out.println(query);
			rsPost = stmtPost.executeQuery(query);
			while (rsPost.next()) {
				paramCon = rsPost.getString("name");
			}
			Report report = new Report();
			report.query = query;
			report.name = table;
			report.dbname = paramCon;
			reportdata = getReports(report, env, paramCon, table, whereCondition);

		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rsPost, stmtPost);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}

	public User userAuthentication(User obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User user = null;
		Connection con = null;
		String query = null;

		try {
			con = dbUtil.getPostGresConnection();
			if (obj.getUsername() != null && obj.getPassword() != null) {
				query = "SELECT * from users where username= '" + obj.getUsername() + "' AND password='"
						+ obj.getPassword() + "' AND enabled= '" + 1 + "'";
				PreparedStatement ps = con.prepareStatement(query);

				ResultSet rs = ps.executeQuery();

				if (rs.next()) {
					user = new User();
					user.setId(rs.getInt("id"));
					user.setUsername(rs.getString("username"));
					user.setDateformat(rs.getString("dateformat"));
					user.setEmail(rs.getString("emailid"));
					user.setThemeName(rs.getString("themename"));
					user.setPrimaryRole(rs.getString("primaryrole"));
					user.setFirstName(rs.getString("firstname"));
					user.setLastName(rs.getString("lastname"));
					user.setTimezone(rs.getString("timezone"));
				}
				ps.close();
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return user;
	}

	public List<String> getEmailAddressByBusinessRole(String businessRoleName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		String query = null;
		List<String> usersList = null;
		try {
			con = dbUtil.getPostGresConnection();
			query = "select emailid from users where business_role = ? and emailid is not null and emailid <> ''";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, businessRoleName);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				usersList = new ArrayList<>();
				do {
					usersList.add(rs.getString("emailid"));
				} while (rs.next());
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return usersList;
	}
}
