package com.relevance.prism.data;

public class GoodsHistory {
 String purchasingDocument;
 String materialDocument;
 String poHistory;
 String movementType;
 String  quantity;
 String amount;
 String currency;
 String postingDate;
public String getPurchasingDocument() {
	return purchasingDocument;
}
public void setPurchasingDocument(String purchasingDocument) {
	this.purchasingDocument = purchasingDocument;
}
public String getMaterialDocument() {
	return materialDocument;
}
public void setMaterialDocument(String materialDocument) {
	this.materialDocument = materialDocument;
}
public String getPoHistory() {
	return poHistory;
}
public void setPoHistory(String poHistory) {
	this.poHistory = poHistory;
}
public String getMovementType() {
	return movementType;
}
public void setMovementType(String movementType) {
	this.movementType = movementType;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getCurrency() {
	return currency;
}
public void setCurrency(String currency) {
	this.currency = currency;
}
public String getPostingDate() {
	return postingDate;
}
public void setPostingDate(String postingDate) {
	this.postingDate = postingDate;
}

}
