package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.gson.Gson;
import com.relevance.prism.data.PrismException;
 
/**
 * @author Shawkath Khan
 * @CreatedOn Mar 15, 2016
 * @Purpose To maintain static methods to handle exceptions
 * @ModifiedBy
 * @ModifiedDate
 * 
 */

public class PrismHandler {

	
	/**
	 * @throws AppException 
	 * @throws SQLException 
	 * 
	 */
	 
	public static String fullClassName = null;
	public static String methodName = null;
	public static int lineNumber = 0;	
	
	public static void logMethodEntry(String className, String methodName){
		//this is not a useful implementation and simply filling the log file so removed.
		//Log.Info("Method Entry for " + methodName + " in " + className);
	}
	
	public static String handleException(Exception ex, boolean throwExceptionAgain) throws Exception{
		String failureMessage = null;
		if(ex instanceof NullPointerException){
			setExceptionProperties(ex, 0);
			failureMessage = returnFailureMessage(ex, "NullPointerException", fullClassName, methodName, lineNumber);
			return handleIndividualExceptions(ex, failureMessage, throwExceptionAgain);
		} 
		if(ex instanceof SQLException){
			setExceptionProperties(ex, 8);
			failureMessage = returnFailureMessage(ex, "SQLException", fullClassName, methodName, lineNumber);
			return handleIndividualExceptions(ex, failureMessage, throwExceptionAgain);
		} 
		/*if(ex instanceof AppException){
			setExceptionProperties(ex, 0);
			failureMessage = returnFailureMessage(ex, "AppException", fullClassName, methodName, lineNumber);
			return handleIndividualExceptions(ex, failureMessage, throwExceptionAgain);
		} */
		if(ex instanceof Exception){
			setExceptionProperties(ex, 0);
			failureMessage = returnFailureMessage(ex, "Exception", fullClassName, methodName, lineNumber);
			return handleIndividualExceptions(ex, failureMessage, throwExceptionAgain);
		}
		return handleIndividualExceptions(ex, failureMessage, throwExceptionAgain);
	}
	
	public static String handleException(Exception ex)
	{
		String response = null;
		try
		{
			response = handleException(ex, false);
		}
		catch(Exception e)
		{
			Log.Error("Unhandled error from framework" + e);
		}
		return response;
	}
	
	public static void handleFinally(Connection con, ResultSet rs, Statement stmt) throws AppException, SQLException, NullPointerException, Exception{
		if(con != null)
			try {
				if(rs != null)
					rs.close();
				
				if(stmt != null)
					stmt.close();
				
				if (con != null)
					con.close();
			} catch(SQLException sql) {
				Log.Error(sql);
				handleException(sql, false);
			} catch(NullPointerException ne) {
				Log.Error(ne);
				handleException(ne, false);
			} 
			catch(Exception ex) {
				Log.Error(ex);
				handleException(ex, false);
			}		
	}
	
	public static void logMethodExit(String className, String methodName){
		//this is not a useful implementation and simply filling the log file so removed.
		//Log.Info("Method Exit for " + methodName + " in " + className);
	}
	
	public static void logDebug(String className, String methodName, String message){
		Log.Info("Debug Message for " + methodName + " in " + className + ":" + message);
	}
	
	private static String handleIndividualExceptions(Exception ex, String failureMessage, boolean throwExceptionAgain) throws Exception{
		Log.Error(failureMessage);
		if(throwExceptionAgain){
			throw ex;
		}
		return failureMessage;
	}
	
	private static void setExceptionProperties(Exception ex, int index){
		lineNumber = ex.getStackTrace()[index].getLineNumber();
		fullClassName = ex.getStackTrace()[index].getClassName();
		methodName = ex.getStackTrace()[index].getMethodName();
	}
		
	private static String returnFailureMessage(Exception ex, String exceptionType, String fullClassName, String methodName, int lineNumber){
		String failureMessage = null;
		PrismException prismException = new PrismException();
		prismException.setMessage("FAILURE");
		prismException.setExceptionType(exceptionType);
		prismException.setCustomMessage("Error While Processing Request");
		prismException.setIsException("true");
		prismException.setClassName(fullClassName);
		prismException.setMethodName(methodName);
		prismException.setLineNumber(lineNumber);
		if(!exceptionType.equalsIgnoreCase("NullPointerException")){
			prismException.setMessage(ex.getMessage().replaceAll("\"", "").replaceAll("\n", "."));
		}else{
			prismException.setMessage(ex.getMessage());			
		}
		Gson gson = new Gson();
		failureMessage = gson.toJson(prismException);
		return failureMessage;
	}    
}