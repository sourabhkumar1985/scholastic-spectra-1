package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class RefreshMysqlToPG extends BaseUtil {
	private PrismDbUtil dbUtil = new PrismDbUtil();

	public void refreshMysql() throws Exception, SQLException {
		// SSH login username
		String strSshUser = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_USERNAME); 
		// SSH login password
		String strSshPassword = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_PASSWORD); 
		// String strSshHost =
		// E2emfAppUtil.getAppProperty(E2emfConstants.SSH_HOST); // hostname or
		// ip
		// or
		// SSH server
		String strSshHost = E2emfAppUtil.getAppProperty(E2emfConstants.SSH_HOSTNAME);
		// remote SSH host port number
		int nSshPort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.SSH_PORT)); 
		// hostname or ip of your database server
		String strRemoteHost = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_HOSTNAME); 
		//local port number use to bind SSH tunnel
		int nLocalPort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_LOCAL_PORT)); 
		// remote port number of your database
		int nRemotePort = Integer.parseInt(E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_REMOTE_PORT));
		// database loging username
		String strDbUser = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_USERNAME); 
		// database login password
		String strDbPassword = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_PASSWORD); 
		String dataBase = E2emfAppUtil.getAppProperty(E2emfConstants.MYSQL_DBNAME);
		Session session = doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort,
				nRemotePort);
		Log.Info("Sshtunneling done");
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:" + nLocalPort + "/" + dataBase, strDbUser,
				strDbPassword);
		Log.Info("Connected to Mysql");
		System.out.println("Connected to Mysql");
		Statement stmt = con.createStatement();
		Statement stmt2 = con.createStatement();
		Statement stmt3 = con.createStatement();

		String sql = "insert into catalog_document_attributes values (?,?,?,?,?)";
		String sql1 = "insert into user_secondary_roles values (?,?,?,?,?,?,?,?)";
		String sql3 = "insert into cmsdataemail values (?,?,?,?,?,?)";

		PreparedStatement pst = null;
		PreparedStatement pst2 = null;
		PreparedStatement pst3 = null;

		Connection spectraConn = null;
		Connection qfindConn = null;
		ResultSet userRoles = null;
		ResultSet docAttributesSet = null;
		ResultSet cmsDataSet = null;
		ResultSet existingUserRole = null;
		String facet_name = "";
		Statement truncDocAttributes = null;
		Statement truncCMSDataEmail = null;
		/*
		 * int count = 0; int count2 = 0; int count3 = 0;
		 */
		try {
			spectraConn = getPostGresDBConnectionSpectra();
			qfindConn = getPostGresDBConnectionQfind();
			Statement stmt4 = spectraConn.createStatement();
			Statement updRecord = spectraConn.createStatement();
			truncDocAttributes = qfindConn.createStatement();
			truncCMSDataEmail = qfindConn.createStatement();

			userRoles = stmt.executeQuery("select sections.name,u_p.* from integra2015.sections inner join "
					+ "(select user_permission.name,section_id,email,group_concat(id),group_concat(permissions.name)"
					+ " from integra2015.permissions inner join(SELECT user_id,name,email,permission_id"
					+ " FROM integra2015.permission_user inner join integra2015.users on permission_user.user_id = users.id)"
					+ " as user_permission on user_permission.permission_id = permissions.id group by name,section_id) u_p on sections.id = u_p.section_id");
			pst2 = spectraConn.prepareStatement(sql1);
			Log.Info("Refreshing User attributes");
			System.out.println("Refreshing User attributes");
			while (userRoles.next()) {
				String email = userRoles.getString("email");
				if (email.equalsIgnoreCase("benjamin.o'brien@integralife.com")) {
					continue;
				}
				existingUserRole = stmt4.executeQuery(
						"select section_id, section_name, facet_name, group_ids, group_names from user_secondary_roles where trim(email) = trim('"
								+ userRoles.getString("email") + "') and section_id = " + userRoles.getString(3).trim());
				if ((existingUserRole != null & existingUserRole.next())
						&& (existingUserRole.getString("section_id").trim().equalsIgnoreCase(userRoles.getString(3).trim())
								& existingUserRole.getString("section_name").trim().equalsIgnoreCase(userRoles.getString(1).trim()))) {
					System.out.println("Section Name in DB : " + existingUserRole.getString("section_name").trim()
							+ " Section Name in MySQL : " + userRoles.getString(1).trim());
					System.out.println("Facet Name in DB : " + existingUserRole.getString("facet_name").trim()
							+ " Facet Name in MySQL : " + userRoles.getString(1) != null && !userRoles.getString(1).isEmpty()
									? userRoles.getString(1).toLowerCase().replaceAll(" ", "_") : userRoles.getString(1));
					
					 /* if
					 * (rs.getString(1).trim().equalsIgnoreCase(rs4.getString(
					 * "section_name").trim())) { updRecord.executeUpdate(
					 * "update user_secondary_roles set section_name = '" +
					 * rs.getString(1) + "' where trim(email) =trim('" +
					 * rs.getString("email") + "')"); }
					 
					
					 * if
					 * (rs.getString(1).trim().equalsIgnoreCase(rs4.getString(
					 * "facet_name").trim())) { facet_name = (rs.getString(1) !=
					 * null && !rs.getString(1).isEmpty()) ?
					 * rs.getString(1).toLowerCase().replaceAll(" ", "_") :
					 * rs.getString(1); updRecord.executeUpdate(
					 * "update user_secondary_roles set facet_name = '" +
					 * facet_name + "' where trim(email) =trim('" +
					 * rs.getString("email") + "')"); }*/
					 
					if (!userRoles.getString(5).trim().equalsIgnoreCase(existingUserRole.getString("group_ids").trim())) {
						updRecord.executeUpdate("update user_secondary_roles set group_ids = '" + userRoles.getString(5)
								+ "' where trim(email) =trim('" + userRoles.getString("email") + "')");
					}
					if (!userRoles.getString(6).trim().equalsIgnoreCase(existingUserRole.getString("group_names").trim())) {
						updRecord.executeUpdate("update user_secondary_roles set group_names = '" + userRoles.getString(6)
								+ "' where trim(email) =trim('" + userRoles.getString("email") + "')");
					}

				} else {
					pst2.setInt(1, Integer.valueOf(userRoles.getString(3)));
					System.out.println("Section Name: " + userRoles.getString(1));
					pst2.setString(2, userRoles.getString(1));
					pst2.setString(3, userRoles.getString(1).toLowerCase().replaceAll(" ", "_"));
					pst2.setString(4, userRoles.getString(5));
					pst2.setString(5, userRoles.getString(6));
					pst2.setString(6, userRoles.getString(2));
					pst2.setString(7, userRoles.getString(4));
					pst2.setString(8, ",");
					pst2.executeUpdate();
					// System.out.println("Inserting into User Secondary Roles"
					// + count++);
				}
			}
			Log.Info("Refreshing Document attributes");
			System.out.println("Refreshing Document attributes");
			docAttributesSet = stmt2.executeQuery(
					"SELECT filename,file_display_name,section_id,GROUP_CONCAT(permissions.id),GROUP_CONCAT(permissions.name)"
							+ " FROM integra2015.permissions inner join"
							+ " (SELECT content_permission.*,file_content.filename,file_content.display_name as file_display_name"
							+ " FROM integra2015.content_permission inner join"
							+ " (SELECT content.id as content_id, files.* FROM integra2015.files"
							+ " inner join integra2015.content on files.id = content.file_id) as file_content"
							+ " on file_content.content_id = content_permission.content_id) as file_content_permission"
							+ " on permissions.id = file_content_permission.permission_id group by filename,file_display_name,section_id");
			pst = qfindConn.prepareStatement(sql);
			Log.Info("Truncating catalog_document_attributes");
			System.out.println("Truncating catalog_document_attributes");
			truncDocAttributes.execute("truncate table catalog_document_attributes");
			while (docAttributesSet.next()) {

				pst.setString(1, docAttributesSet.getString(1));
				pst.setString(2, docAttributesSet.getString(2));
				pst.setString(3, docAttributesSet.getString(3));
				pst.setString(4, docAttributesSet.getString(4));
				pst.setString(5, docAttributesSet.getString(5));
				pst.executeUpdate();
				// System.out.println("Inserting into
				// catalog_document_attributes" + count2++);
			}

			Log.Info("Refreshing cmsdataemail attributes");
			System.out.println("Refreshing cmsdataemail attributes");
			cmsDataSet = stmt3.executeQuery(
					"select content_id,folder_id,concat(replace(path,'!','/'),'/',content_path),filename from integra2015.folders inner join"
							+ " (select content_folder.content_id,content_folder.folder_id,content_path,filename from integra2015.content_folder inner join"
							+ " (SELECT content.id as content_id,concat(content.pathname,'.',content.type) as content_path , files.* FROM integra2015.files inner join integra2015.content on files.id = content.file_id) as file_content"
							+ " on content_folder.content_id = file_content.content_id) as file_content_folder"
							+ " on folders.id = file_content_folder.folder_id");
			pst3 = qfindConn.prepareStatement(sql3);
			Log.Info("Truncating cmsdataemail");
			System.out.println("Truncating cmsdataemail");
			truncCMSDataEmail.execute("truncate table cmsdataemail");
			while (cmsDataSet.next()) {
				String franshisedata[] = cmsDataSet.getString(3).split("/");
				String franchise = franshisedata[0];
				if (franchise.equals("upper-extremity")) {
					franchise = "Upper Extremity";
				} else if (franchise.equals("hernia-prs")) {
					franchise = "Hernia prs";
				} else if (franchise.equals("wound-care")) {
					franchise = "Wound Care";
				} else if (franchise.equals("regenerative")) {
					franchise = "Regenerative";
				} else if (franchise.equals("gncc")) {
					franchise = "GNCC";
				} else if (franchise.equals("dural-repair")) {
					franchise = "Dural Repair";
				} else if (franchise.equals("tissue-ablation")) {
					franchise = "Tissue Ablation";
				} else if (franchise.equals("cranial-stabilization")) {
					franchise = "Cranial Stabilization";
				} else if (franchise.equals("brain-mappingstereotaxy")) {
					franchise = "Brain Mapping / Sterotaxy";
				} else if (franchise.equals("acute-instruments")) {
					franchise = "Acute Instruments";
				} else if (franchise.equals("alternate-instruments")) {
					franchise = "Alternate Instruments";
				} else if (franchise.equals("lower-extremity")) {
					franchise = "Lower Extremity";
				} else if (franchise.equals("shunts")) {
					franchise = "Shunts";
				} else {
					franchise = null;
				}
				pst3.setString(1, cmsDataSet.getString(1));
				pst3.setString(2, cmsDataSet.getString(2));
				pst3.setString(3, cmsDataSet.getString(3));
				pst3.setString(4, cmsDataSet.getString(4));
				pst3.setString(5, null);
				pst3.setString(6, franchise);
				pst3.executeUpdate();
				// System.out.println("Inserting into cmsdataemail" + count3++);

			}
			Log.Info("Refresh Job Completed");
			System.out.println("Refresh job Completed");
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
		} finally {
			PrismHandler.handleFinally(spectraConn, userRoles, stmt);
			PrismHandler.handleFinally(qfindConn, docAttributesSet, stmt2);
			PrismHandler.handleFinally(null, cmsDataSet, stmt3);
			truncCMSDataEmail.close();
			
			con.close();
			session.disconnect();
		}
		System.out.println("Refresh to MySqlToPG Job has completed.");
	}

	private static Session doSshTunnel(String strSshUser, String strSshPassword, String strSshHost, int nSshPort,
			String strRemoteHost, int nLocalPort, int nRemotePort) throws JSchException {
		final JSch jsch = new JSch();
		Session session = jsch.getSession(strSshUser, strSshHost, 22);
		session.setPassword(strSshPassword);

		final Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);

		session.connect();
		session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
		return session;

	}

	public Connection getPostGresDBConnectionQfind() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection(E2emfConstants.qfind_db);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}

	public Connection getPostGresDBConnectionSpectra() throws Exception {
		Connection connection = null;
		try {
			connection = dbUtil.getPostGresConnection(E2emfConstants.postGresDBContext);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		return connection;
	}
}
