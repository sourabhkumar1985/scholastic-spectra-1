package com.relevance.prism.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.relevance.prism.dao.SankeyGeneratorDao;
import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.Link;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.data.Sankey;
import com.relevance.prism.data.SankeyConfig;
import com.relevance.prism.data.Stack;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class SankeyGeneratorService extends BaseService{
	public Sankey getJSON(String source, String database,SankeyConfig sankeyConfig) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<FlowData> flowDataList = new ArrayList<FlowData>();
		Sankey Sankey = new Sankey();
		try{
			Log.Info("getJSON() method called on SankeyGeneratorService");
			SankeyGeneratorDao sankeyGeneratorDao = new SankeyGeneratorDao();
			flowDataList = sankeyGeneratorDao.getJSON(source,database,sankeyConfig);
			Sankey = convertConfigListToNodeAndLink(flowDataList);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return Sankey;
	}
	
	public HashMap<String, String> getLookUpValues(String source, String database,String lookUpQuery) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, String> lookUpMap = null;
		try{
			Log.Info("getLookUpValues() method called on SankeyGeneratorService");
			SankeyGeneratorDao sankeyGeneratorDao = new SankeyGeneratorDao();
			lookUpMap = sankeyGeneratorDao.getLookUpValues(source,database,lookUpQuery);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return lookUpMap;
	}
	
	public List<MasterItem> getSearchSuggestions(String q,String source, String database,String lookUpQuery) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<MasterItem> suggestionsList = null;
		try{
			Log.Info("getSuggestions() method called on SankeyGeneratorService");
			SankeyGeneratorDao sankeyGeneratorDao = new SankeyGeneratorDao();
			suggestionsList = sankeyGeneratorDao.getSearchSuggestions(q,source,database,lookUpQuery);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return suggestionsList;
	}
	
	private Sankey convertConfigListToNodeAndLink(List<FlowData> flowDataList) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Sankey sankey = new Sankey();
		ArrayList<Link> links = new ArrayList<Link>();
		HashMap<String,Stack> nodesDistinct = new HashMap<String,Stack>();
		HashMap<String,Integer> stackDistinct = new HashMap<String,Integer>(); 
		try {
			for (FlowData flowData : flowDataList) {
				
				if (stackDistinct.containsKey(flowData.getToCode())){
					if (stackDistinct.get(flowData.getToCode()) != flowData.getTo()){
						throw new AppException("",
								"Error while processing the request.", 1, null,
								true);
					}
				}
				
				if (stackDistinct.containsKey(flowData.getFromCode())){
					if (stackDistinct.get(flowData.getFromCode()) != flowData.getFrom()){
						throw new AppException("",
								"Error while processing the request.", 1, null,
								true);
					}
				}
				
				Stack stackSource = new Stack();
				stackSource.setName(flowData.getFromCode());
				stackSource.setText(flowData.getFromName());
				stackSource.setStack(flowData.getFrom());
				stackSource.setColor(flowData.getFromColor());
				nodesDistinct.put(flowData.getFromCode(),stackSource);
				
				Stack stackTarget = new Stack();
				stackTarget.setName(flowData.getToCode());
				stackTarget.setText(flowData.getToName());
				stackTarget.setStack(flowData.getTo());
				stackTarget.setColor(flowData.getToColor());
				nodesDistinct.put(flowData.getToCode(),stackTarget);
				
				Link link = new Link();
				link.setSource(flowData.getFromCode());
				link.setSourcename(flowData.getFromName());
				link.setTarget(flowData.getToCode());
				link.setTargetname(flowData.getToName());
				link.setColor(flowData.getLinkColor());
				link.setText(flowData.getLinkType());
				link.setValue(flowData.getLinkValue());
				String linkToolTip = flowData.getLinkToolTip();
				if(linkToolTip != null){
					link.setLinkToolTip(linkToolTip);
				}
				String linkValueSuffix = flowData.getLinkValueSuffix();
				if(linkValueSuffix != null){
					link.setLinkValueSuffix(linkValueSuffix);
				}
				String linkValuePrefix = flowData.getLinkValuePrefix();
				if(linkValuePrefix != null){
					link.setLinkValuePrefix(linkValuePrefix);
				}
				links.add(link);
			}
			ArrayList<Stack> nodes = new ArrayList<Stack>();
			for (Map.Entry<String, Stack> stackList : nodesDistinct.entrySet())
			{
				nodes.add(stackList.getValue());
			}
			sankey.setLinks(links);
			sankey.setNodes(nodes);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return sankey;
	}
}
