package com.relevance.prism.data;

import java.util.List;

public class UpdateColumns {
	private String source;
	private String database;
	private String table;
	private List<Audit> values;
	private List<Audit> primaryKey;
	private String userName;
	private String userId;
	private boolean create;
	private boolean enableAudit;
	public boolean isCreate() {
		return create;
	}
	public void setCreate(boolean create) {
		this.create = create;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public List<Audit> getValues() {
		return values;
	}
	public void setValues(List<Audit> values) {
		this.values = values;
	}
	public List<Audit> getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(List<Audit> primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public boolean isEnableAudit() {
		return enableAudit;
	}
	public void setEnableAudit(boolean enableAudit) {
		this.enableAudit = enableAudit;
	}
}
