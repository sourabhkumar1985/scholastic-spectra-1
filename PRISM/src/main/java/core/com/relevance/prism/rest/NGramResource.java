package com.relevance.prism.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.NGramConfig;
import com.relevance.prism.data.NGramList;
import com.relevance.prism.service.NGramService;
import com.relevance.prism.util.PrismHandler;

@Path("/nGram")
public class NGramResource extends BaseResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/nGramAnalyser")
	public String nGramAnalyser(@FormParam("config") String config) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		NGramList nGramView = new NGramList();
		NGramConfig nGramConfig;
		NGramService nGramService = new NGramService();
		try {
			Gson gson = new Gson();
			nGramConfig = gson.fromJson(config, NGramConfig.class);
			nGramView = nGramService.nGramAnalyser(nGramConfig);
			response = gson.toJson(nGramView);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/nGramAnalyserNLP")
	public String nGramAnalyserNLP(@FormParam("config") String config) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		NGramList nGramView = new NGramList();
		NGramConfig nGramConfig;
		NGramService nGramService = new NGramService();
		try {
			Gson gson = new Gson();
			nGramConfig = gson.fromJson(config, NGramConfig.class);
			nGramView = nGramService.nGramAnalyserNLP(nGramConfig);
			response = gson.toJson(nGramView);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getAllNgramFilter")
	public String getAllNgramFilter(
			@FormParam("userName") String userName,
//			@FormParam("profileId") Integer profileId,
			@FormParam("ngramType") String ngramType) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			NGramService nGramService = new NGramService();
			NGramList ngram = new NGramList();
			ngram.setUserName(userName);
//			ngram.setProfileId(profileId);
			ngram.setNgramType(ngramType);
			List<NGramList> nGramList =  nGramService.getNgramFilter(ngram);
			Gson gson = new Gson();
			response = gson.toJson(nGramList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addNgramFilter")
	public String addNgramFilter(
			@FormParam("userName") String userName,
			@FormParam("profileId") Integer profileId,
			@FormParam("ngramKey") String ngramKey,
			@FormParam("ngramValue") String ngramValue,
			@FormParam("ngramType") String ngramType) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			NGramService nGramService = new NGramService();
			response = nGramService.addNgramFilter(userName, profileId, ngramKey, ngramValue, ngramType);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateNgramFilter")
	public String updateNgramFilter(
			@FormParam("ngramId") Integer ngramId,
			@FormParam("ngramKey") String ngramKey,
			@FormParam("ngramValue") String ngramValue,
			@FormParam("ngramType") String ngramType) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			NGramService nGramService = new NGramService();
			response = nGramService.updateNgramFilter(ngramId, ngramKey, ngramValue, ngramType);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteNgramFilter")
	public String deleteNgramFilter(
			@FormParam("ngramId") Integer ngramId,
			@FormParam("ngramType") String ngramType) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			NGramService nGramService = new NGramService();
			response = nGramService.deleteNgramFilter(ngramId, ngramType);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
