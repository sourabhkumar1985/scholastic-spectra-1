package com.relevance.prism.util;

public class FormConstants {

	public final static String MATERIAL = "MATERIAL";
	public final static String CUSTOMER = "CUSTOMER";
	public final static String SUPPLIER = "SUPPLIER";
	public final static String TIER1 = "TIER1";//plant
	public final static String TIER2 = "TIER2";	
	public final static String KEY = "key";
	
}
