package com.relevance.prism.data;

public class Link {
	public String source;
	public String sourcename;
	public String target;
	public String targetname;
	public String color;
	public double value;
	public String text;
	public String linkToolTip;
	public String linkValueSuffix;
	public String linkValuePrefix;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSourcename() {
		return sourcename;
	}
	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getTargetname() {
		return targetname;
	}
	public void setTargetname(String targetname) {
		this.targetname = targetname;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLinkToolTip() {
		return linkToolTip;
	}
	public void setLinkToolTip(String linkToolTip) {
		this.linkToolTip = linkToolTip;
	}
	public String getLinkValueSuffix() {
		return linkValueSuffix;
	}
	public void setLinkValueSuffix(String linkValueSuffix) {
		this.linkValueSuffix = linkValueSuffix;
	}
	public String getLinkValuePrefix() {
		return linkValuePrefix;
	}
	public void setLinkValuePrefix(String linkValuePrefix) {
		this.linkValuePrefix = linkValuePrefix;
	}	
}
