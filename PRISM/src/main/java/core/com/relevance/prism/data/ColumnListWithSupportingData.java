package com.relevance.prism.data;

import java.util.HashMap;

public class ColumnListWithSupportingData {

	private HashMap<Integer,HashMap<String,String>> supportingData =  new HashMap<Integer,HashMap<String,String>>();
	private HashMap<Integer,String> columnList = new HashMap<Integer,String>();
	public HashMap<Integer, HashMap<String, String>> getSupportingData() {
		return supportingData;
	}
	public void setSupportingData(
			HashMap<Integer, HashMap<String, String>> supportingData) {
		this.supportingData = supportingData;
	}
	public HashMap<Integer, String> getColumnList() {
		return columnList;
	}
	public void setColumnList(HashMap<Integer, String> columnList) {
		this.columnList = columnList;
	}
}
