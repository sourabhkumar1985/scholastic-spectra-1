package com.relevance.prism.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HtmlTOPdfService extends BaseService {

	public File htmlToPdfService(String html, String css, String fileName, String height) {

		FileWriter fWriter = null;
		BufferedWriter writer = null;
		File file = null;
		File pdfOutput = null;
		Date date = new Date();
		String dateTime = null;

		try {

			dateTime = new SimpleDateFormat("E dd MMM yyyy HH:mm:ss z").format(date).toString();

			file = new File(fileName + ".html");
			file.createNewFile();

			pdfOutput = new File(fileName + ".pdf");
			if (pdfOutput.exists())
				pdfOutput.delete();

			pdfOutput.createNewFile();

			fWriter = new FileWriter(file, false);
			writer = new BufferedWriter(fWriter);
			writer.write("<html><head><style>");
			writer.write(css);
			
			writer.write("</style></head><body>");
			writer.write("<div>");
			//margin-bottom:5px
			writer.write("<div style='padding:15px;'><span style='float:left ; font-size:40px';> " + fileName
					+"</span> <span style='float:right;font-size:15px;padding-top: 24px;'> " + dateTime
					+ " </span></div><br></br>");
			writer.write("<hr style='height:2px;border:none;color:#333;background-color:#333;'/>");
			writer.write("<div>" + html + "</div>");
			writer.write("</div>");
			writer.write("</body>");
			writer.write("</html>");
			writer.close();
			fWriter.close();

			System.out.println(height.toString());

			ProcessBuilder pb = new ProcessBuilder("wkhtmltopdf", "--page-width", "286", "--page-height", height,
					"--viewport-size", "1280x1024", fileName + ".html", fileName + ".pdf");

			Process ps = pb.start();
			ps.waitFor();

		} catch (IOException e) {

			e.printStackTrace();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		return pdfOutput;
	}

}
