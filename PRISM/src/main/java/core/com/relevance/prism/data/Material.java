package com.relevance.prism.data;


public class Material {	
	private String name;
    private Double netvalue;
    private int size;
    private String groupName;   
    private String materialId;
    
    private String materialnumber;
    private String materialdescription;
    private String transactiontype;
    private String materialtype;
    private float netprice;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getNetvalue() {
		return netvalue;
	}
	
	public String getMaterialId() {
		return materialId;
	}
	public void setNetvalue(Double netvalue) {
		this.netvalue = netvalue;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getMaterialnumber() {
		return materialnumber;
	}
	public void setMaterialnumber(String materialnumber) {
		this.materialnumber = materialnumber;
	}
	public String getMaterialdescription() {
		return materialdescription;
	}
	public void setMaterialdescription(String materialdescription) {
		this.materialdescription = materialdescription;
	}
	public String getTransactiontype() {
		return transactiontype;
	}
	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}
	public String getMaterialtype() {
		return materialtype;
	}
	public void setMaterialtype(String materialtype) {
		this.materialtype = materialtype;
	}
	public float getNetprice() {
		return netprice;
	}
	public void setNetprice(float netprice) {
		this.netprice = netprice;
	}		
}
