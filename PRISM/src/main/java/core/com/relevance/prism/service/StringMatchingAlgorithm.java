/*package com.relevance.prism.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import org.json.JSONObject;

import com.relevance.prism.util.PrismHandler;

public class StringMatchingAlgorithm extends BaseService implements Callable<String> {

	public StringMatchingAlgorithm() {

	}

	public String call()  throws Exception{
		Map<String, Integer> nGramHashMapTemp = null;
		try {
	matchObject = new LinkedHashMap<String, String>();
	String sourceString = entry.getValue().toUpperCase();
	Integer sourceKey = entry.getKey();
	matchObject.put("sourceText", sourceString);
	matchObject.putAll(sourceSupportingData.get(sourceKey));
	double objLVDistance  = 1000;
	int objHammingDistance =1000;
	double objJaroWrinklerDistance= 1;
	double objSorensenDice = 1;
	double objJaccard = 1;
	double objCosine = 1;
	int lvKey =0;
	int hammingKey = 0;
	int jaroWrinklerKey = 0;
	int sorensenDiceKey = 0;
	int jaccardKey =0;
	int cosineKey = 0;
	Set<String> algorithmsSet = new HashSet<String>(Arrays.asList(algorithms.split(",")));
	for (Map.Entry<Integer, String> targetEntry : targetStrings.entrySet()) {
		String targetString = targetEntry.getValue().toUpperCase();
		int targetKey = targetEntry.getKey();
		if (algorithmsSet.contains("lvDistance")){
			try{
				double tempLvDistance  = levenshteinDistance.distance(sourceString, targetString);
				if  (objLVDistance > tempLvDistance){
					lvKey = targetKey;
					matchObject.put("LV-Match", targetString);
					matchObject.put("LV-Distance", Double.toString(tempLvDistance));
					objLVDistance = tempLvDistance;
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(con, null, stmt);
			}	
		}
		if (algorithmsSet.contains("hammingDistance")){
			try{
				int tempHammingDistance  = hammingDistance.apply(sourceString, targetString);
				if  (objHammingDistance > tempHammingDistance){
					hammingKey = targetKey;
					matchObject.put("Hamming-Match", targetString);
					matchObject.put("Hamming-Distance", Integer.toString(tempHammingDistance));
					objHammingDistance = tempHammingDistance;
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(con, null, stmt);
			}	
		}
		if (algorithmsSet.contains("jaroWrinklerDistance")){
			try{
				double tempJaroWrinklerDistance  = jaroWrinklerDistance.distance(sourceString, targetString);
				
				if  (objJaroWrinklerDistance > tempJaroWrinklerDistance){
					jaroWrinklerKey = targetKey;
					matchObject.put("JaroWrinkler-Match", targetString);
					matchObject.put("JaroWrinkler-Distance", Double.toString(round(tempJaroWrinklerDistance,2)));
					objJaroWrinklerDistance = tempJaroWrinklerDistance; 
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(con, null, stmt);
			}	
		}
		if (algorithmsSet.contains("sorensenDice")){
			try{
				double tempSorensenDice  = sorensenDice.distance(sourceString, targetString);
				if  (objSorensenDice > tempSorensenDice){
					sorensenDiceKey = targetKey;
					matchObject.put("SorensenDice-Match", targetString);
					matchObject.put("SorensenDice-Distance", Double.toString(round(tempSorensenDice,2)));
					objSorensenDice = tempSorensenDice; 
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(con, null, stmt);
			}	
		}
		if (algorithmsSet.contains("jaccard")){
			try{
				double tempJaccard  = jaccard.distance(sourceString, targetString);
				
				if  (objJaccard > tempJaccard){
					jaccardKey = targetKey;
					matchObject.put("Jaccard-Match", targetString);
					matchObject.put("Jaccard-Distance", Double.toString(round(tempJaccard,2)));
					objJaccard = tempJaccard; 
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(con, null, stmt);
			}	
		}
		if (algorithmsSet.contains("cosine")){
			try{
				double tempCosine  = cosine.distance(sourceString, targetString);
				if  (objCosine > tempCosine){
					cosineKey = targetKey;
					matchObject.put("Cosine-Match",targetString);
					matchObject.put("Cosine-Distance", Double.toString(round(tempCosine,2)));
					objCosine = tempCosine; 
				}
			} catch(Exception ex){
				PrismHandler.handleException(ex, true);
			}
			finally {
				PrismHandler.handleFinally(con, null, stmt);
			}	
		}
	}

	if (lvKey != 0)
		matchObject.putAll(getPrefixHashMap(targetSupportingData.get(lvKey),"LV"));
	if(hammingKey !=0)
		matchObject.putAll(getPrefixHashMap(targetSupportingData.get(hammingKey),"Hamming"));
	if (jaroWrinklerKey != 0)
		matchObject.putAll(getPrefixHashMap(targetSupportingData.get(jaroWrinklerKey),"JaroWrinkler"));
	if(sorensenDiceKey !=0)
		matchObject.putAll(getPrefixHashMap(targetSupportingData.get(sorensenDiceKey),"SorensenDice"));
	if (jaccardKey != 0)
		matchObject.putAll(getPrefixHashMap(targetSupportingData.get(jaccardKey),"Jaccard"));
	if(cosineKey !=0)
		matchObject.putAll(getPrefixHashMap(targetSupportingData.get(cosineKey),"Cosine"));
	JSONObject orderedJson = new JSONObject(matchObject);
	json.put(orderedJson);
		}
		 catch (Exception ex) {
				PrismHandler.handleException(ex, true);
			}
}
*/