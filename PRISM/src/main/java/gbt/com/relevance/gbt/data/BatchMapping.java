package com.relevance.gbt.data;

public class BatchMapping {
	String baseBatch;
	String baseMaterialId;
	String baseMaterialName;
	String baseSystem;
	String targetMaterialId;
	String targetMaterialName;
	String targetSystem;
	String targetBatch;
	String occurrence;
	String type;
	String sourceId;
	String targetId;
	
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
	public String getTargetId() {
		return targetId;
	}
	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}
	public String getOccurrence() {
		return occurrence;
	}
	public void setOccurrence(String occurrence) {
		this.occurrence = occurrence;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBaseBatch() {
		return baseBatch;
	}
	public void setBaseBatch(String baseBatch) {
		this.baseBatch = baseBatch;
	}
	public String getBaseMaterialId() {
		return baseMaterialId;
	}
	public void setBaseMaterialId(String baseMaterialId) {
		this.baseMaterialId = baseMaterialId;
	}
	public String getBaseMaterialName() {
		return baseMaterialName;
	}
	public void setBaseMaterialName(String baseMaterialName) {
		this.baseMaterialName = baseMaterialName;
	}
	public String getBaseSystem() {
		return baseSystem;
	}
	public void setBaseSystem(String baseSystem) {
		this.baseSystem = baseSystem;
	}
	public String getTargetMaterialId() {
		return targetMaterialId;
	}
	public void setTargetMaterialId(String targetMaterialId) {
		this.targetMaterialId = targetMaterialId;
	}
	public String getTargetMaterialName() {
		return targetMaterialName;
	}
	public void setTargetMaterialName(String targetMaterialName) {
		this.targetMaterialName = targetMaterialName;
	}
	public String getTargetSystem() {
		return targetSystem;
	}
	public void setTargetSystem(String targetSystem) {
		this.targetSystem = targetSystem;
	}
	public String getTargetBatch() {
		return targetBatch;
	}
	public void setTargetBatch(String targetBatch) {
		this.targetBatch = targetBatch;
	}
}
