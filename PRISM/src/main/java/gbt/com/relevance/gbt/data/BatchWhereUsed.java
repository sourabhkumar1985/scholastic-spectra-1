package com.relevance.gbt.data;

public class BatchWhereUsed {
	String client;
	String receiptIndicator;
	String plant;
	String material;
	String batch;
	String order;
	String itemNumber;
	String purchasingDocument;
	String item;
	String salesOrder;
	String salesOrderItem;
	String materialDocument;
	String materialDocYear;
	String zeile;
	String postingDate;
	String debitCreditInd;
	String movementType;
	String revmvmntTypeInd;
	String movementIndicator;
	String specialStock;
	String quantity;
	String baseUnitofMeasure;
	String zusch;
	String zustd;
	String availableFrom;
	String sledbbd;
	String receivingPlant;
	String receivingMaterial;
	String receivingBatch;
	String vendor;
	String vendorBatch;
	String customer;
	String itemCategory;
	String orderCategory;
	String delivery;
	String posnr;
	String dummy;
	String unRestricted;
	String stockInTransferCUMLM;
	String inQualityInsp;
	String restrictedUseStockCEINM;
	String blockedCSPEM; 
	String returnsCRETM;
	String unrestrUseStock;
	String stockInTransferCVMUM;
	String inQualityInspCVMIN;
	String restrictedUseStockCVMEI; 
	String blockedCVMSP;
	String returnsCVMRE;
	String key;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getReceiptIndicator() {
		return receiptIndicator;
	}
	public void setReceiptIndicator(String receiptIndicator) {
		this.receiptIndicator = receiptIndicator;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	public String getPurchasingDocument() {
		return purchasingDocument;
	}
	public void setPurchasingDocument(String purchasingDocument) {
		this.purchasingDocument = purchasingDocument;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getSalesOrder() {
		return salesOrder;
	}
	public void setSalesOrder(String salesOrder) {
		this.salesOrder = salesOrder;
	}
	public String getSalesOrderItem() {
		return salesOrderItem;
	}
	public void setSalesOrderItem(String salesOrderItem) {
		this.salesOrderItem = salesOrderItem;
	}
	public String getMaterialDocument() {
		return materialDocument;
	}
	public void setMaterialDocument(String materialDocument) {
		this.materialDocument = materialDocument;
	}
	public String getMaterialDocYear() {
		return materialDocYear;
	}
	public void setMaterialDocYear(String materialDocYear) {
		this.materialDocYear = materialDocYear;
	}
	public String getZeile() {
		return zeile;
	}
	public void setZeile(String zeile) {
		this.zeile = zeile;
	}
	public String getPostingDate() {
		return postingDate;
	}
	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}
	public String getDebitCreditInd() {
		return debitCreditInd;
	}
	public void setDebitCreditInd(String debitCreditInd) {
		this.debitCreditInd = debitCreditInd;
	}
	public String getMovementType() {
		return movementType;
	}
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	public String getRevmvmntTypeInd() {
		return revmvmntTypeInd;
	}
	public void setRevmvmntTypeInd(String revmvmntTypeInd) {
		this.revmvmntTypeInd = revmvmntTypeInd;
	}
	public String getMovementIndicator() {
		return movementIndicator;
	}
	public void setMovementIndicator(String movementIndicator) {
		this.movementIndicator = movementIndicator;
	}
	public String getSpecialStock() {
		return specialStock;
	}
	public void setSpecialStock(String specialStock) {
		this.specialStock = specialStock;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getBaseUnitofMeasure() {
		return baseUnitofMeasure;
	}
	public void setBaseUnitofMeasure(String baseUnitofMeasure) {
		this.baseUnitofMeasure = baseUnitofMeasure;
	}
	public String getZusch() {
		return zusch;
	}
	public void setZusch(String zusch) {
		this.zusch = zusch;
	}
	public String getZustd() {
		return zustd;
	}
	public void setZustd(String zustd) {
		this.zustd = zustd;
	}
	public String getAvailableFrom() {
		return availableFrom;
	}
	public void setAvailableFrom(String availableFrom) {
		this.availableFrom = availableFrom;
	}
	public String getSledbbd() {
		return sledbbd;
	}
	public void setSledbbd(String sledbbd) {
		this.sledbbd = sledbbd;
	}
	public String getReceivingPlant() {
		return receivingPlant;
	}
	public void setReceivingPlant(String receivingPlant) {
		this.receivingPlant = receivingPlant;
	}
	public String getReceivingMaterial() {
		return receivingMaterial;
	}
	public void setReceivingMaterial(String receivingMaterial) {
		this.receivingMaterial = receivingMaterial;
	}
	public String getReceivingBatch() {
		return receivingBatch;
	}
	public void setReceivingBatch(String receivingBatch) {
		this.receivingBatch = receivingBatch;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getVendorBatch() {
		return vendorBatch;
	}
	public void setVendorBatch(String vendorBatch) {
		this.vendorBatch = vendorBatch;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getItemCategory() {
		return itemCategory;
	}
	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}
	public String getOrderCategory() {
		return orderCategory;
	}
	public void setOrderCategory(String orderCategory) {
		this.orderCategory = orderCategory;
	}
	public String getDelivery() {
		return delivery;
	}
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	public String getPosnr() {
		return posnr;
	}
	public void setPosnr(String posnr) {
		this.posnr = posnr;
	}
	public String getDummy() {
		return dummy;
	}
	public void setDummy(String dummy) {
		this.dummy = dummy;
	}
	public String getUnRestricted() {
		return unRestricted;
	}
	public void setUnRestricted(String unRestricted) {
		this.unRestricted = unRestricted;
	}
	public String getStockInTransferCUMLM() {
		return stockInTransferCUMLM;
	}
	public void setStockInTransferCUMLM(String stockInTransferCUMLM) {
		this.stockInTransferCUMLM = stockInTransferCUMLM;
	}
	public String getInQualityInsp() {
		return inQualityInsp;
	}
	public void setInQualityInsp(String inQualityInsp) {
		this.inQualityInsp = inQualityInsp;
	}
	public String getRestrictedUseStockCEINM() {
		return restrictedUseStockCEINM;
	}
	public void setRestrictedUseStockCEINM(String restrictedUseStockCEINM) {
		this.restrictedUseStockCEINM = restrictedUseStockCEINM;
	}
	public String getBlockedCSPEM() {
		return blockedCSPEM;
	}
	public void setBlockedCSPEM(String blockedCSPEM) {
		this.blockedCSPEM = blockedCSPEM;
	}
	public String getReturnsCRETM() {
		return returnsCRETM;
	}
	public void setReturnsCRETM(String returnsCRETM) {
		this.returnsCRETM = returnsCRETM;
	}
	public String getUnrestrUseStock() {
		return unrestrUseStock;
	}
	public void setUnrestrUseStock(String unrestrUseStock) {
		this.unrestrUseStock = unrestrUseStock;
	}
	public String getStockInTransferCVMUM() {
		return stockInTransferCVMUM;
	}
	public void setStockInTransferCVMUM(String stockInTransferCVMUM) {
		this.stockInTransferCVMUM = stockInTransferCVMUM;
	}
	public String getInQualityInspCVMIN() {
		return inQualityInspCVMIN;
	}
	public void setInQualityInspCVMIN(String inQualityInspCVMIN) {
		this.inQualityInspCVMIN = inQualityInspCVMIN;
	}
	public String getRestrictedUseStockCVMEI() {
		return restrictedUseStockCVMEI;
	}
	public void setRestrictedUseStockCVMEI(String restrictedUseStockCVMEI) {
		this.restrictedUseStockCVMEI = restrictedUseStockCVMEI;
	}
	public String getBlockedCVMSP() {
		return blockedCVMSP;
	}
	public void setBlockedCVMSP(String blockedCVMSP) {
		this.blockedCVMSP = blockedCVMSP;
	}
	public String getReturnsCVMRE() {
		return returnsCVMRE;
	}
	public void setReturnsCVMRE(String returnsCVMRE) {
		this.returnsCVMRE = returnsCVMRE;
	}
}
