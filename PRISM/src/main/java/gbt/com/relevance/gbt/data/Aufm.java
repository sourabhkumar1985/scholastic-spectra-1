package com.relevance.gbt.data;
/**
 * 
 * E2emf GBT Pojo  
 *
 */
public class Aufm {
	
	private String id;
	
	private String workOrder;
	
	private String materialNum;
	
	private String referenceNum;
	
	private String batchNum;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWorkOrder() {
		return workOrder;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public String getReferenceNum() {
		return referenceNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setWorkOrder(String workOrder) {
		this.workOrder = workOrder;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	
}
