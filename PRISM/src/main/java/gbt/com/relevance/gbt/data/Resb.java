package com.relevance.gbt.data;
/**
 * 
 * E2emf GBT Pojo  
 *
 */
public class Resb {
	
	private String id; 
	
	private String workOrder;
	
	private String referenceNum;
	
	private String materialNum;
	
	private String batchNum;

	public String getId() {
		return id;
	}

	public String getWorkOrder() {
		return workOrder;
	}

	public String getReferenceNum() {
		return referenceNum;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setWorkOrder(String workOrder) {
		this.workOrder = workOrder;
	}

	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}
	
	

}
