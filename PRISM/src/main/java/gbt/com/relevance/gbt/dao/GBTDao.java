package com.relevance.gbt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.gbt.data.BatchMapping;
import com.relevance.gbt.data.BatchStocks;
import com.relevance.gbt.data.BatchWhereUsed;
import com.relevance.gbt.data.GbtSearchView;
import com.relevance.gbt.data.MaterialMapping;
import com.relevance.gbt.util.GbtConstants;
import com.relevance.gbt.util.GbtSolrUtil;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;

//import com.salesforce.phoenix.jdbc.PhoenixDriver;

/**
 *  E2emf Search Data Access Object
 * 
 */
public class GBTDao extends BaseDao{

	GbtSolrUtil gbtUtil = null;

	Connection con = null;
	Statement stmt = null;
	GbtSolrUtil solrUtil = null;
	String gbt_hbase_db = null;
	
	static String gbtQuery = "select id as batch,json as json from batch";
	static String gbtBottomUPQuery = "select id as batch,json as json from ForwardTraceability";
	static String gbtTopDownQuery = "select id as batch,json as json from BackwardTraceability";
	static String gbtBatchWhereUsedListQuery = "select key1, MANDT as client,XZUGA as receiptIndicator,WERKS as plant,MATNR as material,CHARG as batch,AUFNR as orderNumber,AUFPS as itemNumber,EBELN as purchasingDocument,EBELP as item,KDAUF as salesOrder,KDPOS as salesOrderItem,MBLNR as materialDocument,MJAHR as materialDocYear,ZEILE as zeile,BUDAT as postingDate,SHKZG as debitCreditInd,BWART as movementType,XSTBW as revmvmnttypeInd,KZBEW as movementIndicator,SOBKZ as specialStock,MENGE as quantity,MEINS as baseUnitofMeasure,ZUSCH as zusch,ZUSTD as zustd,VERAB as availableFrom,VFDAT as sledbbd,UMWRK as receivingPlant,UMMAT as receivingMaterial,UMCHA as receivingBatch,LIFNR as vendor,LICHA as vendorBatch,KUNNR as customer,PSTYP as itemCategory,AUTYP as orderCategory from ";
	//static String gbtBatchStocksQuery_part1 = "select  key1,CASE WHEN CLABS != '0.000' THEN 'Unrestricted' WHEN CUMLM != '0.000' THEN 'Stock in transfer' WHEN CINSM != '0.000' THEN 'In Quality Insp.' WHEN CEINM != '0.000' THEN 'Restricted-Use Stock' WHEN CSPEM != '0.000' THEN 'Blocked' WHEN CRETM != '0.000' THEN 'Returns' WHEN CVMLA != '0.000' THEN 'Unrestr.-use stock' WHEN CVMUM != '0.000' THEN 'Stock in transfer' WHEN CVMIN != '0.000' THEN 'In quality insp.' WHEN CVMEI != '0.000' THEN 'Restricted-use stock' WHEN CVMSP != '0.000' THEN 'Blocked' WHEN CVMRE != '0.000' THEN 'Returns' END as qualifier,CASE WHEN CLABS != '0.000' THEN CLABS WHEN CUMLM != '0.000' THEN CUMLM WHEN CINSM != '0.000' THEN CINSM WHEN CEINM != '0.000' THEN CEINM WHEN CSPEM != '0.000' THEN CSPEM WHEN CRETM != '0.000' THEN CRETM WHEN CVMLA != '0.000' THEN CVMLA WHEN CVMUM != '0.000' THEN CVMUM WHEN CVMIN != '0.000' THEN CVMIN  WHEN CVMEI != '0.000' THEN CVMEI  WHEN CVMSP != '0.000' THEN CVMSP  WHEN CVMRE != '0.000' THEN CVMRE  END as quantity,CLABS as unRestricted, CUMLM as stockInTransfer,CINSM as inQualityInsp,CEINM as restrictedUseStock,CSPEM as Blocked, CRETM as ReturnsCRETM, CVMLA as UnrestrUseStock, CVMUM as StockInTransfer, CVMIN as InQualityInsp, CVMEI as RestrictedUseStock,CVMSP as Blocked, CVMRE as ReturnsCVMRE, MANDT as Client, MATNR as material,WERKS as plant,LGORT as storagelocation,CHARG as batch,LVORM as stockdeletionflag,ERSDA as createdon, ERNAM as createdby,LAEDA as lastchange,AENAM as changedby,LFGJA as yearcurrentperiod,LFMON as currentperiod,KZICL as warehousestockcy,KZICQ as qualityinspectionstockcy,KZICE as restrictedusestock,KZICS as blockedstock,KZVCL as warehousestockpy,KZVCQ as qualInspstockprvpd,KZVCE as restrictedusepp,KZVCS as blckedstockprevpd,HERKL as Countryoforigin,CHDLL as DateofLastCount,CHJIN as CHJIN,CHRUE as CHRUE from ";
	static String gbtBatchStocksQuery_part1 = "select  key1,CASE WHEN CLABS != '0.000' THEN 'Unrestricted' WHEN CUMLM != '0.000' THEN 'Stock in transfer' WHEN CINSM != '0.000' THEN 'In Quality Insp.' WHEN CEINM != '0.000' THEN 'Restricted-Use Stock' WHEN CSPEM != '0.000' THEN 'Blocked' WHEN CRETM != '0.000' THEN 'Returns' WHEN CVMLA != '0.000' THEN 'Unrestr.-use stock' WHEN CVMUM != '0.000' THEN 'Stock in transfer' WHEN CVMIN != '0.000' THEN 'In quality insp.' WHEN CVMEI != '0.000' THEN 'Restricted-use stock' WHEN CVMSP != '0.000' THEN 'Blocked' WHEN CVMRE != '0.000' THEN 'Returns' END as qualifier,CASE WHEN CLABS != '0.000' THEN CLABS WHEN CUMLM != '0.000' THEN CUMLM WHEN CINSM != '0.000' THEN CINSM WHEN CEINM != '0.000' THEN CEINM WHEN CSPEM != '0.000' THEN CSPEM WHEN CRETM != '0.000' THEN CRETM WHEN CVMLA != '0.000' THEN CVMLA WHEN CVMUM != '0.000' THEN CVMUM WHEN CVMIN != '0.000' THEN CVMIN  WHEN CVMEI != '0.000' THEN CVMEI  WHEN CVMSP != '0.000' THEN CVMSP  WHEN CVMRE != '0.000' THEN CVMRE  END as quantity,CLABS as unRestricted, CUMLM as stockInTransfer,CINSM as inQualityInsp,CEINM as restrictedUseStock, CRETM as ReturnsCRETM, CVMLA as UnrestrUseStock, CVMSP as Blocked, CVMRE as ReturnsCVMRE, MANDT as Client, MATNR as material,WERKS as plant,LGORT as storagelocation,CHARG as batch,LVORM as stockdeletionflag,ERSDA as createdon, ERNAM as createdby,LAEDA as lastchange,AENAM as changedby,LFGJA as yearcurrentperiod,LFMON as currentperiod,KZICL as warehousestockcy,KZICQ as qualityinspectionstockcy,KZICS as blockedstock,KZVCL as warehousestockpy,KZVCQ as qualInspstockprvpd,KZVCE as restrictedusepp,KZVCS as blckedstockprevpd,HERKL as Countryoforigin,CHDLL as DateofLastCount,CHJIN as CHJIN,CHRUE as CHRUE from ";
	static String gbtBatchStocksQuery_part2 = " where (CLABS != '0.000' or CUMLM != '0.000' or CINSM != '0.000' or CEINM != '0.000' or CVMEI != '0.000' or CVMSP != '0.000' or CVMRE != '0.000')";
	//static String materialMappingQuery = "select * from materialmapping";
	static String materialMappingQuery = "select * from ";
	//static String batchMappingQuery = "select * from batchmapping";
	static String batchMappingQuery = "select * from ";

	public GBTDao() {
		solrUtil = new GbtSolrUtil();
		gbt_hbase_db = solrUtil.getAppProperty(GbtConstants.hbaseDB);
	}

	public String executeQuery(String batch) throws AppException {

		Log.Info("ExecuteQuery called with batch \n " + batch);
		StringBuilder query = null;
		String jsonStr = null;
		StringBuilder where = new StringBuilder(" where id = '");
		where.append(batch).append("'");

		try {

			stmt = dbUtil.getPostGresConnection().createStatement();
			query = new StringBuilder(gbtQuery).append(where);

			ResultSet rs = stmt.executeQuery(query.toString());
			Log.Info("Resultset fetched with size " + rs.getFetchSize());

			if (rs != null) {
				while (rs.next()) {
					jsonStr = rs.getString("json");
				}
			}

		} catch (SQLException sql) {
			Log.Error("SQLException while executing Query."
					+ sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception in executing Query." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}

		return jsonStr;
	}

	public String executeRelatedBatchQuery(String batch, String type)
			throws AppException {

		Log.Info("ExecuteQuery called with batch \n " + batch);
		StringBuilder query = null;
		String jsonStr = null;
		StringBuilder where = new StringBuilder(" where id = '");
		where.append(batch).append("'");

		try {

			con = dbUtil.getHiveConnection(E2emfConstants.gbtDB);

			stmt = con.createStatement();

			if (type != null && type.equalsIgnoreCase("bottomup")) {
				query = new StringBuilder(gbtBottomUPQuery).append(where);
			} else {
				query = new StringBuilder(gbtTopDownQuery).append(where);
			}
			ResultSet rs = stmt.executeQuery(query.toString());
			Log.Info("Resultset fetched with size " + rs.getFetchSize());

			if (rs != null) {
				while (rs.next()) {
					jsonStr = rs.getString("json");
				}
			}
			if (jsonStr == null) {
				jsonStr = "null";
			}

		} catch (SQLException sql) {
			Log.Error("SQLException while executing Query."
					+ sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception in executing Query." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}

		return jsonStr;
	}

	public GbtSearchView executegetBatchWhereUsedListQuery(String batchs, String system)
			throws AppException {

		GbtSearchView gbtSearchView = null;
		StringBuilder query = null;
		String jsonStr = null;

				
		String batchArray [] = batchs.split(",");
		StringBuffer batchList = new StringBuffer();
		batchList.append("'");
		for (int i=0; i<batchArray.length; i++){
			String materialBatch = batchArray[i];
			if(i == (batchArray.length-1)){
				String batchString = materialBatch.substring(materialBatch.lastIndexOf("-") + 1, materialBatch.length() - 1);
				batchList.append(batchString);
			}
			else{
				String batchString = materialBatch.substring(materialBatch.lastIndexOf("-") + 1, materialBatch.length() - 1);
				batchList.append(batchString);
				batchList.append("','");
			}
		}
		batchList.append("'");
		StringBuilder where = new StringBuilder(" where key1 in (");
		where.append(batchs).append(")");
		try {
			con = dbUtil.getPhoenixConnection();

			query = new StringBuilder(gbtBatchWhereUsedListQuery + gbt_hbase_db + "." + "CHVW").append(where);
			Log.Info("ExecuteQuery called with batch \n " + query.toString());
			PreparedStatement statement = con
					.prepareStatement(query.toString());
			ResultSet rs = statement.executeQuery();
			gbtSearchView = new GbtSearchView();
			BatchWhereUsed batchWhereUsed = null;
			List<BatchWhereUsed> batchWhereUsedList = new ArrayList<BatchWhereUsed>();
			Log.Info("Resultset fetched with size " + rs.getFetchSize());

			if (rs != null) {
				while (rs.next()) {
					batchWhereUsed = new BatchWhereUsed();
					batchWhereUsed.setKey(rs.getString("key1"));
					batchWhereUsed.setClient(rs.getString("client"));
					batchWhereUsed.setReceiptIndicator(rs
							.getString("receiptindicator"));
					batchWhereUsed.setPlant(rs.getString("plant"));
					batchWhereUsed.setMaterial(rs.getString("material"));
					batchWhereUsed.setBatch(rs.getString("batch"));
					batchWhereUsed.setOrder(rs.getString("ordernumber"));
					batchWhereUsed.setItemNumber(rs.getString("itemnumber"));
					batchWhereUsed.setPurchasingDocument(rs
							.getString("purchasingdocument"));
					batchWhereUsed.setItem(rs.getString("item"));
					batchWhereUsed.setSalesOrder(rs.getString("salesorder"));
					batchWhereUsed.setSalesOrderItem(rs
							.getString("salesorderitem"));
					batchWhereUsed.setMaterialDocument(rs
							.getString("materialdocument"));
					batchWhereUsed.setMaterialDocYear(rs
							.getString("materialdocyear"));
					batchWhereUsed.setZeile(rs.getString("zeile"));
					batchWhereUsed.setPostingDate(rs.getString("postingdate"));
					batchWhereUsed.setDebitCreditInd(rs
							.getString("debitcreditind"));
					batchWhereUsed
							.setMovementType(rs.getString("movementtype"));
					batchWhereUsed.setRevmvmntTypeInd(rs
							.getString("revmvmnttypeind"));
					batchWhereUsed.setMovementIndicator(rs
							.getString("movementindicator"));
					batchWhereUsed
							.setSpecialStock(rs.getString("specialstock"));
					batchWhereUsed.setQuantity(rs.getString("quantity"));
					batchWhereUsed.setBaseUnitofMeasure(rs
							.getString("baseunitofmeasure"));
					batchWhereUsed.setZusch(rs.getString("zusch"));
					batchWhereUsed.setZustd(rs.getString("zustd"));
					batchWhereUsed.setAvailableFrom(rs
							.getString("availablefrom"));
					batchWhereUsed.setSledbbd(rs.getString("sledbbd"));
					batchWhereUsed.setReceivingPlant(rs
							.getString("receivingplant"));
					batchWhereUsed.setReceivingMaterial(rs
							.getString("receivingmaterial"));
					batchWhereUsed.setReceivingBatch(rs
							.getString("receivingbatch"));
					batchWhereUsed.setVendor(rs.getString("vendor"));
					batchWhereUsed.setVendorBatch(rs.getString("vendorbatch"));
					batchWhereUsed.setCustomer(rs.getString("customer"));
					batchWhereUsed
							.setItemCategory(rs.getString("itemcategory"));
					batchWhereUsed.setOrderCategory(rs
							.getString("ordercategory"));
					batchWhereUsedList.add(batchWhereUsed);
				}
			}
			Log.Info("Fetched " + batchWhereUsedList.size());
			gbtSearchView.setBatchWhereUsedList(batchWhereUsedList);
			if (jsonStr == null) {
				jsonStr = "null";
			}

		} catch (SQLException sql) {
			Log.Error("SQLException while executing Query."
					+ sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception in executing Query." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}

		return gbtSearchView;
	}

	public GbtSearchView executegetBatchStock(String batchs, String system)
			throws AppException {

		GbtSearchView gbtSearchView = null;
		StringBuilder query = null;
		String jsonStr = null;
		
		String batchArray [] = batchs.split(",");
		StringBuffer batchList = new StringBuffer();
		batchList.append("'");
		for (int i=0; i<batchArray.length; i++){
			String materialBatch = batchArray[i];
			if(i == (batchArray.length-1)){
				String batchString = materialBatch.substring(materialBatch.indexOf("-") + 1, materialBatch.length() - 1);
				batchList.append(batchString);
			}
			else{
				String batchString = materialBatch.substring(materialBatch.indexOf("-") + 1, materialBatch.length() - 1);
				batchList.append(batchString);
				batchList.append("','");
			}
		}
		batchList.append("'");
		StringBuilder where = new StringBuilder(" and key1 in (");
		where.append(batchs).append(")");
		try {
			con = dbUtil.getPhoenixConnection();
			query = new StringBuilder(gbtBatchStocksQuery_part1 + gbt_hbase_db + "." + "MCHB" + gbtBatchStocksQuery_part2).append(where);
			Log.Info("ExecuteQuery called with batch \n " + query.toString());
			PreparedStatement statement = con
					.prepareStatement(query.toString());
			ResultSet rs = statement.executeQuery();
			gbtSearchView = new GbtSearchView();
			BatchStocks batchStocks = null;
			List<BatchStocks> batchStocksList = new ArrayList<BatchStocks>();
			Log.Info("Resultset fetched with size " + rs.getFetchSize());

			if (rs != null) {
				while (rs.next()) {
					batchStocks = new BatchStocks();
					batchStocks.setKey(rs.getString("key1"));

					batchStocks.setClient(rs.getString("client"));
					batchStocks.setMaterial(rs.getString("material"));
					batchStocks.setPlant(rs.getString("plant"));
					batchStocks.setStorageLocation(rs
							.getString("storagelocation"));
					batchStocks.setBatch(rs.getString("batch"));
					batchStocks.setStockDeletionFlag(rs
							.getString("stockdeletionflag"));
					batchStocks.setCreatedOn(rs.getString("createdon"));
					batchStocks.setCreatedBy(rs.getString("createdby"));
					batchStocks.setLastChange(rs.getString("lastchange"));
					batchStocks.setChangedBy(rs.getString("changedby"));
					batchStocks.setYearCurrentPeriod(rs
							.getString("yearcurrentperiod"));
					batchStocks.setCurrentPeriod(rs.getString("currentperiod"));
					batchStocks.setQuantity(rs.getString("quantity"));
					batchStocks.setQualifier(rs.getString("qualifier"));
					batchStocks.setCountryOfOrigin(rs
							.getString("countryoforigin"));
					batchStocks.setDateofLastCount(rs
							.getString("dateoflastcount"));
					batchStocks.setChjin(rs.getString("chjin"));
					batchStocks.setChrue(rs.getString("chrue"));
					batchStocks.setWarehouseStockCY(rs
							.getString("warehousestockcy"));
					batchStocks.setQualityInspectionStockCY(rs
							.getString("qualityinspectionstockcy"));
					batchStocks.setRestrictedUseStock(rs
							.getString("restrictedusestock"));
					batchStocks.setBlockedStock(rs.getString("blockedstock"));
					batchStocks.setWarehouseStockPY(rs
							.getString("warehousestockpy"));
					batchStocks.setQualInspStockPrvPD(rs
							.getString("qualinspstockprvpd"));
					batchStocks.setRestrictedUsePP(rs
							.getString("restrictedusepp"));
					batchStocks.setBlckedStockPrevPD(rs
							.getString("blckedstockprevpd"));
					batchStocksList.add(batchStocks);
				}
			}
			Log.Info("Fetched " + batchStocksList.size());
			gbtSearchView.setBatchStocksList(batchStocksList);
			if (jsonStr == null) {
				jsonStr = "null";
			}

		} catch (SQLException sql) {
			Log.Error("SQLException while executing Query."
					+ sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception in executing Query." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}

		return gbtSearchView;
	}

	public GbtSearchView getMaterialMapping() throws AppException {

		GbtSearchView gbtSearchView = null;
		try {
			con = dbUtil.getPhoenixConnection();
			Log.Info("ExecuteQuery called with batch \n " + materialMappingQuery);
			PreparedStatement statement = con
					.prepareStatement(materialMappingQuery + gbt_hbase_db + "." + "materialmapping");
			ResultSet rs = statement.executeQuery();
			gbtSearchView = new GbtSearchView();
			MaterialMapping materialMapping = null;
			List<MaterialMapping> materialMappingList = new ArrayList<MaterialMapping>();
			Log.Info("Resultset fetched with size " + rs.getFetchSize());

			if (rs != null) {
				while (rs.next()) {
					materialMapping = new MaterialMapping();
					materialMapping.setBaseMaterialId(rs
							.getString("baseitemid"));
					materialMapping.setBaseMaterialName(rs
							.getString("baseitemname"));
					materialMapping.setBaseSystem(rs
							.getString("basesystem"));
					materialMapping.setTargetMaterialId(rs
							.getString("targetitemid"));
					materialMapping.setTargetMaterialName(rs
							.getString("targetitemname"));
					materialMapping.setTargetSystem(rs
							.getString("targetsystem"));
					materialMapping.setOccurrence(rs
							.getString("occurrence"));
					materialMapping.setType(rs
							.getString("type"));
					materialMappingList.add(materialMapping);
				}
			}
			Log.Info("Fetched " + materialMappingList.size());
			gbtSearchView.setMaterialMappingList(materialMappingList);

		} catch (SQLException sql) {
			Log.Error("SQLException while executing Query."
					+ sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception in executing Query." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}

		return gbtSearchView;
	}

	public GbtSearchView getBatchMapping(String batchsString) throws AppException {

		GbtSearchView gbtSearchView = null;
		StringBuilder query = null;
		StringBuilder where = new StringBuilder(" where id in (");
		where.append(batchsString).append(")");

		try {
			con = dbUtil.getPhoenixConnection();
			Log.Info("ExecuteQuery called with batch \n " + batchMappingQuery);
			query = new StringBuilder(batchMappingQuery + gbt_hbase_db + "." + "batchmapping");
			if (batchsString  != null && !batchsString.isEmpty() && !batchsString.equalsIgnoreCase("null"))
				query = query.append(where);
				//query = new StringBuilder(batchMappingQuery).append(where);
			Log.Info("ExecuteQuery called with batch \n " + query.toString());
			PreparedStatement statement = con
					.prepareStatement(query.toString());
			
			ResultSet rs = statement.executeQuery();
			gbtSearchView = new GbtSearchView();
			BatchMapping batchMapping = null;
			List<BatchMapping> batchMappingList = new ArrayList<BatchMapping>();
			Log.Info("Resultset fetched with size " + rs.getFetchSize());

			if (rs != null) {
				while (rs.next()) {
					batchMapping = new BatchMapping();
					batchMapping.setBaseBatch(rs
							.getString("basebatch"));
					batchMapping.setBaseMaterialId(rs
							.getString("baseitemid"));
					batchMapping.setBaseMaterialName(rs
							.getString("baseitemname"));
					batchMapping.setBaseSystem(rs
							.getString("basesystem"));
					batchMapping.setTargetMaterialId(rs
							.getString("targetitemid"));
					batchMapping.setTargetMaterialName(rs
							.getString("targetitemname"));
					batchMapping.setTargetSystem(rs
							.getString("targetsystem"));
					batchMapping.setOccurrence(rs
							.getString("typevarcharoccurrence"));
					batchMapping.setTargetBatch(rs
							.getString("targetbatch"));
					batchMapping.setSourceId(rs
							.getString("baseitemid")+"-"+rs
							.getString("basebatch"));
					batchMapping.setTargetId(rs
							.getString("targetitemid")+"-"+rs
							.getString("targetbatch"));
					batchMappingList.add(batchMapping);
				}
			}
			Log.Info("Fetched " + batchMappingList.size());
			gbtSearchView.setBatchMappingList(batchMappingList);

		} catch (SQLException sql) {
			Log.Error("SQLException while executing Query."
					+ sql);
			throw new AppException(sql.getMessage(),
					"Error while processing the request.", 1, sql.getCause(),
					true);
		} catch (Exception e) {
			Log.Error("Exception in executing Query." + e);
			if (e instanceof NullPointerException) {
				throw new AppException("NullPointerException",
						e.getStackTrace(),
						"Error while processing the request.", 3, e.getCause(),
						true);
			}
			throw new AppException(e.getMessage(),
					"Error while processing the request.", 2, e.getCause(),
					true);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Log.Error(e);
					throw new AppException(e.getMessage(),
							"Error while processing the request.", 1,
							e.getCause(), true);
				}

		}

		return gbtSearchView;
	}

}
