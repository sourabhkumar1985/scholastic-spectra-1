package com.relevance.gbt.util;

public class GbtConstants {

	public static final String solarServerUrl = "SOLR_SERVER_URL";
	public static final String gbtSLRSolarInstance = "SOLR_SERVER_SLR_INSTANCE_NAME";
	public static final String gbtALTSolarInstance = "SOLR_SERVER_ALT_INSTANCE_NAME";
	public static final String rocksDBServiceURL = "ROCKSDB_SERVICE_PATH";
	public static final String hbaseDB = "HBASE_DB";
	
	//Solar Params
	public static final String definitionType = "DEFTYPE";
	public static final String fetchFormat = "WT";
	public static final String startIndex = "START";
	public static final String noOfRows = "ROWS";
	public static final String queryFilter = "QF";
	public static final String filterQuery = "FQ";
	
	public static final String postGresConnURL = "CONNECTION_URL_POSTGRES";
	public static final String postGresConnDriver = "CONNECTION_DRIVER_POSTGRES";
	public static final String postGresUser = "CONNECTION_USER";
	public static final String postGresPwd = "CONNECTION_PWD";
	
	
	

}
