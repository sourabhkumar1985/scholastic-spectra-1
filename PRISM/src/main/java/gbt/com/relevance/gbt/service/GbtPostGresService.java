package com.relevance.gbt.service;

import com.relevance.gbt.dao.GBTDao;
import com.relevance.gbt.data.GbtSearchView;
import com.relevance.gbt.util.GbtSolrUtil;
import com.relevance.prism.util.AppException;
/**
 * 
 * GBT Search Service 
 *
 */
public class GbtPostGresService  {
	
	GbtSolrUtil solrUtil;
	
	
	public GbtPostGresService() {


	}

	public String getBatchJSON(String batch) throws AppException{
		try{
			String batchJSON = null;
					
			GBTDao gbtdao = new GBTDao();
			batchJSON = gbtdao.executeQuery(batch);	
			
			return batchJSON;
		}catch(AppException appe){
			throw appe;
		}
	}
	
	public String getRelatedBatchs(String batch, String type) throws AppException{
		try{
			String relatedBatchJSON = null;
					
			GBTDao gbtdao = new GBTDao();
			relatedBatchJSON = gbtdao.executeRelatedBatchQuery(batch, type);	
			
			return relatedBatchJSON;
		}catch(AppException appe){
			throw appe;
		}
	}
	
	public GbtSearchView getBatchWhereUsedList(String batchs, String system) throws AppException{
		try{
			GbtSearchView gbtSearchView = null;
					
			GBTDao gbtdao = new GBTDao();
			gbtSearchView = gbtdao.executegetBatchWhereUsedListQuery(batchs, system);	
			
			return gbtSearchView;
		}catch(AppException appe){
			throw appe;
		}
	}
	
	public GbtSearchView getBatchStocksList(String batchs, String system) throws AppException{
		try{
			GbtSearchView gbtSearchView = null;
					
			GBTDao gbtdao = new GBTDao();
			gbtSearchView = gbtdao.executegetBatchStock(batchs, system);	
			
			return gbtSearchView;
		}catch(AppException appe){
			throw appe;
		}
	}
	
	public GbtSearchView getBatchMapping(String batchsString) throws AppException{
		try{
			GbtSearchView gbtSearchView = null;
					
			GBTDao gbtdao = new GBTDao();
			gbtSearchView = gbtdao.getBatchMapping(batchsString);	
			
			return gbtSearchView;
		}catch(AppException appe){
			throw appe;
		}
	}
	
	public GbtSearchView getMaterialMapping() throws AppException{
		try{
			GbtSearchView gbtSearchView = null;
					
			GBTDao gbtdao = new GBTDao();
			gbtSearchView = gbtdao.getMaterialMapping();	
			
			return gbtSearchView;
		}catch(AppException appe){
			throw appe;
		}
	}
}
