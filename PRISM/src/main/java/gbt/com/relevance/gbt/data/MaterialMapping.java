package com.relevance.gbt.data;

public class MaterialMapping {
	String baseMaterialId;
	String baseMaterialName;
	String baseSystem;
	String targetMaterialId;
	String targetMaterialName;
	String targetSystem;
	String occurrence;
	
	public String getOccurrence() {
		return occurrence;
	}
	public void setOccurrence(String occurrence) {
		this.occurrence = occurrence;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	String type;
	public String getBaseMaterialId() {
		return baseMaterialId;
	}
	public void setBaseMaterialId(String baseMaterialId) {
		this.baseMaterialId = baseMaterialId;
	}
	public String getBaseMaterialName() {
		return baseMaterialName;
	}
	public void setBaseMaterialName(String baseMaterialName) {
		this.baseMaterialName = baseMaterialName;
	}
	public String getTargetMaterialId() {
		return targetMaterialId;
	}
	public void setTargetMaterialId(String targetMaterialId) {
		this.targetMaterialId = targetMaterialId;
	}
	public String getTargetMaterialName() {
		return targetMaterialName;
	}
	public void setTargetMaterialName(String targetMaterialName) {
		this.targetMaterialName = targetMaterialName;
	}
	public String getBaseSystem() {
		return baseSystem;
	}
	public void setBaseSystem(String baseSystem) {
		this.baseSystem = baseSystem;
	}
	public String getTargetSystem() {
		return targetSystem;
	}
	public void setTargetSystem(String targetSystem) {
		this.targetSystem = targetSystem;
	}
}
