package com.relevance.gbt.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.log4j.*;
import org.apache.log4j.xml.DOMConfigurator;

import com.relevance.prism.util.Log;

/**
 * 
 * E2emf Log Utility  
 *
 */
public class GbtLog {

	public static int LOG_REFRESH_FREQ = 5*60; //seconds
	
	public static final int HIGH = 5;
    public static final int MED = 3;
    public static final int LOW = 1;
    
  
	public static Logger _gbtlogger = null;
    public static Logger _gbtErrorlogger = null;
	  
	public static boolean isGbtLoggerInitialized = false;
	
	public static String logFile = "log4j.xml";

	public static void initialize(String appName){
		
		try{
			//PropertyConfigurator.configure("log4j.properties");		
			
			System.out.println("Initializing GBT Logger ...");
			
			if(!isGbtLoggerInitialized){
				
				//System.setProperty("log4j.defaultInitOverride", "false");
				Log.class.getClassLoader().getResourceAsStream(logFile);
				
				DOMConfigurator.configureAndWatch(logFile, Log.LOG_REFRESH_FREQ * 1000);
				
				_gbtlogger = Logger.getLogger("gbtInfo");
				
				_gbtErrorlogger = Logger.getLogger("gbtError");
				
				isGbtLoggerInitialized = true;		
				//System.out.println("Configuring  GBT logger " + _gbtlogger + " and " + _gbtErrorlogger);
				
				_gbtlogger.info("Info Logger Initialized ...");
				_gbtErrorlogger.error("Error Logger Initialized ...");
			}
			
		}catch(Exception e) {
			System.err.println("Exception initializing the GBT logger..." + e);
			PropertyConfigurator.configure(logFile);
			
		}
		
        _gbtlogger.info( _gbtlogger.getName()+ " Info Log Initialized ");
        
        _gbtErrorlogger.error(_gbtErrorlogger.getName() + " Error Log Initialized... ");
		
	}
	
	
	
	
	
		
	   /**
     *  Write "Error" to syslog.
     *  To be used for logging errors e.g. "db connection failed".
     */
    public static void Error(String msg) {
        try {
          
        	_gbtErrorlogger.error(msg);          
          
        } catch (Exception ex) {
        	System.out.println("Exception logging Error..." + ex); // ignore
        }
    }

    /**
     *  This method will print stack traces to the Syslogs.
     *  @param throwable error object.
     */
    public static void Error(Throwable throwable) {
        try {
        	_gbtErrorlogger.error("",throwable);
           
        } catch (Exception ex) {
           System.out.println("Exception logging Error..." + ex); // ignore
        }
    }
    
    /**
     *  Write "Warning" to syslog.
     *  To be used for logging warnings.
     */
    public static void Info(String msg) {
        try {
           
        	_gbtlogger.info(msg);
           } catch (Exception ex) {
        	   System.out.println("Exception logging Error..." + ex); // ignore
        }
    }


    /**
     *  Write "Warning" to syslog.
     *  To be used for logging warnings.
     */
    public static void Warning(String msg) {
        try {
        	_gbtlogger.warn(msg);             
        	} catch (Exception ex) {
        		System.out.println("Exception logging Error..." + ex); // ignore
        }
    }
    
    
    /**
     * Write "Debug Trace" to syslog.<br>
     *
     *  To be used for logging debug messages.
     *  Current tracelevel is set as a property in the database.
     *  Programmer should set the trace level to HIGH for things
     *  like "entering Constructor()", etc.
     */
    public static void Debug(int level, String msg) {
    	
    	if(_gbtlogger != null && _gbtlogger.isDebugEnabled())
        {
    		_gbtlogger.debug(msg);
        }         
    }

    public static void Debug(String msg) {
    	if(_gbtlogger != null && _gbtlogger.isDebugEnabled())
        {
    		_gbtlogger.debug(msg);
        }         
    }
    
    public static String getStackTrace(Throwable aThrowable) {
        String ret = "";
        try
        {
            Writer result = new StringWriter();
            PrintWriter printWriter= new PrintWriter(result);
            //aThrowable.printStackTrace(printWriter);
            System.out.println("Error Occured..." + aThrowable); // ignore
            ret= result.toString();
            result.close();
            printWriter.close();
        }
        catch(Exception e)
        {
        	System.out.println("Exception writing throwable Exception to the PrintWriter..." + e);
        }

        return ret;
    }
	
    
}