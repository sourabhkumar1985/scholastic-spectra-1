package com.relevance.gbt.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.gbt.data.Afko;
import com.relevance.gbt.data.Afpo;
import com.relevance.gbt.data.Aufm;
import com.relevance.gbt.data.GbtSearchView;
import com.relevance.gbt.data.Makt;
import com.relevance.gbt.data.Mara;
import com.relevance.gbt.data.Resb;
/**
 * 
 * E2emf GBT Solr Util  
 *
 */
public class GbtSolrUtil {

	protected static Properties solarProps = new Properties();

	public static final String propFile = "gbt.properties";

	private static String solarServerUrl = null;

	private static String gbtSolarInstance = null;

	private static boolean isSolarInitialized = false;
	
	Connection connection = null;
	
	private static String postGresConnURL= null;
	
	private static String postGresConnDriver= null;
	
	//private static String user= null;
	
	//private static String pwd= null;

	public GbtSolrUtil() {

		if (!GbtLog.isGbtLoggerInitialized)
			GbtLog.initialize("gbt");
		//System.out.println("Initializing the Solar...");
		initializeSolar();

	}

	private static void initializeSolar() {

		try {

			if (!isSolarInitialized) {
				GbtLog.Info("Loading gbt.properties");
				solarProps.load(GbtSolrUtil.class.getClassLoader().getResourceAsStream(propFile));
				GbtLog.Info("Loaded GBT Properties Successfully... ");
				solarServerUrl = solarProps.getProperty(GbtConstants.solarServerUrl);
				gbtSolarInstance = solarProps.getProperty(GbtConstants.gbtSLRSolarInstance);
				postGresConnURL = solarProps.getProperty(GbtConstants.postGresConnURL);
				postGresConnDriver = solarProps.getProperty(GbtConstants.postGresConnDriver);
				//user = solarProps.getProperty(GbtConstants.postGresUser);
				//pwd = solarProps.getProperty(GbtConstants.postGresPwd);
				
				isSolarInitialized = true;
				GbtLog.Info(" Fetched solarServerUrl: " + solarServerUrl
						+ "\n gbtSolarInstance: " + gbtSolarInstance);
				GbtLog.Info("Fetched postgres details: " + postGresConnURL	+ "\n postgresDriver: " + postGresConnDriver);

			}

		} catch (IOException e) {
			GbtLog.Error("IOException in loading the files" + e);
		} catch (Exception e) {
			GbtLog.Error("Exception in loading the files" + e);
		}

	}

	public String getAppProperty(String key){	
		try {
			solarProps.load(GbtSolrUtil.class.getClassLoader().getResourceAsStream(propFile));
			return solarProps.getProperty(key,"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			GbtLog.Error(e);
		}
		return null;
	}
	
	public HttpSolrServer getSolarConnection() {

		HttpSolrServer solarServerCon = null;
		String solarFqdn = null;
		try {
			solarFqdn = solarServerUrl + gbtSolarInstance;
			solarServerCon = new HttpSolrServer(solarFqdn);
		} catch (Exception e) {
			GbtLog.Error("Exception establishing connection to Solar "
					+ e);
		}

		return solarServerCon;

	}

	public Map<String, List<String>> extractRequestParams(
			Object... requestParams) {

		Map<String, List<String>> facetParamListMap = new HashMap<String, List<String>>();
		List<String> paramList = new ArrayList<String>();

		String reqParam = (String) requestParams[0];
		String[] parameters = reqParam.split(",");

		String queryParam = parameters[0];
		String facetParam = parameters[1];

		System.out.println("queryParam " + queryParam + " facetParam : "
				+ facetParam);
		String[] queryTokens = queryParam.split(":");
		String queryKeyToken = queryTokens[0];
		String queryValueToken = queryTokens[1];

		System.out.println("queryKeyToken " + queryKeyToken
				+ " queryValueToken : " + queryValueToken);

		StringTokenizer st = new StringTokenizer(queryValueToken);
		StringBuilder queryStr = new StringBuilder();
		boolean isFirstToken = true;
		while (st.hasMoreTokens()) {

			if (isFirstToken) {
				queryStr.append("*").append(st.nextToken()).append("*");
				isFirstToken = false;
			} else {
				queryStr.append(st.nextToken()).append("*");
			}

		}

		paramList.add(queryStr.toString());

		String[] facetKeyValue = facetParam.split(":");
		String facetValue = facetKeyValue[1];

		System.out.println("Adding the facet " + facetValue
				+ " to the Map for list " + queryStr.toString());
		facetParamListMap.put(facetValue, paramList);

		return facetParamListMap;
	}

	public ModifiableSolrParams getModifiableSolarRequestParams(
			Map<String, List<String>> facetParamMap) {

		ModifiableSolrParams parameters = new ModifiableSolrParams();
		String facetName = null;
		String paramValue = null;

		try {
			for (Map.Entry<String, List<String>> entry : facetParamMap
					.entrySet()) {

				facetName = entry.getKey();
				List<String> list = entry.getValue();
				paramValue = list.get(0);

			}
			// String qValue = "allgbtdataci

			parameters.set("q", paramValue);
			parameters.set("defType", "edismax");
			parameters.set("wt", "xml");
			parameters.set("start", "0");
			parameters.set("rows", "1000");
			parameters.set("qf", "allgbtdataci priorityci^2 keywordsci");
			parameters.set("fq", "facet:" + facetName);

		} catch (Exception e) {
			GbtLog.Error(e);
		}

		return parameters;
	}
	
	

	public String formatSolarResponse(QueryResponse response) {
		//String solarSearchResults = null;
		SolrDocumentList solarDocumentList;

		//String poProfileDetailedViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		GbtSearchView gbtSearchResults = new GbtSearchView();
		String searchResponseJson = null;
		try {

			// System.out.println("Response :: " + response);
			solarDocumentList = response.getResults();

			long documentCount = solarDocumentList.getNumFound();
			
			System.out.println("No of Entries fetch as documentCount : " + documentCount);

			
			String facet = null;
			
			List<Mara> maraList = new ArrayList<Mara>();
			List<Makt> maktList = new ArrayList<Makt>();
			List<Afko> afkoList = new ArrayList<Afko>();
			List<Afpo> afpoList = new ArrayList<Afpo>();
			List<Aufm> aufmList = new ArrayList<Aufm>();
			List<Resb> resbList = new ArrayList<Resb>();
			
			Mara mara = null;
			Makt makt = null;
			Afko afko = null;
			Afpo afpo = null;
			Aufm aufm = null;
			Resb resb = null;
			
			for (int i = 0; i < documentCount; i++) {

				facet = (String) solarDocumentList.get(i).getFieldValue("facet");
				System.out.println("Fetched Solar Search Results for facet: "	+ facet);

				
				 String ID = (String) solarDocumentList.get(i).getFieldValue("id");
				 String workOrder = (String) solarDocumentList.get(i).getFieldValue("field3"); 
				 String referenceNum = (String) solarDocumentList.get(i).getFieldValue("field14");
				  
			  System.out.println(" ID : ** " + ID );
			  System.out.println(" workOrder : *** "  + workOrder );
			  System.out.println(" resNum : *** " + referenceNum );

				if (facet.equalsIgnoreCase("MARA")) {
					mara = new Mara();
					mara.setId((String) solarDocumentList.get(i).getFieldValue("id"));
					mara.setMaterialNum((String) solarDocumentList.get(i).getFieldValue("field3"));
					mara.setMaterialDesc((String) solarDocumentList.get(i).getFieldValue("field13"));
					maraList.add(mara);

				}else if (facet.equalsIgnoreCase("MAKT")) {
					makt = new Makt();
					makt.setId((String) solarDocumentList.get(i).getFieldValue("id"));
					makt.setMaterialNum((String) solarDocumentList.get(i).getFieldValue("field3"));
					makt.setMaterialDesc((String) solarDocumentList.get(i).getFieldValue("field5"));
					maktList.add(makt);
					
				} else if (facet.equalsIgnoreCase("AFKO")) {
					afko = new Afko();
					afko.setId((String) solarDocumentList.get(i).getFieldValue("id"));
					afko.setWorkOrder((String) solarDocumentList.get(i).getFieldValue("field3"));
					afko.setReferenceNum((String) solarDocumentList.get(i).getFieldValue("field14"));
					afkoList.add(afko);

				} else if (facet.equalsIgnoreCase("AFPO")) {
					afpo = new Afpo();
					afpo.setId((String) solarDocumentList.get(i).getFieldValue("id"));
					afpo.setWorkOrder((String) solarDocumentList.get(i).getFieldValue("field3"));
					afpo.setMaterialNum((String) solarDocumentList.get(i).getFieldValue("field22"));
					afpo.setBatchNum((String) solarDocumentList.get(i).getFieldValue("field61"));
					afpoList.add(afpo);
					
				} else if (facet.equalsIgnoreCase("AUFM")) {
					aufm = new Aufm();
					aufm.setId((String) solarDocumentList.get(i).getFieldValue("id"));
					aufm.setWorkOrder((String) solarDocumentList.get(i).getFieldValue("field30"));
					aufm.setMaterialNum((String) solarDocumentList.get(i).getFieldValue("field9"));
					aufm.setReferenceNum((String) solarDocumentList.get(i).getFieldValue("field31"));
					aufm.setBatchNum((String) solarDocumentList.get(i).getFieldValue("field12"));
					aufmList.add(aufm);
					
				} else if (facet.equalsIgnoreCase("RESB")) {
					resb = new Resb();
					resb.setId((String) solarDocumentList.get(i).getFieldValue("id"));
					resb.setWorkOrder((String) solarDocumentList.get(i).getFieldValue("field32"));
					resb.setMaterialNum((String) solarDocumentList.get(i).getFieldValue("field12"));
					resb.setReferenceNum((String) solarDocumentList.get(i).getFieldValue("field3"));
					resb.setBatchNum((String) solarDocumentList.get(i).getFieldValue("field16"));
					resbList.add(resb);
					
				}			 
				if (i > 998)
					break;
			}
			
			//Populating the searchResults to the GBT SearchView
			if (facet.equalsIgnoreCase("MARA")) {
				gbtSearchResults.setMaraSearchResults(maraList);
			}else if (facet.equalsIgnoreCase("MAKT")) {
				gbtSearchResults.setMaktSearchResults(maktList);
			}else if (facet.equalsIgnoreCase("AFKO")) {
				gbtSearchResults.setAfkoSearchResults(afkoList);
			} else if (facet.equalsIgnoreCase("AFPO")) {
				gbtSearchResults.setAfpoSearchResults(afpoList);
			} else if (facet.equalsIgnoreCase("AUFM")) {
				gbtSearchResults.setAufmSearchResults(aufmList);
			} else if (facet.equalsIgnoreCase("RESB")) {
				gbtSearchResults.setResbSearchResults(resbList);
			} 
			
			
			searchResponseJson = gson.toJson(gbtSearchResults);

			System.out.println("Created the Gson for the Gbt Search: \n " + searchResponseJson);

		} catch (Exception e) {
			GbtLog.Error(e);
		}

		return searchResponseJson;
	}
	
	
		
/*	  public Connection getPostGresConnection()
	{
		try {
			
			if (connection != null && !connection.isClosed()) {
				return connection;
			}			
			
			GbtLog.Info("-------- PostGres JDBC Connection Testing ------");  
			Class.forName(postGresConnDriver);
			System.out.println("After loading the postgres driver");
 
		} catch (ClassNotFoundException e) { 
			GbtLog.Error(e.getMessage());
			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return null; 
		}
		catch (Exception e) {	
			GbtLog.Error(e.getMessage());
			System.out.println("General Exception");
			e.printStackTrace();
			return null; 
		}
 
		GbtLog.Info("-------PostGres JDBC Driver Registered! ------");		
 
		try { 
			connection = DriverManager.getConnection(postGresConnURL,user,pwd); 
		} catch(Exception ee){
			GbtLog.Error("Exception in getting data from batch table " + ee.getMessage());
			System.out.println("Exception" + ee.getMessage());
			System.out.println("Exception in detail" + ee);
			ee.printStackTrace();
			GbtLog.Error(ee.getMessage());
		}		
		catch (SQLException e) {
			GbtLog.Error(e.getMessage());
			GbtLog.Info("Connection Failed! Check output console");
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return null; 
		}
 
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
			GbtLog.Info("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
			GbtLog.Info("Failed to make connection!");
		}
		
		return connection;
	}*/
 
	public void closeConnection() throws SQLException
	{
		if (connection != null) {
			connection.close();
		} 
	}
	
	

}
