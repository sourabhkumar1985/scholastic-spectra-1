<%@page import="java.security.Principal"%>
<%@page import="org.springframework.security.authentication.UsernamePasswordAuthenticationToken"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.relevance.prism.util.PrismHandler"%>
<%@page import="com.relevance.prism.util.Log"%>
<%@page import="java.sql.*"%>
<%@page import="org.springframework.security.saml.SAMLCredential"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
   <title>Evidence Search Portal</title>
   <link rel="shortcut icon" href="img/favicon/favicon_qrcode.ico" type="image/x-icon">
   <link rel="icon" href="img/favicon/favicon_qrcode.ico" type="image/x-icon">
   <link rel="icon" href="css/bootstrap.min.css" type="image/x-icon">
	<style>
	.jumbotron-header {
		font-family: 'open-sans';
		font-weight: 600;
		font-style: normal;
		font-size: 36px;
		color: rgb(213, 25, 0);
		text-align: center;
	}
	.jumbotron-paragraph {
		font-family: 'open-sans';
		font-weight: 400;
		font-style: normal;
		font-size: 18px;
		color: #828282;
		text-align: center;
	}
	</style>
</head>
<body>	

	<%
	    PrismHandler.logMethodEntry("SSO Entry", "SSO Logic started");
		Connection connection = null;
	//	request.getUserPrincipal().getName();
	try {
	out.print("entered sso jsp page");
	Log.Info("entered to sso  ____________");
	System.out.println("entered sso page ------------");
		
		session.setAttribute("user", "anonymoususer");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SAMLCredential credential=(SAMLCredential)SecurityContextHolder.getContext().getAuthentication().getCredentials();
		String username=credential.getAttributeAsString("userID");
		//String email=credential.getAttributeAsString("Email");
		username=username.trim();
		out.print(username);
		Log.Info(username+" --------- username from idp");
		//System.out.println(email+" --------- emial from idp");
	
	          try {

	               Class.forName("com.mysql.jdbc.Driver"); 
	               connection = DriverManager.getConnection("jdbc:mysql://192.168.101.62:3306/cur_relevance","admin","celgene@123");               
	               Log.Info("Using Postgress connection...");

	        } catch (Exception ee) {
	        	  Log.Info("Exception in getting data from batch table "  + ee.getMessage());
	        	  Log.Info("Exception" + ee.getMessage());
	        	  Log.Info("Exception in detail" + ee);
	               ee.printStackTrace();
	               Log.Info(ee.getMessage());
	               throw ee;
	        } 		

	        if (connection != null) {
	               //System.out.println("You made it, take control your database now!");
	               Log.Info("You made it, take control your database now!");
	        } else {
	               //System.out.println("Failed to make connection!");
	               Log.Info("Failed to make connection!");
	        }
	        Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery("select a.enabled,a.primaryrole,a.status from users a where  a.username ='"+username+"';");
           System.out.println("select a.enabled,a.primaryrole,a.status from users a where  a.username ='"+username+"';");
           
            Log.Info("Result set object:" +rs);
            String primaryRole = null;
            String status = null;
            String enabled = null;
            String userStatus = null;
			String userRemarks = null;
			
                	session.setAttribute("user", username);
                	session.setAttribute("app", "ROLE_RLSF");
            	
            	Log.Info("send redirect called ---------------------");
            	Log.Info("path :" +request.getContextPath());
           		response.sendRedirect(request.getContextPath()+ "/index");
	}catch(Exception ee){
		Log.Info("Exception ****************" +ee.getMessage());
		response.sendRedirect(request.getContextPath()+ "/login?app=ROLE_Evidence");
		PrismHandler.logMethodEntry("SSO Exception", ee.getMessage());
	}
	  finally{
				try{
					connection.close();
				}catch(Exception sqe){
					 Log.Info("Connection closed");
					 Log.Info("Exception" + sqe.getMessage());
				}
        }		
    	 PrismHandler.logMethodEntry("SSO Exit", "SSO Logic ended");
	%>	
</body>
</html>
