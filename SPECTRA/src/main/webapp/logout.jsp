<html>
 
<head>
    <title>okta authentication</title>
</head>
 
<body>
 <%session.invalidate();%>
<% HttpSession nsession = request.getSession(false);%>
<%
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
         cookie.setMaxAge(0);
         response.addCookie(cookie);
     }
%>
    <%
        String redirectURL = "https://scholastic.oktapreview.com/login/signout";
        response.sendRedirect(redirectURL);
    %>
 
</body>
