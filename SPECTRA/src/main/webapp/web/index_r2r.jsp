<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en-us">
<head>
<meta charset="utf-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title>Strategic Enterprise Assessment | Home</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.min.css">
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="print"
	href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/dc.css">

<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables-->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-production_unminified.css">

<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-skins.css">

<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

<!-- We recommend you use "your_style.css" to override SmartAdmin
		specific styles this will also ensure you retrain your customization
		with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css"> -->

<link rel="stylesheet" type="text/css" media="screen"
	href="css/your_style.css">
<link rel="stylesheet" type="text/css" media="print"
	href="css/your_style.css">
<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp 
<link rel="stylesheet" type="text/css" media="screen"
	href="css/demo.css">
-->
<!-- FAVICONS -->
<link rel="shortcut icon" href="img/favicon/sea_favicon.ico"
	type="image/x-icon">
<link rel="icon" href="img/favicon/seae_favicon.ico"
	type="image/x-icon">

<!-- GOOGLE FONT -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>
<body class="${themename}">
	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<c:url value="smart-style-4" var="csstheme" />
	<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

	<!-- HEADER -->
	<header id="header">
		<div id="logo-group">

			<!-- PLACE YOUR LOGO HERE -->
			<span id="" style="padding-left:20px"> <img src="img/logo_sea.png" alt="SmartAdmin" >
			</span>
			<!-- END LOGO PLACEHOLDER -->

			<!-- Note: The activity badge color changes when clicked and resets the number to 0
				Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
			<!--	<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>-->

			<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->

			<!-- END AJAX-DROPDOWN -->
		</div>

		<!-- projects dropdown -->

		<!-- end projects dropdown -->

		<!-- pulled right: nav area -->
		<div class="pull-right">

			<!-- collapse menu button -->
			<div id="hide-menu" class="btn-header pull-right">
				<span> <a href="javascript:void(0);" title="Collapse Menu"><i
						class="fa fa-reorder"></i></a>
				</span>
			</div>
			<!-- end collapse menu -->

			<!-- logout button -->
			<div id="logout" class="btn-header transparent pull-right">
				<span> <a href="${logoutUrl}" title="Log Out"><i
						class="fa fa-sign-out"></i></a>
				</span>
			</div>
			<!-- end logout button -->

			<!-- search mobile button (this is hidden till mobile view port) -->
			<div id="search-mobile" class="btn-header transparent pull-right">
				<span> <a href="javascript:void(0)" title="Search"><i
						class="fa fa-search"></i></a>
				</span>
			</div>
			<!-- end search mobile button -->

			<!-- input: search field -->
			<form action="Search" class="header-search pull-right"
				id="frm-header-search">
				<input type="text" placeholder="Search.." id="search-fld">
				<button type="submit">
					<i class="fa fa-search"></i>
				</button>
				<a href="javascript:void(0);" id="cancel-search-js"
					title="Cancel Search"><i class="fa fa-times"></i></a>
			</form>
			<!-- end input: search field -->

			<!--multiple lang dropdown : find all flags in the image folder -->
			<!-- <ul class="header-dropdown-list hidden-xs">
				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="" src="img/flags/Canada.png"> <span> Canada</span>
						<i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu pull-right">
						<li class="active"><a href="javascript:void(0);"><img
								alt="" src="img/flags/Canada.png"> Canada</a></li>
						<li><a href="javascript:void(0);"><img alt=""
								src="img/flags/Japan.png"> Japan</a></li>
						<li><a href="javascript:void(0);"><img alt=""
								src="img/flags/us.png"> US</a></li>
					</ul></li>
			</ul> -->
			<ul class="header-dropdown-list hidden-xs">
				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown">
						 <span> USA</span>
						<i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu pull-right">
						<li class="active"><a href="javascript:void(0);"><!-- <img
								alt="" src="img/flags/Canada.png"> --> USA</a></li>
						<li><a href="javascript:void(0);"><!-- <img alt=""
								src="img/flags/India.png"> --> APAC</a></li>
						<li><a href="javascript:void(0);"><!-- <img alt=""
								src="img/flags/germany.png"> --> EU</a></li>
					</ul></li>
			</ul>
			<!-- end multiple lang -->

		</div>
		<!-- end pulled right: nav area -->

	</header>
	<!-- END HEADER -->

	<!-- Left panel : Navigation area -->
	<!-- Note: This width of the aside area can be adjusted through LESS variables -->
	<aside id="left-panel">

		<!-- User info -->
		<div class="login-info">
			<span> <!-- User image size is adjusted inside CSS, it should stay as it -->
				<img src="img/avatars/male.png" alt="me" class="online" /> <a
				id="show-shortcut" style="color: #808080"><%=request.getSession().getAttribute("userDisplayName")%></a>
			</span>
		</div>
		<!-- end user info -->

		<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
		<nav>
			<!-- NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

			<ul>
				<!--		<li class="">
						<a href="ajax/dashboard.html" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
					</li>
					 <li>
						<a href="ajax/inbox.html"><i class="fa fa-lg fa-fw fa-inbox"></i> <span class="menu-item-parent">Inbox</span><span class="badge pull-right inbox-badge">14</span></a>
					</li>
					 -->
				<!-- <li class="hide" id="DataFlow"><a href="CanadaE2EMF"><i
						class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span
						class="menu-item-parent">Flow Diagrams</span></a>
					<ul>

						<li><a href="CanadaE2EMF" class="hide">Canada E2E-MF</a></li>
						<li><a href="SystemMapChord">System Map</a></li>
						<li><a href="SystemMapSankey">System Map sankey</a></li>
						<li><a href="SystemMapNew" class="hide">System Map 3</a></li>
						<li><a href="SystemMap" class="hide">System Map</a></li>
						<li><a href="POFlow" class="hide">Purchase Orders Flow</a></li>
							<li>
								<a href="#">Lat-Am E2E-MF</a>
							</li> 
					</ul></li>
				<li class="hide" id="Profiling"><a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span
						class="menu-item-parent">Profiling</span></a>
					<ul>
						<li><a href="POProfiling" class="hide">Purchase Orders
								Profiling</a></li>
						<li><a href="PricingProfiling" class="hide">Pricing
								Profiling</a></li>
						<li><a href="SOProfilling" class="hide">Sales Orders
								Profiling</a>
							<ul>
								<li><a href="MaterialDistribution" class="hide">Material
										Distribution</a></li>
							</ul></li>
						<li><a href="GLProfilling" class="hide">Manual GL
								Profiling</a></li>
						<li><a href="SystemMapProfiling" class="hide">System
								Profiling</a></li>
						<li><a href="plantProfiling" class="hide">Plant Profiling</a></li>

					</ul></li>
				<li><a href="Search"><i class="fa fa-lg fa-fw fa-search"></i>
						<span class="menu-item-parent" class="hide">Search</span></a></li>
				<li><a href="#"><i class="fa fa-lg fa-fw fa-dashboard"></i>
						<span class="menu-item-parent">Token Analysis</span></a>
						<ul>
						<li><a href="TokenAnalysisSearch">Search</a></li>
						<li><a href="TokenAnalysisDiagram">Diagram</a></li>
					</ul>
						</li>
				<li><a href="#"><i
						class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span
						class="menu-item-parent">Plan & Estimation</span></a>
					<ul>
						<li><a href="#"><i class="fa fa-folder-o"></i> Source</a>
							<ul>
								<li><a href="POMangement"><i class="fa fa-file-o"></i>Purchase
										Order Management</a></li>
								<li><a href="OutsourcedManufacturing"><i
										class="fa fa-file-o"></i>Outsourced Manufacturing</a></li>

								<li><a href="Affiliate"><i class="fa fa-file-o"></i>Affiliate
										(Inter-Intra Company)</a></li>



							</ul></li>
						<li><a href="#"><i class="fa fa-folder-o"></i>Plan</a>
							<ul>
								<li><a href="supplyplanning"><i class="fa fa-file-o"></i>Plan/Supply
										Planning</a></li>
							</ul></li>

					</ul></li>
				<li><a href="#"><i class="fa fa-lg fa-fw fa-search"></i> <span
						class="menu-item-parent">GBT</span></a>
					<ul>
						<li><a href="GBTSearch" class="hide">Search</a></li>
						<li><a href="GBT" class="hide">Batch Genealogy</a></li>
					</ul></li>
				<li><a href="#"><i
						class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span
						class="menu-item-parent" class="hide">Report</span></a>
					<ul>
						<li><a href="#"><i class="fa fa-folder-o"></i>JDE</a>
							<ul>
								<li><a href="POReport" class="hide">PO Report</a></li>
								<li><a href="SOReport" class="hide">SO Report</a></li>
								<li><a href="WOReport" class="hide">WO Report</a></li>
							</ul></li>

					</ul></li>

				<li><a href="#"><i class="fa fa-lg fa-fw fa-search"></i> <span
						class="menu-item-parent">Report</span></a>
						<li><a href="#"><i class="fa fa-folder-o"></i> JDE</a>
					<ul>
						<li><a href="POReport">PO Report</a></li>
						<li><a href="SOReport">SO Report</a></li>
					</ul>
					</li> -->

			</ul>

		</nav>
		<span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i>
		</span> <a class="batchNumberDecorator" id="batchNumberId" href="#" style=""></a>
	</aside>
	<!-- END NAVIGATION -->

	<!-- MAIN PANEL -->
	<div id="main" role="main">

		<!-- RIBBON -->
		<div id="ribbon">

			<span class="ribbon-button-alignment"> <span id="refresh"
				class="btn btn-ribbon" data-title="refresh" rel="tooltip"
				data-placement="bottom"
				data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings."
				data-html="true"><i class="fa fa-refresh"></i></span>
			</span>

			<!-- breadcrumb -->
			<ol class="breadcrumb">
				<!-- This is auto generated -->
			</ol>
			<!-- end breadcrumb -->

			<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->
		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content"></div>
		<!-- END MAIN CONTENT -->
		<div
			class="modal hide zd-infopopup attributePalette in docked zd-palettepopup"
			data-name="Group" aria-hidden="false"
			style="display: block; right: auto; bottom: auto; top: 120px; left: 750px;"
			id="demisionModalDiv">
			<div class="modal-header zd-infopopup-titlebar">
				<button type="button" class="close icon zd close"
					data-dismiss="modal" aria-hidden="true"
					onclick="$('#demisionModalDiv').addClass('hide')">&times;</button>
				<i class="fa fa-refresh padding-right10 pull-right cursor-pointer"></i>
				<h3 class="dialog-head zd-infopopup-header demisionTitle"></h3>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body dialog-body zd-infopopup-body"
				style="max-height: 170px;">
				<ul class="nav nav-tabs nav-stacked" id="demisionlist">
					<!-- <li><span class="zd icon checkbox radio"><span
						class="zd checked "></span></span>
					<div class="attributeItem">
						<div class="text">1632-Burnaby:144</div>
					</div></li>
				 <li class="filtered"><span class="zd icon checkbox radio"><span
						class="zd checked  filtered"></span></span>
					<div class="attributeItem">
						<div class="text">Product Group</div>
					</div></li>
				 -->
				</ul>
			</div>
			<div class="zd-infopopup-background"></div>
			<div class="zd-infopopup-background small"></div>
			<div class="zd-infopopup-pointer"
				style="border-top-width: 14px; border-top-style: solid; border-top-color: transparent; border-bottom-width: 14px; border-bottom-style: solid; border-bottom-color: transparent; border-right-width: 14px; border-right-style: solid; border-right-color: rgba(50, 50, 50, 0.901961); top: 127px; left: -14px;">
				<div class="zd-infopopup-pointer-arrow"></div>
			</div>
			<div id="deleteDiv" class="hide">
				<div
					class="btn-group display-inline pull-right text-align-left hidden-tablet">
					<button class="btn btn-xs btn-default dropdown-toggle"
						data-toggle="dropdown">
						<i class="fa fa-cog fa-lg"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-xs pull-right">
						<li><a href="javascript:#.Delete($)"
							id="deleteLink"><i
								class="fa fa-times fa-lg fa-fw txt-color-red"></i> <u>D</u>elete</a></li>
						<li class="divider"></li>
						<li class="text-align-center"><a href="javascript:void(0);">Cancel</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN PANEL -->

	<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		
		<div id="shortcut">
			<ul>
				<li>
					<a href="#ajax/inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
				<li>
					<a href="#ajax/calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
				</li>
				<li>
					<a href="#ajax/gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
				</li>
				<li>
					<a href="#ajax/invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
				</li>
				<li>
					<a href="#ajax/gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
				</li>
				<li>
					<a href="javascript:void(0);" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
				</li>
			</ul>
		</div>-->
	<!-- END SHORTCUT AREA -->

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script src="//code.jquery.com/jquery-2.0.2.min.js"></script>
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="js/plugin/libs/jquery-2.0.2.min.js"><\/script>');
		}
	</script>
	<script
		src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
	<script>
		if (!window.jQuery.ui) {
			document
					.write('<script src="js/plugin/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>




	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

	<!-- BOOTSTRAP JS -->
	<script
		src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<script>
		if (typeof ($.fn.modal) === "undefined") {
			document
					.write('<script src="js/plugin/bootstrap/bootstrap.min.js"><\/script>');
		}
	</script>
	<!-- 
	<script src="js/spin.min.js"></script>
	<script src="js/crossfilter.min.js"></script>
	<script src="js/colorbrewer.js"></script>
	<script src="js/tipsy.min.js"></script>
	CUSTOM NOTIFICATION
	<script src="js/notification/SmartNotification.min.js"></script>

	JARVIS WIDGETS
	<script src="js/smartwidgets/jarvis.widget.min.js"></script>

	EASY PIE CHARTS
	<script src="js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	SPARKLINES
	<script src="js/plugin/sparkline/jquery.sparkline.min.js"></script>

	JQUERY VALIDATE
	<script src="js/plugin/jquery-validate/jquery.validate.min.js"></script>

	JQUERY MASKED INPUT
	<script src="js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	JQUERY SELECT2 INPUT
	<script src="js/plugin/select2/select2.min.js"></script>

	JQUERY UI + Bootstrap Slider
	<script src="js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	browser msie issue fix
	<script src="js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	SmartClick: For mobile devices
	<script src="js/plugin/smartclick/smartclick.js"></script> -->

	<!-- All above plugin into one file-->
	<script src="js/plugin/d3.min.js"></script>
	<script src="js/plugin/jquery_all_plugins.js"></script>
	<script src="js/plugin/dc.min.js"></script>
	<script src="js/Common/MetaData.js"></script>
	<script src="js/Common/System.js"></script>
	<script src="js/Common/Common.js"></script>
	<script src="js/Profiling/Graphing.js"></script>
	<script src="js/Profiling/Profiling.js"></script>
	<script src="js/plugin/datatables/jquery.dataTables-cust.min.js"></script>
	<script src="js/plugin/datatables/media/js/TableTools.min.js"></script>
	<script src="js/plugin/datatables/DT_bootstrap.js"></script>
	<script src="js/plugin/datatables/jquery.dataTables-cust.min.js"></script>
	<script src="js/plugin/datatables/media/js/TableTools.min.js"></script>
	<script src="js/plugin/datatables/DT_bootstrap.js"></script>
	<script src="js/plugin/jquery.PrintArea.js"></script>
	<script src="js/plugin/jquery.jsontotable.min.js"></script>
	<script src="js/plugin/jspdf.debug.js"></script>
	<script src="js/Profiling/Graphing.js"></script>
	<script src="js/Profiling/Profiling.js"></script>

	<!--[if IE 7]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

	<!-- Demo purpose only
		<script src="js/demo.js"></script> -->

	<!-- MAIN APP JS FILE -->
	<script src="js/Common/app.js"></script>
	<script>
		$("#batchNumberId").html("IDP Version : " + localStorage["buildInfo"]);
	</script>
	<script>
		constructMenu(${userroleactions});
		storeLookupTable(${lookupvalues});
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {

				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),

			m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js',
				'ga');
		var trackingCode = lookUpTable.get("GoogleAnalyticsKey");
		ga('create', trackingCode, "auto");
	</script>


	<!-- Your GOOGLE ANALYTICS CODE Below -->
</body>

</html>