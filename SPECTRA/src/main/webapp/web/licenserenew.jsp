<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.sql.*" %> 
<%@ page import="java.io.*" %> 
<%@ page import="com.relevance.prism.service.LicenseKeyService" %> 

<html>
<head>
<meta charset="ISO-8859-1">
<title>Select Application to Proceed</title>
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/font-awesome.min.css">
<!-- <link rel="stylesheet" type="text/css" media="screen"
	href="css/metro.css"> -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-production_unminified.css?v=${version}">
	<style>
	  .jumbotron {
	      background: linear-gradient(#009899, #009899 30%, #009899 45%, #009899 100%);
	      color: #fff;
	  }
	  .input-group .form-control {
  		  width: 65px;
	  }
	  .input-group-btn:last-child>.btn {
		  margin-left: 15px;
	  }
  </style>
</head>
<link rel="icon" href="img/favicon/favicon-bullseye.ico" type="image/x-icon">
<body class="loaded">
	<div class="jumbotron text-center">
	  <h1 style="margin-bottom: 30px;">qSync</h1> 
 	  <p>License key</p>
	  <form class="form-inline" method="get" action="licenserenew.jsp">
	    <div class="input-group text-center" style="width: 100%;">
	      <input type="text" class="form-control" id="first" name="first" size="4" maxlength="4" required> - 
	      <input type="text" class="form-control" id="second" name="second" size="4" maxlength="4" required> - 
	      <input type="text" class="form-control" id="third" name="third" size="4" maxlength="4" required> - 
	      <input type="text" class="form-control" id="fourth" name="fourth" size="4" maxlength="4" required>
	    </div>
      	<div class="text-center">
        	<button type="submit" id="updateBtn" class="btn btn-primary" style="width: 100px;" disabled="disabled">Update</button>
      	</div>
      	
      	<%
	   String first = request.getParameter("first");
	   String second = request.getParameter("second");
	   String third = request.getParameter("third");
	   String fourth = request.getParameter("fourth");
	   
	   String productKey = first+"-"+second+"-"+third+"-"+fourth;
	   //out.println(productKey);
	   
	  
	   
	     if(first != null)
	   {
	       LicenseKeyService keyservice = new LicenseKeyService();
	  	   String status = keyservice.getLicenseStatus(productKey); 	
	  	   
	  	     if(status.equalsIgnoreCase("Invalid"))
	  	   {
	  	 %>
	  	    	<div class="alert alert-success alert-dismissible" style="margin-top: 60px;">
	   				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <strong>Invalid!</strong> License key entered is invalid, please enter valid key.
	        	</div>   
	           	<script>
		           	$(function(){
		        		$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
		        		    $("#success-alert").slideUp(500);
		        		});
		           	});
	           	</script>	 
	  	 <%   	 
	  	   } else if(status.equalsIgnoreCase("Expired"))
	  	   {
	  	 %>	   
	  		 	<div class="alert alert-success alert-dismissible" style="margin-top: 60px;">
	   				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <strong>Expired!</strong> License key entered is expired, please enter valid key.
	         	</div>
	           	<script>
		           	$(function(){
		        		$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
		        		    $("#success-alert").slideUp(500);
		        		});
		           	});
	           	</script>
	      <%   
	  	   }
	  	   else
	  	   {
	         String connectionURL = "jdbc:postgresql://192.168.101.42:5432/SPECTRA";      
             Connection connection = null;    
             PreparedStatement pstatement = null;
     
            Class.forName("org.postgresql.Driver").newInstance();
            int updateQuery = 0;
  
  		  try {
             connection = DriverManager.getConnection(connectionURL, "platformuser", "platform!@#");                 
             String queryString = "Update app_properties set value = ? where name = 'Licence_key'";
           	    
              pstatement = connection.prepareStatement(queryString);          
			  pstatement.setString(1, productKey);			 
              updateQuery = pstatement.executeUpdate();
             
             if (updateQuery != 0) 
            { 
           %>
	           	<div class="alert alert-success alert-dismissible" style="margin-top: 60px;">
	   				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <strong>Success!</strong> License key is updated successfully, try accessing application.
	           	</div>
	           	<script>
		           	$(function(){
		        		$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
		        		    $("#success-alert").slideUp(500);
		        		});
		           	});
	           	</script>
      		<%	   
           }
         } 
         catch (Exception ex) {
                out.println("Unable to connect to batabase.");
            }
         finally {
             // close all the connections.
             pstatement.close();
             connection.close();
         }
	  	}
	  }
	
%>
      	
	  </form>
	</div>
	<!-- JQUERY JS -->
 	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
 	<script> if (!window.jQuery) { document.write('<script src="js/plugin/libs/jquery-2.0.2.min.js"><\/script>');} </script>
 	<!-- BOOTSTRAP JS -->
   	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 	<script>if (!window.jQuery) { document.write('<script src="src="js/plugin/bootstrap/bootstrap.min.js"><\/script>');}</script>
	<script>
	$(function(){
		$('#first').keyup(function(){
			debugger;
			$("#updateBtn").prop("disabled", true);
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g;
			var isSplChar = re.test(yourInput);
			if(isSplChar){
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g, '');
				$(this).val(no_spl_char);
				debugger;
			} else {
				if($(this).val().length == 4){
					$("#second").focus();
					if($("#second").val().length === 4 && $("#third").val().length === 4 && $("#fourth").val().length === 4) {
						$("#updateBtn").prop("disabled", false);
					} 
				}
			}
		});
		$('#second').keyup(function(){
			$("#updateBtn").prop("disabled", true);
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g;
			var isSplChar = re.test(yourInput);
			if(isSplChar) {
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g, '');
				$(this).val(no_spl_char);
			} else {
				if($(this).val().length == 4){
					$("#third").focus();
					if($("#first").val().length === 4 && $("#third").val().length === 4 && $("#fourth").val().length === 4) {
						$("#updateBtn").prop("disabled", false);
					} 
				}
			}
		});
		$('#third').keyup(function(){
			$("#updateBtn").prop("disabled", true);
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g;
			var isSplChar = re.test(yourInput);
			if(isSplChar) {
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g, '');
				$(this).val(no_spl_char);
			} else {
				if($(this).val().length == 4){
					$("#fourth").focus();
					if($("#first").val().length === 4 && $("#second").val().length === 4 && $("#fourth").val().length === 4) {
						$("#updateBtn").prop("disabled", false);
					} 
				}
			} 
		});
		$('#fourth').keyup(function(){
			$("#updateBtn").prop("disabled", true);
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g;
			var isSplChar = re.test(yourInput);
			if(isSplChar) {
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\sa-z]/g, '');
				$(this).val(no_spl_char);
			} else {
				if($(this).val().length == 4){
					if($("#first").val().length === 4 && $("#second").val().length === 4 && $("#third").val().length === 4) {
						$("#updateBtn").prop("disabled", false);
					} 
				}
			} 
		});
	});
	</script>
	   

</body>
</html>