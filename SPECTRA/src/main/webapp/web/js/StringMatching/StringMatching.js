pageSetUp();
var configurationData = {};
var StringMatching = function(configData, id, mode, filters) {
	/*
	 * var selectedSource ,selectedTable; var selectedDatabase;
	 */
	var THIS = this;
	var allColumns = {};
	var filterContentId = {};
	id = id || 0;
	mode = mode || 'config';
	var title = '';
	var description = '';
	$('body').toggleClass("minified");
	if (mode !== 'view') {
		$("#sourceConfig,#targetConfig,#algoDiv,#wid-id-5").hide();
		$(".well").unbind("click").bind("click", function() {
			sourceSelect(this);
		});
		$("#generateSM").unbind("click").bind("click", function() {
			if ($(this).attr('data-type') === 'view') {
				mode = 'view'
			}
			generateSM();
		});
		$('#btnOpenSearchModal').unbind('click').bind('click', function() {
			$('#myModalSaveSearch').modal('show');
		});
		$('#mySavedStringProfile').unbind('click').bind('click', function() {
			$(".myProfilesConfig").hide(300);
			$("#myProfiles").show(300);
			if ($('#profileConfig').length > 0) {
				$("#profileConfig").hide(300);
			}
			if ($('#querybuilder').length > 0) {
				$('#querybuilder').hide(300);
			}
		});

		$("#backToDesign").unbind("click").bind("click", function() {
			$(".myProfilesConfig").show(300);
			$("#myProfiles").hide(300);
		});

		$('#btnSaveSearch').unbind('click').bind('click', function(e) {
			e.preventDefault();
			if (!$('#profilename').val()) {
				showNotification("error", 'Enter profile name');
				return;
			}
			title = $('#profilename').val();
			description = $('#description').val();
			saveConfig();
		});
	}

	var getFilterQueryFromFilterConfig = function(filterConfig, source, type) {
		source = source || THIS.sourceType;
		var filterQuery = "";
		if (filterConfig && filterConfig.length > 0) {
			for (var i = 0; i < filterConfig.length; i++) {
				if (type == filterConfig[i].diffType) {
					var searchtype = filterConfig[i].searchtype || " IN ", fromDateOperator = ">=", toDateOperator = "<=";
					if ($.trim(searchtype.toLowerCase()) === "not in") {
						fromDateOperator = "<=";
						toDateOperator = ">=";
					}
					if (filterConfig[i]["fieldType"] === 'lookup'
							|| filterConfig[i]["fieldType"] === 'select') {
						var lookupIds = "";
						var lookupData = filterConfig[i]["defaultvalue"];
						if (!lookupData || lookupData.length === 0)
							continue;
						if (lookupData && filterConfig[i].isSingle) {
							if (lookupData.id !== '""'
									&& lookupData.id !== 'null') {
								if ($.trim(searchtype.toLowerCase()) === "in"
										|| $.trim(searchtype.toLowerCase()) === "not in") {
									lookupIds = "'" + lookupData.id + "'";
									filterQuery += " ("
											+ addTildInColumnName(filterConfig[i]["fieldName"])
											+ " " + searchtype + " ("
											+ lookupIds + ")";
								} else {
									lookupIds = lookupData.id;
									filterQuery += addTildInColumnName(filterConfig[i]["fieldName"])
											+ " "
											+ searchtype
											+ " '%"
											+ lookupIds + "%'";
								}
							}
						} else if (lookupData && lookupData.length > 0) {
							if ($.trim(searchtype.toLowerCase()) === "in"
									|| $.trim(searchtype.toLowerCase()) === "not in") {
								for (var x = 0; x < lookupData.length; x++) {
									if (lookupData[x].id !== '""'
											&& lookupData[x].id !== 'null') {
										if (x === 0)
											lookupIds = "'" + lookupData[x].id
													+ "'";
										else
											lookupIds += ",'"
													+ lookupData[x].id + "'";
									}
								}
								if (lookupIds) {
									filterQuery += " AND ("
											+ addTildInColumnName(filterConfig[i]["fieldName"])
											+ " " + searchtype + " ("
											+ lookupIds + ")";
								}
							} else {
								filterQuery += " AND (";
								var operator = $.trim(searchtype.toLowerCase()) === 'like' ? ' OR '
										: ' AND ';
								for (var x = 0; x < lookupData.length; x++) {
									if (x === 0)
										filterQuery += addTildInColumnName(filterConfig[i]["fieldName"])
												+ " "
												+ searchtype
												+ " '%"
												+ lookupData[x].id + "%'";
									else
										filterQuery += operator
												+ addTildInColumnName(filterConfig[i]["fieldName"])
												+ " " + searchtype + " '%"
												+ lookupData[x].id + "%'";
								}
							}
						}

						if ((lookupIds === "" || lookupIds === "''"
								|| lookupIds === '""' || lookupIds === 'null')
								&& $.trim(searchtype.toLowerCase()) === "in")
							filterQuery += " AND ("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " is null OR "
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " = '')";
						else if ((lookupIds === "" || lookupIds === "''"
								|| lookupIds === '""' || lookupIds === 'null')
								&& $.trim(searchtype.toLowerCase()) === "not in")
							filterQuery += " AND ("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " is not null AND "
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " <> '')";
						else if ((lookupIds === "" || lookupIds === "''"
								|| lookupIds === '""' || lookupIds === 'null')
								&& $.trim(searchtype.toLowerCase()) === "like")
							filterQuery += " AND ("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " like '%null%' )";
						else if ((lookupIds === "" || lookupIds === "''"
								|| lookupIds === '""' || lookupIds === 'null')
								&& $.trim(searchtype.toLowerCase()) === "not like")
							filterQuery += " AND ("
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " not like '%null% )";
						else
							filterQuery += " )";
					} else if (filterConfig[i]["fieldType"] === 'NUM') {
						if (filterConfig[i]["from-defaultvalue"]
								&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND "
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " >= "
									+ filterConfig[i]["from-defaultvalue"];
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND "
									+ addTildInColumnName(filterConfig[i]["fieldName"])
									+ " <= "
									+ filterConfig[i]["to-defaultvalue"];
					} else if (filterConfig[i]["fieldType"] === 'dateRange') {
						var toDate, fromDate;
						var formatForDate = filterConfig[i]["dateFormat"];
						if (formatForDate === undefined) {
							formatForDate = 'MM/dd/yyyy';
						}
						var returnedValue = dataAnalyser
								.storedDateRange(filterConfig[i]["defaultvalue"])
						if (returnedValue != "") {
							fromDate = returnedValue[0].trim();
							toDate = returnedValue[1].trim();
						}

						if (source === "postgreSQL") {
							if (fromDate && $.trim(fromDate) !== "")
								filterQuery += " AND "
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ fromDateOperator + " to_date('"
										+ fromDate + "', 'MM/dd/yyyy') ";
							if (toDate && $.trim(toDate) !== "")
								filterQuery += " AND "
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ toDateOperator + " to_date('"
										+ toDate + "', 'MM/dd/yyyy')+1";
						} else {
							if (fromDate && $.trim(fromDate) !== "")
								filterQuery += " AND unix_timestamp("
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ ", '" + formatForDate + "') "
										+ fromDateOperator
										+ " unix_timestamp('" + fromDate
										+ "','MM/dd/yyyy') ";
							if (toDate && $.trim(toDate) !== "")
								filterQuery += " AND unix_timestamp("
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ ",'" + formatForDate + "') "
										+ toDateOperator + " unix_timestamp('"
										+ toDate + "','MM/dd/yyyy')";
						}

					} else if (filterConfig[i]["fieldType"] === 'DATE'
							|| filterConfig[i]["fieldType"] === 'dynamicDate') {
						var fromDate = filterConfig[i]["from-defaultvalue"];
						var toDate = filterConfig[i]["to-defaultvalue"];
						var formatForDate = filterConfig[i]["dateFormat"];
						if (formatForDate === undefined) {
							formatForDate = 'MM/dd/yyyy';
						}
						if (filterConfig[i]["fieldType"] === 'dynamicDate'
								&& isNaN(Date
										.parse(filterConfig[i]["from-defaultvalue"]))) {
							fromDate = eval(filterConfig[i]["from-defaultvalue"]);
							toDate = eval(filterConfig[i]["to-defaultvalue"]);
						}
						if (source === "postgreSQL") {
							if (filterConfig[i]["from-defaultvalue"]
									&& $
											.trim(filterConfig[i]["from-defaultvalue"]) !== "")
								filterQuery += " AND "
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ fromDateOperator + " to_date('"
										+ fromDate + "', '" + formatForDate
										+ "') ";
							if (filterConfig[i]["to-defaultvalue"]
									&& $
											.trim(filterConfig[i]["to-defaultvalue"]) !== "")
								filterQuery += " AND "
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ toDateOperator + " to_date('"
										+ toDate + "','" + formatForDate
										+ "')+1";
						} else {
							if (filterConfig[i]["from-defaultvalue"]
									&& $
											.trim(filterConfig[i]["from-defaultvalue"]) !== "")
								filterQuery += " AND unix_timestamp("
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ ", '" + formatForDate + "') "
										+ fromDateOperator
										+ " unix_timestamp('" + fromDate
										+ "','MM/dd/yyyy') ";
							if (filterConfig[i]["to-defaultvalue"]
									&& $
											.trim(filterConfig[i]["to-defaultvalue"]) !== "")
								filterQuery += " AND unix_timestamp("
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ ",'" + formatForDate + "') "
										+ toDateOperator + " unix_timestamp('"
										+ toDate + "','MM/dd/yyyy')";
						}
					} else {
						if (filterConfig[i]["defaultvalue"]
								&& $.trim(filterConfig[i]["defaultvalue"]) !== "") {
							if ($.trim(searchtype.toLowerCase()) === "in"
									|| $.trim(searchtype.toLowerCase()) === "not in") {
								filterQuery += " AND lower("
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ ") "
										+ searchtype
										+ " ('"
										+ filterConfig[i]["defaultvalue"]
												.toLowerCase() + "')";
							} else {
								filterQuery += " AND lower("
										+ addTildInColumnName(filterConfig[i]["fieldName"])
										+ ") "
										+ searchtype
										+ " '%"
										+ filterConfig[i]["defaultvalue"]
												.toLowerCase() + "%'";
							}

						}

					}
				}
			}

		}
		filterQuery = filterQuery.replace("AND", "WHERE");
		return filterQuery;
	};

	var algorithms = [ {
		"id" : "lvDistance",
		"text" : "Rule 1" // Levenshtein distance
	}, {
		"id" : "hammingDistance",
		"text" : "Rule 2" // Hamming distance
	}, {
		"id" : "jaroWrinklerDistance",
		"text" : "Rule 3" // Jaro-Winkler distance
	}, {
		"id" : "sorensenDice",
		"text" : "Rule 4" // Sorensen-Dice coefficient (bigrams)
	}, {
		"id" : "jaccard",
		"text" : "Rule 5" // Jaccard Simmilarity (bigrams)
	}, {
		"id" : "cosine",
		"text" : "Rule 6" // Cosine Similarity
	} ];
	$("#frmSM").submit(function(e) {
		e.preventDefault();
		// var filters = getFilters();
	});
	$("#smart-mod-eg1").unbind("click").bind("click", function() {
		$.fileDownload('rest/material/materialreportexcel?name=SMData');
	});
	$("#frmSM").validate({
		rules : {
			sourceDatabase : {
				required : true
			},
			sourceTable : {
				required : true
			},
			sourceMC : {
				required : true
			},
			targetDatabase : {
				required : true
			},
			targetTable : {
				required : true
			},
			targetMC : {
				required : true
			},
			smAlgorithms : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			sourceDatabase : {
				required : 'Select source database'
			},
			sourceTable : {
				required : 'Select source table'
			},
			sourceMC : {
				required : 'Select source string matching column'
			},
			targetDatabase : {
				required : 'Select target database'
			},
			targetTable : {
				required : 'Select target table'
			},
			targetMC : {
				required : 'Select target string matching column'
			},
			smAlgorithms : {
				required : 'Select String matching algorithms'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element);
		}
	});

	var previewPublishProfile = function(data, obj) {
		$("#userEnabled").removeAttr("checked");
		$("#smart-form-register input").removeAttr("readonly");
		if ($(obj).html() != "Publish") {
			publishedActiondetails(data, obj);
		} else {
			var publishDataObj = new Object();
			publishDataObj.action = data.displayname.replace(/ /g, '');
			publishDataObj.displayname = data.displayname;
			publishDataObj.refid = data.id;
			publishDataObj.source = "";
			var callBackPublishProfile = function(responseData) {
				$(obj).html("Published").removeClass("btn-outline").addClass(
						"btn-outline-success");
				$(obj).attr("data-action", data.displayname.replace(/ /g, ''));
				$(obj).attr("data-actionid", responseData.actionId);
			};

			$("#smart-form-register input").val("");
			$("#frmSankeyFilter em").remove();
			$(".state-error").removeClass("state-error");

			templates.managePublishing(publishDataObj, "CREATE",
					callBackPublishProfile);
		}
	};

	var publishedActiondetails = function(data, obj) {
		enableLoading();
		var callbackSucessGetAction = function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				if (response == null) {
					disableLoading();
					alert("Selected Profile is Published as Tab");
					return;
				}
				response.refid = data.id;
				response.actionId = Number(data.actionid)
						|| Number($(obj).attr("data-actionid"));
				templates.managePublishing(response, "VIEW", undefined);
			}
		};

		var request = {
			"action" : data.profilekey || $(obj).attr("data-action")
		};
		callAjaxService("getAction", function(response) {
			callbackSucessGetAction(response);
		}, callBackFailure, request, "POST");
	};

	var getById = function() {
		var request = {
			"id" : id
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetById, callBackFailure,
				request, "POST");
	}

	var callBackGetById = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification('error', response.customMessage);
			return;
		}
		if (response.config) {
			response.config = JSON.parse(response.config);
			response.config = response.config[0];
			var parseFilter = response.config.filters;
			THIS.filters = JSON.parse(parseFilter.replace(/@/g, "'"));
			title = response.config.displayname;
			$('#profilename').val(response.config.displayname);
			description = response.config.remarks;
			$('#description').val(description);
			id = response.config.id;
			configData = response.config.config;
			configData = JSON.parse(configData);
			getProfileDropDowns();

		}
	}

	var getProfileDropDowns = function() {
		var requestServices = [ "getDatabaseList", "getDatabaseList",
				"getTableList", "getTableList", "getColumnList",
				"getColumnList" ];
		var requests = [ {
			"source" : configData["sourceEnv"]
		}, {
			"source" : configData["targetEnv"]
		}, {
			"source" : configData["sourceEnv"],
			"database" : configData["sourceDB"]
		}, {
			"source" : configData["targetEnv"],
			"database" : configData["targetDB"]
		}, {
			"source" : configData["sourceEnv"],
			"database" : configData["sourceDB"],
			"table" : configData["sourceTable"]
		}, {
			"source" : configData["targetEnv"],
			"database" : configData["targetDB"],
			"table" : configData["targetTable"]
		} ];

		populateDefaultFields();
		enableLoading();
		callAjaxServices(requestServices, callBackProfileConfigs,
				callBackFailure, requests, [ "POST", "POST", "POST", "POST",
						"POST", "POST" ], "json", true);
	}
	var constructFilters = function(config, filterContentId) {
		filterContentId = filterContentId || "divSankeyFilter";
		var rowDiv, mainSection, rowIndex = 0, sectionClassName = "col col-4 col-sm-4", isInline;
		if (config && config.length > 0) {
			var mandatoryPass = true;
			$('#' + filterContentId).html('');
			rowDiv = $('<div/>').addClass(
					'row smart-form' + (isInline ? ' col-sm-11' : ''));
			$('#' + filterContentId).append(rowDiv);
			$
					.each(
							config,
							function(index, obj) {
								mainSection = $('<section/>').addClass(
										sectionClassName).attr({
									'key' : index,
									'id' : 'f' + (index + 1)
								}).data({
									"lookup-query" : obj.lookupquery,
									"field-name" : obj.fieldName,
									"display-name" : obj.displayName,
									"field-type" : obj.fieldType,
									"isSingle" : obj.isSingle,
									"isMandatory" : obj.isMandatory,
									"invisible" : obj.invisible,
									"searchtype" : obj.searchtype,
									"defaultvalue" : obj.defaultvalue,
									"dateformat" : obj.dateFormat,
									"chat" : obj.chat
								});
								if (obj.profileid)
									mainSection
											.data("profileid", obj.profileid);
								if (obj.isMandatory
										&& (!obj.defaultvalue || obj.defaultvalue.length === 0))
									mandatoryPass = false;
								if (obj.invisible) {
									mainSection.hide();
								}
								rowDiv
										.append(mainSection
												.append(
														$('<div/>')
																.addClass(
																		'note')
																.append(
																		$(
																				'<strong/>')
																				.html(
																						obj.displayName
																								+ ' :')))
												.append(
														$('<label/>')
																.addClass(
																		'input width-100per')
																.append(
																		obj.innotin ? $(
																				"<select/>")
																				.attr(
																						{
																							"name" : "searchtype"
																						})
																				.css(
																						{
																							"padding-left" : "0px"
																						})
																				.addClass(
																						"form-control col col-3 ")
																				.append(
																						$(
																								"<option/>")
																								.html(
																										"In")
																								.attr(
																										{
																											"value" : "in"
																										}))
																				.append(
																						$(
																								"<option/>")
																								.attr(
																										{
																											"value" : "not in"
																										})
																								.html(
																										"Not In"))
																				.append(
																						$(
																								"<option/>")
																								.html(
																										"Like")
																								.attr(
																										"value",
																										"like"))
																				.append(
																						$(
																								"<option/>")
																								.html(
																										"Not Like")
																								.attr(
																										"value",
																										"not like"))
																				: '')
																.append(
																		obj.fieldType === 'TEXT' ? $(
																				'<input/>')
																				.val(
																						obj.defaultvalue
																								.replace(
																										/'/g,
																										""))
																				.attr(
																						{
																							'placeholder' : 'Enter '
																									+ obj.displayName
																						})
																				.addClass(
																						'form-control'
																								+ (obj.innotin ? ' width-75per'
																										: ''))
																				: (obj.fieldType === 'lookup'
																						|| obj.fieldType === 'select' || obj.fieldType === 'lookupdropdown') ? $(
																						'<div/>')
																						.attr(
																								{
																									"name" : "dropdowndiv"
																								})
																						.addClass(
																								(obj.innotin ? 'col-md-9'
																										: 'col-md-12'))
																						.append(
																								$(
																										'<input/>')
																										.addClass(
																												'input'
																														+ isInline ? ' chart-bar '
																														: ' width-100per'))
																						: $(
																								'<div/>')
																								.addClass(
																										obj.innotin ? 'row'
																												: '')
																								.append(
																										$(
																												'<div/>')
																												.addClass(
																														obj.innotin ? 'col-sm-4'
																																: 'col-sm-6')
																												// .append($('<div/>').addClass('note
																												// col-sm-3')
																												// .append($('<strong/>').html('From')))
																												.append(
																														$(
																																'<label/>')
																																.addClass(
																																		'input')
																																.append(
																																		$(
																																				'<input/>')
																																				.attr(
																																						{
																																							"name" : "from-defaultvalue",
																																							"placeholder" : "From"
																																						})
																																				.css(
																																						{
																																							"width" : "95%"
																																						})
																																				.addClass(
																																						'form-control')
																																				.val(
																																						obj["from-defaultvalue"]))))
																								.append(
																										$(
																												'<div/>')
																												.addClass(
																														obj.innotin ? 'col-sm-4'
																																: 'col-sm-6')
																												.append(
																														$(
																																'<label/>')
																																.addClass(
																																		'input')
																																.append(
																																		$(
																																				'<input/>')
																																				.attr(
																																						{
																																							"name" : "to-defaultvalue",
																																							"placeholder" : "To"
																																						})
																																				.css(
																																						{
																																							"width" : "95%"
																																						})
																																				.addClass(
																																						'form-control')
																																				.val(
																																						obj["to-defaultvalue"])))))));
								if (obj.innotin) {
									mainSection.find("[name='innotin']").val(
											"notin");
								}
								if (obj.searchtype) {
									mainSection.find("[name='searchtype']")
											.val(obj.searchtype);
								}
								if (obj.fieldType === 'DATE'
										|| obj.fieldType === 'dynamicDate') {
									if (obj.fieldType === 'dynamicDate') {
										mainSection.find('input:eq(0)').val(
												eval(obj["from-defaultvalue"]));
										mainSection.find('input:eq(1)').val(
												eval(obj["to-defaultvalue"]));
									}
									mainSection
											.find('input:eq(0)')
											.datepicker(
													{
														defaultDate : "+1w",
														changeMonth : true,
														onClose : function(
																selectedDate) {
															$(
																	'div[key="'
																			+ index
																			+ '"]')
																	.find(
																			'input:eq(1)')
																	.datepicker(
																			"option",
																			"minDate",
																			selectedDate);
														}
													});
									mainSection
											.find('input:eq(1)')
											.datepicker(
													{
														defaultDate : "+1w",
														changeMonth : true,
														onClose : function(
																selectedDate) {
															$(
																	'div[key="'
																			+ index
																			+ '"]')
																	.find(
																			'input:eq(0)')
																	.datepicker(
																			"option",
																			"maxDate",
																			selectedDate);
														}
													});
								} else if (obj.fieldType === 'dateRange') {
									mainSection.find('input:eq(0)').attr({
										'readonly' : true
									});
									mainSection.find('input:eq(1)').attr({
										'readonly' : true
									});
									var defaultValue = obj.defaultvalue;
									if (defaultValue) {
										var previousDateRange = dataAnalyser
												.storedDateRange(defaultValue);
										if (previousDateRange != "") {
											mainSection
													.find('input:eq(0)')
													.val(
															previousDateRange[0]
																	.trim());
											mainSection
													.find('input:eq(1)')
													.val(
															previousDateRange[1]
																	.trim());
										}

									}
									mainSection
											.find('input')
											.click(
													function() {
														currentFieldset = $(
																this).parents(
																"section");// find("section#"+currentField.attr('id'));
														var Fieldset = currentFieldset
																.data('defaultvalue');
														$('#dialog')
																.find(
																		'.tab-content')
																.find(
																		'.tab-pane.fade')
																.removeClass(
																		'active');
														$("#dialog")
																.dialog(
																		{
																			width : 500,
																			resizable : false,
																			height : 250,
																			open : function(
																					event) {
																				$(
																						".selectedRange")
																						.html(
																								'');
																				$(
																						'input[type="radio"]')
																						.prop(
																								'checked',
																								false);
																				$(
																						".nextRange")
																						.attr(
																								"disabled",
																								true);
																				$(
																						".lastRange")
																						.attr(
																								"disabled",
																								true);
																			}
																		});

														$("#dialog").dialog(
																"open");
														$("#dialog")
																.data(
																		currentFieldset);
														if (config
																&& jQuery
																		.isEmptyObject(Fieldset)) {
															for (var i = 0; i < config.length; i++) {
																if (currentFieldset
																		.attr('id') === config[i].id) {
																	dataAnalyser
																			.storedDateRange(config[i].defaultvalue);
																}
															}
														}

														else if (!jQuery
																.isEmptyObject(Fieldset)) {
															dataAnalyser
																	.storedDateRange(Fieldset);

														}

													});

								} else if (obj.fieldType === 'lookup'
										|| obj.fieldType === 'select'
										|| obj.fieldType === 'lookupdropdown') {
									function formatResult(resp) {
										return resp.id
												+ (resp.name ? ' - '
														+ resp.name : '');
									}

									function formatSelection(resp) {
										return resp.id
												+ (resp.name ? ' - '
														+ resp.name : '');
									}
									if (obj.fieldType === 'select') {
										var selectdata = obj.lookupquery;
										if (selectdata) {
											selectdata = Jsonparse(selectdata,
													"FILTER LOOKUP");
											if (!selectdata
													|| Object.keys(selectdata).length === 0) {
												selectdata = [];
												var values = obj.lookupquery
														.split(',');
												if (values && values.length > 0) {
													for (var i = 0; i < values.length; i++) {
														selectdata.push({
															id : values[i],
															text : values[i]
														});
													}
												}
											}
										}
										mainSection.find("input").select2(
												{
													allowClear : obj.isSingle,
													placeholder : "Enter "
															+ obj.displayName,
													multiple : !obj.isSingle,
													data : selectdata || []
												});
									} else if (obj.fieldType === 'lookupdropdown') {
										constructDropDown(mainSection.find("[name='dropdowndiv']"),
												obj.lookupquery, !obj.isSingle,
												obj.defaultvalue, "",
												"Select " + obj.displayName,
												THIS.sourceType, THIS.database);
									} else {
										constructSelect2(mainSection
												.find('input'),
												obj.lookupquery, !obj.isSingle,
												obj.defaultvalue, "", null,
												"Enter " + obj.displayName,
												THIS.sourceType, THIS.database);
									}
								}
								rowIndex++;
							});
		}
		if (!localStorage["isMultipleProfile"]) {
			$('[name="applyFilter"]')
					.unbind('click')
					.bind(
							'click',
							function(e) {
								var filterConfig = getFiltersFromProfile(filterContentId);
								if (!filterConfig) {
									e.preventDefault();
									return;
								}
								enableLoading();
								callService(THIS.config, filterConfig,
										THIS.Summary);
								$("#filterModal").modal('hide');
							});
			if (!mandatoryPass && localStorage["from"] === "") {
				disableLoading();
				$("#widget-grid > div >div:not(.search-div)").hide();
				if (THIS.additionalconfig
						&& THIS.additionalconfig.isFilterInline) {
					$('#widget-inline-filter [name="applyFilter"]').trigger(
							"click");
				} else {
					$("#filterModal").modal("show");
					$('#filterModal [name="applyFilter"]').trigger("click");
				}
				return false;
			} else {
				return true;
			}
		}
	};

	var getFiltersFromProfile = function(filterContentId) {
		filterContentId = filterContentId || "";
		if (filterContentId === "")
			return;
		var filterConfig = [];
		$('#' + filterContentId + ' section')
				.each(
						function() {
							var currentObj = $(this), isDefaultValue;
							var tempConfig = {
								"fieldName" : currentObj.data("field-name"),
								"displayName" : currentObj.data("display-name"),
								"fieldType" : currentObj.data("field-type"),
								"searchtype" : currentObj.data("searchtype"),
								"id" : $(this).attr("id"),
								"chat":currentObj.data("chat")
							};
							if (currentObj.data("profileid"))
								tempConfig.profileid = currentObj
										.data("profileid");
							if (currentObj.find("[name='searchtype']").length > 0) {
								tempConfig.searchtype = currentObj.find(
										"[name='searchtype']").val();
							}
							if (tempConfig["fieldType"] === 'lookup'
									|| tempConfig["fieldType"] === 'select'
									|| tempConfig["fieldType"] === 'lookupdropdown') {
								tempConfig["lookupquery"] = currentObj
										.data("lookup-query");
								if (tempConfig["fieldType"] === 'lookupdropdown') {
									tempConfig["defaultvalue"] = currentObj
											.find("[name='dropdowndiv']").find(
													"select").val();
								} else {
									tempConfig["defaultvalue"] = currentObj
											.find("input").select2('data');
								}
								tempConfig.isSingle = currentObj
										.data("isSingle");
								if (tempConfig.defaultvalue
										&& Object.keys(tempConfig.defaultvalue).length > 0) {
									isDefaultValue = true;
								}
							} else if (tempConfig["fieldType"] === 'dateRange') {
								tempConfig["defaultvalue"] = currentObj
										.data('defaultvalue');
								tempConfig["dateFormat"] = currentObj
										.data('dateformat');
								;
							} else if (tempConfig["fieldType"] === 'DATE'
									|| tempConfig["fieldType"] === 'dynamicDate'
									|| tempConfig["fieldType"] === 'NUM') {
								tempConfig["from-defaultvalue"] = currentObj
										.find("[name^=from-defaultvalue]")
										.val();
								tempConfig["to-defaultvalue"] = currentObj
										.find("[name^=to-defaultvalue]").val();
								tempConfig["dateFormat"] = currentObj
										.data('dateformat');
								;
								if (tempConfig["from-defaultvalue"]
										&& tempConfig["to-defaultvalue"]) {
									isDefaultValue = true;
								}
							} else {
								tempConfig["defaultvalue"] = currentObj.find(
										"input").val();
								if (tempConfig.defaultvalue) {
									isDefaultValue = true;
								}
							}
							$(currentObj).find("em").remove();
							if (currentObj.data("isMandatory")
									&& !isDefaultValue) {
								$(currentObj)
										.find("label:eq(0)")
										.append(
												$("<em/>")
														.addClass("invalid")
														.html(
																currentObj
																		.data("display-name")
																		+ " can not be empty."));
								filterConfig = false;
								return false;
							}
							filterConfig.push(tempConfig);
						});
		return filterConfig;
	};
	var populateDefaultFields = function() {
		$("#smAlgorithms").select2({
			placeholder : "Select String Matching Algorithms",
			multiple : false,
			data : algorithms
		});
		$("#source").data("type", configData["sourceEnv"]);
		// $("#lookupId").val(configData["sourceWhereCondition"]);
		$("#target").data("type", configData["targetEnv"]);
		// $("#targetFC").val(configData["targetWhereCondition"]);
		$("[name=excludeSpace]").prop("checked", !!configData["excludeSpaces"]);
		$("[name=excludeSC]").prop("checked",
				!!configData["excludeSpecialChar"]);
		$("#smAlgorithms").select2("val",
				configData["algorithms"].split(',')[0]);
	}

	var callBackProfileConfigs = function(response) {
		disableLoading();
		// drawFilters(response);
		if (response && response.length > 0 && response.length === 6) {
			var databaseList, tableList, columnList;
			if (response[0] && response[0].length > 0) {
				databaseList = new Array();
				for (var i = 0; i < response[0].length; i++) {
					databaseList.push({
						"id" : response[0][i],
						"text" : response[0][i]
					});
				}
				$("#sourceDatabase").select2({
					placeholder : "Choose Database",
					data : databaseList
				}).on("change", function(e) {
					tableSelect(e.val, type, source);
				});
				$("#sourceDatabase").select2("val", configData["sourceDB"]);
			}
			if (response[1] && response[1].length > 0) {
				databaseList = new Array();
				for (var i = 0; i < response[1].length; i++) {
					databaseList.push({
						"id" : response[1][i],
						"text" : response[1][i]
					});
				}
				$("#targetDatabase").select2({
					placeholder : "Choose Database",
					data : databaseList
				}).on("change", function(e) {
					tableSelect(e.val, type, source);
				});
				$("#targetDatabase").select2("val", configData["targetDB"]);
			}
			if (response[2] && response[2].length > 0) {
				tableList = new Array();
				for (var i = 0; i < response[2].length; i++) {
					tableList.push({
						"id" : response[2][i],
						"text" : response[2][i]
					});
				}
				$("#sourceTable").select2({
					placeholder : "Choose table",
					data : tableList
				}).on("change", function(e) {
					columnSelect(e.val, type, source, database);
				});
				$("#sourceTable").select2("val", configData["sourceTable"]);
			}
			if (response[3] && response[3].length > 0) {
				tableList = new Array();
				for (var i = 0; i < response[3].length; i++) {
					tableList.push({
						"id" : response[3][i],
						"text" : response[3][i]
					});
				}
				$("#targetTable").select2({
					placeholder : "Choose table",
					data : tableList
				}).on("change", function(e) {
					columnSelect(e.val, type, source, database);
				});
				$("#targetTable").select2("val", configData["targetTable"]);
			}
			if (response[4] && response[4].length > 0) {
				columnList = new Array();
				for (var i = 0; i < response[4].length; i++) {
					columnList.push({
						"id" : response[4][i].columnName,
						"text" : response[4][i].columnName
					});
				}
				$("#sourceSC").select2({
					placeholder : "Choose Supporting Columns",
					multiple : true,
					data : columnList
				});
				$("#sourceMC").select2({
					placeholder : "Choose Matching Column",
					data : columnList
				});
				allColumns.source = columnList;
				$("#sourceMC").select2("val", configData["sourceColumn"]);
				$("#sourceSC").select2("val",
						configData["sourceSupportingColumns"].split(','));

			}
			if (response[5] && response[5].length > 0) {
				columnList = new Array();
				for (var i = 0; i < response[5].length; i++) {
					columnList.push({
						"id" : response[5][i].columnName,
						"text" : response[5][i].columnName
					});
				}
				$("#targetSC").select2({
					placeholder : "Choose Supporting Columns",
					multiple : true,
					data : columnList
				});
				$("#targetMC").select2({
					placeholder : "Choose Matching Column",
					data : columnList
				});
				allColumns.target = columnList;
				$("#targetMC").select2("val", configData["targetColumn"]);
				$("#targetSC").select2("val",
						configData["targetSupportingColumns"].split(','));
			}
			$("#algoDiv").show();
			$("#source, #target").hide(300);
			$("#sourceConfig, #targetConfig").show(300);
			$("#backToDesign").trigger('click');
			drawFilters(THIS.filters, allColumns);
		}
	}

	var sourceSelect = function(obj, source, type) {
		if (obj) {
			source = $(obj).data("source");
			type = $(obj).data("type");
			selectedSource = source;
		}
		if (source === "hive" || source === "postgreSQL") {
			var request = {
				"source" : source
			};
			if (obj) {
				$(obj).parents(".col-sm-12:first").data("type", source);
			}
			enableLoading();
			callAjaxService("getDatabaseList", function(response) {
				callBackGetDatabaseList(response, type, source);
			}, callBackFailure, request, "POST");
		}
	};
	var callBackGetDatabaseList = function(response, type, source) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			$("#algoDiv").show();
			$("#smAlgorithms").select2({
				placeholder : "Select String Matching Algorithms",
				multiple : false,
				data : algorithms
			});
			$("#" + type).hide(300);
			$("#" + type + "Config").show(300);
			var databaseList = new Array();
			for (var i = 0; i < response.length; i++) {
				databaseList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#" + type + "Database").select2({
				placeholder : "Choose Database",
				data : databaseList
			}).on("change", function(e) {
				// selectedDatabase = e.val ;
				tableSelect(e.val, type, source);
			});
			$("#" + type + "Table").select2({
				placeholder : "Choose Table",
				data : new Array()
			});
			$("#" + type + "SC").select2({
				placeholder : "Choose Supporting Columns",
				multiple : true,
				data : new Array()
			});
			$("#" + type + "MC").select2({
				placeholder : "Choose Matching Column",
				data : new Array()
			});
		}
	};
	var tableSelect = function(database, type, source) {
		var request = {
			"source" : source,
			"database" : database
		};
		enableLoading();
		callAjaxService("getTableList", function(response) {
			callBackGetTableList(response, type, source, database);
		}, callBackFailure, request, "POST");
	};
	var callBackGetTableList = function(response, type, source, database) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var tableList = new Array();
			for (var i = 0; i < response.length; i++) {
				tableList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#" + type + "Table").select2({
				placeholder : "Choose table",
				data : tableList
			}).on("change", function(e) {
				// selectedTable = e.val;
				THIS.source = source;
				THIS.database = database;
				columnSelect(e.val, type, source, database);
			});
		}
	};
	var columnSelect = function(table, type, source, database) {
		var request = {
			"source" : source,
			"database" : database,
			"table" : table
		};
		enableLoading();
		callAjaxService("getColumnList", function(response) {
			callBackGetColumnList(response, type);
		}, callBackFailure, request, "POST");
	};
	var callBackGetColumnList = function(response, type) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var columnList = new Array();
			for (var i = 0; i < response.length; i++) {
				columnList.push({
					"id" : response[i].columnName,
					"text" : response[i].columnName
				});
			}
			$("#" + type + "SC").select2({
				placeholder : "Choose Supporting Columns",
				multiple : true,
				data : columnList
			});
			$("#" + type + "MC").select2({
				placeholder : "Choose Matching Column",
				data : columnList
			});
			$('#' + type).find('[name="field"]').select2({
				placeholder : "Choose Columns",
				// multiple : true,
				data : columnList
			});

			$('[name= "field_' + type + 'Filter"]').select2({
				placeholder : "Choose Columns",
				// multiple : true,
				data : columnList
			});

			$('[name= "field_' + type + '"]').select2({
				placeholder : "Choose Columns",
				// multiple : true,
				data : columnList
			});

			$('#' + type).find('[name= "field_source"]').select2({
				placeholder : "Choose Columns",
				// multiple : true,
				data : columnList
			});

			$('#' + type).find('[name= "field_target"]').select2({
				placeholder : "Choose Columns",
				// multiple : true,
				data : columnList
			});
		}
	};
	var generateSM = function() {
		if (mode !== 'view' && !$("#frmSM").valid()) {
			return;
		}
		enableLoading();
		var request;
		if (mode === 'view') {
			var getFiltersOnPublishForSource = getFiltersFromProfile("source");
			var getFiltersOnPublishForTarget = getFiltersFromProfile("target");
			var getFilterSource = getFilterQueryFromFilterConfig(getFiltersOnPublishForSource);
			var getFilterTarget = getFilterQueryFromFilterConfig(getFiltersOnPublishForTarget);
			configData = configurationData;
			configData["sourceWhereCondition"] = getFilterSource;
			configData["targetWhereCondition"] = getFilterTarget;
			configData["excludeSpaces"] = $("[name=excludeSpace]").is(
					":checked");
			configData["excludeSpecialChar"] = $("[name=excludeSC]").is(
					":checked");
			configData["algorithms"] = $("#smAlgorithms").select2("val");

			if (!configData["algorithms"]) {
				showNotification('error', 'Please Select an Algorithm');
				return;
			}

			request = configData;
		} else {
			var filters = this.getFilters();
			var filterQuery = getFilterQueryFromFilterConfig(filters);
			request = {
				"sourceEnv" : $("#source").data("type"),
				"sourceDB" : $("#sourceDatabase").select2("val"),
				"sourceTable" : $("#sourceTable").select2("val"),
				"sourceColumn" : $("#sourceMC").select2("val"),
				"sourceSupportingColumns" : $("#sourceSC").select2("val").join(
						","),
				"sourceWhereCondition" : getFilterQueryFromFilterConfig(
						filters, $("#source").data("type"), "source"),
				"targetEnv" : $("#target").data("type"),
				"targetDB" : $("#targetDatabase").select2("val"),
				"targetTable" : $("#targetTable").select2("val"),
				"targetColumn" : $("#targetMC").select2("val"),
				"targetSupportingColumns" : $("#targetSC").select2("val").join(
						","),
				"targetWhereCondition" : getFilterQueryFromFilterConfig(
						filters, $("#source").data("type"), "target"),
				"algorithms" : $("#smAlgorithms").select2("val"),
				"excludeSpaces" : $("[name=excludeSpace]").is(":checked"),
				"excludeSpecialChar" : $("[name=excludeSC]").is(":checked")
			};
		}
		callAjaxService("getClosestMatch", callBackGetClosestMatch,
				callBackFailure, request, "POST");
	};
	var callBackGetClosestMatch = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		response = JSON.parse(response);
		$("#wid-id-5").show(300);
		try {
			var colosFunction = [];
			var exportColumn = [];
			var coloum_config = Object.keys(response[0]);
			colosFunction
					.push({
						"data" : '<span class="smart-form"><label class="checkbox"><i></i><input type="checkbox" name="id[]" value="" /><i data-swchon-text="ON" data-swchoff-text="OFF"></i></label></span>',
						"defaultContent" : "N/A",
						"sortable" : false,
						"title" : '<span class="smart-form"><label class="checkbox"><i></i><input type="checkbox" name="id[]" value="" /><i data-swchon-text="ON" data-swchoff-text="OFF"></i></label></span>',
						"render" : function(data, type, full, meta) {
							return '<span class="smart-form"><label class="checkbox"><input type="checkbox" name="id[]" value="" /><i data-swchon-text="ON" data-swchoff-text="OFF"></i></label></span>';
						}
					});
			for (var i = 0, idx = coloum_config.length; i < idx; i++) {
				var fun = new Function('obj', 'return obj.data.'
						+ coloum_config[i].trim());
				var colTitle = lookUpTable.get(coloum_config[i].trim());
				if (colTitle == null) {
					colTitle = coloum_config[i].trim();
				}
				colosFunction.push({
					"data" : coloum_config[i].trim(),
					"defaultContent" : "N/A",
					"sortable" : true,
					"title" : colTitle,
				});
			}
			var facetTable = $("#detailTable")
					.dataTable(
							{
								"language" : {
									"zeroRecords" : "No records to display"
								},
								"dom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
										+ "t"
										+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"destroy" : true,
								"processing" : true,
								"lengthChange" : true,
								"bSort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : response,
								"bAutoWidth" : true,
								"columns" : colosFunction
							});
		} catch (err) {
			console.log(err);
		}
	};

	var saveConfig = function() {
		if (!$("#frmSM").valid())
			return;
		enableLoading();
		var filters = getFilters();
		var filterQuery = getFilterQueryFromFilterConfig(filters);
		var configRequest = {
			"sourceEnv" : $("#source").data("type"),
			"sourceDB" : $("#sourceDatabase").select2("val"),
			"sourceTable" : $("#sourceTable").select2("val"),
			"sourceColumn" : $("#sourceMC").select2("val"),
			"sourceSupportingColumns" : $("#sourceSC").select2("val").join(","),
			"sourceWhereCondition" : getFilterQueryFromFilterConfig(filters, $(
					"#source").data("type"), "source"),
			"targetEnv" : $("#target").data("type"),
			"targetDB" : $("#targetDatabase").select2("val"),
			"targetTable" : $("#targetTable").select2("val"),
			"targetColumn" : $("#targetMC").select2("val"),
			"targetSupportingColumns" : $("#targetSC").select2("val").join(","),
			"targetWhereCondition" : getFilterQueryFromFilterConfig(filters, $(
					"#source").data("type"), "target"),
			"algorithms" : $("#smAlgorithms").select2("val"),
			"excludeSpaces" : $("[name=excludeSpace]").is(":checked"),
			"excludeSpecialChar" : $("[name=excludeSC]").is(":checked")
		};
		var request = {
			"source" : '',
			"database" : '',
			"table" : '',
			"fact" : 'stringMatching',
			"customfacts" : '',
			"config" : JSON.stringify(configRequest),
			"filters" : JSON.stringify(filters),
			"summary" : '',
			"remarks" : description,
			"displayname" : title,
			"additionalconfig" : null,
			"type" : "stringMatching",
			"id" : (id === 0) ? null : id
		};
		callAjaxService("saveProfile", callBackSaveProfile, callBackFailure,
				request, "POST");
	}

	var callBackSaveProfile = function(response) {
		disableLoading();
		$('#myModalSaveSearch').modal('show');
		if (response && response.isException) {
			showNotification('error', response.customMessage);
			return;
		}
		showNotification('success', 'Profile Saved Successfully');
		id = response;
	}

	var getProfiles = function() {
		var request = {
			type : 'stringMatching',
			getAll : 'true'
		};
		enableLoading();
		callAjaxService("getMyProfiles", callBackGetProfiles, callBackFailure,
				request, "POST");
	}

	var callBackGetProfiles = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			$("#myProfileList").empty();
			for (var i = 0; i < response.length; i++) {
				var publishStatus = "Publish";
				var displayStatus = '';
				if (publishStatus === '') {
					displayStatus = 'none';
				}
				var btnClass = "btn-outline";
				var actionId = 0;
				var canPublish = true;
				if (response[i].profilekey && response[i].profilekey != null
						&& response[i].profilekey != "") {
					publishStatus = "Published";
					btnClass = "btn-outline-success";
					actionId = response[i].actionid;
				}
				$("#myProfileList")
						.append(
								$("<li/>")
										.unbind("click")
										.bind("click", response[i],
												function(e) {
													id = e.data.id;
													getById();
												})
										.addClass("list-group-item")
										.data({
											"id" : response[i].id
										})
										.append(
												$("<a/>")
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-2")
																		.html(
																				response[i].displayname))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-4")
																		.html(
																				response[i].database))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-2")
																		.html(
																				response[i].configtable))
														.append(
																$("<span/>")
																		.addClass(
																				"text-left col-md-3")
																		.html(
																				response[i].remarks))
														.append(
																$("<button/>")
																		.addClass(
																				btnClass
																						+ " col-md-1")
																		.css(
																				'display',
																				displayStatus)
																		.html(
																				publishStatus)
																		.attr(
																				{
																					'data-target' : '#profilePublishModal',
																					'data-toggle' : 'modal',

																				})
																		.unbind(
																				'click')
																		.bind(
																				'click',
																				response[i],
																				function(
																						e) {
																					e
																							.stopPropagation();
																					previewPublishProfile(
																							e.data,
																							this);
																				}))));
			}
		}
	}

	if (configData) {
		$('#algoDiv').show();
		mode = 'view';
		populateDefaultFields();
		if (filters && filters.length > 0) {
			constructFilters(filters.filter(function(o) {
				return o.diffType === "source"
			}), "source");
			constructFilters(filters.filter(function(o) {
				return o.diffType === "target"
			}), "target");
		}
		configurationData = configData;
		generateSM();

	} else {
		mode = 'config';
		getProfiles();

	}
	// filter implementation code
	var drawFilters = function(filters, allColumns, type) {
		$("#filters").empty();
		if (filters && filters.length > 0) {
			for (var i = 0; i < filters.length; i++) {
				var tempFilterObj = drawFilter(filters[i]);
				// $(tempFilterObj).find("[name=field]").select2('val',filters[i].fieldName);
				$(tempFilterObj).find("[name=displayname]").val(
						filters[i].displayName);
				$(tempFilterObj).find("[name=fieldtype]").val(
						filters[i].fieldType);
				if (filters[i].diffType === "source") {
					$(tempFilterObj).find("[name=field_source]").select2({
						data : allColumns.source
					}).select2('val', filters[i].fieldName);
				} else {
					$(tempFilterObj).find("[name=field_target]").select2({
						data : allColumns.target
					}).select2('val', filters[i].fieldName);
				}
				if (filters[i].isMandatory) {
					$(tempFilterObj).find("[name=isMandatory]").attr("checked",
							"checked");
				}
				if (filters[i].invisible) {
					$(tempFilterObj).find("[name=invisible]").attr("checked",
							"checked");
				}
				if (filters[i].innotin) {
					$(tempFilterObj).find("[name=innotin]").attr("checked",
							"checked");
				}
				$(tempFilterObj).find("[name=searchtype]").val(
						filters[i].searchtype ? $.trim(filters[i].searchtype)
								.toLowerCase() : "in");
				onChangeTypeInFieldConfig(tempFilterObj
						.find("[name=fieldtype]"), filters[i]);
				if (filters[i].fieldType === "lookup"
						|| filters[i].fieldType === "select") {
					if (filters[i].isSingle) {
						$(tempFilterObj).find("[name=selecttype]").val(
								filters[i].isSingle);
					}
					$(tempFilterObj).find("[name=lookup]").val(
							filters[i].lookupquery);
					if (filters[i].defaultvalue
							&& Object.keys(filters[i].defaultvalue).length > 0) {
						$(tempFilterObj).find("[name=defaultvalue]").select2(
								'data', filters[i].defaultvalue, true);
					}

				} else if (filters[i].fieldType === 'DATE'
						|| filters[i].fieldType === 'dynamicDate'
						|| filters[i].fieldType === 'NUM') {
					$(tempFilterObj).find("[name=from-defaultvalue]").val(
							filters[i]["from-defaultvalue"]);
					$(tempFilterObj).find("[name=to-defaultvalue]").val(
							filters[i]["to-defaultvalue"]);
					$(tempFilterObj).find($("[name='lookupdiv']")).find(
							$("input[name='lookup']")).val(
							filters[i]["dateFormat"]);

				} else if (filters[i].fieldType === 'dateRange') {
					$(tempFilterObj).data(filters[i].defaultvalue);
					$(tempFilterObj).find($("[name='lookupdiv']")).find(
							$("input[name='lookup']")).val(
							filters[i]["dateFormat"]);
					var previousSelection = dataAnalyser
							.storedDateRange(filters[i].defaultvalue);
					if (previousSelection != "") {
						$(tempFilterObj).find("[name=from-defaultRangevalue]")
								.val(previousSelection[0].trim());
						$(tempFilterObj).find("[name=to-defaultRangevalue]")
								.val(previousSelection[1].trim());
					}

				} else {
					$(tempFilterObj).find("[name=defaultvalue]").val(
							filters[i].defaultvalue);
				}
			}
		} else
			drawFilter(fields);
	};

	var drawFilter = function(obj) {
		var findClickedDiv = $(obj).attr("data-select") || obj.diffType;
		var selectedDatabase = $(obj).parents()
				.find(
						'#' + findClickedDiv + 'Config #' + findClickedDiv
								+ 'Database').val()
				|| "";
		var selectedTable = $(obj).parents().find(
				'#' + findClickedDiv + 'Config #' + findClickedDiv + 'Table')
				.val()
				|| "";
		$('#wid-id-filter').show();
		$("#dialog").hide();
		$(".lastRange,.nextRange").attr("disabled", true);
		var tempObj = $("<div/>").addClass(
				"well col-sm-12 col-md-12 margin-bottom-10 padding-bottom-10")
				.attr(
						{
							'id' : findClickedDiv + 'f'
									+ ($("#filters").find(".well").length + 1),
							'data-filter' : findClickedDiv
						});
		tempObj
				.append(
						$("<div/>").addClass("col col-2").append(
								$("<div/>").addClass("note padding-2").append(
										$("<strong/>").html("Field"))).append(
								$("<label/>").addClass("input").append(
										$("<input/>").attr({
											type : "text",
											name : "field_" + findClickedDiv
										}).addClass("width-100per").val(""))))
				.append(
						$("<div/>").addClass("col col-2").append(
								$("<div/>").addClass("note padding-2").append(
										$("<strong/>").html("Display Name")))
								.append(
										$("<label/>").addClass("input").append(
												$("<input/>").attr({
													type : "text",
													name : "displayname"
												}).val(""))))
				.append(
						$("<div/>")
								.addClass("col col-1")
								.append(
										$("<label/>")
												.addClass("note padding-2")
												.append(
														$("<strong/>").html(
																"Type")))
								.append(
										$("<Select/>")
												.change(
														function() {
															onChangeTypeInFieldConfig(this);
														})
												.addClass(
														"form-control input-sm")
												.attr({
													name : "fieldtype"
												})
												.append(
														$("<option/>")
																.attr("value",
																		"TEXT")
																.html("Text"))
												.append(
														$("<option/>")
																.attr("value",
																		"DATE")
																.html("Date"))
												.append(
														$("<option/>")
																.attr("value",
																		"dynamicDate")
																.html(
																		"Dynamic Date"))
												.append($("<option/>").attr({
													"value" : "dateRange"
												}).html("Date Range"))
												.append(
														$("<option/>").attr(
																"value", "NUM")
																.html("Number"))
												.append(
														$("<option/>").attr(
																"value",
																"lookup").html(
																"Look Up"))
												.append(
														$("<option/>").attr(
																"value",
																"select").html(
																"Drop Down"))))
				.append(
						$("<div/>").addClass("col col-4").append(
								$("<div/>").addClass("note padding-2").append(
										$("<strong/>").html("Default Value")))
								.append(
										$("<label/>").attr({
											"name" : "defaultvaluelabel"
										}).addClass("input").append(
												$("<input/>").attr({
													type : "text",
													name : "defaultvalue"
												}))))
				.append(
						$("<div/>").addClass("col col-0").append(
								$("<div/>").addClass("note").append(
										$("<strong/>").html("Invisible")))
								.append(
										$("<label/>").addClass("toggle")
												.append($("<input/>").attr({
													"type" : "checkbox",
													"name" : "invisible"
												})).append($("<i/>").attr({
													"data-swchon-text" : "On",
													"data-swchoff-text" : "Off"
												}))))
				.append(
						$("<div/>").addClass("col col-0").append(
								$("<div/>").addClass("note").append(
										$("<strong/>").html("Mandatory")))
								.append(
										$("<label/>").addClass("toggle")
												.append($("<input/>").attr({
													"type" : "checkbox",
													"name" : "isMandatory"
												})).append($("<i/>").attr({
													"data-swchon-text" : "On",
													"data-swchoff-text" : "Off"
												}))))
				.append(
						$("<div/>")
								.addClass("col col-0")
								.append(
										$("<div/>")
												.addClass("note")
												.append(
														$("<strong/>")
																.html(
																		"Add "
																				+ findClickedDiv)))
								/* .append($("<div/>").addClass("note").append($("<strong/>").html("Add"))) */
								.append(
										$("<i/>")
												.addClass(
														"fa fa-md fa-fw fa-plus-square color-blue cursor-pointer")
												.attr("title", "add link")
												.unbind("click").bind("click",
														function() {
															drawFilter(obj);
														}))
								.append(
										$("<i/>")
												.addClass(
														"fa fa-md fa-fw fa fa-remove txt-color-red cursor-pointer")
												.attr("title", "remove link")
												.unbind("click").bind("click",
														function() {
															removeFilter(this);
														})))
				.append(
						$("<div/>")
								.addClass("col col-9")
								.attr({
									"name" : "lookupdiv"
								})
								.append($("<div/>").addClass("note").attr({
									"id" : "lookupId" + findClickedDiv
								}).append($("<strong/>").html("Look Up Query")))
								.append(
										$("<label/>")
												.addClass("input")
												.append(
														$("<input/>")
																.attr(
																		{
																			type : "text",
																			name : "lookup",
																			id : "lookUpId"
																					+ findClickedDiv
																		}))))
				.append(
						$("<div/>")
								.addClass("col col-1")
								.attr({
									"name" : "lookupdiv"
								})
								.append(
										$("<div/>").addClass("note").append(
												$("<strong/>").html(
														"Select Type")))
								.append(
										$("<label/>")
												.addClass("input")
												.append(
														$("<select/>")
																.addClass(
																		"input-sm")
																.append(
																		$(
																				"<option/>")
																				.attr(
																						"value",
																						"")
																				.html(
																						"Multiple"))
																.append(
																		$(
																				"<option/>")
																				.attr(
																						"value",
																						"true")
																				.html(
																						"Single"))
																.attr(
																		{
																			name : "selecttype"
																		}))))
				.append(
						$("<div/>")
								.addClass("col col-1")
								.append(
										$("<div/>").addClass("note").append(
												$("<strong/>").html(
														"Search Type")))
								.append(
										$("<label/>")
												.addClass("input")
												.append(
														$("<select/>")
																.addClass(
																		"input-sm")
																.attr(
																		{
																			name : "searchtype"
																		})
																.append(
																		$(
																				"<option/>")
																				.html(
																						"In")
																				.attr(
																						"value",
																						"in"))
																.append(
																		$(
																				"<option/>")
																				.html(
																						"Not In")
																				.attr(
																						"value",
																						"not in"))
																.append(
																		$(
																				"<option/>")
																				.html(
																						"Like")
																				.attr(
																						"value",
																						"like"))
																.append(
																		$(
																				"<option/>")
																				.html(
																						"Not Like")
																				.attr(
																						"value",
																						"not like")))))
				.append(
						$("<div/>").addClass("col col-0").append(
								$("<div/>").addClass("note").append(
										$("<strong/>").html("Show Type")))
								.append(
										$("<label/>").addClass("toggle")
												.append($("<input/>").attr({
													"type" : "checkbox",
													"name" : "innotin"
												})).append($("<i/>").attr({
													"data-swchon-text" : "On",
													"data-swchoff-text" : "Off"
												}))));
		$("#filters").append(tempObj);
		if (findClickedDiv === "source") {
			tempObj.find($("[data-filter='source']").attr('style',
					'background-color: antiquewhite;'));
			if (configData != null && configData != undefined
					&& configData != "") {
				selectedTable = configData.sourceTable;
				selectedSource = configData.sourceEnv;
				selectedDatabase = configData.sourceDB;
			}

		} else if (findClickedDiv === "target") {
			$("[data-filter='target']").attr('style',
					'background-color: gainsboro;');
			if (configData != null && configData != undefined
					&& configData != "") {
				selectedTable = configData.targetTable;
				selectedSource = configData.targetEnv;
				selectedDatabase = configData.targetDB;

			}
		}

		var currentSelection = tempObj.attr('id');
		columnSelect(selectedTable, currentSelection, selectedSource,
				selectedDatabase);
		return tempObj;
	}
	$("[name='addFilterSource'], [name='addFilterTarget'] ").unbind('click')
			.bind('click', function() {
				drawFilter(this);
			});
	var removeFilter = function(obj) {
		$(obj).parents(".well:first").remove();
	}

	/**
	 * Configures Filter data and returns the value
	 * 
	 */
	this.getFilters = function() {
		return getFilters();
	};
	var getFilters = function() {
		var filterConfig = [];
		$("#filters > .well")
				.each(
						function(index) {
							var currentObj = $(this);
							if (currentObj.find("[name=field]").select2('val')
									&& currentObj.find("[name=field]").select2(
											'val') !== "") {
								if (($(this).data("filter") === "source")){
									var tempConfig1 = {
										// "fieldName" :
										// currentObj.find("[name=field_sourceFilter]").select2('val'),
										"fieldName" : currentObj.find(
												"[name=field_source]").select2(
												'val'),
										"displayName" : currentObj.find(
												"[name=displayname]").val(),
										"fieldType" : currentObj.find(
												"[name=fieldtype]").val(),
										'id' : 'f' + (index + 1),
										'diffType' : currentObj.data('filter')
									};
									if (currentObj.find("[name=isMandatory]")
											.is(":checked")) {
										tempConfig1.isMandatory = true;
									}
									if (currentObj.find("[name=invisible]").is(
											":checked")) {
										tempConfig1.invisible = true;
									}
									if (currentObj.find("[name=innotin]").is(
											":checked")) {
										tempConfig1.innotin = true;
									}
									if (currentObj.find("[name=searchtype]")) {
										tempConfig1.searchtype = currentObj
												.find("[name=searchtype]")
												.val();
									}
									if (tempConfig1["fieldType"] === 'lookup'
											|| tempConfig1["fieldType"] === 'select') {
										tempConfig1["lookupquery"] = currentObj
												.find("[name=lookup]").val();
										tempConfig1["defaultvalue"] = currentObj
												.find("[name=defaultvalue]")
												.select2('data');
										tempConfig1.isSingle = currentObj.find(
												"[name=lookupdiv]:eq(1)").find(
												'select').val();
									} else if (tempConfig1["fieldType"] === 'DATE'
											|| tempConfig1["fieldType"] === 'dynamicDate'
											|| tempConfig1["fieldType"] === 'NUM') {
										tempConfig1["from-defaultvalue"] = currentObj
												.find(
														"[name=from-defaultvalue]")
												.val();
										tempConfig1["to-defaultvalue"] = currentObj
												.find("[name=to-defaultvalue]")
												.val();
										if (currentObj.find('div').find(
												$("input[name='lookup']"))
												.val() != '') {
											tempConfig1["dateFormat"] = currentObj
													.find('div')
													.find(
															$("input[name='lookup']"))
													.val();
										}

									} else if (tempConfig1["fieldType"] === 'dateRange') {
										if (currentObj.find('div').find(
												$("input[name='lookup']"))
												.val() != '') {
											tempConfig1["dateFormat"] = currentObj
													.find('div')
													.find(
															$("input[name='lookup']"))
													.val();
										}
										tempConfig1["defaultvalue"] = currentObj
												.data();
									} else {
										tempConfig1["defaultvalue"] = currentObj
												.find("[name=defaultvalue]")
												.val();
									}
									filterConfig.push(tempConfig1);
								} else if ($(this).data("filter") === "target") {
									var tempConfig2 = {
										"fieldName" : currentObj.find(
												"[name=field_target]").select2(
												'val'),
										"displayName" : currentObj.find(
												"[name=displayname]").val(),
										"fieldType" : currentObj.find(
												"[name=fieldtype]").val(),
										'id' : 'f' + (index + 1),
										'diffType' : currentObj.data('filter')
									};
									if (currentObj.find("[name=isMandatory]")
											.is(":checked")) {
										tempConfig2.isMandatory = true;
									}
									if (currentObj.find("[name=invisible]").is(
											":checked")) {
										tempConfig2.invisible = true;
									}
									if (currentObj.find("[name=innotin]").is(
											":checked")) {
										tempConfig2.innotin = true;
									}
									if (currentObj.find("[name=searchtype]")) {
										tempConfig2.searchtype = currentObj
												.find("[name=searchtype]")
												.val();
									}
									if (tempConfig2["fieldType"] === 'lookup'
											|| tempConfig2["fieldType"] === 'select') {
										tempConfig2["lookupquery"] = currentObj
												.find("[name=lookup]").val();
										tempConfig2["defaultvalue"] = currentObj
												.find("[name=defaultvalue]")
												.select2('data');
										tempConfig2.isSingle = currentObj.find(
												"[name=lookupdiv]:eq(1)").find(
												'select').val();
									} else if (tempConfig2["fieldType"] === 'DATE'
											|| tempConfig2["fieldType"] === 'dynamicDate'
											|| tempConfig2["fieldType"] === 'NUM') {
										tempConfig2["from-defaultvalue"] = currentObj
												.find(
														"[name=from-defaultvalue]")
												.val();
										tempConfig2["to-defaultvalue"] = currentObj
												.find("[name=to-defaultvalue]")
												.val();
										if (currentObj.find('div').find(
												$("input[name='lookup']"))
												.val() != '') {
											tempConfig2["dateFormat"] = currentObj
													.find('div')
													.find(
															$("input[name='lookup']"))
													.val();
										}

									} else if (tempConfig2["fieldType"] === 'dateRange') {
										if (currentObj.find('div').find(
												$("input[name='lookup']"))
												.val() != '') {
											tempConfig2["dateFormat"] = currentObj
													.find('div')
													.find(
															$("input[name='lookup']"))
													.val();
										}
										tempConfig2["defaultvalue"] = currentObj
												.data();
									} else {
										tempConfig2["defaultvalue"] = currentObj
												.find("[name=defaultvalue]")
												.val();
									}
									filterConfig.push(tempConfig2);
								}
							}
						});
		return filterConfig;
	};

	var formatforDate = $('#filters').find("[name='lookupdiv']").find(
			"[name='lookup']").val();
	var onChangeTypeInFieldConfig = function(obj, data) {
		var inputLabel = $(obj).parent().parent().find(
				"[name='defaultvaluelabel']");
		// $(obj).parent().next().find("label:eq(1)");
		$(inputLabel).removeClass().empty();
		if ($(obj).val() === 'lookup' || $(obj).val() === 'select') {
			var mainDiv = $(obj).parent().parent();
			// $(inputLabel).addClass("input").append($("<input/>").attr({"type":"text","name":"defaultvalue"}));
			mainDiv.find("[name='lookupdiv']").show().find("select").unbind(
					"change").bind("change", function() {
				constructDefaultValueSelect();
			});
			mainDiv.find("[name='lookupdiv']:eq(0)").find("input").unbind(
					"blur").bind("blur", function() {
				constructDefaultValueSelect();
			});
			function formatResult(resp) {
				return resp.id + ' - ' + resp.name;
			}

			function formatSelection(resp) {
				return resp.id + ' - ' + resp.name;
			}

			function constructDefaultValueSelect() {
				var selectType = mainDiv.find("[name='lookupdiv']:eq(1)").find(
						"select").val();
				if (data && data.isSingle) {
					selectType = data.isSingle;
				}
				// $(obj).parent().parent().find("defaultvaluelabel]").select2('destroy');
				if ($(obj).val() === 'select') {
					var selectdata = mainDiv.find("[name='lookupdiv']:eq(0)")
							.find('input').val()
							|| data.lookupquery;
					if (selectdata) {
						selectdata = Jsonparse(selectdata, "FILTER LOOKUP");
						if (!selectdata || Object.keys(selectdata).length === 0) {
							selectdata = [];
							var values = mainDiv.find(
									"[name='lookupdiv']:eq(0)").find('input')
									.val()
									|| data.lookupquery;
							values = values.split(',');
							if (values && values.length > 0) {
								for (var i = 0; i < values.length; i++) {
									selectdata.push({
										id : values[i],
										text : values[i]
									});
								}
							}
						}
					} else if (data.defaultvalue) {
						selectdata = data.defaultvalue;
					}
					$(obj).parent().parent().find("[name='lookupdiv']:eq(0)")
							.find("strong").html("Enter Select value");
					$(inputLabel).find("[name='defaultvalue']").select2(
							{
								placeholder : "Enter "
										+ $(obj).parent().prev().find("input")
												.val(),
								allowClear : selectType,
								multiple : !selectType,
								width : 'resolve!important',
								data : selectdata || []
							});
				} else {
					mainDiv.find("[name='lookupdiv']:eq(0)").find("strong")
							.html("Look Up Query");
					var selectedData;
					if (data && data.defaultvalue) {
						selectedData = data.defaultvalue;
					}
					$(inputLabel)
							.find("[name='defaultvalue']")
							.select2(
									{
										placeholder : "Enter "
												+ $(obj).parent().prev().find(
														"input").val(),
										minimumInputLength : 1,
										allowClear : selectType,
										width : 'resolve!important',
										multiple : !selectType,
										ajax : {
											url : serviceCallMethodNames["suggestionsLookUp"].url,
											dataType : 'json',
											type : 'GET',
											data : function(term) {
												return {
													q : term,
													source : source,
													database : database,
													lookupQuery : mainDiv
															.find(
																	"[name='lookupdiv']:eq(0)")
															.find("input")
															.val()
												};
											},
											results : function(data) {
												return {
													results : data
												};
											}
										},
										initSelection : function(element,
												callback) {
											callback($.map(element.val().split(
													','), function(id) {
												id = id.split(":");
												return {
													id : id[0],
													name : id[1]
												};
											}));
										},
										formatResult : formatResult,
										formatSelection : formatSelection,
										dropdownCsclass : "bigdrop",
										escapeMarkup : function(m) {
											return m;
										}
									});

					if (selectedData && Object.keys(selectedData).length > 0) {
						if (selectType) {
							selectedData = Array.isArray(selectedData) ? selectedData[0]
									: selectedData;
						} else {
							selectedData = Array.isArray(selectedData) ? selectedData
									: [ selectedData ];
						}
						$(obj).parent().parent().find("[name='defaultvalue']")
								.select2('data', selectedData);
					}
				}
			}
			$(inputLabel).addClass("input").append($("<input/>").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}).addClass("width-100per"));
			constructDefaultValueSelect();

		} else if ($(obj).val() === 'dateRange') {
			$(obj).parent().parent().find($("[name='lookupdiv'] #lookupId"))
					.find('strong').text('Date Format');
			$(inputLabel).addClass("row").append(
					$('<span/>').addClass('col-sm-2 text-right').html('From:'))
					.append($('<input/>').addClass('col-sm-3 input-xs').attr({
						"name" : "from-defaultRangevalue",
						"readonly" : true
					})).append(
							$('<span/>').addClass('col-sm-1 text-right').html(
									'To:')).append(
							$('<input/>').addClass('col-sm-3 input-xs').attr({
								"name" : "to-defaultRangevalue",
								"readonly" : true
							}));
			$(
					"input[name='from-defaultRangevalue'],input[name='to-defaultRangevalue']")
					.click(
							function() {
								currentFieldset = $(this).parents('.well');
								var currentindex = $(this).parent();
								$("#dialog").dialog(
										{
											autoOpen : false,
											width : 500,
											resizable : false,
											height : 250,
											open : function(event) {
												$('#savedRange').find('span')
														.html('');
												$('input[type="radio"]').prop(
														'checked', false);
												$(".nextRange").attr(
														"disabled", true);
												$(".lastRange").attr(
														"disabled", true);
												$(".nextRange").val('');
												$(".lastRange").val('');
											}
										});
								$("#dialog").data(currentindex);
								$("#dialog").dialog("open");
								if ((data && currentFieldset.attr('id') === data.id)
										&& jQuery.isEmptyObject(currentFieldset
												.data())) {
									dataAnalyser
											.storedDateRange(data.defaultvalue);
								} else if (!jQuery
										.isEmptyObject(currentFieldset.data())) {
									dataAnalyser
											.storedDateRange(currentFieldset
													.data());
								}
							});
		} else if ($(obj).val() === 'DATE' || $(obj).val() === 'dynamicDate'
				|| $(obj).val() === 'NUM') {
			$(obj).parent().parent().find($("[name='lookupdiv'] #lookupId"))
					.find('strong').text('Look Up Query');
			if ($(obj).val() != 'NUM') {
				$(obj).parent().parent()
						.find($("[name='lookupdiv'] #lookupId")).find('strong')
						.text('Date Format');
			}
			$(inputLabel).addClass("row").append(
					$('<span/>').addClass('col-sm-2 text-right').html('From:'))
					.append(
							$('<input/>').addClass('col-sm-3 input-xs').attr(
									"name", "from-defaultvalue")).append(
							$('<span/>').addClass('col-sm-1 text-right').html(
									'To:')).append(
							$('<input/>').addClass('col-sm-3 input-xs').attr(
									"name", "to-defaultvalue"));
			if ($(obj).val() === 'DATE') {
				$(inputLabel).find('input:eq(0)').datepicker(
						{
							defaultDate : "+1w",
							changeMonth : true,
							onClose : function(selectedDate) {
								$(inputLabel).find('input:eq(1)').datepicker(
										"option", "minDate", selectedDate);
							}
						});
				$(inputLabel).find('input:eq(1)').datepicker(
						{
							defaultDate : "+1w",
							changeMonth : true,
							onClose : function(selectedDate) {
								$(inputLabel).find('input:eq(0)').datepicker(
										"option", "maxDate", selectedDate);
							}
						});
			}
			// $(obj).parent().parent().find("[name='lookupdiv']").hide();
		} else {
			$(obj).parent().parent().find($("[name='lookupdiv'] #lookupId"))
					.find('strong').text('Look Up Query');
			$(inputLabel).addClass("input").append($("<input/>").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}));
			// $(obj).parent().parent().find("[name='lookupdiv']").hide();

		}
		if ($(obj).val() != 'DATE' || $(obj).val() != 'dynamicDate'
				|| $(obj).val() != 'dateRange') {
			$(obj).parent().parent().find('div').find(
					$("[name='lookup']#lookUpId")).val('');
		}
	};

	this.storedDateRange = function(dataObject) {
		var radioOpt = dataObject.currentRangeType;
		$('#dialog').find('.tab-content').find('.tab-pane.fade').removeClass(
				'active');
		$('#myTab a[href="#' + radioOpt + '"]').tab('show');
		$('#' + radioOpt).addClass('active');
		var radioOption = $('#' + radioOpt).find($("input[type='radio']"));// list
																			// of
																			// radio
																			// buttons
		var val = dataObject.radioValue; // local storage value
		var selected = dataObject.selectedRangeDate;

		for (var i = 0; i < radioOption.length; i++) {
			if (radioOption[i].value == val) {
				radioOption[i].checked = true; // marking the required radio as
												// checked
				if (val === 'lastRangeValue'
						&& $("input[name='" + radioOpt + "']:checked").val() === 'lastRangeValue') {
					$('#' + radioOpt).find('#' + radioOpt + 'LastRange').val(
							selected);
					$('#' + radioOpt + 'NextRange').attr("disabled", true);
					$('#' + radioOpt + 'LastRange').attr("disabled", false);
				} else if (val === 'nextRangeValue'
						&& $("input[name='" + radioOpt + "']:checked").val() === 'nextRangeValue') {
					$('#' + radioOpt).find('#' + radioOpt + 'NextRange').val(
							selected);
					$('#' + radioOpt + 'NextRange').attr("disabled", false);
					$('#' + radioOpt + 'LastRange').attr("disabled", true);
				}
			}
		}
		return dateRangeFun(val, radioOpt, selected);
	};

	$("#dialog input[type='number']").bind(
			'keyup mouseup',
			function() {
				selectedRangeDate = $(this).val();
				if (selectedRangeDate != "") {
					var dataVal = $(this).data("value");
					var selectTab = $('#dialog').find('.active').find('a')
							.text().toLowerCase();
					dynamicRange(dataVal, selectTab, selectedRangeDate);
				}
			});
	var dynamicRange = function(dataVal, selectTab, selectedRangeDate) {
		var dataValue = dataVal;
		var selectedTab = selectTab;
		selectedRangeDate = selectedRangeDate;
		var result;
		if (dataValue === 'nextRangeValue' && selectedTab === "day") {
			result = getManipulatedDate(selectedTab) + ' To '
					+ getManipulatedDate(selectedTab, selectedRangeDate);
		} else if (dataValue === 'lastRangeValue' && selectedTab === "day") {
			selectedRangeDate = '-' + selectedRangeDate;
			result = getManipulatedDate(selectedTab, selectedRangeDate)
					+ ' To ' + getManipulatedDate(selectedTab);
		} else if (dataValue === 'nextRangeValue') {
			result = getTruncateDate(selectedTab,
					getManipulatedDate(selectedTab))
					+ ' To '
					+ getTruncateDate(selectedTab, getManipulatedDate(
							selectedTab, selectedRangeDate), true);
		} else if (dataValue === 'lastRangeValue') {
			selectedRangeDate = '-' + selectedRangeDate;
			result = getTruncateDate(selectedTab, getManipulatedDate(
					selectedTab, selectedRangeDate))
					+ ' To '
					+ getTruncateDate(selectedTab,
							getManipulatedDate(selectedTab), true);
		}
		$('#savedRange').find('span').html(result);
		return result;
	};

	$('#myTab li a').on(
			'click',
			function() {
				var currentSelectedTab = $(this).attr('data-attr');
				// $('#dialog').find('.tab-content').find('.tab-pane.fade').removeClass('active');
				// $('#dialog').find('.tab-content').find('#'+currentSelectedTab).addClass('active');
				var fieldSet = currentFieldset.data();
				if ($('#sankeyModalFilterContent').length != 0 && fieldSet) {
					fieldSet = currentFieldset.data('defaultvalue');
				}
				if (!jQuery.isEmptyObject(fieldSet)
						&& fieldSet.currentRangeType != currentSelectedTab) {
					// $( ".selectedRange" ).html('');
					$('#savedRange').find('span').html('');
					$('input[type="radio"]').prop('checked', false);
					$(".nextRange").attr("disabled", true);
					$(".lastRange").attr("disabled", true);
					$(".lastRange").val('');
					$(".nextRange").val('');
				} else if (!jQuery.isEmptyObject(fieldSet)
						&& fieldSet.currentRangeType === currentSelectedTab) {
					dataAnalyser.storedDateRange(fieldSet);
				}
			});
	$('.saveYear').click(
			function() {
				selectedRange = $('#savedRange').find('span').text();
				var selectedDateRange = selectedRange.split("To");
				if (selectedDateRange != "") {
					if ($('#sankeyModalFilterContent').length != 0) {
						currentFieldset.data("defaultvalue", {
							"currentRangeType" : currentRangeType,
							"selectedRangeDate" : selectedRangeDate,
							"radioValue" : radioValue,

						});
						$("#dialog").data().find(
								$("input[name='from-defaultvalue']")).val(
								selectedDateRange[0].trim());
						$("#dialog").data().find(
								$("input[name='to-defaultvalue']")).val(
								selectedDateRange[1].trim());
					} else {
						currentFieldset.data({
							"currentRangeType" : currentRangeType,
							"selectedRangeDate" : selectedRangeDate,
							"radioValue" : radioValue,

						});
						$("#dialog").data().find(
								$("input[name='from-defaultRangevalue']")).val(
								selectedDateRange[0].trim());
						$("#dialog").data().find(
								$("input[name='to-defaultRangevalue']")).val(
								selectedDateRange[1].trim());
					}

				}
				$('#dialog').dialog('close');
				return false;
			});

	$("#dialog input[type='radio']").click(
			function() {
				var currentlySelectedRangeType = this.name;
				var currentselection = $(
						"input[name='" + this.name + "']:checked").val();
				dateRangeFun(currentselection, currentlySelectedRangeType);
			});

	var dateRangeFun = function(currentselection, currentlySelectedRangeType,
			Range) {
		var result, calculatedRange, calculatedDateRange;
		currentRangeType = currentlySelectedRangeType;
		radioValue = currentselection;
		if (radioValue === 'previous' || radioValue === 'next') {
			selectedRangeDate = -1;
			if (radioValue === 'next') {
				selectedRangeDate = 1;
			}
			calculatedRange = getTruncateDate(currentRangeType,
					getManipulatedDate(currentRangeType, selectedRangeDate))
					+ ' To '
					+ getTruncateDate(currentRangeType, getManipulatedDate(
							currentRangeType, selectedRangeDate), true);
		} else if (radioValue === 'current') {
			selectedRangeDate = 0;
			calculatedRange = getTruncateDate(currentRangeType) + ' To '
					+ getTruncateDate(currentRangeType, '', true);
		} else if (radioValue === 'currentDay' || radioValue === 'previousDay') {
			selectedRangeDate = 0;
			if (radioValue === 'previousDay') {
				selectedRangeDate = -1;
			}
			calculatedRange = getManipulatedDate(currentRangeType,
					selectedRangeDate)
					+ ' To ' + getManipulatedDate(currentRangeType);
		} else if (radioValue === 'nextDay') {
			selectedRangeDate = 1;
			calculatedRange = getManipulatedDate(currentRangeType) + ' To '
					+ getManipulatedDate(currentRangeType, selectedRangeDate);
		}
		$("#" + currentRangeType + "LastRange").val('');
		$("#" + currentRangeType + "NextRange").val('');
		$("#" + currentRangeType + "LastRange").attr("disabled", true);
		$("#" + currentRangeType + "NextRange").attr("disabled", true);
		if (radioValue === 'nextRangeValue'
				&& $("input[name='" + currentRangeType + "']").is(':checked')) {
			selectedRangeDate = Range;
			$("#" + currentRangeType + "NextRange").val(selectedRangeDate);
			$("#" + currentRangeType + "LastRange").val('');
			$("#" + currentRangeType + "NextRange").attr("disabled", false);
			$("#" + currentRangeType + "LastRange").attr("disabled", true);
			if (selectedRangeDate != undefined) {
				result = dynamicRange(radioValue, currentRangeType,
						selectedRangeDate);
			}

		} else if (radioValue === 'lastRangeValue'
				&& $("input[name='" + currentRangeType + "']").is(':checked')) {
			selectedRangeDate = Range;
			$("#" + currentRangeType + "NextRange").val('');
			$("#" + currentRangeType + "LastRange").val(selectedRangeDate);
			$("#" + currentRangeType + "NextRange").attr("disabled", true);
			$("#" + currentRangeType + "LastRange").attr("disabled", false);
			if (selectedRangeDate != undefined) {
				result = dynamicRange(radioValue, currentRangeType,
						selectedRangeDate);
			}
		}
		$('#savedRange').find('span').html(calculatedRange);
		if (result != undefined) {
			calculatedRange = result;
		}
		if (calculatedRange != undefined) {
			calculatedDateRange = calculatedRange.split("To");
		}

		return calculatedDateRange;
	};
};
StringMatching();