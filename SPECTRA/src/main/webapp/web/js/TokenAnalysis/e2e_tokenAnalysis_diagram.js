/*function callBackEntitySearchResult(response){
	    		alert(response);
	    	};
var request = {
	    		"key": 2000,
	    		"facet": "source"
	    	};
	    	callAjaxService("tokenAnalysisVisualizationService", callBackEntitySearchResult,callBackFailure,request,"POST");*/

pageSetUp();
var tokenAnalysisVisualizationInstance = function() {
	var traceabilityValues = [ {
		"id" : "mastre",
		"text" : "Master"
	}, {
		"id" : "transaction",
		"text" : "Transaction"
	} ];
	$("#typeId").select2({
		data : traceabilityValues
	});
	tokenAnalysisVisualization = new tokenAnalysisVisualizationObject();
	if (TokenAnalysisSearchValues && TokenAnalysisSearchValues["key"]
			&& TokenAnalysisSearchValues["key"] != null) {
		$("#searchterm").val(TokenAnalysisSearchValues["key"]);
		tokenAnalysisVisualization.LoadMaterialGroup();
		TokenAnalysisSearchValues = undefined;
	}
	// tokenAnalysisVisualization.LoadMaterialGroup();
};

var tokenAnalysisVisualizationObject = function() {
	var THIS = this;
	this.materialGroupObject;
	THIS.groupBy = "sum";
	THIS.type = "2";
	var numberFormat = d3.format(".f");
	$("#searchForm").submit(function(e) {
		e.preventDefault();
	});
	$("#search").click(function() {
		tokenAnalysisVisualization.LoadMaterialGroup();
	});
	this.LoadMaterialGroup = function(source) {
		if (!$("#mainTokenDivId").hasClass("hide"))
			$("#mainTokenDivId").addClass("hide");
		
		if (!$("#getByChoice").hasClass("hide"))
			$("#getByChoice, #getByType, #getGroupBy").addClass("hide");
		enableLoading('loading');
		source = source || "SAP";

		THIS.viewtype  = $("#getByChoice").find("label.active").data("source") ||	 "facet";
		THIS.type = $("#getByType").find("label.active").data("source") ||"2";
		THIS.key = $("#searchterm").val();
		var request = {
			"key" : THIS.key,
			"facet" : THIS.viewtype,
			"type": THIS.type
		};
		callAjaxService("tokenAnalysisVisualizationService",
				callBackSuccessTokenAnalysisVisualization,
				callBackFailure, request, "POST");
	};
	this.loadViewType = function(viewtype) {
		if (!$("#mainTokenDivId").hasClass("hide"))
			$("#mainTokenDivId").removeClass("hide");
		
		enableLoading();
		var datasource = $("#myTab").find("li.active").data("source");
		datasource = datasource || "SAP";
		THIS.viewtype =  $("#getByChoice").find("label.active").data("source") || "facet";
		THIS.key = $("#searchterm").val();
		THIS.type = viewtype ||"2";
		var request = {
			"key" : THIS.key,
			"facet" : THIS.viewtype,
			"type": THIS.type
		};
		callAjaxService("tokenAnalysisVisualizationService",
				callBackSuccessTokenAnalysisVisualization,
				callBackFailure, request, "POST");
	};

	this.loadViewGroup = function(viewGroup) {
		if (!$("#mainTokenDivId").hasClass("hide"))
			$("#mainTokenDivId").removeClass("hide");
		
		enableLoading();
		var datasource = $("#myTab").find("li.active").data("source");
		datasource = datasource || "SAP";
		THIS.viewtype = viewGroup || "facet";
		THIS.key = $("#searchterm").val();
		THIS.type = $("#getByType").find("label.active").data("source") ||"2";
		var request = {
			"key" : THIS.key,
			"facet" : THIS.viewtype,
			"type": THIS.type
		};
		callAjaxService("tokenAnalysisVisualizationService",
				callBackSuccessTokenAnalysisVisualization,
				callBackFailure, request, "POST");
	};
	this.loadGroupBy = function(group){
		THIS.groupBy = group;
		constructMaterialGroupChart(90,100);
	};
	
	var callBackSuccessTokenAnalysisVisualization = function(sroot) {
		if (sroot && sroot.isException){
			showNotification("error",sroot.customMessage);
			return;
		}
		if (sroot && sroot != null && sroot["weight"] && sroot["weight"] != null && sroot["weight"]["facets"] && sroot["weight"]["facets"] != null){
			if (sroot["weight"]["facets"][THIS.viewtype])
				THIS.materialGroupObject = sroot["weight"]["facets"][THIS.viewtype];
			else 
				THIS.materialGroupObject = null;
		} else 
			THIS.materialGroupObject = null;
		constructMaterialGroupChart(90, 100);
	};
/*	var callBackSuccessTokenAnalysisVisualizationfacet = function(sroot) {
		console.log("1ok");
		THIS.materialGroupObject = sroot["weight"]["facets"]["facet"];
		constructMaterialGroupChart(90, 100);
	};*/
	var constructMaterialGroupChart = function(fromMaterialPercentage,
			toMaterialPercentage) {
		disableLoading('loading');
		if (THIS.materialGroupObject && THIS.materialGroupObject != null
				&& THIS.materialGroupObject.length > 0) {
			THIS.horzBarChart = dc.rowChart("#row-chart");
			$("#mainTokenDivId,#getByChoice,#getByType,#getGroupBy").removeClass("hide");
			$("#wid-id-12,#wid-id-22").css('display', '');
			THIS.materialGroupObject.sort(function(a, b){
			    return b.zz - a.zz;
			});
			var data = THIS.materialGroupObject;
			data.forEach(function(d) {
				d.sum = +d.sum;
				d.count = +d.count;
				d.zz = (THIS.groupBy == "sum") ? isNaN(d.sum) ? 0 : d.sum
						: isNaN(d.count) ? 0 : d.count;
			});
			data.sort(function(a, b){
			    return b.zz - a.zz;
			});
			$("#results").empty();
			var percentageMaterialCount = THIS.materialGroupObject.length / 100;
			var fromMaterialCount = (100 - fromMaterialPercentage)
					* percentageMaterialCount;
			var toMaterialCount = (100 - toMaterialPercentage)
					* percentageMaterialCount;
			var chartData = THIS.materialGroupObject/*
													 * .slice(Math
													 * .round(toMaterialCount),
													 * Math.round(fromMaterialCount))
													 */
			if (THIS.viewtype == "materialgroup")
				$("#chatyType").html(
						"Material Groups Filtered <span class='pull-right'>"
								+ chartData.length + "/"
								+ THIS.materialGroupObject.length + "</span>");
			else
				$("#chatyType").html(
						"Materials Filtered <span class='pull-right'>"
								+ chartData.length + "/"
								+ THIS.materialGroupObject.length + "</span>");
			$("#chartPercentage").css(
					{
						"width" : Math.round(toMaterialPercentage)
								- Math.round(fromMaterialPercentage) + "%"
					});
			$("#rev-toggles-1").removeClass("hide");

			var root = {
				"name" : "bubble",
				"children" : chartData
			};
			var diameter = 600, format = d3.format(",d"), color = d3.scale
					.category20c();

			var bubble = d3.layout.pack().sort(null).size(
					[ diameter, diameter ]).padding(1.5);
			$("#materialgroup svg").remove();
			var svg = d3.select("#materialgroup").append("svg").attr("width",
					diameter).attr("height", diameter).attr("class", "bubble")
					.attr('id', 'materialgroupsvg').attr('style','margin:auto;display:block');
			var node = svg.selectAll(".node").data(
					bubble.nodes(classes(root)).filter(function(d) {
						return !d.children;
					})).enter().append("g").attr("class", "node").attr(
					"transform", function(d) {
						return "translate(" + d.x + "," + d.y + ")";
					});

			node
					.append("title")
					.text(
							function(d) {
								if (THIS.viewtype == "facet") {
									return "Table Name: "
											+ d.className
											+ "\nDistinct Field: "
											+ format(d.distict)
											+ "\n"
											+ "Total Count: "
											+ numberFormat(Math
													.round(d.sum * 100) / 100);
								} else {
									return "Field Name: "
											+ d.className
											+ "\nDistinct Table: "
											+ format(d.distict)
											+ "\n"
											+ "Total Count: "
											+ numberFormat(Math
													.round(d.sum * 100) / 100);
								}
							});

			node.append("circle").attr("r", function(d) {
				return d.r;
			}).style("fill", function(d) {
				return color(d.className);
			});

			node.append("text").attr("dy", ".3em").style("text-anchor",
					"middle").text(function(d) {
				if (d.className)
					return d.className.substring(0, d.r / 3);
				else
					return "";
			});
			PanZoomsvg('materialgroupsvg');
		
			var ndx = crossfilter(data);
			var all = ndx.groupAll();
			var horzBarDimension = ndx.dimension(function(d) {
				return d.name || "Others";
			});
			var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
				return d.zz;
			});
			THIS.horzBarChart.width(180).height(610).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(horzBarGrp).dimension(horzBarDimension).ordinalColors(
					[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
					.label(function(d) {

						return d.key;// .split(".")[1];
					}).title(function(d) {
						return d.key + ': ' + d.value;
					}).elasticX(true).xAxis().ticks(4);

			dc.renderAll();
		} else {
			$("#row-chart svg").remove();
			$("#materialgroup svg").remove();
			$("#results")
					.html(
							'<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">�</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found for selected filters.</p></div>');
			$("#results").show();
		}
	};
	// Returns a flattened hierarchy containing all leaf nodes under the root.
	function classes(root) {
		var classes = [];

		function recurse(name, node) {
			if (node.children)
				node.children.forEach(function(child) {
					recurse(node.materialGroupName, child);
				});
			else
				classes.push({
					packageName : name,
					className : node.name,
					value : node.zz,
					sum: node.sum,
					distict : node.count,
					id : node.name
				});
		}

		recurse(null, root);
		return {
			children : classes
		};
	}
};
tokenAnalysisVisualizationInstance();