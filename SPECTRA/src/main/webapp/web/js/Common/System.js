var System = {};
var clientCacheTimeOut = 86400;  // In secs
System.CacheKeys = { "MethodNames": [], "Keys": [] };
System.CacheKeyIndexor = { "MethodNames": [], Keys: [] };
System.dbName = "e2e_mf";
System.dbAPACName = "e2emf_apac";
var userActions = [];
var userActionTabs = [];
var currentVersionForLocalStorage = 1.12;
var storeVersion =function(version){
	if (version && version !== null){
		System.version = version;
		currentVersionForLocalStorage = version;
	}
	if (System.IsLocalStorageAvailable) {
	    ValidateLocalStorageForVersion();
	}
};
var lookUpTable = new Hashtable();
var searchKey = "";
System.imgError = function(image) {
	$(image).remove();
    /*
	 * image.onerror = ""; image.src = "/img/noimage.gif";
	 */
    return true;
};
System.filtertag = function(buttonclick,text){
	if($("#ribbon").length && $("#ribbon").find('.breadcrumb').text().toLowerCase().includes('raw data ngram')){
		if(buttonclick==0){
			if($("#datatable_1_wrapper").length && $($("#datatable_1_wrapper").find('Table')[0]).length ){
				$($("#datatable_1_wrapper").find('Table')[0]).find('tr.columnSearch').each(function(){
					$(this).find('input').eq(1).val(text);	
					$($(this).find('input').eq(1)).trigger("keyup");			 
				});
			}
		}
		if(buttonclick==2){
			if($("#keyngramStopwords").length>0 && $("#firstBtnngramStopwords").length>0){
				$("#keyngramStopwords").val(text);
				$("#firstBtnngramStopwords").trigger('click');
			}
		}
		$(".tooltip-toolbar").hide();
	}
};
System.getFormattedDate = function(date) {
	  var year = date.getFullYear();
	  var month = (1 + date.getMonth()).toString();
	  month = month.length > 1 ? month : '0' + month;
	  var day = date.getDate().toString();
	  day = day.length > 1 ? day : '0' + day;
	  return month + '/' + day + '/' + year;
	};
	System.getFormattedDateMonthYear = function(date) {
		  var year = date.getFullYear()||"";
		  var month = date.getMonthNameShort()||"";
		  return month + ' ' + year;
		};
System.TrimObject = function (obj) {
    var type = $.type(obj);
    if (type !== "object") {
        return obj;
    }
    $.each(obj, function (i, value) {
        if ($.type(value) === "string") {
            obj[i] = value.trim();
        }
        else if ($.type(value) === "object") {
            obj[i] = System.TrimObject(obj[i]);
        }
    });
    return obj;
};
String.prototype.capitalize = function(){
    var sa = this.replace(/-/g,' ');
    var saa = sa.toLowerCase().trim();
    var sb = saa.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
   // var sc = sb.replace(/\s+/g, '-');
    return sb;
};
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
Date.prototype.getMonthName = function(lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].month_names[this.getMonth()];
};

Date.prototype.getMonthNameShort = function(lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].month_names_short[this.getMonth()];
};

Date.locale = {
    en: {
       month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
       month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    }
};
System.formatDate = function (input) {
	if (input){
	  var datePart = input.match(/\d+/g);
	  if (datePart && datePart.length > 2){
		  var year = datePart[0], // get only two digits
		  month = datePart[2], day = datePart[1];
	  return day+'/'+month+'/'+year;
	  } else {
		  return input;
	  }
	} else 
		return "No Data";
};
System.abbreviateNumber = function(number, decPlaces) {
	decPlaces = decPlaces || 0;
	if(!number){
		return 0;
	}
	var isNegativeNumber = false;
	if(number<0){
		isNegativeNumber = true;
		number = -number;
	}
	
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "k", "m", "b", "t" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number) {
             // Here, we multiply by decPlaces, round, and then divide by
				// decPlaces.
             // This gives us nice rounding to a particular decimal place.
             number = Math.round(number*decPlaces/size)/decPlaces;

             // Handle special case where we round up to the next
				// abbreviation
             if((number === 1000) && (i < abbrev.length - 1)) {
                 number = 1;
                 i++;
             }

             // Add the letter for the abbreviation
             number += abbrev[i];

             // We are done... stop
             break;
        }
    }
    return isNegativeNumber === true ? ("-"+number):number;
};
System.getDemisionValue = function(obj){
	console.log("--------->",obj);
	var offset_t = $(obj).offset().top - $(window).scrollTop() - 50;
    var offset_l = $(obj).offset().left - $(window).scrollLeft();
    var numberFormat = d3.format($(obj).data("numformat") || ",f");
    var prefix = $(obj).data("prefix");
    var suffix = $(obj).data("suffix");
    var showpercentage = $(obj).data("showpercentage");
    if (offset_l < 900)
    	offset_l += 115;
    else 
    	offset_l -= 340;
    $(".demisionTitle").html($(obj).html());
	var piechart = $(obj).next("a");
	if ($(piechart).length > 0){
		var piehref = $(piechart).attr('href');
		if (piehref && piehref !== null){
			var objsArray =	piehref.replace("javascript:","").split(".");
			if (objsArray.length > 1){
				var demisionData = eval(objsArray[0]+"."+objsArray[1]+".data()");
				 var totalValue = 0;
				 
				if (demisionData && demisionData.length > 0 && demisionData[0].value !== undefined && demisionData[0].value !== null){
					if($(obj).data("orderby") === "1"){
						demisionData.sort();						
					}else if($(obj).data("orderby") === "-1"){
						demisionData.sort(function(a, b){
							if (isNaN(a.value)){
								var batchOneValue = a.value.destroyedBatch/(a.value.destroyedBatch+a.value.nondestroyedBatch);
								var batchTwoValue = b.value.destroyedBatch/(b.value.destroyedBatch+b.value.nondestroyedBatch);
								return $(obj).hasClass("descending")===true?(batchOneValue-batchTwoValue):(batchTwoValue-batchOneValue);
							}else
								return $(obj).hasClass("descending")===true?(b.value - a.value):(a.value - b.value);
						});
					}else if($(obj).data("orderby") === "2" || $(obj).data("orderby") === "3"){
						if (isNaN(demisionData[0].key)){
							demisionData.sort(function(a, b){
								return (a.key.toLowerCase() < b.key.toLowerCase())?-1:(a.key.toLowerCase() > b.key.toLowerCase())?1:0;
							});
							if($(obj).data("orderby") === "3"){
								demisionData.reverse();
							}
						}else{
							demisionData.sort(function(a, b){
								return $(obj).data("orderby") === "2"?(a.key - b.key):(b.key - a.key);
							});
						}
					}
					
					var demisionFilters = eval(objsArray[0]+"."+objsArray[1]+".filters()");
					/* var type = eval(objsArray[0]+".type")|| "materialCount"; */
					var	type = ": ";
					var demisionlist = $("#demisionlist");
					demisionlist.empty();
					$("#demisionModalDiv").removeClass("hide").css({"top":offset_t+"px","left":offset_l+"px"});
					 $("#demisionModalDiv").find(".fa-refresh").bind('click', function(){System.demisionReset(objsArray[0]+"."+objsArray[1]);});
					
						$.each(demisionData, function () {
							totalValue += this.value;
						});
					for (var i=0; i<demisionData.length;i++){
						if (isNaN(demisionData[i].value)){
							var liValue =  $.trim(demisionData[i].key) +"<br><i class='txt-color-coral'>"+type + prefix +numberFormat(demisionData[i].value.destroyedBatch)+suffix+
							(showpercentage=== "yes"? "("+Math.round(demisionData[i].value.destroyedBatch/(demisionData[i].value.destroyedBatch+demisionData[i].value.nondestroyedBatch)*10000)/100+"%)":'') + 
									"<i/>/ <i class='txt-color-yellowgreen'>" + prefix +numberFormat(demisionData[i].value.nondestroyedBatch)+suffix +
							(showpercentage === "yes"?"("+Math.round(demisionData[i].value.nondestroyedBatch/(demisionData[i].value.destroyedBatch+demisionData[i].value.nondestroyedBatch)*10000)/100+"%)":'');
							var liValueTitle =  $.trim(demisionData[i].key)+"&#13;UnFavourable"+type + prefix + numberFormat(demisionData[i].value.destroyedBatch)+suffix+
							(showpercentage === "yes"? "("+Math.abs(Math.round(demisionData[i].value.destroyedBatch/(demisionData[i].value.destroyedBatch+demisionData[i].value.nondestroyedBatch)*10000)/100)+"%)":'')+
									"&#13;Favourable"+type + prefix +numberFormat(demisionData[i].value.nondestroyedBatch)+suffix+
							(showpercentage === "yes"? "("+Math.abs(Math.round(demisionData[i].value.nondestroyedBatch/(demisionData[i].value.destroyedBatch+demisionData[i].value.nondestroyedBatch)*10000)/100)+"%)":'');
						}else{							
							var liValue =  $.trim(demisionData[i].key) + type + prefix + numberFormat(demisionData[i].value)+suffix +
							(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value/totalValue*10000)/100+"%)":'');
							var liValueTitle = $.trim(demisionData[i].key) + type +prefix + numberFormat(demisionData[i].value)+suffix +
							(showpercentage === "yes"? " ("+ Math.round(demisionData[i].value/totalValue*10000)/100+"%)":'');
						}
						if (demisionData[i].value!==0){
							var liObj = $("<li/>").appendTo(demisionlist);
							var isFiltered = false;
							for (var j=0;j<demisionFilters.length;j++){
								if ( demisionData[i].key === demisionFilters[j]){
									isFiltered = true;
									break;
								}
							}
							if (isFiltered){
								liObj.addClass("filtered").append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked filtered")));
							} else {
								liObj.append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked")));
							}
							var excludeAnchor = $("<a/>").addClass("pull-right tooltip-toolbar-action").html("Exclude").append($("<i/>").addClass("fa fa-times txt-color-red")).bind('click',function(e){
								System.filterChart(chart.anchor(),$(this).next().text().split(":")[0],true);
								$("#demisionlist").find("li").each(function(){
									var text = $(this).find(".attributeItem").find("span").text();
									if(text){
										text = text.split(":");
										if(chart.filters().indexOf(text[0])>-1){
											$(this).addClass("filtered").find(".zd.checked").addClass("filtered");
										}else{
											$(this).removeClass("filtered").find(".zd.checked").removeClass("filtered");
										}
									}
								});
								e.preventDefault();
								e.stopPropagation();
							});
							liObj.append($("<div/>").addClass("attributeItem").append($("<div class='text' title='"+liValueTitle+"'/>").append(excludeAnchor).append($("<span/>").html(liValue))));
							liObj.bind('click', function(){System.demisionFilter(objsArray[0]+"."+objsArray[1],this);});
						}
					}
				} else{
					/*
					 * demisionData.sort(function(a, b){ return a.value -
					 * b.value; });
					 */
					var demisionFilters = eval(objsArray[0]+"."+objsArray[1]+".filters()");
					var type = eval(objsArray[0]+".type")|| "materialCount";
					if (type && (type==="netvalue" || type === "productioncost"))
						type = ": ";
					else
						type = ": ";
					var demisionlist = $("#demisionlist");
					demisionlist.empty();
					$("#demisionModalDiv").removeClass("hide").css({"top":offset_t+"px","left":offset_l+"px"});
					 $("#demisionModalDiv").find(".fa-refresh").bind('click', function(){System.demisionReset(objsArray[0]+"."+objsArray[1]);});
					if (demisionData.length>1){
						for (var i=0; i < demisionData[0].values.length;i++){
							var liValueTitle =  $.trim(demisionData[0].values[i].x)+":&#13;UnFavourable"+type + prefix+ numberFormat(demisionData[1].values[i].y)+suffix+
							(showpercentage === "yes"?"("+Math.abs(Math.round(demisionData[1].values[i].y/(demisionData[1].values[i].y+demisionData[0].values[i].y)*10000)/100)+"%)":'')+
									"&#13;Favourable"+type+prefix+numberFormat(demisionData[0].values[i].y)+suffix+
							(showpercentage === "yes"?"("+Math.abs(Math.round(demisionData[0].values[i].y/(demisionData[1].values[i].y+demisionData[0].values[i].y)*10000)/100)+"%)":'');
							var liValue =  $.trim(demisionData[0].values[i].x) + ":<br><i class='txt-color-coral'>"+type.substr(-1) + prefix+ numberFormat(demisionData[1].values[i].y)+suffix+
							(showpercentage === "yes"?"("+Math.abs(Math.round(demisionData[1].values[i].y/(demisionData[1].values[i].y+demisionData[0].values[i].y)*10000)/100)+"%)":'')+
									"<i/>/ <i class='txt-color-yellowgreen'>"+prefix+numberFormat(demisionData[0].values[i].y)+suffix+
							(showpercentage === "yes"?"("+Math.abs(Math.round(demisionData[0].values[i].y/(demisionData[1].values[i].y+demisionData[0].values[i].y)*10000)/100)+"%)":'');
							
							if (demisionData[0].values!==0){
								var liObj = $("<li/>").appendTo(demisionlist);
								var isFiltered = false;
								for (var j=0;j<demisionFilters.length;j++){
									if ( demisionData[0].values[i].x === demisionFilters[j]){
										isFiltered = true;
										break;
									}
								}
								if (isFiltered){
									liObj.addClass("filtered").append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked filtered")));
								} else {
									liObj.append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked")));
								}
								var excludeAnchor = $("<a/>").addClass("pull-right tooltip-toolbar-action").html("Exclude").append($("<i/>").addClass("fa fa-times txt-color-red")).bind('click',function(e){
									System.filterChart(chart.anchor(),$(this).next().text().split(":")[0],true);
									$("#demisionlist").find("li").each(function(){
										var text = $(this).find(".attributeItem").find("span").text();
										if(text){
											text = text.split(":");
											if(chart.filters().indexOf(text[0])>-1){
												$(this).addClass("filtered").find(".zd.checked").addClass("filtered");
											}else{
												$(this).removeClass("filtered").find(".zd.checked").removeClass("filtered");
											}
										}
									});
									e.preventDefault();
									e.stopPropagation();
								});
								liObj.append($("<div/>").addClass("attributeItem").append($("<div class='text' title='"+liValueTitle+"'/>").append(excludeAnchor).append($("<span/>").html(liValue))));
								liObj.bind('click', function(){System.demisionFilter(objsArray[0]+"."+objsArray[1],this);});
							}
						}
					} else if (demisionData.length>0){
						$.each(demisionData[0].values, function () {
							totalValue += this.y;
						});
						for (var i=0; i < demisionData[0].values.length;i++){
							var liValue =  $.trim(demisionData[0].values[i].x) + type +prefix+ numberFormat(demisionData[0].values[i].y)+suffix +
							(showpercentage === "yes"?" ("+ Math.round(demisionData[0].values[i].y/totalValue*10000)/100+"%)":'');
							var liValueTitle = $.trim(demisionData[0].values[i].x) + type +prefix+ numberFormat(demisionData[0].values[i].y)+suffix +
							(showpercentage === "yes"?" ("+ Math.round(demisionData[0].values[i].y/totalValue*10000)/100+"%)":'');
							if (demisionData[0].values!==0){
								var liObj = $("<li/>").appendTo(demisionlist);
								var isFiltered = false;
								for (var j=0;j<demisionFilters.length;j++){
									if ( demisionData[0].values[i].x === demisionFilters[j]){
										isFiltered = true;
										break;
									}
								}
								if (isFiltered){
									liObj.addClass("filtered").append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked filtered")));
								} else {
									liObj.append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked")));
								}
								var excludeAnchor = $("<a/>").addClass("pull-right tooltip-toolbar-action").html("Exclude").append($("<i/>").addClass("fa fa-times txt-color-red")).bind('click',function(e){
									System.filterChart(chart.anchor(),$(this).next().text().split(":")[0],true);
									$("#demisionlist").find("li").each(function(){
										var text = $(this).find(".attributeItem").find("span").text();
										if(text){
											text = text.split(":");
											if(chart.filters().indexOf(text[0])>-1){
												$(this).addClass("filtered").find(".zd.checked").addClass("filtered");
											}else{
												$(this).removeClass("filtered").find(".zd.checked").removeClass("filtered");
											}
										}
									});
									e.preventDefault();
									e.stopPropagation();
								});
								liObj.append($("<div/>").addClass("attributeItem").append($("<div class='text' title='"+liValueTitle+"'/>").append(excludeAnchor).append($("<span/>").html(liValue))));
								liObj.bind('click', function(){System.demisionFilter(objsArray[0]+"."+objsArray[1],this);});
							}
						}
					}
				}
			}
		} else{
			var chart = null;
			var anchorId = $(obj).parent().attr("id");
			var chartAggregate  = $(obj).parent().data("aggregate");
			var chartFieldName  = $(obj).parent().data("fieldname");
			var calculatedformula = $(obj).data("calculatedformula");
			$.each(dc.chartRegistry.list(),function(index,chartInstance) { 
    			if (chartInstance.anchorName() === anchorId){
    				chart = chartInstance;
    				return false;
    			}else{
    				var scroll = chartInstance.anchorName().split('container');
    				if(scroll[0] === anchorId){
    					chart = chartInstance;
        				return false;	
    				}
    			}
			});
			var demisionData  = chart.data();
			var valuekey = [];
			var valueKeysColl;
			try{
				valueKeysColl = demisionData[0].values[0].data.value;
			}catch(e){
				valueKeysColl = demisionData[0].value;
			}
			for ( var key in valueKeysColl) {
				if (key !== "count") {
					valuekey.push(key);
				}
			}
			
			var calculateValueTotal = function(value){
				var rtnVal = 0;
				if(valuekey && valuekey !== undefined && valuekey.length>0){
					$.each(valuekey, function( index, itme ) {
						rtnVal += value[itme];
						});
				}
				return rtnVal;
			};
			var calculateValueByFormula = function(d){
				return eval(calculatedformula)=== Infinity || isNaN(eval(calculatedformula))? 0:eval(calculatedformula) ;
			};
			var demisionFilters  = chart.filters();
			 var totalValue = 0;
			 var type = ": ";
			 var demisionlist = $("#demisionlist");
				demisionlist.empty();
			$("#demisionModalDiv").removeClass("hide").css({"top":offset_t+"px","left":offset_l+"px"});
			$("#demisionModalDiv").find(".fa-refresh").bind('click', function(){System.demisionReset(chart,true);});
			if (demisionData && demisionData.length > 0 && demisionData[0].value !== undefined && demisionData[0].value !== null){
				if($(obj).data("orderby") === "1"){
					demisionData.sort(function(a, b){
						if(chartAggregate === "calculated"){
							return calculateValueByFormula(a) - calculateValueByFormula(b);
						}else{
							return calculateValueTotal(a.value)-calculateValueTotal(b.value);
						}		
					});
				}else if($(obj).data("orderby") === "-1"){
					demisionData.sort(function(a, b){
						if(chartAggregate === "calculated"){
							return isNaN(calculateValueByFormula(b) - calculateValueByFormula(a))? 0 :calculateValueByFormula(b) - calculateValueByFormula(a);
						}else{
							return calculateValueTotal(b.value)-calculateValueTotal(a.value);
						}
					});
				}else if($(obj).data("orderby") === "2" || $(obj).data("orderby") === "3"){
					if (isNaN(demisionData[0].key)){
						demisionData.sort(function(a, b){
							return (a.key.toLowerCase() < b.key.toLowerCase())?-1:(a.key.toLowerCase() > b.key.toLowerCase())?1:0;
						});
						if($(obj).data("orderby") === "3"){
							demisionData.reverse();
						}
					}else{
						demisionData.sort(function(a, b){
							return $(obj).data("orderby") === "2"?(a.key - b.key):(b.key - a.key);
						});
					}
				}			
					$.each(demisionData, function () {
						if (this.value.count > 0){
							if (chartAggregate ==="count")
								totalValue += this.value.count;
							else if (chartAggregate ==="avg")
								totalValue += calculateValueTotal(this.value)/this.value.count;
							else if(chartAggregate ==="calculated"){
								totalValue+= calculateValueByFormula(this);
							}
							else
								totalValue +=calculateValueTotal(this.value);
						}
					});
				for (var i=0; i<demisionData.length;i++){
					var key = demisionData[i].key;
					try{
						if (dataAnalyser && dataAnalyser.masterData && dataAnalyser.masterData[chartFieldName] && dataAnalyser.masterData[chartFieldName][key] && dataAnalyser.masterData[chartFieldName][key]!== ""){
							key += "-"+ dataAnalyser.masterData[chartFieldName][key];
						}
					}catch(ex){
						console.log(ex);
					}
					if (isNaN(demisionData[i].value)){
						// if (demisionData[i].value.count > 0 &&
						// demisionData[i].value[valuekey] &&
						// demisionData[i].value[valuekey] > 0){
						var liValue,liValueTitle;
						if (demisionData[i].value.count > 0){// &&calculateValueTotal(demisionData[i].value)
							if (chartAggregate ==="count"){
								var liValue =  $.trim(key) + type + prefix + numberFormat(demisionData[i].value.count) +suffix+ 
								(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value.count/totalValue*10000)/100+"%)":'');
								var liValueTitle = $.trim(key) + type +prefix + numberFormat(demisionData[i].value.count)+suffix +
								(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value.count/totalValue*10000)/100+"%)":'');
							}else if (chartAggregate ==="avg" && demisionData[i].value.count > 0){
								var liValue =  $.trim(key) + type + prefix + numberFormat(calculateValueTotal(demisionData[i].value)/demisionData[i].value.count) +suffix +
								(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value[valuekey]/(demisionData[i].value.count*totalValue)*10000)/100+"%)":'');
								var liValueTitle = $.trim(key) + type + prefix + numberFormat(calculateValueTotal(demisionData[i].value)/demisionData[i].value.count) +suffix +
								(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value[valuekey]/(demisionData[i].value.count*totalValue)*10000)/100+"%)":'');
							}else if (chartAggregate ==="calculated"){
								var liValue =  $.trim(key) + type + prefix + numberFormat(calculateValueByFormula(demisionData[i])) +suffix +
								(showpercentage === "yes"?" ("+ Math.round(calculateValueByFormula(demisionData[i])/(demisionData[i].value.count*totalValue)*10000)/100+"%)":'');
								var liValueTitle = $.trim(key) + type + prefix + numberFormat(calculateValueByFormula(demisionData[i])) +suffix +
								(showpercentage === "yes"?" ("+ Math.round(calculateValueByFormula(demisionData[i])/(demisionData[i].value.count*totalValue)*10000)/100+"%)":'');
							}
							else{
								var liValue =  $.trim(key) + type + prefix + numberFormat(calculateValueTotal(demisionData[i].value))+suffix +
								(showpercentage === "yes"?" ("+ Math.round(calculateValueTotal(demisionData[i].value)/totalValue*10000)/100+"%)":'');
								var liValueTitle = $.trim(key) + type + prefix + numberFormat(calculateValueTotal(demisionData[i].value))+suffix +
								(showpercentage === "yes"?" ("+ Math.round(calculateValueTotal(demisionData[i].value)/totalValue*10000)/100+"%)":'');
							}
						}else{
							demisionData[i].value[valuekey] = 0;
						}
					}else{					
						var liValue =  $.trim(key) + type + prefix + numberFormat(demisionData[i].value)+suffix +
						(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value/totalValue*10000)/100+"%)":'');
						var liValueTitle = $.trim(key) + type + prefix + numberFormat(demisionData[i].value) + suffix +
						(showpercentage === "yes"?" ("+ Math.round(demisionData[i].value/totalValue*10000)/100+"%)":'');
					}
					if (calculateValueTotal(demisionData[i].value)!==0){
						var liObj = $("<li/>").appendTo(demisionlist).data("key",demisionData[i].key);
						var isFiltered = false;
						for (var j=0;j<demisionFilters.length;j++){
							if ( demisionData[i].key === demisionFilters[j]){
								isFiltered = true;
								break;
							}
						}
						if (isFiltered){
							liObj.addClass("filtered").append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked filtered")));
						} else {
							liObj.append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked")));
						}
						var excludeAnchor = $("<a/>").addClass("pull-right tooltip-toolbar-action").html("Exclude").append($("<i/>").addClass("fa fa-times txt-color-red")).bind('click',function(e){
							System.filterChart(chart.anchor(),$(this).next().text().split(":")[0],true);
							$("#demisionlist").find("li").each(function(){
								var text = $(this).find(".attributeItem").find("span").text();
								if(text){
									text = text.split(":");
									if(chart.filters().indexOf(text[0])>-1){
										$(this).addClass("filtered").find(".zd.checked").addClass("filtered");
									}else{
										$(this).removeClass("filtered").find(".zd.checked").removeClass("filtered");
									}
								}
							});
							e.preventDefault();
							e.stopPropagation();
						});
						liObj.append($("<div/>").addClass("attributeItem").append($("<div class='text' title='"+liValueTitle+"'/>").append(excludeAnchor).append($("<span/>").html(liValue))));
						liObj.bind('click', function(){System.demisionFilter(chart,this,true);});
					}
				}
			} else if (demisionData.length>0){
				
				$.each(demisionData[0].values, function (index,value) {
					// totalValue += this.y;
					totalValue += calculateValueTotal(value.data.value);
				});
				if($(obj).data("orderby") === "1"){					
					demisionData[0].values.sort(function(a, b){
						if(chartAggregate === "calculated"){
							return calculateValueByFormula(a.data) - calculateValueByFormula(b.data);
						}else{
							return calculateValueTotal(a.data.value)-calculateValueTotal(b.data.value);
						}
					});				
				}else if($(obj).data("orderby") === "-1"){
					demisionData[0].values.sort(function(a, b){
						if(chartAggregate === "calculated"){
							return calculateValueByFormula(b.data) - calculateValueByFormula(a.data);
						}else{
							return calculateValueTotal(b.data.value)-calculateValueTotal(a.data.value);
						}
					});
				}else if($(obj).data("orderby") === "2" || $(obj).data("orderby") === "3"){
					if (isNaN(demisionData[0].values[0].data.key)){
						demisionData[0].values.sort(function(a, b){
							return (a.data.key.toLowerCase() < b.data.key.toLowerCase())?-1:(a.data.key.toLowerCase() > b.data.key.toLowerCase())?1:0;
						});
						if($(obj).data("orderby") === "3"){
							demisionData[0].values.reverse();
						}
					}else{
						demisionData[0].values.sort(function(a, b){
							return $(obj).data("orderby") === "2"?(a.data.key - b.data.key):(b.data.key - a.data.key);
						});
					}
				}
				for (var i=0; i < demisionData[0].values.length;i++){
					var value;
					if(chartAggregate === "calculated"){
						value = calculateValueByFormula(demisionData[0].values[i].data);
					}else{
						value = calculateValueTotal(demisionData[0].values[i].data.value);
					}
					var checkValue = "1";
					if($(obj).data("zerovalues") ==='no'){
						checkValue = numberFormat(value);
					}
					if(checkValue != "0" && checkValue !="-0"){
					var liValue =  $.trim(demisionData[0].values[i].x) + type +prefix + numberFormat(value) +suffix+ 
					(showpercentage === "yes"?" ("+ Math.round((value)/totalValue*10000)/100+"%)":'');
					var liValueTitle = $.trim(demisionData[0].values[i].x) + type + prefix +numberFormat(value) +suffix +
					(showpercentage === "yes"?" ("+ Math.round(value/totalValue*10000)/100+"%)":'');
					if (demisionData[0].values!==0){
						var liObj = $("<li/>").appendTo(demisionlist).data("key",demisionData[0].values[i].x);
						var isFiltered = false;
						for (var j=0;j<demisionFilters.length;j++){
							if ( demisionData[0].values[i].x === demisionFilters[j]){
								isFiltered = true;
								break;
							}
						}
						if (isFiltered){
							liObj.addClass("filtered").append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked filtered")));
						} else {
							liObj.append($("<span/>").addClass("zd icon checkbox radio").append($("<span/>").addClass("zd checked")));
						}
						var excludeAnchor = $("<a/>").addClass("pull-right tooltip-toolbar-action").html("Exclude").append($("<i/>").addClass("fa fa-times txt-color-red")).bind('click',function(e){
							System.filterChart(chart.anchor(),$(this).next().text().split(":")[0],true);
							$("#demisionlist").find("li").each(function(){
								var text = $(this).find(".attributeItem").find("span").text();
								if(text){
									text = text.split(":");
									if(chart.filters().indexOf(text[0])>-1){
										$(this).addClass("filtered").find(".zd.checked").addClass("filtered");
									}else{
										$(this).removeClass("filtered").find(".zd.checked").removeClass("filtered");
									}
								}
							});
							e.preventDefault();
							e.stopPropagation();
						});
						liObj.append($("<div/>").addClass("attributeItem").append($("<div class='text' title='"+liValueTitle+"'/>").append(excludeAnchor).append($("<span/>").html(liValue))));
						liObj.bind('click', function(){System.demisionFilter(chart,this,true);});
					}
				}
				}
			}
		}
	}
	 $("#demisionlist").prepend($("<li/>").append($("<div/>").addClass("input-group margin-2")
	    		.append($("<span/>").addClass("input-group-addon")
	    				.append($("<i/>").addClass("fa fa-filter")))
	    		.append($("<input/>").addClass("form-control").keyup(function () {
	    	        var rex = new RegExp($(this).val().trim(), 'i');
	    	        $('#demisionlist li').hide().filter(function () {
	    	        	 return rex.test($(this).text()) || $(this).find("input").length > 0;
	    	        }).show();
	    	    }))));
}; 

System.demisionFilter = function(classInstance, obj,dynamic){
	dynamic = dynamic || false;
	var demisionValue = $(obj).text().split(":");
	if (demisionValue.length>0 && !dynamic){
		eval(classInstance+".filter('"+demisionValue[0]+"')");
		eval(classInstance+".redrawGroup()");
	}else{
		demisionValue =  $(obj).data("key");
		classInstance.filter(demisionValue);
		classInstance.redrawGroup();
	}
	 /* $($(obj).parent('ul').children('li').removeClass('filtered').find(".zd.checked")).removeClass("filtered"); */
	if ($(obj).hasClass("filtered"))
		$($(obj).removeClass("filtered").find(".zd.checked")).removeClass("filtered");
	else
	 $($(obj).addClass("filtered").find(".zd.checked")).addClass("filtered");
};

System.demisionReset = function(classInstance,dynamic){
	dynamic = dynamic || false;
	if (!dynamic)
		eval(classInstance+".filterAll();dc.redrawAll();");
	else{
		classInstance.filterAll();dc.redrawAll();
	}
	$($("#demisionlist").children('li').removeClass('filtered').find(".zd.checked")).removeClass("filtered");
};

System.IsLocalStorageAvailable = false;

try {
    if (localStorage) {
        System.IsLocalStorageAvailable = true;
    }
    else {
        System.IsLocalStorageAvailable = false;
        clientCacheTimeOut = 720;
    }
}
catch (ex) {
    System.IsLocalStorageAvailable = false;
    clientCacheTimeOut = 720;
}

System.CachingService = new Services();

function Services() {
    this.GetAllService = new GetAllService();
    this.ErrorHandling = true;
    // if (applicationMode === "Debug") {
    // this.ErrorHandling = false;
    // }

    this.CallService = function (serviceCallMethodName, request, successHandler, errorHandler,serviceType, dataType ) {
        var returnDataType = "json";
        if (dataType) {
            returnDataType = dataType;
        }
        var url = serviceCallMethodNames[serviceCallMethodName].url;
        var virtaulDir = "";// getVirtualPathField();
        if (virtaulDir === null) {
            virtaulDir = "";
        }
        if (virtaulDir.length > 0) {
            while (url.indexOf("..") !== -1) {
                url = url.substring(url.indexOf("..") + 2, url.length);
            }
            if (url.indexOf(virtaulDir, 0) !== 0) {
                url = virtaulDir + url;
            }
        }
        url = url.replace(".html",".html?v="+currentVersionForLocalStorage);
        var ajaxValue =
        {
            "type": serviceType,
            "url": url,
           // "contentType": "application/x-www-form-urlencoded;
			// charset=UTF-8",
            "dataType": returnDataType,
            "success": successHandler,
            "error": errorHandler,
            "complete": function (XMLHttpRequest, textStatus) {
                if (textStatus === "error" && textStatus === "parseerror") { this; }
            }
        };
        if (serviceType === "POST"){
        	ajaxValue.data = request;
        } 
        if ($.ajax) {
            $.ajax(ajaxValue);
        }
        else {
            jQuery.ajax(ajaxValue);
        }
    };

    this.ClearCache = function (methodName) {
        if (methodName === "GetCatalogValues") {
            this.CatalogService.CatalogMastersWithCatalogValues = [];
            this.CatalogService.CurrentCatalogs = [];
            if (System.IsLocalStorageAvailable && localStorage) {
                var index = System.CacheKeyIndexor.MethodNames.find(methodName);
                if (index && index.length > 0) {
                    var keys = System.CacheKeyIndexor.Keys[index[0]];
                    if (keys) {
                        for (var i = 0; i < System.CacheKeyIndexor.Keys[index[0]].length; i++) {
                            localStorage.removeItem(System.CacheKeyIndexor.Keys[index[0]][i]);
                            System.CacheKeyIndexor.Keys[index[0]].splice(i, 1);
                            i--;
                        }
                    }
                }
            }
        }
        else if (this.GetAllService.GetAlls) {
            for (var i = 0; i < this.GetAllService.GetAlls.length; i++) {
                if (this.GetAllService.GetAllsIndexer[i].ServiceCallMethodName && this.GetAllService.GetAllsIndexer[i].ServiceCallMethodName === methodName) {
                    this.GetAllService.GetAllsIndexer.splice(i, 1);
                    this.GetAllService.GetAlls.splice(i, 1);
                    i--;
                }
            }
            if (System.IsLocalStorageAvailable && localStorage) {
                var index = System.CacheKeyIndexor.MethodNames.find(methodName);
                if (index && index.length > 0) {
                    var keys = System.CacheKeyIndexor.Keys[index[0]];
                    if (keys) {
                        for (var i = 0; i < System.CacheKeyIndexor.Keys[index[0]].length; i++) {
                            localStorage.removeItem(System.CacheKeyIndexor.Keys[index[0]][i]);
                            System.CacheKeyIndexor.Keys[index[0]].splice(i, 1);
                            i--;
                        }
                    }
                }
            }
        }
    };
}

function GetAllService() {
    this.GetAlls = null;
    this.GetAllsIndexer = null;
    this.SuccessHandler = null;
    this.ErrorHandler = null;
    this.CurrentRequest = null;
    this.CurrentMethodName = null;
    this.ExecutionAlreadyInProgress = false;

    /*
	 * Used for maintaining the queue
	 */
    var SuccessHandlerQueue = [];
    var ErrorHandlerQueue = [];
    var GetAllsRequestQueue = [];
    var GetAllsMethodNameQueue = [];
    var serviceCallType = "POST";
    var serviceCallReturnType = "json";
    function csvJSON(csv){
   	 
  	  var lines=csv.split("\n");
  	 
  	  var result = [];
  	 
  	  var headers=lines[0].split(",");
  	 
  	  for(var i=1;i<lines.length;i++){
  	 
  		  var obj = {};
  		  var currentline=lines[i].split(",");
  	 
  		  for(var j=0;j<headers.length;j++){
  			  obj[headers[j]] = currentline[j];
  		  }
  	 
  		  result.push(obj);
  	 
  	  }
  	  
  	  // return result; //JavaScript object
  	  return JSON.stringify(result); // JSON
  	}
    
    function callAllsServiceSuccess(response, request) {
        if (response && (response.IsException === true || response.IsFailure === true)) {
            if (System.CachingService.ErrorHandling) {
                try { System.CachingService.GetAllService.SuccessHandler(response); } catch (ex) { }
            }
            else {
                System.CachingService.GetAllService.SuccessHandler(response);
            }

            GetAllsRequestQueue.splice(0, 1);
            GetAllsMethodNameQueue.splice(0, 1);
            SuccessHandlerQueue.splice(0, 1);
            ErrorHandlerQueue.splice(0, 1);
            System.CachingService.GetAllService.ExecutionAlreadyInProgress = false;
        }
        else {
            var valueIsPresent = false;
            if (request.__BypassClientAndServerCache) {
                valueIsPresent = contains(request, System.CachingService.GetAllService.GetAllsIndexer, true);
            }
            if (response && request.__BypassClientAndServerCache && !valueIsPresent) {
                System.CachingService.GetAllService.GetAlls.push(response);
                System.CachingService.GetAllService.GetAllsIndexer.push(request);
                if (serviceCallReturnType === "html"){
                    SetLocalStorage(request.ClientCacheKey, response);
                } else {
                    SetLocalStorage(request.ClientCacheKey, JSON.stringify(response));
                }
            }
            else if (response && !request.__BypassClientAndServerCache) {
                System.CachingService.GetAllService.GetAlls.push(response);
                System.CachingService.GetAllService.GetAllsIndexer.push(request);
                if (serviceCallReturnType === "html"){
                    SetLocalStorage(request.ClientCacheKey, response);
                } else {
                    SetLocalStorage(request.ClientCacheKey, JSON.stringify(response));
                }
                // localStorage.setItem(request.ClientCacheKey,
				// JSON.stringify(response));
            }
            returnGetAlls();
        }
    }

    function callGetAllsServiceError(response) {
        if (System.CachingService.GetAllService.ErrorHandler) {
            if (System.CachingService.ErrorHandling) {
                try { System.CachingService.GetAllService.ErrorHandler(response); } catch (ex) { }
            }
            else {
                System.CachingService.GetAllService.ErrorHandler(response);
            }
        }
        GetAllsRequestQueue.splice(0, 1);
        GetAllsMethodNameQueue.splice(0, 1);
        SuccessHandlerQueue.splice(0, 1);
        ErrorHandlerQueue.splice(0, 1);
        System.CachingService.GetAllService.ExecutionAlreadyInProgress = false;
    }

    this.GetAll = function (request, methodName, successHandler, errorHandler,serviceType,returnDataType) {
        // request.ServiceCallMethodName = methodName;
        GetAllsRequestQueue.push(request);
        GetAllsMethodNameQueue.push(methodName);
        SuccessHandlerQueue.push(successHandler);
        ErrorHandlerQueue.push(errorHandler);
        if (System.CachingService.GetAllService.GetAlls === null) {
            System.CachingService.GetAllService.GetAlls = [];
            System.CachingService.GetAllService.GetAllsIndexer = [];
        }
        serviceCallType = serviceType;
        serviceCallReturnType = returnDataType;
        schedule();
        
    };

    function dispatch() {
        System.CachingService.GetAllService.ExecutionAlreadyInProgress = true;
        var request = GetAllsRequestQueue[0];
        System.CachingService.GetAllService.CurrentMethodName = GetAllsMethodNameQueue[0];
        System.CachingService.GetAllService.CurrentRequest = request;
        System.CachingService.GetAllService.SuccessHandler = SuccessHandlerQueue[0];
        System.CachingService.GetAllService.ErrorHandler = ErrorHandlerQueue[0];
        var serviceCallRequired = true;

        if (request) {
            if (contains(request, System.CachingService.GetAllService.GetAllsIndexer)) {
                serviceCallRequired = false;
            }
        }
        if (serviceCallRequired) {
            System.CachingService.CallService(System.CachingService.GetAllService.CurrentMethodName, request, function (response) { callAllsServiceSuccess(response, request); }, callGetAllsServiceError,serviceCallType,serviceCallReturnType);
        }
        else {
            setTimeout(returnGetAlls, 0);
        }
    }

    function schedule() {
        if (GetAllsRequestQueue.length > 0 && !System.CachingService.GetAllService.ExecutionAlreadyInProgress) {
            dispatch();
        }
        else if (GetAllsRequestQueue.length > 0) {
            wait();
        }
    }

    function wait() {
        if (!System.CachingService.GetAllService.ExecutionAlreadyInProgress) {
            schedule();
        }
        if (GetAllsRequestQueue.length > 0) {
            setTimeout(wait, 100);
        }
    }

    function returnGetAlls() {
        var itemFound = false;
        for (var i = 0; i < System.CachingService.GetAllService.GetAlls.length; i++) {
            if (System.CachingService.GetAllService.CurrentRequest.ClientCacheKey === System.CachingService.GetAllService.GetAllsIndexer[i].ClientCacheKey) {
                if (System.CachingService.GetAllService.SuccessHandler) {
                     if (System.CachingService.ErrorHandling) {
                        if (serviceCallReturnType !== "html"){
                            var cloneObj = System.CloneObject(System.CachingService.GetAllService.GetAlls[i]);
                        } else {
                            var cloneObj = System.CachingService.GetAllService.GetAlls[i];
                        }
                        try { System.CachingService.GetAllService.SuccessHandler(cloneObj); itemFound = true; } catch (ex) { }
                    }
                    else {
                        if (serviceCallReturnType !== "html"){
                            var cloneObj = System.CloneObject(System.CachingService.GetAllService.GetAlls[i]);
                        } else {
                            var cloneObj = System.CachingService.GetAllService.GetAlls[i];
                        }
                        System.CachingService.GetAllService.SuccessHandler(cloneObj);
                        itemFound = true;
                    }
                }
                break;
            }
        }

        if (!itemFound && System.IsLocalStorageAvailable) {
            var item = localStorage.getItem(System.CachingService.GetAllService.CurrentRequest.ClientCacheKey);
            if (serviceCallReturnType === "html"){
                     System.CachingService.GetAllService.SuccessHandler(item);
                } else {
                     System.CachingService.GetAllService.SuccessHandler(JSON.parse(item));
                }
           
        }

        GetAllsRequestQueue.splice(0, 1);
        GetAllsMethodNameQueue.splice(0, 1);
        SuccessHandlerQueue.splice(0, 1);
        ErrorHandlerQueue.splice(0, 1);
        System.CachingService.GetAllService.ExecutionAlreadyInProgress = false;
    }

    function contains(request, requests, overrideDefaultCheck) {
        var index = -1;
        if (requests) {
            for (var i = 0; i < requests.length; i++) {
                if (requests[i].ClientCacheKey === request.ClientCacheKey) {
                    index = i;
                    break;
                }
            }
        }
        if (index > -1 && !overrideDefaultCheck && request.__BypassClientAndServerCache) {
            removeCache(index);
            return false;
        }
        var present = false;
        if (requests && index > -1) {
            var timeOut = hasTimedOut(index);
            if (timeOut) {
                present = false;
                removeCache(index);
            }
            else {
                present = true;
            }
        }
        if (System.IsLocalStorageAvailable && localStorage && present === false) {
            if (!overrideDefaultCheck && request.__BypassClientAndServerCache) {
                return false;
            }
            if (localStorage.getItem(request.ClientCacheKey)) {
                var timeOut = hasTimedOut(-1, request.ClientCacheKey);
                if (timeOut) {
                    present = false;
                    removeCache(-1);
                }
                else {
                    present = true;
                }
            }
        }
        return present;
    }

    function hasTimedOut(index, clientCacheKey) {
        if (index !== -1) {
            if (!System.CachingService.GetAllService.GetAllsIndexer[index].__CallTimeIn) {
                System.CachingService.GetAllService.GetAllsIndexer[index].__CallTimeIn = new Date();
                return false;
            }
        }
        var item = null;
        var dateTimeCall = null;
        if (index === -1 && clientCacheKey) {
            if (System.IsLocalStorageAvailable && localStorage) {
                item = localStorage.getItem("__CallTimeIn__" + clientCacheKey);
                if (!item) {
                    return false;
                }
                else {
                    dateTimeCall = (new Date(item)).getTime();
                }
            }
            else {
                return false;
            }
        }
        else {
            dateTimeCall = System.CachingService.GetAllService.GetAllsIndexer[index].__CallTimeIn.getTime();
        }
        var dateTimeNow = new Date();
        var difference = dateTimeNow.getTime() - dateTimeCall;
        difference = difference / 1000;
        var cacheTimeOut;
        if (index !== -1) {
            cacheTimeOut = System.CachingService.GetAllService.GetAllsIndexer[index].__CacheTimeOut;
        }
        else {
            cacheTimeOut = clientCacheTimeOut;
        }
        if (cacheTimeOut) {
            if (cacheTimeOut >= 0 && difference > cacheTimeOut) {
                return true;
            }
        }
        else if (clientCacheTimeOut >= 0 && difference > clientCacheTimeOut) {
            return true;
        }
        return false;
    }

    function removeCache(index) {
        if (index !== -1) {
            System.CachingService.GetAllService.GetAlls.splice(index, 1);
            System.CachingService.GetAllService.GetAllsIndexer.splice(index, 1);
        }
        if (System.IsLocalStorageAvailable && localStorage) {
            localStorage.removeItem(System.CachingService.GetAllService.CurrentRequest.ClientCacheKey);
        }
    }
}

function modifyCacheKey(requestObj, count, isSpecificToUser) {
    if ($.type(requestObj) === "string") {
        requestObj = JSON.parse(requestObj);
    }
    $.each(requestObj, function (i, value) {
        switch ($.type(value)) {
            case "object":
                modifyCacheKey(value, count, isSpecificToUser);
                break;
            case "string":
                if (i === "UserId" && count < 2 && !isSpecificToUser) {
                    requestObj[i] = null;
                    count++;
                }
                break;
            case "boolean":
                if (i === "IsRefresh" && count < 2) {
                    requestObj[i] = false;
                    count++;
                }
                break;
        }
        if (count === 2) {
            return JSON.stringify(requestObj);
        }
    });
    return JSON.stringify(requestObj);
}

function isRequestForRefresh(requestObj, count, isRefresh) {
    if ($.type(requestObj) === "string") {
        requestObj = JSON.parse(requestObj);
    }
    $.each(requestObj, function (i, value) {
        switch ($.type(value)) {
            case "object":
                if (requestObj && (requestObj.IsRefresh === false || requestObj.IsRefresh === true)) {
                    isRefresh = requestObj.IsRefresh;
                }
                else {
                    isRefresh = isRequestForRefresh(value, count, isRefresh);
                }
                return isRefresh;
                break;
            case "boolean":
                if (i === "IsRefresh" && count < 3) {
                    isRefresh = requestObj[i];
                    return isRefresh;
                    count++;
                }
                break;
        }
        if (count === 3) {
            return isRefresh;
        }
    });
    return isRefresh;
}

function clearOrUpdateCache(methodNames) {
    if (methodNames) {
        for (var i = 0; i < methodNames.length; i++) {
            if (methodNames[i] === "GetCatalogValues") {
                System.CachingService.ClearCache(methodNames[i]);
            }
            else if (serviceCallMethodNames[methodNames[i]] && serviceCallMethodNames[methodNames[i]].EnableCaching) {
                System.CachingService.ClearCache(methodNames[i]);
            }
        }
    }
}

var SetLocalStorage = function (key, value) {
    if (System.IsLocalStorageAvailable && localStorage) {
        try {
            localStorage.setItem(key, value);
            localStorage.setItem("__CallTimeIn__" + key, Date());
        }
        catch (ex) {
            if (ex.code === 22) {
                if (localStorage.clear) {
                    localStorage.clear();
                }
                else {
                    for (var i = 0; i < localStorage.length; i++) {
                        localStorage.removeItem(localStorage.key(i));
                    }
                }
                System.CacheKeyIndexor.MethodNames = [];
                System.CacheKeyIndexor.Keys = [];
            }
        }
    }
};
// To Set an item in localStorage - end

// To Get an item from localStorage - start
var GetLocalStorage = function (key) {
    var item = null;
    if (System.IsLocalStorageAvailable && localStorage) {
        try {
            item = localStorage.getItem(key);
        }
        catch (ex) {
        }
    }
    return item;
};
// To Get an item from localStorage - end

// To Update localStorage - start
var UpdateLocalStorageKeys = function (dataChangeInfo) {
    if (!System.IsLocalStorageAvailable || !localStorage) {
        return;
    }
    if (!System.CacheKeyIndexor.MethodNames || System.CacheKeyIndexor.MethodNames.length === 0) {
        var item = GetLocalStorage("__CacheKeyIndexor");
        if (item) {
            item = JSON.parse(item);
            System.CacheKeyIndexor.MethodNames = item.MethodNames;
            System.CacheKeyIndexor.Keys = item.Keys;
        }
    }
    if (dataChangeInfo && dataChangeInfo.length > 0 && System.MethodNameIndexor) {
        if (System.CacheKeyIndexor.MethodNames && System.CacheKeyIndexor.MethodNames.length > 0) {
            for (var j = 0; j < dataChangeInfo.length; j++) {
                var clientMethodName = System.MethodNameIndexor[dataChangeInfo[j].MethodName];
                if (clientMethodName) {
                    dataChangeInfo[j].MethodName = clientMethodName;
                }
                for (var methodIndex = 0; methodIndex < dataChangeInfo[j].MethodName.length; methodIndex++) {
                    var index = System.CacheKeyIndexor.MethodNames.find(dataChangeInfo[j].MethodName[methodIndex]);
                    if (index && index.length > 0) {
                        var keys = System.CacheKeyIndexor.Keys[index[0]];
                        if (keys) {
                            var itemRemoved = false;
                            var serverModifiedDateTime = dataChangeInfo[j].UpdatedDateTime;
                            for (var i = 0; i < System.CacheKeyIndexor.Keys[index[0]].length; i++) {
                                var clientStorageDateTime = GetLocalStorage("__CallTimeIn__" + System.CacheKeyIndexor.Keys[index[0]][i]);
                                if (serverModifiedDateTime && clientStorageDateTime) {
                                    if (((new Date(serverModifiedDateTime)) - (new Date(clientStorageDateTime))) > (System.ServerDateTime - System.ClientDateTime)) {
                                        localStorage.removeItem(System.CacheKeyIndexor.Keys[index[0]][i]);
                                        itemRemoved = true;
                                        System.CacheKeyIndexor.Keys[index[0]].splice(i, 1);
                                        i--;
                                    }
                                }
                                else {
                                    localStorage.removeItem(System.CacheKeyIndexor.Keys[index[0]][i]);
                                    System.CacheKeyIndexor.Keys[index[0]].splice(i, 1);
                                    i--;
                                    itemRemoved = true;
                                }
                            }
                            if (itemRemoved) {
                                clearOrUpdateCache([dataChangeInfo[j].MethodName[methodIndex]]);
                                SetLocalStorage("__CacheKeyIndexor", JSON.stringify(System.CacheKeyIndexor));
                            }
                        }
                    }
                }
            }
        }
    }
    setTimeout(UpdateLocalStorageCall, 480000); // Repeat every 8 mins(480000)
};
// To Update localStorage - end

var ClearLocalStorageAndCache = function (showMessage) {
    if (System.IsLocalStorageAvailable && localStorage) {
        localStorage.clear();
    }
    System.CachingService = new Services();
    System.CacheKeyIndexor = { "MethodNames": [], Keys: [] };
    if (showMessage) {
        SuccessMessageHandler("Local storage and cache cleared");
    }
};

var ValidateLocalStorageForVersion = function () {
    var versionInLocalStorage = GetLocalStorage("CurrentVersionForLocalStorage");
    if (versionInLocalStorage && currentVersionForLocalStorage != versionInLocalStorage) {
        ClearLocalStorageAndCache(false);
    }
    SetLocalStorage("CurrentVersionForLocalStorage", currentVersionForLocalStorage);
    // console.log(currentVersionForLocalStorage);
};


var UpdateLocalStorageCall = function () {
    var callBack = function (response) {
        if (response !== null) {
            if (response[0].Response !== null && response[0].Response.IsException === false) {
                System.ServerDateTime = new Date(response[0].Response.ServerDateTime);
                System.ClientDateTime = new Date();
                UpdateLocalStorageKeys(response[0].Response.DataChangeInfo);
            }
        }
    };
    var TenantId = readCookie('TenantId');
    var requestG = { Request: { TenantId: TenantId }, ServiceCallMethodName: "GetDataChangeInfo" };
    var ascm = new AsynchronousServiceCallManager(callBack);
    ascm.AddService(requestG);
    ascm.Execute();
};
System.filterChart = function(anchorId,filterValue,exclude){
	var chart = null;
	anchorId = anchorId.replace("#","");
	$.each(dc.chartRegistry.list(),function(index,chartInstance) { 
		if (chartInstance.anchorName() === anchorId){
			chart = chartInstance;
			return false;
		}else{
			var scroll = chartInstance.anchorName().split('container');
			if(scroll[0] === anchorId){
				chart = chartInstance;
				return false;	
			}
		}
	});
	chart.filterAll();
	if (!exclude){
		chart.filter(filterValue);
	}else{
		var keys = [];
		var chartData = chart.data();
		if (chartData[0].values)
			chartData = chartData[0].values;
		chartData.forEach(function(obj){
			var key;
			try{
				key = obj.data.key;
			}catch(e){
				key = obj.key;
			}
			if (key !== filterValue)
			{
				keys.push(key);
			}
		});
		d3.selectAll('text.pie-slice').text(function(d) {
           if($(this).text() && $(this).text().trim() !== "")
        	   $(this).attr("isText",true);
        });
		chart.filter([keys]);
		d3.selectAll('text.pie-slice').text(function(d) {
	       if(!$(this).attr("isText"))
	        	return "";
	       else{
	        	$(this).removeAttr("isText");
	        	  return $(this).text();
	           }   
	    });
	}
	dataAnalyser.profiling.grapher.resetAllProfiles(chart);
};
if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (typeof position !== 'number' || !isFinite(position) 
          || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}
// {"DataChangeInfo":[{"MethodName":"getE2EMF","UpdatedDateTime":"Fri, 11 Apr
// 2014 10:14:45 GMT"}],"IsException":false,"ServerDateTime":"Fri, 11 Apr 2014
// 10:21:44 GMT"}
