var serviceCallMethodNames = [];
//var directoryName = "/E2EMF";
var directoryName = sessionStorage.getItem("directoryName");
serviceCallMethodNames["JiraProfile"] = { "url":directoryName + "/rest/profiling/soprofile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["JiraProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_JiraProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DynamicProfile"] = { "url":directoryName + "/web/ajax/DynamicProfiling/DynamicProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DynamicMaterialGroupDistribution"] = { "url": directoryName + "/rest/tableandfield/fieldreport", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["LoadDynamicValues"] = { "url": directoryName + "/rest/tableandfield/gettablesmap", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getE2EMF"] = { "url": directoryName + "/rest/searchLndData/getE2EMF", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetDataChangeInfo"] = { "url": directoryName + "/rest/searchLndData/GetDataChangeInfo", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getE2emfpo"] = { "url": directoryName + "/rest/searchLndData/getE2emfpo", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getPoProfiling"] = { "url": directoryName + "/rest/profiling/dropship", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getSystemMapSankey"] = { "url": "js/system_map.json", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOProfile"] = { "url":directoryName + "/rest/profiling/soprofile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["dropship"] = { "url":directoryName + "/rest/profiling/dropship", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["systemMapProfiling"] = { "url":directoryName + "/web/systemmap_profiling.csv", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["PODocView"] = { "url":directoryName + "/web/ajax/DataFlow/PODocView.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DocSperaView"] = { "url":directoryName + "/web/ajax/DataFlow/DocSpera.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SODocView"] = { "url":directoryName + "/web/ajax/DataFlow/SODocView.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["MaterialFlow"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_canada_flow.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

//Copy of e2e_canada_flow for APAC
serviceCallMethodNames["seacopy"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_canada_flow_copy.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SystemMapChord"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_system_map.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SystemMapSankey"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_system_map_sankey.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SystemMapNew"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_System_Map_New.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["POFlow"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_po_flow.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

//Copy of Purchase Order Flow for APAC
serviceCallMethodNames["purchaseOrderFlow"] = { "url":directoryName + "/web/ajax/DataFlow/purchase.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };


serviceCallMethodNames["POProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_profiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["PricingProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_pricingprofiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOProfilling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_SOProfilling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GLProfilling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_Manual_GLProfilling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["MaterialDistribution"] = { "url":directoryName + "/web/ajax/Profiling/e2e_material.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

//Copy of material Distribution for APAC
serviceCallMethodNames["materialDistributionCopy"] = { "url":directoryName + "/web/ajax/Profiling/materialDistributionCopy.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SystemMapProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2esysmap.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Search"] = { "url":directoryName + "/web/ajax/Search/e2e_search.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["MerckSearchSummary"] = { "url":directoryName + "/web/ajax/Merck/search.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

//Copy of Search
serviceCallMethodNames["SearchAPAC"] = { "url":directoryName + "/web/ajax/Search/e2e_search_copy.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["POMangement"] = { "url":directoryName + "/web/ajax/PlanandEstimate/e2e_pom.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["OutsourcedManufacturing"] = { "url":directoryName + "/web/ajax/PlanandEstimate/e2e_om.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Affiliate"] = { "url":directoryName + "/web/ajax/PlanandEstimate/e2e_iic.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["supplyplanning"] = { "url":directoryName + "/web/ajax/Plana ndEstimate/e2e_supplyplanning.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SystemMap"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_SystemMap.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

//Copy of System Map for APAC
serviceCallMethodNames["systemMapCopy"] = { "url":directoryName + "/web/ajax/DataFlow/systemMapCopy.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["poprofile"] = { "url":directoryName + "/rest/profiling/poprofile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["soprofile"] = { "url":directoryName + "/rest/profiling/soprofile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["popricingprofile"] = { "url":directoryName + "/rest/profiling/pricing", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["systemmap"] = { "url":directoryName + "/rest/profiling/systemmap", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["pricingProfile"] = { "url":directoryName + "/rest/profiling/pricing", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["manualGL"] = { "url":directoryName + "/rest/profiling/manualgl", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GBT"] = { "url":directoryName + "/web/ajax/GBT/TreeView.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GBTNEW"] = { "url":directoryName + "/web/ajax/GBT/TreeViewNew.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GBTSearch"] = { "url":directoryName + "/web/ajax/GBT/gbt_search.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["TokenAnalysisSearch"] = { "url":directoryName + "/web/ajax/TokenAnalysis/e2e_tokenAnalysis_search.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["TokenAnalysisDiagram"] = { "url":directoryName + "/web/ajax/TokenAnalysis/e2e_tokenAnalysis_diagram.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SearchL3View"] = { "url":directoryName + "/web/ajax/Search/SearchL3View.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["MaterialGroupDistribution"] = { "url":directoryName + "/rest/material/materialgroup", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOReport"] = { "url":directoryName + "/web/ajax/Report/SOReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOReportPost"] = { "url":directoryName + "/rest/profiling/soprofile", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["POReport"] = { "url":directoryName + "/web/ajax/Report/POReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["POReportPost"] = { "url":directoryName + "/rest/profiling/poprofile", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["WOReport"] = { "url":directoryName + "/web/ajax/Report/WOReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["WOReportPost"] = { "url":directoryName + "/rest/profiling/woprofile", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
//serviceCallMethodNames["BatchGenealogy"] = { "url":"../rest/geneology/getBatchJSON", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["BatchGenealogy"] = { "url":directoryName + "/rest/geneology/getReletedBatch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Level3Details"] = { "url":directoryName + "/rest/level3/details", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBatchWhereUsedList"] = { "url":directoryName + "/rest/geneology/getBatchWhereUsedList", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBatchStocksList"] = { "url":directoryName + "/rest/geneology/getBatchStocksList", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBatchGenealogy"] = { "url":directoryName + "/rest/geneology/batchgenealogy", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBomGenealogy"] = { "url":directoryName + "/rest/traceability/getBOM", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["plantProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_plantProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["plantProfilling"] = { "url": directoryName + "/rest/profiling/plantprofile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["search"] = { "url": directoryName + "/rest/search/searchservice", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["solrSearch"] = { "url": directoryName + "/rest/search/solrSearch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["searchSuggestions"] = { "url": directoryName + "/rest/search/searchSuggestions", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["searchEntity"] = { "url": directoryName + "/rest/search/searchservice/", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getDatafromTable"] = { "url": directoryName + "/rest/level3/details/", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["searchEntity"] = { "url": directoryName + "/rest/search/searchservice/", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["plantFlow"] = { "url":directoryName + "/web/ajax/DataFlow/e2e_plant_flow.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["plantFlowData"] = { "url": directoryName + "/rest/plantprofile", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getGoodsHistroy"] = { "url": directoryName + "/rest/searchLndData/getGoodsHistroy", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getLinkedPO"] = { "url": directoryName + "/rest/level3/productionorder", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["tokenAnalysisSearchService"] = { "url": directoryName + "/rest/tokenAnalysis/search", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["tokenAnalysisVisualizationService"] = { "url": directoryName + "/rest/tokenAnalysis/visualization", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SOOthersReport"] = { "url":directoryName + "/web/ajax/Report/SOOthersReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOOthersReportPost"] = { "url":directoryName + "/rest/profiling/soprofile", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SOOthersDetailReport"] = { "url":directoryName + "/web/ajax/Report/SOOthersDetailReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOOthersDetailReportPost"] = { "url":directoryName + "/rest/profiling/soprofile", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["MaterialReport"] = { "url":directoryName + "/web/ajax/Report/MaterialReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["MaterialReportPost"] = { "url":directoryName + "/rest/material/materialreport", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["wireframe"] = { "url":directoryName + "/web/ajax/WireFrame/MoleculeAnalysis1.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UserReport"] = { "url":directoryName + "/web/ajax/Report/UserReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UserReportPost"] = { "url":directoryName + "/rest/user/getuserlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateUser"] = { "url":directoryName + "/rest/user/updateuser", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetUser"] = { "url":directoryName + "/rest/user/getuser", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Notification"] = { "url":directoryName + "/rest/user/sendCredentialsToUser", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateUser"] = { "url":directoryName + "/rest/user/updateuser", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateUser"] = { "url":directoryName + "/rest/user/adduser", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["FAQReports"] = { "url":directoryName + "/web/ajax/FAQ/FAQReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["FAQ"] = { "url":directoryName + "/web/ajax/FAQ/FAQ.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["FAQReportPost"] = { "url":directoryName + "/rest/ContextHelpResource/getfaqlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateFAQ"] = { "url":directoryName + "/rest/ContextHelpResource/updateFaq", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateFAQ"] = { "url":directoryName + "/rest/ContextHelpResource/addfaq", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetFAQ"] = { "url":directoryName + "/rest/ContextHelpResource/getfaq", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };


serviceCallMethodNames["RoleActionReport"] = { "url":directoryName + "/web/ajax/Report/RoleActionReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["RoleActionReportPost"] = { "url":directoryName + "/rest/admin/getroleactionlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateRoleAction"] = { "url":directoryName + "/rest/admin/addroleaction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateRoleAction"] = { "url":directoryName + "/rest/admin/getroleactionlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetRoleAction"] = { "url":directoryName + "/rest/admin/getroleactionlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteRoleAction"] = { "url":directoryName + "/rest/admin/deleteroleaction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ActionReport"] = { "url":directoryName + "/web/ajax/Report/ActionReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ActionReportPost"] = { "url":directoryName + "/rest/admin/getactionlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateAction"] = { "url":directoryName + "/rest/admin/addaction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteAction"] = { "url":directoryName + "/rest/admin/deleteaction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateAction"] = { "url":directoryName + "/rest/admin/updateaction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetAction"] = { "url":directoryName + "/rest/admin/getaction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SupplyChainVisibility"] = { "url":directoryName + "/web/ajax/SupplyChainVisibility/SupplyChainVisibility.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["widgetConfig"] = { "url":directoryName + "/web/ajax/WidgetConfig/widgetConfig.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
// -->> Creating for Role
serviceCallMethodNames["roles"] = { "url":directoryName + "/web/ajax/Report/Role.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetAllRoles"] = { "url":directoryName + "/rest/user/getroles", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateRoles"] = { "url":directoryName + "/rest/user/addrole", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateRoles"] = { "url":directoryName + "/rest/user/updaterole", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetRole"] = { "url":directoryName + "/rest/user/getrole", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

/*Creating for AdhocReports*/
serviceCallMethodNames["AdhocReports"] = { "url":directoryName + "/web/ajax/Report/AdhocReports.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetAdhocReports"] = { "url":directoryName + "/rest/adhocReport/getadhocReports", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GetAdhocReport"] = { "url":directoryName + "/rest/adhocReport/getadhocReport", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateAdhocReports"] = { "url":directoryName + "/rest/adhocReport/createadhocReport", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateAdhocReports"] = { "url":directoryName + "/rest/adhocReport/updateadhocReport", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["LookupReport"] = { "url":directoryName + "/web/ajax/Report/LookupReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateLookup"] = { "url":directoryName + "/rest/admin/addlookupvalue", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["LookupReportPost"] = { "url":directoryName + "/rest/admin/getlookuplist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteLookup"] = { "url":directoryName + "/rest/admin/deletelookupvalue", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateLookUp"] = { "url":directoryName + "/rest/admin/updatelookupvalue", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["CreateIssue"] = { "url":directoryName + "/rest/user/jiraissue", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };


serviceCallMethodNames["getGBTSearchCount"] = { "url":directoryName + "/rest/geneology/getSearchParams", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getGBTSearchEntity"] = { "url":directoryName + "/rest/geneology/getSearchParams", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["woprofile"] = { "url":directoryName + "/web/ajax/Profiling/e2e_WOProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["woProfiling"] = { "url": directoryName + "/rest/profiling/woprofile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getGBTSearchEntity"] = { "url":directoryName + "/rest/geneology/getSearchParams", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ExcelDownload"] = { "url":directoryName + "/rest/material/materialreportexcel", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["FinanceReport"] = { "url":directoryName + "/web/ajax/Report/FinanceReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["FinanceReportPost"] = { "url":directoryName + "/rest/user/getreports", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["batchMapping"] = { "url":directoryName + "/web/ajax/GBT/BatchMapping.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["materialMapping"] = { "url":directoryName + "/web/ajax/GBT/MaterialMapping.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["BatchMappingPost"] = { "url":directoryName + "/rest/geneology/getBatchMapping", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["MaterialMappingPost"] = { "url":directoryName + "/rest/geneology/getMaterialMapping", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["catalogSearch"] = { "url":directoryName + "/ajax/InstrumentCatalog/CatalogSearch.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["catalogSearchNew"] = { "url":directoryName + "/ajax/InstrumentCatalog/CatalogSearch1.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["catalogSearchPost"] = { "url":directoryName + "/rest/instrumentcatalog/instrumentsearch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["catalogTagUpdate"] = { "url":directoryName + "/rest/instrumentcatalog/catalogTagUpdate", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["shortlistDocument"] = { "url":directoryName + "/rest/instrumentcatalog/shortlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["getShortlists"] = { "url":directoryName + "/rest/instrumentcatalog/getshortlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["ReplacedCatalogSearchTitle"] = { "url":directoryName + "/rest/instrumentcatalog/dobulkupdate", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["DeleteCatalogSearchTitle"] = { "url":directoryName + "/rest/instrumentcatalog/updatedeletestatus", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };

//For Clinical Speciality
serviceCallMethodNames["clinicalspeciality"] = { "url":directoryName + "/web/ajax/InstrumentCatalog/ClinicalSpeciality.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getclinicalspeciality"] = { "url":directoryName + "/rest/instrumentcatalog/getclinicalspeciality", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["AddClinicalSpeciality"] = { "url":directoryName + "/rest/instrumentcatalog/addclinicalspeciality", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateClinicalSpeciality"] = { "url":directoryName + "/rest/instrumentcatalog/updateclinicalspeciality", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteClinicalSpeciality"] = { "url":directoryName + "/rest/instrumentcatalog/deleteclinicalspeciality", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["bookmarksearch"] = { "url":directoryName + "/rest/instrumentcatalog/bookmarksearch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["shortlist"] = { "url":directoryName + "/ajax/InstrumentCatalog/shortlist.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["sendEmail"] = { "url":directoryName + "/rest/email/sendemail", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["downloadZip"] = { "url":directoryName + "/rest/download/downloadzip", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["bulkPrint"] = { "url":directoryName + "/rest/pdf/mergepdf", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["incrementactioncount"] = { "url":directoryName + "/rest/instrumentcatalog/incrementactioncount", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["makeProfile"] = { "url":directoryName + "/ajax/SLOB/makeProfile.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["makeProfilePost"] = { "url":directoryName + "/rest/slob/slobview", "EnableCaching": true, "EnableServerCaching": true, "DependentMethods": null };

//For Email Notification
serviceCallMethodNames["SendMailWithAttachments"] = { "url":directoryName + "/rest/mail/sendmailwithattachments", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["nGramAnalyserIC"] = { "url":directoryName + "/ajax/InstrumentCatalog/nGramAnalyserIC.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyserICPost"] = { "url":directoryName + "/rest/instrumentcatalog/nGramAnalyser", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyserICPostStoredResults"] = { "url":directoryName + "/rest/instrumentcatalog/nGramAnalyserStored", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["refreshNgram"] = { "url":directoryName + "/rest/instrumentcatalog/refreshNgram", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyserNonDictionary"] = { "url":directoryName + "/ajax/nGram/nGramNonDictionaryAnalyserIC.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyserNonDictionaryPost"] = { "url":directoryName + "/rest/dataCorrections/getDataUnCorrectedWords", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyser"] = { "url":directoryName + "/ajax/nGram/nGramAnalyser.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyserPost"] = { "url":directoryName + "/rest/nGram/nGramAnalyser", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["nGramAnalyserNLPPost"] = { "url":directoryName + "/rest/nGram/nGramAnalyserNLP", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["poextractanalysis"] = { "url":directoryName + "/rest/profiling/extractanalysispo", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["POExtractProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_extractanalysis.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["soextractanalysis"] = { "url":directoryName + "/rest/profiling/extractanalysisso", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SOExtractProfiling"] = { "url":directoryName + "/web/ajax/Profiling/e2e_extractanalysis_so.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["buyProfile"] = { "url":directoryName + "/ajax/SLOB/buyProfile.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["buyProfilePost"] = { "url":directoryName + "/rest/slob/slobbuyprofileview", "EnableCaching": true, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["profileClusterPost"] = { "url":directoryName + "/rest/slob/slobclusterdetails", "EnableCaching": true, "EnableServerCaching": true, "DependentMethods": null };

serviceCallMethodNames["ppaMaterialMapping"] = { "url":directoryName + "/ajax/PlantPurchase/ppa_materialMapping.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ppaMaterialMappingPost"] = { "url":directoryName + "/rest/PlantPurchase/materialMapping", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };


serviceCallMethodNames["ppaPOProfiling"] = { "url":directoryName + "/ajax/PlantPurchase/ppa_purchaseOrderProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ppaPOProfilingPost"] = { "url":directoryName + "/rest/emea/emeapoprofileview", "EnableCaching": true, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["ppaPOProfilingL2ViewPost"] = { "url":directoryName + "/rest/emea/emeapoprofilel2view", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ppaQuotesProfiling"] = { "url":directoryName + "/ajax/PlantPurchase/ppa_qoutesProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ppa_varianceAnalysis"] = { "url":directoryName + "/ajax/PlantPurchase/ppa_varianceAnalysis.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ppaVarianceAnalysisGet"] = { "url":directoryName + "/rest/emea/emeapovarianceanalysisview", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ppaQuotesProfilingPost"] = { "url":directoryName + "/rest/emea/emeaquotesprofileview", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ppaQuotesProfilingL2ViewPost"] = { "url":directoryName + "/rest/emea/emeaquotesprofilel2view", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ppaPpvProfiling"] = { "url":directoryName + "/ajax/PlantPurchase/ppa_ppvProfiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ppaPpvProfilingPost"] = { "url":directoryName + "/rest/emea/emeappvprofileview", "EnableCaching": false, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["ppaPpvProfilingL2ViewPost"] = { "url":directoryName + "/rest/emea/emeappvprofilel2view", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ppaPpvReportTemplate"] = { "url":directoryName + "/ajax/PlantPurchase/ppa_ppvReportTemplate.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ppaPpvOverviewPost"] = { "url":directoryName + "/rest/emea/emeappvoverview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["profileAnalysis"] = { "url":directoryName + "/ajax/SLOB/profileAnalysis.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Dslink"] = { "url":directoryName + "/ajax/InstrumentCatalog/dsLink.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Eplink"] = { "url":directoryName + "/ajax/InstrumentCatalog/epLink.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Jrlink"] = { "url":directoryName + "/ajax/InstrumentCatalog/jrLink.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Avalignlink"] = { "url":directoryName + "/ajax/InstrumentCatalog/Avalign.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Blacksmithlink"] = { "url":directoryName + "/ajax/InstrumentCatalog/Blacksmith.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["gSourcelink"] = { "url":directoryName + "/ajax/InstrumentCatalog/gSource.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Teleflexlink"] = { "url":directoryName + "/ajax/InstrumentCatalog/Teleflex.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["hebumedicallink"] = { "url":directoryName + "/ajax/InstrumentCatalog/hebumedical.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Aesculaplink"] = { "url":directoryName + "/ajax/InstrumentCatalog/Aesculap.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["p2020Profiling"] = { "url":directoryName + "/ajax/P2020/P2020Profiling.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020InventoryDashboadPost"] = { "url":directoryName + "/rest/p2020/p2020agprofileview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020SourceListPost"] = { "url":directoryName + "/rest/p2020/p2020ListSources", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["p2020CustomerOTIF"] = { "url":directoryName + "/ajax/P2020/p2020_customerOTIF.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020CustomerOTIFPost"] = { "url":directoryName + "/rest/p2020/p2020otifprofileview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020CustomerOTIFSearchData"] = { "url":directoryName + "/rest/p2020/searchOtifProfileData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["P2020PRMS"] = { "url":directoryName + "/ajax/P2020/P2020_PRMS.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020PRMSPost"] = { "url":directoryName + "/rest/p2020/prmsAgProfileview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020PRMSFilterPost"] = { "url":directoryName + "/rest/p2020/prmsAgFilterProfileView", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020SearchProfileData"] = { "url":directoryName + "/rest/p2020/searchP2020ProfileData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020PRMSAgSalesCustProfilePost"] = { "url":directoryName + "/rest/p2020/prmsAgSalesCustProfileView", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["prmsAgSalesCustFilterProfileView"] = { "url":directoryName + "/rest/p2020/prmsAgSalesCustFilterProfileView", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["prmsAgSalesprofileView"] = { "url":directoryName + "/rest/p2020/prmsAgSalesprofileView", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["P2020MappingRules"] = { "url":directoryName + "/ajax/P2020/p2020_MappingRules.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["p2020MappingRulesPost"] = { "url":directoryName + "/rest/p2020/p2020mappingview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };


serviceCallMethodNames["SystemProperties"] = { "url":directoryName + "/web/ajax/Report/DataObfuscation.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SystemPropertiesGetAction"] = { "url":directoryName + "/rest/app_properties/getproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SystemPropertiesRefreshAction"] = { "url":directoryName + "/rest/app_properties/refreshproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["FormDataReport"] = { "url":directoryName + "/web/ajax/Report/FormDataReport.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["FormDataReportPost"] = { "url":directoryName + "/rest/app_properties/getformdatalist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateFormDataPost"] = { "url":directoryName + "/rest/app_properties/updateformdata", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["deleteFormData"] = { "url":directoryName + "/rest/app_properties/deleteformdata", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["addformdata"] = { "url":directoryName + "/rest/app_properties/addformdata", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["ObfuscationWords"] = { "url":directoryName + "/web/ajax/Report/ObfuscationWords.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ObfuscationWordsPost"] = { "url":directoryName + "/rest/app_properties/getobfwordlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateObfuscationWords"] = { "url":directoryName + "/rest/app_properties/addobfwords", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateObfuscationWords"] = { "url":directoryName + "/rest/app_properties/updateobfwords", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteObfuscationWords"] = { "url":directoryName + "/rest/app_properties/deleteobfwords", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["AppProperties"] = { "url":directoryName + "/web/ajax/Report/AppProperties.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["AppPropertiesPost"] = { "url":directoryName + "/rest/app_properties/getpropertylist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["searchHistory"] = { "url":directoryName + "/web/ajax/Report/searchHistory.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["searchHistoryPost"] = { "url":directoryName + "/rest/instrumentcatalog/getsearchhistory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["bookmarks"] = { "url":directoryName + "/web/ajax/Report/bookmarks.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["bookmarksPost"] = { "url":directoryName + "/rest/instrumentcatalog/getsearchhistory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["UpdateAppProperties"] = { "url":directoryName + "/rest/app_properties/updateappproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateAppProperties"] = { "url":directoryName + "/rest/app_properties/addappproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteAppProperties"] = { "url":directoryName + "/rest/app_properties/deleteappproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["Category"] = { "url":directoryName + "/web/ajax/InstrumentCatalog/Category.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CategoryPost"] = { "url":directoryName + "/rest/app_properties/getcategorieslist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["AddCategory"] = { "url":directoryName + "/rest/app_properties/addcategory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["UpdateCategory"] = { "url":directoryName + "/rest/app_properties/updatecategory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteCategory"] = { "url":directoryName + "/rest/app_properties/deletecategory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["CreateApp"] = { "url":directoryName + "/rest/app_properties/addappproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["DeleteApp"] = { "url":directoryName + "/rest/app_properties/deleteappproperties", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["SFStatistics"] = { "url":directoryName + "/web/ajax/InstrumentCatalog/Statistic.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SFStatisticsPost"] = { "url":directoryName + "/rest/instrumentcatalog/smartfindmasterview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["SFStatisticsL2View"] = { "url":directoryName + "/rest/instrumentcatalog/instrumentsearchl2view", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["usrotcDashboard"] = { "url":directoryName + "/ajax/Kitkat/kitkatDashboard.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["kitkatSourceListPost"] = { "url":directoryName + "/rest/kitkat/kitkatSourceList", "EnableCaching": false, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["kitkatDashboardInventoryPost"] = { "url":directoryName + "/rest/kitkat/kitkatAgProfileView", "EnableCaching": false, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["kitkatDashboardSalesPost"] = { "url":directoryName + "/rest/kitkat/kitkatAgSoProfileView", "EnableCaching": false, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["kitkatSourceListPost"] = { "url":directoryName + "/rest/kitkat/kitkatSourceList", "EnableCaching": false, "EnableServerCaching": true, "DependentMethods": null };

serviceCallMethodNames["dataAnalyser"] = { "url":directoryName + "/ajax/DataAnalyser/DataAnalyser.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getDatabaseList"] = { "url":directoryName + "/rest/dataAnalyser/getDatabaseList", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getTableList"] = { "url":directoryName + "/rest/dataAnalyser/getTableList", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getColumnList"] = { "url":directoryName + "/rest/dataAnalyser/getColumnList", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMultiColumnList"] = { "url":directoryName + "/rest/dataAnalyser/getMultiColumnList", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getData"] = { "url":directoryName + "/rest/dataAnalyser/getData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getcolumnInfo"] = { "url":directoryName + "/rest/dataAnalyser/getcolumnInfo", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMultipleData"] = { "url":directoryName + "/rest/dataAnalyser/getMultipleData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["saveFileToLocation"] = { "url":directoryName + "/rest/dataAnalyser/saveFileToLocation", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["saveProfile"] = { "url":directoryName + "/rest/dataAnalyser/saveProfile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMyProfiles"] = { "url":directoryName + "/rest/dataAnalyser/getMyProfiles", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getAction"] = { "url":directoryName + "/rest/dataAnalyser/getAction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getProfileConfig"] = { "url":directoryName + "/rest/dataAnalyser/getProfileConfig", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["excutequery"] = { "url":directoryName + "/rest/dataAnalyser/excutequery", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["executeTestSuite"] = { "url":directoryName + "/rest/dataAnalyser/executeTestSuite", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["importProfile"] = { "url":directoryName + "/rest/dataAnalyser/importProfile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["exportProfile"] = { "url":directoryName + "/rest/dataAnalyser/exportProfile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["PublishProfile"] = { "url":directoryName + "/rest/dataAnalyser/publishProfile", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getPublishProfileTemplate"] = { "url":directoryName + "/ajax/Templates/publishProfile.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getProfileHistory"] = { "url":directoryName + "/rest/dataAnalyser/getProfileHistory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getProfileHistoryConfig"] = { "url":directoryName + "/rest/dataAnalyser/getProfileHistoryConfig", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["saveView"] = { "url":directoryName + "/rest/dataAnalyser/saveView", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getChartConfig"] = { "url":directoryName + "/rest/dataAnalyser/getChartConfigurationList", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getEmailNotificationTemplate"] = { "url":directoryName + "/ajax/DataAnalyser/EmailNotification.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["saveEmailNotification"] = { "url":directoryName + "/rest/mail/saveemailnotification", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateToDB"] = { "url":directoryName + "/rest/dataAnalyser/updateToDB", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateToDBForMultipeRows"] = { "url":directoryName + "/rest/dataAnalyser/updateToDBForMultipeRows", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["clearCache"] = { "url":directoryName + "/rest/dataAnalyser/clearCache", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getWidget"] = { "url":directoryName + "/ajax/Templates/Widget.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getSummary"] = { "url":directoryName + "/ajax/Templates/Summary.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getFilterConfig"] = { "url":directoryName + "/ajax/Templates/filterConfig.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getSummaryConfig"] = { "url":directoryName + "/ajax/Templates/SummaryConfig.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getPie"] = { "url":directoryName + "/ajax/Templates/pieChart.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getDatatable"] = { "url":directoryName + "/ajax/Templates/Datatable.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getCalendar"] = { "url":directoryName + "/ajax/Templates/Calendar.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getNGram"] = { "url":directoryName + "/ajax/Templates/NGram.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getIFrame"] = { "url":directoryName + "/ajax/Templates/IFrame.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getHTMLTemplate"] = { "url":directoryName + "/ajax/Templates/HTMLTemplate.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getFlippedCard"] = { "url":directoryName + "/ajax/Templates/FlippedCard.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getImageWidget"] = { "url":directoryName + "/ajax/Templates/ImageWidget.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["tableEditor"] = { "url":directoryName + "/ajax/TableEditor/tableEditor.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["kitkatInvSalesPost"] = { "url":directoryName + "/rest/kitkat/kitkatAgInvSoProfileView", "EnableCaching": false, "EnableServerCaching": true, "DependentMethods": null };

serviceCallMethodNames["tree"] = { "url":directoryName + "/web/ajax/Kitkat/BomTraceability.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["treeshow"] = { "url":directoryName + "/rest/kitkat/getbomtraceability", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["faq"] = { "url":directoryName + "/web/ajax/InstrumentCatalog/faq.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["DynamicProfiling"] = { "url":directoryName + "/web/ajax/DataAnalyser/DynamicProfile.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getreleateddocments"] = { "url":directoryName + "/rest/instrumentcatalog/getreleateddocments", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["textMatching"] = { "url":directoryName + "/web/ajax/StringMatching/StringMatching.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getClosestMatch"] = { "url":directoryName + "/rest/stringMatchingAlgorithm/getClosestMatch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["geoSpatial"] = { "url":directoryName + "/web/ajax/GeoSpatial/map.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["geoSpatialDc"] = { "url":directoryName + "/web/ajax/GeoSpatialDC/GeoSpatialDC.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getGeoJsonData"] = { "url": directoryName+"/web/data/geoChartData.json", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["sankeyGenarator"] = { "url":directoryName + "/web/ajax/DataAnalyser/SankeyGenerator.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getSankeyJSON"] = { "url":directoryName + "/rest/sankeyGenerator/getJSON", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["masterDataConfig"] = { "url":directoryName + "/web/ajax/DataAnalyser/masterDataConfig.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["generateMapping"] = { "url":directoryName + "/rest/PlantPurchase/generateMaterialMapping", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getimagesetlist"] = { "url":directoryName + "/rest/instrumentcatalog/getimagesetlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["fileUpload"] = { "url":directoryName + "/rest/file/upload", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["uploadCSV"] = { "url":directoryName + "/rest/file/uploadcsv", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["importData"] = { "url":directoryName + "/web/ajax/importData/importData.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["suggestionsLookUp"] = { "url":directoryName + "/rest/sankeyGenerator/suggestionsLookUp", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getDataPagination"] = { "url":directoryName + "/rest/dataAnalyser/getDataPagination", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMultipleDataPagination"] = { "url":directoryName + "/rest/dataAnalyser/getMultipleDataPagination", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getDataTableClientSide"] = { "url":directoryName + "/rest/dataAnalyser/getDataTableClientSide", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getDataFromExcel"] = { "url":directoryName + "/rest/dataAnalyser/getDataFromExcel", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["processcpusimulation"] = { "url":directoryName + "/rest/solenis/processcpusimulation", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["docsperahome"] = { "url":directoryName + "/web/ajax/Docspera/Docspera.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["eventDetailsView"] = { "url":directoryName + "/web/ajax/DataAnalyser/eventDetails.html", "EnableCaching": true, "EnableServerCaching": true, "DependentMethods": null };
serviceCallMethodNames["searchConfig"] = { "url":directoryName + "/web/ajax/SearchConfig/searchConfig.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getEventByID"] = { "url":"/PSSC/rest/event/geteventid", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getPartView"] = { "url":"/PSSC/rest/case/partview", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["sendmail"] = { "url":"/PSSC/rest/mailer/sendmail", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getAllWorkFlowStatus"] = { "url":directoryName +"/rest/workflow/getalltransitions", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getworkflowauditlist"] = { "url":directoryName +"/rest/workflow/getworkflowauditlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateworkflowstatus"] = { "url":directoryName +"/rest/workflow/updateworkflowstatus", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getWorkFlowStatus"] = { "url":directoryName +"/rest/workflow/getentity", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getWorkFlowStatusList"] = { "url":directoryName +"/rest/workflow/getentitylist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getReportQuery"] = { "url":directoryName +"/rest/user/getReportQueryByReportName", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getActionButton"] = { "url":directoryName +"/rest/workflow/getnexttransitions", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["applyWorkFlowAction"] = { "url":directoryName +"/rest/workflow/applynexttransition", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["GP"] = { "url":"/GP/web/ajax/GP.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["addorder"] = { "url": "/PSSC/rest/order/addorder", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getorderdetails"] = { "url": "/PSSC/rest/order/getorderdetails", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updatecase"] = { "url": "/PSSC/rest/event/updatecase", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateactualpartsize"] = { "url": "/PSSC/rest/event/updateactualpartsize", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updatecasestatus"] = { "url": "/PSSC/rest/casestatus/updatecasestatus", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getcaseauditdetailslogs"] = { "url": "/PSSC/rest/caseaudit/getcaseauditdetailslogs", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["createcase"] = { "url": "/PSSC/rest/event/createnewcase", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["treeGrid"] = { "url": "/GP/web/ajax/treeGrid.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getTreeGridData"] = { "url": "/GP/rest/GrossProfitability/getTreeByGroup", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["setGPSettings"] = { "url": "/GP/rest/GrossProfitability/copySettings", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["setGPExplodeTree"] = { "url": "/GP/rest/GrossProfitability/explodeTree", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["calculateGPVolume"] = { "url": "/GP/rest/GrossProfitability/calculate", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["materialbatchlookup"] = { "url": "/GBTRocksDBProcessor/rest/neo4j/materialbatchlookup", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["materialbatch"] = { "url": directoryName +"/web/ajax/GBT/materialbatchlookup.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
//serviceCallMethodNames["materialbatch"] = { "url": directoryName +"/web/ajax/dcfc/d3fc.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getLoginContent"] = { "url":directoryName + "/rest/login/getlogincontent", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateUserPrimaryRole"] = { "url":directoryName + "/rest/user/updateUserPrimaryRole", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getDetailsByPartNumber"] = { "url":directoryName + "/rest/Galaxie/getDetailsByPartNumber", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

//For IntegraLife
serviceCallMethodNames["integraSearch"] = { "url": directoryName + "/rest/integrasearch/searchservice", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["integraSearchEntity"] = { "url": directoryName + "/rest/integrasearch/searchservice/", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["integraBookmarksearch"] = { "url":directoryName + "/rest/integrasearch/bookmarksearch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["integraSearhcHistory"] = { "url":directoryName + "/rest/integrasearch/getsearchhistory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["integraShortlistDocument"] = { "url":directoryName + "/rest/integrasearch/shortlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["integraGetShortLists"] = { "url":directoryName + "/rest/integrasearch/getshortlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["integraGetAgileParts"] = { "url":directoryName + "/rest/integrasearch/getAgileParts", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": "catalogSearchPost" };
serviceCallMethodNames["agileChart"] = { "url": directoryName + "/web/ajax/IntegraSearch/agileChart.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["BOMIntegra"] = { "url": directoryName + "/web/ajax/IntegraSearch/BOMIntegra.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBOM"] = { "url":directoryName + "/rest/integrasearch/getBOM", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBOMIds"] = { "url":directoryName + "/rest/integrasearch/getBOMIds", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getIntegraCategories"] = { "url":directoryName + "/rest/integrasearch/getCategories", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateIntegraCatalogs"] = { "url":directoryName + "/rest/integrasearch/updateCatalogs", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["integraGenericUpdate"] = { "url":directoryName + "/rest/integrasearch/genericUpdate", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getBrands"] = { "url":directoryName + "/rest/integrasearch/getBrands", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMasterData"] = { "url":directoryName + "/rest/integrasearch/getMasterData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["integraMasterData"] = { "url":directoryName + "/web/ajax/IntegraSearch/integraMasterData.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

//Solr Master
serviceCallMethodNames["updateSolrCategories"] = { "url": directoryName + "/rest/solrmaster/updateCategories/", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateCSpeciality"] = { "url": directoryName + "/rest/solrmaster/updateCSpeciality/", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

//Search Configuration
serviceCallMethodNames["getSolrSchema"] = { "url":directoryName + "/rest/search/getSolrSchema", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["indexSolr"] = { "url":directoryName + "/rest/search/indexSolr", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["solrIndexer"] = { "url": directoryName + "/web/ajax/SearchConfig/solrIndexer.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["cart"] = { "url": directoryName + "/web/ajax/SearchConfig/cart.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["queryBuilder"] = { "url": directoryName + "/web/ajax/SearchConfig/queryBuilder.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["searchConfigurator"] = { "url": directoryName + "/web/ajax/SearchConfig/searchConfigurator.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateQueryCache"] = { "url": directoryName + "/rest/search/updateQueryCache", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["searchGetShortLists"] = { "url": directoryName + "/rest/search/getshortlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["shortListSearch"] = { "url": directoryName + "/rest/search/shortlist", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getSearchLogs"] = { "url": directoryName + "/rest/search/getSearchLogs", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["markSearch"] = { "url": directoryName + "/rest/search/markSearch", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["deleteSearchLog"] = { "url": directoryName + "/rest/search/deleteSearchLog", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["applyCSRules"] = { "url":directoryName + "/rest/solrmaster/applyCspecialityRules", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["clearDraftCS"] = { "url":directoryName + "/rest/solrmaster/clearDraftCspeciality", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["applyCategoryRules"] = { "url":directoryName + "/rest/solrmaster/applyCategoryRules", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["clearDraftCategory"] = { "url":directoryName + "/rest/solrmaster/clearDraftCategory", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateCS"] = { "url":directoryName + "/rest/solrmaster/updateCSpeciality", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateCategories"] = { "url":directoryName + "/rest/solrmaster/updateCategories", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getProfileActions"] = { "url":directoryName + "/rest/profileActions/getProfileActions", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["saveProfileAction"] = { "url":directoryName + "/rest/profileActions/saveProfileAction", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateCategories"] = { "url":directoryName + "/rest/solrmaster/updateCategories", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["plotychartdemo"] = { "url":directoryName + "/web/ajax/plotychartdemo.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["profileAnalyser"] = { "url":directoryName + "/web/ajax/profileAnalyser/profileAnalyser.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["queryExecuter"] = { "url":directoryName + "/web/ajax/DataAnalyser/queryTest.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["testSuite"] = { "url":directoryName + "/web/ajax/DataAnalyser/testSuite.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["dataSourceAnalyser"] = { "url":directoryName + "/web/ajax/DataAnalyser/dataSource.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["IntegraMasterData"] = { "url":directoryName + "/web/ajax/Integra/MasterData.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMasterDataDetails"] = { "url":directoryName + "/rest/integrasearch/getMasterDataDetails", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["idmpMasterData"] = { "url":directoryName + "/web/ajax/IDMP/idmpMasterData.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getUpdatedColumns"] = { "url":directoryName + "/rest/dataAnalyser/getUpdatedColumns", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["profileTemplate"] = { "url":directoryName + "/ajax/profileTemplate/profileTemplate.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["profileTemplateView"] = { "url":directoryName + "/ajax/profileTemplate/profileTemplateView.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["ImageCluster"] = { "url":directoryName + "/ajax/ImageCluster/ImageCluster.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["marketforecast"] = { "url":directoryName + "/web/ajax/WireFrame/marketforecast.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["marketforecast_hive"] = { "url":directoryName + "/web/ajax/WireFrame/marketforecast.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["qArtimagedashboard"] = { "url":directoryName + "/web/ajax/Kallik/kallikImageDashboard.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["qArttextdashboard"] = { "url":directoryName + "/web/ajax/Kallik/kallikTextDashBoard.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["updateRAMapping"] = { "url":directoryName + "/rest/regulatoryAffair/generateMapping", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["qArt"] = { "url":directoryName + "/ajax/cockpit/cockpit.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["qSync"] = { "url":directoryName + "/ajax/cockpit/cockpit.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["qFind"] = { "url":directoryName + "/ajax/cockpit/cockpit.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["DynamicHTML"] = { "url":directoryName + "/ajax/EvidenceSearch/DynamicHTML.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["MosaiQ"] = { "url":directoryName + "/ajax/EvidenceSearch/MosaiQ.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Privacy"] = { "url":directoryName + "/ajax/EvidenceSearch/PrivacyLink.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Pubmed"] = { "url":directoryName + "/ajax/EvidenceSearch/Pubmed.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Disclaimer"] = { "url":directoryName + "/ajax/EvidenceSearch/DisclaimerLink.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["EvidenceFAQ"] = { "url":directoryName + "/ajax/EvidenceSearch/FAQLink.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["Evidence_ContactUs"] = { "url":directoryName + "/ajax/EvidenceSearch/Evidence_ConactUs.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["EvidenceSearch"] = { "url":directoryName + "/ajax/EvidenceSearch/SearchEvidence.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["EvidenceSearchHistory"] = { "url":directoryName + "/ajax/EvidenceSearch/EvidenceSearchHistory.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["EvidenceSearchStrategy"] = { "url":directoryName + "/ajax/EvidenceSearch/EvidenceSearchStrategy.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["simulation"] = { "url":directoryName + "/ajax/Solenis/simulation.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["cpusimulation"] = { "url":directoryName + "/ajax/Solenis/bulkSimulation.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["cpureport"] = { "url":directoryName + "/ajax/Solenis/cpureport.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getLDAPUserDetails"] = { "url":directoryName + "/rest/LDAPDetails/getLDAPDetails", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["URLGenerator"] = { "url":directoryName + "/ajax/DynamicURL/URLGenerator.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["movingAverage"] = { "url": "/Forecast/forecastData/movingAverage", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getAllForecast"] = { "url": "/Forecast/forecastVersion/list", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["addForecast"] = { "url": "/Forecast/forecastVersion/add", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateForecast"] = { "url": "/Forecast/forecastVersion/updateForecastVersion", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getForecastById"] = { "url": "/Forecast/forecastVersion/getForecastVersion", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateArchive"] = { "url": "/Forecast/forecastVersion/updateArchive", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getForecastMetaData"] = { "url": "/Forecast/forecastVersion/getForecastMetaData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getProductMapData"] = { "url": "/Forecast/forecastVersion/getProductMapData", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["FishEye"] = { "url":directoryName + "/ajax/FishEye/FishEye.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["saveExtractedText"] = { "url":directoryName + "/rest/crystal/saveExtractedText", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["integraradynamicchart"] = { "url":directoryName + "/ajax/integraRA/submissionListingPieChart.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
//Hurricane
serviceCallMethodNames["hurricaneBoDB"] = { "url":directoryName + "/ajax/hurricane/hurricaneBoDB.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["hrcFlow"] = { "url":directoryName + "/ajax/hurricane/hrcFlow.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

//crystal
serviceCallMethodNames["getCrystalBOM"] = { "url":directoryName + "/rest/merck/getCrystalBOM", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["mongoConnector"] = { "url":directoryName + "/ajax/Crystal/MongoConnector.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMongoDBCollectionList"] = { "url":directoryName + "/rest/merck/getMongoDBCollectionList", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getMongoDBConnection"] = { "url":directoryName + "/rest/merck/getMongoDBConnection", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["executeMongoQuery"] = { "url":directoryName + "/rest/merck/executeMongoQuery", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["uploadCSVToTable"] = { "url":directoryName + "/rest/file/uploadCSVToTable", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["getAllNgramFilter"] = { "url":directoryName + "/rest/nGram/getAllNgramFilter", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["addNgramFilter"] = { "url":directoryName + "/rest/nGram/addNgramFilter", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateNgramFilter"] = { "url":directoryName + "/rest/nGram/updateNgramFilter", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["deleteNgramFilter"] = { "url":directoryName + "/rest/nGram/deleteNgramFilter", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["qFindED"] = { "url":directoryName + "/ajax/pubmed_search/PubmedSearch.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["updateDuplicate"] = { "url":directoryName + "/rest/merck/updateDuplicate", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["TableView"] = { "url":directoryName + "/web/ajax/DataAnalyser/TableView.html", "EnableCaching": true, "EnableServerCaching": false, "DependentMethods": null };

//comments
serviceCallMethodNames["addComment"] = { "url":directoryName + "/rest/comments/addComment", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["getComments"] = { "url":directoryName + "/rest/comments/getComments", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };

serviceCallMethodNames["cassaLanding"] = { "url":directoryName + "/ajax/CASSA/landingPage.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["di_job_instance_details"] = { "url":directoryName + "/ajax/dataInjectionCenter/dataInjectionJobInstance.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };
serviceCallMethodNames["oraclestoragecloud"] = { "url":directoryName + "/ajax/dataInjectionCenter/oracleStorageCloud.html", "EnableCaching": false, "EnableServerCaching": false, "DependentMethods": null };