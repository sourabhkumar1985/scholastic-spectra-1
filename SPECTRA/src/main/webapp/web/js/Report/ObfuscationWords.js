'use strict';
pageSetUp();
var ObfuscationWords;
var ObfuscationWordsInstance = function() {
	ObfuscationWords = new ObfuscationWordsObject();
	ObfuscationWords.Load();
};
var ObfuscationWordsObject = function(){
	
	var THIS = this;
	$("#btnCreate").unbind('click').bind('click', function() {
		ObfuscationWords.Create();
	});
	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			fromword : {
				required : true
			},
			toword : {
				required: true
			},
			
		},
		// Messages for form validation
		messages : {
			login : {
				fromword : 'Please enter From Word'
			},
			toword : {
				required : 'Please enter To Word'
			},
			
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSankeyFilter").submit(function(e) {
	    e.preventDefault();
	});
		

	this.Load = function(source) {
		enableLoading();
		//source = source || "JDE";
		//var request = "viewType=woTypes&source="+source
		var request = {
			//"source" : source,
			//"viewType" : "woTypes"
		};
		callAjaxService("ObfuscationWordsPost", ObfuscationWordsCallback, callBackFailure,
				request, "POST");
	};

	var ObfuscationWordsCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		var deleteDiv = $("#deleteDiv").html();
		if(data !== undefined 
			&& data !== null ){
			var dataTableData = data;
			var dataTable = $(".dc-data-table")
			.DataTable(
					{

						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"lengthChange" : true,
						"bSort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"data" : dataTableData,
						 "bAutoWidth": false,
						"tableTools" : {
							"aButtons" : [ {
								"sExtends" : "collection",
								"sButtonText" : 'Export <span class="caret" />',
								"aButtons" : [ "csv", "pdf" ]
							} ],
							"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
						},
						"destroy" : true,
						"columns" :[{											
							
							"data" : "wordFrom",
							"defaultContent" : ""
						},{

							"data" : "wordTo",
							"defaultContent" : ""
						},{
							"data" : "wordFrom",
							"defaultContent" :"",
							"render" : function(data, type,
									full) {
								return deleteDiv.replace("$","'"+data+"'").replace("#","ObfuscationWords");
							}
						}	],
						"rowCallback" : function(nRow, aData,
								iDisplayIndex, iDisplayIndexFull) {
							$(nRow)
							.attr(
									{
										'onClick' : "javascript:ObfuscationWords.Edit( '"+ aData.wordFrom+ "','"+aData.wordTo+"')",
										'data-target' : '#myModalthredlevel',
										'data-toggle' : 'modal'
									});
						}
					});
		}
	};
	this.CreateView = function(){
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-plus")).append(" Create").unbind('click').bind('click',function(){ObfuscationWords.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("[name=fromword]").removeAttr("readonly");
	};	
	this.Create = function() {
		if (!$("#frmSankeyFilter").valid())
			return;
		var request = {
				"fromword" : $("[name=fromword]").val(),
				"toword" : $("[name=toword]").val(),
		};
		callAjaxService("CreateObfuscationWords", callbackSucessCreateObfuscationWords, callBackFailure,
				request, "POST");
	};
	var callbackSucessCreateObfuscationWords = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Obfuscation value Created Sucessfully..");
		}
	};
	
	THIS.Edit = function(fromword,toword){
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-pencil")).append(" Update").unbind('click').bind('click',function(){ObfuscationWords.Update();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		
		
		$("[name=fromword]").val(fromword).attr("readonly","true");
		$("[name=toword]").val(toword);
	};
	this.Update = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		
		var request = {
				"fromword":$("[name=fromword]").val(),
				"toword": $("[name=toword]").val(),
		};
		callAjaxService("UpdateObfuscationWords",callbackSucessUpdateUser,callBackFailure,request,"POST");
	};
	var callbackSucessUpdateUser = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			THIS.Load();
			showNotification("success","Obfescation property updated Sucessfully..");
		};       
	};
	
	this.Delete = function(event,fromword){
		event.stopPropagation();
		$.confirm({
		    text: "Are you sure you want to delete this Obfuscation?",
		    confirm: function() {
		    	var request = {
						"fromword":fromword 
				};
				callAjaxService("DeleteObfuscationWords",callbackDeleteCreateApp, callBackFailure,
						request, "POST");
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	this.CancelDelete = function(event){
		 event.stopPropagation();
		 $(".hidden-tablet.open").removeClass("open");
		 	};
	var callbackDeleteCreateApp = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "Obfuscation property Deleted Sucessfully..");
		}
	};
	this.SystemPropertiesRefresh = function() {
		callAjaxService("SystemPropertiesRefreshAction", callbackSucessSystemPropertiesRefreshAction, callBackFailure);
	};
	var callbackSucessSystemPropertiesRefreshAction = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "System properties Refreshed Sucessfully.");
		}
	};
	
};
	
ObfuscationWordsInstance();