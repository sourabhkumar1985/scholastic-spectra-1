'use strict';
pageSetUp();
var FinanceReport= null;
var FinanceReportInstance = function() {
	FinanceReport = new FinanceReportObject();
	FinanceReport.Load();
};
var FinanceReportObject = function(){
	var THIS = this;
	$("#btnPrintL3").click(function(e){
		printElement('#poDocView');
	});
	$("#btnSaveL3").click(function(e){
		savePDF('#poDocView');
	});

	this.Load = function(source) {
		enableLoading();
		source = source || "SAP";
		// var request = "viewType=soTypes&source="+source
		var request = {
			"id" : "reportgrid"
		};
		callAjaxService("FinanceReportPost", SOReportPostSuccessCallback, callBackFailure,
				request, "POST");
	};
	
   this.ExportToExcel = function(){
		
	   $.fileDownload('rest/material/materialreportexcel?name='+THIS.name);
	  
	};

	var SOReportPostSuccessCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		var numberFormat2 = d3.format(",.2f");
		if(data !== undefined && data !== null){

			// data = data['aggregatedMaterialReportList'];
			var dataTableData = data.slice(0, 100);
			THIS.dataTableObj = $("#meterialTable")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"sort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : dataTableData,
								 "displayLength":20,
								 "bAutoWidth": false,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"destroy" : true,
								"columns" : [{											
										
											  "data" : "description",
											  "defaultContent" : ""
										   },
										   {
											  "data" : "source",
											  "defaultContent" : ""
										   },
										   {
												data: null,
												className: "center",
												defaultContent: '<a href="" class="report_download">Download</a>'
											}],
											"rowCallback" : function(nRow, aData,
													iDisplayIndex, iDisplayIndexFull) {
												$(nRow).find("td:not(:last-child)").attr({'onClick' : "javascript:FinanceReport.Report( '"+ aData.id+ "','"+aData.name+"')",'data-target' : '#myModalthredlevel','data-toggle' : 'modal'});
												
												if(aData.name == "ERPUNMATCHEDREPORT"){
													$(nRow).find("td:last-child .report_download").hide();
												}else if(aData.name == "EMEA_PO_SSE_RPT"){
													$(nRow).find("td:last-child")
													.attr({'data-target' : '#poReportFilterModal','data-toggle' : 'modal'});
												}else if(aData.name == "EMEA_PPV_RPT"){
													$(nRow).find("td:last-child")
													.attr({'data-target' : '#ppvReportFilterModal','data-toggle' : 'modal'});
												}else if(aData.name == "EMEA_QT_RPT"){
													$(nRow).find("td:last-child .report_download").hide();
												}else if(aData.name == "EMEA_SSE_RPT"){
													$(nRow).find("td:last-child .report_download").hide();
												}else if(aData.name == "EMEA_RPT_VOLUME"){
													$(nRow).find("td:last-child .report_download").hide();
												}
											
											}
							});
			// callAjaxService("ExcelDownload", ExcelDownloadSuccessCallback,
			// callBackFailure,null, "GET");
						
			
		}
	}
	var ExcelDownloadSuccessCallback = function(){
		
	};
	this.Report = function(id,name){
		enableLoading("modalLoading");
		$("#tableDiv").hide();
		THIS.name = name;
		// var request = "viewType=soTypes&source="+source
		var request = {
			"id" : id,
			"name":THIS.name
		};
		
		callAjaxService("FinanceReportPost", FinanceReportSuccessCallback, callBackFailure,
				request, "POST");
	};
	var FinanceReportSuccessCallback = function(response){
		disableLoading("modalLoading");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		$("#statusMsg").html("Showing");
		$("#tableDiv").show();
		/* try{ */
			var colosFunction = [];
			var exportColumn = [];
			var coloum_config = Object.keys(response[0]);	    	
			for ( var i = 0, idx = coloum_config.length; i < idx; i++) {
				
					var fun = new Function('oObj', 'return oObj.aData.'
							+ coloum_config[i].trim());
					var colTitle = lookUpTable.get(THIS.facet+"-"+coloum_config[i].trim());
					if (colTitle == null){
						colTitle = coloum_config[i].trim();
					}
					var showCol = true;
					exportColumn.push(i);
					
					colosFunction.push({
						"data" : coloum_config[i].trim(),
						"defaultContent" : "",
						"sortable" : true,
						"title": colTitle,
						"visible": showCol
					});
				}
			var dataTableData = response.slice(0, 100);
			var facetTable = $("#reportTable")
			.dataTable(
					{
						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"lengthChange" : true,
						"sort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"data" : dataTableData,
						 "autoWidth": false,
						"tableTools" : {
							/*
							 * "aButtons" : [ { "sExtends" : "collection",
							 * "sButtonText" : 'Export <span class="caret" />',
							 * "aButtons" : [ "csv", "pdf" ] } ], "sSwfPath" :
							 * "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							 */
						},
						"destroy" : true,
						"columns" : colosFunction
					});
	/*
	 * } catch(err){
	 *  }
	 */
	};
	$("#ppvReportFilter, #poReportFilter").submit(function(e) {
		e.preventDefault();
	});
	
	$('#poReportFilterModal, #ppvReportFilterModal').on('hidden.bs.modal', function (e) {
	  $(e.currentTarget).find("form").trigger("reset");
	});
	
	$('#ppvReportFilter').submit(
			function(obj) {
				var request = {
						name :"EMEA_PPV_RPT",
						year : $("#ppvYear").val()|| null,
						month: $("#ppvMonth").val()|| null,
						plant : $("#ppvPlant").val()|| null,
						material : $("#ppvMaterial").val()|| null,
						supplier : $("#ppvSupllier").val()|| null,
						ponumber : $("#ppvPONumber").val()|| null,
						invoicenumber : $("#ppvInvoiceNumber").val()|| null
				};
				downloadReport(request);
				$("#ppvReportFilterModal").modal('hide');
			});
	
	$("#poReportFilter").submit(function(obj){
		var request = {
				name : "EMEA_PO_SSE_RPT",
				companycode: $("#poCompanyCode").val()|| null,
				plant : $("#poPlant").val()|| null,
				material : $("#poMaterial").val()|| null,
				supplier : $("#poSupllier").val()|| null,
				ponumber : $("#poPONumber").val()|| null,
		};
		downloadReport(request);
		$("#poReportFilterModal").modal('hide');
	});
	
	
	function downloadReport(request){
		var downloadUrl = "rest/material/emeareportbyfilter";
		var index = 0;
		for(var item in request){
			if(request[item] != null){
				downloadUrl = index==0?downloadUrl + "?":downloadUrl +"&";
				downloadUrl = downloadUrl+item+"="+request[item];
				index++;
			}
		};
		$.fileDownload(downloadUrl);
	}
};

		
FinanceReportInstance();