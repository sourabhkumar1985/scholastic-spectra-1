'use strict';
pageSetUp();
var UserReport;
var UserReportInstance = function() {
	UserReport = new UserReportObject();
	UserReport.Load();
};

var UserReportObject = function() {
	$("#btnPrintL3").click(function(e) {
		printElement('#poDocView');
	});
	$("#btnSaveL3").click(function(e) {
		savePDF('#poDocView');
	});

	$('#myModalthredlevel').on('shown', function() {
		$('[name="username"]').focus();
	});
	
	var traceabilityValues = [ {
		"id" : "smart-style-2",
		"text" : "Default"
	}, {
		"id" : "smart-style-3",
		"text" : "Smart Admin"
	}, {
		"id" : "smart-style-4",
		"text" : "Google Skin"
	}, {
		"id" : "smart-style-1",
		"text" : "Dark Elegance"
	}, {
		"id" : "smart-style-8",
		"text" : "Ultra Light"
	}, {
		"id" : "smart-style-5",
		"text" : "Glass"
	}, {
		"id" : "smart-style-6",
		"text" : "PixelSmash"
	}, {
		"id" : "smart-style-7",
		"text" : "Material Design"
	} ];

	$('#themes').select2({
		data : traceabilityValues
	});
	
	var dateFormat=[{
		"id" : "M/d/yyyy",
		"text" : "M/d/yyyy",
	}, {
		"id" : "M/d/yy",
		"text" : "M/d/yy",
	}, {
		"id" : "MM/dd/yy",
		"text" : "MM/dd/yy",
	}, {
		"id" : "MM/dd/yyyy",
		"text" : "MM/dd/yyyy",
	}, {
		"id" : "yy/MM/dd",
		"text" : "yy/MM/dd",
	},
	{
		"id" : "yyy-MM-dd",
		"text" : "yyy-MM-dd",
	},
	{
		"id" : "dd-MMM-yy",
		"text" : "dd-MMM-yy",
	},
	];
	
	$('#dateId').select2({
		data : dateFormat
	});
	
	var timeZones=[{
	    "name": "Dateline Standard Time",
	    "id": "DST",
	    "offset": -12,
	    "isdst": false,
	    "text": "(UTC-12:00) International Date Line West"
	  },
	  {
	    "name": "UTC-11",
	    "id": "U",
	    "offset": -11,
	    "isdst": false,
	    "text": "(UTC-11:00) Coordinated Universal Time-11"
	  },
	  {
	    "name": "Hawaiian Standard Time",
	    "id": "HST",
	    "offset": -10,
	    "isdst": false,
	    "text": "(UTC-10:00) Hawaii"
	  },
	  {
	    "name": "Alaskan Standard Time",
	    "id": "AKDT",
	    "offset": -8,
	    "isdst": true,
	    "text": "(UTC-09:00) Alaska"
	  },
	  {
	    "name": "Pacific Standard Time (Mexico)",
	    "id": "PDT",
	    "offset": -7,
	    "isdst": true,
	    "text": "(UTC-08:00) Baja California"
	  },
	  {
	    "name": "Pacific Standard Time",
	    "id": "PDT",
	    "offset": -7,
	    "isdst": true,
	    "text": "(UTC-08:00) Pacific Time (US & Canada)"
	  },
	  {
	    "name": "US Mountain Standard Time",
	    "id": "UMST",
	    "offset": -7,
	    "isdst": false,
	    "text": "(UTC-07:00) Arizona"
	  },
	  {
	    "name": "Mountain Standard Time (Mexico)",
	    "id": "MDT",
	    "offset": -6,
	    "isdst": true,
	    "text": "(UTC-07:00) Chihuahua, La Paz, Mazatlan"
	  },
	  {
	    "name": "Mountain Standard Time",
	    "id": "MDT",
	    "offset": -6,
	    "isdst": true,
	    "text": "(UTC-07:00) Mountain Time (US & Canada)"
	  },
	  {
	    "name": "Central America Standard Time",
	    "id": "CAST",
	    "offset": -6,
	    "isdst": false,
	    "text": "(UTC-06:00) Central America"
	  },
	  {
	    "name": "Central Standard Time",
	    "id": "CDT",
	    "offset": -5,
	    "isdst": true,
	    "text": "(UTC-06:00) Central Time (US & Canada)"
	  },
	  {
	    "name": "Central Standard Time (Mexico)",
	    "id": "CDT",
	    "offset": -5,
	    "isdst": true,
	    "text": "(UTC-06:00) Guadalajara, Mexico City, Monterrey"
	  },
	  {
	    "name": "Canada Central Standard Time",
	    "id": "CCST",
	    "offset": -6,
	    "isdst": false,
	    "text": "(UTC-06:00) Saskatchewan"
	  },
	  {
	    "name": "SA Pacific Standard Time",
	    "id": "SPST",
	    "offset": -5,
	    "isdst": false,
	    "text": "(UTC-05:00) Bogota, Lima, Quito"
	  },
	  {
	    "name": "Eastern Standard Time",
	    "id": "EDT",
	    "offset": -4,
	    "isdst": true,
	    "text": "(UTC-05:00) Eastern Time (US & Canada)"
	  },
	  {
	    "name": "US Eastern Standard Time",
	    "id": "UEDT",
	    "offset": -4,
	    "isdst": true,
	    "text": "(UTC-05:00) Indiana (East)"
	  },
	  {
	    "name": "Venezuela Standard Time",
	    "id": "VST",
	    "offset": -4.5,
	    "isdst": false,
	    "text": "(UTC-04:30) Caracas"
	  },
	  {
	    "name": "Paraguay Standard Time",
	    "id": "PST",
	    "offset": -4,
	    "isdst": false,
	    "text": "(UTC-04:00) Asuncion"
	  },
	  {
	    "name": "Atlantic Standard Time",
	    "id": "ADT",
	    "offset": -3,
	    "isdst": true,
	    "text": "(UTC-04:00) Atlantic Time (Canada)"
	  },
	  {
	    "name": "Central Brazilian Standard Time",
	    "id": "CBST",
	    "offset": -4,
	    "isdst": false,
	    "text": "(UTC-04:00) Cuiaba"
	  },
	  {
	    "name": "SA Western Standard Time",
	    "id": "SWST",
	    "offset": -4,
	    "isdst": false,
	    "text": "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan"
	  },
	  {
	    "name": "Pacific SA Standard Time",
	    "id": "PSST",
	    "offset": -4,
	    "isdst": false,
	    "text": "(UTC-04:00) Santiago"
	  },
	  {
	    "name": "Newfoundland Standard Time",
	    "id": "NDT",
	    "offset": -2.5,
	    "isdst": true,
	    "text": "(UTC-03:30) Newfoundland"
	  },
	  {
	    "name": "E. South America Standard Time",
	    "id": "ESAST",
	    "offset": -3,
	    "isdst": false,
	    "text": "(UTC-03:00) Brasilia"
	  },
	  {
	    "name": "Argentina Standard Time",
	    "id": "AST",
	    "offset": -3,
	    "isdst": false,
	    "text": "(UTC-03:00) Buenos Aires"
	  },
	  {
	    "name": "SA Eastern Standard Time",
	    "id": "SEST",
	    "offset": -3,
	    "isdst": false,
	    "text": "(UTC-03:00) Cayenne, Fortaleza"
	  },
	  {
	    "name": "Greenland Standard Time",
	    "id": "GDT",
	    "offset": -2,
	    "isdst": true,
	    "text": "(UTC-03:00) Greenland"
	  },
	  {
	    "name": "Montevideo Standard Time",
	    "id": "MST",
	    "offset": -3,
	    "isdst": false,
	    "text": "(UTC-03:00) Montevideo"
	  },
	  {
	    "name": "Bahia Standard Time",
	    "id": "BST",
	    "offset": -3,
	    "isdst": false,
	    "text": "(UTC-03:00) Salvador"
	  },
	  {
	    "name": "UTC-02",
	    "id": "U",
	    "offset": -2,
	    "isdst": false,
	    "text": "(UTC-02:00) Coordinated Universal Time-02"
	  },
	  {
	    "name": "Mid-Atlantic Standard Time",
	    "id": "MDT",
	    "offset": -1,
	    "isdst": true,
	    "text": "(UTC-02:00) Mid-Atlantic - Old"
	  },
	  {
	    "name": "Azores Standard Time",
	    "id": "ADT",
	    "offset": 0,
	    "isdst": true,
	    "text": "(UTC-01:00) Azores"
	  },
	  {
	    "name": "Cape Verde Standard Time",
	    "id": "CVST",
	    "offset": -1,
	    "isdst": false,
	    "text": "(UTC-01:00) Cape Verde Is."
	  },
	  {
	    "name": "Morocco Standard Time",
	    "id": "MDT",
	    "offset": 1,
	    "isdst": true,
	    "text": "(UTC) Casablanca"
	  },
	  {
	    "name": "UTC",
	    "id": "CUT",
	    "offset": 0,
	    "isdst": false,
	    "text": "(UTC) Coordinated Universal Time"
	  },
	  {
	    "name": "GMT Standard Time",
	    "id": "GDT",
	    "offset": 1,
	    "isdst": true,
	    "text": "(UTC) Dublin, Edinburgh, Lisbon, London"
	  },
	  {
	    "name": "Greenwich Standard Time",
	    "id": "GST",
	    "offset": 0,
	    "isdst": false,
	    "text": "(UTC) Monrovia, Reykjavik"
	  },
	  {
	    "name": "W. Europe Standard Time",
	    "id": "WEDT",
	    "offset": 2,
	    "isdst": true,
	    "text": "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"
	  },
	  {
	    "name": "Central Europe Standard Time",
	    "id": "CEDT",
	    "offset": 2,
	    "isdst": true,
	    "text": "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"
	  },
	  {
	    "name": "Romance Standard Time",
	    "id": "RDT",
	    "offset": 2,
	    "isdst": true,
	    "text": "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris"
	  },
	  {
	    "name": "Central European Standard Time",
	    "id": "CEDT",
	    "offset": 2,
	    "isdst": true,
	    "text": "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb"
	  },
	  {
	    "name": "W. Central Africa Standard Time",
	    "id": "WCAST",
	    "offset": 1,
	    "isdst": false,
	    "text": "(UTC+01:00) West Central Africa"
	  },
	  {
	    "name": "Namibia Standard Time",
	    "id": "NST",
	    "offset": 1,
	    "isdst": false,
	    "text": "(UTC+01:00) Windhoek"
	  },
	  {
	    "name": "GTB Standard Time",
	    "id": "GDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) Athens, Bucharest"
	  },
	  {
	    "name": "Middle East Standard Time",
	    "id": "MEDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) Beirut"
	  },
	  {
	    "name": "Egypt Standard Time",
	    "id": "EST",
	    "offset": 2,
	    "isdst": false,
	    "text": "(UTC+02:00) Cairo"
	  },
	  {
	    "name": "Syria Standard Time",
	    "id": "SDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) Damascus"
	  },
	  {
	    "name": "E. Europe Standard Time",
	    "id": "EEDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) E. Europe"
	  },
	  {
	    "name": "South Africa Standard Time",
	    "id": "SAST",
	    "offset": 2,
	    "isdst": false,
	    "text": "(UTC+02:00) Harare, Pretoria"
	  },
	  {
	    "name": "FLE Standard Time",
	    "id": "FDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"
	  },
	  {
	    "name": "Turkey Standard Time",
	    "id": "TDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) Istanbul"
	  },
	  {
	    "name": "Israel Standard Time",
	    "id": "JDT",
	    "offset": 3,
	    "isdst": true,
	    "text": "(UTC+02:00) Jerusalem"
	  },
	  {
	    "name": "Libya Standard Time",
	    "id": "LST",
	    "offset": 2,
	    "isdst": false,
	    "text": "(UTC+02:00) Tripoli"
	  },
	  {
	    "name": "Jordan Standard Time",
	    "id": "JST",
	    "offset": 3,
	    "isdst": false,
	    "text": "(UTC+03:00) Amman"
	  },
	  {
	    "name": "Arabic Standard Time",
	    "id": "AST",
	    "offset": 3,
	    "isdst": false,
	    "text": "(UTC+03:00) Baghdad"
	  },
	  {
	    "name": "Kaliningrad Standard Time",
	    "id": "KST",
	    "offset": 3,
	    "isdst": false,
	    "text": "(UTC+03:00) Kaliningrad, Minsk"
	  },
	  {
	    "name": "Arab Standard Time",
	    "id": "AST",
	    "offset": 3,
	    "isdst": false,
	    "text": "(UTC+03:00) Kuwait, Riyadh"
	  },
	  {
	    "name": "E. Africa Standard Time",
	    "id": "EAST",
	    "offset": 3,
	    "isdst": false,
	    "text": "(UTC+03:00) Nairobi"
	  },
	  {
	    "name": "Iran Standard Time",
	    "id": "IDT",
	    "offset": 4.5,
	    "isdst": true,
	    "text": "(UTC+03:30) Tehran"
	  },
	  {
	    "name": "Arabian Standard Time",
	    "id": "AST",
	    "offset": 4,
	    "isdst": false,
	    "text": "(UTC+04:00) Abu Dhabi, Muscat"
	  },
	  {
	    "name": "Azerbaijan Standard Time",
	    "id": "ADT",
	    "offset": 5,
	    "isdst": true,
	    "text": "(UTC+04:00) Baku"
	  },
	  {
	    "name": "Russian Standard Time",
	    "id": "RST",
	    "offset": 4,
	    "isdst": false,
	    "text": "(UTC+04:00) Moscow, St. Petersburg, Volgograd"
	  },
	  {
	    "name": "Mauritius Standard Time",
	    "id": "MST",
	    "offset": 4,
	    "isdst": false,
	    "text": "(UTC+04:00) Port Louis"
	  },
	  {
	    "name": "Georgian Standard Time",
	    "id": "GST",
	    "offset": 4,
	    "isdst": false,
	    "text": "(UTC+04:00) Tbilisi"
	  },
	  {
	    "name": "Caucasus Standard Time",
	    "id": "CST",
	    "offset": 4,
	    "isdst": false,
	    "text": "(UTC+04:00) Yerevan"
	  },
	  {
	    "name": "Afghanistan Standard Time",
	    "id": "AST",
	    "offset": 4.5,
	    "isdst": false,
	    "text": "(UTC+04:30) Kabul"
	  },
	  {
	    "name": "West Asia Standard Time",
	    "id": "WAST",
	    "offset": 5,
	    "isdst": false,
	    "text": "(UTC+05:00) Ashgabat, Tashkent"
	  },
	  {
	    "name": "Pakistan Standard Time",
	    "id": "PST",
	    "offset": 5,
	    "isdst": false,
	    "text": "(UTC+05:00) Islamabad, Karachi"
	  },
	  {
	    "name": "India Standard Time",
	    "id": "IST",
	    "offset": 5.5,
	    "isdst": false,
	    "text": "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi"
	  },
	  {
	    "name": "Sri Lanka Standard Time",
	    "id": "SLST",
	    "offset": 5.5,
	    "isdst": false,
	    "text": "(UTC+05:30) Sri Jayawardenepura"
	  },
	  {
	    "name": "Nepal Standard Time",
	    "id": "NST",
	    "offset": 5.75,
	    "isdst": false,
	    "text": "(UTC+05:45) Kathmandu"
	  },
	  {
	    "name": "Central Asia Standard Time",
	    "id": "CAST",
	    "offset": 6,
	    "isdst": false,
	    "text": "(UTC+06:00) Astana"
	  },
	  {
	    "name": "Bangladesh Standard Time",
	    "id": "BST",
	    "offset": 6,
	    "isdst": false,
	    "text": "(UTC+06:00) Dhaka"
	  },
	  {
	    "name": "Ekaterinburg Standard Time",
	    "id": "EST",
	    "offset": 6,
	    "isdst": false,
	    "text": "(UTC+06:00) Ekaterinburg"
	  },
	  {
	    "name": "Myanmar Standard Time",
	    "id": "MST",
	    "offset": 6.5,
	    "isdst": false,
	    "text": "(UTC+06:30) Yangon (Rangoon)"
	  },
	  {
	    "name": "SE Asia Standard Time",
	    "id": "SAST",
	    "offset": 7,
	    "isdst": false,
	    "text": "(UTC+07:00) Bangkok, Hanoi, Jakarta"
	  },
	  {
	    "name": "N. Central Asia Standard Time",
	    "id": "NCAST",
	    "offset": 7,
	    "isdst": false,
	    "text": "(UTC+07:00) Novosibirsk"
	  },
	  {
	    "name": "China Standard Time",
	    "id": "CST",
	    "offset": 8,
	    "isdst": false,
	    "text": "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi"
	  },
	  {
	    "name": "North Asia Standard Time",
	    "id": "NAST",
	    "offset": 8,
	    "isdst": false,
	    "text": "(UTC+08:00) Krasnoyarsk"
	  },
	  {
	    "name": "Singapore Standard Time",
	    "id": "MPST",
	    "offset": 8,
	    "isdst": false,
	    "text": "(UTC+08:00) Kuala Lumpur, Singapore"
	  },
	  {
	    "name": "W. Australia Standard Time",
	    "id": "WAST",
	    "offset": 8,
	    "isdst": false,
	    "text": "(UTC+08:00) Perth"
	  },
	  {
	    "name": "Taipei Standard Time",
	    "id": "TST",
	    "offset": 8,
	    "isdst": false,
	    "text": "(UTC+08:00) Taipei"
	  },
	  {
	    "name": "Ulaanbaatar Standard Time",
	    "id": "UST",
	    "offset": 8,
	    "isdst": false,
	    "text": "(UTC+08:00) Ulaanbaatar"
	  },
	  {
	    "name": "North Asia East Standard Time",
	    "id": "NAEST",
	    "offset": 9,
	    "isdst": false,
	    "text": "(UTC+09:00) Irkutsk"
	  },
	  {
	    "name": "Tokyo Standard Time",
	    "id": "TST",
	    "offset": 9,
	    "isdst": false,
	    "text": "(UTC+09:00) Osaka, Sapporo, Tokyo"
	  },
	  {
	    "name": "Korea Standard Time",
	    "id": "KST",
	    "offset": 9,
	    "isdst": false,
	    "text": "(UTC+09:00) Seoul"
	  },
	  {
	    "name": "Cen. Australia Standard Time",
	    "id": "CAST",
	    "offset": 9.5,
	    "isdst": false,
	    "text": "(UTC+09:30) Adelaide"
	  },
	  {
	    "name": "AUS Central Standard Time",
	    "id": "ACST",
	    "offset": 9.5,
	    "isdst": false,
	    "text": "(UTC+09:30) Darwin"
	  },
	  {
	    "name": "E. Australia Standard Time",
	    "id": "EAST",
	    "offset": 10,
	    "isdst": false,
	    "text": "(UTC+10:00) Brisbane"
	  },
	  {
	    "name": "AUS Eastern Standard Time",
	    "id": "AEST",
	    "offset": 10,
	    "isdst": false,
	    "text": "(UTC+10:00) Canberra, Melbourne, Sydney"
	  },
	  {
	    "name": "West Pacific Standard Time",
	    "id": "WPST",
	    "offset": 10,
	    "isdst": false,
	    "text": "(UTC+10:00) Guam, Port Moresby"
	  },
	  {
	    "name": "Tasmania Standard Time",
	    "id": "TST",
	    "offset": 10,
	    "isdst": false,
	    "text": "(UTC+10:00) Hobart"
	  },
	  {
	    "name": "Yakutsk Standard Time",
	    "id": "YST",
	    "offset": 10,
	    "isdst": false,
	    "text": "(UTC+10:00) Yakutsk"
	  },
	  {
	    "name": "Central Pacific Standard Time",
	    "id": "CPST",
	    "offset": 11,
	    "isdst": false,
	    "text": "(UTC+11:00) Solomon Is., New Caledonia"
	  },
	  {
	    "name": "Vladivostok Standard Time",
	    "id": "VST",
	    "offset": 11,
	    "isdst": false,
	    "text": "(UTC+11:00) Vladivostok"
	  },
	  {
	    "name": "New Zealand Standard Time",
	    "id": "NZST",
	    "offset": 12,
	    "isdst": false,
	    "text": "(UTC+12:00) Auckland, Wellington"
	  },
	  {
	    "name": "UTC+12",
	    "id": "U",
	    "offset": 12,
	    "isdst": false,
	    "text": "(UTC+12:00) Coordinated Universal Time+12"
	  },
	  {
	    "name": "Fiji Standard Time",
	    "id": "FST",
	    "offset": 12,
	    "isdst": false,
	    "text": "(UTC+12:00) Fiji"
	  },
	  {
	    "name": "Magadan Standard Time",
	    "id": "MST",
	    "offset": 12,
	    "isdst": false,
	    "text": "(UTC+12:00) Magadan"
	  },
	  {
	    "name": "Kamchatka Standard Time",
	    "id": "KDT",
	    "offset": 13,
	    "isdst": true,
	    "text": "(UTC+12:00) Petropavlovsk-Kamchatsky - Old"
	  },
	  {
	    "name": "Tonga Standard Time",
	    "id": "TST",
	    "offset": 13,
	    "isdst": false,
	    "text": "(UTC+13:00) Nuku'alofa"
	  },
	  {
	    "name": "Samoa Standard Time",
	    "id": "SST",
	    "offset": 13,
	    "isdst": false,
	    "text": "(UTC+13:00) Samoa"
	  }
	];
	
	var region = [{
	    "name": "ASPAC",
	    "id": "ASPAC",
	    "offset": -12,
	    "isdst": false,
	    "text": "ASPAC"
	  },
	  {
	    "name": "EMEA",
	    "id": "EMEA",
	    "offset": -11,
	    "isdst": false,
	    "text": "EMEA"
	  },
	  {
	    "name": "LATAM",
	    "id": "LATAM",
	    "offset": -10,
	    "isdst": false,
	    "text": "LATAM"
	  }];
	
	$('#region').select2({
		data : region
	});
	
	$('#timeZoneId').select2({
		data : timeZones
	});


	var THIS = this;
	$("[name^=password]").show();
	$("#btnCreate").unbind("click").bind("click", function() {
		UserReport.Create();
	});

	$("#frmSankeyFilter").submit(function(e) {
		e.preventDefault();
	});
	
	$("#email-form").submit(function(e) {
		e.preventDefault();
	});
	
	$("#frmSankeyFilter").validate({
		rules : {
			username : {
				required : true
			},
			password : {
				required : true,
				minlength : 3,
				maxlength : 20
			},
			passwordConfirm : {
				required : true,
				minlength : 3,
				maxlength : 20,
				equalTo : '#password'
			},
			type : {
				required : true
			},
			emailId:{
				required: true,
                email: true
                
			},
			firstName:{
				required:true
			},
			lastName:{
				required:true
			}
		},
		messages : {
			login : {
				required : 'Please enter your login'
			},
			password : {
				required : 'Please enter your password'
			},
			passwordConfirm : {
				required : 'Please enter your password one more time',
				equalTo : 'Please enter the same password as above'
			},
			type : {
				required : 'Please select your Role'
			},
			emailId:{
				required : 'Please enter your Emai lId',
				email: 'Enter the valid Email Address'				
			},
			firstName:{
				required : 'Please enter your First Name'
			},
			lastName:{
				required : 'Please enter your Last Name'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	

	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		var request = {
			"source" : source,
			"viewType" : "userTypes"
		};
		callAjaxService("UserReportPost", UserReportPostSuccessCallback,
				callBackFailure, request, "GET");
	};

	var UserReportPostSuccessCallback = function(data) {
		disableLoading();
		if (data && data.isException) {
			showNotification("error", data.customMessage);
			return;
		}
		if (data !== undefined && data !== null) {
			var dataTable = $("#userTable")
					.DataTable(
							{
								"dom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
										+ "t"
										+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"sort" : true,
								"info" : true,
								"jQueryUI" : false,
								"stateSave" : true,
								"data" : data,/*
												 * , "tableTools" : { "aButtons" : [ {
												 * "sExtends" : "collection",
												 * "sButtonText" : 'Export <span
												 * class="caret" />', "aButtons" : [
												 * "csv", "pdf" ] } ],
												 * "sSwfPath" :
												 * "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf" },
												 */
								"destroy" : true,
								"columns" : [
								          {
								            "data" : "notification",
											"defaultContent" : "",
											"title" : "Select User",
											"render" : function(data, type,
											 full) {
												return '<label class="checkbox" > <input type="checkbox" data-email="'+full.email+'" data-type="mailUser" name="selectUser" data-id="'+ full.username +'" class="selectedUser"> <i> </i> </label>';
											}
						            	 
								             },
								             
										{

											"data" : "username",
											"defaultContent" : "",
											"title" : "User name"
										},
										{

											"data" : "firstName",
											"defaultContent" : "",
											"title" : "First Name"
										},

										{

											"data" : "lastName",
											"defaultContent" : "",
											"title" : "Last Name"
										},
     
										/*{

											"data" : "roles",
											"defaultContent" : "",
											"title" : "User role",
											"render" : function(data, type,
													full) {
												var i = 0, roleStr = '';
												for (i = 0; i < data.length; i++) {
													if (i < (data.length - 1))
														roleStr = roleStr
																+ data[i]
																+ ', ';
													else
														roleStr = roleStr
																+ data[i];
												}
												return roleStr;
											},
										},
											 * {
											 * 
											 * "data" : "roles",
											 * "defaultContent" : "" },{
											 * 
											 * "data" :
											 * "relatedWorkOrderTypeDesc",
											 * "defaultContent" : "" },
											 */
										 /*{
											"data" : "menuOnTop",
											"defaultContent" : "",
											"title" : "Menu On Top"
										},*/ {
											"data" : "primaryRole",
											"defaultContent" : "",
											"title" : "Primary Role"
										}, /*{
											"data" : "businessrole",
											"defaultContent" : "",
											"title" : "Business Role"
										},*/
										{
											"data" : "enabled",
											"defaultContent" : "",
											"title" : "Enabled",
											"render" : function(data) {
												if (data)
													return '<span class="label label-success btn-sm">Active</span>';

												return '<span class="label label-danger btn-sm">Disabled</span>';
											}
										}
										 				
										],
							
							"rowCallback" : function(nRow, aData) { 
									$(nRow)
											.attr(
													{   
														'onClick' : "javascript:UserReport.Edit(event,'"
																+ aData.username
																+ "')",
														
													});
								},
								
														/*
							 * "fnRowCallback" : function(nRow, aData,
							 * iDisplayIndex, iDisplayIndexFull) };
							 */
							});
			
			callAjaxService("GetAllRoles", callBackSuccessGetRole,callBackFailure, null, "POST");			
		}
	};
	var getBusinessRole = function(){
		var businessRoleRequest = {
				/*source : "postgreSQL",
				database : "E2EMFPostGres",
				table:"business_role",
				columns:"name as rolename"*/
				source : "postgreSQL",
				database : "E2EMFPostGres",
				table:"role_app_mapping",
				columns:"role_id,role,apps"
		};
		callAjaxService("getData", callBackSuccessGetBusinessRole,callBackFailure, businessRoleRequest, "POST");
	};
	var callBackSuccessGetBusinessRole = function(response){
		/*var businessRoles = [];
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response && response.data && response.data.length > 0 ) {
			for(var i=0;i<response.data.length;i++){
				businessRoles.push({
					"id" : response.data[i].rolename,
					"text" : response.data[i].rolename
				});
			}
			
			$('#businessRole').select2({
				placeholder : "Choose Business Role",
				data : businessRoles
			});
		}*/
		THIS.Apps = []
		var app = [];
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response && response.data && response.data.length > 0 ) {
			for(var i=0;i<response.data.length;i++){
				app.push({
					"id" : response.data[i].role,
					"text" : response.data[i].role
				});
			}
			THIS.Apps = response
			$('#businessRole').select2({
				placeholder : "Apps",
				data : app,
				multiple : false
			});
			
			$('#businessRole').unbind('change').bind('change',function() {
				$("#type").select2('val', null);
				var AllApps = THIS.Apps
				var selectedApps=[]
				var selectedList = [];
				var selectedRole =  $('#businessRole').select2('val')
				var secRole = THIS.Roles;
					for(var y=0;y<AllApps.data.length;y++){
						if(selectedRole && selectedRole==AllApps.data[y].role){
							 selectedApps =AllApps.data[y].apps
							var selApps=selectedApps.split(',')
							if(selApps){
								for(var z=0; z<selApps.length; z++){
									if(!(jQuery.inArray(selApps[z],selectedList)>=0))
									selectedList.push(selApps[z])
								}
								}
						}
						
					
				}
				
				 var displayApps=[] 
				for(var i=0;i<selectedList.length;i++)
				{
				   for(var j=0;j<THIS.Roles.length;j++){
					   if(selectedList[i]==THIS.Roles[j].text){
						  displayApps.push(THIS.Roles[j])
					   }
				   }	
				}
				
				$('#type').select2({
					placeholder : "Apps",
					data : displayApps,
					multiple : true
				});
			    	
			});
		}
		
	};
	/*var getApp = function(){ 
		var appRequest = {
				source : "postgreSQL",
				database : "E2EMFPostGres",
				table:"role_app_mapping",
				columns:"role_id,role,apps,access_level"
		};
		callAjaxService("getData", callBackSuccessGetApp,callBackFailure, appRequest, "POST");
	};
	var callBackSuccessGetApp = function(response){
		THIS.Apps = []
		var app = [];
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response && response.data && response.data.length > 0 ) {
			for(var i=0;i<response.data.length;i++){
				app.push({
					"id" : response.data[i].role,
					"text" : response.data[i].role
				});
			}
			THIS.Apps = response
			$('#businessRole').select2({
				placeholder : "Apps",
				data : app,
				multiple : true
			});
			$('#businessRole').unbind('change').bind('change',function() {
				var AllApps = THIS.Apps
				var selectedApps=[]
				var selectedList = [];
				var selectedRole =  $('#businessRole').select2('data')
				var secRole = THIS.Roles;
				
				for(var x=0;x<selectedRole.length;x++)
				{
					for(var y=0;y<AllApps.data.length;y++){
						if(selectedRole[x].id==AllApps.data[y].role){
							 selectedApps =AllApps.data[y].apps
							var selApps=selectedApps.split(',')
							if(selApps){
								for(var z=0; z<selApps.length; z++){
									if(!(jQuery.inArray(selApps[z],selectedList)>=0))
									selectedList.push(selApps[z])
								}
								}
						}
						
					}
				}
				
				 var displayApps=[] 
				for(var i=0;i<selectedList.length;i++)
				{
				   for(var j=0;j<THIS.Roles.length;j++){
					   if(selectedList[i]==THIS.Roles[j].text){
						  displayApps.push(THIS.Roles[j])
					   }
				   }	
				}
				
				$('#type').select2({
					placeholder : "Apps",
					data : displayApps,
					multiple : true
				});
			    	
			});
		}
	};*/
	var callBackSuccessGetRole = function(data) {
		THIS.Roles = new Array();
		disableLoading();
		getBusinessRole();
		/*getApp();*/
		if (data && data.isException) {
			showNotification("error", data.customMessage);
			return;
		}
		if (data !== undefined && data !== null) { 
			for (var i = 0; i < data.length; i++) {
				if(data[i].displayOrder && data[i].displayOrder != "0"){
					THIS.Roles.push({
						"id" : data[i].name,
						"text" : data[i].displayName
					});
				}
			}
			var secondaryRoles = THIS.Roles; 
			if((lookUpTable.get('qfindPrimaryRole') && localStorage.getItem('lastusedapp') === 'INTEGRA_ADMIN') && (lookUpTable.get('ShowPrimaryRole') != null && lookUpTable.get('ShowPrimaryRole').toLowerCase() === 'false')) {
				secondaryRoles = [];
				THIS.Roles.map(function(role){
					if(role.id !== lookUpTable.get('qfindPrimaryRole')) {
						secondaryRoles.push(role);
					}
				});
			}
			$("#type").select2({
				placeholder : "Choose Roles",
				data : secondaryRoles,
				multiple : true,
			});
			/*$("#app").select2({
				placeholder : "Apps",
				data : secondaryRoles,
				multiple : true,
			});*/
			$('#primaryRole').select2({
				data : THIS.Roles
			});
		}
	};                        
	
	this.sendCredentialsToUser=function(){
		if(selectedUserId.length!=0){
			$('#myModalEmail').modal('show');
			$("#setEmailNotification").html("Send Mail").unbind('click').bind('click',
					function() {
						UserReport.sendCredentials();
					});
		 }
		else{
			showNotification("warning", "please select user first");
	    }
	 };
	
	
	setTimeout(function(){ 
		CKEDITOR.replace( 'message', {  
			extraPlugins: 'base64image',
			removeButtons: 'Image',
			// Upload images to a CKFinder connector (note that the response type is set to JSON).
			uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
			startupFocus : true,
			enterMode : CKEDITOR.ENTER_BR,
	        shiftEnterMode: CKEDITOR.ENTER_P,
	        toolbarStartupExpanded : false,
	        toolbarCanCollapse  : true
			} ); 
		}, 1000);
	
	this.sendCredentials=function(){
         if(CKEDITOR.instances['message'].getData() === ""){
        	 showNotification("info", "Message can't be empty");
        	 return;
         }
         var message = CKEDITOR.instances['message'].getData();
         var request = {
        		 "username": selectedUserId.toString(),
        		 "message" : message,
        		 "subject" : $("[name=subject]").val(),
         };
		callAjaxService("Notification", responseForSentMail, callBackFailure, request, "POST");	
	};
	
	var responseForSentMail=function(response){
		if (response && response.isException) {
		showNotification("error", response.customMessage);
		return;
	     }
	    else{
			showNotification("success", "Mail has been sent sucessfully");

	     }
	   };

	this.CreateView = function() {
		$("#searchForm").submit(function(e) {
			e.preventDefault();
		});
		$("#myModalthredlevel").find("h4").html("Create User");
		$("#smart-form-register input").val("");
		$("[name=username]").removeAttr("readonly");
		$("#type").select2("val", null);
		$("#businessRole").select2("val", null);
		$("#themes").select2("val", null);
		if(lookUpTable.get('qfindPrimaryRole') && localStorage.getItem('lastusedapp') === 'INTEGRA_ADMIN') {
			$("#primaryRole").select2("val", lookUpTable.get('qfindPrimaryRole'));
		} else {
			$("#primaryRole").select2("val", null);
		}
		$("#btnCreate").html("Create").unbind('click').bind('click',
				function() {
					UserReport.Create();
				});
		$("[name^=password]").show();
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		if(lookUpTable.get('ShowPrimaryRole') != null && lookUpTable.get('ShowPrimaryRole').toLowerCase() === 'false') {
			$("#primaryRoleSection").hide();
		} else{
			$("#primaryRoleSection").show();
		}
	};
	this.Create = function() {
		if (!$("#frmSankeyFilter").valid())
			return;
		var enable = 0;
		var menuOnTop = 0;
		if ($("#userEnabled").is(':checked'))
			enable = 1;
		if ($("#menuOnTop").is(':checked'))
			menuOnTop = 1;
		var roleArray = $("#type").select2('val');
		var roles = '';
		for (var i = 0; i < roleArray.length; i++) {
			if (i != 0)
				roles += "," + roleArray[i];
			else
				roles += roleArray[i];
		}
		if((lookUpTable.get('qfindPrimaryRole') && localStorage.getItem('lastusedapp') === 'INTEGRA_ADMIN') &&  roleArray.indexOf(lookUpTable.get('qfindPrimaryRole')) === -1) {
			roles += ',' + lookUpTable.get('qfindPrimaryRole');
		}
		var themeName = '';
		if ($("#themes").select2("val") != "")
			themeName = $("#themes").select2("val");
		
		var timezone = '';
		if ($("#timeZoneId").select2("val") != "")
			timezone = $("#timeZoneId").select2("val");
		
		var datename = '';
		if ($("#dateId").select2("val") != "")
			datename = $("#dateId").select2("val");
		var primaryRole = '';
		if ($("#primaryRole").select2("val") != "")
			primaryRole = $("#primaryRole").select2("val");
		else
			primaryRole = "ROLE_CELGENE";
		var region = '';
		if ($("#region").select2("val") != "")
			region = $("#region").select2("val");
		var business_role_Array = $("#businessRole").select2('val');
		var business_role = '';
		for (var i = 0; i < business_role_Array.length; i++) {
			if (i != 0)
				business_role += "," + business_role_Array[i];
			else
				business_role += business_role_Array[i];
		}
		var request = {
			"username" : $("[name=username]").val(),
			"password" : $("[name=username]").val(),
			"firstname": $("[name=firstName]").val(),
			"lastname": $("[name=lastName]").val(),
			"roles" : roles,
			"themename" : themeName,
			"enabled" : enable,
			"menuontop" : menuOnTop,
			"primaryrole" : primaryRole,
			"emailId" : $("[name=emailId]").val(),
			"timezone": timezone,
			"datename":datename,
			"status" : "Approved",
			"remarks" : $("[name=remarks]").val(),
			"region" : region
		};
		if($("#businessRole").select2("val") != "" ){
			request.businessrole = business_role;
		}
		callAjaxService("CreateUser", callbackSucessCreateUser,
				callBackFailure, request, "POST");
	};
	var callbackSucessCreateUser = function(response) {
		if (response && response.isException) {
			showNotification("error", response.message);
			return;
		} else {
			callGoogleAnalytics();
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val", null);
			THIS.Load();
			showNotification("success", "User created Sucessfully..");
		}
	};
	
    var selectedUserId=[];
	this.Edit = function(e, username) { 
		if (e.target.nodeName === "INPUT" && event.target.type == 'checkbox' && $(event.target).attr('data-type') === 'mailUser') {
			if( $(event.target).attr('data-email') === "undefined" || $(event.target).attr('data-email') === "null")
			{
				e.preventDefault();
				showNotification("error", "This User Doesn't have Email Id");
				return;
			}

			if ($(event.target).is(':checked'))
			{
				selectedUserId.push($(event.target).attr("data-id"));
			}
			else
			{
				selectedUserId.splice(selectedUserId.indexOf($(event.target).attr("data-id")), 1); 
			}
		} else if (e.target.nodeName == "TD") {
			$("#myModalthredlevel").modal("show");
			$("#myModalthredlevel").find("h4").html("Update User");
			var request = {
				"username" : username
			 };

			callAjaxService("GetUser", function(response) {
				callbackSucessGetUser(response);
			}, callBackFailure, request, "POST");
		 }
	};
	
var callbackSucessGetUser = function(response) {
		$("#smart-form-register input").val("");
		$("#type").select2("val", null);
		$('select[name=themeName]').select2("val", null);
		$("#btnCreate").html("Update").unbind('click').bind('click',
				function() {
					UserReport.Update();
				});
		if(lookUpTable.get('ShowPrimaryRole') != null && lookUpTable.get('ShowPrimaryRole').toLowerCase() === 'false') {
			$("#primaryRoleSection").hide();
		} else{
			$("#primaryRoleSection").show();
		}
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		var roles = '';
		for (var i = 0; i < response.roles.length; i++) {
			if (i != 0)
				roles += "," + response.roles[i];
			else
				roles += response.roles[i];
		}
		/*if(response.businessrole){
			$("#businessRole").select2("val",response.businessrole);
		}else{
			$("#businessRole").select2("val","");
		}*/
		var businessrole=response.businessrole.split(',');
		if(businessrole){
		 $("#businessRole").select2('val', businessrole);
		}
		
		if (response.enabled == 1)
			$("#userEnabled").attr("checked", "true");
		else
			$("#userEnabled").removeAttr("checked");

		if (response.menuOnTop == 1)
			$("#menuOnTop").attr("checked", "true");
		else
			$("#menuOnTop").removeAttr("checked");
		$("#type").select2('val', response.roles);
		$("#region").select2('val', response.region);
		
		
		$("[name=username]").val(response.username).attr("readonly", "true");
		$("[name^=password]").hide();
		$("[name=firstName]").val(response.firstName);
		$("[name=remarks]").val(response.remarks);
		$("[name=lastName]").val(response.lastName);
		$("[name=emailId]").val(response.email);
		$("#themes").select2("val", $.trim(response.themeName));		
		$("#primaryRole").select2("val", $.trim(response.primaryRole));
		$("#timeZoneId").select2("val", $.trim(response.timezone));
		$("#dateId").select2("val", $.trim(response.dateformat));
	};

	this.Update = function() {
		if (!$("#frmSankeyFilter").valid())
			return;
		var enable = 0;
		if ($("#userEnabled").is(':checked'))
			enable = 1;
		var menuOnTop = 0;
		if ($("#menuOnTop").is(':checked'))
			menuOnTop = 1;
		var roleArray = $("#type").select2('val');
		var roles = '';
		for (var i = 0; i < roleArray.length; i++) {
			if (i != 0)
				roles += "," + roleArray[i];
			else
				roles += roleArray[i];
		}
		if((lookUpTable.get('qfindPrimaryRole') && localStorage.getItem('lastusedapp') === 'INTEGRA_ADMIN') &&  roleArray.indexOf(lookUpTable.get('qfindPrimaryRole')) === -1) {
			roles += ',' + lookUpTable.get('qfindPrimaryRole');
		}
		var primaryRole = $("#primaryRole").select2("val");
		
		var timezone = '';
		if ($("#timeZoneId").select2("val") != "")
			timezone = $("#timeZoneId").select2("val");
		
		var datename = '';
		if ($("#dateId").select2("val") != "")
			datename = $("#dateId").select2("val");
		
		var themeName = '';
		if ($("#themes").select2("val") != "")
			themeName = $("#themes").select2("val");
		
		var region = '';
		if ($("#region").select2("val") != "")
			region = $("#region").select2("val");
		var business_role = $("#businessRole").select2('val');
//		var business_role = '';
//		for (var i = 0; i < business_role_Array.length; i++) {
//			if (i != 0)
//				business_role += "," + business_role_Array[i];
//			else
//				business_role += business_role_Array[i];
//		}
		
		var request = {
			"username" : $("[name=username]").val(),
			"roles" : roles,
			"themename" : themeName,
			"enabled" : enable,
			"menuontop" : menuOnTop,
			"primaryrole" : primaryRole,
			"firstname" : $("[name=firstName]").val(),
		    "lastname" : $("[name=lastName]").val(),
		    "emailId" : $("[name=emailId]").val(),
		    "timezone": timezone,
		    "datename" : datename,
		    "status" : "Approved",
			"remarks" : $("[name=remarks]").val(),
			"region" : region
		
		};
		if($("#businessRole").select2("val") != "" ){
			request.businessrole = business_role ;
		}
		callAjaxService("UpdateUser", callbackSucessUpdateUser,
				callBackFailure, request, "POST");
	};
	var callbackSucessUpdateUser = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			callGoogleAnalytics();
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val", null);
			THIS.Load();
			showNotification("success", "User updated Sucessfully..");
		}
		;
	};
};
UserReportInstance();