'use strict';
pageSetUp();
var AppProperties;
var AppPropertiesInstance = function() {
	AppProperties = new AppPropertiesObject();
	AppProperties.Load();
};
var AppPropertiesObject = function(){
	var THIS = this;
	$("#btnCreate").unbind('click').bind('click', function() {
		AppProperties.Create();
	});
	 var typeValues = [ {
			"id" : "hive",
			"text" : "Hive"
		}, {
			"id" : "postgres",
			"text" : "PostgreSQL"
		},{
			"id" : "solr",
			"text" : "Solr"
		},{
			"id" : "mongo",
			"text" : "Mongo"
		} ];
	 $('#type').select2({
			data : typeValues
		});
	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			name : {
				required : true
			},
			value : {
				required: true
			},
			
		},
		// Messages for form validation
		messages : {
			login : {
				name : 'Please enter name'
			},
			value : {
				required : 'Please enter value'
			},
			
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSankeyFilter").submit(function(e) {
	    e.preventDefault();
	});
		
	
	this.Load = function(source) {
		enableLoading();
	//	source = source || "JDE";
		//var request = "viewType=woTypes&source="+source
		var request = {
			//"source" : source,
			//"viewType" : "woTypes"
		};
		callAjaxService("AppPropertiesPost", AppPropertiesCallback, callBackFailure,
				request, "POST");
	};

	var AppPropertiesCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined && data !== null ){
			var dataTableData = data;
			var deleteDiv = $("#deleteDiv").html();
			var dataTable = $(".dc-data-table")
			.DataTable(
					{

						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"lengthChange" : true,
						"bSort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"data" : dataTableData,
						 "bAutoWidth": false,
						"tableTools" : {
							"aButtons" : [ {
								"sExtends" : "collection",
								"sButtonText" : 'Export <span class="caret" />',
								"aButtons" : [ "csv", "pdf" ]
							} ],
							"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
						},
						"destroy" : true,
						"columns" :[{											
							
							"data" : "key",
							"defaultContent" : ""
						},{

							"data" : "value",
							"defaultContent" : ""
						},{

							"data" : "type",
							"defaultContent" : ""
						},{
							"data" : "key",
							"defaultContent" :"",
							"render" : function(data, type,
									full) {
								return deleteDiv.replace("$","'"+data+"'").replace("#","AppProperties");
							}
						}
						],
						"rowCallback" : function(nRow, aData,
								iDisplayIndex, iDisplayIndexFull) {
							$(nRow)
							.attr(
									{
										'onClick' : "javascript:AppProperties.Edit( '"+ aData.key+ "','"+aData.value+"','"+aData.remarks+"','"+aData.type+"')",
										'data-target' : '#myModalthredlevel',
										'data-toggle' : 'modal'
									});
						}
					});



		}
	};
	this.CreateView = function(){
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-plus")).append(" Create").unbind('click').bind('click',function(){AppProperties.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("[name=name]").removeAttr("readonly");
		$("#type").select2("val",null);
	};	
	this.Create = function() {
		if (!$("#frmSankeyFilter").valid())
			return;
		var request = {
				"name" : $("[name=name]").val(),
				"value" : $("[name=value]").val(),
				"remarks": $("[name=remarks]").val(),
				"type":$("[name=type]").val(),
		};
		callAjaxService("CreateAppProperties", callbackSucessCreateApp, callBackFailure,
				request, "POST");
	};
	var callbackSucessCreateApp = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "App value Created Sucessfully..");
		}
	};
	
	THIS.Edit = function(name,value,remarks,type){
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-pencil")).append(" Update").unbind('click').bind('click',function(){AppProperties.Update();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		
		
		$("[name=name]").val(name).attr("readonly","true");
		$("[name=value]").val(value);
		$("[name=remarks]").val(remarks);
		$("[name=type]").select2("val",type);
		
	};
	this.Update = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		
		var request = {
				"name":$("[name=name]").val(),
				"value": $("[name=value]").val(),
				"remarks": $("[name=remarks]").val(),
				"type":$("[name=type]").val()
		};
		callAjaxService("UpdateAppProperties",callbackSucessUpdateUser,callBackFailure,request,"POST");
	};
	var callbackSucessUpdateUser = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			THIS.Load();
			showNotification("success","User updated Sucessfully..");
		};
	};
	
	this.Delete = function(event,name){
		event.stopPropagation();
		$.confirm({
		    text: "Are you sure you want to delete this Property?",
		    confirm: function() {
		    	var request = {
						"name":name
				};
				callAjaxService("DeleteAppProperties",callbackDeleteCreateApp, callBackFailure,
						request, "POST");
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	this.CancelDelete = function(event){
		 event.stopPropagation();
		 $(".hidden-tablet.open").removeClass("open");
		 	};
	var callbackDeleteCreateApp = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "App property Deleted Sucessfully..");
		}
	};
	
	this.SystemPropertiesRefresh = function() {
		callAjaxService("SystemPropertiesRefreshAction", callbackSucessSystemPropertiesRefreshAction, callBackFailure);
	};
	var callbackSucessSystemPropertiesRefreshAction = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "System properties Refreshed Sucessfully.");
		}
	};
};
AppPropertiesInstance();