'use strict';
pageSetUp();
var ActionReport;
var ActionReportInstance = function() {
	ActionReport = new ActionReportObject();
	ActionReport.Load();
	//ActionReport.FilterMaterial();
};
var ActionReportObject = function() {
	var THIS = this;
	var mfviewtype = $("#resetFilter").find("label.active").data("source");
	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		// var request = "viewType=userTypes&source="+source
		var request = {
			"source" : source
		};
		// console.log(request);
		callAjaxService("ActionReportPost", ActionReportPostSuccessCallback,
				callBackFailure, request, "GET");
	};

	var ActionReportPostSuccessCallback = function(data) {
		disableLoading();
		if (data && data.isException) {
			showNotification("error", data.customMessage);
			return;
		}
		if (data !== undefined && data !== null) {

			// data = data['userTypeList'].splice(0,100);
			// console.log(data);
			var deleteDiv = $("#deleteDiv").html();
			var dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"sort" : true,
								"filter" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"stateSave": true,
								"order": [[ 0, "desc" ]],
								"data" : data,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"destroy" : true,
								"columns" : [ {

									"data" : "id",
									"defaultContent" : ""
								}, {

									"data" : "action",
									"defaultContent" : ""
								}, {

									"data" : "displayname",
									"defaultContent" : ""
								}, {

									"data" : "parent",
									"defaultContent" : ""
								}, {

									"data" : "parentName",
									"defaultContent" : ""
								}, {

									"data" : "icon",
									"defaultContent" : ""
								}, {

									"data" : "sequence",
									"defaultContent" : ""
								}, {
									"data" : "enabled",
									"defaultContent" : ""
								},{
									"data" : "id",
									"defaultContent" :"",
									"render" : function(data, type,
											full) {
										return deleteDiv.replace("$","'"+data+"'").replace(/#/g,"ActionReport");
									}
								} ],
								"rowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									$(nRow).delegate("td:not(last-child)","click",function(event){
										if (!$(event.target).hasClass("fa-cog") && !$(event.target).hasClass("btn-default")) { 
											THIS.Edit(aData.action,aData.id,aData.refid);
										}
									})
								}
							});
		}
	};
	this.CreateView = function() {
		previewPublishProfile();
	};

	this.Delete = function(event,key){
		 event.stopPropagation();
		$.confirm({
		    text: "Are you sure you want to delete that Action?",
		    confirm: function() {
		    	var request = {
						"actionid":key
				};
				callAjaxService("DeleteAction",callbackDeleteAction, callBackFailure,
						request, "POST");
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	this.CancelDelete = function(event){
		 event.stopPropagation();
		 $(".hidden-tablet.open").removeClass("open");
	};
	var callbackDeleteAction = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "Action Deleted Sucessfully..");
		}
	};
	this.Edit = function(action,actionId,refid){
		event.stopPropagation();
		publishedActiondetails(action,actionId,refid);
	};
	
	var previewPublishProfile = function(obj){
		$("#userEnabled").removeAttr("checked");
		$("#smart-form-register input").removeAttr("readonly");
		var publishDataObj = new Object();
		publishDataObj.action = "";
		publishDataObj.displayname = "";
		publishDataObj.refid = "";
		publishDataObj.source = "";
//		publishDataObj.enabled = 0;
		
		var callBackPublishProfile = function(data){
			$(obj).html("Published").removeClass("btn-outline").addClass("btn-outline-success");
			$(obj).parent().parent().find(".text-left.col-md-2").text(data.action);
			$(obj).parent().parent().find(".text-left.col-md-2").attr("data-action",data.action);
		};
		
		$("#smart-form-register input").val("");			
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		
		templates.managePublishing(publishDataObj,"CREATE",callBackPublishProfile);
	};
	
	var publishedActiondetails = function(action,actionId,refid){
		enableLoading();
		
		var callbackSucessGetAction = function(response){
			if (response && response.isException){
				showNotification("error",response.customMessage);
				return;
			} else {
				if(response == null){
					disableLoading();
					alert("Selected Profile is Published as Tab");
					return;
				}
				
				response.refid = refid;
				response.actionId = Number(actionId);
				templates.managePublishing(response,"EDIT",undefined);
			}
		};
		
		var request = {
				"action":action
		};
		callAjaxService(
				"getAction",
				function (response){callbackSucessGetAction(response);}, callBackFailure, request, "POST");
	};
};


ActionReportInstance();
