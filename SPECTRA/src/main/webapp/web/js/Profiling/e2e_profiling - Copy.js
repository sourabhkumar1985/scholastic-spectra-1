'use strict';
pageSetUp();
var poProfiling;
var test;
var test1;
var poProfileInstance = function() {
	poProfiling = new poProfilingObject();
	poProfiling.Load();
};

/*
 * var config = { endpoint : 'poprofile', dataType: 'json', dataKey:
 * 'aggregatedPurchaseOrderList', reqPayload: {"viewType" : "Default","source" :
 * "SAP"}, mappings : [{ chartType : 'table', htmlElement: '.dc-data-table',
 * field: 'companycode', groupon: 'companycode', coloums: 'a, b, c, d, e, f, g,
 * h, i, j, k', sortby: 'dd', summarydiv : '.dc-data-count', renderlet:
 * 'dc-table-group', hasChildren: true, childRequestLevel :'level2', childurl:
 * '/E2EMF/rest/profiling/poprofile', childDataKey:
 * 'aggregatedPurchaseOrderList', childrenParams : 'companyCode, plantgroup,
 * purchaseorg, purchasegroup, vendor_type, material_type, lastchangedon',
 * childrenColoums : 'a, b,c ,d,e,f,g,h,i' }] };
 * 
 * var Profiling = new Profiling(config); console.log(Profiling);
 * Profiling.init(function(){ $('#loading').hide(); });
 */

var poProfilingObject = function() {
	var THIS = this;
	var dateFormat = d3.timeFormat("%m/%d/%Y");
	// var numberFormat = d3.format(".f");
	var numberFormat = d3.format(",f");
	THIS.type = "materialCount";
	this.Load = function(source) {
		enableLoading();
		source = source || "SAP";
		var request = {
			"source" : source,
			"viewType" : "default"
		};
		callAjaxService("poprofile", callbackSuccessPOProfile, callBackFailure,
				request, "POST");
	};

	this.loadViewType = function(viewType) {
		enableLoading();
		THIS.type = viewType;
		setTimeout(function() {
			drawChart();
		}, 50);
	};
	/*
	 * $("#btnPrintL3").click(function(e){ printElement('#poDocView'); });
	 * $("#btnSaveL3").click(function(e){ savePDF('#poDocView'); });
	 */

	var callbackSuccessPOProfile = function(data) {
		if (data != undefined && data != null
				&& data["aggregatedPurchaseOrderList"] != undefined
				&& data["aggregatedPurchaseOrderList"] != null) {

			THIS.data = data["aggregatedPurchaseOrderList"];
			drawChart();
		}

	};
	var drawChart = function() {
		var data = THIS.data;

		
		
		THIS.pieChartPurchaseGrp = dc.pieChart("#pie-chart-prchasegroup");
		THIS.pieChartPlant = dc.pieChart("#pie-chart-plant");
		THIS.pieChartPurchaseorg = dc.pieChart("#pie-chart-purchaseorg");
		THIS.horzBarChart = dc.rowChart("#row-chart");
		THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
		THIS.timelineChart = dc.barChart("#timeline-chart");

		/*
		 * private String poCount; a private String companyCode; b private
		 * String companyName; c private String purchaseOrg; d private String
		 * purchaseOrgName; e private String purchaseGroup; f private String
		 * purchaseGroupName; g private String purchasePlantGroup; h private
		 * String plantName; i private String orderQuantity; j private String
		 * netPrice; k private String grossOrderValue; l private String source;
		 * m private String lastChanged; n private String materialType; o
		 * private String vendorType; p private String poType; q
		 */
		data.forEach(function(d) {
			d.dd = dateFormat.parse(d.n);
			d.month = d3.time.month(d.dd);
			d.l = +d.l;
			d.zz = (THIS.type == "netvalue") ? isNaN(d.k) ? 0 : d.k
					: isNaN(d.a) ? 0 : d.a;
		});

		var ndx = crossfilter(data);
		// console.log(ndx);
		var all = ndx.groupAll();

		// dimension by month
		var timelineDimension = ndx.dimension(function(d) {
			return d.month;
		});
		// group by total volume within move, and scale down result
		var timelineGroup = timelineDimension.group().reduceSum(function(d) {
			return d.zz;
		});
		// group by total movement within month
		// var timelineSecondGroup = timelineDimension.group().reduceSum(
		// function(d) {
		// return d.grossOrderValue;
		// });
		var timelineAreaGroup = timelineDimension.group().reduce(
				function(p, v) {
					++p.count;
					v.l = v.l || 0;
					p.total += v.l;// (v.open + v.close) / 2;
					// p.avg = Math.round(p.total / p.count);
					return p;
				}, function(p, v) {
					--p.count;
					v.l = v.l || 0;
					p.total -= v.l;// (v.open + v.close) / 2;
					// p.avg = p.count ? Math.round(p.total / p.count) : 0;
					return p;
				}, function() {
					return {
						count : 0,
						total : 0,
					};
				});

		// summerize volume by quarter
		var pieMaterialTypeDimension = ndx.dimension(function(d) {
			return d.o || "Others";
		});
		if (pieMaterialTypeDimension.group().size > 1
				|| pieMaterialTypeDimension.group().all()[0].key.split("-")[0] != "Others") {
			THIS.pieChartMaterialType = dc.pieChart("#pie-chart");
			var pieMaterialType = pieMaterialTypeDimension.group().reduceSum(
					function(d) {
						return d.zz;
					});
			THIS.pieChartMaterialType.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(pieMaterialTypeDimension).group(pieMaterialType).on(
					'filtered', function(chart) {
						THIS.refreshTable(pieMaterialTypeDimension);
					}).title(
					function(d) {
						return THIS.type == "netvalue" ? d.key + ': $'
								+ numberFormat(d.value) : d.key + ': '
								+ numberFormat(d.value);
					});
		}
		var pieChartVendorGroupDimention = ndx.dimension(function(d) {
			return d.p || "Others";
		});
		if (pieChartVendorGroupDimention.group().size > 1
				|| pieChartVendorGroupDimention.group().all()[0].key.split("-")[0] != "Others") {
			THIS.pieChartVendor = dc.pieChart("#pie-chart-Vendor");
			var pieVendorGroup = pieChartVendorGroupDimention.group().reduceSum(
					function(d) {
						return d.zz;
					});
			THIS.pieChartVendor.width(250).height(200).radius(80).innerRadius(40)
			.dimension(pieChartVendorGroupDimention).group(pieVendorGroup)
			.on('filtered', function(chart) {
				THIS.refreshTable(pieChartVendorGroupDimention);
			}).title(
					function(d) {
						return THIS.type == "netvalue" ? d.key + ': $'
								+ numberFormat(d.value) : d.key + ': '
								+ numberFormat(d.value);
					});
		}
		
		var pieChartCompanyGroupDimention = ndx.dimension(function(d) {
			return (d.b || "Others") + "-" + (d.c || "Not Available");
		});
		if (pieChartCompanyGroupDimention.group().size > 1
				|| pieChartCompanyGroupDimention.group().all()[0].key.split("-")[0] != "Others") {
			THIS.pieChartCompanyCode = dc.pieChart("#pie-chart-Companycode");
			var pieCompanyGroup = pieChartCompanyGroupDimention.group().reduceSum(
					function(d) {
						return d.zz;
					});
			THIS.pieChartCompanyCode.width(250).height(200).radius(80)
			// .colors(colorScale)
			// .innerRadius(30)
			.dimension(pieChartCompanyGroupDimention).group(pieCompanyGroup).label(
					function(d) {
						var b = d.key.split("-");
						return b[0];
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartCompanyGroupDimention);
			}).title(
					function(d) {
						return THIS.type == "netvalue" ? d.key + ': $'
								+ numberFormat(d.value) : d.key + ': '
								+ numberFormat(d.value);
					});
		}
		
		var pieChartPurchaseGroupDimention = ndx.dimension(function(d) {
			return (d.f || "Others") + "-" + (d.g || "Not Available");
		});
		if (pieChartPurchaseGroupDimention.group().size > 1
				|| pieChartPurchaseGroupDimention.group().all()[0].key.split("-")[0] != "Others") {
			
		}
		var pieChartPlantDimention = ndx.dimension(function(d) {
			return (d.h || "Others") + "-" + (d.i || "Not Available");
		});
		var pieChartPurchaseorgDimention = ndx.dimension(function(d) {
			return (d.d || "Others") + "-" + (d.e || "Not Available");
		});

		
		
		var piePurchaseGroup = pieChartPurchaseGroupDimention.group()
				.reduceSum(function(d) {
					return d.zz;
				});
		var piePlantGroup = pieChartPlantDimention.group().reduceSum(
				function(d) {
					return d.zz;
				});
		var piePurchaseOrgGroup = pieChartPurchaseorgDimention.group()
				.reduceSum(function(d) {
					return d.zz;
				});

		// counts per weekday
		var horzBarDimension = ndx.dimension(function(d) {
			return d.q || "Others";
		});
		var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
			return d.zz;
		});

		

		

		THIS.pieChartPurchaseGrp.width(250).height(200).radius(80).innerRadius(
				40).dimension(pieChartPurchaseGroupDimention).group(
				piePurchaseGroup).label(function(d) {
			var purchaseGrp = d.key.split("-");
			return purchaseGrp[0];
		}).on('filtered', function(chart) {
			THIS.refreshTable(pieChartPurchaseGroupDimention);
		}).title(
				function(d) {
					return THIS.type == "netvalue" ? d.key + ': $'
							+ numberFormat(d.value) : d.key + ': '
							+ numberFormat(d.value);
				});

		THIS.pieChartPlant.width(250).height(200).radius(80)
		// .innerRadius(30)
		.dimension(pieChartPlantDimention).group(piePlantGroup).label(
				function(d) {
					var h = d.key.split("-");
					return h[0];
				}).on('filtered', function(chart) {
			THIS.refreshTable(pieChartPlantDimention);
		}).title(
				function(d) {
					return THIS.type == "netvalue" ? d.key + ': $'
							+ numberFormat(d.value) : d.key + ': '
							+ numberFormat(d.value);
				});

		THIS.pieChartPurchaseorg.width(250).height(200).radius(80).innerRadius(
				40).dimension(pieChartPurchaseorgDimention).group(
				piePurchaseOrgGroup).label(function(d) {
			var z = d.key.split("-");
			return z[0];
		}).on('filtered', function(chart) {
			THIS.refreshTable(pieChartPurchaseorgDimention);
		}).title(
				function(d) {
					return THIS.type == "netvalue" ? d.key + ': $'
							+ numberFormat(d.value) : d.key + ': '
							+ numberFormat(d.value);
				});

		// #### Row Chart
		THIS.horzBarChart.width(180).height(535).margins({
			top : 20,
			left : 10,
			right : 10,
			bottom : 20
		}).group(horzBarGrp).dimension(horzBarDimension).on('filtered',
				function(chart) {
					THIS.refreshTable(horzBarDimension);
				})
		// assign colors to each value in the x scale domain
		.ordinalColors(
				[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
				.label(function(d) {

					return d.key;// .split(".")[1];
				})
				// title sets the row text
				.title(
						function(d) {
							if (isNaN(d.value))
								d.value = 0;
							return THIS.type == "netvalue" ? d.key + ': $'
									+ numberFormat(d.value) : d.key + ': '
									+ numberFormat(d.value);
						}).elasticX(true).xAxis().ticks(4);

		THIS.timelineAreaChart.renderArea(true).width($('#content').width())
				.height(150).transitionDuration(1000).margins({
					top : 30,
					right : 70,
					bottom : 25,
					left : 80
				}).dimension(timelineDimension).mouseZoomable(true).on(
						'filtered', function(chart) {
							THIS.refreshTable(timelineDimension);
						})
				// Specify a range chart to link the brush extent of the
				// range
				// with
				// the zoom focue of the current chart.
				.rangeChart(THIS.timelineChart).x(
						d3.time.scale()
								.domain(
										[ new Date(2011, 0, 1),
												new Date(2015, 11, 31) ]))
				.round(d3.time.month.round).xUnits(d3.time.months).elasticY(
						true).renderHorizontalGridLines(true)
				// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
				.brushOn(false).group(timelineAreaGroup, "Net Value")
				.valueAccessor(function(d) {
					return d.value.total;
				}).renderHorizontalGridLines(true)
				// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
				.brushOn(false).group(timelineAreaGroup, "Net Value")
				.valueAccessor(function(d) {
					return d.value.total;
				})// .stack(timelineSecondGroup, "Material Count",
				// function(d) {
				// return d.value;
				// })
				// title can be called by any stack layer.
				.title(function(d) {
					var value = d.value.total;

					if (isNaN(value))
						value = 0;
					return dateFormat(d.key) + "\n$" + numberFormat(value);
				});

		THIS.timelineChart.width($('#content').width()).height(80).margins({
			top : 40,
			right : 70,
			bottom : 20,
			left : 80
		}).dimension(timelineDimension).group(timelineGroup).centerBar(true)
				.gap(1).x(
						d3.time.scale()
								.domain(
										[ new Date(2011, 0, 1),
												new Date(2015, 11, 31) ]))
				.round(d3.time.month.round).alwaysUseRounding(true).xUnits(
						d3.time.months).title(function(d) {
					var value = d.value;
					if (isNaN(value))
						value = 0;
					return dateFormat(d.key) + ": " + numberFormat(value);
				});

		dc.dataCount(".dc-data-count").dimension(ndx).group(all);

		var dataTableData = data.slice(0, 100);

		THIS.dataTable = $(".dc-data-table")
				.DataTable(
						{
							"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
							"bProcessing" : true,
							"bLengthChange" : true,
							"bSort" : true,
							"bInfo" : true,
							"bJQueryUI" : false,
							"scrollX" : true,
							"aaData" : dataTableData,
							"bAutoWidth" : false,
							"oTableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"bDestroy" : true,
							"processing" : true,
							"aoColumns" : [
									{
										"mData" : null,
										"sDefaultContent" : '<img src="./img/details_open.png">'
									}, {
										"mData" : "a",
										"sDefaultContent" : "0",
										"mRender" : function(data, type, full) {
											return numberFormat(data);
										},
										"sClass" : "numbercolumn"
									}, {
										"mData" : "b",
										"sDefaultContent" : ""
									}, {
										"mData" : "d",
										"sDefaultContent" : ""
									}, {
										"mData" : "g",
										"sDefaultContent" : ""
									}, {
										"mData" : "h",
										"sDefaultContent" : ""
									}, {
										"mData" : "p",
										"sDefaultContent" : ""
									}, {
										"mData" : "j",
										"sDefaultContent" : "0",
										"mRender" : function(data, type, full) {
											return numberFormat(data);
										},
										"sClass" : "numbercolumn"
									}, {

										"mData" : "k",
										"sDefaultContent" : "",
										"mRender" : function(data, type, full) {
											return numberFormat(data);
										},
										"sClass" : "numbercolumn"
									}, {

										"mData" : "o",
										"sDefaultContent" : ""
									}, {
										"mData" : "l",
										"sDefaultContent" : "0",
										"mRender" : function(data, type, full) {
											return numberFormat(data);
										},
										"sClass" : "numbercolumn"
									}, {
										"mData" : "m",
										"sDefaultContent" : ""
									}, {
										"sDefaultContent" : "",
										"mData" : "q"
									}, {
										"mData" : "n",
										"sDefaultContent" : "",
										"sClass" : "numbercolumn"
									}, ],
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								var request = {
									"viewType" : "level2",
									"source" : aData.m || "null",
									"companyCode" : aData.b || "null",
									"plantName" : aData.h || "null",
									"purchaseOrg" : aData.d || "null",
									"purchaseGroup" : aData.f || "null",
									"vendorType" : aData.p || "null",
									"materialType" : aData.o || "null",
									"lastChanged" : aData.n || "null",
									"poType" : aData.q || "null"
								};
								$(nRow).attr({
									"id" : "row" + iDisplayIndex
								}).unbind('click').bind('click', request,
										poProfiling.callLevel2PoProfile);
							}
						});
		dc.renderAll();
		disableLoading();
	};

	this.callLevel2PoProfile = function(request) {
		// console.log(request);
		var tr = $(this).closest('tr');
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "13",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td", "50%", "30%");
				request = request.data;
				callAjaxService("poprofile", function(response) {
					callbackSucessLevel2POProfile(response, request);
				}, callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};

	var callbackSucessLevel2POProfile = function(response, request) {
		disableLoading(THIS.childId + "td");
		var childTabledata = null;
		if (response != null)
			childTabledata = response["level2PurchaseOrderList"];

		$("#" + THIS.childId + "td")
				.show()
				.html("")
				.append(
						$("<table/>").addClass(
								"table table-hover dc-data-table").attr({
							"id" : THIS.childId + "Table"
						}).append(
								$("<thead/>").append(
										$("<tr/>").append(
												$("<th/>").html(
														"Purchase Order"))
												.append(
														$("<th/>").html(
																"Vendor ID"))
												.append(
														$("<th/>").html(
																"Vendor Name"))
												.append(
														$("<th/>")
																.html("Plant"))
												.append(
														$("<th/>").html(
																"Plant Name"))
												.append(
														$("<th/>").html(
																"Net Value")))));

		$("#" + THIS.childId + "Table")
				.DataTable(
						{
							"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
							"bProcessing" : true,
							"bLengthChange" : true,
							// "bServerSide": true,
							"bInfo" : true,
							"bFilter" : false,
							"bJQueryUI" : false,
							"aaData" : childTabledata,
							"scrollX" : true,
							"oTableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"aoColumns" : [ {
								"mDataProp" : "poNumber",
								"sDefaultContent" : ""
							}, {
								"mDataProp" : "vendorId",
								"sDefaultContent" : ""
							}, {
								"mDataProp" : "vendorName",
								"sDefaultContent" : ""
							}, {
								"mDataProp" : "plant",
								"sDefaultContent" : ""
							}, {
								"mDataProp" : "plantName",
								"sDefaultContent" : ""
							}, {
								"mDataProp" : "netValue",
								"sDefaultContent" : "",
								"mRender" : function(data, type, full) {
									return numberFormat(data);
								},
								"sClass" : "numbercolumn"
							} ],
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								$(nRow)
										.attr(
												{
													'onClick' : "javascript:thirdLevelDrillDown('po','', '"
															+ aData.poNumber
															+ "','"
															+ request.source
															+ "')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}
						});
	};
	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			// console.log('Resetting Table');
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(100));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};
	// #### Version
	// Determine the current version of dc with `dc.version`
	d3.selectAll("#version").text(dc.version);
	var config = {
		endpoint : 'poprofile',
		dataType : 'json',
		dataKey : 'aggregatedPurchaseOrderList',
		dataPreProcess : function(d) {
			var dateFormat = d3.timeFormat("%m/%d/%Y");
			d.dd = dateFormat.parse(d.n);

			d.month = d3.time.month(d.dd);
			d.a = +d.a;
			d.j = +d.j;
			d.k = +d.k;
			d.k = +d.k;
		},
		reqPayload : {
			"source" : 'SAP',
			"viewType" : "default"
		},
		mappings : [
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-Companycode',
					field : 'a',
					resetHandler : '#resetCC'
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-plant',
					field : 'b',
					resetHandler : '#resetPlant'
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-purchaseorg',
					field : 'c',
					resetHandler : '#resetPurOrg'
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-prchasegroup',
					field : 'd',
					resetHandler : '#resetPurGr'
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart',
					field : 'e',
					resetHandler : '#resetMT'
				},
				{
					chartType : 'pie',
					htmlElement : '#pie-chart-Vendor',
					field : 'f',
					resetHandler : '#resetVen'
				},
				{
					chartType : 'timeline',
					htmlElement : '#timeline-area-chart',
					subHtmlElement : '#timeline-chart',
					field : 'n',
					group : 'a',
					extractMonth : true,
					resetHandler : '#resetTAC',
					mapFunction : function(p, v) {
						++p.count;
						p.total += v.k;// (v.open + v.close) / 2;
						// p.avg = numberFormat(p.total / p.count);
						return p;
					},
					reduceFunction : function(p, v) {
						--p.count;
						p.total -= v.k;// (v.open + v.close) / 2;
						// p.avg = p.count ? numberFormat(p.total / p.count) :
						// 0;
						return p;
					},
					countFunction : function() {
						return {
							count : 0,
							total : 0,
						};
					},
					titleFunction : function(d) {
						var dateFormat = d3.timeFormat("%m/%d/%Y");
						var numberFormat = d3.format(".2f");
						var value = d.value.total;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n" + numberFormat(value);
					}
				},
				{
					chartType : 'horbar',
					htmlElement : '#row-chart',
					field : 'q',
					// group: 'a',
					resetHandler : '#resetHor'
				},
				{
					chartType : 'table',
					htmlElement : '.dc-data-table',
					field : 'a',
					groupon : 'a',
					coloums : 'a, b, c, d, e, f, g, h, i, j, k',
					sortby : 'a',
					summarydiv : '.dc-data-count',
					renderlet : 'dc-table-group',
					hasChildren : true,
					childRequestLevel : 'level2',
					childurl : directoryName + '/rest/profiling/poprofile',
					childDataKey : 'aggregatedPurchaseOrderList',
					childrenParams : 'companyCode, plantgroup, purchaseorg, purchasegroup, vendor_type, material_type, lastchangedon',
					// childrenColoums : 'companyCode,
					// materialType,netPrice,netValue,plant,plantName,poNumber,poType,purchaseOrg,purchseGroup,source,unitOfMeasure,vendorId,vendorName,vendorType',
					childMappings : [ {
						name : 'Company Code',
						mapping : 'a'
					}, {
						name : 'Plant Group',
						mapping : 'b'
					}, {
						name : 'Purchase Org',
						mapping : 'c'
					}, {
						name : 'Purchase Group',
						mapping : 'd'
					} ],
					childDrillDown : 'po'

				} ]
	};

	// var ProfilingPricing = new Profiling(config);
	// console.log(ProfilingPricing);
	// ProfilingPricing.init(function(){
	// $('#loading').hide();
	// });
	//	

};
poProfileInstance();