pageSetUp();
var materialDistributionInstance = function() {
	materialDistribution = new materialDistributionObject();
	materialDistribution.Load();
};

var materialDistributionObject = function() {
	var THIS = this;
	this.materialGroupObject;
	var numberFormat = d3.format(",f");
	constructTabs(THIS);
	this.Load = function(obj) {
		enableLoading('loading');
		var source = $(obj).data("source");
		source = source || $("#myTab").find("li.active").data("source");
		
		viewtype = $("#getByChoice").find("label.active").data("source");
		THIS.viewtype = viewtype || "materialgroup";

		var request = {
			"source" : source,
			"viewtype" : THIS.viewtype,
			"dbname" : "APAC_DB"
		};
		callAjaxService("MaterialGroupDistribution",
				callBackSuccessMaterialGroupDistribution, callBackFailure,
				request, "POST");
	};
	this.loadViewType = function(viewtype) {
		enableLoading();
		var datasource = $("#myTab").find("li.active").data("source");
		datasource = datasource || "SAP";
		THIS.viewtype = viewtype || "materialdist";

		var request = {
			"source" : datasource,
			"viewtype" : THIS.viewtype,
			"dbname" : "APAC_DB"
		};
		callAjaxService("MaterialGroupDistribution",
				callBackSuccessMaterialGroupDistribution, callBackFailure,
				request, "POST");
	};
	this.FilterMaterial = function() {
		var fromnetper = 0;
		var tonetper = 100;
		if ($(".irs-from").length > 0
				&& !isNaN($(".irs-from").html())) {
			fromnetper = $(".irs-from").html();
		}
		if ($(".irs-to").length > 0
				&& !isNaN($(".irs-to").html())) {
			tonetper = $(".irs-to").html();
		}
		constructMaterialGroupChart(fromnetper, tonetper);
	};

	var callBackSuccessMaterialGroupDistribution = function(sroot) {
		if (sroot && sroot.isException){
			showNotification("error",sroot.customMessage);
			return;
		}
		THIS.materialGroupObject = sroot["materialList"];
		constructMaterialGroupChart(90, 100);
	};
	var constructMaterialGroupChart = function(fromMaterialPercentage,
			toMaterialPercentage) {
		disableLoading('loading');
		if (THIS.materialGroupObject && THIS.materialGroupObject != null
				&& THIS.materialGroupObject.length > 0) {
			$("#results").empty();
			var percentageMaterialCount = THIS.materialGroupObject.length / 100;
			var fromMaterialCount = parseInt((100 - fromMaterialPercentage)
					* percentageMaterialCount);
			var toMaterialCount = parseInt((100 - toMaterialPercentage)
					* percentageMaterialCount);
			if (fromMaterialCount == 0){
				fromMaterialCount = 1;
			}
			var  chartData = THIS.materialGroupObject.slice(Math
					.round(toMaterialCount), Math.round(fromMaterialCount));
			
			if (THIS.viewtype == "materialgroup")
				$("#chatyType").html("Material Groups Filtered (<span id='range'></span>)<span class='pull-right'>"+chartData.length+"/"+ THIS.materialGroupObject.length+"</span>");
			 else
				 $("#chatyType").html("Materials Filtered (<span id='range'></span>)<span class='pull-right'>"+chartData.length+"/"+ THIS.materialGroupObject.length+"</span>");
			
			$("#range").html(" $"+ numberFormat(THIS.materialGroupObject[Math.round(fromMaterialCount-1)].netvalue)+" - "+ " $"+ numberFormat(THIS.materialGroupObject[Math.round(toMaterialCount)].netvalue)) ;
			$("#chartPercentage").css({"width":Math.round(toMaterialPercentage)-Math.round(fromMaterialPercentage)+"%"});
			$("#rev-toggles-1").removeClass("hide");
			
			var root = {
				"name" : "bubble",
				"children" : chartData
			};
			if ($("#range-slider-1").ionRangeSlider().length)
				$("#range-slider-1").ionRangeSlider("remove");
			$("#range-slider-1").ionRangeSlider({
				min : 0,
				max : 100,
				from : fromMaterialPercentage,
				to : toMaterialPercentage,
				type : 'double',
				step : 1,
				multiply : 1,
				prettify : false,
				hasGrid : true,
				onFinish : function(obj) {
					materialDistribution.FilterMaterial("MaterialGroup");
				}
			});
			var diameter = 1000, format = d3.format(",d"), color = d3.scale
					.category20c();

			var bubble = d3.layout.pack().sort(null).size(
					[ diameter, diameter ]).padding(1);
			$("#materialgroup svg").remove();
			var svg = d3.select("#materialgroup").append("svg").attr("width",
					diameter).attr("height", diameter).attr("class", "bubble")
					.attr('id', 'materialgroupsvg');
			var node = svg.selectAll(".node").data(
					bubble.nodes(classes(root)).filter(function(d) {
						return !d.children;
					})).enter().append("g").attr("class", "node").attr(
					"transform", function(d) {
						return "translate(" + d.x + "," + d.y + ")";
					});

			node.append("title")
					.text(
							function(d) {
								if (THIS.viewtype ==  "materialgroup"){
								return "Material Group: "
										+ d.className
										+ "\nMaterial Group Id: "
										+ d.id
										+ "\nSO Count: "
										+ numberFormat(d.netvalue)
										+ "\n"
										+ "Net Value: $"
										+ numberFormat(d.value);
								} else {
									return "Material: "
									+ d.className
									+ "\nMaterial Id: "
									+ d.id
									+ "\nSO Count: "
									+ numberFormat(d.netvalue)
									+ "\n"
									+ "Net Value: $"
									+ numberFormat(d.value);
								}
							});

			node.append("circle").attr("r", function(d) {
				return d.r;
			}).style("fill", function(d) {
				return color(d.className);
			});

			node.append("text").attr("dy", ".3em").style("text-anchor",
					"middle").text(function(d) {
				if (d.className)
					return d.className.substring(0, d.r / 3);
				else
					return "";
			});
			PanZoomsvg('materialgroupsvg');
		} else {
			$("#materialgroup svg").remove();
			$("#results")
					.html(
							'<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">�</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found for selected filters.</p></div>');
			$("#results").show();
		}
	};
	// Returns a flattened hierarchy containing all leaf nodes under the root.
	function classes(root) {
		var classes = [];

		function recurse(name, node) {
			if (node.children)
				node.children.forEach(function(child) {
					recurse(node.materialGroupName, child);
				});
			else
				classes.push({
					packageName : name,
					className : node.name,
					value : node.netvalue,
					netvalue : node.size,
					id : node.materialId
				});
		}

		recurse(null, root);
		return {
			children : classes
		};
	}
};
materialDistributionInstance();