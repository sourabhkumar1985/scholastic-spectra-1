'use strict';
pageSetUp();
var plantProfiling;
var plantProfileInstance = function() {
	plantProfiling = new plantProfilingObject();
	plantProfiling.Load();
};

var plantProfilingObject = function() {
	var THIS = this;
	var dateFormat = d3.timeFormat("%m/%d/%Y");
	var numberFormat = d3.format(",f");
	THIS.type == "materialCount";
	constructTabs(THIS);
	this.Load = function(obj) {
		enableLoading();
		var source = $(obj).data("source");
		THIS.source = source || $("#myTab").find("li.active").data("source");
		var request = {
				
			"source" : THIS.source,
			"viewType" : "default"
		};
		callAjaxService("plantProfilling", callbackSuccessPlantProfile, callBackFailure,
				request, "POST");
		
	};
	
	this.loadViewType = function(viewType){
		enableLoading();
		THIS.type = viewType;
		setTimeout(function() { drawChart(); disableLoading();}, 50);
		
	};

	var callbackSuccessPlantProfile = function(data) {
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if (data != undefined && data != null && data["aggregatedPlantList"] != undefined && data["aggregatedPlantList"] != null && data["aggregatedPlantList"].length > 0) {
			THIS.data = data["aggregatedPlantList"];
			drawChart();
		}
		disableLoading();
	};
	var drawChart = function(){
		var data = THIS.data;
			data.forEach(function(d) {
				d.dd = dateFormat.parse(d.date);
				d.month = d3.time.month(d.dd);
				d.netvalue = +d.netvalue;
				d.doccount = +d.doccount;
				d.zz = (THIS.type == "netvalue") ?  isNaN(d.netvalue) ? 0 : d.netvalue : isNaN(d.doccount) ? 0 : d.doccount;
			});

			var ndx = crossfilter(data);
			var all = ndx.groupAll();

			THIS.pieChartMaterialType = dc.pieChart("#pie-chart");
			var pieMaterialTypeDimension = ndx.dimension(function(d) {
				return d.materialtype || "Others";
			});
			var pieMaterialType = pieMaterialTypeDimension.group();
			if (pieMaterialType.size() > 1
					|| pieMaterialType.all()[0].key.split("-")[0] != "Others") {
				pieMaterialType = pieMaterialType
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart h4").removeClass("hide");
				pieMaterialType = pieMaterialType
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartVendor = dc.pieChart("#pie-chart-Vendor");
			var pieChartVendorGroupDimention = ndx.dimension(function(d) {
				return d.doctype ||"Others";
			});
			var pieVendorGroup = pieChartVendorGroupDimention.group();
			if (pieVendorGroup.size() > 1
					|| pieVendorGroup.all()[0].key.split("-")[0] != "Others") {
				pieVendorGroup = pieVendorGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-Vendor h4").removeClass("hide");
				pieVendorGroup = pieVendorGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartCompanyCode = dc.pieChart("#pie-chart-Companycode");
			var pieChartCompanyGroupDimention = ndx.dimension(function(d) {
				return (d.companycode || "Others") +" - " +(d.companycodename || "Not Available");
			});
			var pieCompanyGroup = pieChartCompanyGroupDimention.group();
			if (pieCompanyGroup.size() > 1
					|| pieCompanyGroup.all()[0].key.split("-")[0] != "Others") {
				pieCompanyGroup = pieCompanyGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-Companycode h4").removeClass("hide");
				pieCompanyGroup = pieCompanyGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPurchaseGrp = dc.pieChart("#pie-chart-prchasegroup");
			var pieChartPurchaseGroupDimention = ndx.dimension(function(d) {
				return (d.grp || "Others") + '-' + (d.grpname || "Not Available") ;
			});
			var piePurchaseGroup = pieChartPurchaseGroupDimention.group();
			if (piePurchaseGroup.size() > 1
					|| piePurchaseGroup.all()[0].key.split("-")[0] != "Others") {
				piePurchaseGroup = piePurchaseGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-prchasegroup h4").removeClass("hide");
				piePurchaseGroup = piePurchaseGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPlant = dc.pieChart("#pie-chart-plant");
			var pieChartPlantDimention = ndx.dimension(function(d) {
				return d.docsubtype || "Others";
			});
			var piePlantGroup = pieChartPlantDimention.group();
			if (piePlantGroup.size() > 1
					|| piePlantGroup.all()[0].key.split("-")[0] != "Others") {
				piePlantGroup = piePlantGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-plant h4").removeClass("hide");
				piePlantGroup = piePlantGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPurchaseorg = dc.pieChart("#pie-chart-purchaseorg");
			var pieChartPurchaseorgDimention = ndx.dimension(function(d) {
				return (d.organization || "Others") + "-" +(d.organizationname || "Not Available");
			});
			var piePurchaseOrgGroup = pieChartPurchaseorgDimention.group();
			if (piePurchaseOrgGroup.size() > 1
					|| piePurchaseOrgGroup.all()[0].key.split("-")[0] != "Others") {
				piePurchaseOrgGroup = piePurchaseOrgGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				piePurchaseOrgGroup = piePurchaseOrgGroup
				.reduceSum(function() {
					return 0;
				});
			}

			THIS.horzBarChart = dc.rowChart("#row-chart");
			THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
			THIS.timelineChart = dc.barChart("#timeline-chart");
			
			var timelineDimension = ndx.dimension(function(d) {
				return d.month;
			});
			var timelineGroup = timelineDimension.group().reduceSum(
					function(d) {
						d.zz;

					});
			var timelineAreaGroup = timelineDimension.group().reduce(
					function(p, v) {
						++p.count;
						v.netvalue = v.netvalue || 0;
						p.total += v.netvalue;// (v.open + v.close) / 2;
						// p.avg = numberFormat(p.total / p.count);
						return p;
					}, function(p, v) {
						--p.count;
						v.netvalue = v.netvalue || 0;
						p.total -= v.netvalue;// (v.open + v.close) / 2;
						// p.avg = p.count ? numberFormat(p.total / p.count) : 0;
						return p;
					}, function() {
						return {
							count : 0,
							total : 0,
						};
					});
			// counts per weekday
			var horzBarDimension = ndx.dimension(function(d) {
				return (d.plant || "") + '-' + (d.plantname || "Not Available");;
			});
			var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
				return d.zz;
			});

			THIS.pieChartMaterialType.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(pieMaterialTypeDimension).group(pieMaterialType).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			}).on(
					'filtered', function(chart) {
						THIS.refreshTable(pieMaterialTypeDimension);
					});

			THIS.pieChartVendor.width(250).height(200).radius(80).innerRadius(
					40).dimension(pieChartVendorGroupDimention).group(
					pieVendorGroup).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartVendorGroupDimention);
			});

			THIS.pieChartCompanyCode.width(250).height(200).radius(80)
			//.colors(colorScale)
			// .innerRadius(30)
			.dimension(pieChartCompanyGroupDimention).group(pieCompanyGroup)
					.label(function(d) {
						var b = d.key;
						return b;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyGroupDimention);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				});

			THIS.pieChartPurchaseGrp.width(250).height(200).radius(80)
					.innerRadius(40).dimension(pieChartPurchaseGroupDimention)
					.group(piePurchaseGroup).label(function(d) {
						var purchaseGrp = d.key;
						return purchaseGrp;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartPurchaseGroupDimention);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				});

			THIS.pieChartPlant.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(pieChartPlantDimention).group(piePlantGroup).label(
					function(d) {
						var h = d.key;
						return h;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartPlantDimention);
			});

			THIS.pieChartPurchaseorg.width(250).height(200).radius(80)
					.innerRadius(40).dimension(pieChartPurchaseorgDimention)
					.group(piePurchaseOrgGroup).label(function(d) {
						var z = d.key;
						return z;
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartPurchaseorgDimention);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				});

			// #### Row Chart
			THIS.horzBarChart.width(180).height(535).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(horzBarGrp).dimension(horzBarDimension).on('filtered',
					function(chart) {
						THIS.refreshTable(horzBarDimension);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				})
			// assign colors to each value in the x scale domain
			.ordinalColors(
					[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
					/*.label(function(d) {

						return d.key;// .split(".")[1];
					})*/
					// title sets the row text
					.title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			}).elasticX(true).xAxis().ticks(4);

			THIS.timelineAreaChart.renderArea(true).width($('#content').width()).height(150)
					.transitionDuration(1000).margins({
						top : 30,
						right : 70,
						bottom : 25,
						left : 80
					}).dimension(timelineDimension).mouseZoomable(true).on(
							'filtered', function(chart) {
								THIS.refreshTable(timelineDimension);
							})
					.rangeChart(THIS.timelineChart).x(
							d3.time.scale().domain(
									[ new Date(2011, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true)
					// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(timelineAreaGroup, "Net Value")
					.valueAccessor(function(d) {
						return d.value.total;
					})
					.title(function(d) {
						var value = d.value.total;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n $" + numberFormat(value);
					});

			THIS.timelineChart.width($('#content').width()).height(80).margins({
				top : 40,
				right : 70,
				bottom : 20,
				left : 80
			}).dimension(timelineDimension).group(timelineGroup)
					.centerBar(true).gap(1).x(
							d3.time.scale().domain(
									[ new Date(2011, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).alwaysUseRounding(true)
					.xUnits(d3.time.months);

			dc.dataCount(".dc-data-count").dimension(ndx).group(all);

			var dataTableData = data.slice(0, 100);

			THIS.dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"bLengthChange" : true,
								"bSort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : dataTableData,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"bDestroy" : true,
								"columns" : [
										{
											"mData" : null,
											"defaultContent" : '<img src="./img/details_open.png">'
										},
										{
											"mData" : "doccount",
											"defaultContent" : "",
											
										},{
											"mData" : "companycode",
											"defaultContent" : ""
										},
										{
											"mData" : "plant",
											"defaultContent" : "",
											
										},{
											"mData" : "organization",
											"defaultContent" : ""
										},{
											"mData" : "grp",
											"defaultContent" : ""
										},
										{
											"mData" : "doctype",
											"defaultContent" : "",
											
										},
										{
											"mData" : "docsubtype",
											"defaultContent" : ""
										},
										{
											"mData" : "materialtype",
											"defaultContent" : ""
										},
										{
											"data" : "netvalue",
											"defaultContent" : "",
											"render" : function(data, type,
													full) {
												return numberFormat(data);
											},
											"class" : "numbercolumn"
										},
										{
											"mData" : "date",
											"defaultContent" : ""
										}],
								"rowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									var request = {
										"viewType" : "level2",
										"companyCode": aData.companycode,
										"docType" : aData.doctype || "null",
										"plant" : aData.plant || "null",
										"docSubType" : aData.docsubtype || "null",
										"materialType" : aData.materialtype || "null",
										"organization" : aData.organization || "null",
										"group" : aData.grp || "null",
										"date":aData.date || "null",
										"source":aData.source,
										"region":aData.region

									};
									$(nRow).attr({
										"id" : "row" + iDisplayIndex
									}).unbind('click').bind('click', request,
											plantProfiling.callLevel2PoProfile);
								}
							});
		dc.renderAll();
	};

	this.callLevel2PoProfile = function(request) {
		var tr = $(this).closest('tr');
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "13",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td","50%","50%");
				request = request.data;
				callAjaxService(
						"plantProfilling",
						function (response){callbackSucessLevel2POProfile(response, request);}, callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};

	var callbackSucessLevel2POProfile = function(response, request) {
		disableLoading(THIS.childId + "td");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		var payload = "entitytype="+System.dbName+".afpo&criteria=aufnr:";
		if (THIS.source == "JDE")
			payload = "entitytype="+System.dbName+".e2e_jde_wol&criteria=aufnr:";
		var childTabledata = null;
		if (response  && response["level2PlantProfileList"] !=  undefined && response["level2PlantProfileList"] != null){
			childTabledata = response["level2PlantProfileList"];
		}
		var type="WO Order";
		if (request.docType =="BUY"){
				type = "Purchase Order";
		} else if (request.docType =="SELL"){
			type = "Sales Order";
		}
		$("#" + THIS.childId + "td")
				.show()
				.html("")
				.append(
						$("<table/>").addClass(
								"table table-striped table-hover dc-data-table").attr({
							"id" : THIS.childId + "Table"
						}).append(
								$("<thead/>").append(
										$("<tr/>").append(
												$("<th/>").html(type))
												.append(
														$("<th/>").html(
																"Material Number"))
												.append(
														$("<th/>").html(
																"Company Name"))
												.append(
														$("<th/>")
																.html("Plant Name"))
												.append(
														$("<th/>").html(
																"Organization Name"))
												.append(
														$("<th/>").html(
																"Net Value")))));

		$("#" + THIS.childId + "Table")
				.DataTable(
						{
							"dom":
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"processing" : true,
							"bLengthChange" : true,
							// "bServerSide": true,
							"info" : true,
							"filter" : false,
							"jQueryUI" : false,
							"data" : childTabledata,
							"scrollX" : true,
							"tableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"columns" : [ {
								"data" : "docNumber",
								"defaultContent" : ""
							},{
								"data" : "materialNumber",
								"defaultContent" : ""
							}, {
								"data" : "companyDesc",
								"defaultContent" : ""
							}, {
								"data" : "plantDesc",
								"defaultContent" : ""
							}, {
								"data" : "organizationDesc",
								"defaultContent" : ""
							}, {
								"data" : "netValue",
								"defaultContent" : "",
								"render" : function(data, type,
										full) {
									return numberFormat(data);
								},
								"class" : "numbercolumn"
							} ],
							"rowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								if (request.docType =="BUY"){
									$(nRow)
											.attr(
													{
														'onClick' : "javascript:thirdLevelDrillDown('po','', '"
																+ aData.docNumber
																+ "','"
																+ request.source
																+ "')",
														'data-target' : '#myModalthredlevel',
														'data-toggle' : 'modal'
													});									
							} else if  (request.docType =="SELL"){
								$(nRow)
								.attr(
										{
											'onClick' : "javascript:thirdLevelDrillDownSO('', '"
													+ aData.docNumber
													+ "','100','"+request.source+"')",
											'data-target' : '#myModalthredlevel',
											'data-toggle' : 'modal'
										});
								} else{
									var tableName = "";
									if (THIS.source =="SAP")
										tableName = 'AFPO';
									else
										tableName = 'e2e_jde_wol';
									var query = payload+aData.docNumber;
									$(nRow)
										.attr(
												{
													'onClick' : "javascript:searchL3DrillDown('"+query+"','Work Order Details', '"+aData.docNumber+"','"+tableName+"')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}
							}
						});
	};
	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(1000));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};
	// #### Version
	// Determine the current version of dc with `dc.version`
	d3.selectAll("#version").text(dc.version);
};
plantProfileInstance();