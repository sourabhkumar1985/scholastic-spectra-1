var profileTableMap = {};

var Profiling = function(data, config, filters, summary, masterData,
		viewSettings, chartDefaultFilters) {
	this.config = config;
	this.grapher = null;
	this.data = data;
	this.filters = filters;
	this.summary = summary;
	this.masterData = masterData;
	this.viewSettings = viewSettings;
	this.chartDefaultFilters = chartDefaultFilters;
};
Profiling.prototype.destroyObject = function() {
	// dc.deregisterAllCharts();
	this.data = null;
	this.masterData = null;
	this.config = null;
	this.summary = null;
	this.masterData = null;
	this.viewSettings = null;
};

Profiling.prototype.drawChart = function(config, filters, chartDefaultFilters) {

	if (!config.charttype || config.charttype === "") {
		return;
	}
	var graphConfig;
	switch (config.charttype) {

	case 'chart-pie':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['description'] = config.description || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig['absolute'] = config.absolute || "No";
		graphConfig['externallabels'] = config.externallabels || "0";
		graphConfig['externalradiuspadding'] = config.externalradiuspadding || "";
		graphConfig['minangleforlabel'] = config.minangleforlabel || "";
		graphConfig['drawpaths'] = config.drawpaths || 'no';
		graphConfig['showlegend'] = config.showlegend || 'no';
		graphConfig['legendxposition'] = config.legendxposition || "0";
		graphConfig['legendyposition'] = config.legendyposition || "0";
		graphConfig['legendhorizontal'] = (config.legendhorizontal && config.legendhorizontal ==="true")?true:false;
		
		
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.measurelabel = config.measurelabel;
		graphConfig.dynamicdimension = config.dynamicdimension || "";
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig.showtable = config.showtable || "no";
		graphConfig.showtotal = config.showtotal || "no";

		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawPie(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Pie');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-donut':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		if (chartDefaultFilters && chartDefaultFilters.length > 0) {
			for (var j = 0; j < chartDefaultFilters.length; j++) {
				if (chartDefaultFilters[j].fieldName === config.fieldname) {
					config.defaultFiltervalue = chartDefaultFilters[j].fieldVal
							.join(',');
					graphConfig.defaultFilter = chartDefaultFilters[j].fieldVal
							.join(',');
				}
			}
		}
		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['description'] = config.description || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['group'] = config.group;
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig['absolute'] = config.absolute || "No";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig['showlegend'] = config.showlegend || 'no';
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.measurelabel = config.measurelabel;
		graphConfig.dynamicdimension = config.dynamicdimension || "";
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig['externalradiuspadding'] = config.externalradiuspadding || "";
		graphConfig['innerradius'] = config.innerradius || "";
		graphConfig.showtotal = config.showtotal || "no";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawDonut(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Pie');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-row':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		if (chartDefaultFilters && chartDefaultFilters.length > 0) {
			for (var j = 0; j < chartDefaultFilters.length; j++) {
				if (chartDefaultFilters[j].fieldName === config.fieldname) {
					config.defaultFiltervalue = chartDefaultFilters[j].fieldVal
							.join(',');
					graphConfig.defaultFilter = chartDefaultFilters[j].fieldVal
							.join(',');
				}
			}
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['group'] = config.group;
		graphConfig['description'] = config.description || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['customsort'] = config.customsort || "";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig.barwidth = config.barwidth || "auto";
		graphConfig['containerheight'] = config.containerheight || "";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['labelSuffix']=config.labelSuffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.excludeitems = config.excludeitems || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig.measurelabel = config.measurelabel;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.showselectedtext = config.showselectedtext || "no";
		graphConfig.showtotal = config.showtotal || "yes";
		graphConfig.dynamicdimension = config.dynamicdimension || "";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.prefixScale = config.prefixScale || true;
		graphConfig.yaxisinteger = config.yaxisinteger || true;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawRow(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bar');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;

	case 'chart-multilevel':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['group'] = config.group;
		graphConfig['description'] = config.description || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.measurelabel = config.measurelabel;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.rootname = (config.measurelabel && config.measurelabel.root) ? config.measurelabel.root
				: "Total"
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.showtotal = config.showtotal;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.colors = [];
		graphConfig.colors[0] = config["color-1"] !== "#000000" ? config["color-1"]
				: '';
		var charts = [];
		for (var x = 1; x < 6; x++) {
			graphConfig.colors[x] = (config["color-" + x] && config["color-"
					+ x] !== "#000000") ? config["color-" + x] : '';
			var chart = {
				"aggregate" : config["aggre-" + x],
				"level" : config["level-" + x],
				"name" : config["name-" + x] || config["name" + x]
			};
			charts.push(chart);
		}
		graphConfig.colors[6] = (config["color-5"] && config["color-5"] !== "#000000") ? config["color-5"]
				: '';
		graphConfig['charts'] = charts;
		graphConfig['linkaction'] = config.linkaction || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawMultiLevelRow(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bar');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-paired-row':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['group'] = config.group;
		graphConfig['description'] = config.description || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig.pairedfield = config.pairedfield;
		graphConfig.rightrowkey = config.rightrowkey;
		graphConfig.leftrowkey = config.leftrowkey;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig['linkaction'] = config.linkaction || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawPairedRow(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bar');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-bar':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		if (chartDefaultFilters && chartDefaultFilters.length > 0) {
			for (var j = 0; j < chartDefaultFilters.length; j++) {
				if (chartDefaultFilters[j].fieldName === config.fieldname) {
					config.defaultFiltervalue = chartDefaultFilters[j].fieldVal
							.join(',');
					graphConfig.defaultFilter = chartDefaultFilters[j].fieldVal
							.join(',');
				}
			}
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['description'] = config.description || '';
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig.barwidth = config.barwidth || "auto";
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['customsort'] = config.customsort || "";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig['showtotal'] = config.showtotal || "yes";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.codetype = config.codetype;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig.measurelabel = config.measurelabel;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.dynamicdimension = config.dynamicdimension || "";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig.showtable = config.showtable || "no";
		graphConfig.zerovalues = config.zerovalues || "yes";
		graphConfig.prefixScale = config.prefixScale || true;
		graphConfig.renderFunction = config.renderFunction || "";
		graphConfig.yaxisticks = config.yaxisticks || 10;
		graphConfig.yaxisinteger = config.yaxisinteger || true;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawBar(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bar');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-grouped-bar':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig.barwidth = config.barwidth || "auto";
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['stack'] = config.stack;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['type'] = "group";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig['showtotal'] = config.showtotal || "yes";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig.legendxposition = parseFloat(config.legendxposition) == config.legendxposition ? config.legendxposition
				: 0.06;
		graphConfig.legendyposition = parseFloat(config.legendyposition) == config.legendyposition ? config.legendyposition
				: 0;
		graphConfig.measurelabel = config.measurelabel;
		graphConfig.linklabel = config.linklabel;
		graphConfig.showonlybardata = config.showonlybardata;
		graphConfig.legendhorizontal = config.legendhorizontal === "true" ? true
				: false;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawGroupedBar(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bar');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-stacked':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig.barwidth = config.barwidth || "auto";
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['stack'] = config.stack;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['type'] = "stack";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig.legendxposition = parseFloat(config.legendxposition) == config.legendxposition ? config.legendxposition
				: 0.06;
		graphConfig.legendyposition = parseFloat(config.legendyposition) == config.legendyposition ? config.legendyposition
				: 0;
		graphConfig.measurelabel = config.measurelabel;
		graphConfig.legendhorizontal = config.legendhorizontal === "true" ? true
				: false;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig.linklabel = config.linklabel;
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawGroupedBar(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bar');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-grouped-barline':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig.legendxposition = parseFloat(config.legendxposition) == config.legendxposition ? config.legendxposition
				: 0.06;
		graphConfig.legendyposition = parseFloat(config.legendyposition) == config.legendyposition ? config.legendyposition
				: 0;
		graphConfig.measurelabel = config.measurelabel ? config.measurelabel
				: [];
		graphConfig.legendhorizontal = config.legendhorizontal === "true" ? true
				: false;
		graphConfig.renderdatapoints = config.renderdatapoints === "true" ? true
				: false;
		graphConfig.renderarea = config.renderarea === "true" ? true : false;
		graphConfig.interpolate = config.interpolate;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.showtotal = config.showtotal;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig.showtable = config.showtable || "no";
		graphConfig.barwidth = config.barwidth;
		var charts = [];
		for (var x = 1; x < 6; x++) {
			if (config["chart-type-" + x] && config["chart-type-" + x] !== null) {
				var chart = {
					"type" : config["chart-type-" + x],
					"aggregate" : config["aggre-" + x],
					"group" : config["measure-" + x],
					"color" : config["color-" + x],
					"name" : config["name-" + x],
					"dash" : config["dash-" + x] || "0,0",
				};
				charts.push(chart);
			}
		}
		graphConfig['charts'] = charts;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawGroupedBarLine(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Grouped Bar Line');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-composite':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig.legendxposition = parseFloat(config.legendxposition) == config.legendxposition ? config.legendxposition
				: 0.06;
		graphConfig.legendyposition = parseFloat(config.legendyposition) == config.legendyposition ? config.legendyposition
				: 0;
		graphConfig.measurelabel = config.measurelabel ? config.measurelabel
				: [];
		graphConfig.legendhorizontal = config.legendhorizontal === "true" ? true
				: false;
		graphConfig.renderdatapoints = config.renderdatapoints === "true" ? true
				: false;
		graphConfig.renderarea = config.renderarea === "true" ? true : false;
		graphConfig.interpolate = config.interpolate;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.showtotal = config.showtotal;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig.showtable = config.showtable || "no";
		var charts = [];
		for (var x = 1; x < 6; x++) {
			if (config["chart-type-" + x] && config["chart-type-" + x] !== null) {
				var chart = {
					"type" : config["chart-type-" + x],
					"aggregate" : config["aggre-" + x],
					"group" : config["measure-" + x],
					"color" : config["color-" + x],
					"name" : config["name-" + x],
					"dash" : config["dash-" + x] || "0,0",
				};
				charts.push(chart);
			}
		}
		graphConfig['charts'] = charts;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawCompositeChart(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Pie');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-line':
		graphConfig = {
			html : "#" + config.alias,
			lineHTML : "#timeline-" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['group'] = config.group || '';
		graphConfig['barHTML'] = "#timebar-" + config.alias || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartheight'] = config.chartheight || 'Auto';
		graphConfig['chartwidth'] = config.chartwidth || 'Auto';
		graphConfig['map'] = config.mapFunction || '';
		graphConfig['reduce'] = config.reduceFunction || '';
		graphConfig['count'] = config.countFunction || '';
		graphConfig['extractMonth'] = config.extractMonth || '';
		graphConfig['title'] = config.titleFunction || '';
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig['timelinetype'] = config.timelinetype || "months";
		graphConfig['hidechart'] = config.hidechart || "";
		graphConfig['hideyaxisline'] = config.hideyaxisline || "";
		graphConfig['hideyaxisbar'] = config.hideyaxisbar || "";
		graphConfig.autoplaytype = config.autoplaytype || 'day';
		graphConfig.animation = config.animation || 300;
		graphConfig.playunit = config.playunit || 1;
		graphConfig.autoplay = config.autoplay || null;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalueFrom = config.defaultFiltervalueFrom
				|| null;
		graphConfig.defaultFiltervalueTo = config.defaultFiltervalueTo || null;
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawTimeLineChart(graphConfig);
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'imagegrid':
		graphConfig = {
			html : config.alias
		};
		graphConfig['filteredColumn'] = config.filteredColumn;
		graphConfig['imageColumn'] = config.imageColumn;
		graphConfig['source'] = config.source;
		graphConfig['database'] = config.database;
		graphConfig['table'] = config.table;
		graphConfig['filters'] = filters;
		this.grapher.drawImageGrid(graphConfig);
		break;
	case 'datatable':
		if (config.columns === undefined || config.columns === null)
			throw new Error('Data Table needs a attribute coloum');
		graphConfig = {
			html : config.alias
		};
		graphConfig['columns'] = config.columns || [];
		graphConfig['data'] = config.data || '';
		graphConfig['filters'] = filters;
		graphConfig['isEditable'] = config.isEditable;
		graphConfig['editType'] = config.editType;
		graphConfig['fixedRightColumn'] = config.fixedRightColumn || 0;
		graphConfig['fixedLeftColumn'] = config.fixedLeftColumn || 0;
		graphConfig['enableAudit'] = config.enableAudit;
		graphConfig['showAudit'] = config.showAudit;
		graphConfig['pageLength'] = config.pageLength;
		graphConfig['fieldMapping'] = config.filtermapping;
		graphConfig['enableAdd'] = config.enableAdd;
		graphConfig['enableBulkUpdate'] = config.enableBulkUpdate;
		graphConfig['hideNoOfRows'] = config.hideNoOfRows;
		graphConfig['hideShowHide'] = config.hideShowHide;
		graphConfig['hidePagination'] = config.hidePagination;
		graphConfig['hideSearch'] = config.hideSearch;
		graphConfig['hideFilter'] = config.hideFilter;
		graphConfig['isOverride'] = config.isOverride;
		graphConfig['isFilterNotapplicable'] = config.isFilterNotapplicable;
		
		profileTableMap[graphConfig.html] = graphConfig;
		this.grapher.drawDataTableJ(graphConfig);
		/*
		 * if(config.data.serverType === "clientSide"){
		 * this.grapher.dataTableClientSide(graphConfig); } else{
		 * this.grapher.drawDataTableJ(graphConfig); }
		 */
		break;
	case 'calendar':
		graphConfig = {
			html : "#" + config.alias
		};
		graphConfig['filters'] = filters;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['source'] = config.source;
		graphConfig['database'] = config.database;
		graphConfig['defaultdate'] = config.defaultdate;
		graphConfig['table'] = config.table;
		graphConfig['eventid'] = config.eventid;
		graphConfig['eventname'] = config.eventname;
		graphConfig['eventstart'] = config.eventstart;
		graphConfig['eventend'] = config.eventend;
		graphConfig['eventcolor'] = config.eventcolor || '';
		graphConfig['eventclickcallback'] = config.eventclickcallback || '';
		graphConfig['chartheight'] = config.chartheight ? parseInt(config.chartheight)
				: 'auto';
		graphConfig['chartwidth'] = config.chartwidth ? parseInt(config.chartwidth)
				: 'auto';
		graphConfig['linkaction'] = config.linkaction || "";
		this.grapher.drawCalendar(graphConfig);
		/*
		 * this.grapher.drawCalendar(graphConfig, function( success) { if
		 * (!success) console.log('Error Drawing Table'); });
		 */
		break;
	case 'NGram':
		var _THIS = this;
		graphConfig = {
			html : "#" + config.alias
		};
		graphConfig['filters'] = filters;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['source'] = config.source;
		graphConfig['database'] = config.database;
		graphConfig['table'] = config.tableNGram;
		graphConfig['tableField'] = config.tableField;
		graphConfig['ngramType'] = config.ngramType;
		graphConfig['ngramSynonyms'] = config.ngramSynonyms;
		graphConfig['ngramStopwords'] = config.ngramStopwords;
		graphConfig['ngramLemmatization'] = config.ngramLemmatization;
		loadScript("js/plugin/d3.layout.cloud.js", function() {
			_THIS.grapher.drawNGram(graphConfig);
		});
		break;
	case 'IFrame':
		var _THIS = this;
		graphConfig = {
			html : "#" + config.alias,
			iframe : config.IFrameURL
		};
		_THIS.grapher.drawIFrame(graphConfig);
		break;
	case 'HTMLTemplate':
		var _THIS = this;
		graphConfig = {
			html : "#" + config.alias,
			template : config.HTMLTemplateContent,
			layout : config.contentLayout
		};
		_THIS.grapher.drawHTMLTemplate(graphConfig);
		break;
	case 'chart-geo':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['group'] = config.group;
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['chartColor'] = config.chartcolor || "Oranges";
		graphConfig['bubble'] = config.bubble;
		graphConfig['latitude'] = config.latitude;
		graphConfig['longitude'] = config.longitude;
		graphConfig.maptype = config.maptype || 'usa';
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['label'] = config.label;
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig['startColor'] = config.startColor || "red";
		graphConfig['endColor'] = config.endColor || "#e377c2";
		graphConfig['defaultFill'] = config.defaultFill || "#ddd";
		graphConfig['projection'] = config.projection || 'mercator';
		graphConfig['borderColor'] = config.borderColor || '#999999';
		graphConfig['highlightFillColor'] = config.highlightFillColor
				|| '#bbaa99';
		graphConfig['highlightBorderColor'] = config.highlightBorderColor
				|| '#999999';
		graphConfig['showCountryLabels'] = config.showCountryLabels || "No";

		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawUSChart(graphConfig);
			$(".dc-data-count").show();
		}
		break;
	case 'chart-map':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['group'] = config.group;
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['chartColor'] = config.chartcolor || "Oranges";
		graphConfig['bubble'] = config.bubble;
		graphConfig['latitude'] = config.latitude;
		graphConfig['longitude'] = config.longitude;
		graphConfig.zoom = config.zoom || 'no';
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['label'] = config.label;
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig['tooltipkey'] = config.tooltipkey || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawGoogleMap(graphConfig);
		}
		break;
	case 'chart-distribution':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['radiusPrefix'] = config["radius-prefix"] || "";
		graphConfig['xAxisPrefix'] = config["xaxis-prefix"] || "";
		graphConfig['yAxisPrefix'] = config["yaxis-prefix"] || "";
		graphConfig['xAxisAggre'] = config["xaxis-aggre"] || "sum";
		graphConfig['yAxisAggre'] = config["yaxis-aggre"] || "sum";
		graphConfig['radiusAggre'] = config["radius-aggre"] || "sum";
		graphConfig['xAxisLabel'] = config["xaxis-label"] || "";
		graphConfig['yAxisLabel'] = config["yaxis-label"] || "";
		graphConfig['radiusLabel'] = config["radius-label"] || "";
		graphConfig['xAxis'] = config.xaxis;
		graphConfig['yAxis'] = config.yaxis;
		graphConfig['radius'] = config.radius;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawDistributionChart(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing distribution chart');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-bubble':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['radiusPrefix'] = config["radius-prefix"] || "";
		graphConfig['xAxisPrefix'] = config["xaxis-prefix"] || "";
		graphConfig['yAxisPrefix'] = config["yaxis-prefix"] || "";
		graphConfig['radiusSuffix'] = config["radius-suffix"] || "";
		graphConfig['xAxisSuffix'] = config["xaxis-suffix"] || "";
		graphConfig['yAxisSuffix'] = config["yaxis-suffix"] || "";
		graphConfig['xAxisAggre'] = config["xaxis-aggre"] || "sum";
		graphConfig['yAxisAggre'] = config["yaxis-aggre"] || "sum";
		graphConfig['radiusAggre'] = config["radius-aggre"] || "sum";
		graphConfig['xAxisLabel'] = config["xaxis-label"] || "";
		graphConfig['yAxisLabel'] = config["yaxis-label"] || "";
		graphConfig['radiusLabel'] = config["radius-label"] || "";
		graphConfig['xAxis'] = config.xaxis;
		graphConfig['date-' + config.xaxis] = config["data-" + config.xaxis];
		graphConfig['yAxis'] = config.yaxis;
		graphConfig['radius'] = config.radius;
		graphConfig.maxbubblesize = config.maxbubblesize || "0.07";
		graphConfig['xaxisformate'] = config.xaxisformate;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.elastic = config.elastic;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.xaxistype = config.xaxistype || "number";
		graphConfig.xaxisinterval = config.xaxisinterval || null;
		graphConfig.xaxistooltipdateformat = config.xaxistooltipdateformat
				|| "%m-%d-%Y";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawBubble(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bubble Chart');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-scatter':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['radiusPrefix'] = config["radius-prefix"] || "";
		graphConfig['xAxisPrefix'] = config["xaxis-prefix"] || "";
		graphConfig['yAxisPrefix'] = config["yaxis-prefix"] || "";
		graphConfig['radiusSuffix'] = config["radius-suffix"] || "";
		graphConfig['xAxisSuffix'] = config["xaxis-suffix"] || "";
		graphConfig['yAxisSuffix'] = config["yaxis-suffix"] || "";
		graphConfig['xAxisAggre'] = config["xaxis-aggre"] || "sum";
		graphConfig['yAxisAggre'] = config["yaxis-aggre"] || "sum";
		graphConfig['radiusAggre'] = config["radius-aggre"] || "sum";
		graphConfig['xAxisLabel'] = config["xaxis-label"] || "";
		graphConfig['yAxisLabel'] = config["yaxis-label"] || "";
		graphConfig['radiusLabel'] = config["radius-label"] || "";
		graphConfig['xAxis'] = config.xaxis;
		graphConfig['date-' + config.xaxis] = config["data-" + config.xaxis];
		graphConfig['yAxis'] = config.yaxis;
		graphConfig['radius'] = config.radius;
		graphConfig['xaxisformate'] = config.xaxisformate;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.elastic = config.elastic;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.xaxistype = config.xaxistype || "number";
		graphConfig.xaxisinterval = config.xaxisinterval || null;
		graphConfig.xaxistooltipdateformat = config.xaxistooltipdateformat
				|| "%m-%d-%Y";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig.brushOn = config.brushon;
		graphConfig.yAxisNumberFormat = config.yaxisnumberformat || "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawScatterPlot(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bubble Chart');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-series':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['radiusPrefix'] = config["radius-prefix"] || "";
		graphConfig['xAxisPrefix'] = config["xaxis-prefix"] || "";
		graphConfig['yAxisPrefix'] = config["yaxis-prefix"] || "";
		graphConfig['radiusSuffix'] = config["radius-suffix"] || "";
		graphConfig['xAxisSuffix'] = config["xaxis-suffix"] || "";
		graphConfig['yAxisSuffix'] = config["yaxis-suffix"] || "";
		graphConfig['xAxisAggre'] = config["xaxis-aggre"] || "sum";
		graphConfig['yAxisAggre'] = config["yaxis-aggre"] || "sum";
		graphConfig['radiusAggre'] = config["radius-aggre"] || "sum";
		graphConfig['xAxisLabel'] = config["xaxis-label"] || "";
		graphConfig['yAxisLabel'] = config["yaxis-label"] || "";
		graphConfig['radiusLabel'] = config["radius-label"] || "";
		graphConfig['xAxis'] = config.xaxis;
		graphConfig['date-' + config.xaxis] = config["data-" + config.xaxis];
		graphConfig['yAxis'] = config.yaxis;
		graphConfig['radius'] = config.radius;
		graphConfig['xaxisformate'] = config.xaxisformate;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.orderby = config.orderby;
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.elastic = config.elastic;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.xaxistype = config.xaxistype || "number";
		graphConfig.xaxisinterval = config.xaxisinterval || null;
		graphConfig.xaxistooltipdateformat = config.xaxistooltipdateformat
				|| "%m-%d-%Y";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig.brushOn = config.brushon === "true" ? true : false;
		graphConfig.yAxisNumberFormat = config.yaxisnumberformat || "";
		graphConfig.legendxposition = parseFloat(config.legendxposition) == config.legendxposition ? config.legendxposition
				: 0.06;
		graphConfig.legendyposition = parseFloat(config.legendyposition) == config.legendyposition ? config.legendyposition
				: 0;
		graphConfig.legendhorizontal = config.legendhorizontal === "true" ? true
				: false;
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.interpolate = config.interpolate;
		graphConfig.legendorder = config.legendorder ? config.legendorder : '';
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig.measurelabel = config.measurelabel ? config.measurelabel
				: [];
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawSeries(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bubble Chart');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	case 'chart-heatmap':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['containerheight'] = config.containerheight || "";
		graphConfig['radiusPrefix'] = config["radius-prefix"] || "";
		graphConfig['xAxisPrefix'] = config["xaxis-prefix"] || "";
		graphConfig['yAxisPrefix'] = config["yaxis-prefix"] || "";
		graphConfig['radiusSuffix'] = config["radius-suffix"] || "";
		graphConfig['xAxisSuffix'] = config["xaxis-suffix"] || "";
		graphConfig['yAxisSuffix'] = config["yaxis-suffix"] || "";
		graphConfig['xAxisAggre'] = config["xaxis-aggre"] || "sum";
		graphConfig['yAxisAggre'] = config["yaxis-aggre"] || "sum";
		graphConfig['radiusAggre'] = config["radius-aggre"] || "sum";
		graphConfig['xAxisLabel'] = config["xaxis-label"] || "";
		graphConfig['yAxisLabel'] = config["yaxis-label"] || "";
		graphConfig['radiusLabel'] = config["radius-label"] || "";
		graphConfig['xAxis'] = config.xaxis;
		graphConfig['date-' + config.xaxis] = config["data-" + config.xaxis];
		graphConfig['yAxis'] = config.yaxis;
		graphConfig['radius'] = config.radius;
		graphConfig['xaxisformate'] = config.xaxisformate || "%Y/%m";
		graphConfig['xaxisdateformat'] = config.xaxisdateformat || "%Y/%m";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.elastic = config.elastic;
		graphConfig.orderby = config.orderby;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.xaxistype = config.xaxistype || "number";
		graphConfig.yaxistype = config.yaxistype || "number";
		graphConfig.color1 = config["color-1"] || "#FFD1DC";
		graphConfig.color2 = config["color-2"] || "#560319";
		graphConfig.xaxisinterval = config.xaxisinterval || null;
		graphConfig.xaxistooltipdateformat = config.xaxistooltipdateformat
				|| "%m-%d-%Y";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.tooltipkey = config.tooltipkey;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawHeatMap(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Bubble Chart');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	// added code for Treemap
	case 'chart-treemap':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['group'] = config.group;
		graphConfig['description'] = config.description || '';
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.measurelabel = config.measurelabel;
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig.rootname = (config.measurelabel && config.measurelabel.root) ? config.measurelabel.root
				: "Total"
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.showtotal = config.showtotal;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig.colors = [];
		graphConfig.colors[0] = config["color-1"] !== "#000000" ? config["color-1"]
				: '';
		var charts = [];
		for (var x = 1; x < 6; x++) {
			graphConfig.colors[x] = (config["color-" + x] && config["color-"
					+ x] !== "#000000") ? config["color-" + x] : '';
			var chart = {
				"aggregate" : config["aggre-" + x],
				"level" : config["level-" + x],
				"name" : config["name-" + x] || config["name" + x]
			};
			charts.push(chart);
		}
		graphConfig.colors[6] = (config["color-5"] && config["color-5"] !== "#000000") ? config["color-5"]
				: '';
		graphConfig['charts'] = charts;
		graphConfig['linkaction'] = config.linkaction || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawTreeMap(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Tree Map');
			});
			$(".dc-data-count").show();
		}
		break;
	case 'table':
		if (config.coloums === undefined || config.coloums === null)
			throw new Error('Data Table needs a attribute coloum');
		graphConfig = {
			html : config.alias,
			field : config.field
		};
		if (config.groupon !== undefined)
			graphConfig['group'] = config.field;
		else
			graphConfig['group'] = config.groupon;

		graphConfig['coloums'] = config.coloums || [];
		graphConfig['sortBy'] = config.sortby || '';
		graphConfig['renderlet'] = config.renderlet || '';
		graphConfig['summarydiv'] = config.summarydiv || '';
		graphConfig['hasChildren'] = config.hasChildren || '';
		graphConfig['childReqParam'] = config.childrenParams || '';
		graphConfig['childname'] = config.childGraphName || '';
		graphConfig['coloumsChild'] = config.childrenColoums || '';
		graphConfig['childurl'] = config.childurl || '';
		graphConfig['childDataKey'] = config.childDataKey || '';
		graphConfig['childMapping'] = config.childMappings || '';
		graphConfig['childDrillDown'] = config.childDrillDown;
		if (config.hasChildren) {
			this.grapher.drawDataTableChild(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Table');
			});
		} else {
			this.grapher.drawDataTableJ(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Table');
			});
		}

		// drawDataTable(config.htmlElement,
		// config.field,
		// dataCount, config.coloums);
		break;
	case 'chart-hierarchie':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		if (config['level-1'])
			graphConfig['level-1'] = config["level-1"] || "0";
		if (config['level-2'])
			graphConfig['level-2'] = config["level-2"] || "0";
		if (config['level-3'])
			graphConfig['level-3'] = config["level-3"] || "0";
		if (config['level-4'])
			graphConfig['level-4'] = config["level-4"] || "0";

		if (config['level-1-desc'])
			graphConfig['level-1-desc'] = config["level-1-desc"] || "";
		if (config['level-2-desc'])
			graphConfig['level-2-desc'] = config["level-2"] || "";
		if (config['level-3-desc'])
			graphConfig['level-3-desc'] = config["level-3-desc"] || "";
		if (config['level-4-desc'])
			graphConfig['level-4-desc'] = config["level-4-desc"] || "";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawHierarchie(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Pie');
			});
			$(".dc-data-count").show();
		}
		break;
	case 'chart-slider':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};

		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['defaultFilterFromvalue'] = config.defaultFiltervalueFrom
				|| "0";
		graphConfig['defaultFilterTovalue'] = config.defaultFiltervalueTo
				|| "100";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.codetype = config.codetype;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		if (config['level-1'])
			graphConfig['level-1'] = config["level-1"] || "0";
		if (config['level-2'])
			graphConfig['level-2'] = config["level-2"] || "0";
		if (config['level-3'])
			graphConfig['level-3'] = config["level-3"] || "0";
		if (config['level-4'])
			graphConfig['level-4'] = config["level-4"] || "0";
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawSlider(graphConfig);
			$(".dc-data-count").show();
		}
		break;
	case 'chart-composite-brush':
		graphConfig = {
			html : "#" + config.alias,
			field : config.alias,
			fieldname : config.fieldname
		};
		if (chartDefaultFilters && chartDefaultFilters.length > 0) {
			for (var j = 0; j < chartDefaultFilters.length; j++) {
				if (chartDefaultFilters[j].fieldName === config.fieldname) {
					config.defaultFiltervalue = chartDefaultFilters[j].fieldVal
							.join(',');
					graphConfig.defaultFilter = chartDefaultFilters[j].fieldVal
							.join(',');
				}
			}
		}
		if (config.grouponcount !== undefined && config.grouponcount) {
			graphConfig['count'] = data.length;
		}
		graphConfig['filter'] = config.filter || null;
		graphConfig['filtervalue'] = config.filtervalue ? config.filtervalue
				.split(";") : null;
		graphConfig['description'] = config.description || '';
		graphConfig['groupRound'] = config.groupRound || '';
		graphConfig['resetHandler'] = config.resetHandler || '';
		graphConfig['group'] = config.group;
		graphConfig['aggregate'] = config.aggregate || "sum";
		graphConfig['chartwidth'] = config.chartwidth || "Auto";
		graphConfig['chartheight'] = config.chartheight || "Auto";
		graphConfig['numFormat'] = config.numformat || ",~f";
		graphConfig['prefix'] = config.prefix || "";
		graphConfig['suffix'] = config.suffix || "";
		graphConfig['xaxisorientation'] = config.xaxisorientation || "";
		graphConfig['xaxislabel'] = config.xaxislabel || "";
		graphConfig['yaxislabel'] = config.yaxislabel || "";
		graphConfig['orderby'] = config.orderby || "0";
		graphConfig.colorcodes = config.colorcodes || null;
		graphConfig.rangebandpadding = config.rangebandpadding || "no";
		graphConfig.codetype = config.codetype;
		graphConfig.colorfield = config.colorfield || null;
		graphConfig.filterfrom = config.filterfrom || null;
		graphConfig.filtertype = config.filtertype || null;
		graphConfig.filterdefaultval = config.filterdefaultval || null;
		graphConfig.filtervalues = config.filtervalues || null;
		graphConfig.defaultFiltervalue = config.defaultFiltervalue || null;
		graphConfig['hideyaxis'] = config.hideyaxis || "";
		graphConfig['hidexaxis'] = config.hidexaxis || "";
		graphConfig.margintop = config.margintop || 20;
		graphConfig.marginbottom = config.marginbottom
				|| (graphConfig.xaxisorientation ? 60 : 30);
		graphConfig.marginright = config.marginright || 10;
		graphConfig.marginleft = config.marginleft || 40;
		graphConfig.legendxposition = parseFloat(config.legendxposition) == config.legendxposition ? config.legendxposition
				: 0.06;
		graphConfig.legendyposition = parseFloat(config.legendyposition) == config.legendyposition ? config.legendyposition
				: 0;
		graphConfig.measurelabel = config.measurelabel ? config.measurelabel
				: [];
		graphConfig.legendhorizontal = config.legendhorizontal === "true" ? true
				: false;
		graphConfig.renderdatapoints = config.renderdatapoints === "true" ? true
				: false;
		graphConfig.renderarea = config.renderarea === "true" ? true : false;
		graphConfig.interpolate = config.interpolate;
		graphConfig['calculatedfield'] = config.calculatedfield || "";
		graphConfig.showpercentage = config.showpercentage || "yes";
		graphConfig.showtotal = config.showtotal;
		graphConfig.tooltipkey = config.tooltipkey;
		graphConfig.calculatedformula = config.calculatedformula ? config.calculatedformula
				.replace(/[\[\"]+/g, "d.value['").replace(/[\]\"]+/g, "']")
				: "";
		graphConfig['linkaction'] = config.linkaction || "";
		graphConfig.runningtotal = config.runningtotal || "no";
		graphConfig.showtable = config.showtable || "no";
		graphConfig.showtable = config.showtable || "no";
		graphConfig.barwidth = config.barwidth;
		var charts = [];
		for (var x = 1; x < 10; x++) {
			if (config["chart-type-" + x] && config["chart-type-" + x] !== null) {
				var chart = {
					"type" : config["chart-type-" + x],
					"aggregate" : config["aggre-" + x],
					"group" : config["measure-" + x],
					"color" : config["color-" + x],
					"name" : config["name-" + x],
					"symbol" : config["symbol-" + x] || "symbolTriangle",
					"dash" : config["dash-" + x] || "0,0",
					"symbolsize" : config["symbol-size-" + x] || 14,
				};
				charts.push(chart);
			}
		}
		graphConfig['charts'] = charts;
		if (this.grapher.ndx && this.grapher.ndx.dimension) {
			this.grapher.drawCompositeBrush(graphConfig, function(success) {
				if (!success)
					console.log('Error Drawing Pie');
			});
			$(".dc-data-count").show();
			$(graphConfig.html).find("h4").addClass("hide");
		}else{
			$(graphConfig.html).find("svg").remove();
			$(graphConfig.html).find("h4").removeClass("hide");
		}
		break;
	default:
		throw new Error('Chart Type not Found');
	}
	// /}

};
Profiling.prototype.init = function() {
	var config = this.config;
	var summary = this.summary;
	if ((!config || config.length === 0) && (summary.length < 1 || !summary)) {
		return;
	}
	var charts = [];
	for (var j = 0; j < config.length; j++) {
		if (!config[j].charts || config[j].charts.length === 0) {
			return;
		}
		var mapping = config[j].charts;
		for (var i = 0, len = mapping.length; i < len; i++) {
			charts.push(mapping[i]);
		}
	}
	if (summary && summary.length > 0) {
		for (var a = 0; a < summary.length; a++) {
			if (!summary[a] || summary[a].length === 0) {
				return;
			}
			var summaryMapping = summary[a];
			for (var b = 0; b < summaryMapping.length; b++) {
				charts.push(summaryMapping[b]);
			}
		}
	}
	this.loadRequiredJS(charts);
};
Profiling.prototype.loadRequiredJS = function(charts) {
	var calback;
	var _self = this;
	if (!charts || charts.length === 0)
		return;

	if (charts.length === 1) {
		calback = function() {
			_self.chartConfig();
		};
	} else {
		calback = function() {
			_self.loadRequiredJS(charts);
		};
	}
	var config = charts[0];
	var chartType = config.charttype || "summary";
	var chartHTML = config.html;
	charts = charts.slice(1, charts.length);
	if (chartType === "chart-slider")
		loadScript("js/plugin/ion-slider/ion.rangeSlider.min.js", calback,
				chartHTML);
	else if (chartType === "chart-geo")
		loadScript("js/plugin/topojson/topojson.js", function() {
			loadScript("js/plugin/datamaps-master/dist/datamaps.all.js",
					calback, chartHTML);
		}, chartHTML);
	else if (chartType === "chart-map") {
		loadScript("js/plugin/leaflet.js", function() {
			loadScript("js/plugin/leaflet.markercluster.js", function() {
				loadScript("js/plugin/dc-leaflet.js", calback, chartHTML);
			}, chartHTML);
		}, chartHTML);
	} else if (chartType === "calendar")
		if (config.eventclickcallback && config.eventclickcallback !== null
				&& $.trim(config.eventclickcallback) !== "")
			loadScript(
					"js/DataAnalyser/calendarDrilldown.js",
					function() {
						loadScript(
								"js/plugin/fullcalendar/lib/moment.min.js",
								function() {
									loadScript(
											"js/plugin/fullcalendar/fullcalendar.min.js",
											calback, chartHTML);
								}, chartHTML);
					}, chartHTML);
		else
			loadScript("js/plugin/fullcalendar/lib/moment.min.js", function() {
				loadScript("js/plugin/fullcalendar/fullcalendar.min.js",
						calback, chartHTML);
			}, chartHTML);
	else if (chartType === "datatable") {
		if (config.data && config.data.drilldowncallback !== null
				&& $.trim(config.data.drilldowncallback) !== "")
			loadScript("js/DataAnalyser/datatableDrilldown.js", null, chartHTML);
		else {
			for (var i = 0; i < config.columns.length; i++) {
				if (config.columns[i].renderfunction
						&& $.trim(config.columns[i].renderfunction) !== ""
						&& config.columns[i].renderfunction
								.indexOf('dataTableDrillDown') > -1) {
					loadScript("js/DataAnalyser/datatableDrilldown.js", null,
							chartHTML);
					break;
				}
			}
		}

		if (config.fixedRightColumn || config.fixedLeftColumn)
			loadScript("js/DataAnalyser/ActionCallbackHandler.js", function() {
				loadScript(
						"js/plugin/datatables/dataTables.fixedColumns.min.js",
						calback, chartHTML);
			}, chartHTML);
		else
			loadScript("js/DataAnalyser/ActionCallbackHandler.js", calback,
					chartHTML);

	} else if (chartType === "summary")
		loadScript("js/plugin/liquidFillGauge/liquidFillGauge.js", calback,
				config.id);
	else if (charts.length > 0)
		_self.loadRequiredJS(charts);
	else
		calback();

};
Profiling.prototype.chartConfig = function() {
	this.grapher = new Graphing(this.data, this.filters, this.masterData,
			this.viewSettings);
	var config = this.config;
	var filters = this.filters;
	var summary = this.summary;
	var chartDefaultFilters = this.chartDefaultFilters
	if (this.data && this.data.length > 0)
		this.grapher.resetAllFilters();
	this.destroyObject();

	if ((!config || config.length === 0) && (summary.length < 1 || !summary)) {
		return;
	}
	for (var j = 0; j < config.length; j++) {
		if (!config[j].charts || config[j].charts.length === 0) {
			return;
		}
		var mapping = config[j].charts;
		for (var i = 0, len = mapping.length; i < len; i++) {
			this.drawChart(mapping[i], filters, chartDefaultFilters);
		}
	}

	if (summary && summary.length > 0) {
		for (var j = 0; j < summary.length; j++) {
			if (summary[j] && summary[j].length > 0) {
				var mapping = summary[j];
				for (var i = 0, len = mapping.length; i < len; i++) {
					var config1 = liquidFillGaugeDefaultSettings();
					if(mapping[i].filter && $.trim(mapping[i].filter) !== ""){
						config1.filter = mapping[i].filter;
					}
					if(mapping[i].measurelabel && $.trim(mapping[i].measurelabel) !== ""){
						config1.measurelabel = mapping[i].measurelabel; 
					}
					if ((mapping[i]["min-value"] && $
							.trim(mapping[i]["min-value"]) !== "")
							|| (mapping[i]["minValue"] && $
									.trim(mapping[i]["minValue"]) !== ""))
						config1.minValue = mapping[i]["min-value"]
								|| mapping[i]["minValue"]; // The gauge minimum
					// value.
					if ((mapping[i]["max-value"] && $
							.trim(mapping[i]["max-value"]) !== "")
							|| (mapping[i]["maxValue"] && $
									.trim(mapping[i]["maxValue"]) !== ""))
						config1.maxValue = mapping[i]["max-value"]
								|| mapping[i]["maxValue"]; // The gauge maximum
					// value.
					else
						config1.maxValue = 0;
					if ((mapping[i]["circle-thickness"] && $
							.trim(mapping[i]["circle-thickness"]) !== "")
							|| (mapping[i]["circleThickness"] && $
									.trim(mapping[i]["circleThickness"]) !== ""))
						config1.circleThickness = mapping[i]["circle-thickness"]
								|| mapping[i]["circleThickness"]; // The outer
					// circle
					// thickness
					// as a
					// percentage
					// of it's
					// radius.
					if ((mapping[i]["circle-fillgap"] && $
							.trim(mapping[i]["circle-fillgap"]) !== "")
							|| (mapping[i]["circleFillgap"] && $
									.trim(mapping[i]["circleFillgap"]) !== ""))
						config1.circleFillGap = mapping[i]["circle-fillgap"]
								|| mapping[i]["circleFillgap"]; // The size of
					// the gap
					// between the
					// outer circle
					// and wave
					// circle as a
					// percentage of
					// the outer
					// circles
					// radius.
					if ((mapping[i]["circle-color"] && $
							.trim(mapping[i]["circle-color"]) !== "")
							|| (mapping[i]["circleColor"] && $
									.trim(mapping[i]["circleColor"]) !== ""))
						config1.circleColor = mapping[i]["circle-color"]
								|| mapping[i]["circleColor"]; // The color of
					// the outer
					// circle.
					if ((mapping[i]["wave-height"] && $
							.trim(mapping[i]["wave-height"]) !== "")
							|| (mapping[i]["waveHeight"] && $
									.trim(mapping[i]["waveHeight"]) !== ""))
						config1.waveHeight = mapping[i]["wave-height"]
								|| mapping[i]["waveHeight"]; // The wave
					// height as a
					// percentage of
					// the radius of
					// the wave
					// circle.
					if ((mapping[i]["wave-count"] && $
							.trim(mapping[i]["wave-count"]) !== "")
							|| (mapping[i]["waveCount"] && $
									.trim(mapping[i]["waveCount"]) !== ""))
						config1.waveCount = mapping[i]["wave-count"]
								|| mapping[i]["waveCount"]; // The number of
					// full waves per
					// width of the wave
					// circle.
					if ((mapping[i]["wave-risetime"] && $
							.trim(mapping[i]["wave-risetime"]) !== "")
							|| (mapping[i]["waveRisetime"] && $
									.trim(mapping[i]["waveRisetime"]) !== ""))
						config1.waveRiseTime = mapping[i]["wave-risetime"]
								|| mapping[i]["waveRisetime"]; // The amount of
					// time in
					// milliseconds
					// for the wave
					// to rise from
					// 0 to it's
					// final height.
					if ((mapping[i]["wave-animate-time"] && $
							.trim(mapping[i]["wave-animate-time"]) !== "")
							|| (mapping[i]["waveAnimateTime"] && $
									.trim(mapping[i]["waveAnimateTime"]) !== ""))
						config1.waveAnimateTime = mapping[i]["wave-animate-time"]
								|| mapping[i]["waveAnimateTime"]; // The
					// amount of
					// time in
					// milliseconds
					// for a
					// full wave
					// to enter
					// the wave
					// circle.
					if ((mapping[i]["wave-rise"] && $
							.trim(mapping[i]["wave-rise"]) !== "")
							|| (mapping[i]["waveRise"] && $
									.trim(mapping[i]["waveRise"]) !== ""))
						config1.waveRise = mapping[i]["wave-rise"]
								|| mapping[i]["waveRise"]; // Control if the
					// wave should rise
					// from 0 to it's
					// full height; or
					// start at it's
					// full height.
					if ((mapping[i]["wave-height-scaling"] && $
							.trim(mapping[i]["wave-height-scaling"]) !== "")
							|| (mapping[i]["waveHeightScaling"] && $
									.trim(mapping[i]["waveHeightScaling"]) !== ""))
						config1.waveHeightScaling = mapping[i]["wave-height-scaling"]
								|| mapping[i]["waveHeightScaling"]; // Controls
					
					if ((mapping[i]["wave-animate"] && $
							.trim(mapping[i]["wave-animate"]) !== "")
							|| (mapping[i]["waveAnimate"] && $
									.trim(mapping[i]["waveAnimate"]) !== ""))
						config1.waveAnimate = mapping[i]["wave-animate"]
								|| mapping[i]["waveAnimate"]; // Controls if
					// the wave
					// scrolls or is
					// static.
					if ((mapping[i]["wave-color"] && $
							.trim(mapping[i]["wave-color"]) !== "")
							|| (mapping[i]["waveColor"] && $
									.trim(mapping[i]["waveColor"]) !== ""))
						config1.waveColor = mapping[i]["wave-color"]
								|| mapping[i]["waveColor"]; // The color of the
					// fill wave.
					if ((mapping[i]["wave-offset"] && $
							.trim(mapping[i]["wave-offset"]) !== "")
							|| (mapping[i]["waveOffset"] && $
									.trim(mapping[i]["waveOffset"]) !== ""))
						config1.waveOffset = mapping[i]["wave-offset"]
								|| mapping[i]["waveOffset"]; // The amount to
					// initially
					// offset the
					// wave. 0 = no
					// offset. 1 =
					// offset of one
					// full wave.
					if ((mapping[i]["text-vert-position"] && $
							.trim(mapping[i]["text-vert-position"]) !== "")
							|| (mapping[i]["textVertPosition"] && $
									.trim(mapping[i]["textVertPosition"]) !== ""))
						config1.textVertPosition = mapping[i]["text-vert-position"]
								|| mapping[i]["textVertPosition"]; // The
					// height at
					// which to
					// display
					// the
					// percentage
					// text
					// withing
					// the wave
					// circle. 0
					// = bottom;
					// 1 = top.
					if ((mapping[i]["text-size"] && $
							.trim(mapping[i]["text-size"]) !== "")
							|| (mapping[i]["textSize"] && $
									.trim(mapping[i]["textSize"]) !== ""))
						config1.textSize = mapping[i]["text-size"]
								|| mapping[i]["textSize"]; // The relative
					// height of the
					// text to display
					// in the wave
					// circle. 1 = 50%
					if ((mapping[i]["text-color"] && $
							.trim(mapping[i]["text-color"]) !== "")
							|| (mapping[i]["textColor"] && $
									.trim(mapping[i]["textColor"]) !== ""))
						config1.textColor = mapping[i]["text-color"]
								|| mapping[i]["textColor"]; // The color of the
					// value text when
					// the wave does not
					// overlap it.
					if ((mapping[i]["text-wave-color"] && $
							.trim(mapping[i]["text-wave-color"]) !== "")
							|| (mapping[i]["textWaveColor"] && $
									.trim(mapping[i]["textWaveColor"]) !== ""))
						config1.waveTextColor = mapping[i]["text-wave-color"]
								|| mapping[i]["textWaveColor"]; // The color of
					// the value
					// text when the
					// wave overlaps
					// it.
					if (mapping[i]["prefix"]
							&& $.trim(mapping[i]["prefix"]) !== "")
						config1.prefix = mapping[i]["prefix"];
					if (mapping[i]["suffix"]
							&& $.trim(mapping[i]["suffix"]) !== "")
						config1.suffix = mapping[i]["suffix"];
					if (mapping[i]["detail"]
							&& $.trim(mapping[i]["detail"]) !== "")
						config1.detailText = mapping[i]["detail"];
					if ((mapping[i]["detail-size"] && $
							.trim(mapping[i]["detail-size"]) !== "")
							|| (mapping[i]["detailSize"] && $
									.trim(mapping[i]["detailSize"]) !== ""))
						config1.detailTextSize = mapping[i]["detail-size"]
								|| mapping[i]["detailSize"];
					if ((mapping[i]["detail-color"] && $
							.trim(mapping[i]["detail-color"]) !== "")
							|| (mapping[i]["detailColor"] && $
									.trim(mapping[i]["detailColor"]) !== ""))
						config1.detailTextColor = mapping[i]["detail-color"]
								|| mapping[i]["detailColor"];
					if ((mapping[i]["detail-vert-position"] && $
							.trim(mapping[i]["detail-vert-position"]) !== "")
							|| (mapping[i]["detailVertPosition"] && $
									.trim(mapping[i]["detailVertPosition"]) !== ""))
						config1.detailTextVertPosition = mapping[i]["detail-vert-position"]
								|| mapping[i]["detailVertPosition"];
					if (mapping[i]["description"]
							&& $.trim(mapping[i]["description"]) !== "")
						config1.descText = mapping[i]["description"];
					if ((mapping[i]["desc-size"] && $
							.trim(mapping[i]["desc-size"]) !== "")
							|| (mapping[i]["descSize"] && $
									.trim(mapping[i]["desCSize"]) !== ""))
						config1.descTextSize = mapping[i]["desc-size"]
								|| mapping[i]["desCSize"];
					if ((mapping[i]["desc-color"] && $
							.trim(mapping[i]["desc-color"]) !== "")
							|| (mapping[i]["descColor"] && $
									.trim(mapping[i]["descColor"]) !== ""))
						config1.descTextColor = mapping[i]["desc-color"]
								|| mapping[i]["descColor"];
					if ((mapping[i]["desc-vert-position"] && $
							.trim(mapping[i]["desc-vert-position"]) !== "")
							&& (mapping[i]["descVertPosition"] && $
									.trim(mapping[i]["descVertPosition"]) !== ""))
						config1.descTextVertPosition = mapping[i]["desc-vert-position"]
								|| mapping[i]["descVertPosition"];
					if ((mapping[i]["date-format"] && $
							.trim(mapping[i]["date-format"]) !== "")
							&& (mapping[i]["dateFormat"] && $
									.trim(mapping[i]["dateFormat"]) !== ""))
						config1.dateFormat = mapping[i]["date-format"]
								|| mapping[i]["dateFormat"];

					config1.fieldType = mapping[i]["field-type"]
							|| mapping[i]["fieldType"] || "string";
					if (config1.fieldType !== "string") {
						config1.textFormat = mapping[i]["format"] || "%b %y";
					} else {
						config1.textFormat = mapping[i]["format"] || ".4s";
					}
					if (mapping[i].valuetype) {
						config1.valuetype = mapping[i].valuetype;
					}
					mapping[i].calculateformula = mapping[i].calculatedformula ? mapping[i].calculatedformula
							.replace(/[\[\"]+/g, "d.value['").replace(
									/[\]\"]+/g, "']")
							: "";
					mapping[i].fieldType = mapping[i]["field-type"]
							|| mapping[i]["fieldType"] || "string";
					mapping[i].textFormat = mapping[i]["format"] || ".4s";
					mapping[i].liquidFillGaugeSettings = config1;
					if (this.grapher.ndx && this.grapher.ndx.dimension) {
						this.grapher.drawSummary(mapping[i], function(success) {
							if (!success)
								console.log('Error Drawing Bar');
						});
					}
				}
			}
		}
	}
};