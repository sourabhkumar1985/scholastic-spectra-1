var Graphing = function(data, filters, masterData,viewSettings) {
	var test;
	if (data && data.length > 0) {
		localStorage["datatablefilter"] = "";
		this.ndx = crossfilter(data);
		data = null;
		this.color = getRandomColors(100);
	} else {
		this.ndx = [];
	}
	$("#content").mousemove(function( event ) {
		  var xcord = event.pageX;
		  var ycord = event.pageY;
		  var flagTool = 0;
		  $(".d3-tip").each(function(){
			 var position = $(this).position();
			 var sourceHeight = $(this).attr("source-height");
			 var sourceWidth = $(this).attr("source-width");
			 sourceHeight = parseInt(sourceHeight) || 0;
			 sourceWidth = parseInt(sourceWidth) || 0;
			 sourceWidth = sourceWidth > 30 ? sourceWidth:30;
			 if ((ycord - position.top - $(this).height() -30) > sourceHeight || position.top > ycord || (Math.abs(position.left + 10 + ($(this).width()/2) - xcord) >  sourceWidth))
			 {
				 $(this).css("opacity",0).empty();
				 flagTool++;
			 }
		  });
		  if (flagTool === $(".d3-tip").length && $(".tooltip-hightlight").length > 0){
			  $(".tooltip-hightlight").each(function(){
				  var tempClass = $(this).attr("class");
				  if (tempClass)
					  $(this).attr("class",tempClass.replace("tooltip-hightlight",""));
			  });
		  }
	});
	this.dataTable = [];
	this.dtDim = null;
	this.refreshTable = null;
	this.childTable = null;
	this.drawnGraph = [];
	this.defaultHeight = $(window).width() / 6.83;
	this.refreshUSChart = null;
	this.refreshSummary = null;
    this.stackMeasures = []; 
	this.usCharts = [];
	this.topNBarCharts = [];
	this.distributionCharts = [];
	this.mapCharts = [];
	this.pairedRowCharts = [];
	this.groupedMultiLevelCharts = [];
	this.groupedBarLineCharts = [];
	this.groupedTreeMapCharts = [];
	this.compositeBrushchart = [];
	this.hierarchieCharts = [];
	this.summaryCharts = [];
	this.sliders = [];
	this.filters = filters;
	filters = null;
	this.masterData = masterData;
	masterData = null;
	this.reduceFieldsAdd = function(measures,labels,filter,filterValue,dimension) {
		labels = labels || [];
		filterValue = filterValue || [];
		dimension = dimension || [];
		return function(p, v) {
			if (!filter || filterValue.length === 0 || (v[filter] && filterValue.indexOf(v[filter]) !== -1)){
				p.count += +v._count;
				measures.forEach(function(f) {
					if(v[f] && !isNaN(v[f]))
						p[f] += +v[f];
				});
			}else{
				measures.forEach(function(f) {
					p[f] += 0 ;
				});
			}
			labels.forEach(function(f) {
				p[f] = v[f];
			});
			dimension.forEach(function(f) {
				if(!p[f][v[f]])
					  p[f][v[f]] = 0;
				p[f][v[f]]++;
			});
			return p;
		};
	};
	this.reduceFieldsRemove = function(measures,labels,filter,filterValue,dimension) {
		labels = labels || [];
		filterValue = filterValue || [];
		dimension = dimension || [];
		return function(p, v) {
			if (!filter || filterValue.length === 0 || (v[filter] && filterValue.indexOf(v[filter]) !== -1)){
				p.count -= +v._count;
				measures.forEach(function(f) {
					if(v[f] && !isNaN(v[f]))
						p[f] = p[f] - v[f];
					var reducedValue = p[f];
					var numFormat = d3.format(".0f");
					if(numFormat(reducedValue) === "0" || numFormat(reducedValue) === "-0" ){
						p[f] = 0 ;
					}
				});
			}else{
				measures.forEach(function(f) {
					p[f] -= 0 ;
				});
			}
			labels.forEach(function(f) {
				p[f] = v[f];
			});
			dimension.forEach(function(f) {
				if(!p[f][v[f]])
					  p[f][v[f]] = 0;
				p[f][v[f]]++;
			});
			return p;
		};
	};
	this.reduceFieldsInitial = function(measures,labels,filter,filterValue,dimension) {
		labels = labels || [];
		dimension = dimension||[];
		return function() {
			var ret = {};
			ret["count"] = 0;
			measures.forEach(function(f) {
				ret[f] = 0;
			});
			labels.forEach(function(f) {
				ret[f] = "";
			});
			dimension.forEach(function(f) {
				ret[f] = {};
			});
			return ret;
		};
	};
	
	this.reduceMultipleFieldsAdd = function(measures,d){
		d = d || [];
		return function(p, v) {
			p.count++;
			if(v[measures] && !isNaN(v[measures]))
				p[measures]+= +v[measures];
			var temp = p;
			for (var i=0; i<d.length; i++) {
		        var currentProp = v[d[i]];
		        if (!(currentProp in temp))
		         	temp[currentProp] = {};
	            if(!temp[currentProp][measures])
	               temp[currentProp][measures] = 0;
	            if(v[measures] && !isNaN(v[measures]))
	            	temp[currentProp][measures] += parseInt(v[measures]);
	            temp = temp[currentProp];
		    }
			return p;
		};
	};
	this.reduceMultipleFieldsRemove = function(measures,d){
		d = d || [];
		return function(p, v) {
			var temp = p;
			for (var i=0; i<d.length; i++) {
		        var currentProp = v[d[i]];
		        if (!(currentProp in temp))
		         	temp[currentProp] = {};
	            if(!temp[currentProp][measures])
	               temp[currentProp][measures] = 0;
	            if(v[measures] && !isNaN(v[measures]))
	            	temp[currentProp][measures] -= parseInt(v[measures]);
	            temp = temp[currentProp];
		    }
			return p;
		};
	};
	this.reduceMultipleFieldsInitial = function(measures,dimention){
		dimention = dimention || [];
		return function() {
			var ret = {};
			ret["count"] = 0;
			ret[measures] = 0;
			/*
			 * dimention.forEach(function(f) { ret[f] = []; });
			 */
			/*
			 * labels.forEach(function(f) { ret[f] = ""; });
			 */
			return ret;
		};
	};
	
	var randomColor = (function() {
		var golden_ratio_conjugate = 0.618033988749895;
		var h = Math.random();

		var hslToRgb = function(h, s, l) {
			var r, g, b;

			if (s === 0) {
				r = g = b = l; // achromatic
			} else {
				function hue2rgb(p, q, t) {
					if (t < 0)
						t += 1;
					if (t > 1)
						t -= 1;
					if (t < 1 / 6)
						return p + (q - p) * 6 * t;
					if (t < 1 / 2)
						return q;
					if (t < 2 / 3)
						return p + (q - p) * (2 / 3 - t) * 6;
					return p;
				}

				var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
				var p = 2 * l - q;
				r = hue2rgb(p, q, h + 1 / 3);
				g = hue2rgb(p, q, h);
				b = hue2rgb(p, q, h - 1 / 3);
			}

			return '#' + Math.round(r * 255).toString(16)
					+ Math.round(g * 255).toString(16)
					+ Math.round(b * 255).toString(16);
		};

		return function() {
			h += golden_ratio_conjugate;
			h %= 1;
			return hslToRgb(h, 0.5, 0.60);
		};
	})();
	this.getValueByAgrregate = function(type, field, filterValue,absolute) {
		return function(d) {
			var groupValue;
			var groupCount;
			if (type === "calculated"){
				groupValue = eval(field);
				groupValue = groupValue === Infinity ? 0 : groupValue;
				return isNaN(groupValue) ? 0 : groupValue;
			}else{
				groupValue = isNaN(d.value[field]) ? 0 : d.value[field];
				groupCount = isNaN(d.value.count)? 0 : d.value.count;
				if (groupValue !== 0 && groupCount !== 0) {
					if (type.toLowerCase() === "sum")
						return absolute === "Yes"?Math.abs(groupValue):groupValue;
					else if (type.toLowerCase() === "count")
						return groupCount;
					else
						return absolute === "Yes"? (Math.abs(groupValue) / groupCount):groupValue / groupCount;
				} else {
					return 0;
				}
			}
			
			
		};
	};
	this.getKeyColorCodeByRange = function(grp, config,keyName) {
		var keyColorCodes = {};
		if(typeof(config.colorcodes) === 'string' && config.codetype !== "standard"){
			config.colorcodes = Jsonparse(config.colorcodes);
		}
		if (grp && grp.all().length !== 0) {
			var allValues = grp.all();
			for ( var i = 0; i < allValues.length; i++) {
				var val,key = allValues[i].key || allValues[i][keyName] ;
				if (config.aggregate.toLowerCase() === "sum")
					val = allValues[i].value[config.xAxis] || allValues[i].value[config.group];
				else if (config.aggregate.toLowerCase() === "count")
					val = allValues[i].value.count;
				else if (config.aggregate.toLowerCase() === "calculated"){
					var d = allValues[i];
					val = allValues[i].value[config.xAxis] || allValues[i].value[config.group];// eval(config.calculatedformula);
				}
				else{
					val = allValues[i].value[config.xAxis] || allValues[i].value[config.group];
					val = val / allValues[i].value.count;	
				}					
				keyColorCodes[key] = randomColor();
				if (config.codetype === "key" && config.colorcodes && config.colorcodes[key]) {
					keyColorCodes[key] = config.colorcodes[key];
				}
				else if(config.codetype === "field" && config.colorcodes && config.colorcodes[allValues[i].value[config.colorfield]]){
					keyColorCodes[key] = config.colorcodes[allValues[i].value[config.colorfield]];
				}
				else if(config.codetype === "standard"){
					keyColorCodes[key] = config.colorcodes;
				}
				else if (val && d3.keys(config.colorcodes).length > 0) {
					for ( var obj in config.colorcodes) {
						var range = obj.split(":");
						if (val >= range[0] && val <= range[1]) {
							keyColorCodes[key] = config.colorcodes[obj];
							break;
						}
					}
				}
			}

		}
		return keyColorCodes;
	};
	this.getTitleAgrregate = function(d,config, dateFormat) {;
		var numFormat = d3.format(config.numFormat);
		var masterData = this.masterData;
		var key = dateFormat ? dateFormat(d.key) : d.key;;
		config.description = config.description || "";
		if(config.tooltipkey === "code"){
			key = dateFormat ? dateFormat(d.key) : d.key;
		}else if(config.tooltipkey === "description"){
			if ($.trim(config.description) !== "" && masterData[config.fieldname][key]) {
				key = masterData[config.fieldname][key];
			}
		}else{
			if ($.trim(config.description) !== "" && masterData[config.fieldname][key]) {
				key = (dateFormat ? dateFormat(d.key) : d.key)+"-" + masterData[config.fieldname][key];
			}
		}
		var keyValue;
		if (config.aggregate.toLowerCase() === "calculated"){
			keyValue = eval(config.calculatedformula);
			keyValue = isNaN(keyValue) ? 0 : numFormat(keyValue);
		} else {
			var groupValue = isNaN(d.value[config.group]) ? 0 : d.value[config.group];
			var groupCount = isNaN(d.value.count)? 0 : d.value.count;
			if (groupValue !== 0 && groupCount !== 0){
				if (config.aggregate.toLowerCase() === "sum")
					keyValue = config.absolute === "Yes"?(config.prefix + numFormat(Math.abs(groupValue)) + config.suffix):(config.prefix+ numFormat(groupValue) + config.suffix);
				else if (config.aggregate.toLowerCase() === "count")
					keyValue = config.prefix
							+ numFormat(groupCount) + config.suffix;
				else{
					keyValue = config.absolute === "Yes"?(config.prefix+ numFormat(Math.abs(groupValue) / groupCount)+ config.suffix):(config.prefix+ numFormat(groupValue / groupCount)+ config.suffix);
				}
			} else {
				keyValue = 0;
			}
		}
		
		if(config.prefixScale !== undefined && !config.prefixScale){
			keyValue = config.prefix+ numFormat(keyValue) + config.suffix;
		}
		   var toolTip= $("<div/>");
		   toolTip.append($('<table/>').attr({"class":"d3-tip-table"}).append($('<tr/>')
				   .append($('<td/>').html( key +((config.tooltipkey != "key")?':':'')).attr({"class":"d3-tip-key"}))
				   .append($('<td/>').html((config.tooltipkey != "key")?keyValue:"" ).attr({"class":"font-bold"}))));
		if (config.linkaction){
			var actionDiv = $("<div/>");
			actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
			var actions = config.linkaction.split(",");
			var obj = {};
			obj[config.fieldname] = key;
			for (var i = 0;i<actions.length;i++){
				actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','data-filtersForwaded': JSON.stringify(obj),'title':'Go to "'+ actions[i]+'"','onclick':"dataAnalyser.tooltipActionClick(this)"}));
			}
			toolTip.append(actionDiv);
		}
		/* changing code here */
		var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
		$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+key+'")');
		var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
		$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ key +'",true)');
		var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
		divTag.append(anchorTag).append(excludeAnchor);
		toolTip.append(divTag);
		return toolTip[0].outerHTML;
	};
	this.sortbyGroup = function(config, d) {
		config.orderby = config.orderby || 1;
		if(config.orderby == "4" && (config.customsort == null || config.customsort==""))
			config.orderby =  1;
		var tempValue;
		if (config.orderby === "2" || config.orderby === "3") {
			if (isNaN(d.key)) {
				var str = d.key.toLowerCase();
				var maxLength =  config.maxKeyLength || str.length;
				tempValue = 0;
				for(var i=0;i<str.length;i++,maxLength--){
					if(str.charCodeAt(i) !== 32){
						tempValue+= Math.pow(10, maxLength) * (str.charCodeAt(i));
					}
				}
			} else {
				tempValue = parseInt(d.key);
			}
			if(config.orderby === "3"){
				tempValue = tempValue * -1;
			}else{
				tempValue = tempValue * 1;				
			}
		}else if(config.orderby === "4" ){
			var customarr = config.customsort.split(',');
			if(customarr.indexOf(d.key)>=0)
			tempValue = customarr.indexOf(d.key);
			else{
				var nextindex= customarr.length;
				tempValue = ++nextindex;
			}
	   }else if(config.orderby === "5" || config.orderby === "6"){
		   var dateObj = new Date(d.key);
		   if(!isNaN(dateObj.getTime())){
			   tempValue = dateObj.getTime();
		   }
		   if(config.orderby === "6")
			   tempValue = tempValue * -1;
	   } else if (config.aggregate.toLowerCase() === "sum")
			tempValue = d.value[config.group] * config.orderby;
		else if (config.aggregate.toLowerCase() === "count")
			tempValue = d.value.count * config.orderby;
		else if (config.aggregate.toLowerCase() === "calculated")
			tempValue = eval(config.calculatedformula)=== Infinity || isNaN(eval(config.calculatedformula)) ? 0 * config.orderby: (eval(config.calculatedformula) ) * config.orderby;
			// tempValue = d.value.count * config.orderby;
		else
			tempValue = (d.value[config.group] / d.value.count)
					* config.orderby;
		return tempValue;
	};
	this.sortGroup = function(field, type, order,filterValue) {
		order = (order !== null && order !== undefined)?order:1;
		if (order !== "-1") {
			return function(a, b) {
				var aGroupValue = isNaN(a.value[field]) ? 0 : a.value[field];
				var bGroupValue = isNaN(b.value[field]) ? 0 : b.value[field];
				var aGroupCount = isNaN(a.value.count)? 0 : a.value.count;
				var bGroupCount = isNaN(b.value.count)? 0 : b.value.count;
				var sortStatus = 0, aValue = 0, bValue = 0;
				if (aGroupValue !== 0 && aGroupCount !== 0) {
					if (type.toLowerCase() === "sum")
						aValue = aGroupValue;
					else if (type.toLowerCase() === "count")
						aValue = aGroupCount;
					else
						aValue = aGroupValue / aGroupCount;
				}
				if (bGroupValue !== 0 && bGroupCount !== 0) {
					if (type.toLowerCase() === "sum")
						bValue = bGroupValue;
					else if (type.toLowerCase() === "count")
						bValue = bGroupCount;
					else
						bValue = bGroupValue / bGroupCount;
				}
				if (aValue < bValue) {
					sortStatus = (order === 0) ? -1 : 1;
				} else if (aValue > bValue) {
					sortStatus = (order === 0) ? 1 : -1;
				}
				return sortStatus;
			};
		}
	};
	this.constructSelect = function(config, dim) {
		var allOptionValue = dim.group().size();
		if (config.filtertype === "percentage") {
			allOptionValue = 100;
		}
		var values = config.filtervalues.split(',');
		var select = $('<select/>').attr({"name":"filterselect"}).append($('<option/>').attr('value', allOptionValue).html('All'));
		if (config.filterdefaultval && values.indexOf(config.filterdefaultval) === -1) {
			select.append($('<option/>').attr("value", config.filterdefaultval)
					.html(config.filterdefaultval + (config.filtertype === 'percentage' ? '%': '')));
		}
		for ( var i = 0; i < values.length; i++) {
			select.append($('<option/>').attr("value", values[i]).html(values[i]+ (config.filtertype === 'percentage' ? '%': '')));
		}
		$(select).val(config.filterdefaultval).addClass("col-md-3 onoffswitch");
		$(config.html).find(".reset").after($("<div/>").addClass("smart-form row ")
				.append($("<div/>").addClass("col-md-6").append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox"})).append($("<i/>").attr({"data-swchon-text":"Bottom","data-swchoff-text":"Top"}))))
				.append(select));
	};
	this.constructDimentionSelect = function(config){
		var options = config.dynamicdimension.split(",");
		var select = $('<select/>').attr({"name":"dimensionselect"});
		select.append($('<option/>').attr('value', config.field).html(config.displayname || config.fieldname));		
		for(var i=0;i<options.length;i++){
			select.append($('<option/>').attr('value', options[i]).html((config.measurelabel && config.measurelabel[options[i]])?config.measurelabel[options[i]]: options[i]));
		}
		if(options.indexOf($(config.html).data('fieldname')) > -1)
			select.val($(config.html).data('fieldname'));
		$(config.html).prepend(select);
		return select;
	};
	var applyDefaultFilter = function(config,chart){
		if(config.defaultFiltervalue && $.trim(config.defaultFiltervalue) !== ""){
			if(!Array.isArray(config.defaultFiltervalue))
				config.defaultFiltervalue = config.defaultFiltervalue.split(",");
		}else{
			config.defaultFiltervalue = [];
		}
		if (viewSettings && viewSettings.length > 0){
			$.each(dataAnalyser.viewSettings,function(){
				var viewanchor = this.anchorName.split('container');
				if(viewanchor[0] ===  config.field && this.resetedDefaultVal === true){
					config.defaultFiltervalue = [];
				}
				if (viewanchor[0] ===  config.field && this.filterValues){
					config.defaultFiltervalue = $.merge(config.defaultFiltervalue,this.filterValues);
					return;
				}
			});
		}
		if(config.defaultFiltervalue && $(".reset", $(config.html)).data('isReset')){
			var defaultFiltervalue = config.defaultFiltervalue;
			defaultFiltervalue.forEach(function(d) {
				d= d.replace('`',',')
				chart.filter(d);
			});
		}
	};
	Graphing.prototype.drawSummary = function(config) { 
		var THIS = this;
		var label = "d['" + config.fieldname + "']|| 'Others'";
		var dimFun = new Function('d', 'return ' + label + ';');
		var dim = this.ndx.dimension(dimFun);
		this.summaryCharts.push({
			"config" : config,
			"dim" : dim
		});
		var pieHeight = parseInt($("#" + (config.id)).width());
		pieHeight = pieHeight > this.defaultHeight ? this.defaultHeight
				: pieHeight;
		
		$("#svg-" + (config.id)).attr({
			"width" : pieHeight,
			"height" : pieHeight
		});
		
		var renderSummary = function(config, dim) { 
			if(config.isfilter){
				if(config.overrideclick && $.trim(config.overrideclick) !== ""){
					$("#" + (config.id)).children().css("cursor","pointer").attr({"onClick":"javascript:"+config.overrideclick});
				}else if(config.filterby){
					$("#" + (config.id)).children().css("cursor","pointer").unbind('click').bind('click',function(){
						clickOnSummary(this,dim,config);
					});
				}
			}
			var keyValue = 0;
			var keyValueMax = 0;
			var fields = [];
			var calculatedField = [];
			if (config.calculatedfield && config.calculatedfield !== null
					&& $.trim(config.calculatedfield) !== "") {
				calculatedField = config.calculatedfield.split(",");
			}
			fields.push(config.group);
			fields = $.merge(fields, calculatedField);
			
			if (config.aggregate !== "min" && config.aggregate !== "max"
					&& config.aggregate !== "range"
					&& config.aggregate !== "distinct") {

				var divider = 1;
				if (config.divider !== undefined && $.trim(config.divider) !== "" && isNaN(config.divider)) {
					fields.push(config.divider);
					divider = "d.value." + config.divider ;
				} else if (isNaN(config.divider) === false)
					divider = config.divider;
				var aggregateFun = new Function('d', "return d.value." + config.group.trim() + ";");
				if (config.aggregate === "avg")
					aggregateFun = new Function('d', "return d.value."+ config.group.trim() + "/d.value.count;");
				else if (config.aggregate === "count")
					aggregateFun = new Function('d', "return d.value.count;");
				else if (config.aggregate === "calculated")
					aggregateFun = new Function('d,formula', " return eval(formula);");

				var grpFun = function(filters) {
					return function(d) {
						var temp = 0;
						var filterFlag = false;
						for ( var i = 0; i < filters.length; i++) {
							var item = filters[i];
							if ((item.indexOf("!") === -1 && item === d.key)) {
								filterFlag = true;
							}else if(item.indexOf(null)> -1 && (!d.key || d.key === "Others")){
								filterFlag = true;
							}else if (item.indexOf("!") > -1) {
								if (item !== "!" + d.key) {
									filterFlag = true;
								} else {
									filterFlag = false;
									break;
								}
							}
						}
						if ((filters.length > 0 && filterFlag) || filters.length === 0) {
							return aggregateFun(d,config.calculateformula);
						}
						return temp;
					};
				};
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields),
						THIS.reduceFieldsRemove(fields),
						THIS.reduceFieldsInitial(fields));
				var grpDividerFun = new Function('d', 'return ' + divider + ';');
				var filters = [];
				if (config.filterby && $.trim(config.filterby) !== ""){
					if(config.filtervaluetype == "function")
					{
						filters=eval(config.filterby);
						filters = filters.split(",");
					}
					else{
						filters = config.filterby.split(",");
					}
				}
				if (config.aggregate === "avg"){
					keyValue = d3.mean(grp.all(), grpFun(filters));
				}else{
					if(config.filtervaluetype == 'separatevalue'){
							var newkeyValue
							var overRideClick = []
							keyValue = ""
							if(config.overrideclick){
								overRideClick = config.overrideclick.split(",")
							}
							for(var x = 0; x < filters.length; x++){
								newkeyValue = d3.sum(grp.all(), grpFun([filters[x]]));
								newkeyValue = d3.format(config.format)(newkeyValue);
								if(overRideClick && overRideClick[x]){
									keyValue = keyValue + '<a href="javascript:void(0)"  onclick="'+overRideClick[x]+'" class="separatedSum">'+newkeyValue+'</a>' +"/"
								}else{
									keyValue = keyValue + newkeyValue+"/";
								}
								
							}
							keyValue = keyValue.slice(0, -1);
						}else{
							keyValue = d3.sum(grp.all(), grpFun(filters));
						}
				}
				var dividerValue = divider;
				if (divider !== undefined && divider !== "" && isNaN(divider)) {
					dividerValue = d3.sum(grp.all(), grpDividerFun);
				}
				if (dividerValue && dividerValue !== 0) {
					keyValue = keyValue / dividerValue;
				}
			} else if (config.aggregate === "min") {
				keyValue = dim.bottom(1)[0][config.fieldname];
			} else if (config.aggregate === "distinct") {
				if(config.filter){
					var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,[config.filter]),
							THIS.reduceFieldsRemove(fields,[config.filter]),
							THIS.reduceFieldsInitial(fields,[config.filter]));
					if(config.filtervaluetype == 'separatevalue'){
						var filters = [];
						if (config.filterby && $.trim(config.filterby) !== ""){
							if(config.filtervaluetype == "function")
							{
								filters=eval(config.filterby);
								filters = filters.split(",");
							}
							else{
								filters = config.filterby.split(",");
							}
						}
						var newkeyValue
						var overRideClick = {};
						var sepratedValue = $('<div/>').css({'font-size':"14px","padding-bottom":"2px"}); //keyValue
						if(config.overrideclick){
							overRideClick = Jsonparse(config.overrideclick);
						}
						var measurelabel = {};
						if(config.measurelabel){
							measurelabel = Jsonparse(config.measurelabel);
						}
						for(var x = 0; x < filters.length; x++){
							newkeyValue = grp.all().filter(function(o){return o.value[config.filter] === filters[x]}).length;
							newkeyValue = d3.format(config.format)(newkeyValue);
							if(overRideClick && overRideClick[filters[x]]){
								sepratedValue.append($("<span>").html(measurelabel[filters[x]]+" ("))
								if(jQuery.inArray( "IIT", System.roles ) >=0&&jQuery.inArray( "CR&D", System.roles ) >=0){
									var anchors = $("<a/>").addClass("cursor-pointer").html(newkeyValue);
								}
								else if(jQuery.inArray( "IIT", System.roles ) >=0){
									if(measurelabel[filters[x]]=="CR&D")
									   {
										var anchors = $("<a/>").css({"pointer-events": "none"}).html(newkeyValue);
									   }
									   else
										 var anchors = $("<a/>").addClass("cursor-pointer").html(newkeyValue);
								}
								else if(jQuery.inArray( "CR&D", System.roles ) >=0){
									if(measurelabel[filters[x]]=="IIT")
									   {
										var anchors = $("<a/>").css({"pointer-events": "none"}).html(newkeyValue);
									   }
									   else
										 var anchors = $("<a/>").addClass("cursor-pointer").html(newkeyValue);
								}
								else{
									var anchors = $("<a/>").addClass("cursor-pointer").html(newkeyValue);
								}
								anchors.unbind('click').bind("click",{msg:overRideClick[filters[x]]},function(event){
									eval(event.data.msg)
									});
								sepratedValue.append(anchors);
								sepratedValue.append($("<span>").html(")"));
							}else{
								sepratedValue.append($("<span>").html(measurelabel[filters[x]]));
								sepratedValue.append($("<span>").html(newkeyValue));
							}
							if(x<filters.length-1){
								sepratedValue.append($("<span/>").html(" / "))
							}
							
						}
						$("#"+ (config.id)).find("h2").css({"margin-top":"-13px"}).html(sepratedValue);
					}
				}
				keyValue = dim.group().all().filter(function(d){
					return d.key !== "Others" && d.key !== "Not Available"  && d.value !== 0;
					}).length;
								
			} else if (config.aggregate === "max") {
				keyValue = dim.top(1)[0][config.fieldname];
			} else if (config.aggregate === "range") {
				keyValue = dim.bottom(1)[0][config.fieldname];
				keyValueMax = dim.top(1)[0][config.fieldname];
			}
			$("#svg-" + (config.id)).empty();
			$("#" + (config.id)).show();
			if (config["widgetType"] && config["widgetType"]!=="fillgauge"){
				if (config.aggregate !== "range"){
			    	if (config.fieldType !== "string" ){
			    		if (keyValue instanceof Date && !isNaN(keyValue.valueOf()))
			    			keyValue =  d3.timeFormat(config.textFormat)(keyValue);
			    	}
			    	else{
			    		if(config.valuetype){
			    			var filters = [];
							if (config.filterby && $.trim(config.filterby) !== "")
								filters = config.filterby.split(",");
			    			var total = d3.sum(grp.all(), grpFun([]));
							var value = d3.sum(grp.all(), grpFun(filters));
							if(config.valuetype === "valuepercentage"){
								keyValue = d3.format(config.textFormat)(keyValue) +' - '+ Math.round(value*100/total)+'%';								
							}else if(config.valuetype === "percentage"){
								keyValue = Math.round(value*100/total)+'%';	
							}
			    		}else{
			    			if(config.filtervaluetype !== 'separatevalue')
			    				keyValue = d3.format(config.textFormat)(keyValue);
			    		}
			    	}			    		
		    	}else{
		    		if (config.fieldType !== "string" ){
			    		if (keyValue instanceof Date && !isNaN(keyValue.valueOf()))
			    			keyValue = d3.timeFormat(config.textFormat)(keyValue)+" - "+ d3.timeFormat(config.textFormat)(keyValueMax);
			    		else 
			    			keyValue = keyValue+ " - "+keyValueMax;
			    	}
			    	else
			    		keyValue = d3.format(config.textFormat)(keyValue)+" - "+ d3.format(config.textFormat)(keyValue);
		    	}
			}
			if (config["widgetType"]==="ibox"){
				if(config.renderFunction){
					var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
					anchor.bind("click",function(){eval(config.renderFunction)});
					$("#"+ (config.id)).find("h2").html(anchor);
				}
				else
					$("#"+ (config.id)).find("h2").html(config.prefix+keyValue+config.suffix);
				var maxValue = config.maxValue || config["max-value"];
				if (!isNaN(maxValue))
					$("#"+ (config.id)).find(".progress-bar").css("width",(keyValue/maxValue)*100+"%");
					
			} else if (config["widgetType"]==="widget"){
				if(config.filtervaluetype == 'separatevalue'){
					$("#"+ (config.id)).find(".font-md").css({"height":"25px !important"});
					if(config.renderFunction){
						if(jQuery.inArray( "IIT", System.roles ) >=0&&jQuery.inArray( "CR&D", System.roles ) >=0){
							var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
						}
						else if(jQuery.inArray( "IIT", System.roles ) >=0){
							if(measurelabel["CR&D"]=="CR&D")
							   {
								var anchor = $("<a/>").css({"pointer-events": "none"}).html(config.prefix+keyValue+config.suffix);
							   }
							   else
								   var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
						}
						else if(jQuery.inArray( "CR&D", System.roles ) >=0){
							if(measurelabel["IIT"]=="IIT")
							   {
								var anchor = $("<a/>").css({"pointer-events": "none"}).html(config.prefix+keyValue+config.suffix);
							   }
							   else
								   var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
						}
						else{
							var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
						}
						anchor.bind("click",function(){eval(config.renderFunction)});
						$("#"+ (config.id)).find("h2").append($("<span/>").html(anchor));
					}
					else
						$("#"+ (config.id)).find("h2").append($("<span/>").html(config.prefix+keyValue+config.suffix));
				}else{
					if(config.renderFunction){
						var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
						anchor.bind("click",function(){eval(config.renderFunction)});
						$("#"+ (config.id)).find("h2").html(anchor);
					}
					else
						$("#"+ (config.id)).find("h2").html(config.prefix+keyValue+config.suffix);
				}
				
				if(config.tooltip && $.trim(config.tooltip)){
					$("#"+ (config.id)).find("h2").append($("<span/>").addClass("pull-right").append('<i style="font-size:20px;color:#444" data-content="'+config.tooltip+'" data-toggle="popover" data-html="true" rel="popover" class="fa fa-info-circle fa-lg iconhover"></i>'));					
				}
			} else if (config["widgetType"]==="circle-tile"){
				if(config.renderFunction){
					var anchor = $("<a/>").addClass("cursor-pointer").html(config.prefix+keyValue+config.suffix);
					anchor.bind("click",function(){eval(config.renderFunction)});
					$("#"+ (config.id)).find(".circle-tile-number").find("span").html(anchor);
				}
				else
					$("#"+ (config.id)).find(".circle-tile-number").find("span").html(config.prefix+keyValue+config.suffix);
			}else{
				loadLiquidFillGauge("svg-" + (config.id), keyValue, config.liquidFillGaugeSettings, keyValueMax);
			}
			$("[name='summaryWidget']").find("i[rel='popover']").popover({
		  		  placement: 'auto',
		  		  container: 'body',
		  		  width:'300px',
		  		  delay :  { "show": 100, "hide": 100 },
		  		  trigger : 'hover'
	  	  	});
		};
		function clickOnSummary(obj,dim,config) { 
			var filterValues = [];
			if (config.filterby && $.trim(config.filterby) !== "")
				filterValues = config.filterby.split(",");			
			if ($(obj).data('click')) {
				dim.filterAll();
				var count = 0;
				$('[id^=summary]').children().each(function(){
					if($(this).data('click')){
						$(this).css('opacity', 1);
						count++;
					}else{
						$(this).css('opacity', 0.2);
					}
				});
				$(obj).css('opacity', 0.2);
				if(count === 1){
					$('[id^=summary]').children().css('opacity', 1);
				}
				$(obj).removeData("filterby");
				$(obj).removeData("fieldname");
			} else {
				$('[id^=summary]').children().each(function(){
					if($(this).data('click')){
						$(this).css('opacity', 1);
					}else{
						$(this).css('opacity', 0.2);
					}
				});
				$(obj).css('opacity', 1);
				$(obj).data("filterby",config.filterby);
				$(obj).data("fieldname",config.fieldname);
				dim.filter(function(d) {
					return filterValues.indexOf(d) > -1;
				});
			}
			$(obj).data('click',!$(obj).data('click'));
			resetAllCharts(THIS,(config.id));
		}
		this.refreshSummary = function(currentConfigHtml) {
			for ( var x = 0; x < this.summaryCharts.length; x++) {
				if (!currentConfigHtml || this.summaryCharts[x]["config"].id !== currentConfigHtml) {				
					renderSummary(this.summaryCharts[x]["config"],
							this.summaryCharts[x]["dim"]);
				}
			}
		};
		renderSummary(config, dim);
	};
	Graphing.prototype.drawDistributionChart = function(config) {
		var THIS = this;
		try {
			$(".reset", $(config.html)).data('isReset',true);
			var label = "d['" + config.field + "'] || 'Others'";
			var dimFun = new Function('d', 'return ' + label + ';');
			var dim1 = this.ndx.dimension(dimFun);
			var fields = [];
			var calculatedField = [];
			if (config.calculatedfield && config.calculatedfield !== null
					&& $.trim(config.calculatedfield) !== "") {
				calculatedField = config.calculatedfield.split(",");
			}
			$(config.html).data('resetedValue',false);
			fields.push(config.group);
			fields = $.merge(fields, calculatedField);
			this.distributionCharts.push({
				"config" : config,
				"dim" : dim1
			});
			if (config.filtervalues && $(config.html).find('select').length === 0) {
				this.constructSelect(config, dim1);
				$(config.html).find('select').unbind('change').bind('change',
						function() {
							config.filterdefaultval = $(this).val();
							renderDistributionChart(config, dim1);
						});
				if(config.filterfrom === "bottom"){
					$(config.html).find('input').attr("checked","checked");
				}
				$(config.html).find("input").unbind('click').bind("click",function(){
					if($(this).is(":checked")){
						config.filterfrom = "bottom";
					}else{
						config.filterfrom = "top";
					}
					renderDistributionChart(config, dim1);
				});
			} else {
				$(config.html).find('select').val(config.filterdefaultval);
				if(config.filterfrom === "bottom"){
					$(config.html).find('input').attr("checked","checked");
				}
			}
			
			var renderDistributionChart = function(config, dim) {
				var numberFormat = d3.format(config.numFormat);
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
						var key = d.key;
						if(config.tooltipkey === "code"){
							key = d.key;
						}else if(config.tooltipkey === "description"){
							if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
								key = THIS.masterData[config.fieldname][key];
							}
						}else{
							if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
								key = d.key+"-" + THIS.masterData[config.fieldname][key];
							}
						}
						 var toolTip= $("<div/>");
						   toolTip.append($('<table/>').attr({"class":"d3-tip-table"}).append($('<tr/>').append($('<td/>').html( key +':').attr({"class":"d3-tip-key"})).append($('<td/>').html(numberFormat(d.value) ).attr({"class":"font-bold"}))));
						if (config.linkaction){
							var actionDiv =  $("<div/>");
							actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
							var actions = config.linkaction.split(",");
							for (var i = 0;i<actions.length;i++){
								actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
							}
							toolTip.append(actionDiv);
						}
						var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action","id":"keep-only"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
						$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+key+'")');
						var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action","id":"exclude"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
						$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ key +'",true)');
						var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
						divTag.append(anchorTag).append(excludeAnchor);
						toolTip.append(divTag);
						return toolTip[0].outerHTML;
					});
				$(config.html).find("svg").remove();
				var labels = [];
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
				
				var distributionChart = dc.drawDistributionChart(config.html);
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()* (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.group, config.aggregate,config.filterfrom === "bottom"?0:1));
					distributionChart.data(function(group) {
						return group.top(defaultVal);
					});
				}
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
				}
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);
				var bubbleHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					bubbleHeight = parseInt($(config.html).width());
					bubbleHeight = bubbleHeight > this.defaultHeight ? this.defaultHeight
							: bubbleHeight;
				} else {
					bubbleHeight = parseInt(config.chartheight);
				}
				bubbleHeight = bubbleHeight-50;
				distributionChart.dimension(dim).width(bubbleHeight).height(bubbleHeight).margins({
					top : config.margintop,
					left : config.marginleft,
					right : config.marginright,
					bottom : config.marginbottom
				})
				.group(grp).valueAccessor(grpAggre).ordering(function(a) {
					return THIS.sortbyGroup(config, a);
				}).tip(tip).linkAction(config.linkaction);
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					distributionChart.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					distributionChart.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				distributionChart.render();
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					distributionChart.filterAll();
					$(config.html).data('resetedValue',true);
					$(e.currentTarget).removeData("isReset");
					resetAllCharts(THIS);
				}.bind(this));
				distributionChart.on('filtered', function() {
					resetAllCharts(THIS, config.html);
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				applyDefaultFilter(config,distributionChart);
			};
		} catch (e) {
			console.log('Error Creating bubble Chart' + e);
		}
		renderDistributionChart(config, dim1);
	};
	Graphing.prototype.drawGoogleMap = function(config) {
		var THIS = this;
		try {
			var label = "d['" + config.field + "'] || 'Others'";
			var dimFun = new Function('d', 'return ' + label + ';');
			var dim = this.ndx.dimension(dimFun);
			var dimBubble = this.ndx.dimension(function(d) {
				if(d[config.latitude] && d[config.longitude])
					return [d[config.latitude],d[config.longitude]].toString(); 
				});
			/*
			 * var dimBubble = this.ndx.dimension(function(d) { return
			 * d[config.bubble]; });
			 */
			var geoHeight = "";
			if (config.chartheight.toLowerCase() === "auto") {
				geoHeight = parseInt($(config.html).width());
				geoHeight = geoHeight > this.defaultHeight ? this.defaultHeight
						: geoHeight;
			} else {
				
				geoHeight = parseInt(config.chartheight);
			}
			var geoWidth;
			if (config.chartwidth.toLowerCase() === "auto") {
				geoWidth = parseInt($(config.html).width());
				geoWidth = geoWidth > this.defaultHeight ? this.defaultHeight : geoWidth;
				geoWidth = geoWidth * 3 / 2;
			} else {
				geoWidth = parseInt(config.chartwidth);
			}
			this.mapCharts.push({
				"config" : config,
				"dim" : dim,
				"dimBubble":dimBubble
			});
			// $(config.html).css("height",geoHeight);
			var renderGoogleMap= function(config, dim,dimBubble) {
				/*
				 * var tip = d3.tip().offset([-8, 0]).attr('class',
				 * 'd3-tip').html(function(d){ return "<table><tr><th class='d3-tip-header'>"+
				 * key + "</th></tr>" + "<tr><td class='d3-tip-key'>" +
				 * config.radiusLabel + "</td><td class='d3-tip-value'>" +
				 * config.radiusPrefix + numberFormat(rad) + config.radiusSuffix + "</td></tr><tr><td class='d3-tip-key'>" +
				 * config.xAxisLabel + "</td><td class='d3-tip-value'>" +
				 * config.xAxisPrefix + xAxisFormat(xAxisValue) +
				 * config.xAxisSuffix + "</td></tr><tr><td class='d3-tip-key'>" +
				 * config.yAxisLabel + "</td><td class='d3-tip-value'>" +
				 * config.yAxisPrefix + numberFormat(yAxisValue) +
				 * config.yAxisSuffix+ "</tr><table/>"; });
				 */
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							if(dc.chartRegistry.list()[chartIndex].off)
								dc.chartRegistry.list()[chartIndex].off();
							if(dc.chartRegistry.list()[chartIndex].remove)
								dc.chartRegistry.list()[chartIndex].remove();
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				$(config.html+"_map").remove();
				$(config.html).append($("<div/>").attr("id",config.field+"_map").data("charttype","chart-map")
						.data("latitude",config.latitude).data("longitude",config.longitude)
						.css("height",geoHeight).addClass($(config.html).attr("class")));
				var grpFun = new Function('d', 'return d.' + config.group.trim()+ ';');
				// var grp = dim.group().reduceSum(grpFun);
				// var grp = dimBubble.group().reduceCount();
				var measure = [];
				var labels = [];
				labels.push(config.latitude);
				labels.push(config.longitude);
				measure.push(config.group);
				labels.push(config.field);
				labels.push(config.bubble);
				var grp = dimBubble.group().reduce(THIS.reduceFieldsAdd(measure,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(measure,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(measure,labels,config.filter,config.filtervalue));
				
				var bubbleData = grp.all();
				var allLatitude = 0,allLongitude = 0,validCount = 0;
				for (var i=0;i<bubbleData.length;i++) {
					if(bubbleData[i].value.latitude){
						allLatitude+= parseFloat(bubbleData[i].value.latitude);
						validCount++;
					}						
					if(bubbleData[i].value.longitude){
						allLongitude+= parseFloat(bubbleData[i].value.longitude);
					}						
				}
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
				}
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);
				 var marker = dc.leafletMarkerChart(config.html+"_map")
	              .dimension(dimBubble)
	              .group(grp).valueAccessor(grpAggre)// grpBubbles
	              .cluster(true)
	              .width(geoWidth)
	              .locationAccessor(function(d) { 
	            	  return d.key;// [d.value.latitude?parseFloat(d.value.latitude):0,d.value.longitude?parseFloat(d.value.longitude):0].toString();
	            	})
	              .height(geoHeight)
	              .center([allLatitude/validCount,allLongitude/validCount])
	              .zoom(2)
	              .rebuildMarkers(true)
              	  .renderPopup(true)
              	  .filterByArea(true)
	              .fitOnRender(true)
	              .renderPopup(true)
	              .popup(function(d,feature) {
	            	var numFormat = d3.format(config.numFormat);
	          		var masterData = THIS.masterData;
	          		var key = d.value[config.field] ? d.value[config.field] : d.key;;
	          		config.description = config.description || "";
	          		if(config.tooltipkey === "description"){
	          			if ($.trim(config.description) !== "" && masterData[config.fieldname][key]) {
	          				key = masterData[config.fieldname][key];
	          			}
	          		}else{
	          			if ($.trim(config.description) !== "" && masterData[config.fieldname][key]) {
	          				key = key+"-" + masterData[config.fieldname][key];
	          			}
	          		}
	          		var keyValue;
	          		if (config.aggregate.toLowerCase() === "calculated"){
	          			keyValue = eval(config.calculatedformula);
	          			keyValue = isNaN(keyValue) ? 0 : numFormat(keyValue);
	          		} else {
	          			var groupValue = isNaN(d.value[config.bubble]) ? 0 : d.value[config.bubble];
	          			var groupCount = isNaN(d.value.count)? 0 : d.value.count;
	          			if (groupValue !== 0 && groupCount !== 0){
	          				if (config.aggregate.toLowerCase() === "sum")
	          					keyValue = config.absolute === "Yes"?(config.prefix + numFormat(Math.abs(groupValue)) + config.suffix):(config.prefix+ numFormat(groupValue) + config.suffix);
	          				else if (config.aggregate.toLowerCase() === "count")
	          					keyValue = config.prefix
	          							+ numFormat(groupCount) + config.suffix;
	          				else{
	          					keyValue = config.absolute === "Yes"?(config.prefix+ numFormat(Math.abs(groupValue) / groupCount)+ config.suffix):(config.prefix+ numFormat(groupValue / groupCount)+ config.suffix);
	          				}
	          			} else {
	          				keyValue = 0;
	          			}
	          		}
	          		   var toolTip= $("<div/>");
	          		   toolTip.append($('<table/>').append($('<tr/>').append($('<td/>').html( key +':')).append($('<td/>').html(keyValue ).attr({"class":"font-bold"}))));
	          		if (config.linkaction){
	          			var actionDiv = $("<div/>");
	          			actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
	          			var actions = config.linkaction.split(",");
	          			for (var i = 0;i<actions.length;i++){
	          				actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
	          			}
	          			toolTip.append(actionDiv);
	          		}	          		
	          		var divTag=$('<div/>');
	          		toolTip.append(divTag);
	          		return toolTip[0].outerHTML;
	              })
	              .fitOnRedraw(true)
	              .cluster(true);
				 marker.on('filtered', function() {
						resetAllCharts(THIS);
						$(".reset", $(config.html)).data('isReset',true).show();
						resetAllProfiles(arguments[0]);
					}.bind(this));
					$(".reset", $(config.html)).off('click').on('click', function(e) {
						marker.filterAll();
						marker.map().setView(marker.center(),2);
						$(e.currentTarget).removeData("isReset").hide();
						resetAllCharts(THIS,config.html);
					}.bind(this));
				 marker.render();
				 var zoomInput = $("<input/>").addClass("onoffswitch-checkbox").attr({"type":"checkbox","id":"zoomonoff"+config.field});
				 zoomInput.unbind("change").bind("change",marker.map(),function(e){
					 if($(this).is(":checked"))
						 e.data.scrollWheelZoom.enable();
					 else
						 e.data.scrollWheelZoom.disable();
				 });
				 var zoomSwitch = $("<span/>").addClass("onoffswitch").append(zoomInput).append($("<label/>").addClass("onoffswitch-label").attr({"for":"zoomonoff"+config.field})
						 .append($("<span/>").addClass("onoffswitch-inner").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))
						 .append($("<span/>").addClass("onoffswitch-switch")));
				 $(config.html).parent().parent().parent().find(".onoffswitch").remove();
				 $(config.html).parent().parent().parent().find("header").find(".widget-toolbar").prepend(zoomSwitch);
				 if(config.zoom && config.zoom === "no"){
					 marker.map().scrollWheelZoom.disable();
				 }else{
					 zoomInput.attr("checked","checked");
				 }
				 // THIS.drawnGraph.push(marker);
				 /*
					 * d3.selectAll(config.html).selectAll("img.leaflet-marker-icon").call(tip);
					 * d3.selectAll(config.html).selectAll("img.leaflet-marker-icon").on('mouseover',
					 * tip.show) .on('mouseout', tip.hide);
					 */
				 /*
					 * var choro = dc.leafletChoroplethChart(config.html)
					 * .dimension(dim) .group(grp) .width(geoWidth)
					 * .height(geoHeight)
					 * .center([allLatitude/bubbleData.length,allLongitude/bubbleData.length])
					 * .zoom(5) // .geojson(geojson)
					 * .colors(colorbrewer.YlGnBu[7]) .colorDomain([
					 * d3.min(grp.all(), dc.pluck('value')), d3.max(grp.all(),
					 * dc.pluck('value')) ]) .colorAccessor(function(d,i) {
					 * return d.value; }) .featureKeyAccessor(function(feature) {
					 * return feature.properties.code; }) .renderPopup(true)
					 * .popup(function(d,feature) { return
					 * feature.properties.nameEn+" : "+d.value; })
					 * .legend(dc.leafletLegend().position('bottomright'));
					 * choro.render();
					 */
				/*
				 * var mapOptions = { center: new
				 * google.maps.LatLng(allLatitude/bubbleData.length,allLongitude/bubbleData.length),
				 * zoom: 5 },circles = {}, map = new
				 * google.maps.Map($(config.html)[0], mapOptions); var
				 * infoWindowCircle = new google.maps.InfoWindow(); for (var
				 * i=0;i<bubbleData.length;i++) { var center = {lat:
				 * parseFloat(bubbleData[i].value.latitude) , lng:
				 * parseFloat(bubbleData[i].value.longitude)}; var cityCircle =
				 * new google.maps.Circle({ strokeColor: '#FF0000',
				 * strokeOpacity: 0.8, strokeWeight: 2, clickable:true,
				 * fillColor: '#FF0000', fillOpacity: 0.35, content: '<div><h1>'+bubbleData[i].key +'</h1><div>'+config.bubble+ ':
				 * '+bubbleData[i].value.bubbleSum+'</div></div>', map: map,
				 * center: center, radius:
				 * Math.sqrt(bubbleData[i].value.bubbleSum)/map.getZoom() * 100
				 * }); google.maps.event.addListener(cityCircle, 'click',
				 * function(ev){ infoWindowCircle.setPosition(ev.latLng);
				 * infoWindowCircle.setContent(this.content);
				 * infoWindowCircle.open(map,cityCircle); });
				 * circles[bubbleData[i].key] = cityCircle; }
				 */
			};
			renderGoogleMap(config,dim,dimBubble);
			
		}catch (e) {
			console.log('Error Creating Google map' + e);
		}
	};
	Graphing.prototype.drawPairedRow = function(config) {
		var THIS = this;
		try {
			var dim = this.ndx.dimension(function(d) {		
				if(d[config.field] && d[config.pairedfield])
					return [d[config.field],d[config.pairedfield]];
			});
			var chartHeight = "";
			if (config.chartheight.toLowerCase() === "auto") {
				chartHeight = parseInt($(config.html).width());
				chartHeight = chartHeight > this.defaultHeight ? this.defaultHeight: chartHeight;
			} else {
				
				chartHeight = parseInt(config.chartheight);
			}
			var chartWidth;
			if (config.chartwidth.toLowerCase() === "auto") {
				chartWidth = parseInt($(config.html).width());
			} else {
				chartWidth = parseInt(config.chartwidth);
			}
			this.pairedRowCharts.push({
				"config" : config,
				"dim" : dim
			});
			$(config.html).data('resetedValue',false);
			if (config.filtervalues && $(config.html).find('select').length === 0) {
				this.constructSelect(config, dim);
				$(config.html).find('select').unbind('change').bind('change',function() {
					config.filterdefaultval = $(this).val();
					renderPairedRow(config, dim);
				});
				if(config.filterfrom === "bottom"){
					$(config.html).find('input').attr("checked","checked");
				}
				$(config.html).find("input").unbind('click').bind("click",function(){
					if($(this).is(":checked")){
						config.filterfrom = "bottom";
					}else{
						config.filterfrom = "top";
					}
					renderPairedRow(config, dim);
				});
			} else {
				$(config.html).find('select').val(config.filterdefaultval);
				if(config.filterfrom === "bottom"){
					$(config.html).find('input').attr("checked","checked");
				}
			}
			$(config.html).data("pairedfield",config.pairedfield);
			var renderPairedRow= function(config, dim) {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var grpFun = new Function('d', 'return d.' + config.group.trim()+ ';');
				var measure = [];
				var labels = [];
				labels.push(config.field);
				labels.push(config.pairedfield);
				measure.push(config.group);
				labels.push(config.fieldname);
				var pairedRow = dc.pairedRowChart(config.html);
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(measure,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(measure,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(measure,labels,config.filter,config.filtervalue));
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()* (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.radius, config.aggregate,config.filterfrom === "bottom"?0:1));
					pairedRow.data(function(group) {
						return group.top(defaultVal);
					});
				}
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField = config.group;
				}
				var tip = d3.tip().offset([-8, 0]).attr('class', config.linkaction?" tip-action":"d3-tip").html(function(e){
					var data = e;
					data.key = data.key.toString();
					return THIS.getTitleAgrregate(data,config,null,this);
				});
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);				 
	              pairedRow.dimension(dim)
	              .group(grp).valueAccessor(grpAggre)
	              .elasticX(true)
	              .width(chartWidth)
	              .height(chartHeight)
	              .colorCalculator(function(d) {
	                if (d.value[config.pairedfield] === config.leftrowkey) {
	                    return '#5A9BCA';
	                }
	                return '#C95AC7';
	              })
	              .leftKeyFilter(function(d) {
	            	  return d.value[config.pairedfield] === config.leftrowkey;// "Active";
	              })
				  .rightKeyFilter(function(d) {
				      return d.value[config.pairedfield] === config.rightrowkey;// "Inactive";
				  });
				 	pairedRow.on('filtered', function() {
						resetAllCharts(THIS);
						$(".reset", $(config.html)).data('isReset',true).show();
						resetAllProfiles(arguments[0]);
					}.bind(this));
					$(".reset", $(config.html)).off('click').on('click', function(e) {
						pairedRow.filterAll();
						$(e.currentTarget).removeData("isReset").hide();
						$(config.html).data('resetedValue',true);
						resetAllCharts(THIS,config.html);
					}.bind(this));
					pairedRow.render();
					d3.selectAll(config.html).select("svg").selectAll(".row").call(tip);
					d3.selectAll(config.html).select("svg").selectAll(".row").on('mouseover', tip.show);
			};
			renderPairedRow(config,dim);
			
		}catch (e) {
			console.log('Error Creating paired row chart' + e);
		}
	};
	Graphing.prototype.drawUSChart = function(config) {
		var THIS = this;
		 try {
			var label = "d['" + config.field + "'] || 'Others'";
			var dimFun = new Function('d', 'return ' + label + ';');
			var dim = this.ndx.dimension(dimFun);
			var dimBubble = this.ndx.dimension(function(d) {
				return d[config.bubble] || 'Others';
			});
			$(config.html).data('resetedValue',false);
			var geoHeight = "";
			if (config.chartheight.toLowerCase() === "auto") {
				geoHeight = parseInt($(config.html).width());
				geoHeight = geoHeight > this.defaultHeight ? this.defaultHeight
						: geoHeight;
			} else {
				
				geoHeight = parseInt(config.chartheight);
			}
			var geoWidth;
			if (config.chartwidth.toLowerCase() === "auto") {
				geoWidth = parseInt($(config.html).width());
				geoWidth = geoWidth > this.defaultHeight ? this.defaultHeight : geoWidth;
				geoWidth = geoWidth * 3 / 2;
			} else {
				geoWidth = parseInt(config.chartwidth);
			}
			this.usCharts.push({
				"config" : config,
				"dim" : dim,
				"dimBubble":dimBubble
			});
			var stateClick;
			var renderUSChart = function(config, dim,dimBubble) {
				var numFormat = config.numFormat? d3.format(config.numFormat) : ",~f";

				var grpFun = new Function('d', 'return d.' + config.group.trim()
						+ ';');
				var grp = dim.group().reduceSum(grpFun);
				
				var grpBubbleTemp = dimBubble.group().reduceSum(grpFun);
				
				var grpBubbles = dimBubble
						.group()
						.reduce(
								function(p, v) {
									p.bubbleSum += +(!isNaN(+v[config.group])) ? +v[config.group]
											: 0;
									p.latitude = v[config.latitude];
									p.longitude = v[config.longitude];
									return p;
								},
								function(p, v) {
									p.bubbleSum -= +(!isNaN(+v[config.group])) ? +v[config.group]
											: 0;
									p.latitude = v[config.latitude];
									p.longitude = v[config.longitude];
									return p;
								}, function() {
									return {
										bubbleSum : 0.00,
										latitude : 0.00,
										longitude : 0.00
									};
								});
				var numberFormat = d3.format(",f");
				var paceColors = {};
				var bubbleColors = {};
				var cLevels = 9;
				var cellWidth = 30, // Width of color legend cell
				cbarWidth = cellWidth * cLevels, cbarHeight = 15;

				var stateWiseData = grp.all();
				var bubbleData = grpBubbles.all();
				var minPace = grp.top(grp.size())[grp.size() - 1].value, maxPace = grp
						.top(grp.size())[0].value;
				var minBubble = grpBubbleTemp.top(grpBubbleTemp.size())[grpBubbleTemp
						.size() - 1].value, maxBubble = grpBubbleTemp
						.top(grpBubbleTemp.size())[0].value;
				maxBubble = 0;
				bubbleData.forEach(function(d){if(d.value[config.latitude] !== null && d.value[config.longitude] !== null && d.value.bubbleSum>maxBubble)maxBubble = d.value.bubbleSum});
				var tripPalette = colorbrewer[config.chartColor][cLevels];
				var paceScale = d3.scaleQuantize()
						.domain([ minPace, maxPace ]).range(tripPalette);
				var startColor = config.startColor || "red";
				var endColor = config.endColor || "#e377c2";
				var bubbleColor = d3.scaleLinear().domain(
						[ minBubble, maxBubble ]).range([ startColor, endColor ]);
				var linearScale = d3.scaleLinear().domain(
						[ minBubble, maxBubble ]).range([ 3, 50]);
				var stateData = {}, bubbles = [];
				function buildGradient(palette, gradientId) {
					d3.select('.datamap').append('linearGradient').attr('id',
							gradientId).attr("gradientUnits", "userSpaceOnUse")
							.attr("x1", 0).attr("y1", 0).attr("x2", cbarWidth)
							.attr("y2", 0).selectAll('stop').data(palette)
							.enter().append('stop').attr('offset',
									function(d, i) {
										return i / (cLevels - 1) * 100.0 + '%';
									}).attr('stop-color', function(d) {
								return d;
							});
				};
				for ( var i = 0; i < stateWiseData.length; i++) {
					if (stateWiseData[i].key
							&& $.trim(stateWiseData[i].key) !== ""
							) {
						paceColors[stateWiseData[i].key] = paceScale(stateWiseData[i].value);
						stateData[stateWiseData[i].key] = config.prefix
								+ numberFormat(stateWiseData[i].value);
					}
				}
				paceColors.defaultFill = "#fff";
				for ( var i = 0; i < bubbleData.length; i++) {
					if (Math.round(bubbleData[i].value.bubbleSum) !== 0
							&& bubbleData[i].value.latitude
							&& $.trim(bubbleData[i].value.latitude) !== ""
							&& bubbleData[i].value.longitude
							&& $.trim(bubbleData[i].value.longitude) !== "") {
						bubbleColors[bubbleData[i].key] = bubbleColor(bubbleData[i].value.bubbleSum);
						bubbles
								.push({
									name : bubbleData[i].key,
									latitude : bubbleData[i].value.latitude,
									longitude : bubbleData[i].value.longitude,
									fillKey : bubbleData[i].key,
									label : config.label,
									/*
									 * address:response[i].address1,
									 * city:response[i].city,
									 * state:response[i].state,
									 */
									salesamount : config.prefix+ numberFormat(bubbleData[i].value.bubbleSum),
									radius : linearScale(bubbleData[i].value.bubbleSum)
								});
					}
				}
				bubbleColors.defaultFill = config.defaultFill || "#ddd";
				function statePopup(geography, data) {
					data = data || 0;
					var html = '<div class="hoverinfo">'
							+ '<div class="hover-state-title" align="center">'
							+ '<b>' + geography.properties.name + '</b>'
							+ '</div>' + '<div class="hover-state-stats">'
							+ '<table>' + '<tr>' + '<td>' + config.label
							+ ':</td>' + '<td>' + numFormat(data) + '</td>' + '</tr>'
							+ '</table>' + '</div>' + '</div>';
					return html;
				}
				;
				
				$(config.html).find("svg").remove();
				$(config.html).find(".datamaps-hoverover").remove();
				var map = new Datamap(
						{
							scope : config.maptype,// world,usa
							done : function(datamap) {
								datamap.svg.call(d3.zoom().on("zoom",redraw));
								function redraw() {
									datamap.svg.selectAll("g").attr(
											"transform", d3.event.transform
													 );
								}
								datamap.svg.selectAll('.datamaps-subunit').on(
										'click',
										function(geography) {
											stateClick(dim, geography,config.html, this,true);
										});
							},
							element : $(config.html)[0],
							projection : config.projection || 'mercator',
							fills : bubbleColors,
							height : geoHeight,
							data : stateData,
							geographyConfig : {
								borderWidth : 1,
								borderColor : config.borderColor || '#999999',
								popupOnHover : true,
								highlightOnHover : true,
								highlightFillColor : config.highlightFillColor || '#bbaa99',
								highlightBorderColor : config.highlightBorderColor || '#999999',
								highlightBorderWidth : 2,
								popupTemplate : function(geography, data) {
									return statePopup(geography, data);
								}
							}
						});
				if (config.showCountryLabels == "Yes"){
					map.labels();
				}
				stateClick = function(dimState, geography, parentDiv, ele,isClick) {
					var filteredStates = [];
					if ($('.datamaps-subunit[class*=deselected]').length === 0) {
						$('.datamaps-subunit').each(function(){
							$(this).attr("class",$(this).attr("class")+ " deselected");
						});
						$(".reset", $(parentDiv)).show();
					}
					if ($(ele).attr("class").indexOf("deselected") !== -1) {
						$(ele).attr(
								{
									"class" : $(ele).attr("class").replace(
											"deselected", "selected"),
									"state" : geography.id
								});
					} else {
						$(ele).attr(
								{
									"class" : $(ele).attr("class").replace(
											"selected", "deselected"),
									"state" : geography.id
								});
					}
					dimState.filterAll();
					if ($('.datamaps-subunit:not([class*=deselected])').length === 0) {
						$('.datamaps-subunit').attr(
								"class",
								$('.datamaps-subunit').attr("class").replace(
										" deselected", "").replace(" selected",
										"")).css({
							"stroke-width" : "1px",
							"fill-opacity" : 1
						});
						$(".reset", $(parentDiv)).hide();
						$(config.html).removeData("filters");
						config.defaultFiltervalue = [];
					} else {
						$('.datamaps-subunit:not([class*=deselected])').each(
								function(index, ele) {
									if ($(ele).attr("state")
											&& $(ele).attr("state") !== null
											&& $(ele).attr("state") !== "")
										filteredStates.push($(ele)
												.attr("state"));
								});
						dimState.filter(function(d) {
							return filteredStates.indexOf(d) > -1;
						});
						$(config.html).data("filters",filteredStates);
						config.defaultFiltervalue = filteredStates;
						
					}
					resetAllCharts(THIS, config.html);
					if(isClick){
						resetAllProfiles(map,"world");
					}
				};
				function bubbleClick(dimBubble, ele, parentDiv) {
					var filteredBubbles = [];
					if ($('.datamaps-bubble[class*=deselected]').length === 0) {
						$('.datamaps-bubble').attr(
								"class",
								$('.datamaps-bubble').attr("class")
										+ " deselected");
						$(".reset", $(parentDiv)).show();
					}
					if ($(ele).attr("class").indexOf("deselected") !== -1) {
						$(ele).attr(
								{
									"class" : $(ele).attr("class").replace(
											"deselected", "selected")
								});
					} else {
						$(ele).attr(
								{
									"class" : $(ele).attr("class").replace(
											"selected", "deselected")
								});
					}
					dimBubble.filterAll();
					if ($('.datamaps-bubble:not([class*=deselected])').length === 0) {
						$('.datamaps-bubble').attr(
								"class",
								$('.datamaps-bubble').attr("class").replace(
										" deselected", "").replace(" selected",
										"")).css({
							"stroke-width" : "1px",
							"fill-opacity" : 1
						});
						$(".reset", $(parentDiv)).hide();
					} else {
						$('.datamaps-bubble:not([class*=deselected])').each(
								function(index, ele) {
									if ($(ele).attr("state")
											&& $(ele).attr("state") !== null
											&& $(ele).attr("state") !== "")
										filteredBubbles.push($(ele).attr(
												"state"));
								});
						dimBubble.filter(function(d) {
							return filteredBubbles.indexOf(d) > -1;
						});
					}
				}

				// buildGradient(tripPalette, 'tripGradient');
				var visWidth = $(config.html).outerWidth();

				/*
				 * var cbar = d3.select('.datamap').append('g').attr('id',
				 * 'colorBar').attr('class', 'colorbar');
				 * 
				 * cbar.append('rect').attr('id', 'gradientRect').attr('width',
				 * cbarWidth).attr('height', cbarHeight).style('fill',
				 * 'url(#paceGradient)'); cbar.append('text').attr('id',
				 * 'colorBarMinText').attr('class', 'colorbar').attr('x',
				 * 0).attr('y', cbarHeight + 15) .attr('dx', 0).attr('dy',
				 * 0).attr('text-anchor', 'start');
				 * cbar.append('text').attr('id',
				 * 'colorBarMaxText').attr('class', 'colorbar').attr('x',
				 * cbarWidth).attr('y', cbarHeight + 15).attr('dx',
				 * 0).attr('dy', 0).attr( 'text-anchor', 'end');
				 * 
				 * cbar.attr('transform', 'translate(' + (visWidth - cbarWidth) /
				 * 2.0 + ', 30)'); // Shift to center
				 */
				d3.select('#gradientRect').style('fill', 'url(#tripGradient)');
				var minPrefix = d3.formatPrefix(minPace);
				var minSymbol = minPrefix.symbol; // "G"
				var minValue = Math.round(minPrefix(minPace)); // 1.21
				var maxPrefix = d3.formatPrefix(maxPace);
				var maxSymbol = maxPrefix.symbol; // "G"
				var maxValue = Math.round(maxPrefix(maxPace)); // 1.21
				d3.select('#colorBarMinText').text(minValue + minSymbol);
				d3.select('#colorBarMaxText').text(maxValue + maxSymbol);

				// map.updateChoropleth(paceColors);
				$(".reset", $(config.html)).off('click').on('click',dim,
						function() {
							$(this).hide();
							dim.filterAll();
							$('.datamaps-subunit').attr(
									"class",
									$('.datamaps-subunit').attr("class")
											.replace(" deselected", "")
											.replace(" selected", "")).css({
								"stroke-width" : "1px"
							});
							$(config.html).data('resetedValue',true);
							resetAllCharts(THIS);
							$(config.html).removeData("filters");
							resetAllProfiles(map,"world");
						});
				map.bubbles(bubbles,
								{
									popupTemplate : function(geo, data) {
										var html = '<div class="hoverinfo">'
												+ '<div class="hover-state-title" align="center">'
												+ '<b>'
												+ data.name
												+ '</b>'
												+ '</div>'
												+ '<div class="hover-state-stats">'
												+ '<table>' + '<tr>' + '<td>'
												+ data.label + ':</td>'
												+ '<td>' + numFormat(data.salesamount)
												+ '</td>' + '</tr>'
												+ '</table>' + '</div>'
												+ '</div>';
										return html;
										// return "<div class='hoverinfo'><h3
										// class='margin-2'>Name
										// : " + data.name + "</h3><div>Sales
										// Amount
										// :"+data.salesamount+"</div></div>";
									},
									borderWidth : function(data) {
										return data.radius / 20;
									}
								});

				/*
				 * map.svg.selectAll('.datamaps-bubble').on('click', function() {
				 * bubbleClick(dimBubble,this,config.html); });
				 */
			};

			this.refreshUSChart = function(currentConfigHtml) {
				for ( var x = 0; x < this.usCharts.length; x++) {
					if (!currentConfigHtml
							|| this.usCharts[x]["config"].html !== currentConfigHtml) {
						renderUSChart(this.usCharts[x]["config"],
								this.usCharts[x]["dim"],this.usCharts[x]["dimBubble"]);
					}
				}
			};
			renderUSChart(config, dim,dimBubble);
			if (viewSettings && viewSettings.length > 0){
				$.each(dataAnalyser.viewSettings,function(){
					if (this.anchorName ===  config.field && this.filterValues){
						config.defaultFiltervalue = this.filterValues;
						return;
					}
				});
			}
			profileAnalyser.chartFilters.map(
					function(chartFilter){
						if (chartFilter.chartid == config.field)
							config.defaultFiltervalue = chartFilter.defaultvalue;
						}
					);
			if(config.defaultFiltervalue && !$(".reset", $(config.html)).data('isReset')){
				for (var state = 0; state < config.defaultFiltervalue.length;state++){
					var tempState = config.defaultFiltervalue[state];
					if ($('.datamaps-subunit.'+tempState).length > 0){
						stateClick(dim,{"id":tempState},config.html,$('.datamaps-subunit.'+tempState)[0]);
					}
				}
			}
		
		 } catch (e) { console.log('Error Creating US Chart' + e); }
		 
	};
	Graphing.prototype.drawGeo = function(config) {

		try {

			var numberFormat = d3.format(".2f");
			var geoChart = dc.geoChoroplethChart(config.html);// "#us-chart"
			var THIS = this;
			d3.csv("web/data/vc.csv", function(csv) {
				var data = crossfilter(csv);

				var states = data.dimension(function(d) {
					return d["State"];
				});
				var stateRaisedSum = states.group().reduceSum(function(d) {
					return d["Raised"];
				});

				d3.json("web/data/us-states.json", function(statesJson) {
					geoChart.width(990).height(500).dimension(states).group(
							stateRaisedSum).colors(
							d3.scaleQuantize().range(
									[ "#E2F2FF", "#C4E4FF", "#9ED2FF",
											"#81C5FF", "#6BBAFF", "#51AEFF",
											"#36A2FF", "#1E96FF", "#0089FF",
											"#0061B5" ])).colorDomain(
							[ 0, 200 ]).colorCalculator(function(d) {
						return d ? geoChart.colors()(d) : '#ccc';
					}).overlayGeoJson(statesJson.features, "state",
							function(d) {
								return d.properties.name;
							}).title(
							function(d) {
								return "State: " + d.key
										+ "\nTotal Amount Raised: "
										+ numberFormat(d.value ? d.value : 0)
										+ "M";
							});

					geoChart.on('filtered', function(chart) {
						if (this.refreshTable)
							this.refreshTable(chart);

					}.bind(THIS));
					$(".reset", $(config.html)).on('click', function() {
						geoChart.filterAll();
						dc.redrawAll();
					}.bind(THIS));

					THIS.drawnGraph.push(geoChart);

					geoChart.render();
					
				});
			});
		} catch (e) {
			console.log('Error Creating Geo Chart' + e);

		}
	};

	Graphing.prototype.drawBubble = function(config) {
		var THIS = this;
		var dim1;
		var dimDate = null;
		var measures = [];
		var labels = [];
		labels.push(config.field);
		$(config.html).data('resetedValue',false);
		if (config.xaxistype === "date"){
			$(config.html).data("xaxistype",config.xaxistype);
			dim1 = this.ndx.dimension(function(d) {
				return d[config.field]+"-"+d[config.xAxis];
			});
			dimDate = this.ndx.dimension(function(d) {
				return d["date-"+config.xAxis];
			});
			labels.push("date-"+config.xAxis);
		}else{
			dim1 = this.ndx.dimension(function(d) {
				return d[config.field];
			});
			measures.push(config.xAxis);
		}
		if(config.colorfield){
			labels.push(config.colorfield);
		}
		if (measures.indexOf(config.yAxis)===-1)
			measures.push(config.yAxis);
		if (measures.indexOf(config.radius)===-1)
			measures.push(config.radius);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.radiusPrefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.radiusSuffix
			});
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderBubble(config, dim1);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderBubble(config, dim1);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}		
		var renderBubble = function(config, dim,dimDate) {
			var numberFormat = d3.format(config.numFormat);
			try {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var bubbleChart = dc.bubbleChart(config.html);
				
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(measures,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(measures,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(measures,labels,config.filter,config.filtervalue));
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()* (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.radius, config.aggregate,config.filterfrom === "bottom"?0:1));
					bubbleChart.data(function(group) {
						return group.top(defaultVal);
					});
				}				
				var bubbleChartWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					bubbleChartWidth = parseInt($(config.html).width());
				} else {
					bubbleChartWidth = config.chartwidth;
				}
				var bubbleChartHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					bubbleChartHeight = parseInt($(config.html).width());
					bubbleChartHeight = bubbleChartHeight > THIS.defaultHeight ? THIS.defaultHeight
							: bubbleChartHeight;
				} else {
					
					bubbleChartHeight = config.chartheight;
				}
				
				var grpYAxis = THIS.getValueByAgrregate(config.yAxisAggre,config.yAxis,config.filtervalue,config.absolute);
				var grpradius = THIS.getValueByAgrregate(config.radiusAggre,config.radius,config.filtervalue,config.absolute);
				
				var yAxisMax = d3.max(grp.all(), grpYAxis);

				var radiusMax = d3.max(grp.all(), grpradius);
				var radiusMin = d3.min(grp.all(), grpradius);
				var maxBubbleRelativeSize =  config.maxbubblesize;
				if (radiusMax === radiusMin)
					maxBubbleRelativeSize = 0.01;
				var xaxisformate;
				var xAxisMax;
				var grpXAxis;
				var xAxisUnit;
				var xAxisFormat;
				var xAxisField;
				if (config.xaxistype === "date"){
					grpXAxis = function(d){
						return d.value["date-"+config.xAxis];
					};
					var xAxisMin = dimDate.bottom(1)[0]["date-"+config.xAxis];
					xAxisMax = dimDate.top(1)[0]["date-"+config.xAxis];
					xaxisformate = d3.time.scale().domain([xAxisMin, xAxisMax]).nice(d3.time.day);
					xAxisUnit = d3.time.day;
					xAxisFormat = d3.timeFormat(config.xaxistooltipdateformat);
					xAxisField = "date-"+config.xAxis;
				}else{
					grpXAxis = THIS.getValueByAgrregate(config.xAxisAggre,config.xAxis,config.filtervalue,config.absolute);
					xAxisMax = d3.max(grp.all(), grpXAxis);
					xaxisformate =  d3.scaleLinear().domain([1, xAxisMax ]);
					xAxisUnit = dc.units.ordinal();
					xAxisFormat = config.xaxisformate? d3.format(config.xaxisformate):d3.format(config.numFormat);
					xAxisField = config.xAxis;
				}				
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
					var rad;
					var xAxisValue;
					var yAxisValue;
					var groupValueRadius = isNaN(d.value[config.radius]) ? 0 : d.value[config.radius];
					var groupValueXaxis = isNaN(d.value[config.xAxis]) ? 0 : d.value[config.xAxis];
					var groupValueYaxis = isNaN(d.value[config.yAxis]) ? 0 : d.value[config.yAxis];
					var groupCount = isNaN(d.value.count)? 0 : d.value.count;
					if (config.radiusAggre.toLowerCase() === "sum"){
						rad = groupValueRadius;				
					}else if (config.radiusAggre.toLowerCase() === "count"){
						rad = groupCount;
					}else{
						rad = groupValueRadius / groupCount;
					}
					if (config.xAxisAggre.toLowerCase() === "sum"){
						xAxisValue = groupValueXaxis;
					}else if (config.xAxisAggre.toLowerCase() === "count"){
						xAxisValue = groupCount;
					}else{
						xAxisValue = groupValueXaxis / groupCount;
					}
					if (config.yAxisAggre.toLowerCase() === "sum"){
						yAxisValue = groupValueYaxis;
						
					}else if (config.yAxisAggre.toLowerCase() === "count"){
						yAxisValue = groupCount;
					}else{
						yAxisValue = groupValueYaxis / groupCount;
					}
					var key = d.value[config.field];
					if(config.tooltipkey === "code"){
						key = d.value[config.field];
					}else if(config.tooltipkey === "description"){
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = THIS.masterData[config.fieldname][key];
						}
					}else{
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = d.value[config.field]+"-" + THIS.masterData[config.fieldname][key];
						}
					}					
					var tooltip=$("<div/>");
					tooltip.append($('<table/>').append($('<tr/>').append($('<th/>').html(key).attr({"class":"d3-tip-header"}))).append($('<tr/>').append($('<td/>').html(config.radiusLabel).attr({"class":"d3-tip-key"}))
						.append($('<td/>').html(config.radiusPrefix+ numberFormat(rad)+ config.radiusSuffix).attr({"class":"d3-tip-value"}))).append($('<tr/>').append($('<td/>').html(config.xAxisLabel).attr({"class":"d3-tip-key"}))
						.append($('<td/>').html(config.xAxisPrefix+ xAxisFormat(xAxisValue)+ config.xAxisSuffix).attr({"class":"d3-tip-value"}))).append($('<tr/>').append($('<td/>').html(config.yAxisLabel)).append($('<td/>')
						.html(config.yAxisPrefix+ numberFormat(yAxisValue)+ config.yAxisSuffix).attr({"class":"d3-tip-value"}))) );
					if (config.linkaction){
						var actionDiv = $("<div/>");
						actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
						var actions = config.linkaction.split(",");
						for (var i = 0;i<actions.length;i++){
							actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
						}
						tooltip.append(actionDiv);
					}
					var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action","id":"keep-only"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
					$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+key+'")');
					var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action","id":"exclude"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
					$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ key +'",true)');
					var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
					divTag.append(anchorTag).append(excludeAnchor);
					tooltip.append(divTag);
					return tooltip[0].outerHTML;
				});
				bubbleChart
						.width(bubbleChartWidth)
						.height(bubbleChartHeight)
						.margins({
							top : config.margintop,
							left : config.marginleft,
							right : config.marginright,
							bottom : config.marginbottom
						})
						.dimension(dim)
						.group(grp)
						.colors(d3.scale.category10())
						.keyAccessor(grpXAxis)
						.valueAccessor(grpYAxis)
						.radiusValueAccessor(grpradius)
						// .x(d3.scaleOrdinal())
						.x(xaxisformate)						
						// .round(d3.time.month.round)//day,month
						.xUnits(xAxisUnit)// days,months
						// .y(d3.scaleOrdinal())
						.y(d3.scaleLinear().domain([ 1, yAxisMax ]))
						// .y(d3.scaleOrdinal())
						.minRadius(5)
						// .xUnits(dc.units.ordinal)
						.r(d3.scaleLinear().domain([ 1, radiusMax ]))
						// .r(d3.scaleOrdinal())
						.minRadiusWithLabel(20)
						.elasticY(true)
						.elasticX(true)
						.elasticRadius(true)
						.yAxisPadding(yAxisMax / 10)
						// To calculate dynamically later - adds points before
						// and
						// after
						// min & max values
						// .xAxisPadding(xAxisMax / 10)
						// To calculate dynamically later - adds points before
						// and
						// after
						// min & max values
						.xAxisLabel(config.xAxisLabel)
						.yAxisLabel(config.yAxisLabel)
						.maxBubbleRelativeSize(maxBubbleRelativeSize)
						.renderHorizontalGridLines(true)
						.renderVerticalGridLines(true)
						.title(function () { return ""; });
						
				bubbleChart.yAxis().ticks(10).tickFormat(
						function(d) {
							var prefix = d3.formatPrefix(d);
							return config.yAxisPrefix + prefix(d)
									+ prefix.symbol + config.yAxisSuffix;
						});
				if (config.xaxistype !== "date"){
					bubbleChart.xAxisPadding(xAxisMax / 10).xAxis().tickFormat(
							function(d) {
								var prefix = d3.formatPrefix(d);
								if(d%1 === 0){
									return config.xAprefix(pefix(d)
									+ prefix.symbol + config.xAxisSuffix);
								}else{
									return config.xAxisPrefix + d + config.xAxisSuffix;
								}
								
							});
				}
				bubbleChart.on('pretransition', function(){
					var xCurrentMax = d3.max(grp.all(), grpXAxis);
					var yCurrentMax = d3.max(grp.all(), grpYAxis);
					var xRatio = xCurrentMax/xAxisMax;
					var yRatio = yCurrentMax/yAxisMax;
					if((xRatio < .05 || xRatio > 1) && config.elastic === 'yes'){
						bubbleChart.elasticX(true)
				    	.x(d3.scaleLinear().domain([ 1,xCurrentMax  ])).xAxisPadding(xCurrentMax / 10);
				        xAxisMax = xCurrentMax;
					} else {
						bubbleChart.elasticX(false);
				    }
					
					if((yRatio < .1 || yRatio > 1) && config.elastic === 'yes'){
						yAxisMax = yCurrentMax;
						bubbleChart.elasticY(true)
				    	.y(d3.scaleLinear().domain([ 1, yCurrentMax ])).yAxisPadding(yCurrentMax / 10);
					} else {
						bubbleChart.elasticY(false);
				    }
				  });
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					bubbleChart.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					bubbleChart.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				bubbleChart.on('filtered', function() {
					resetAllCharts(THIS, config.html);
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					bubbleChart.filterAll();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					resetAllCharts(THIS);
				}.bind(this));
				THIS.drawnGraph.push(bubbleChart);
				bubbleChart.render();
				applyDefaultFilter(config,bubbleChart);
				d3.selectAll(config.html).selectAll(".bubble").call(tip);
				d3.selectAll(config.html).selectAll(".bubble").on('mouseover', tip.show);
			} catch (e) {
				console.log('Error Creating Bubble Chart' + e);
			}
		};
		renderBubble(config, dim1,dimDate);
	};
	Graphing.prototype.drawScatterPlot = function(config) {
		var THIS = this;
		var dim1;
		var dimDate = null;
		var measures = [];
		var labels = [];
		labels.push(config.field);
		$(config.html).data('resetedValue',false);
		if (config.xaxistype === "date"){
			$(config.html).data("xaxistype",config.xaxistype);
			dim1 = this.ndx.dimension(function(d) {
				return d[config.field]+"-"+d[config.xAxis];
			});
			dimDate = this.ndx.dimension(function(d) {
				return d["date-"+config.xAxis];
			});
			labels.push("date-"+config.xAxis);
		}else{
			dim1 = this.ndx.dimension(function(d) {
				return [d[config.xAxis],d[config.yAxis]];
			});
			measures.push(config.xAxis);
		}
		if(config.colorfield){
			labels.push(config.colorfield);
		}
		if (measures.indexOf(config.yAxis)===-1)
			measures.push(config.yAxis);
		if (measures.indexOf(config.radius)===-1)
			measures.push(config.radius);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.radiusPrefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.radiusSuffix
			});
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderScatterPlot(config, dim1);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderScatterPlot(config, dim1);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}		
		var renderScatterPlot = function(config, dim,dimDate) {
			var numberFormat = d3.format(config.numFormat);
			try {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var scatterPlotChart = dc.scatterPlot(config.html);
				
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(measures,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(measures,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(measures,labels,config.filter,config.filtervalue));
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()* (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.radius, config.aggregate,config.filterfrom === "bottom"?0:1));
					scatterPlotChart.data(function(group) {
						return group.top(defaultVal);
					});
				}				
				var scatterPlotChartWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					scatterPlotChartWidth = parseInt($(config.html).width());
				} else {
					scatterPlotChartWidth = config.chartwidth;
				}
				var scatterPlotChartHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					scatterPlotChartHeight = parseInt($(config.html).width());
					scatterPlotChartHeight = scatterPlotChartHeight > THIS.defaultHeight ? THIS.defaultHeight
							: scatterPlotChartHeight;
				} else {
					
					scatterPlotChartHeight = config.chartheight;
				}
				
				var grpYAxis = THIS.getValueByAgrregate(config.yAxisAggre,
						config.yAxis,config.filtervalue,config.absolute);
				
				var yAxisMax = d3.max(grp.all(), grpYAxis);
				var xaxisformate;
				var xAxisMax;
				var grpXAxis;
				var xAxisUnit;
				var xAxisFormat;
				var xAxisField;
				if (config.xaxistype === "date"){
					grpXAxis = function(d){
						return d.value["date-"+config.xAxis];
					};
					var xAxisMin = dimDate.bottom(1)[0]["date-"+config.xAxis];
					xAxisMax = dimDate.top(1)[0]["date-"+config.xAxis];
					xaxisformate = d3.time.scale().domain([xAxisMin, xAxisMax]).nice(d3.time.day);
					xAxisUnit = d3.time.day;
					xAxisFormat = d3.timeFormat(config.xaxistooltipdateformat);
					xAxisField = "date-"+config.xAxis;
				}else{
					grpXAxis = THIS.getValueByAgrregate(config.xAxisAggre,config.xAxis,config.filtervalue,config.absolute);
					xAxisMax = d3.max(grp.all(), grpXAxis);
					xaxisformate =  d3.scaleLinear().domain([1, xAxisMax ]);
					xAxisUnit = dc.units.ordinal();
					xAxisFormat = config.xaxisformate? d3.format(config.xaxisformate):d3.format(config.numFormat);
					xAxisField = config.xAxis;
				}
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
					var rad;
					var xAxisValue;
					var yAxisValue;
					var groupValueRadius = isNaN(d.value[config.radius]) ? 0 : d.value[config.radius];
					var groupValueXaxis = isNaN(d.value[config.xAxis]) ? 0 : d.value[config.xAxis];
					var groupValueYaxis = isNaN(d.value[config.yAxis]) ? 0 : d.value[config.yAxis];
					var groupCount = isNaN(d.value.count)? 0 : d.value.count;
					if (config.radiusAggre.toLowerCase() === "sum"){
						rad = groupValueRadius;				
					}else if (config.radiusAggre.toLowerCase() === "count"){
						rad = groupCount;
					}else{
						rad = groupValueRadius / groupCount;
					}
					if (config.xAxisAggre.toLowerCase() === "sum"){
						xAxisValue = groupValueXaxis;
					}else if (config.xAxisAggre.toLowerCase() === "count"){
						xAxisValue = groupCount;
					}else{
						xAxisValue = groupValueXaxis / groupCount;
					}
					if (config.yAxisAggre.toLowerCase() === "sum"){
						yAxisValue = groupValueYaxis;
						
					}else if (config.yAxisAggre.toLowerCase() === "count"){
						yAxisValue = groupCount;
					}else{
						yAxisValue = groupValueYaxis / groupCount;
					}
					var key = d.value[config.field];
					if(config.tooltipkey === "code"){
						key = d.value[config.field];
					}else if(config.tooltipkey === "description"){
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = THIS.masterData[config.fieldname][key];
						}
					}else{
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = d.value[config.field]+"-" + THIS.masterData[config.fieldname][key];
						}
					}
					var tooltip= $("<div/>");
						tooltip.append($('<table/>').append($('<tr/>').append($('<th/>').html(key).attr({"class":"d3-tip-header"})))
						.append($('<tr/>').append($('<td/>').html(config.radiusLabel).attr({"class":"d3-tip-key"})).append($('<td/>').html(config.radiusPrefix+ numberFormat(rad)+ config.radiusSuffix).attr({"class":"d3-tip-value"})))		
						.append($('<tr/>').append($('<td/>').html(config.xAxisLabel).attr({"class":"d3-tip-key"})).append($('<td/>').html(config.xAxisPrefix+ xAxisFormat(xAxisValue)+ config.xAxisSuffix).attr({"class":"d3-tip-value"})))		
						.append($('<tr/>').append($('<td/>').html(config.yAxisLabel).attr({"class":"d3-tip-key"})).append($('<td/>').html(config.yAxisPrefix+numberFormat(yAxisValue)+ config.yAxisSuffix).attr({"class":"d3-tip-value"})))		
						);
					if (config.linkaction){
						var actionDiv = $("<div/>");
						actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
						var actions = config.linkaction.split(",");
						for (var i = 0;i<actions.length;i++){
							actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
						}
						tooltip.append(actionDiv);
					}
					return tooltip[0].outerHTML;
				});
				scatterPlotChart
						.width(scatterPlotChartWidth)
						.height(scatterPlotChartHeight)
						.margins({
							top : config.margintop,
							left : config.marginleft,
							right : config.marginright,
							bottom : config.marginbottom
						})
						.dimension(dim)
						.group(grp)
						.colors(d3.scale.category10())
						.keyAccessor(grpXAxis)
						.valueAccessor(grpYAxis)
						.symbolSize(8)
						.clipPadding(10)
						.excludedOpacity(0.5)
						.brushOn(config.brushOn)
						// .radiusValueAccessor(grpradius)
						// .x(d3.scaleOrdinal())
						.x(xaxisformate)						
						// .round(d3.time.month.round)//day,month
						// .xUnits(xAxisUnit)//days,months
						// .y(d3.scaleOrdinal())
						// .y(d3.scaleLinear().domain([ 1, yAxisMax ]))
						// .y(d3.scaleOrdinal())
						// .minRadius(5)
						// .xUnits(dc.units.ordinal)
						// .r(d3.scaleLinear().domain([ 1, radiusMax ]))
						// .r(d3.scaleOrdinal())
						// .minRadiusWithLabel(20)
						// .yAxis().tickFormat(d3.format(".2s"))
						.elasticY(true)
						.elasticX(true)
						// .elasticRadius(true)
						// .yAxisPadding(yAxisMax / 10)
						// To calculate dynamically later - adds points before
						// and
						// after
						// min & max values
						// .xAxisPadding(xAxisMax / 10)
						// To calculate dynamically later - adds points before
						// and
						// after
						// min & max values
						.xAxisLabel(config.xAxisLabel)
						.yAxisLabel(config.yAxisLabel)
						// .maxScatterPlotRelativeSize(maxScatterPlotRelativeSize)
						.renderHorizontalGridLines(true)
						.renderVerticalGridLines(true)
						.title(function () { return ""; });
				scatterPlotChart.yAxis().ticks(10).tickFormat(
						function(d) {
							var numFormat;
							if (config.yAxisNumberFormat){
								numFormat= d3.format(config.yAxisNumberFormat)(d);
							}else{
								var prefix = d3.formatPrefix(d);
								numFormat= prefix(d) + prefix.symbol;
							}
							return config.yAxisPrefix + numFormat
							 + config.yAxisSuffix;
						});
				if (config.xaxistype !== "date"){
					scatterPlotChart.xAxisPadding(xAxisMax / 10).xAxis().tickFormat(
							function(d) {
								var prefix = d3.formatPrefix(d);
								if(d%1 === 0){
									return config.xAxisPrefix + prefix(d)
									+ prefix.symbol + config.xAxisSuffix;
								}else{
									return config.xAxisPrefix + d + config.xAxisSuffix;
								}
								
							});
				}
				scatterPlotChart.on('pretransition', function(){
					var xCurrentMax = d3.max(grp.all(), grpXAxis);
					var yCurrentMax = d3.max(grp.all(), grpYAxis);
					var xRatio = xCurrentMax/xAxisMax;
					var yRatio = yCurrentMax/yAxisMax;
					if((xRatio < .05 || xRatio > 1) && config.elastic === 'yes'){
						scatterPlotChart.elasticX(true)
				    	.x(d3.scaleLinear().domain([ 1,xCurrentMax  ])).xAxisPadding(xCurrentMax / 10);
				        xAxisMax = xCurrentMax;
					} else {
						scatterPlotChart.elasticX(false);
				    }
					
					if((yRatio < .1 || yRatio > 1) && config.elastic === 'yes'){
						yAxisMax = yCurrentMax;
						scatterPlotChart.elasticY(true)
				    	.y(d3.scaleLinear().domain([ 1, yCurrentMax ])).yAxisPadding(yCurrentMax / 10);
					} else {
						scatterPlotChart.elasticY(false);
				    }
				  });
				/*
				 * scatterPlotChart.legend(dc.legend().x(config.legendxposition).horizontal(config.legendhorizontal).autoItemWidth(true).y(config.legendyposition).itemHeight(13)
				 * .gap(5).legendText(function (d) { var legendName; if
				 * (config.measurelabel && config.measurelabel[d.name]){
				 * legendName = config.measurelabel[d.name]; }else{ legendName =
				 * d.name; } if(config.colorcodes && config.colorcodes[d.name])
				 * $(this).prev().attr("fill",config.colorcodes[d.name]); return
				 * d.name; }));
				 */
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					scatterPlotChart.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					scatterPlotChart.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				scatterPlotChart.on('filtered', function() {
					resetAllCharts(THIS, config.html);
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				$(".reset", $(config.html)).on('click', function(e) {
					scatterPlotChart.filterAll();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					resetAllCharts(THIS);
				}.bind(this));
				THIS.drawnGraph.push(scatterPlotChart);
				scatterPlotChart.render();
				applyDefaultFilter(config,scatterPlotChart);
				d3.selectAll(config.html).selectAll(".symbol").call(tip);
				d3.selectAll(config.html).selectAll(".symbol").on('mouseover', tip.show);
			} catch (e) {
				console.log('Error Creating ScatterPlot Chart' + e);
			}
		};
		renderScatterPlot(config, dim1,dimDate);
	};
	Graphing.prototype.drawSeries = function(config) {
		var THIS = this;
		var dim1;
		var dimDate = null;
		var measures = [];
		var labels = [];
		var tickValue =[];
		labels.push(config.field);
		$(config.html).data('resetedValue',false);
		$(config.html).data('chartReseted',false);
		if (config.xaxistype === "date"){
			$(config.html).data("xaxistype",config.xaxistype);
			dim1 = this.ndx.dimension(function(d) {
				return d[config.field]+"-"+ d[config.radius]+"-"+d[config.xAxis];
			});
			dimDate = this.ndx.dimension(function(d) {
				return d["date-"+config.xAxis];
			});
			labels.push("date-"+config.xAxis);
		}else{
			dim1 = this.ndx.dimension(function(d) {
				tickValue.push(d[config.xAxis]);
				return [d[config.field],d[config.xAxis]];
			});
			labels.push(config.xAxis);
		}
		if(config.colorfield){
			labels.push(config.colorfield);
		}
		if (measures.indexOf(config.yAxis)===-1)
			measures.push(config.yAxis);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.radiusPrefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.radiusSuffix
			});
		
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderSeries(config, dim1);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderSeries(config, dim1);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}		
		var renderSeries = function(config, dim,dimDate) {
			var numberFormat = d3.format(config.numFormat);
			try {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var seriesChart = dc.seriesChart(config.html);
				
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(measures,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(measures,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(measures,labels,config.filter,config.filtervalue));
				
				 if(config.runningtotal ==='yes'){
					 grp = accumulate_group(grp,config);	
					}
				grp = remove_empty(grp,config);
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()* (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.radius, config.aggregate,config.filterfrom === "bottom"?0:1));
					seriesChart.data(function(group) {
						return group.top(defaultVal);
					});
					
					
				}				
				var seriesChartWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					seriesChartWidth = parseInt($(config.html).width());
				} else {
					seriesChartWidth = config.chartwidth;
				}
				var seriesChartHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					seriesChartHeight = parseInt($(config.html).width());
					seriesChartHeight = seriesChartHeight > THIS.defaultHeight ? THIS.defaultHeight
							: seriesChartHeight;
				} else {
					seriesChartHeight = config.chartheight;
				}
				
				
				var tip = d3.tip().offset([-10, 0]).attr('class', 'd3-tip').html(function(e) {
					var d = e.data;
					var xAxisValue;
					var yAxisValue;
					var groupValueXaxis;
					if (config.xaxistype === "date"){
						groupValueXaxis = d.value["date-"+config.xAxis];
					}else{
						groupValueXaxis = isNaN(d.value[config.xAxis]) ? 0 : d.value[config.xAxis];
					}
					var groupValueYaxis = isNaN(d.value[config.yAxis]) ? 0 : d.value[config.yAxis];
					var groupCount = isNaN(d.value.count)? 0 : d.value.count;
					if (config.xAxisAggre.toLowerCase() === "sum"){
						xAxisValue = groupValueXaxis;
					}else if (config.xAxisAggre.toLowerCase() === "count"){
						xAxisValue = groupCount;
					}else{
						xAxisValue = groupValueXaxis / groupCount;
					}
					if (config.yAxisAggre.toLowerCase() === "sum"){
						yAxisValue = groupValueYaxis;
						
					}else if (config.yAxisAggre.toLowerCase() === "count"){
						yAxisValue = groupCount;
					}else{
						yAxisValue = groupValueYaxis / groupCount;
					}
					var key = d.value[config.field];
					if(config.tooltipkey === "code"){
						key = d.value[config.field];
					}else if(config.tooltipkey === "description"){
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = THIS.masterData[config.fieldname][key];
						}
					}else{
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = d.value[config.field]+"-" + THIS.masterData[config.fieldname][key];
						}
					}
					var xKeyValue = config.xAxisPrefix + xAxisFormat(xAxisValue) + config.xAxisSuffix;
					var yKeyValue = config.yAxisPrefix + numberFormat(yAxisValue)+ config.yAxisSuffix;
					  var toolTip= $("<div/>");
					   toolTip.append($('<table/>').attr({"class":"d3-tip-table"})
							   .append($('<tr/>').append($('<th/>').attr({"class":"d3-tip-header"}).html( config.measurelabel[key]?config.measurelabel[key]:key)))
							   .append($('<tr/>').append($('<td/>').html( config.xAxisLabel?config.xAxisLabel:config.xAxis).attr({"class":"d3-tip-key"})).append($('<td/>').html(xKeyValue ).attr({"class":"d3-tip-value"})))
							   .append($('<tr/>').append($('<td/>').html( config.yAxisLabel?config.yAxisLabel:config.yAxis).attr({"class":"d3-tip-key"})).append($('<td/>').html(yKeyValue ).attr({"class":"d3-tip-value"})))
							   );
					if (config.linkaction){
						var actionDiv = $("<div/>");
						actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
						var actions = config.linkaction.split(",");
						for (var i = 0;i<actions.length;i++){
							actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
						}
						toolTip.append(actionDiv);
					}
					/* changing code here */
					var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
					$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+key+'")');
					var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
					$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ key +'",true)');
					var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
					divTag.append(anchorTag).append(excludeAnchor);
					toolTip.append(divTag);
					return toolTip[0].outerHTML;
					
					/*
					 * return "<table><tr><th class='d3-tip-header'>"+ key + "</th></tr>" + "<tr><td class='d3-tip-key'>" + + "</td><td class='d3-tip-value'>" + "</td></tr><tr><td class='d3-tip-key'>" +
					 * config.yAxisLabel + "</td><td class='d3-tip-value'>" + + "</tr><table/>";
					 */
				});
				
				seriesChart
						.width(seriesChartWidth)
						.height(seriesChartHeight)
						
						.margins({
							top : config.margintop,
							left : config.marginleft,
							right : config.marginright,
							bottom : config.marginbottom
						})
						.dimension(dim)
						.group(grp)
						.clipPadding(10)
						.colorAccessor(function(d){
						  return d.key;
						})
						.colors(d3.scale.category10())
						.seriesAccessor(function(d) {
							return d.value[config.field];}
						)
						.brushOn(config.brushOn)
						.chart(function(c) { return dc.lineChart(c).interpolate(config.interpolate); })
						.elasticY(true)
						.elasticX(true).xAxisLabel(config.xAxisLabel)
						.yAxisLabel(config.yAxisLabel)
						.renderHorizontalGridLines(true)
						.renderVerticalGridLines(true)
						.mouseZoomable(false)
						.title(function () { return ""; });
				seriesChart.yAxis().ticks(10).tickFormat(
						function(d) {
							var numFormat;
							if (config.yAxisNumberFormat){
								numFormat= d3.format(config.yAxisNumberFormat)(d);
							}else{
								var prefix = d3.formatPrefix(d);
								numFormat= prefix(d) + prefix.symbol;
							}
							return config.yAxisPrefix + numFormat
							 + config.yAxisSuffix;
						});
				var grpYAxis = THIS.getValueByAgrregate(config.yAxisAggre,
						config.yAxis,config.filtervalue,config.absolute);
				
				var xaxisformate;
				var xAxisMax;
				var grpXAxis;
				var xAxisUnit;
				var xAxisFormat;
				var xAxisField;
				if (config.xaxistype === "date"){
					grpXAxis = function(d){
						return d.value["date-"+config.xAxis];
					};
					var xAxisMin = dimDate.bottom(1)[0]["date-"+config.xAxis];
					xAxisMax = dimDate.top(1)[0]["date-"+config.xAxis];
					xaxisformate = d3.time.scale().domain([xAxisMin, xAxisMax]).nice(d3.time.day);
					xAxisUnit = d3.time.day;
					xAxisFormat = d3.timeFormat(config.xaxistooltipdateformat);
					xAxisField = "date-"+config.xAxis;
					seriesChart.x(xaxisformate)
					
				}else if(config.xaxistype === "number"){
					 grpXAxis = THIS.getValueByAgrregate(config.xAxisAggre,
								config.xAxis,config.filtervalue,config.absolute);
					xAxisMax = d3.max(grp.all(), grpXAxis);
					xaxisformate =  d3.scaleLinear();
					xAxisUnit = dc.units.ordinal();
					xAxisFormat = config.xaxisformate? d3.format(config.xaxisformate):d3.format(config.numFormat);
					xAxisField = config.xAxis;
					seriesChart.x(xaxisformate)
					
				}else{
					 grpXAxis = THIS.getValueByAgrregate(config.xAxisAggre,
								config.xAxis,config.filtervalue,config.absolute);
					xAxisMax = d3.max(grp.all(), grpXAxis);
					xAxisFormat = config.xaxisformate? d3.format(config.xaxisformate):d3.format(config.numFormat);
					xAxisField = config.xAxis;
					seriesChart.x(d3.scaleOrdinal()).xUnits(dc.units.ordinal);
					
				}

				seriesChart.keyAccessor(grpXAxis)
				seriesChart.valueAccessor(grpYAxis)
				if (config.legendorder != ''){
					var legends = config.legendorder.split(",");
					dc.override(seriesChart, 'legendables', function() {
				          var items = seriesChart._legendables();
				          return items.sort(function(a, b) { 
				        	var aIndex = legends.indexOf(a.name);
				        	var bIndex = legends.indexOf(b.name);
				        	aIndex = aIndex===-1?10000:aIndex;
				        	bIndex = bIndex===-1?10000:bIndex;
				        	  return aIndex-bIndex ;
				          });
				      });
					seriesChart.legendables();
				}
				if (config.xaxistype !== "date"){
					if(config.xaxistype === "number"){
						var unique = tickValue.filter(function(elem, index, self) {
						    return index == self.indexOf(elem);
						})
						seriesChart.xAxis().tickValues(unique).tickFormat(
								function(d) {
									var prefix = d3.formatPrefix(d);
									if(d%1 === 0){
										return config.xAxisPrefix + prefix(d)
										+ prefix.symbol + config.xAxisSuffix;
									}else{
										return config.xAxisPrefix + d + config.xAxisSuffix;
									}
								});
					}else{
						seriesChart.xAxisPadding(xAxisMax/ 10).xAxis().tickFormat(
							function(d) {
								var prefix = d3.formatPrefix(d);
								if(d%1 === 0){
									return config.xAxisPrefix + prefix(d)
									+ prefix.symbol + config.xAxisSuffix;
								}else{
									return config.xAxisPrefix + d + config.xAxisSuffix;
								}
							});
					}
				}
			
				seriesChart.legend(dc.legend().x(config.legendxposition).horizontal(true).autoItemWidth(true).y(config.legendyposition).itemHeight(13)
						.gap(5).legendText(function (d) {
							var legendName;
							if (config.measurelabel && config.measurelabel[d.name]){
								legendName = config.measurelabel[d.name];
							}else{
								legendName = d.name;
							}
							if(config.colorcodes && config.colorcodes[d.name])
								$(this).prev().attr("fill",config.colorcodes[d.name]);
					        return legendName;
					    }));	
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					seriesChart.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					seriesChart.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				
				seriesChart.on('filtered', function() {
					resetAllCharts(THIS, config.html);
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true); 
					seriesChart.filterAll();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					resetAllCharts(THIS);
					$(config.html).data('chartReseted',false);
				}.bind(this));
				
				seriesChart.on("postRedraw", function(chart){
					d3.selectAll(config.html).selectAll(".dot").call(tip);
					d3.selectAll(config.html).selectAll(".dot").on('mouseover', tip.show);
				});
				THIS.drawnGraph.push(seriesChart);
				seriesChart.render();
				applyDefaultFilter(config,seriesChart);
				
				d3.selectAll(config.html).selectAll(".dot").call(tip);
				d3.selectAll(config.html).selectAll(".dot").on('mouseover', tip.show);
			} catch (e) {
				console.log('Error Creating Series Chart' + e);
			}
		};
		renderSeries(config, dim1,dimDate);
	};
	Graphing.prototype.drawHeatMap = function(config) {
		var THIS = this;
		var dim1;
		var dimDate;
		var measures = [];
		var labels = [];
		var dimension = [];
		labels.push(config.field);
		$(config.html).data('resetedValue',false);
		if(config.colorfield){
			labels.push(config.colorfield);
		}
		if (config.xaxistype === "date"){
			$(config.html).data("xaxistype",config.xaxistype);
			dim1 = this.ndx.dimension(function(d) {
				return [d[config.field],d[config.xAxis]];
			});
			labels.push("date-"+config.xAxis);
			dimDate = this.ndx.dimension(function(d) {
				return d["date-"+config.xAxis];
			});
		}else if(config.xaxistype === "string"){
			dim1 = this.ndx.dimension(function(d) {
				return [d[config.field]];
			});
			dimension.push(config.xAxis);
		}
		else{
			dim1 = this.ndx.dimension(function(d) {
				return [d[config.field]];
			});
			measures.push(config.xAxis);
		}if(config.yaxistype === "string"){
			dimension.push(config.yAxis);
		}else{
			measures.push(config.yAxis);
		}		
		measures.push(config.radius);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.radiusPrefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.radiusSuffix
			});
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderHeatMap(config, dim1,dimDate);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderHeatMap(config, dim1,dimDate);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}		
	
		var renderHeatMap = function(config, dim,dimDate) {
			var numberFormat = d3.format(config.numFormat);
			try {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var heatMapChart = dc.heatMap(config.html);
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(measures,labels,config.filter,config.filtervalue,dimension),
						THIS.reduceFieldsRemove(measures,labels,config.filter,config.filtervalue,dimension),
						THIS.reduceFieldsInitial(measures,labels,config.filter,config.filtervalue,dimension));
				var defaultVal = grp.size();
				if (config.filtertype) {
					defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()* (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.radius, config.aggregate,config.filterfrom === "bottom"?0:1));
					heatMapChart.data(function(group) {
						return group.top(defaultVal);
					});
				}
				if (config.orderby !== 0) {
					if(config.orderby === "2" || config.orderby === "3"){
						config.maxKeyLength = 0;
						grp.all().map(function(o){
							if(config.maxKeyLength < o.key.length){
								config.maxKeyLength = o.key.length;
							}
						});						
					}
					heatMapChart.ordering(function(a) {
						return THIS.sortbyGroup(config, a);
					});
				}
				if(config.containerheight){
					$(config.html).css({"max-height":config.containerheight+"px"});
				}
				var heatMapChartWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					heatMapChartWidth = parseInt($(config.html).width());
				} else {
					heatMapChartWidth = config.chartwidth;
				}
				var heatMapChartHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					heatMapChartHeight = parseInt($(config.html).width());
					heatMapChartHeight = heatMapChartHeight > THIS.defaultHeight ? THIS.defaultHeight
							: heatMapChartHeight;
				} else {
					heatMapChartHeight = config.chartheight;
					
				}
				var grpYAxis;
				var grpXAxis;
				var xAxisFormat;
				var xAxisField;
				if (config.xaxistype === "date"){
					grpXAxis = function(d){
						return d3.timeFormat(config.xaxisformate)(d.value["date-"+config.xAxis]);
					};
					xAxisFormat = d3.timeFormat(config.xaxisdateformat);
					xAxisField = "date-"+config.xAxis;
				}else if(config.xaxistype === "string"){
					var xfield = config.xAxis;
					grpXAxis = function(d){
						return Object.keys(d.value[xfield]).join();
					};
					xAxisField = config.xAxis;
					xAxisFormat = config.xaxisformate? d3.format(config.xaxisformate):d3.format(config.numFormat);
				}
				else{
					grpXAxis = THIS.getValueByAgrregate(config.xAxisAggre,config.xAxis,config.filtervalue,config.absolute);
					xAxisFormat = config.xaxisformate? d3.format(config.xaxisformate):d3.format(config.numFormat);
					xAxisField = config.xAxis;
				}
				if(config.yaxistype === "string"){
					var yfield = config.yAxis;
					grpYAxis = function(d){
						return Object.keys(d.value[yfield]).join();
					};
				}else{
					grpYAxis = THIS.getValueByAgrregate(config.yAxisAggre,config.yAxis,config.filtervalue,config.absolute);
				}
				var radiusMin = dim.top(1)[0][config.radius];
				var radiusMax = 0;
				var yaxisObj = {};
				var xaxisObj = {};
				var count = 0;
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
					var rad;
					var xAxisValue;
					var yAxisValue;
					var groupValueXaxis;
					var groupValueRadius = isNaN(d.value[config.radius]) ? 0 : d.value[config.radius];
					if(config.xaxistype === "string" && d.value[config.xAxis]){
						groupValueXaxis = d.value[config.xAxis];			
					} else if(config.xaxistype === "date" && d.value[["date-"+config.xAxis]]){
						groupValueXaxis = d.value["date-"+config.xAxis];
					} else{
						groupValueXaxis = isNaN(d.value[config.xAxis]) ? 0 : d.value[config.xAxis];
					}
					var groupValueYaxis;
					if(config.yaxistype === "string" && d.value[config.yAxis]){
						groupValueYaxis = Object.keys(d.value[config.yAxis])[0];		
					} else if(config.yaxistype === "date" && d.value["date-"+config.yAxis]){
						groupValueYaxis = d.value["date-"+config.yAxis];
					} else{
						groupValueYaxis = isNaN(d.value[config.yAxis]) ? 0 : d.value[config.yAxis];
					}
					var groupCount = isNaN(d.value.count)? 0 : d.value.count;
					if (config.radiusAggre.toLowerCase() === "sum"){
						rad = groupValueRadius;				
					}else if (config.radiusAggre.toLowerCase() === "count"){
						rad = groupCount;
					}else{
						rad = groupValueRadius / groupCount;
					}
					if (config.xAxisAggre.toLowerCase() === "sum"){
						xAxisValue = groupValueXaxis;
					}else if (config.xAxisAggre.toLowerCase() === "count"){
						xAxisValue = groupCount;
					}else{
						xAxisValue = groupValueXaxis / groupCount;
					}
					if (config.yAxisAggre.toLowerCase() === "sum"){
						yAxisValue = groupValueYaxis;
						
					}else if (config.yAxisAggre.toLowerCase() === "count"){
						yAxisValue = groupCount;
					}else{
						yAxisValue = groupValueYaxis / groupCount;
					}
					if (config.yaxistype === "number")
						yAxisValue = numberFormat(yAxisValue);
					var key = d.value[config.field];
					if(config.tooltipkey === "code"){
						key = d.value[config.field];
					}else if(config.tooltipkey === "description"){
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = THIS.masterData[config.fieldname][key];
						}
					}else{
						if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
							key = d.value[config.field]+"-" + THIS.masterData[config.fieldname][key];
						}
					}					
				    var tooltip= $("<div/>");
					tooltip.append($('<table/>').append($('<tr/>').append($('<th/>').html(key).attr({"class":"d3-tip-header"})))
					.append($('<tr/>').append($('<td/>').html(config.yAxisLabel).attr({"class":"d3-tip-key"})).append($('<td/>').html(config.yAxisPrefix+ yAxisValue+ config.yAxisSuffix).attr({"class":"d3-tip-value"})))
					.append($('<tr/>').append($('<td/>').html(config.xAxisLabel).attr({"class":"d3-tip-key"})).append($('<td/>').html(config.xAxisPrefix+ xAxisFormat(xAxisValue)+ config.xAxisSuffix).attr({"class":"d3-tip-value"})))
					.append($('<tr/>').append($('<td/>').html(config.radiusLabel).attr({"class":"d3-tip-key"})).append($('<td/>').html(config.radiusPrefix+ numberFormat(rad)+config.radiusSuffix).attr({"class":"d3-tip-value"})))
					);
					if (config.linkaction){
						var actionDiv = $("<div/>");
						actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
						var actions = config.linkaction.split(",");
						for (var i = 0;i<actions.length;i++){
							actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
						}
						tooltip.append(actionDiv);
					}
					var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action","id":"keep-only"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
					$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+key+'")');
					var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action","id":"exclude"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
					$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ key +'",true)');
					var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
					divTag.append(anchorTag).append(excludeAnchor);
					tooltip.append(divTag);
					return tooltip[0].outerHTML;
				});
				grp.all().map(function(d){
					if(count <= defaultVal){
						count++;
						if(config.yaxistype === "string" && d.value[config.yAxis]){
							for(var key in d.value[config.yAxis]){
								if(!yaxisObj[key]){
									yaxisObj[key] = 0;
								}
								yaxisObj[key]++;
							}
						}else if(d.value[config.yAxis]){
							if(!yaxisObj[d.value[config.yAxis]]){
								yaxisObj[d.value[config.yAxis]] = 0;
							}
							yaxisObj[d.value[config.yAxis]]++;
						}
						if(d.value["date-"+config.xAxis]){
							var key = d3.timeFormat(config.xaxisformate)(d.value["date-"+config.xAxis]);
							if(!xaxisObj[key]){
								xaxisObj[key] = 0;
							}
							xaxisObj[key]++;
						}else if(config.xaxistype === "string" && d.value[config.xAxis]){
							for(var key in d.value[config.xAxis]){
								if(!xaxisObj[key]){
									xaxisObj[key] = 0;
								}
								xaxisObj[key]++;
							}
							d3.keys(d.value[xAxisField]).join()
						}
						else if(d.value[config.xAxis]){
							if(!xaxisObj[d.value[config.xAxis]]){
								xaxisObj[d.value[config.xAxis]] = 0;
							}
							xaxisObj[d.value[config.xAxis]]++;
						}
					}
					
					if(radiusMax < d.value[config.radius] ){
						radiusMax = d.value[config.radius];
					}
					if(radiusMin > d.value[config.radius]){
						radiusMin = d.value[config.radius];
					}
				});
				if(d3.keys(yaxisObj).length > 0){
					heatMapChartHeight = d3.keys(yaxisObj).length * 40;
				}
				heatMapChart
						.width(heatMapChartWidth)
						.height(heatMapChartHeight)
						.margins({
							top : config.margintop,
							left : config.marginleft,
							right : config.marginright,
							bottom : config.marginbottom
						})
						.dimension(dim)
						.group(grp)
						.colors(function(d) {
							return d3.scaleLinear().domain([radiusMin, radiusMax]).range([config.color1, config.color2])(d);
						})
						.keyAccessor(grpXAxis)
						.valueAccessor(grpYAxis)
						.colorAccessor(function(d) {
							return d.value[config.radius];
						})
						.on('renderlet', function (chart) {
                        chart.selectAll("g.cols.axis text")
                            .attr("transform", function () {
                            	var rotate = 0;
                            	if($(this).parents("svg").parent().attr('class').indexOf("axis-rotate")>-1){
                            		rotate = $(this).parents("svg").parent().attr('class').match(/[0-9]+/g)[0];
                            		rotate = rotate*-1;
                            	}
                                var coord = this.getBBox();
                                var x = coord.x + (coord.width),
                                y = coord.y + (coord.height);
                                return "rotate("+rotate+" " + x + " " + y + ")";
                            });
						})
						.title(function () { return ""; });
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					heatMapChart.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					heatMapChart.colorAccessor(function(d) { 
					 	return keyColorCodes[d.key];
				    });
				}
				heatMapChart.on('filtered', function() {
					resetAllCharts(THIS, config.html);
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					heatMapChart.filterAll();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					resetAllCharts(THIS);
				}.bind(this));
				THIS.drawnGraph.push(heatMapChart);
				heatMapChart.render();
				applyDefaultFilter(config,heatMapChart);
				d3.selectAll(config.html).selectAll(".heat-box").call(tip);
				d3.selectAll(config.html).selectAll(".heat-box").on('mouseover', tip.show);
				d3.selectAll(config.html).on('mouseenter',function(){$(".d3-tip").css("opacity",0);});
			} catch (e) {
				console.log('Error Creating heat map Chart ' + e);
			}
		};
		renderHeatMap(config, dim1,dimDate);
	};
	Graphing.prototype.drawPie = function(config) {
		var THIS = this;
		var label;
		var fParams = config.field.split(',');
		$(config.html).data('resetedValue',false);
		$(config.html).data('chartReseted',false);
		if (config.field.indexOf(',') > 0) {
			var temp = '';
			for ( var i = 0; i < fParams.length; i++) {
				var dash = i < fParams.length - 1 ? '+ \' - \' +' : '';
				temp += ('d.' + fParams[i].trim() + dash);
			}
			label = temp;
		} else {
			label = "d['" + config.field + "']|| 'Others'";
		}
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.prefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.suffix
			});
		var dimFun = new Function('d', 'return ' + label + ';');
		if (viewSettings && viewSettings.length > 0){
			$.each(viewSettings,function(index,view){
				if (view.anchorName ===  config.field && view.dynamicDimension && view.dynamicDimension !== config.field){
					dimFun = new Function('d', "return d['" + view.dynamicDimension	+ "'] || 'Others' ;");
					config.selectedDim = view.dynamicDimension;
					$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[view.dynamicDimension])?config.measurelabel[view.dynamicDimension]: view.dynamicDimension);
					return;
				}
			});
		}
		var dim1 = this.ndx.dimension(dimFun);
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find("select[name='filterselect']").unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						var dimension = dim1;
						if($(config.html).find("[name='dimensionselect']").length>0){
							var dimValue = $(config.html).find("[name='dimensionselect']").val();
							var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
							dimension = THIS.ndx.dimension(dimFunction);
						}
						renderPie(config, dimension);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(config.html).find("[name='dimensionselect']").length>0){
					var dimValue = $(config.html).find("[name='dimensionselect']").val();
					var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
					dim1 = THIS.ndx.dimension(dimFunction);
				}
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderPie(config, dim1);
			});
		}else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		var renderPie = function(config, dim) {
			try {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var pie = dc.pieChart(config.html);
				var fields = [],labels = [];
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				var calculatedField = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				fields.push(config.group);
				fields = $.merge(fields, calculatedField);
				
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size() * (config.filterdefaultval / 100));
					}
					grp.all().sort(THIS.sortGroup(config.group, config.aggregate,config.filterfrom === "bottom"?0:1));
					pie.data(function(group) {
						return group.top(defaultVal);
					});
				}
				var pieHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					pieHeight = parseInt($(config.html).width());
					pieHeight = pieHeight > THIS.defaultHeight ? THIS.defaultHeight
							: pieHeight;
				} else {
					$(config.html).css({"min-height":config.chartheight+"px"});
					pieHeight = parseInt(config.chartheight);
					
				}
				var pieWidth; 
				if (config.chartwidth.toLowerCase() === "auto") {
				pieWidth = parseInt($(config.html).width());
				pieWidth = pieWidth > THIS.defaultHeight ? THIS.defaultHeight
				: pieWidth;
				} else {
				pieWidth = config.chartwidth;
				}
				
				var numFormat = config.numFormat? d3.format(config.numFormat) : ",~f";
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
				}
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(e){
					return THIS.getTitleAgrregate(e.data,config);
				});
				var pieRadius = pieHeight / 2.5;
				pie.width(pieWidth).height(pieHeight).radius(pieRadius)
						.dimension(dim).group(grp).valueAccessor(grpAggre)
						.title(function () { return ""; }).drawPaths(true);
				if (config.orderby !== 0) {
					if(config.orderby === "2" || config.orderby === "3"){
						config.maxKeyLength = 0;
						grp.all().map(function(o){
							if(config.maxKeyLength < o.key.length){
								config.maxKeyLength = o.key.length;
							}
						});						
					}
					pie.ordering(function(a) {
						return THIS.sortbyGroup(config, a);
					});
				}
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					pie.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					pie.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				if(config.showlegend=="yes")
					pie.legend(dc.legend().x(parseInt(config.legendxposition)).y(parseInt(config.legendyposition)).gap(5).horizontal(config.legendhorizontal))
				if(config.drawpaths == "yes"){
					pie.drawPaths(true);
					if(config.externallabels){
						var labels=parseInt(config.externallabels);
						pie.externalLabels(labels);
					}
					
					if(config.externalradiuspadding){
						pie.externalRadiusPadding(parseInt(config.externalradiuspadding))
					}
					
					if(config.minangleforlabel){
						pie.minAngleForLabel(parseInt(config.minangleforlabel))
					}
					
					// Hot fix to avoid overlapping of text in pie and donut
					
					pie.on('renderlet', function(chart) {
						chart.selectAll('text.pie-slice').text(function(d, i) {
					          $(this).bind('mouseover', function(){
					    		  var lineClass = $(this)[0].classList[2];
					    		  $($(this).parent().parent().find("."+lineClass)[2]).css("stroke",'#e6550d')
					    		  
					    	  })
					    	  $(this).bind('mouseout', function(){
					    		  var lineClass = $(this)[0].classList[2];
					    		  $($(this).parent().parent().find("."+lineClass)[2]).css("stroke",'black')
					    		  
					    	  })
					          return $(this).text()
					      })
					  });
					
				}
				if (config.showtotal === "yes") {
					pie.on('pretransition', function(chart) {
				        chart.selectAll('text.pie-slice').text(function(data) {
				        	var d = data.data;
				        	var groupValue = isNaN(d.value[config.group]) ? 0 : d.value[config.group];
							var groupCount = isNaN(d.value.count)? 0 : d.value.count;
							if (groupValue !== 0 && groupCount !== 0){
								if (config.aggregate.toLowerCase() === "sum")
									keyValue = config.absolute === "Yes"?(config.prefix + numFormat(Math.abs(groupValue)) + config.suffix):(config.prefix+ numFormat(groupValue) + config.suffix);
								else if (config.aggregate.toLowerCase() === "count")
									keyValue = config.prefix
											+ numFormat(groupCount) + config.suffix;
								else{
									keyValue = config.absolute === "Yes"?(config.prefix+ numFormat(Math.abs(groupValue) / groupCount)+ config.suffix):(config.prefix+ numFormat(groupValue / groupCount)+ config.suffix);
								}
								return d.key + ':' + dc.utils.printSingleValue(keyValue) ;
							} 
				            // return d.data.key + ' (' +
							// dc.utils.printSingleValue((d.endAngle -
							// d.startAngle) / (2*Math.PI) * 100) + '%)';
				        })
				    });
				}
				pie.on('filtered', function(chart) {
					resetAllCharts(THIS);
					var filters = chart.filters();
					if(filters.length>0){
						$(".reset", $(config.html)).show();
					}else{
						$(".reset", $(config.html)).hide();
					}
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true);
					pie.filterAll();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					resetAllCharts(THIS);
					$(config.html).data('chartReseted',false);
				}.bind(this));

				THIS.drawnGraph.push(pie);
				pie.render();
				if(config.dynamicdimension){
					$(config.html).find("[name='dimensionselect']").remove();
					var select = THIS.constructDimentionSelect(config);
					if(config.selectedDim){
						select.val(config.selectedDim);
						delete config.selectedDim;
					}
					select.unbind('change').bind('change',
							function() {
								var dimValue = $(this).val();
								if(config.dynamicdimension.indexOf(dimValue)>-1){
									$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[dimValue])?config.measurelabel[dimValue]: dimValue);
									$(config.html).data('fieldname',dimValue);
								}else{
									$(config.html).find(".axisLabel").html(config.displayname || config.fieldname);
									$(config.html).data('fieldname',config.fieldname);
								}
								var dimFun = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
								var dimention = THIS.ndx.dimension(dimFun);
								renderPie(config, dimention);
					});
				}
				applyDefaultFilter(config,pie);
				d3.selectAll(config.html).selectAll(".pie-slice").call(tip);
				d3.selectAll(config.html).selectAll(".pie-slice").on('mouseover', tip.show);
				d3.selectAll(config.html).on('mouseenter',function(){$(".d3-tip").css("opacity",0);});
			} catch (e) {
				console.log('Error Creating Pie Chart' + e);
			}
		};
		renderPie(config, dim1);
	};

	Graphing.prototype.drawDonut = function(config) {
		var THIS = this;
		var label;
		var fParams = config.field.split(',');
		$(config.html).data('resetedValue',false);
		if (config.field.indexOf(',') > 0) {
			var temp = '';
			for ( var i = 0; i < fParams.length; i++) {
				var dash = i < fParams.length - 1 ? '+ \' - \' +' : '';
				temp += ('d.' + fParams[i].trim() + dash);
			}
			label = temp;
		} else {
			label = "d['" + config.field + "']|| 'Others'";
		}		
		var dimFun = new Function('d', 'return ' + label + ';');
		$(".reset", $(config.html)).data('isReset',true);
		$(config.html).data('chartReseted',false);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.prefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.suffix
			});
		if (viewSettings && viewSettings.length > 0){
			$.each(viewSettings,function(index,view){
				if (view.anchorName ===  config.field && view.dynamicDimension && view.dynamicDimension !== config.field){
					dimFun = new Function('d', "return d['" + view.dynamicDimension	+ "'] || 'Others' ;");
					config.selectedDim = view.dynamicDimension;
					$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[view.dynamicDimension])?config.measurelabel[view.dynamicDimension]: view.dynamicDimension);
					return;
				}
			});
		}
		var dim1 = this.ndx.dimension(dimFun);
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find("select[name='filterselect']").unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						var dimension = dim1;
						if($(config.html).find("[name='dimensionselect']").length>0){
							var dimValue = $(config.html).find("[name='dimensionselect']").val();
							var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
							dimension = THIS.ndx.dimension(dimFunction);
						}
						renderDonut(config, dimension);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(config.html).find("[name='dimensionselect']").length>0){
					var dimValue = $(config.html).find("[name='dimensionselect']").val();
					var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
					dim1 = THIS.ndx.dimension(dimFunction);
				}
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderDonut(config, dim1);
			});
		}else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		var renderDonut = function(config, dim) {
			try {
				var fields = [];
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var pie = dc.pieChart(config.html);
				var calculatedField = [],labels = [];
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				fields.push(config.group);
				fields = $.merge(fields, calculatedField);
				
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));				
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size()
								* (config.filterdefaultval / 100));
						per = " %";
					}
					grp.all().sort(THIS.sortGroup(config.group, config.aggregate,config.filterfrom === "bottom"?0:1));
					pie.data(function(group) {
						return group.top(defaultVal);
					});
				}
				var numFormat = config.numFormat? d3.format(config.numFormat) : ",~f";

				// var grp = dim.group().reduceSum(grpFun);
				var pieHeight = "";
				if (config.chartheight.toLowerCase() === "auto") {
					pieHeight = parseInt($(config.html).width());
					pieHeight = pieHeight > THIS.defaultHeight ? THIS.defaultHeight
							: pieHeight;
				} else {
					
					pieHeight = config.chartheight;
				}
				var pieWidth; 
				if (config.chartwidth.toLowerCase() === "auto") {
				pieWidth = parseInt($(config.html).width());
				pieWidth = pieWidth > THIS.defaultHeight ? THIS.defaultHeight
				: pieWidth;
				} else {
				pieWidth = config.chartwidth;
				}
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
				}
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(e){
					return THIS.getTitleAgrregate(e.data,config);
				});
				
				var pieRadius = pieHeight / 2.5;
				pie.width(pieHeight).height(pieHeight).radius(pieRadius)
						.innerRadius(pieRadius / 2).dimension(dim).group(grp)
						.valueAccessor(grpAggre).title(function () { return ""; });
				pie.width(pieWidth).height(pieHeight).radius(pieRadius)
				.dimension(dim).group(grp).valueAccessor(grpAggre)
				.title(function () { return ""; }).drawPaths(true);
				if (config.orderby !== 0) {
					if(config.orderby === "2" || config.orderby === "3"){
						config.maxKeyLength = 0;
						grp.all().map(function(o){
							if(config.maxKeyLength < o.key.length){
								config.maxKeyLength = o.key.length;
							}
						});						
					}
					pie.ordering(function(a) {
						return THIS.sortbyGroup(config, a);
					});
				}
				if (config.colorcodes) {
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					pie.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					pie.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				if(config.innerradius){
				pie.innerRadius(parseInt(config.innerradius));
				}
				if(config.externalradiuspadding){
					pie.externalRadiusPadding(parseInt(config.externalradiuspadding))
					
				}
				if (config.showtotal === "yes") {
					pie.on('pretransition', function(chart) {
				        chart.selectAll('text.pie-slice').text(function(data) {
				        	var d = data.data;
				        	var groupValue = isNaN(d.value[config.group]) ? 0 : d.value[config.group];
							var groupCount = isNaN(d.value.count)? 0 : d.value.count;
							if (groupValue !== 0 && groupCount !== 0){
								if (config.aggregate.toLowerCase() === "sum")
									keyValue = config.absolute === "Yes"?(config.prefix + numFormat(Math.abs(groupValue)) + config.suffix):(config.prefix+ numFormat(groupValue) + config.suffix);
								else if (config.aggregate.toLowerCase() === "count")
									keyValue = config.prefix
											+ numFormat(groupCount) + config.suffix;
								else{
									keyValue = config.absolute === "Yes"?(config.prefix+ numFormat(Math.abs(groupValue) / groupCount)+ config.suffix):(config.prefix+ numFormat(groupValue / groupCount)+ config.suffix);
								}
								return d.key + ':' + dc.utils.printSingleValue(keyValue) ;
							} 
				            // return d.data.key + ' (' +
							// dc.utils.printSingleValue((d.endAngle -
							// d.startAngle) / (2*Math.PI) * 100) + '%)';
				        })
				    });
				}
				
				pie.on('filtered', function(chart) {					
					resetAllCharts(THIS);
					$(".reset", $(config.html)).data('isReset',true);
					var filters = chart.filters();
					var groupCount = chart.group().all().length;
					if(filters.length>0 && groupCount > filters.length){
						$(".reset", $(config.html)).show();
					}else{
						$(".reset", $(config.html)).hide();
					}
					/*
					 * if(config.defaultFilter ===
					 * undefined){//config.defaultFiltervalue.length !==
					 * filters.length resetAllProfiles(chart); }
					 */
				}.bind(this));
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true);
					pie.filterAll();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					$(config.html).data('chartReseted',false);
					resetAllProfiles(pie);
				}.bind(this));
				THIS.drawnGraph.push(pie);

				pie.render();
				if(config.dynamicdimension){
					$(config.html).find("[name='dimensionselect']").remove();
					var select = THIS.constructDimentionSelect(config);
					if(config.selectedDim){
						select.val(config.selectedDim);
						delete config.selectedDim;
					}
					select.unbind('change').bind('change',
							function() {
								var dimValue = $(this).val();
								if(config.dynamicdimension.indexOf(dimValue)>-1){
									$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[dimValue])?config.measurelabel[dimValue]: dimValue);
									$(config.html).data('fieldname',dimValue);
								}else{
									$(config.html).find(".axisLabel").html(config.displayname || config.fieldname);
									$(config.html).data('fieldname',config.fieldname);
								}
								var dimFun = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
								var dimention = THIS.ndx.dimension(dimFun);
								renderDonut(config, dimention);
					});
				}
				applyDefaultFilter(config,pie);
				pie.selectAll('g.pie-slice').on('click', function(chart){
					resetAllProfiles(pie);
				}.bind(this));
				d3.selectAll(config.html).selectAll(".pie-slice").call(tip);
				d3.selectAll(config.html).selectAll(".pie-slice").on('mouseover', tip.show);
				d3.selectAll(config.html).on('mouseenter',function(){$(".d3-tip").css("opacity",0);});
			} catch (e) {
				console.log('Error Creating Pie Chart' + e);
			}
		};
		renderDonut(config, dim1);
	};

	Graphing.prototype.resetGraphs = function() {
		if (this.drawnGraph) {
			for ( var i = 0, idx = this.drawnGraph.length; i < idx; i++) {
				this.drawnGraph[i].filterAll();
				this.drawnGraph[i].render();
			}
		}
	};
		
	Graphing.prototype.drawRow = function(config) { 
		var THIS = this;
		var dimFun = new Function('d', "return d['" + config.field	+ "'] || 'Others' ;");
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.prefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.suffix,
			"data-labelSuffix":config.labelSuffix,
			"data-prefixScale":config.prefixScale
			});
		$(config.html).data('resetedValue',false);
		$(config.html).data('chartReseted',false);
		if (viewSettings && viewSettings.length > 0){
			$.each(viewSettings,function(index,view){
				var viewanchor =view.anchorName.split('container');
				if (viewanchor[0] ===  config.field && view.dynamicDimension && view.dynamicDimension !== config.field){
					dimFun = new Function('d', "return d['" + view.dynamicDimension	+ "'] || 'Others' ;");
					config.selectedDim = view.dynamicDimension;
					$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[view.dynamicDimension])?config.measurelabel[view.dynamicDimension]: view.dynamicDimension);
					return;
				}
			});
		}
		var dim1 = this.ndx.dimension(dimFun);
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find("select[name='filterselect']").unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						var dimension = dim1;
						if($(config.html).find("[name='dimensionselect']").length>0){
							var dimValue = $(config.html).find("[name='dimensionselect']").val();
							var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
							dimension = THIS.ndx.dimension(dimFunction);
						}
						renderRow(config, dimension);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(config.html).find("[name='dimensionselect']").length>0){
					var dimValue = $(config.html).find("[name='dimensionselect']").val();
					var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
					dim1 = THIS.ndx.dimension(dimFunction);
				}
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderRow(config, dim1);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		var renderRow = function(config, dim) {
			try {
				function remove_bins(source_group, limit) {
					return {
						all : function() {
							return source_group.top(limit);
						}
					};
				}
				
				function remove_empty_bins(source_group, item) {
					
					return {
						all : function() {
							return source_group.all().filter(function(d){
								return d.key != item
							});
						}
					};
				}
				function remove_bins_zero(source_group, limit,fields) { 
					return {
						all : function(d) {
							if(limit){
								return source_group.top(limit);
							}else{
								return source_group.all().filter(function(d) {
								var isNonZero = false;
								if(fields && fields.length>0){
									for(var i=0;i<fields.length;i++){
										if(d.value[fields[i]] != 0){
											isNonZero = true;
										}										
									}										
								}
								return isNonZero;
								});								
							}
						}
					};
				}
				
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						var chartanchor =dc.chartRegistry.list()[chartIndex].anchor().split('container');
						if(chartanchor[0] === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var tip = d3.tip().offset([-5, 0]).attr('class', 'd3-tip').html(function(e){ 
					return THIS.getTitleAgrregate(e,config);
				});
				var bar = dc.rowChart("#"+$(config.html).find("[name='maxheight']").attr('id'));
				var defaultVal;
				var fields = [];;
				var calculatedField = [],labels = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				if(config.colorfield){
					labels.push(config.colorfield);
				}				
				fields.push(config.group);
				fields = $.merge(fields, calculatedField);
				fields =  $.unique(fields);
				var grp1 = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
				var grp;
				if (config.filtertype) {
					defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp1.size() * (config.filterdefaultval / 100));
					}
					grp1.all().sort(THIS.sortGroup(config.group, config.aggregate,config.filterfrom === "bottom"?0:1));
					/*
					 * bar.data(function(group) { return group.top(defaultVal);
					 * });
					 */
					if(config.excludeitems){ 
						var excludedItem = []
						var grpDefault
						grpDefault = grp1
						excludedItem = config.excludeitems.split(',')
						excludedItem.forEach(function(item){
							grp1 = remove_empty_bins(grpDefault, item)
							grpDefault = grp1
							;})
							
					}
					grp1 = grpDefault
					grp = remove_bins(grp1, defaultVal);
				}else{
					if(config.excludeitems){
						var excludedItem = []
						var grpDefault
						grpDefault = grp1
						excludedItem = config.excludeitems.split(',')
						excludedItem.forEach(function(item){
							grp = remove_empty_bins(grpDefault, item)
							grpDefault = grp
							;})
						grp = grpDefault
						defaultVal = grp.all().length;
					}
					else{
						grp = grp1;
						defaultVal = grp.size();
					}
				}
				
				var numberOfCharts =$(config.html).parent().find('[name="charts"]').length >1 ? $(config.html).parent().find('[name="charts"]').length * 10:30;
				var barWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					barWidth = parseInt($(config.html).width()) - numberOfCharts ;
				} else {
					barWidth = config.chartwidth;
				}
				// config.filtervalue = config.filtervalue.split(";");
				var barHeight;
				var containerheight;
				if (config.chartheight.toLowerCase() === "auto") {
					barHeight = parseInt($(config.html).width());
					barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
							: barHeight;
				} else {
					$(config.html).css({"min-height":config.chartheight+"px"});
					barHeight = config.chartheight;
				}
				var chartHeight;
				if(config.barwidth && config.barwidth === "auto"){
					chartHeight  =  barHeight;
				}else{					
					chartHeight  = config.margintop+config.marginbottom+(defaultVal*config.barwidth);
					$(config.html).css({"max-height":($(config.html).parent().parent().height())+"px"});
					containerheight = $(config.html).parent().parent().height() - (numberOfCharts+30);
				}
				if(chartHeight < barHeight)
					chartHeight  =  barHeight;
				if(config.containerheight){
					$(config.html).css({"max-height":config.containerheight+"px"});
					containerheight = config.containerheight - 50;
				}
				if(config.containerheight==="0"){
					$(config.html).find("[name='maxheight']").css({"max-height":(containerheight)+"px","width":"100%","position": "absolute","min-height":"130px"});
				}
				else
				$(config.html).find("[name='maxheight']").css({"max-height":(containerheight)+"px","width":"100%","margin-left": "25px","min-height":"130px"});
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
			}
			;
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);
				// var titleAggre = THIS.getTitleAgrregate(config);
				bar.width(barWidth).height(chartHeight).margins({
					top : config.margintop,
					left : config.marginleft,
					right : config.marginright,
					bottom : config.marginbottom
				}).group(remove_bins_zero(grp,null,[config.group])).dimension(dim).valueAccessor(grpAggre).title(function () { return ""; })
				.elasticX(true);
				
				if (config.yaxisinteger){
					bar.xAxis().ticks(4).tickFormat(function(d) {
					    return d % 1 ? null : d3.format(",~s")(d);
					});
				}else{
					bar.xAxis().ticks(4).tickFormat(d3.format("d"));// ~s
					}
				// bar;
				// .tickFormat(d3.format("~s"));
				// .tickFormat(function(v) {return "";}).ticks(0)
				bar.label(function(d){ 
					var tempValue,returnValue;
					var labelSuffix = config.labelSuffix?config.labelSuffix:""
					if (config.aggregate.toLowerCase() === "sum"){
						tempValue = d.value[config.group];
					}
					else if (config.aggregate.toLowerCase() === "count")
						tempValue = d.value.count;
					else if (config.aggregate.toLowerCase() === "calculated"){	
						tempValue = eval(config.calculatedformula);
				        tempValue = (tempValue === Infinity) ? 0:tempValue;
					}
					else
						tempValue = d.value[config.group]/ d.value.count;
					if(config.prefixScale === "false"){
						var numFormat = d3.format(config.numFormat);
						tempValue = isNaN(tempValue) ? 0 : numFormat(tempValue);
						returnValue = d.key +" : "+tempValue+config.suffix;		 
					}else{
						var prefix;
						var prefixScale="";
						if(!isNaN(tempValue)){
							prefix = d3.formatPrefix(Math.round(tempValue),1);
							prefixScale = Math.round(prefix(tempValue));
						}
						returnValue = d.key +" : "+ prefixScale + labelSuffix;
					}
					
					if(config.showtotal === "yes")
							return returnValue
					else
						return d.key;
		        });
				if (config.orderby !== 0) {
					if(config.orderby === "2" || config.orderby === "3"){
						config.maxKeyLength = 0;
						grp.all().map(function(o){
							if(config.maxKeyLength < o.key.length){
								config.maxKeyLength = o.key.length;
							}
						});						
					}
					bar.ordering(function(a) {
						return THIS.sortbyGroup(config, a);
					});
				}
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true);
					bar.filterAll();
					if($(e.currentTarget).data("isReset")){
						resetAllProfiles(bar);
					}
					$(e.currentTarget).removeData("isReset").hide();
					$(config.html).data('chartReseted',false);
					
				}.bind(this));
				if (config.colorcodes) {
					
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					bar.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					bar.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				
				bar.on('filtered', function(chart) {
					if (config.yaxisinteger){
						chart.xAxis().tickFormat(function(d) {
						    return d % 1 ? null : d3.format(",~s")(d);
						});
					}else{
						chart.xAxis().tickFormat(d3.format("d"));// ~s
						}
					$(".reset", $(config.html)).data('isReset',true);
					var filters = chart.filters();
					if(filters.length>0){
						$(".reset", $(config.html)).show();
						$(config.html).find("[class='reset']").css({"display":"contents"});
					}else{
						$(".reset", $(config.html)).hide();
					}
					if(config.showselectedtext && config.showselectedtext === 'yes'){
						var selectedFilter = arguments[0].filters();
						$(config.html).find("[name='showselectedtext']").empty();
						if(selectedFilter && selectedFilter.length>0){
							for(var i=0;i<selectedFilter.length;i++){
								var color = chartColorCodes[selectedFilter[i]];
								var textColor = invertColor(color,true);
								color = color+" !important";
								$(config.html).find("[name='showselectedtext']")
								.append($("<span/>").css({"color":textColor,"background-color":color}).addClass("badge margin-3")
										.append($("<strong/>").html(selectedFilter[i]))
										.append($("<a/>").data("key",selectedFilter[i]).append($("<i/>").css({"font-size":"small"}).addClass("fa fa-times pull-right")).addClass("close ")));
							}
							$(config.html).find("[name='showselectedtext']").find("a").unbind("click").bind("click",function(){
								var key = $(this).data("key");
								bar.filter(key);
								bar.redrawGroup();
								resetAllProfiles(bar);
							});
						}
					}
					if(!config.isRedraw){
						$(config.html).data('resetedValue',true);
						resetAllCharts(THIS,config.html);
					};
					/*
					 * if(config.defaultFilter ===
					 * undefined){//config.defaultFiltervalue.length !==
					 * filters.length resetAllProfiles(arguments[0]); }
					 */
				}.bind(this));
				
				THIS.drawnGraph.push(bar);
				bar.render();
				var chartColorCodes ={};
				if(config.showselectedtext && config.showselectedtext === 'yes'){
					$(config.html).find("[name='showselectedtext']").remove();
					$(config.html).prepend($("<div/>").css({"max-height":"100px","overflow":"auto"}).addClass("text-left").attr({"name":"showselectedtext"}));
					bar.selectAll("g.row rect").attr("fill", function(d){
						chartColorCodes[d.key] = $(this).attr("fill");
						return $(this).attr("fill");
				    });
				}
				if(config.hidexaxis && config.hidexaxis == 'yes'){
					$(config.html).find("svg").find(".axis").hide();
				}
				if(config.dynamicdimension){
					$(config.html).find("[name='dimensionselect']").remove();
					var select = THIS.constructDimentionSelect(config);
					if(config.selectedDim){
						select.val(config.selectedDim);
						delete config.selectedDim;
					}	
					select.unbind('change').bind('change',
							function() {
								var dimValue = $(this).val();
								if(config.dynamicdimension.indexOf(dimValue)>-1){
									$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[dimValue])?config.measurelabel[dimValue]: dimValue);
									$(config.html).data('fieldname',dimValue);
								}else{
									$(config.html).find(".axisLabel").html(config.displayname || config.fieldname);
									$(config.html).data('fieldname',config.fieldname);
								}
								var dimFun = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
								var dimention = THIS.ndx.dimension(dimFun);
								renderRow(config, dimention);
					});
				}
				applyDefaultFilter(config,bar);

				config.isRedraw = false;
				bar.selectAll('g.row').on('click', function(chart){
					resetAllProfiles(bar);
				}.bind(this));
				d3.selectAll(config.html).selectAll("rect").call(tip);
				d3.selectAll(config.html).selectAll("rect").on('mouseover', tip.show);
				d3.selectAll(config.html).on('mouseenter',function(){$(".d3-tip").css("opacity",0);});
				bar.on('renderlet.add-tip', function(chart) {
				    chart.selectAll('rect')
				        .call(tip)
				        .on('mouseover', tip.show);
				       
				});
				return bar;
				
			} catch (e) {
				console.log('Error Drawing Horizontal Bar ' + e);
			}
		};
		var rowChart = renderRow(config, dim1);
		if (config.filtertype) {
			THIS.topNBarCharts.push({"config":config,"dim":dim1,"chart":rowChart,"type":"row"});
		}
		
		THIS.renderTopNRowCharts = function(currentConfigHtml){
			THIS.renderTopNCharts(currentConfigHtml,renderRow,"row");
		};
	};

	Graphing.prototype.drawGroupedBarLine1 = function(config){
		var THIS = this;
		var dimFun = new Function('d', "return d['" + config.field + "'] || 'Others' ;");
		var dim1 = this.ndx.dimension(dimFun);
		$(".reset", $(config.html)).data('isReset',true);
		$(config.html).data('chartReseted',false);
		
		var barWidth;
		if (config.chartwidth.toLowerCase() === "auto") {
			barWidth = parseInt($(config.html).width());
// barWidth = "auto";
		} else {
			barWidth = config.chartwidth;
		}
		var barHeight;
		if (config.chartheight.toLowerCase() === "auto") {
			barHeight = parseInt($(config.html).width());
			barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
					: barHeight;
		} else {
			barHeight = config.chartheight;
		}
		config.legendxposition = barWidth * config.legendxposition;
		config.legendyposition = barHeight * config.legendyposition;
		this.groupedBarLineCharts.push({
			"config" : config,
			"dim" : dim1
		});
		var renderGroupedBarLine = function(config, dim) {
			try{
				$(config.html).find("svg").remove();
				$(config.html).data("fieldname",config.fieldname);
				$(config.html).data("filtervalue",[]);				
				var fields = [];
				var stacks = [];
				if (config.charts && config.charts !== null
						&& config.charts.length > 0) {
					for ( var x = 0; x < config.charts.length; x++) {
						if(config.charts[x].type === 'stack'){
							stacks = config.charts[x].group.split(",");
							THIS.stackMeasures= stacks;
						}
						else if (fields.indexOf(config.charts[x]["group"])){
							fields.push(config.charts[x]["group"]);
						}
					}
				};
				var calculatedField = [],labels = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				
				if(stacks.length >0){
					
					fields = $.merge(fields, stacks,calculatedField);
				}else{
					fields = $.merge(fields, calculatedField);
				}
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));				
				var grpAll = grp.all(),Data = [];
				for(var i=0;i<grpAll.length;i++){
					var barCategories = [];
					var lineCategories = [];
					var isNonZero = false;
					for(var j=0;j<config.charts.length;j++){
						if(grpAll[i].value[config.charts[j].group] && grpAll[i].value[config.charts[j].group] !== "0"){
							isNonZero = true;
						}
						if(config.charts[j].type === "bar"){
							barCategories.push({
								Name:config.charts[j].name || config.charts[j].group,
								Value:grpAll[i].value[config.charts[j].group]?parseInt(grpAll[i].value[config.charts[j].group]):0
							});
						}else{
							lineCategories.push({
								Name:config.charts[j].name || config.charts[j].group,
								Value:grpAll[i].value[config.charts[j].group]?parseInt(grpAll[i].value[config.charts[j].group]):0
							});
						}
					}
					if(isNonZero){
						Data.push({
							Date : grpAll[i].key,
							Categories:barCategories,
							LineCategory:lineCategories
						});
					}
				}
				
				if(config.chartwidth.toLowerCase() === "auto"){
					if(grpAll.length * 37> barWidth)
						barWidth = grpAll.length * 37;

				}

			        var width = barWidth - config.marginleft - config.marginright,
			            height = barHeight - config.margintop - config.marginbottom;

			        var textWidthHolder = 0;
			        Data.forEach(function (d) {
			            d.LineCategory.forEach(function (b) {
			                b.Date = d.Date;
			            });
			        });

			        var Categories = new Array();
			        var ageNames;
			        var x0 = d3.scaleOrdinal()
			            .range([0, barWidth], .1);
			        var XLine = d3.scaleOrdinal()
			            .range([0, barWidth], .1);
			        var x1 = d3.scaleOrdinal();

			        var y = d3.scaleLinear()
			            .range([height, 0]);

			        var YLine = d3.scaleLinear().range([height, 0])
			        .domain([0, d3.max(Data, function (d) { return d3.max(d.LineCategory, function (b) { return b.Value }) })]);

// var color = d3.scaleOrdinal()
// .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c",
// "#ff8c00"]);
			        var color = new Map();
			        config.charts.forEach(function(x){color.set(x.name,x.color)});
			        
			        var line = d3.svg.line().x(function (d) {
			            return x0(d.Date) + x0.rangeBand() / 2;
			        }).y(function (d) { return YLine(d.Value) });
			        var xAxis = d3.axisBottom(x0);
			            // .orient("bottom");

			        var yAxis = d3.axisLeft(y)
			            // .orient("left")
			            .tickFormat(d3.format(".2s"));

			        var YLeftAxis = d3.axisRight(YLine).tickFormat(d3.format(".2s"));

			        var svg = d3.select(config.html).append("svg").attr("class","groupedbarline")
			            .attr("width", width + config.marginleft + config.marginright)
			            .attr("height", height + config.margintop + config.marginbottom)
			          .append("g")
			            .attr("transform", "translate(" + config.marginleft + "," + config.margintop + ")");
			        // Bar Data categories
			        Data.forEach(function (d) {
			            d.Categories.forEach(function (b) {
			                if (Categories.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			                    b.Type = "bar";
			                    Categories.push(b)
			                }
			            });
			        });


			        // Line Data categories
			        Data.forEach(function (d) {
			            d.LineCategory.forEach(function (b) {
			                if (Categories.findIndex(function (c) { return c.Name === b.Name }) == -1) {
			                    b.Type = "line";
			                    Categories.push(b)
			                }
			            })
			        });

			        // Processing Line data
			        lineData = DataSegregator(Data.map(function (d) { return d.LineCategory }), "Name");

			        x0.domain(Data.map(function (d) { return d.Date; }));
			        XLine.domain(Data.map(function (d) { return d.Date; }));
			        x1.domain(Categories.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).range([0, x0.rangeBand()]);
			        y.domain([0, d3.max(Data, function (d) { return d3.max(d.Categories, function (d) { return d.Value; }); })]);

			        svg.append("g")
			            .attr("class", "x axis")
			            .attr("transform", "translate(0," + height + ")")
			            .call(xAxis);

			      /*
					 * svg.append("g") .attr("class", "y axis")
					 * .attr("transform", "translate(" + (width) + ",0)")
					 * .call(YLeftAxis)
					 * 
					 * .append("text") .attr("transform", "rotate(-90)")
					 * .attr("y", -10)
					 * 
					 * .attr("dy", ".71em") .style("text-anchor", "end")
					 * .text("Percent");
					 */

			        svg.append("g")
			            .attr("class", "y axis")
			            .call(yAxis)
			          .append("text")
			            .attr("transform", "rotate(-90)")
			            .attr("y", 6)
			            .attr("dy", ".71em")
			            .style("text-anchor", "end")
			            .text("");


			        var state = svg.selectAll(".state")
			            .data(Data)
			          .enter().append("g")
			            .attr("class", "state")
			            .on("click",function(d) {
			            	var isSelected = $(this).data("click");
			            	var filtervalue = $(config.html).data("filtervalue");
			            	if(!filtervalue){
			            		filtervalue = [];
			            	}
			            	if(!isSelected){
			            		$(this).data("click",true);
			            		$(this).css("opacity","0.2");
				            	filtervalue.push(d.Date);
			            	}else{
			            		$(this).css("opacity","1");
			            		$(this).data("click",false);
			            		filtervalue.splice(filtervalue.indexOf(d.Date), 1);
			            	}
			            	$(config.html).find('a').data('isReset',false).show();
			            	dim.filter(function(value) {
			            	});
			            	 $(config.html).data("filtervalue",filtervalue);
			            	// $(this).data("",filterValues);
			            	resetAllCharts(THIS, config.html);
			            })
			            .attr("transform", function (d) { return "translate(" + x0(d.Date) + ",0)"; });

			        state.selectAll("rect")
			            .data(function (d) { return d.Categories; })
			          .enter().append("rect")
			            .attr("width", x1.rangeBand())
			            .attr("x", function (d) { 
			            	return x1(d.Name); 
			            	})
			            .attr("y", function (d) {
			            	return y(d.Value);
			            	})
			            // .attr("height", function (d) { return height -
						// y(d.Value); })
			            .style("fill", function (d) {
			            	return color.get(d.Name) || d3.scale.category10()(); 
			            	})
			            .transition().delay(500).attrTween("height", function (d) {
			                var i = d3.interpolate(0, height - y(d.Value));
			                return function (t)
			                {
			                    return i(t);
			                }
			            });

			        // drawaing lines
			        svg.selectAll(".lines").data(lineData).enter().append("g").attr("class", "line")
			        .each(function (d) {
			            Name=d[0].Name
			            d3.select(this).append("path").attr("d", function (b) { return line(b) }).style({ "stroke-width": "2px", "fill": "none" }).style("stroke", color.get(Name) || d3.scale.category10()()).transition().duration(1500);
			        })


			        // Legends
			        var LegendHolder = svg.append("g").attr("class", "legendHolder");
			        var legend = LegendHolder.selectAll(".legend")
			            .data(Categories.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			          .enter().append("g")
			            .attr("class", "legend")
			            .attr("transform", function (d, i) { return "translate(0,0)";})// +(
																						// height+
																						// config.marginbottom/2
																						// )+
																						// ")";
																						// })
			            .each(function (d,i) {
			                // Legend Symbols


			                d3.select(this).append("rect")
			                .attr("width", function () { return 18 })
			                .attr("x", function (b) {

			                    left = (i+1) * 15 + i * 18 + i * 5 + textWidthHolder +i*35;
			                    return left;
			                })
			                 .attr("y", function (b) { return b.Type == 'bar'?0:7})
			                .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			                .style("fill", function (b) { return b.Type == 'bar' ? color.get(d.Name) || d3.scale.category10()() : color.get(d.Name) || d3.scale.category10()() });

			                // Legend Text

			                d3.select(this).append("text")
			                .attr("x", function (b) {

			                    left = (i+1) * 15 + (i+1) * 18 + (i + 1) * 5 + textWidthHolder +i*35;

			                    return left;
			                })
			                .attr("y", 9)
			                .attr("dy", ".35em")
			                .style("text-anchor", "start")
			                .text(d.Name);

			                textWidthHolder += getTextWidth(d.Name, "10px", "calibri");
			            });


			        // Legend Placing
			        $(config.html).css("overflow","auto");
			        d3.select(".legendHolder")
		            .attr("width","700")
			        .attr("transform", function (d) {
			            thisWidth = d3.select(this).node().getBBox().width;
			            return "translate(0,-50)";
			        });
			        $(".reset", $(config.html)).on('click', dim, function() {
						$(this).hide();
						$(this).data('isReset',true);
						dim.filterAll();
						resetAllCharts(THIS);
					});
			}catch(ex){
				console.log(ex);
			}
		}
		function getTextWidth(text, fontSize, fontName) {
            c = document.createElement("canvas");
            ctx = c.getContext("2d");
            ctx.font = fontSize + ' ' + fontName;
            return ctx.measureText(text).width;
        }

        function DataSegregator(array, on) {
            var SegData;
            OrdinalPositionHolder = {
                valueOf: function () {
                    thisObject = this;
                    keys = Object.keys(thisObject);
                    keys.splice(keys.indexOf("valueOf"), 1);
                    keys.splice(keys.indexOf("keys"), 1);
                    return keys.length == 0 ? -1 : d3.max(keys, function (d) { return thisObject[d] })
                }
                , keys: function () {
                    keys = Object.keys(thisObject);
                    keys.splice(keys.indexOf("valueOf"), 1);
                    keys.splice(keys.indexOf("keys"), 1);
                    return keys;
                }
            }
            array[0].map(function (d) { return d[on] }).forEach(function (b) {
                value = OrdinalPositionHolder.valueOf();
                OrdinalPositionHolder[b] = OrdinalPositionHolder > -1 ? ++value : 0;
            });

            SegData = OrdinalPositionHolder.keys().map(function () {
                return [];
            });

            array.forEach(function (d) {
                d.forEach(function (b) {
                    SegData[OrdinalPositionHolder[b[on]]].push(b);
                });
            });

            return SegData;
        }
        this.refreshGroupedBarLine = function(currentConfigHtml) {
			for ( var x = 0; x < this.groupedBarLineCharts.length; x++) {
				if (!currentConfigHtml || this.groupedBarLineCharts[x]["config"].html !== currentConfigHtml) {
					renderGroupedBarLine(this.groupedBarLineCharts[x]["config"],this.groupedBarLineCharts[x]["dim"]);
				}
			}
		};
		renderGroupedBarLine(config,dim1);
	};
	
	Graphing.prototype.drawGroupedBarLine =function(config){
		var THIS = this;
		var dimFun = new Function('d', "return d['" + config.field + "'] || 'Others' ;");
		var dim1 = this.ndx.dimension(dimFun);
		$(".reset", $(config.html)).data('isReset',true);
		$(config.html).data('chartReseted',false);
		
		var barWidth;
		if (config.chartwidth.toLowerCase() === "auto") {
			barWidth = parseInt($(config.html).width());
		} else {
			barWidth = config.chartwidth;
		}
		var barHeight;
		if (config.chartheight.toLowerCase() === "auto") {
			barHeight = parseInt($(config.html).width());
			barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
					: barHeight;
		} else {
			
			barHeight = config.chartheight;
		}
		config.legendxposition = barWidth * config.legendxposition;
		config.legendyposition = barHeight * config.legendyposition;
		this.groupedBarLineCharts.push({
			"config" : config,
			"dim" : dim1
		});
		var renderGroupedBarLine = function(config, dim) {
			try{
				$(config.html).find("svg").remove();
				$(config.html).data("fieldname",config.fieldname);
				$(config.html).data("filtervalue",[]);				
				var fields = [];
				var stacks = [];
				var columnsCategory = {
					"bar" : [],
					"stack" : [],
					"line":[]
				};
				if (config.charts && config.charts !== null
						&& config.charts.length > 0) {
					for ( var x = 0; x < config.charts.length; x++) {
						columnsCategory[config.charts[x].type].push(config.charts[x].group); 
						if(config.charts[x].type === 'stack'){
							stacks.push(config.charts[x].group);
							THIS.stackMeasures= stacks;
						}
						else if (fields.indexOf(config.charts[x]["group"])){
							fields.push(config.charts[x]["group"]);
						}
					}
				};
				var calculatedField = [],labels = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				
				if(stacks.length >0){					
					fields = $.merge(fields, stacks,calculatedField);
				}else{
					fields = $.merge(fields, calculatedField);
				}
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));				
				var grpAll = grp.all();
				var data = [];
				for(var i=0;i<grpAll.length;i++){
					var isNonZero = false;
					for(var j=0;j<config.charts.length;j++){
						if(grpAll[i].value[config.charts[j].group] && grpAll[i].value[config.charts[j].group] !== "0"){
							isNonZero = true;
						}
					}
					if(isNonZero){
						data.push(grpAll[i]);
					}
				}
				var defaultVal = data.length;
				var chartwidth;
				if(config.barwidth){
					chartwidth  = config.marginleft+config.marginright+(defaultVal*config.barwidth);
				}else{
					chartwidth = barWidth;
				}
				if(chartwidth < barWidth){
					chartwidth = barWidth;
				}
				// *******************Start************************
				var x0 = d3.scaleOrdinal()
			    .range([0, chartwidth], 0.4);
			 
				var x1 = d3.scaleOrdinal();
				 
				var y = d3.scaleLinear()
				    .range([barHeight, 0]);
				 
				var xAxis = d3.axisBottom(x0);
				    // .orient("bottom");
				 
				var yAxis = d3.axisLeft(y)
				    // .orient("left")
				    .tickFormat(d3.format(".2s"));
				 
				var color = d3.scaleOrdinal()
				    .range(config.charts.map(function(o){return o.color}));
				 
				var svg = d3.select(config.html).append("svg")
				    .attr("width", chartwidth + config.marginleft + config.marginright)
				    .attr("height", barHeight + config.margintop + config.marginbottom)
				  .append("g")
				    .attr("transform", "translate(" + config.marginleft + "," + config.margintop + ")");
				var yBegin;
				var lineData = [];				
				var columnHeaders = config.charts.map(function(o){return o.group});
				  color.domain(columnHeaders);
				  data.forEach(function(d) {
					  lineData.push({
		    			  key:d.key,
		    			  Name:columnsCategory.line[0],
		    			  Value:d.value[columnsCategory.line[0]]
		    		  });
					var lines = [];
				    var yColumn = new Array();
				    d.columnDetails = columnHeaders.map(function(name) {
				      for (chartType in columnsCategory) {
				    	  if(chartType === "line"){
				    		  //  
				    	  }else{
				    		  if($.inArray(name, columnsCategory[chartType]) >= 0){
						          if (!yColumn[chartType]){
						            yColumn[chartType] = 0;
						          }
						          yBegin = yColumn[chartType];
						          yColumn[chartType] += +d.value[name];
						          return {
						        	  name: name,
						        	  column: chartType,
						        	  yBegin: yBegin,
						        	  yEnd: (d.value[name] && yBegin !== undefined)?(+d.value[name] + yBegin):0,
						        	  key:d.key
						        	};
						        }
				    	  }
				        
				      }
				      
				    });
				    d.total = d3.max(d.columnDetails, function(d) {
						if(d){
							return d.yEnd;
						}
				    });
				  });
				  lineData = [lineData];
				  x0.domain(data.map(function(d) { return d.key; }));
				  x1.domain(d3.keys(columnsCategory)).range([0, x0.rangeBand()]);
				 
				  y.domain([0, d3.max(data, function(d) { 
				    return d.total; 
				  })]);
				 
				  svg.append("g")
				      .attr("class", "x axis")
				      .attr("transform", "translate(0," + barHeight + ")")
				      .call(xAxis);
				 
				  svg.append("g")
				      .attr("class", "y axis")
				      .call(yAxis)
				    .append("text")
				      .attr("transform", "rotate(-90)")
				      .attr("y", 6)
				      .attr("dy", ".7em")
				      .style("text-anchor", "end")
				      .text("");
				 
				  var stackedbar = svg.selectAll(".project_stackedbar")
				      .data(data)
				    .enter().append("g")
				      .attr("class", "bar")
				      .on("click",function(d) {
			            	var isSelected = $(this).data("click");
			            	var filtervalue = $(config.html).data("filtervalue");
			            	if(!filtervalue){
			            		filtervalue = [];
			            	}
			            	if(filtervalue.length === 0){
			            		$(config.html).find('.bar').css("opacity","0.2");
			            	}
			            	if(!isSelected){
			            		$(this).data("click",true);
			            		$(this).css("opacity","1");
				            	filtervalue.push(d.key);
			            	}else{
			            		$(this).css("opacity","0.2");
			            		$(this).data("click",false);
			            		filtervalue.splice(filtervalue.indexOf(d.key), 1);
			            	}
			            	$(config.html).find('a').data('isReset',false).show();
			            	dim.filter(function(value) {
			            		return d.key === value;
			            	});
			            	 $(config.html).data("filtervalue",filtervalue);
			            	// $(this).data("",filterValues);
			            	resetAllCharts(THIS, config.html);
			            })
				      .attr("transform", function(d) { return "translate(" + x0(d.key) + ",0)"; });
				 
				  stackedbar.selectAll("rect")
				      .data(function(d) { return d.columnDetails; })
				    .enter().append("rect")
				      .attr("width", x1.rangeBand())
				      .attr("class", "stack")
				      .attr("x", function(d) { 
							if(d){
								return x1(d.column);
							}
				         })
				      .attr("y", function(d) {
						if(d){
							return y(d.yEnd); 
						}
				      })
				      .attr("height", function(d) { 
						if(d){
							return y(d.yBegin) - y(d.yEnd); 
						}
				      })
				      .style("fill", function(d) {if(d){ return color(d.name)}; });
				  var XLine = d3.scaleOrdinal()
		            .rangeRoundPoints([0, chartwidth], .1);
			        XLine.domain(data.map(function (d) { return d.key; }));
				  var YLine = d3.scaleLinear().range([barHeight, 0])
			        .domain([0, d3.max(data, function (d) { 
			        	return d.value[columnsCategory.line[0]]
			        	})]);
				  var line = d3.svg.line().x(function (d) {
			            return x0(d.key) + x0.rangeBand() / 2;
			        }).y(function (d) { 
			        	return YLine(d.Value); 
			        	});
				  if(lineData && lineData.length>0){
					  svg.selectAll(".lines").data(lineData).enter().append("g").attr("class", "line")
				        .each(function (d) {
				            if(d && d.length>0){
					            Name=d[0].Name;
				            }
				            d3.select(this).append("path")
				            .attr("d", function (b) {
				            	return line(b); 
				            	}).style({ "stroke-width": "2px", "fill": "none" })
				            .style("stroke", function(b){
				            	var color = "#ff0000";
				            	if(b && b.length>0){
				            		lineConfig = config.charts.filter(function(obj){return obj.group == b[0].Name;});
				            		if(lineConfig && lineConfig.length>0){
				            			color = lineConfig[0].color;
				            		}
					            }
				            	return color;
				            	}).transition().duration(1500);
				        });
				  }
				  var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
					  var keyValue = d.name +": "+ d.yEnd
					  var toolTip= $("<div/>");
					  var table = $('<table/>').attr({"class":"d3-tip-table"}).append($('<tr/>').append($('<td/>').html( d.key +':').attr({"class":"d3-tip-key"}))
							   .append($('<td/>').html("").attr({"class":"font-bold"})));
					   
					   if(config.charts && config.charts.length>0){
						   for(var j=0;j<config.charts.length;j++){
								if(d.value[config.charts[j].group] !== undefined){
									table.append($("<tr/>")
											.append($("<td/>").html(config.charts[j].name || config.charts[j].group))
											.append($("<td/>").html(d.value[config.charts[j].group])));
								}
							}
					   }
						if (config.linkaction){
							var actionDiv = $("<div/>");
							actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
							var actions = config.linkaction.split(",");
							for (var i = 0;i<actions.length;i++){
								actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
							}
							toolTip.append(actionDiv);
						}
						toolTip.append(table);
						/* changing code here */
						var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
						$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+d.key+'")');
						var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
						$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ d.key +'",true)');
						var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
						divTag.append(anchorTag).append(excludeAnchor);
						toolTip.append(divTag);
						return toolTip[0].outerHTML;
					});
				  var textWidthHolder = 0;
			        var LegendHolder = svg.append("g").attr("class", "legendHolder");
			        var legend = LegendHolder.selectAll(".legend")
			            .data(config.charts)
			          .enter().append("g")
			            .attr("class", "legend")
			            .attr("transform", function (d, i) { 
			            	return "translate("+i*35+"," +( 25 )+ ")";
			            	})
			            .each(function (d,i) {

			                d3.select(this).append("rect")
			                .attr("width", function () { return 18 })
			                .attr("x", function (b) {

			                    left = (i+1) * 15 + i * 18 + i * 5 + textWidthHolder;
			                    return left;
			                })
			                 .attr("y", function (b) { return b.Type == 'bar'?0:7})
			                .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			                .style("fill", function (b) { 
			                	return b.color;// color(d.group);
			                });

			                // Legend Text

			                d3.select(this).append("text")
			                .attr("x", function (b) {

			                    left = (i+1) * 15 + (i+1) * 18 + (i + 1) * 5 + textWidthHolder;

			                    return left;
			                })
			                .attr("y", 9)
			                .attr("dy", ".35em")
			                .style("text-anchor", "start")
			                .text(d.name || d.group);

			                textWidthHolder += getTextWidth(d.name, "10px", "calibri");
			            });
			        d3.select(".legendHolder").attr("transform", function (d) {
			            thisWidth = d3.select(this).node().getBBox().width;
			            // return "translate(" + ((config.barwidth) / 2 -
						// thisWidth / 2) + ",-50)";
			            return "translate(0,-50)";
			        });  
				d3.selectAll(config.html).selectAll(".bar").call(tip);
				d3.selectAll(config.html).selectAll(".bar").on('mouseover', tip.show);
				$(config.html).css("overflow","auto");
				// *******************End*****************
			        $(".reset", $(config.html)).on('click', dim, function() {
						$(this).hide();
						$(this).data('isReset',true);
						dim.filterAll();
						resetAllCharts(THIS);
					});
			}catch(ex){
				console.log(ex);
			}
		}
		function getTextWidth(text, fontSize, fontName) {
            c = document.createElement("canvas");
            ctx = c.getContext("2d");
            ctx.font = fontSize + ' ' + fontName;
            return ctx.measureText(text).width;
        }
        this.refreshGroupedBarLine = function(currentConfigHtml) {
			for ( var x = 0; x < this.groupedBarLineCharts.length; x++) {
				if (!currentConfigHtml || this.groupedBarLineCharts[x]["config"].html !== currentConfigHtml) {
					renderGroupedBarLine(this.groupedBarLineCharts[x]["config"],this.groupedBarLineCharts[x]["dim"]);
				}
			}
		};
		renderGroupedBarLine(config,dim1);
	
	}
	Graphing.prototype.drawGroupedRow = function(config){
		var THIS = this;
		var dimFun = new Function('d', "return d['" + config.field	+ "'] || 'Others' ;");
		var dim = this.ndx.dimension(dimFun);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.prefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.suffix
			});
		// $(config.html).data('resetedValue',false);
		this.groupedRowCharts.push({
			"config" : config,
			"dim" : dim
		});
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderGroupedRow(config, dim);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderGroupedRow(config, dim);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		var renderGroupedRow = function(config, dim) {
			try{
				var labels = [];
				var dimentionData = [];
				var data = {labels:[],series:[]};
				if (config.charts && config.charts !== null
						&& config.charts.length > 0) {
					for ( var x = 0; x < config.charts.length; x++) {
						if (config.charts[x]["level"]){
							dimentionData.push(config.charts[x]["level"]);
						}
					}
				};
				var grp = dim.group().reduce(THIS.reduceMultipleFieldsAdd(config.group,dimentionData),
						THIS.reduceMultipleFieldsRemove(config.group,dimentionData),
						THIS.reduceMultipleFieldsInitial(config.group,dimentionData));
				var grpAll = grp.all();// .sort(THIS.sortGroup(config.group,
										// config.aggregate,1));
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size() * (config.filterdefaultval / 100));
					}
					/*
					 * if(config.filterfrom && config.filterfrom === "bottom"){
					 * sortedArray = sortedArray.slice(sortedArray.length -
					 * defaultVal); }else{ sortedArray = sortedArray.slice(0,
					 * defaultVal); }
					 */
				}
				var dataObj ={};
				grpAll.forEach(function(obj){
					for(var j=0;j<obj.value[config.charts[0].level].length;j++){
						if(!dataObj[obj.value[config.charts[0].level][j]])
							dataObj[obj.value[config.charts[0].level][j]] = 0;
							dataObj[obj.value[config.charts[0].level][j]] += obj.value[config.group];
					}
				});
				data.labels = d3.keys(dataObj);
				if(grpAll && grpAll.length>0){
					for(var i=0;i<grpAll.length;i++){
						var dataObj = grpAll[i].value[config.charts[0].level];
						if(!data.series[i]){
							data.series.push({label:grpAll[i].key});
							data.series[i].values = new Array(data.labels.length);							
						}
						for(var j=0;j<grpAll[i].value[config.charts[0].level].length;j++){
							var L1Value = grpAll[i].value[config.charts[0].level][j];
							data.series[i].values[data.labels.indexOf(L1Value)] = grpAll[i].value[config.group][L1Value];						
						}
					}
				}
				$(config.html).find("svg").remove();
				$(config.html).find("section").remove();
				$(config.html).append($("<section/>").addClass("row")	
				.append($("<div>").attr({"name":"groupedlabel"}).addClass("col-md-1"))
				.append($("<div>").attr({"id":config.field+"rowchart"}).addClass("col-md-11")));
				var chartWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					chartWidth = parseInt($(config.html+"rowchart").width());
				} else {
					chartWidth = config.chartwidth;
				}
				var chartHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					chartHeight = parseInt($(config.html).width());
					chartHeight = chartHeight > THIS.defaultHeight ? THIS.defaultHeight
							: chartHeight;
				} else {
					
					chartHeight = config.chartheight;
				}
				var barHeight = Math.round(chartHeight/(data.series.length*data.labels.length));
				var groupHeight = barHeight * data.series.length,
			    gapBetweenGroups = 10,
			    spaceForLabels   = (config.charts.filter(function(d){if(d.level) return d}).length-1) *50,
			    spaceForLegend   = 150;
				chartWidth = chartWidth - (spaceForLabels + spaceForLegend);
				// Zip the series data together (first values, second values,
				// etc.)
				var zippedData = [];
				for (var i=0; i<data.labels.length; i++) {
				  for (var j=0; j<data.series.length; j++) {
				    zippedData.push(data.series[j].values[i]);
				  }
				}
				var color = THIS.getKeyColorCodeByRange(grp, config);// d3.scale.category20();
				// var chartHeight = barHeight * zippedData.length +
				// gapBetweenGroups * data.labels.length;
				var x = d3.scaleLinear()
			    .domain([0, d3.max(zippedData)])
			    .range([0, chartWidth]);

				var y = d3.scaleLinear()
				    .range([chartHeight + gapBetweenGroups, 0]);
	
				var yAxis = d3.svg.axis()
			    .scale(y)
			    .tickFormat('')
			    .tickSize(0)
			    .orient("left");
				// Specify the chart area and dimensions
				
				var chart = d3.select(config.html+"rowchart").append("svg")
					.attr("class","groupedrow")
				    .attr("width", spaceForLabels+ chartWidth + spaceForLegend)
				    .attr("height", chartHeight);
				/*
				 * chart.data(function(group) { return group.top(2); });
				 */
				// Create bars
				var bar = chart.selectAll("g")
				    .data(zippedData)
				    .enter().append("g")
				    .attr("data-value",function(d,i,j){
				    	return d;
				    })
				    .attr("transform", function(d, i) {
				      return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i/data.series.length))) + ")";
				    });
				bar.append("title").text(function(d,i){
					var j  = i%data.series.length;
					
			    	 if(data.series[j] && data.series[j].label)
					return config.prefix+ d +config.suffix;
				});		
				// Create rectangles of the correct width
				var rect = bar.append("rect")
				    .attr("fill",function(d,i) { 
				    	i = i%data.series.length;
				    	 if(data.series[i] && data.series[i].label) 
				    		 return color[data.series[i].label];				    	 
				    })
				    .attr("data-value",function(d,i,j){
				    	i = i%data.series.length;
				    	return data.series[i].label;
				    })
				    .attr("class", "bar")
				    .attr("width", x)
				    .attr("height", barHeight - 1);
				rect.on("mousedown", function() {
					clickOnBar(this, dim);
				}).on("filtered", function() {
					clickOnBar(this, dim);
				});
				$(".reset", $(config.html)).on('click', dim, function() {
					$(this).hide();
					$(this).data('isReset',true);
					dim.filterAll();
					resetAllCharts(THIS);
				});
				// Add text label in bar
				/*
				 * bar.append("text") .attr("x", function(d) { return x(d) + 3; })
				 * .attr("y", barHeight / 2) .attr("fill", function(d,i) { i =
				 * i%data.series.length; if(data.series[i] &&
				 * data.series[i].label) return color[data.series[i].label]; })
				 * .attr("dy", ".35em") .text(function(d) { return d; });
				 */

				// Draw labels
				bar.append("text")
				    .attr("class", "label")
				    .attr("x", function(d) { return - 10; })
				    .attr("y", groupHeight / 2)
				    .attr("dy", ".35em")
				    .text(function(d,i) {
				      if (i % data.series.length === 0)
				        return data.labels[Math.floor(i/data.series.length)];
				      else
				        return ""});		
				chart.append("g")
			      .attr("class", "y axis")
			      .attr("transform", "translate(" + spaceForLabels + ", " + -gapBetweenGroups/2 + ")")
			      .call(yAxis);
				// Draw legend
				var legendRectSize = 18,
				    legendSpacing  = 4;

				var legend = chart.selectAll('.legend')
				    .data(data.series)
				    .enter()
				    .append('g')
				    .attr('transform', function (d, i) {
				        var height = legendRectSize + legendSpacing;
				        var offset = -gapBetweenGroups/2;
				        var horz = spaceForLabels + chartWidth + 40 - legendRectSize;
				        var vert = i * height - offset;
				        return 'translate(' + horz + ',' + vert + ')';
				    });

				legend.append('rect')
				    .attr('width', legendRectSize)
				    .attr('height', legendRectSize)
				    .style('fill', function (d, i) { 
				    	if(d.label) return color[d.label];
				    })
				    .style('stroke', function (d, i) { 
				    	return color[d.label];
				    });

				legend.append('text')
				    .attr('class', 'legend')
				    .attr('x', legendRectSize + legendSpacing)
				    .attr('y', legendRectSize - legendSpacing)
				    .text(function (d) {
				    	return d.label; 
				    	});		

						
			}catch (e) {
				console.log('Error Creating Grouped Row Chart' + e);
			}
		};
		renderGroupedRow(config, dim);
		clickOnBar = function(obj, dim) {
			var svg = $(obj).parents('svg'), filterValues = [], currentConfigHtml = '#'+svg.parent().parent().parent().attr("id");
			if (svg.find(".bar[data-select^=active]").length === 0 || svg.find(".bar").length === svg
							.find(".bar[data-select^=active]").length) {// 
				svg.find(".bar").attr('data-select', 'inactive').css('fill-opacity', 0.2);
				// $(".node[data-select!=active]").next().css('fill-opacity',
				// 0.2);
			}
			if ($(obj).attr('data-select') !== "active") {
				$(obj).attr('data-select', 'active').css(
						'fill-opacity', 1);
			} else {
				$(obj).attr('data-select', 'inactive').css(
						'fill-opacity', 0.2);
			}
			dim.filterAll();
			if (svg.find('[data-select^=active]').length === 0) {
				svg.find('.bar').attr('data-select', 'active').css('fill-opacity', 1);
			}
			svg.find('[data-select^=active]').each(function(i, ele) {
				filterValues.push($(ele).data("value"));
			});
			dim.filter(function(d) {
				return filterValues.indexOf(d) > -1;
			});
			svg.parent().parent().parent().find('a').data('isReset',false).show();
			resetAllCharts(THIS, currentConfigHtml);
		};
		this.refreshGroupedRowChart = function(currentConfigHtml) {
			for ( var x = 0; x < this.groupedRowCharts.length; x++) {
				if (!currentConfigHtml || this.groupedRowCharts[x]["config"].html !== currentConfigHtml) {
					renderGroupedRow(this.groupedRowCharts[x]["config"],
							this.groupedRowCharts[x]["dim"]);
				}

			}
		};
	};
	Graphing.prototype.drawMultiLevelRow = function(config){
		var THIS = this;
		var dimFun = new Function('d', "return d['" + config.field	+ "'] || 'Others' ;");
		var dim = this.ndx.dimension(dimFun);
		var labelDim = [];
		$(".reset", $(config.html)).data('isReset',true);		
		if (config.charts && config.charts !== null
				&& config.charts.length > 0) {
			for ( var x = 0; x < config.charts.length; x++) {
				if (config.charts[x]["level"]){
					labelDim[x] = this.ndx.dimension(function(d) {
						return d[config.charts[x].level];
					});
				}
			}
		};
		$(config.html).data({"chartindex":this.groupedMultiLevelCharts.length});
		this.groupedMultiLevelCharts.push({
			"config" : config,
			"dim" : dim,
			"labelDim":labelDim
		});
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderGroupedRow(config, dim);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderGroupedRow(config, dim);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		var numFormat = d3.format(config.numFormat);
		var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
			var nameObj = {};
	    	  var count = config.prefix+(numFormat(d.count))+config.suffix;
	    	  if(config.charts[d.depth-2] && config.charts[d.depth-2].level){
	    		  nameObj = THIS.masterData[config.charts[d.depth-2].level];
	    	  }else if(config.fieldname){
	    		  nameObj = THIS.masterData[config.fieldname];
	    	  }
			return ((nameObj && nameObj[d.name])?nameObj[d.name]:d.name) +" : "+ count;
		});
		var renderMultiLevelRow = function(config, dim,labelDim) {
			try{
				var numberFormat = d3.format(config.numFormat);
				$(config.html).find(".multilevel").remove();
				$(config.html).find("[name='headerinfo']").remove();
				var dimentionData = [];
				var chartWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					chartWidth = parseInt($(config.html).width());
				} else {
					chartWidth = config.chartwidth;
				}
				var chartHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					chartHeight = parseInt($(config.html).width());
					chartHeight = chartHeight > THIS.defaultHeight ? THIS.defaultHeight
							: chartHeight;
				} else {
					chartHeight = config.chartheight;
				}
				var headerinfoDiv = $("<div/>").attr({"name":"headerinfo"}).appendTo(config.html);
				$(headerinfoDiv).append($("<div/>").addClass("fuelux").append($("<div/>").addClass("wizard").append($("<ul/>").hide().addClass("steps"))));
				$(headerinfoDiv).append($("<table/>").addClass("table").css({"width": chartWidth,"margin-bottom":"0px"}).append($("<thead/>").append($("<tr/>"))));
				if (config.charts && config.charts !== null
						&& config.charts.length > 0) {
					var width = (100/(config.charts.length+2))+"%";
					headerinfoDiv.find("tr").append($("<th/>").css({"width":width,"text-align":"center"}).html(config.rootname));
					headerinfoDiv.find("tr").append($("<th/>").css({"width":width,"text-align":"center"})
					.html(config.measurelabel && config.measurelabel[config.fieldname]? config.measurelabel[config.fieldname]:config.fieldname));
					for ( var x = 0; x < config.charts.length; x++) {
						if (config.charts[x]["level"]){
							dimentionData.push(config.charts[x]["level"]);
							headerinfoDiv.find("tr").append($("<th/>").html(config.measurelabel && config.measurelabel[config.charts[x].level]? config.measurelabel[config.charts[x].level] : config.charts[x].level)
							.css({"width":width,"text-align":"center"}));
						}
					}
				};
				var grp = dim.group().reduce(THIS.reduceMultipleFieldsAdd(config.group,dimentionData),
						THIS.reduceMultipleFieldsRemove(config.group,dimentionData),
						THIS.reduceMultipleFieldsInitial(config.group,dimentionData));
				var grpAll = grp.all();// .sort(THIS.sortGroup(config.group,
										// config.aggregate,1));
				if (config.filtertype) {
					var defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp.size() * (config.filterdefaultval / 100));
					}
					/*
					 * if(config.filterfrom && config.filterfrom === "bottom"){
					 * sortedArray = sortedArray.slice(sortedArray.length -
					 * defaultVal); }else{ sortedArray = sortedArray.slice(0,
					 * defaultVal); }
					 */
				}
				
				
				var root ={};
				var levelObj = {};
				for(var i=0;i<grpAll.length;i++){
					var obj = grpAll[i];
					levelObj[obj.key] = obj.value;
					if(!levelObj[config.group])
						levelObj[config.group] = 0;
					levelObj[config.group]+= obj.value[config.group];
				}
				constructRootObj(root,levelObj,config.rootname,config.group);
				function constructRootObj(root,levelObj,name,group){
					root.name = name;
					if(!root.count)
						root.count = 0;
					root.count += levelObj[group];
					if(!root.children)
						root.children = [];
					if(d3.keys(levelObj).length>0){
						var i=0;
						for(var k in levelObj){
							if(k !== group){
								root.children[i] = {};
								constructRootObj(root.children[i++],levelObj[k],k,group);								
							}
						}
					}
				}				
				var w = chartWidth,
			    h = chartHeight,
			    x = d3.scaleLinear().range([0, w]),
			    y = d3.scaleLinear().range([0, h]);			
			var vis = d3.select(config.html).append("div").attr("chartid",config.field)
			    .attr("class", "multilevel")
			    /*
				 * .style("width", w + "px") .style("height", h + "px")
				 */
			    .append("svg:svg")
			    .attr("width", w)
			    .attr("height", h);

			var partition = d3.layout.partition()
			    .value(function(d) { return d.count; });
			
			  var g = vis.selectAll("g")
			      .data(partition.nodes(root))
			    .enter().append("svg:g")
			      .attr("transform", function(d) { return "translate(" + x(d.y) + "," + y(d.x) + ")"; })
			     .on('click',null).on("click", function(d){
			    	  click(d,dim,labelDim);
			    	  });

			  var kx = w / root.dx,
			      ky = h / 1;
			  
			  g.append("svg:rect")
			      .attr("width", root.dy * kx)
			      .attr("height", function(d) { return d.dx * ky; })
			      .style("fill",function(d){
			    	  return config.colors[d.depth] || d3.scale.category20()();
			      })
			      .attr("class", function(d) { return d.children ? "parent" : "child"; });
			  d3.selectAll(config.html).selectAll(".parent,.child").call(tip);
				d3.selectAll(config.html).selectAll(".parent,.child").on('mouseover', tip.show)
		                .on('mouseout', tip.hide);
			  g.append("svg:text")
			      .attr("transform", transform)
			      .attr("dy", ".35em")
			      .style("white-space", "pre")
			      .style("opacity", function(d) { return d.dx * ky > 12 ? 1 : 0; })
			      .text(function(d) {
			    	  var count='';
			    	  if((config.showtotal && config.showtotal === "yes") || !d.depth){
			    		  count = ' : ';
			    		  count += config.prefix+(numFormat(d.count))+config.suffix;
			    	  }			    		  
			    	  return (d.name+count);
			    	});
			  $(".reset", $(config.html)).unbind('click').bind('click', function() {
				  click(root,dim,labelDim);
			  });
			  function click(d,dim,labelDim) {
			    kx = (d.y ? w - 40 : w) / (1 - d.y);
			    ky = h / d.dx;
			    x.domain([d.y, 1]).range([d.y ? 40 : 0, w]);
			    y.domain([d.x, d.x + d.dx]);

			    var t = g.transition()
			        .duration(d3.event && d3.event.altKey ? 7500 : 750)
			        .attr("transform", function(d) { return "translate(" + x(d.y) + "," + y(d.x) + ")"; });
			    t.select("rect")
			        .attr("width", d.dy * kx)
			        .attr("height", function(d) { return d.dx * ky; });
			    t.select("text")
			        .attr("transform", transform)
			        .style("opacity", function(d) {
			        	return (d.dx * ky > 12) ? 1 : 0; });
			    var table = $(config.html).find("table");
			    table.find("th").hide();
			    $(config.html).find(".steps").html("").hide();
			    if(d.depth){
			    	var filtervalue = {};
			    	$(config.html).find('a').data('isReset',false).show();
			    	table.find("th:gt("+(d.depth -1)+")").show();
			    	var tempObj = d,label;
			    	for(var i=d.depth;i>0;i--){
			    		if(labelDim[i-2]){
			    			label = (config.measurelabel && config.measurelabel[config.charts[i-2].level])?
			    					config.measurelabel[config.charts[i-2].level]: config.charts[i-2].level;
			    			$(config.html).find(".steps").show().prepend($("<li/>")
			    					.append($("<span/>").addClass("font-normal").html(label))
			    					.append($("<span/>").addClass("text-bold").html(" : "+tempObj.name))
			    					.append($("<span/>").addClass("chevron")));
				    		filtervalue[config.charts[i-2].level] = tempObj.name;
			    			labelDim[i-2].filterAll();
			    			labelDim[i-2].filter(function(value) {
								return tempObj.name === value;
							});
			    		}else if(dim){
			    			label = (config.measurelabel && config.measurelabel[config.fieldname])?
			    					config.measurelabel[config.fieldname]: config.fieldname;
			    			$(config.html).find(".steps").show().prepend($("<li/>")
			    					.append($("<span/>").addClass("font-normal").html(label))
			    					.append($("<span/>").addClass("text-bold").html(" : "+tempObj.name))
			    					.append($("<span/>").addClass("chevron")));
				    		filtervalue[config.fieldname] = tempObj.name;
			    			dim.filterAll();
			    			dim.filter(function(value) {
								return tempObj.name === value;
							});
			    		}
			    		tempObj = tempObj.parent;
			    	}
			    	$(".steps",$(config.html)).find("li:last").addClass("active");
			    	$(config.html).data("filtervalue",filtervalue);
			    	resetAllCharts(THIS, config.html);
			    	
			    }else{
			    	table.find("th").show();
			    	$(config.html).find('a').data('isReset',true).hide();
			    	if(labelDim && labelDim.length > 0){
			    		for(var i=0;i<labelDim.length;i++){
			    			if(labelDim[i]){
			    				labelDim[i].filterAll();
			    			} 
			    		}
			    	}
			    	if(dim){
		    			dim.filterAll();
		    		}
			    	$(config.html).data("filtervalue",{});
			    	resetAllCharts(THIS);
			    }
			    if(d3.event && d3.event.stopPropagation)
			    	d3.event.stopPropagation();
			  }
			  function transform(d) {
			    return "translate(8," + d.dx * ky / 2 + ")";
			  }
						
			}catch (e) {
				console.log('Error Creating Multi Level Chart' + e);
			}
		};
		renderMultiLevelRow(config, dim,labelDim);
		this.refreshMultiLevelRowChart = function(currentConfigHtml) {
			for ( var x = 0; x < this.groupedMultiLevelCharts.length; x++) {
				if (!currentConfigHtml || this.groupedMultiLevelCharts[x]["config"].html !== currentConfigHtml) {
					renderMultiLevelRow(this.groupedMultiLevelCharts[x]["config"],this.groupedMultiLevelCharts[x]["dim"],this.groupedMultiLevelCharts[x]["labelDim"]);
				}
			}
		};
	};
	
	function remove_empty(source_group,config) {
	    return {
	        all:function () {
	            return source_group.all().filter(function(d) {
	            	var xaxisDim = config.xAxis;
	            	if(isNaN(d.value[xaxisDim])){
	            		var keyName = d.key.split(',');
	            		d.value[config.xAxis]=0;
	            		d.value[config.field]=keyName[0];
						return {key:d.key, value:d.value}
	            }else{
	            	return {key:d.key, value:d.value}
	            }
	            });
	        }
	    };
	}
	function accumulate_group(source_group,config) {
		var arr = source_group.all();
		if(config && config.field !=undefined && config.field !=''){
			var previousKey =config.field;
		}
		if(config && config.xaxistype === "number"){
			var numberXaxis =config.xaxistype;
		}
		var indexKeyMap = {};
		for(var i = 0; i<arr.length; i+=1) {
			indexKeyMap[arr[i].key] = arr[i].value; 
		}
		var sortedObj = sortObject(indexKeyMap);
		var sortedArr = [];
		for(var i in sortedObj) {
			sortedArr.push({"key" : i , value : sortedObj[i]});
		}
	    return {
	        all:function () {
	            
	            var objectOfMeasure={};
	            
	            return sortedArr.map(function(d) { // source_group.all()
	            	var cumulate = 0;
	            var allKeys = Object.keys(d.value);
	            for(var i =0; i< allKeys.length ; i++ ){
	            	var currentKey =allKeys[i];
	            	if(previousKey && d.value[previousKey] !=objectOfMeasure[previousKey] ){
	            		objectOfMeasure[currentKey]=0;
	            	}
	            	var previousKeyValue = objectOfMeasure[currentKey]?objectOfMeasure[currentKey]:0;
	            	cumulate = d.value[currentKey];
	            	if(typeof(d.value[currentKey]) ==='number'){
	            		cumulate = previousKeyValue + d.value[currentKey];
	            	}
	            	if(numberXaxis !=undefined && numberXaxis !=='' && numberXaxis ==='number' && currentKey === config.xAxis){
	            		cumulate = parseInt(cumulate);
	            	}
	            	objectOfMeasure[allKeys[i]]= cumulate;
	            }
	               return {key:d.key, value:System.CloneObject(objectOfMeasure)};
	            });
	        }
	    };
	}
	
	function table_transpose(grp,fields,measureLabel){
		var allMeasures  = Object.keys(grp.all()[0].value),
        selectedMeasures = fields;

      var commonMeasure = $.grep(allMeasures, function(element) {
             return $.inArray(element, selectedMeasures ) !== -1;
         });
       var numberFormat = d3.format(".2s"); 
	     var table ='<table name="transposeTable" class="table table-striped table-hover">';
		 table += '<thead> <tr>';
		 table += '<th>'+ '' +'</th>'
		 for(var j=0;j<grp.all().length;j++){
		 table += '<th>'+grp.all()[j].key+'</th>';
		 }
		  table += '</tr></thead><tbody>';
		for(var k=0;k< commonMeasure.length;k++){
			var lables =measureLabel[commonMeasure[k]]!=undefined ?measureLabel[commonMeasure[k]]:commonMeasure[k];
		  table += '<tr>';
		  table +='<td style="word-wrap: break-word">' + lables + '</td>';	  
		  for(var i=0;i<grp.all().length;i++){
			  var cellValue = numberFormat(grp.all()[i].value[commonMeasure[k]]);
		  table +='<td style="text-align:right">' + cellValue + '</td>';
		  }
		  table += '</tr>'; 
		}

		table += '</tbody></table>';
		return table;
	}

	Graphing.prototype.drawBar	 = function(config) { 
		var THIS = this;
		var dimFun = new Function('d', "return d['" + config.field	+ "'] || 'Others' ;");
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.prefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.suffix,
			"data-zeroValues":config.zerovalues
			});
		$(config.html).data('resetedValue',false);
		$(config.html).data('chartReseted',false);
		if (viewSettings && viewSettings.length > 0){
			$.each(viewSettings,function(index,view){
				var viewanchor=view.anchorName.split('container');
				if (viewanchor[0] ===  config.field && view.dynamicDimension && view.dynamicDimension !== config.field){
					dimFun = new Function('d', "return d['" + view.dynamicDimension	+ "'] || 'Others' ;");
					config.selectedDim = view.dynamicDimension;
					$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[view.dynamicDimension])?config.measurelabel[view.dynamicDimension]: view.dynamicDimension);
					return;
				}
			});
		}
		var dim1 = this.ndx.dimension(dimFun);
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find("select[name='filterselect']").unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						var dimension = dim1;
						if($(config.html).find("[name='dimensionselect']").length>0){
							var dimValue = $(config.html).find("[name='dimensionselect']").val(); 
							var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
							dimension = THIS.ndx.dimension(dimFunction);
						}
						renderBar(config, dimension);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(config.html).find("[name='dimensionselect']").length>0){
					var dimValue = $(config.html).find("[name='dimensionselect']").val();
					var dimFunction = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
					dim1 = THIS.ndx.dimension(dimFunction);
				}
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderBar(config, dim1);
			});
		}else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			};
		}
		function renderBar(config, dim) { 
			try {
				function remove_bins_zero(source_group, limit,fields) { // (source_group,
					// bins...}
					return {
						all : function(d) {
							if(limit){
								return source_group.top(limit);
							}else{
								return source_group.all().filter(function(d) {
								var isNonZero = false;
								if(fields && fields.length>0){
									for(var i=0;i<fields.length;i++){
										if(d.value[fields[i]] != 0){
											isNonZero = true;
										}										
									}										
								}
								return isNonZero;
								});								
							}
						}
					};
				}
				function remove_bins(source_group, limit) { // (source_group,
															// bins...}
					return {
						all : function() {
							return source_group.top(limit);
						}
					};
				}
				
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						var chartanchor =dc.chartRegistry.list()[chartIndex].anchor().split('container');
						if(chartanchor[0] === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(e){
					return THIS.getTitleAgrregate(e.data,config);
				});
				var defaultVal; 
				var bar = dc.barChart("#"+$(config.html).find("[name='maxheight']").attr('id'));
				if(config.hideyaxis && config.hideyaxis === 'yes'){
					$(config.html).addClass('no-yaxis');
				}
				if(config.hidexaxis && config.hidexaxis === 'yes'){
					$(config.html).addClass('no-xaxis');
				}
				var fields = [];
				fields.push(config.group);
				if(dim.group()){
					dim.group().dispose();
				}
				var calculatedField = [],labels = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				};
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				fields = $.merge(fields, calculatedField);
				var grp1 = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
				var grp;
				if (config.filtertype) {
					defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp1.size() * (config.filterdefaultval / 100));
					}
					grp1.all().sort(THIS.sortGroup(config.group, config.aggregate, config.filterfrom === "bottom"?0:1));
					grp = remove_bins(grp1, defaultVal);
				} else {
					grp = grp1;
					defaultVal = grp.size();
				}
				var barWidth;
				if (config.chartwidth.toLowerCase() === "auto") {
					barWidth = parseInt($(config.html).width());
				} else {
					barWidth = config.chartwidth;
				}
				var barHeight;
				if (config.chartheight.toLowerCase() === "auto") {
					barHeight = parseInt($(config.html).width());
					barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
							: barHeight;
				} else {
					$(config.html).css({"min-height":config.chartheight+"px"});
					barHeight = config.chartheight;
				}
				var chartwidth;
				if(config.barwidth && config.barwidth === "auto"){
					chartwidth  =  barWidth;
				}else{
					chartwidth  = config.marginleft+config.marginright+(defaultVal*config.barwidth);
				}
				if(chartwidth < barWidth)
					chartwidth  =  barWidth;
				
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
				}
				$(config.html).find("[name='maxheight']").css({"width":"100%","overflow-x":"overlay"});
				// var accumulateValues = accumulate_group(grp);
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField, config.filtervalue,config.absolute);
				bar.margins({
					top : config.margintop,
					left : config.marginleft,
					right : config.marginright,
					bottom : config.marginbottom
				}).width(chartwidth).height(barHeight).x(d3.scaleOrdinal())
						.xUnits(dc.units.ordinal).brushOn(false)
						.xAxisLabel(config.xaxislabel ? config.xaxislabel : '')
						.elasticX(true)
						.yAxisLabel(config.yaxislabel ? config.yaxislabel : '')
						.dimension(dim)
						.gap(5)
						.clipPadding(20)
						.title(function () { return ""; }).elasticY(true)
						.group(remove_bins_zero(grp,null,[config.group])).valueAccessor(grpAggre);
				if (config.orderby !== 0) {
					if(config.orderby === "2" || config.orderby === "3"){
						config.maxKeyLength = 0;
						grp.all().map(function(o){
							if(config.maxKeyLength < o.key.length){
								config.maxKeyLength = o.key.length;
							}
						});
					}
					bar.ordering(function(a) {
						return THIS.sortbyGroup(config, a);
					});
				}
				var grpyMax = 5;
				if(grpyMax<10){
					config.yaxisticks = grpyMax;
				}
				// bar
				if (config.yaxisinteger){
					bar.yAxis().ticks(config.yaxisticks).tickFormat(function(d) {
					    return d % 1 ? null : d3.format(",~s")(d);
					});
				}else{
					bar.yAxis().ticks(config.yaxisticks).tickFormat(d3.format("d"));// ~s
					}
				
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true);
					bar.filterAll();
                    $(e.currentTarget).hide();
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('chartReseted',false);
					resetAllProfiles(bar);
				}.bind(this));
				
				if (config.colorcodes) { 
					var keyColorCodes;
					keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
					bar.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
					bar.colorAccessor(function(d) { 
					 	return d.key;
				    });
				}
				if (!config.colorcodes) { 
				var keyColorCodes;
				keyColorCodes = dc.config.defaultColors();
				bar.ordinalColors(keyColorCodes);
				bar.colorAccessor(function(d) { 
				 	return d.key;
			    });
				}
				
				bar.on('filtered', function(chart) {
					$(".reset", $(config.html)).data('isReset',true);
					var filters = chart.filters();
					if(filters.length>0){
						$(".reset", $(config.html)).show();
					}else{
						$(".reset", $(config.html)).hide();
					}
					if(!config.isRedraw){
						$(config.html).data('resetedValue',true);						
						resetAllCharts(THIS,config.html);
						// resetAllProfiles(arguments[0]);
					}
					if(config.defaultFiltervalue.length !== filters.length){
						if(config.renderFunction !== undefined){
						      var renderFun = config.renderFunction.replace(/data/g, arguments[0].filters().slice());
						      var chartRenderFun = new Function( "return "+renderFun);
						      chartRenderFun();
						     }
						// resetAllProfiles(arguments[0]);
					}
				}.bind(this));
			
				THIS.drawnGraph.push(bar);
				if (config.showtotal === "yes") {
					bar.renderLabel(true);
				}
				bar.render();
				if(config.dynamicdimension){
					$(config.html).find("[name='dimensionselect']").remove();
					var select = THIS.constructDimentionSelect(config);
					if(config.selectedDim){
						select.val(config.selectedDim);
						delete config.selectedDim;
					}
					select.unbind('change').bind('change',
							function() {
								var dimValue = $(this).val();
								if(config.dynamicdimension.indexOf(dimValue)>-1){
									$(config.html).find(".axisLabel").html((config.measurelabel && config.measurelabel[dimValue])?config.measurelabel[dimValue]: dimValue);
									$(config.html).data('fieldname',dimValue);
								}else{
									$(config.html).find(".axisLabel").html(config.displayname || config.fieldname);
									$(config.html).data('fieldname',config.fieldname);
								}
								var dimFun = new Function('d', "return d['" + dimValue	+ "'] || 'Others' ;");
								var dimention = THIS.ndx.dimension(dimFun);
								renderBar(config, dimention);
					});
				}
				bar.on('renderlet.barclicker', function(chart, filter){
						    chart.selectAll('rect.bar').on('click.custom', function(d) {
						    	resetAllProfiles(chart);
						        // use the data in d to take the right action
						    });
				});

				if (config.showtotal === "yes") {
					bar.on("preRedraw",function(chart){
						var barWidth;
						var defaultVal = chart.group().all().length;
						if (config.chartwidth.toLowerCase() === "auto") {
							barWidth = parseInt($(config.html).width());
						} else {
							barWidth = config.chartwidth;
						}
						var chartwidth;
						if(config.barwidth && config.barwidth === "auto"){
							chartwidth  =  barWidth;
						}else{
							chartwidth  = config.marginleft+config.marginright+(defaultVal*config.barwidth);
						}
						if(chartwidth < barWidth)
							chartwidth  =  barWidth;
						chart.width(chartwidth);
						$(config.html).find("svg").attr("width",chartwidth);
					});
					bar.renderlet(function(chart) {
							if (config.yaxisinteger){
								chart.yAxis().ticks(config.yaxisticks).tickFormat(function(d) {
									    return d % 1 ? null : d3.format(",~s")(d);
								  });
							}
						moveGroupNames(chart,{group:config.group,aggregate:config.aggregate,calculatedformula:config.calculatedformula});
					});
				}
				applyDefaultFilter(config,bar);
				config.isRedraw =false;
				d3.selectAll(config.html).selectAll(".bar").call(tip);
				d3.selectAll(config.html).selectAll(".bar").on('mouseover', tip.show);
				d3.selectAll(config.html).on('mouseenter',function(){$(".d3-tip").css("opacity",0);});
			} catch (e) {
				console.log('Error Drawing Horizontal Bar ' + e);
			}
			return bar;
		}
		
		var renderedChart = renderBar(config, dim1);
		if (config.filtertype) {
			THIS.topNBarCharts.push({"config":config,"dim":dim1,"chart":renderedChart,"type":"bar"});
		}
		THIS.renderTopNBarCharts = function(currentConfigHtml){
			THIS.renderTopNCharts(currentConfigHtml,renderBar,"bar");
		};
		
		function moveGroupNames(chart,chartConfig) {
			var gLabels = chart.select(".labels");
			if (gLabels.empty()) {
				gLabels = chart.select(".chart-body").append('g').classed(
						'labels', true);
			}

			var gLabelsData = gLabels.selectAll("text").data(
					chart.selectAll(".bar"));

			gLabelsData.exit().remove(); // Remove unused elements

			gLabelsData.enter().append("text"); // Add new elements

			gLabelsData
					.attr('text-anchor', 'middle')
					.attr('fill', 'white')
					.text(
							function(e) {
								var d = d3.select(e).data()[0].data;
								var tempValue;
								
								if (chartConfig.aggregate.toLowerCase() === "sum")
									tempValue = d.value[chartConfig.group];
								else if (chartConfig.aggregate.toLowerCase() === "count")
									tempValue = d.value.count;
								else if (chartConfig.aggregate.toLowerCase() === "calculated")
									tempValue = eval(chartConfig.calculatedformula);
								else
									tempValue = d.value[chartConfig.group]
											/ d.value.count;
								// config.prefixScale="false";
								if(config.prefixScale === "false"){
									var numFormat = d3.format(config.numFormat);
									tempValue = isNaN(tempValue) ? 0 : numFormat(tempValue);
									returnValue =tempValue+config.suffix;		 
								}else{
									var prefix = d3.formatPrefix(tempValue);// d3.select(d).data()[0].data.value
									var prefixScale = Math.round(prefix
											.scale(tempValue), 0);
									// var prefix =
									// d3.formatPrefix(Math.round(tempValue),1);
									// var prefixScale =
									// Math.round(prefix.scale(tempValue));
									returnValue =prefixScale + prefix.symbol ;
								}
								return returnValue ;
							}).attr(
							'x',
							function(d) {
								return +d.getAttribute('x')
										+ (d.getAttribute('width') / 2);
							}).attr('y', function(d) {
						return +d.getAttribute('y') + 15;
					}).attr('style', function(d) {
						if (+d.getAttribute('height') < 18)
							return "display:none";
					});

		}
	};

	Graphing.prototype.drawGroupedBar = function(config) {
		var THIS = this;
		function remove_bins(source_group, limit) { // (source_group, bins...}
			return {
				all : function() {
					return source_group.all().filter(function(d, index) {
						if (limit > index)
							return -1;
					});
				}
			};
		}
		var numberFormat = d3.format(config.numFormat);
		var dimFun = new Function('d', "return d['" + config.field	+ "'] || 'Others' ;");
		$(config.html).data('resetedValue',false);
		$(config.html).data('chartReseted',false);
		var dim1 = this.ndx.dimension(dimFun);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.prefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.suffix
			});
		var barWidth;
		if (config.chartwidth.toLowerCase() === "auto") {
			barWidth = parseInt($(config.html).width());
		} else {
			barWidth = config.chartwidth;
		}
		var barHeight;
		if (config.chartheight.toLowerCase() === "auto") {
			barHeight = parseInt($(config.html).width());
			barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
					: barHeight;
		} else {
			
			barHeight = config.chartheight;
		}
		config.chartwidth = barWidth;
		config.legendxposition = barWidth * config.legendxposition;
		config.legendyposition = barHeight * config.legendyposition;
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderGroupedBar(config, dim1);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderGroupedBar(config, dim1);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		function renderGroupedBar(config, dim) {
			// try {
				function remove_bins(source_group, limit,fields) { // (source_group,
															// bins...}
					return {
						all : function(d) {
							if(limit){
								return source_group.top(limit);
							}else{
								return source_group.all().filter(function(d) {
									var isNonZero = false;
					                if(fields && fields.length>0){
										for(var i=0;i<fields.length;i++){
											if(d.value[fields[i]] != 0){
												isNonZero = true;
											}										
										}										
									}
					                return isNonZero;
					            });								
							}
						}
					};
				}
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var bar = dc.barChart(config.html);
				var defaultVal;
				if(config.hideyaxis && config.hideyaxis === 'yes'){
					$(config.html).addClass('no-yaxis');
				}
				if(config.hidexaxis && config.hidexaxis === 'yes'){
					$(config.html).addClass('no-xaxis');
				}
				var fields = [];
				var stacks = [];
				if (config.stack && config.stack !== null
						&& $.trim(config.stack) !== "") {
					stacks = config.stack.split(",");
				}
				var calculatedField = [],labels = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				fields.push(config.group);
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				fields = $.merge(fields, stacks,calculatedField);
				var grp1 = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue)), grp;
				if (config.filtertype) {
					defaultVal = config.filterdefaultval;
					if (config.filtertype === "percentage") {
						defaultVal = Math.round(grp1.size()
								* (config.filterdefaultval / 100));
					}
					grp1.all().sort(THIS.sortGroup(config.group, config.aggregate, config.filterfrom === "bottom"?0:1));
					grp = remove_bins(grp1, defaultVal);
				} else {
					grp = remove_bins(grp1,null,fields);
					defaultVal = grp1.all().length;
				}
				var barWidth;
				if (config.chartwidth && config.chartwidth === "auto") {
					barWidth = parseInt($(config.html).width());
				} else {
					
					barWidth = config.chartwidth;
				}
				var chartWidth;
				if(config.barwidth && config.barwidth === "auto"){
					chartWidth  =  config.chartwidth;
				}else{
					var groupCount = config.type === 'group'?(stacks.length+1):1;
					chartWidth  = config.marginleft+config.marginright+(defaultVal*config.barwidth*groupCount);
				}
				if(chartWidth < barWidth)
					chartWidth  =  barWidth;
				var aggField;
				if (config.aggregate === "calculated"){
					aggField = config.calculatedformula;
				}else{
					aggField =config.group;
				}
				var grpAggre = THIS.getValueByAgrregate(config.aggregate,
						aggField,config.filtervalue,config.absolute);
				var tip = d3.tip().offset([-10, 0]).attr('class', 'd3-tip').html(function(e) {
					var returnString;
					var keyName;
					var key;
					if (config.showonlybardata && config.showonlybardata === "yes"){
						key = e.x;
						returnString = "<table class='d3-tip-table'><tr><th class='d3-tip-header'>"+ key + "</th></tr>";
						keyName = config.measurelabel[e.layer]||e.layer;
						returnString += "<tr><td class='d3-tip-key'>"+keyName + " </td><td class='d3-tip-value'>"
						+ numberFormat(e.y) + "</td></tr></table>";
					}else{
						var d = e.data;
						var keys = Object.keys(d.value);
						var keyValue;
						var totalvalue = 0;
						key = d.key;
						if(config.tooltipkey === "code"){
							key = d.key;
						}else if(config.tooltipkey === "description"){
							if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
								key = THIS.masterData[config.fieldname][key];
							}
						}else{
							if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
								key = d.key+"-" + THIS.masterData[config.fieldname][key];
							}
						}
						returnString ="<table class='d3-tip-table'><tr><th class='d3-tip-header'>"+ key + "</th></tr>";
						for ( var x = 0; x < keys.length; x++) {
							if (config.aggregate === "sum")
								keyValue = d.value[keys[x]];
							else if (config.aggregate === "count")
								keyValue = d.value.count;
							else
								keyValue = d.value[keys[x]]
										/ d.value.count;
							
							if (config.measurelabel && config.measurelabel[keys[x]])
								keyName = config.measurelabel[keys[x]]||keys[x];
							else
								keyName = keys[x];
							
							if (keyName !== "count" && keyValue !== 0){
								totalvalue+= keyValue;
								returnString += "<tr><td class='d3-tip-key'>"+keyName + " </td><td class='d3-tip-value'>"
										+ numberFormat(keyValue) + "</td></tr>";
							}
						}
						returnString += "<tr><td class='d3-tip-key'>Total</td><td class='d3-tip-value'>"
						+ numberFormat(totalvalue) + "</td></tr>";
						returnString += "</table>";
					}
					if (config.linklabel && Object.keys(config.linklabel).length>0){
						returnString += "<hr class='no-margin-bottom'/>";
						if(config.linklabel[e.layer]){
							var actions = config.linklabel[e.layer].split(",");
							for (var i = 0;i<actions.length;i++){
								returnString += "<div class='tooltip-action' title='Go to "+ actions[i]+ "' onclick='dataAnalyser.tooltipActionClick(this)'>"+actions[i]+"</div>";
							}
						}
					}
					else if (config.linkaction){
						returnString += "<hr class='no-margin-bottom'/>";
						var actions = config.linkaction.split(",");
						for (var i = 0;i<actions.length;i++){
							returnString += "<div class='tooltip-action' title='Go to "+ actions[i]+ "' onclick='dataAnalyser.tooltipActionClick(this)'>"+actions[i]+"</div>";
						}
					}
					returnString += "<div class='tooltip-toolbar'><a class='tooltip-toolbar-action' onclick=System.filterChart('"+ config.html +"','"+key+"')><i class='fa fa-check txt-color-greenLight'></i> Keep Only</a><a class='tooltip-toolbar-action' onclick=System.filterChart('"+config.html+"','"+key+"',true)><i class='fa fa-times txt-color-red'></i> Exclude</a><\div>";
					return returnString;
				});

				bar.margins({
							top : config.margintop,
							left : config.marginleft,
							right : config.marginright,
							bottom : config.marginbottom
						})
						.width(chartWidth)
						.height(barHeight)
						.x(d3.scaleOrdinal())
						.xUnits(dc.units.ordinal)
						.brushOn(false)
						.dimension(dim)
						// .renderLabel(true)
						// .renderType(config.type)
						.xAxisLabel(config.xaxislabel ? config.xaxislabel : '')
						.yAxisLabel(config.yaxislabel ? config.yaxislabel : '')
						.elasticY(true)
						.title(function () { return ""; })
						.group(grp, config.group).valueAccessor(grpAggre);
				function sel_stack(i) {
					return function(d) {
						var stackValue;
						if (config.aggregate.toLowerCase() === "sum"){
							stackValue = d.value[i];
						}else if (config.aggregate.toLowerCase() === "count"){
							stackValue = d.value.count;
						}else{
							stackValue = d.value[i] / d.value.count;
						}
						return stackValue;
					};
				}
				
				for ( var i = 0; i < stacks.length; i++) {
					var stack = stacks[i];
					bar.stack(grp, stack, sel_stack(stack));
				}
				;
				if (config.orderby !== 0) {
					if(config.orderby === "2" || config.orderby === "3"){
						config.maxKeyLength = 0;
						grp.all().map(function(o){
							if(config.maxKeyLength < o.key.length){
								config.maxKeyLength = o.key.length;
							}
						});						
					}
					bar.ordering(function(a) {
						return THIS.sortbyGroup(config, a);
					});
				}
				bar.yAxis().ticks(10).tickFormat(d3.format("~s"));				
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true);
					bar.filterAll();
					$(config.html).data('resetedValue',true);
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('chartReseted',false);
				}.bind(this));
				bar.on('filtered', function() {
					resetAllCharts(THIS);
					$(".reset", $(config.html)).data('isReset',true);
					resetAllProfiles(arguments[0]);
				}.bind(this));
				/*
				 * bar.on('pretransition',function(chart){
				 * chart.selectAll('rect.bar').each(function(d){
				 * if(config.colorcodes && config.colorcodes[d.layer])
				 * d3.select(this).attr("fill", config.colorcodes[d.layer]); });
				 * });
				 */
				dc.override(bar, 'legendables', function () {
			        var legends = bar._legendables();
			        return legends.map(function(d) {
			        var legendName,count;
					var chartData = d.chart.data();
					for(var i=0;i< chartData.length;i++){
						count=0;
						if(chartData[i].name ===d.name){
							for(var j=0;j < chartData[i].values.length;j++){
								if(chartData[i].values[j].y !=0){
									count++;
								}
							}
							if(count !=0){
								return d;
							}else{
								d.hidden=true;
								return d;
							}
							 
						}
					}});
			    });
				bar.legend(dc.legend().x(config.legendxposition).horizontal(config.legendhorizontal).autoItemWidth(true).y(config.legendyposition).itemHeight(13)
						.gap(5).legendText(function (d) {
							var legendName,count;
							var chartData = d.chart.data();
							for(var i=0;i< chartData.length;i++){
								count=0;
								if(chartData[i].name ===d.name){
									for(var j=0;j < chartData[i].values.length;j++){
										if(chartData[i].values[j].y !=0){
											count++;
										}
									}
								if(count !=0){
										if (config.measurelabel && config.measurelabel[d.name]){
											legendName = config.measurelabel[d.name];
										}else{
											legendName = d.name;
										}
										if(config.colorcodes && config.colorcodes[d.name])
											$(this).prev().attr("fill",config.colorcodes[d.name]);
										return legendName;
									}
									 
								}
							}
							
					       
					    }));
				THIS.drawnGraph.push(bar);
				
				bar.on("renderlet",function(chart){
					if(config.colorcodes){
						chart.selectAll("rect.bar").attr("fill", function(d){
							  return config.colorcodes[d.layer];
							});
					}
					if (config.showtotal === "yes") {
						moveGroupNames(chart, config);
					}										
				});
				bar.render();
				applyDefaultFilter(config,bar);
				 d3.selectAll(config.html).selectAll(".bar").call(tip);
				 d3.selectAll(config.html).selectAll(".bar").on('mouseover',
				 tip.show);
				/*
				 * if (!config.linklabel)
				 * d3.selectAll(config.html).selectAll(".bar").on('mouseout',
				 * tip.hide);
				 */ 
			// } catch (e) {
			// console.log('Error Drawing Horizontal Bar ' + e);
			// }
			return bar;
		}
		var renderedChart = renderGroupedBar(config, dim1);
		if (config.filtertype) {
			THIS.topNBarCharts.push({"config":config,"dim":dim1,"chart":renderedChart});
		}
		function moveGroupNames(chart, config) {
			var gLabels = chart.select(".labels");
			if (gLabels.empty()) {
				gLabels = chart.select(".chart-body").append('g').classed(
						'labels', true);
			}

			var gLabelsData = gLabels.selectAll("text").data(
					chart.selectAll(".bar"));

			gLabelsData.exit().remove(); // Remove unused elements

			gLabelsData.enter().append("text"); // Add new elements

			gLabelsData
					.attr('text-anchor', 'middle')
					.attr('fill', 'white')
					.text(
							function(d) {
								var keyValue;
								if (config.aggregate === "sum")
									keyValue = d3.select(d).data()[0].data.value[d3
											.select(d).data()[0].layer];
								else if (config.aggregate === "count")
									keyValue = d3.select(d).data()[0].data.value.count;
								else
									keyValue = d3.select(d).data()[0].data.value[d3
											.select(d).data()[0].layer]
											/ d3.select(d).data()[0].data.value.count;
								var prefix = d3.formatPrefix(keyValue);
								var prefixScale = Math.round(prefix
										.scale(keyValue), 0);
								return prefixScale + prefix.symbol;
							}).attr(
							'x',
							function(d) {
								return +d.getAttribute('x')
										+ (d.getAttribute('width') / 2);
							}).attr('y', function(d) {
						return +d.getAttribute('y') + 15;
					}).attr('style', function(d) {
						if (+d.getAttribute('height') < 18)
							return "display:none";
					});

		}
	};

	/*
	 * Graphing.prototype.callRenderer = function (config, callback) {
	 * dc.renderAll(); }
	 */
	Graphing.prototype.dataTableClientSide = function(config){
			localStorage["datatablefilter"] = config.filters;
			var defaultTableFilter = "";
			if(config.data.filter && config.data.filter !== ""){
				defaultTableFilter = config.data.filter.replace(/@/g, "'");
			}
			config.data.exclude = [];
			var url = directoryName + "/rest/dataAnalyser/getDataTableClientSide";
			var summarycolumns = [];
			var summaryData;
			var columnData = [];
			if(config.data && config.data.isSelectAll){
				var selectAll = {
						'sTitle':'<label class="checkbox"><input type="checkbox" name="checkboxselectall"><i></i></label>',
						'name':'selectAll',
						'searchable':false,
				         'orderable':false,
				         'className': 'dt-body-center',
							"render":function(){
								 return '<label class="checkbox"><input type="checkbox" name="checkboxinline"><i></i></label>';
							}
						};
				config.columns.splice(0, 0, selectAll);
				config.data.exclude.push(0);
			}
			if(config.data.columninfo){
				$(config.html).parent().append($("<div/>").data("columns",config.columns).attr({"name":"infodiv"}).addClass("demo infosidebar hide")
						.append($("<legend/>").addClass("no-padding margin-bottom-10")
								.append($("<span/>"))
								.append($("<a/>").addClass("pull-right").unbind('click').bind('click',function(){
									$(this).parents(".demo").removeClass("activate").addClass("hide");
								}).append($("<i/>").addClass("fa fa-close txt-color-blueDark"))))
						.append($("<section/>")));
			}
			if (config.columns && config.columns.length !== 0) {
				for ( var i = 0; i < config.columns.length; i++) {
					if (config.columns[i].renderfunction && $.trim(config.columns[i].renderfunction) !== "") {
						config.columns[i].render = (function(tempRender) {
							return function(data, type, full) {
								return eval(tempRender);
							};
						})(config.columns[i].renderfunction.replace(/@/g, "'"));

					}
					config.columns[i].bVisible = !config.columns[i].hidecoloumn;
					if (config.columns[i].hidecoloumn){
						config.data.exclude.push(i);						
					}
					if(config.columns[i].showsummary
							&& config.columns[i].showsummary === true){
						summarycolumns.push(config.columns[i].data);
					}
					var columnObj ={};
					columnObj.columnName = config.columns[i].data;
					if(config.columns[i].summaryaggregate &&  config.columns[i].summaryaggregate !==null){
						columnObj.summaryaggregate = config.columns[i].summaryaggregate;
					}
					if(config.columns[i].groupbyaggregate &&  config.columns[i].groupbyaggregate !==null){
						columnObj.groupbyaggregate = config.columns[i].groupbyaggregate;
					}
					
					columnData.push(columnObj);
				}
			}
			var buttons = [];
			if(config.enableAdd && config.enableAdd === true) {
				var obj = {
						"text": 'My button',
			            "action": function ( e, dt, node, config ) {
			                   alert( 'Button activated' );
			            }
				}
				buttons.push(obj);
			}
			var dataTableInstance = $(config.html)
					.DataTable(
							{
								"dom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
										+ "t"
// / + "<tfoot> </tfoot>"
										+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"buttons": buttons,	
								"oColVis": { "aiExclude": config.data.exclude},
								"processing" : true,
								"lengthChange" : true,
								"sort" : true,
								"info" : true,
								"jQueryUI" : false,
								"order" : [ [ config.data.orderby, config.data.ordertype || "desc" ] ],
								"scrollX" : true,
								"ajax" : {
									"url" : url,
									"type" : "POST",
									"data" : function(d) {
												d.source = config.data.source,
												d.database = config.data.database,
												d.table = config.data.table,
												d.columns = config.data.columns,
												d.columnConfigurations = JSON.stringify(columnData),
												d.profilekey = location.hash
														.replace(/^#/, ''),
												d.filter = function(){
													var rtnFilters = localStorage["datatablefilter"];
													if(defaultTableFilter !==""){
														if(rtnFilters && rtnFilters !==""){
															rtnFilters += " AND "+defaultTableFilter;
														}else{
															rtnFilters = defaultTableFilter;
														}
													}
													return rtnFilters;
												},
												d.groupbyenabled = config.data.groupbyenabled,
												d.summaryenabled = config.data.summaryenabled
									},
									"dataSrc" : function(json) {
										if (json && json.isException) {
											showNotification("error", json.message);
											return [];
										}
										if (json !== undefined && json !== null) {
											if(json.summaryData)
												summaryData = JSON.parse(json.summaryData);
											return JSON.parse(json.data);
										}
									},
									error : function() {
										// showNotification("error", 'Error
										// while Processing..' );
									}
								},
								"autoWidth" : false,
								"destroy" : true,
								"columns" : config.columns,
								"fnRowCallback" : function(nRow, aData) { 
									if (config.data.drilldowncallback && config.data.drilldowncallback !== null && config.data.drilldowncallback !== "") {
										$('td', nRow).attr(
														{
															'onClick' : "javascript:dataTableDrillDown."
																	+ config.data.drilldowncallback
																	+ "(this)"
														});

										$(nRow).data("request",
												JSON.stringify(aData));
									}else if(config.data.isSelectAll){
										$(nRow).data("request",JSON.stringify(aData));
									}
								},
								"initComplete": function () {
									if(config.data.summaryenabled){
							        	 var api = this.api();
								            $(config.html).append("<tfoot><tr class='tableSummaryRow'></tr></tfoot>");
								            var isFirst = true;
								            var colSpan =0;
								            for(var index=0; index<api.columns().dataSrc().length;index++){
								            	if(api.columns(index).visible()[0] === true){
									            	var summaryVal = "";
									            	colSpan++;
									            	for(var item in summaryData[0]){
									            		if(api.columns().dataSrc()[index] === item){
									            			summaryVal = summaryData[0][item];
									            		}
									            	}
									            	if(summaryVal !== "" && isFirst === true){
									            		isFirst = false;
									            		$("tfoot tr").append('<th colspan="'+(colSpan-1)+'"  rowspan="1"  style="text-align:right">Total: </th');
									            		$("tfoot tr").append('<th colspan="1"  rowspan="1" >'+summaryVal+'</th');
									            	}else if(summaryVal !=="" || !isFirst){
									            		$("tfoot tr").append('<th colspan="1"  rowspan="1">'+summaryVal+'</th');
									            	}
								            	}
								            }
									}
						        },
								"fnHeaderCallback": function( nHead) {
									$(nHead).find("[name=checkboxselectall]").unbind('click').bind('click',this,function(e){
										$('input[name=checkboxinline]',$(e.data)).prop('checked', $(this).is(":checked"));
									});
									if($(nHead).parents("[id^='datatable']").parent().find("[name='infodiv']").length>0 && $(nHead).find("i").length === 0){
										var data = $(nHead).parents("[id^='datatable']").parent().find("[name='infodiv']").data();
										$(nHead).find("th").each(function(index){
											var val = $(this).html();
											$(this).html($("<div/>").css({"display":"inline-flex"}).html(val)
													.prepend($("<i/>").css({"margin":"4px 5px 0px 0px"}).addClass("fa fa-info-circle txt-color-blue").unbind('click').bind('click',data.columns[index],function(e){
												e.preventDefault();
												e.stopPropagation();
												getColumnInformation(e.data,$(this).parents("[id^='datatable']").next());
											})));
										});
									}
								  },
								   "drawCallback": function () {
							        if(config.data.summaryenabled){
							        	 var api = this.api();
								           $(config.html).append("<tfoot><tr class='tableSummaryRow'></tr></tfoot>");
								            var isFirst = true;
								            var colSpan =0;
								            for(var index=0; index<api.columns().dataSrc().length;index++){
								            	if(api.columns(index).visible()[0] === true){
									            	var summaryVal = "";
									            	colSpan++;
									            	for(var item in summaryData[0]){
									            		if(api.columns().dataSrc()[index] === item){
									            			summaryVal = summaryData[0][item];
									            		}
									            	}
									            	if(summaryVal !=="" && isFirst === true){
									            		isFirst = false;
									            		$("tfoot tr").append('<th colspan="'+(colSpan-1)+'"  rowspan="1" style="text-align:right">Total: </th');
									            		$("tfoot tr").append('<th colspan="1"  rowspan="1">'+summaryVal+'</th');
									            	}else if(summaryVal !=="" || !isFirst ){
									            		$("tfoot tr").append('<th colspan="1"  rowspan="1">'+summaryVal+'</th');
									            	}
									            }
								            }
							        	}
							        }
							})
							.on( 'column-visibility.dt', function ( e, settings) {
								setTimeout(function(){
									var api = new $.fn.dataTable.Api( settings );
									  var isFirst = true;
									  var colSpan =0;
									 $(config.html).append("<tfoot><tr class='tableSummaryRow'></tr></tfoot>");
									for(var index=0; index<api.columns().dataSrc().length;index++){
										if(api.columns(index).visible()[0] === true){
											var summaryVal = "";
											colSpan++;
											for(var item in summaryData[0]){
												if(api.columns().dataSrc()[index] === item){
													summaryVal = summaryData[0][item];
												}
											}
											if(summaryVal !=="" && isFirst === true){
												isFirst = false;
												$("tfoot tr").append('<th colspan="'+(colSpan-1)+'"  rowspan="1"  style="text-align:right">Total: </th');
												$("tfoot tr").append('<th colspan="1"  rowspan="1" >'+summaryVal+'</th');
											}else if(summaryVal !=="" || !isFirst){
												$("tfoot tr").append('<th colspan="1"  rowspan="1">'+summaryVal+'</th');
											}
										}
									}
									
								},25)
					    } );

			this.dataTable.push(dataTableInstance);
			if (config.resetHandler) {
				$(config.resetHandler).on('click', function(e) {
					e.preventDefault();
					this.resetGraphs();
				}.bind(this));
			}
			// }

			this.refreshTable = function() {
				var filter = "";
				if (dc.chartRegistry.list()) {
					$.each(dc.chartRegistry.list(),
									function(index, chart) {
										if (chart.hasFilter()) {
											var fieldName = $(
													"#" + chart.anchorName()).data(
													'fieldname');
											var fieldValues = chart.filters().slice();
											var othersIndex = fieldValues
													.indexOf("Others");
											if (othersIndex !== -1) {
												fieldValues.push("");
											}

											if (chart.anchorName().indexOf("timeline-") === -1 && chart.anchorName().indexOf("timebar-") === -1) {
												if (filter === "") {
													filter += othersIndex === -1 ? " "+ fieldName+ " IN ('"+ fieldValues.join("','")+ "')"
															: " ("
																	+ fieldName
																	+ " IN ('"
																	+ fieldValues
																			.join("','")
																	+ "') OR "
																	+ fieldName
																	+ " IS NULL )";
												} else {
													filter += othersIndex === -1 ? " AND "
															+ fieldName
															+ " IN ('"
															+ fieldValues
																	.join("','")
															+ "')"
															: " AND ("
																	+ fieldName
																	+ " IN ('"
																	+ fieldValues
																			.join("','")
																	+ "') OR "
																	+ fieldName
																	+ " IS NULL )";
												}
											} else if (chart.anchorName().indexOf(
													"timeline-") !== -1) {
												fieldName = fieldName.replace(
														"timeline-", "");
												if (filter !== "")
													filter += " AND";

												filter += " unix_timestamp("
														+ fieldName
														+ ", 'MM/dd/yyyy') >= unix_timestamp('"
														+ System.getFormattedDate(fieldValues[0][0])
														+ "', 'MM/dd/yyyy') AND "
														+ " unix_timestamp("
														+ fieldName
														+ ", 'MM/dd/yyyy') <= unix_timestamp('"
														+ System
																.getFormattedDate(fieldValues[0][1])
														+ "', 'MM/dd/yyyy')";
											}

										}
									});
					if (this.refreshUSChart) {
						$(".datamap")
								.each(
										function(index, ele) {
											var dataMapField = $(ele).parent("div")
													.data("fieldname");
											var pathSelected = $(ele).find(
													"path.selected");
											var stateSelected = [];
											if (pathSelected.length > 0) {

												pathSelected.each(function(
														pathIndex, pathEle) {
													stateSelected.push($(pathEle)
															.attr("state"));
												});
												if (filter !== "")
													filter += " AND";
												filter += " " + dataMapField
														+ " IN('"
														+ stateSelected.join("','")
														+ "')";
											}
										});
					}
					if (this.refreshSummary) {
						$('[id^=summary]').children().each(
								function(index, ele) {
									var dataMapField = $(ele).data("fieldname");
									var filterby = $(ele).data("filterby");
									if(filterby){
										if (filter !== "")
											filter += " AND";
										filter += " " + dataMapField + " IN('" + filterby + "')";
									}
									
								});
					}
					if (this.refreshHierarchieChart) {
						$(".hierarchie").each(
								function(index, ele) {
									var dataHierarchieField = $(ele).parent("div")
											.data("fieldname");
									var dataHierarchieValue = $(ele).parent("div")
											.data("fieldvalue");
									if (dataHierarchieValue
											&& dataHierarchieValue !== null
											&& dataHierarchieValue !== "null") {
										if (filter !== "")
											filter += " AND";
										filter += " " + dataHierarchieField
												+ " IN('" + dataHierarchieValue
												+ "')";
									}
								});
					}
					if (this.sliders && this.sliders.length > 0) {
						$(".irs-with-grid").each(
								function(index, ele) {
									var dataSliderField = $(ele).parent("div")
											.data("fieldname");
									var dataSliderValue = $(ele).parent("div")
											.data("fieldvalue");
									if (dataSliderValue && dataSliderValue !== null
											&& dataSliderValue !== "null"
											&& dataSliderValue.length > 0) {
										if (filter !== "")
											filter += " AND";
										filter += " " + dataSliderField + " IN('"
												+ dataSliderValue.join("','")
												+ "')";
									}
								});
					}
					
					if (this.filters && filter !== "") {
						filter = this.filters + " AND " + filter;
					} else if (this.filters) {
						filter = this.filters;
					}
					localStorage["datatablefilter"] = filter;

					this.dataTable.forEach(function(_datatable) {
					// $.ajaxQ.abortAll();
						_datatable.settings()[0].jqXHR.abort();
						_datatable.ajax.reload();
					});
				}
			};
	};
	var filterMapping = function(filterConfig,fieldMapping,THIS){ 
		var filterQuery;
		if (!filterConfig || filterConfig.length === 0)
			return '';
		filterQuery = "1=1";
		var includenull = "";
		for ( var i = 0; i < filterConfig.length; i++) {
			var ignoreInClause = false;
			var mappedField = filterConfig[i]["fieldName"];
			includenull = "";
			if(filterConfig[i].includenull){
				includenull = " or "+addTildInColumnName(filterConfig[i]["fieldName"])+" = '' or "+addTildInColumnName(filterConfig[i]["fieldName"]) +" is null ";
			}
			try{
				if(profileAnalyser && profileAnalyser !== null && profileAnalyser.filterMapping.length ===0){
					mappedField = filterQueryWithFiltersAndCharts(mappedField,THIS);
				}
				else if (profileAnalyser.filterMapping && profileAnalyser.filterMapping.length >0){
				// && fieldMapping[mappedField] &&
				// fieldMapping[mappedField].length > 0
					mappedField = mappedField;
					if (typeof(mappedField) === "object")
						mappedField = mappedField[0];
					if (mappedField.indexOf("#") !== -1)
						ignoreInClause = true;
					mappedField = filterQueryWithFiltersAndCharts(mappedField,THIS);
				}
			}catch(e){
				if (fieldMapping && !$.isEmptyObject(fieldMapping) && fieldMapping[mappedField] && fieldMapping[mappedField].length > 0){
					mappedField = fieldMapping[mappedField];
					if (typeof(mappedField) === "object")
						mappedField = mappedField[0];
					if (mappedField.indexOf("#") !== -1)
						ignoreInClause = true;
					mappedField = filterQueryWithFiltersAndCharts(mappedField,THIS);
				} else if (fieldMapping && !$.isEmptyObject(fieldMapping)){
					continue;
				}
			}
			if (ignoreInClause && $.trim(mappedField)){
				filterQuery += " AND " + addTildInColumnName(mappedField);
			}else if ($.trim(mappedField)){
				var fromDateOperator=">=",toDateOperator="<=";
				var searchtype = filterConfig[i].searchtype || " IN ";
				if(filterConfig[i].searchtype && filterConfig[i].searchtype === 'not in'){
					fromDateOperator="<=";
					toDateOperator=">=";					
					 // filterConfig[i]["defaultvalue"].length
						// ===0?filterConfig[i]["defaultvalue"] =['']:null;
				}
				if (filterConfig[i]["fieldType"] === 'lookup' || filterConfig[i]["fieldType"] === 'select' || filterConfig[i]["fieldType"] === 'lookupdropdown') {
					var lookupIds = "";
					var lookupData = filterConfig[i]["defaultvalue"];
					if (!lookupData || lookupData.length === 0)
						continue;
					if(filterConfig[i]["fieldType"] === 'lookupdropdown' && filterConfig[i].isSingle && !lookupData.trim())
						continue;
					if(lookupData && filterConfig[i].isSingle){
						if(filterConfig[i]["fieldType"] === 'select' && typeof(lookupData) === "object" && lookupData.id){
							lookupData = lookupData.id;
						 }
						if((filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select') && lookupData.trim()){
							lookupIds = lookupData;
						}else if(lookupData.id !== '""' && lookupData.id !== 'null'){
							lookupIds = lookupData.id;
						}
						if(lookupIds){	
							if(lookupIds === "?"){
								if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "like")
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) + " is null OR " + addTildInColumnName(filterConfig[i]["fieldName"]) + " = '')";
								else
									filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" is not null AND " + addTildInColumnName(filterConfig[i]["fieldName"]) + " <> '')";
							}else{
								if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
									lookupIds = "'" + lookupIds + "'";
									// lookupIds = (filterConfig[i]["fieldType"]
									// === 'lookupdropdown')? ("'" + lookupData
									// + "'"):("'" + lookupData.id + "'");
									filterQuery += " AND (" + addTildInColumnName(mappedField)
									+" "+ searchtype+ " (" + lookupIds + ")"+includenull;
								}else{
									lookupIds = filterConfig[i]["fieldType"] === 'lookupdropdown'? lookupData: lookupData.id;
									filterQuery+= " AND ("+addTildInColumnName(mappedField)+" "+searchtype + " '%" + lookupIds + "%'";
								}
							}
						}
					} else if (lookupData && lookupData.length > 0) {
						if( filterConfig[i]["fieldType"] === 'select')
							lookupData = lookupData.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
						if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
							if(filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select'){
								var indexVal = lookupData.indexOf(" ");
								if(indexVal>-1)
									lookupData.splice(indexVal,1);
								if(lookupData.indexOf("?")>-1){
									lookupData = lookupData.map(function(o) { return o == "?" ? "" : o; });
									lookupData.push(null);
								}
								lookupIds = lookupData.map(function(e) {e = (e && e.indexOf('`')>-1) ? e.replace(/`/g, '"') :e;return "'" + e+ "'"}).toString();
							}else{
								for ( var x = 0; x < lookupData.length; x++) {
									if(lookupData[x].id !== '""' && lookupData[x].id !== 'null'){
										if (x === 0 )
											lookupIds = "'" + lookupData[x].id + "'";
										else
											lookupIds += ",'" + lookupData[x].id + "'";
									}
								}
							}	
							
							if (lookupData.indexOf("Others") !== -1){
									if(lookupData.length==1)
									{
										filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
										+ " IS NULL"+includenull;
									}
									else
									{  
										filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
										+ " IS NULL AND " +  addTildInColumnName(filterConfig[i]["fieldName"])
										+" "+ searchtype+ " (" + lookupIds + ")"+includenull;
									}
							}
							else{
								filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
								+" "+ searchtype+ " (" + lookupIds + ")"+includenull;								
							}
						}else{
							filterQuery += " AND (";
							var operator = $.trim(searchtype.toLowerCase()) === 'like'?' OR ':' AND ';
							if(filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select'){
								var indexVal = lookupData.indexOf(" ");
								if(indexVal>-1)
									lookupData.splice(indexVal,1);
								if(lookupData.indexOf("?")>-1){
									lookupData = lookupData.map(function(o) { return o == "?" ? null : o; });
								}
							}
							for(var x=0;x<lookupData.length;x++){
								if(filterConfig[i]["fieldType"] === 'lookupdropdown')
									lookupIds=lookupData[x];
								else if(filterConfig[i]["fieldType"] === 'lookup')
									lookupIds=lookupData[x].id;
								if(x === 0)
									filterQuery+= addTildInColumnName(mappedField)+" "+searchtype + " '%" + lookupIds + "%'";
								else
									filterQuery+= operator +addTildInColumnName(mappedField)+" "+searchtype + " '%" + lookupIds + "%'";
							}
						}							
					}
					if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "in")
						filterQuery += " AND (" + addTildInColumnName(mappedField) + " is null OR " + addTildInColumnName(mappedField) + " = '')";
					else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not in")
						filterQuery += " AND (" + addTildInColumnName(mappedField) +" is not null OR " + addTildInColumnName(mappedField) + " <> '')";
					else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "like")
						filterQuery += " AND (" + addTildInColumnName(mappedField) +" like '%null%' )";
					else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not like")
						filterQuery += " AND (" + addTildInColumnName(mappedField) +" not like '%null% )";
					else
						filterQuery += " )";
				} else if (filterConfig[i]["fieldType"] === 'NUM') {
					if (filterConfig[i]["from-defaultvalue"]
							&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
						filterQuery += " AND " + mappedField
								+ " >= " + filterConfig[i]["from-defaultvalue"];
					if (filterConfig[i]["to-defaultvalue"]
							&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
						filterQuery += " AND " + mappedField
								+ " <= " + filterConfig[i]["to-defaultvalue"];
				} else if(filterConfig[i]["fieldType"] === 'dateRange'){
					var toDate,fromDate;
					var formatForDate = filterConfig[i]["dateFormat"];
					if(formatForDate === undefined){
						formatForDate = 'MM/dd/yyyy'; 
					}
	              	var returnedValue = dataAnalyser.storedDateRange(filterConfig[i]["defaultvalue"]);
					if(returnedValue && returnedValue != ""){
						 fromDate = returnedValue[0].trim();
						 toDate = returnedValue[1].trim();
					}
					if (dataAnalyser.sourceType ==="postgreSQL"){
						if (fromDate && $.trim(fromDate) !== "")
							filterQuery += " AND ("
									+ mappedField
									+  fromDateOperator+" to_date('"
									+ fromDate
									+ "', 'MM/dd/yyyy')"+includenull+" )";
						if (toDate && $.trim(toDate) !== "")
							filterQuery += " AND ("
									+ mappedField
									+ toDateOperator+" to_date('"
									+ toDate
									+ "', 'MM/dd/yyyy')+1"+includenull+")";
					}else{
						if (fromDate && $.trim(fromDate) !== "")
							filterQuery += " AND (unix_timestamp("
									+ mappedField
									+ ",'" +formatForDate+"') "+fromDateOperator+" unix_timestamp('"
									+ fromDate
									+ "', 'MM/dd/yyyy')"+includenull+" )";
						if (toDate&& $.trim(toDate) !== "")
							filterQuery += " AND (unix_timestamp("
									+ mappedField
									+ ",'" +formatForDate+"') "+toDateOperator+" unix_timestamp('"
									+ toDate
									+ "', 'MM/dd/yyyy')"+includenull+")";
					}
				}else if (filterConfig[i]["fieldType"] === 'DATE' || filterConfig[i]["fieldType"] === 'dynamicDate') {
					var fromDate = filterConfig[i]["from-defaultvalue"];
					var toDate = filterConfig[i]["to-defaultvalue"];
					var formatForDate = filterConfig[i]["dateFormat"];
					if(formatForDate === undefined){
						formatForDate = 'MM/dd/yyyy'; 
					}
					if(filterConfig[i]["fieldType"] === 'dynamicDate' && isNaN(Date.parse(filterConfig[i]["from-defaultvalue"]))){
						fromDate = eval(filterConfig[i]["from-defaultvalue"]);
						toDate = eval(filterConfig[i]["to-defaultvalue"]);
					}
					if (dataAnalyser.sourceType ==="postgreSQL"){
						if (filterConfig[i]["from-defaultvalue"]
						&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND ("
									+ mappedField
									+  fromDateOperator+" to_date('"
									+ fromDate
									+ "','MM/dd/yyyy')"+includenull+" )";
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND ("
									+ mappedField
									+ toDateOperator+" to_date('"
									+ toDate
									+ "', 'MM/dd/yyyy')+1"+includenull+")";
					}else{
						if (filterConfig[i]["from-defaultvalue"]
								&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND (unix_timestamp("
									+ mappedField
									+ ",'" +formatForDate+"') "+fromDateOperator+" unix_timestamp('"
									+ fromDate
									+ "', 'MM/dd/yyyy')"+includenull+" )";
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND (unix_timestamp("
									+ mappedField
									+ ",'" +formatForDate+"') "+toDateOperator+" unix_timestamp('"
									+ toDate
									+ "', 'MM/dd/yyyy')"+includenull+")";
					}
				}else if (filterConfig[i]["fieldType"] === 'raw' && filterConfig[i]["defaultvalue"] && $.trim(filterConfig[i]["defaultvalue"]) !== "") {
					filterQuery += " AND ";
					filterQuery+=filterConfig[i]["defaultvalue"];
				}
				else {
					if (filterConfig[i]["defaultvalue"] && $.trim(filterConfig[i]["defaultvalue"]) !== ""){
						if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
							filterQuery += " AND lower(" + addTildInColumnName(filterConfig[i]["fieldName"])
							+ ") "+searchtype+ " ('" + filterConfig[i]["defaultvalue"].toLowerCase()
							+ "')"+includenull;
						}else{
							filterQuery += " AND lower(" + addTildInColumnName(filterConfig[i]["fieldName"])
							+ ") "+searchtype+ " '%" + filterConfig[i]["defaultvalue"].toLowerCase()
							+ "%'"+includenull;
						}
						
					}
				}
			}
		}
		
		return filterQuery;
	};
	this.constructFilterQuery = function (fieldMapping,defaultFilter,profileid){
		var THIS = this;
		var constructQuery = function(fieldName,fieldValues,fieldMapping){
			var ignoreInClause;
			if (fieldMapping && !$.isEmptyObject(fieldMapping) && fieldMapping[fieldName] && fieldMapping[fieldName].length > 0){
				fieldName = fieldMapping[fieldName];
				if (typeof(fieldName) === "object")
					fieldName = fieldName[0];
				if (fieldName.indexOf("#") !== -1)
					ignoreInClause = true;
				fieldName = filterQueryWithFiltersAndCharts(fieldName,THIS);
			} else if (fieldMapping && !$.isEmptyObject(fieldMapping)){
				return '';
			}
			fieldName = addTildInColumnName(fieldName);
			var query = "";
			if (ignoreInClause && $.trim(fieldName)){
				query+=  " AND " + fieldName;
			}else if ($.trim(fieldName)){
				query+= " AND ";
				if (fieldValues.indexOf("Others") !== -1)
					query += "(";
				query+= fieldName + " IN ('";
				
			
				if (typeof(fieldValues)==="object"){
					var filterArray =[];
					for(var i=0;i<fieldValues.length;i++){
						filterArray.push(fieldValues[i].replace(/'/g, "\\'"));
						}
					var filterValues= filterArray.join("','");
					query +=  filterValues;
			
				}else{
					query +=  fieldValues.replace(/'/g, "\\'");
				}
				query += "')";
				if (fieldValues.indexOf("Others") !== -1){
					query += " OR " + fieldName + " IS NULL )";
					
				}
			}
		
			return query;
		};
		var filter = "1=1 ";
		if (dc.chartRegistry.list()) {
			$.each(dc.chartRegistry.list(),
			function(index, chart) {
				var anchorName = chart.anchorName().split('container')[0];
				if (chart.hasFilter() && !$("#" + anchorName).data('customtype')) {
					     var fieldName = $("#" + anchorName).data('fieldname');
					     var charttype = $("#" + anchorName).data('charttype');
					var fieldValues = chart.filters().slice();
					if($("#" + chart.anchorName()).data('xaxistype') && $("#" + chart.anchorName()).data('xaxistype') === "date"){
						for(var i=0;i<fieldValues.length;i++){
							var grpObj = chart.group().all().filter(function (obj) {
								  return obj.key === fieldValues[i];
								});
							fieldValues[i] = grpObj[0].value[chart.anchorName()];
						}
					}
					var othersIndex = fieldValues.indexOf("Others");
					if (othersIndex !== -1) {
						fieldValues.push("");
					}
					if (charttype !== "chart-map" && charttype !== "chart-paired-row" && chart.anchorName().indexOf("timeline-") === -1 && chart.anchorName().indexOf("timebar-") === -1 && charttype !== 'chart-scatter' ) {
						filter += constructQuery(fieldName,fieldValues,fieldMapping);	
					}else if(charttype === 'chart-paired-row'){
						var values = [];
						var pairedValues = [];
						for(var i=0;i<fieldValues.length;i++){
							values.push(fieldValues[i].split(",")[0]);
							pairedValues.push(fieldValues[i].split(",")[1]);
						}
						filter += constructQuery(fieldName,values,fieldMapping);
						filter += constructQuery($("#" + chart.anchorName()).data('pairedfield'),pairedValues,fieldMapping);						
					}
					else if (charttype === 'chart-map') {		
						var latitude = $("#" + chart.anchorName()).data("latitude");
						var longitude = $("#" + chart.anchorName()).data("longitude");
						if (!fieldMapping || $.isEmptyObject(fieldMapping) || fieldMapping[latitude]){
							latitude = (fieldMapping && fieldMapping[latitude])?fieldMapping[latitude]:latitude;
							filter += " AND " + addTildInColumnName(latitude) +" < " + fieldValues[0]._northEast.lat +" AND "+ addTildInColumnName(latitude) +" > " +  fieldValues[0]._southWest.lat;
						}
						if (!fieldMapping || $.isEmptyObject(fieldMapping) || fieldMapping[longitude]){
							longitude = (fieldMapping && fieldMapping[longitude])?fieldMapping[longitude]:longitude;
							filter += " AND " + addTildInColumnName(longitude) +" < " + fieldValues[0]._northEast.lng +" AND "+ addTildInColumnName(longitude) +" > " +  fieldValues[0]._southWest.lng;
						}
					}
					else if (charttype === 'chart-scatter') { 
						var xAxisField = $("#" + chart.anchorName()).data('xaxis');
						var yAxisField = $("#" + chart.anchorName()).data('yaxis');
						if (fieldMapping && !$.isEmptyObject(fieldMapping)){
							xAxisField = fieldMapping[xAxisField];
							yAxisField = fieldMapping[yAxisField];
						} 
						if (!fieldMapping || $.isEmptyObject(fieldMapping) || fieldMapping[xAxisField])
							filter += " AND " + addTildInColumnName(xAxisField) +" BETWEEN " + fieldValues[0][0][0] +" AND " +  fieldValues[0][1][0];
						
						if (!fieldMapping || $.isEmptyObject(fieldMapping) || fieldMapping[yAxisField])
						filter += " AND " + addTildInColumnName(yAxisField) +" BETWEEN " + fieldValues[0][0][1] +" AND " +  fieldValues[0][1][1];	
						
					}else if (chart.anchorName().indexOf("timeline-") !== -1) {
						fieldName = fieldName.replace("timeline-", "");
						var fromDateOperator=">=",toDateOperator="<=";
						if (dataAnalyser.sourceType ==="postgreSQL"){
							if (fieldValues[0][0] && $.trim(fieldValues[0][0]) !== "")
								filter += " AND cast("
										+ fieldName+" as date)"
										+  fromDateOperator+" to_date('"
										+ System.getFormattedDate(fieldValues[0][0])
										+ "', 'MM/dd/yyyy') ";
							if (fieldValues[0][1] && $.trim(fieldValues[0][1]) !== "")
								filter += " AND cast("
										+ fieldName+" as date)"
										+ toDateOperator+" to_date('"
										+ System.getFormattedDate(fieldValues[0][1])
										+ "', 'MM/dd/yyyy')+1";
						}else{
							filter += " AND unix_timestamp("
									+ addTildInColumnName(fieldName)
									+ ", 'MM/dd/yyyy') >= unix_timestamp('"
									+ System.getFormattedDate(fieldValues[0][0])
									+ "', 'MM/dd/yyyy') AND "
									+ " unix_timestamp("
									+ addTildInColumnName(fieldName)
									+ ", 'MM/dd/yyyy') <= unix_timestamp('"
									+ System.getFormattedDate(fieldValues[0][1])
									+ "', 'MM/dd/yyyy')";
						}
						
					}
				}
			});
		}
		if (this.refreshUSChart) {
			$(".datamap").each(
				function(index, ele) {
					var dataMapField = $(ele).parent("div").data("fieldname");
					var pathSelected = $(ele).find("path.selected");
					var stateSelected = [];
					if (pathSelected.length > 0) {
						pathSelected.each(function(pathIndex, pathEle) {
							stateSelected.push($(pathEle).attr("state"));
						});
						filter += constructQuery(dataMapField,stateSelected,fieldMapping);
					}
				});
		}
		
		if (this.refreshSummary || $('[id^=summary]').length) {
			$('[id^=summary]').children().each(
				function(index, ele) {
					var dataMapField = $(ele).data("fieldname");
					var filterby = $(ele).data("filterby");
					if(filterby){
						filter += constructQuery(dataMapField,filterby,fieldMapping);
					}
				});
		}
		if (this.refreshMultiLevelRowChart) {
			$(".multilevel").each(
					function(index, ele) {
						var filtervalue = $(ele).parent().data("filtervalue");
						for(var field in filtervalue) { 
							filter += constructQuery(field,filtervalue[field],fieldMapping); 
						}
					});
		}
		if (this.refreshGroupedBarLine) {
			$(".groupedbarline").each(
					function(index, ele) {
						var fieldName = $(ele).parent().data("fieldname");
						var filtervalue = $(ele).parent().data("filtervalue");
						if(filtervalue && filtervalue.length>0){
							filter += constructQuery(fieldName,filtervalue,fieldMapping);
						}						
					});
		}
		// added code for TreeMapCharts
		if (this.refreshTreeMapChart) {
			$(".treemap").each(
					function(index, ele) {
						var filtervalue = $(ele).parent().data("filtervalue");
						for(var field in filtervalue) { 
							filter += constructQuery(field,filtervalue[field],fieldMapping); 
						}
					});
		}
		if (this.refreshHierarchieChart) {
			$(".hierarchie").each(
				function(index, ele) {
					var dataHierarchieField = $(ele).parent("div")
							.data("fieldname");
					var dataHierarchieValue = $(ele).parent("div")
							.data("fieldvalue");
					if (dataHierarchieValue
							&& dataHierarchieValue !== null
							&& dataHierarchieValue !== "null") {
						filter += constructQuery(dataHierarchieField,dataHierarchieValue,fieldMapping);
					}
				});
		}
		
		if (this.sliders && this.sliders.length > 0) {
			$(".irs-with-grid").each(
				function(index, ele) {
					if ($(ele).find(".irs-from").text() === "0" && $(ele).find(".irs-to").text() === "100")
						return true;
					var dataSliderField = $(ele).parent("div").data("fieldname");
					var dataSliderValue = $(ele).parent("div").data("fieldvalue");
					if (dataSliderValue && dataSliderValue !== null
							&& dataSliderValue !== "null"
							&& dataSliderValue.length > 0) {
						filter += constructQuery(dataSliderField,dataSliderValue,fieldMapping);
					}
				});
		}
		var profileFilters = this.filters;
		if(profileid){
			profileFilters=this.filters.filter(function(obj){
			if(!obj.hasOwnProperty("chartid") )
				return obj;
			});
			
		}
			
		var filterMappingQuery = filterMapping(profileFilters,fieldMapping,this);
		if (filterMappingQuery !== '' && filter !== "") {
				filter = filterMappingQuery + " AND " + filter;
		} else if (this.filters && this.filters.length>0) {
			filter = filterMappingQuery;
		}
		if (localStorage["from"] && localStorage["from"] !== "" && localStorage[localStorage["from"]+"_"+location.hash.replace(/^#/, '').split("?")[0]]){
			if (filter && filter !== "")
				filter	+= " AND ";
			filter +=  localStorage[localStorage["from"]+"_"+location.hash.replace(/^#/, '').split("?")[0]];
		}
		if (defaultFilter  && filter) {
			defaultFilter = filterQueryWithFiltersAndCharts(defaultFilter, THIS);
			if ($.trim(defaultFilter) !== ""){
				filter = defaultFilter + " AND " + filter;
			}
		} else if (defaultFilter) {
			filter = defaultFilter;
		}
		return filter;
	};
	var filterQueryWithFiltersAndCharts = function(defaultFilter,THIS){
		var expersionList = defaultFilter.match(/\[(.*?)\]/g)||[];
		$.each(expersionList,function(){
			var compliedExpersion= "";
			var currentExpersion = this.toString();
			compliedExpersion = currentExpersion;
			var re = /(?:^|\W)#(\w+)(?!\w)/g, match;
			var chartFilters = THIS.getChartFilters()||[];
			var validExp = true;
			var firstEle = true;
			while (match = re.exec(currentExpersion)) {
			  var tempVar = match[1];
			  if (chartFilters.length === 0)
				  validExp = false;
			  if (tempVar.charAt(0) === "c"){
				  var validExpTemp = false;
				  $.each(chartFilters,function(){
					  if (this.anchorName === tempVar){
						  if (this.filterValues && this.filterValues.length !== 0){
								  validExpTemp = true;
							  if (firstEle)
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.filterValues.join("','"));
							  else
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,"'"+this.filterValues.join("','")+"'");
							  
							  firstEle = false;
						  }
						  return;
					  }
				  });
				  if (!validExpTemp)
					  validExp = false;
				  
			  } else if (tempVar.charAt(0) === "f"){
				  THIS.filters = THIS.filters ||[];
				  $.each(THIS.filters,function(){
					  if (this.id === tempVar){
						  firstEle = false;
						  if ((!this.defaultvalue || this.defaultvalue.length === 0) && compliedExpersion.indexOf("function()")=== -1)
						  {
							  validExp = false;
							  return;
						  }
						  if (this.fieldType === "lookup"){
							  if(this.isSingle)
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.defaultvalue && this.defaultvalue.id)?this.defaultvalue.id:'');
							  else
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.defaultvalue?this.defaultvalue.map(function(o){return "'"+o.id+"'"}).toString():'');
						  }
						  else if(this.fieldType === "select"){
							  if(!this.isSingle){
								  this.defaultvalue = this.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
								  var index = this.defaultvalue.indexOf(" ");
								  if(index>-1)
									  this.defaultvalue.splice(index,1);
							  }else{
								  if(typeof(this.defaultvalue) === "object" && this.defaultvalue.id){
									  this.defaultvalue = this.defaultvalue.id;
								  }
							  }
							  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.isSingle && this.defaultvalue.trim())?this.defaultvalue:this.defaultvalue.map(function(o){return "'"+o+"'"}).toString());	  
						  }
						  else if(this.fieldType === "lookupdropdown" ){
							  if(!this.isSingle){
								  var index = this.defaultvalue.indexOf(" ");
								  if(index>-1)
									  this.defaultvalue.splice(index,1);
							  }
							  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.isSingle && this.defaultvalue.trim())?this.defaultvalue:this.defaultvalue.map(function(o){return "'"+o+"'"}).toString());
						  }
						  else if(this.fieldType === "TEXT")
							  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.defaultvalue);
						  else
							  validExp = false;
						  
						  return;
					  };									
				  });
			  }
			}
			if (validExp){
				if(compliedExpersion.indexOf("function()")>-1){
					try {
						defaultFilter = defaultFilter.replace(this,eval(compliedExpersion.replace("[","").replace("]","")));
				    } catch (e) {
				    	defaultFilter = defaultFilter.replace(this,' ');
				    }
					
				}else{
					defaultFilter = defaultFilter.replace(this,compliedExpersion.replace("[","").replaceAll("]",""));
				}
			}else{
				defaultFilter = defaultFilter.replace(this,' ');
			}
		});
		return defaultFilter;
	};
	
	Graphing.prototype.drawImageGrid = function(config) {
		var THIS = this;
		this.refreshImageGrid = function(disableRefreshTable) {
			var filter = THIS.constructFilterQuery(config.fieldMapping,"",$(config.html).data("profileid"));
			renderImaggeGrid(0,filter);
		};
		var callBackGetImageData = function(response){
			disableLoading();
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			}
			if(response && response.data){
				var data =  Jsonparse(response.data," IMAGE GRID DATA");
				var pageSize = $(config.html).prev().parents(".jarviswidget-sortable").find("[name='pagedropdown']").val() || 50;
				var pageNo =$(config.html).data("currentpage") || 0;
				$(config.html).next().find("[name='showcountdiv']").html("Showing "+((pageSize*pageNo)+1)+" to "+((pageSize*pageNo)+data.length)+" of "+response.recordsTotal+" entries (filtered from "+response.recordsFiltered+" total entries)");
				$(config.html).empty();
				if(data && data.length>0){
					/*
					 * var mainDiv = $("<div/>").addClass("superbox col-sm-12
					 * smart-form"); $(config.html).append(mainDiv); for(var
					 * i=0;i<data.length;i++){ mainDiv.append($("<div/>").addClass("superbox-list
					 * col-sm-2") .append($("<img/>").addClass("superbox-img").attr({"src":data[i][config.imageColumn]}))); }
					 */
					var mainDiv = $("<div/>").addClass("col-sm-12 smart-form");
					$(config.html).append(mainDiv);
					for(var i=0,count=0;i<data.length;i++){
						mainDiv.append($("<div/>").addClass("col-sm-2").append($("<div/>").addClass("panel panel-teal margin-2")
								.append( $("<div/>").addClass("panel-heading")
										.append($("<h3/>").addClass("panel-title").append( $("<label>").addClass("checkbox col-sm-1").append($("<input/>").attr({"type":"checkbox","name":"imagecheckbox"})).append( $("<i>").addClass("margin-left-10")))
												.append($("<div/>").css({"font-size":"12px"}).attr({"title":data[i][config.filteredColumn]}).addClass("textellipsis").html(data[i][config.filteredColumn]))))
								.append( $("<div/>").addClass("panel-body no-padding text-align-center")
										.append($("<div/>").addClass("inner").append($("<div/>").addClass("superbox-list").css({"width":"90%"}).unbind("click").bind("click",{url:data[i][config.imageColumn],name:"expendedImage"+count},function(e){
											expandImage(e.data);
											$(this).addClass("active");
										}).append($("<img/>").css({"height":"100px"}).addClass("superbox-img img-responsive img-thumbnail").attr({"src":data[i][config.imageColumn]})))))				
								));
						if((i+1)%6 === 0){
							mainDiv.append($("<section/>").attr({"name":"expendedImage"+count}).addClass("superbox-show").css({"display":"none"}));
							count++;
						}
					}
					mainDiv.append($("<section/>").attr({"name":"expendedImage"+count}).addClass("superbox-show").css({"display":"none"}));
					$(config.html).parents(".jarviswidget-sortable").find("[name='selectAllCheckbox']").change(function() {
						$(config.html).find("[name='imagecheckbox']").prop('checked', $(this).is(":checked"));
					});
				}
			}
			createPaging(response.recordsFiltered);
		};
		var expandImage = function(data){
			$(".superbox-list").removeClass("active");
			$(".superbox-show").empty().css({"display":"none"});
			$(config.html).find("[name='"+data.name+"']").css({"display":"block"}).append($("<img/>").addClass("superbox-current-img").attr({"src":data.url}))
			.append($("<div/>").unbind("click").bind("click",function(){
				$(".superbox-list").removeClass("active");
				$(this).parent().empty().css({"display":"none"});
			}).addClass("superbox-close txt-color-white").append($("<i/>").addClass("fa fa-times fa-lg")));
		};
		var renderImaggeGrid = function(pagenumber,chartFilter){ 
			$(config.html).data("currentpage",pagenumber || 0);
			var pageSize = $(config.html).prev().parents(".jarviswidget-sortable").find("[name='pagedropdown']").val() || 50;
			var filter = filterMapping(config.filters,null,THIS);
			var columns = config.imageColumn+","+config.filteredColumn;
			var columnConfigurations = [{columnName:config.imageColumn},{columnName:config.filteredColumn}];
			if(chartFilter)
				filter = chartFilter;
			var request = {
				"source":config.source,
				"database" : config.database,
				"table": config.table,
				// query: config.query,
				"columns":columns,
				"columnConfigurations":JSON.stringify(columnConfigurations),
				// "search[value]":config.searchValue,
				"filter":filter || "1=1",
				"order[0][column]":config.order || 1,
				"order[0][dir]":config.orderType || 'desc',
				"start":(pagenumber || 0)*pageSize,
				"length":pageSize,
				"groupbyenabled":config.groupByEnabled,
				// profilekey:config.profilekey
			};
			enableLoading();
			callAjaxService("getDataPagination", callBackGetImageData,callBackFailure, request, "POST",null,true);
		};
		var createPaging = function(totalRecord){
			var currentPage = $(config.html).data("currentpage") || 0;
			var noOfItemInPage = $(config.html).parents(".jarviswidget-sortable").find("[name='pagedropdown']").val() || 50;
			var noOfPage =  Math.ceil(totalRecord/noOfItemInPage);
			$(config.html).next().find("[name='pagingdiv']").empty();
			var ul = $("<ul/>").addClass("pagination pagination-sm scrollpaging");
			$(config.html).next().find("[name='pagingdiv']").addClass("width-50per").html(ul);
			ul.append($("<li/>").append($("<a/>").bind("click",function(){
				renderImaggeGrid(i);
			}).attr({"href":"javascript:void(0)"}).append($("<i/>").addClass("fa fa-chevron-left"))));
			for(var i=0;i<noOfPage;i++){
				if(currentPage == i){
					ul.append($("<li/>").addClass("active").append($("<a/>").attr({"href":"javascript:void(0)"}).html(i+1) ));
				}else{
					ul.append($("<li/>").append($("<a/>").bind("click",i,function(e){
						renderImaggeGrid(e.data);
					}).attr({"href":"javascript:void(0)"}).html(i+1) ));
				}
			}
			ul.append($("<li/>").append($("<a/>").bind("click",function(){
				renderImaggeGrid(i);
			}).attr({"href":"javascript:void(0)"}).append($("<i/>").addClass("fa fa-chevron-right"))));
		};
		$(config.html).parents(".jarviswidget-sortable").find("[name='pagedropdown']").unbind('change').bind('change',function() {
			renderImaggeGrid(0);
		});
		renderImaggeGrid();
	};
	Graphing.prototype.drawDataTableJ = function(config) { 
		var THIS = this;  
		this.refreshTable = function(disableRefreshTable) {
			try{
				if(!profileAnalyser){
					var filter = THIS.constructFilterQuery(config.fieldMapping,config.data.filter,$(config.html).data("profileid"));
					localStorage["datatablefilter_"+config.html.replace("#",'')] = filter;
				}
			}catch(e){
				var filter = THIS.constructFilterQuery(config.fieldMapping,config.data.filter,$(config.html).data("profileid"));
					localStorage["datatablefilter_"+config.html.replace("#",'')] = filter;
			}
			if (!disableRefreshTable){
				this.dataTable.forEach(function(_datatable) {
					if(_datatable.ajax.params("data") && _datatable.ajax.params("data").fieldMapping){
					var filter = THIS.constructFilterQuery(_datatable.ajax.params("data").fieldMapping,_datatable.ajax.params("data").defaultFilter);
					localStorage["datatablefilter_"+_datatable.context[0].sTableId.replace("#",'')] = filter;
					if(!$(config.html).data("profileid"))
						_datatable.settings()[0].jqXHR.abort();
					_datatable.ajax.reload();
					}
				});
			};
		};
		localStorage["datatablefilter_"+config.html.replace("#",'')] = THIS.constructFilterQuery(config.fieldMapping);// filterMapping(config.filters,config.fieldMapping,this);
		localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";
		var url = directoryName + "/rest/dataAnalyser/getDataPagination";
		var summarycolumns = [];
		var summaryData;
		var columnData = [];
		var editOptions = [];
		var primaryKeys = [];
		config.data.exclude = [];
		if (!config.columns || config.columns.length === 0) {
			return;
		}
		if(config.data.columninfo){ 
			$(config.html).parent().append($("<div/>").data("columns",config.columns).attr({"name":"infodiv"}).addClass("demo infosidebar hide")
					.append($("<legend/>").addClass("no-padding margin-bottom-10")
							.append($("<span/>"))
							.append($("<a/>").attr({"href":"javascript:void(0)"}).addClass("pull-right").unbind('click').bind('click',function(){
								$(this).parents(".demo").removeClass("activate").addClass("hide");
							}).append($("<i/>").addClass("fa fa-close txt-color-blueDark"))))
					.append($("<section/>")));
		}
		if(config.data && config.data.isSelectAll && $(config.html).find("[name='checkboxselectall']").length === 0){
			var selectAll = {
					'sTitle':'<label class="checkbox"><input type="checkbox" name="checkboxselectall"><i></i></label>',
					'name':'selectAll',
					'searchable':false,
			         'orderable':false,
			         'className': 'dt-body-center',
						"render":function(){
							 return '<label class="checkbox"><input type="checkbox" name="checkboxinline"><i></i></label>';
						}
					};
			config.columns.splice(0, 0, selectAll);
			config.data.exclude.push(0);
		}
		var tableColumns = config.data.columns.split(",");
		for(var i=0;i<tableColumns.length;i++){
			tableColumns[i] = addTildInColumnName(tableColumns[i]);
		}
		config.data.columns = tableColumns.toString();
		for ( var i = 0; i < config.columns.length; i++) {
			if (config.columns[i].renderfunction && $.trim(config.columns[i].renderfunction) !== "") {
					try{
						var colorConfig = jQuery.parseJSON(config.columns[i].renderfunction);
						config.columns[i].render = (function(colorConfig) {
							return function(data, type, full,meta) {
								var sampleapi = new $.fn.dataTable.Api( meta.settings );
								var cell = $(sampleapi.cell({row:meta.row,column:meta.col}).node());
								for (var key in colorConfig) {
									var dataRange = key.split(":");
									if(dataRange.length > 1 && (data >= dataRange[0] && data <= dataRange[1])){
										cell.css('background-color',colorConfig[key]);
									}else if(dataRange.length === 1 && data === dataRange[0]){
										cell.css('background-color',colorConfig[key]);
									}
									return data;
								};
							};
						})(colorConfig);
						
					}
						catch(err){
							config.columns[i].renderfunction = config.columns[i].renderfunction.replace(/@/g, '"');
							config.columns[i].renderfunction = config.columns[i].renderfunction.replace(/\\\\\\\\/g, "\\");
							config.columns[i].render = Function( "data","type","row","meta", "return "+config.columns[i].renderfunction);
						}
				}
			if(config.columns[i].wraptext && config.columns[i].wraptext === true){
				var wrapper = "'<div class=\"dt-wrap-text\">'+ data +'</div>'";
				config.columns[i].render = (function(tempRender) {
					return function(data, type, full) {
						if(!data) {							
							data = '';
						}
						return eval(tempRender);
					};
				})(wrapper.replace(/@/g, "'"));
			}
			config.columns[i]["width"] = config.columns[i].colwidth || 0;
				config.columns[i].bVisible = !config.columns[i].hidecoloumn;
				if (config.columns[i].hidecoloumn ){
					config.data.exclude.push(i);					
				}
				if(config.columns[i].showsummary && config.columns[i].showsummary === true){
					summarycolumns.push(config.columns[i].data);
				}
				var columnObj = {};
				columnObj.columnName = config.columns[i].data;
				if(config.columns[i].summaryaggregate &&  config.columns[i].summaryaggregate !==null){
					columnObj.summaryaggregate = config.columns[i].summaryaggregate;
				}
				if(config.columns[i].groupbyaggregate &&  config.columns[i].groupbyaggregate !==null){
					columnObj.groupbyaggregate = config.columns[i].groupbyaggregate;
				}	
				columnData.push(columnObj);
				// if ((config.columns[i].editable && config.columns[i].editable
				// === true)) { 
					var columnEdit = {
						"field": config.columns[i].data,
						"title":  config.columns[i].title || config.columns[i].data,
						"visible":config.columns[i].bVisible,
						"mandatory":  config.columns[i].mandatory,
						"type": config.columns[i].edittype,
						"options": config.columns[i].typeoptions,
						"editable": config.columns[i].editable,
						"defaultvalue":config.columns[i].editvalue,
						"disable":config.columns[i].disable,
						"sourceconfig":config.columns[i].sourceconfig,
						"databaseconfig":config.columns[i].databaseconfig
					};					
					editOptions.push(columnEdit);
				// };
			
			if (config.columns[i].primarykey && config.columns[i].primarykey === true){
				primaryKeys.push({"fieldName":config.columns[i].data,"fieldType":config.columns[i].edittype,"enableAudit":config.enableAudit});
			}
		}
		// to get default filters from chart
		this.refreshTable(true);
		var isScriptLoaded;
	    if (config.isEditable || (config.enableAudit && config.showAudit)){
	    	var tableDetails = {
	    			"source":dataAnalyser.sourceType,
	    			"database": dataAnalyser.database,
	    			"table":config.data.table
	    	};
	    	
	    	if(config.enableBulkUpdate){  
	    		editOptions["enableBulkUpdate"]=true;
	    	}
	    	if(config.enableComments){
	    		editOptions["enableComments"]=true;
	    	}
	    	if(config.commentSectionName){
	    		editOptions["commentSectionName"]=true;
	    	}
	    	
	    	var enableEditor = function(){ 
	    		var dataTableEditor = new DataTableEditor(); 
			    $(config.html).on( 'click', 'tbody td', function (e) {
			    	if((e.target.nodeName === 'a' || e.target.nodeName === 'A') && $(e.target).attr('target') === '_blank') { 
			    		 return;
			    	}
			    	if($(this).hasClass('audit-log')) {
	    				dataTableEditor.showAuditLog(this,tableDetails);
	    			}else{
				    	if(config.data.history){
				    		config.data.history.rowData = Jsonparse($(e.target).parent().data("request"));
				    	}
	    				if (config.editType === "inline")
				    		dataTableEditor.inline(editOptions,this,tableDetails,config.data.history);
	    				
				    	else if (config.editType === "form")
				    		dataTableEditor.form(editOptions,this,tableDetails,config.data.history);
				    	else if(config.editType === "actionForm"){
	    				  // $(".editColumn").on('click', function(){
	    				   if (config.editType === "actionForm" && config.isOverride && config.data.overrideFunction){
				    		  //dataTableEditor.actionForm(actionCallbackHandler.createObjectPopUp(null,this),this,tableDetails,config.data.formName); 
	    					   $('.editColumn').attr({'onClick' : "javascript:actionCallbackHandler."+ config.data.overrideFunction+ "(this)"});
	    					   //actionCallbackHandler.createObjectPopUp(null,this)
				    	   }
	    				   else
	    					 $(".editColumn").on('click', function(){
				    		     dataTableEditor.actionForm(editOptions,this,tableDetails,config.data.formName);
	    					 });
			            // });
	    				}	 
	    				
	    			}
			    });
	    	};	    	
	    	loadScript("js/DataAnalyser/datatableEditor.js",enableEditor,config.html); 
	    	if(config.enableAudit && config.showAudit && config.data.columns.split(",").length === config.columns.length){
		    	var logColumn = {
						"data":null,
						"defaultContent":"<i class='fa fa-info-circle' title='Audit Log'></i>",
						"class":"audit-log",
						"title":"Audit Trail"
				};
				config.columns.push(logColumn);	
	    	}
	    	if(config.editType === "actionForm"){
		    	var actionColumn = {
						"data":null,
						"defaultContent":"<div class='btn-group display-inline pull-right text-align-left hidden-tablet' style='padding: 5px 14px;'><button class='btn btn-xs btn-default dropdown-toggle' data-toggle='dropdown'><i class='fa fa-cog fa-lg'></i></button><ul class='dropdown-menu dropdown-menu-xs pull-right' style='right:-67px'><li><a class='editColumn'><i class='fa fa-fw fa-edit'></i>Edit</a></li><li class='divider'></li><li class='text-align-center'><a onclick='javascript:#.CancelDelete(event)'>Cancel</a></li></ul></div>",
						"class":"actionColumn",
						"title":"Action"
				};
				config.columns.unshift(actionColumn);	
	    	}
	    	$(config.html).addClass("editable-datatable");	    	
	    };
	    var pageLength = parseInt(config.pageLength) || 10;
	    var buttons = [];
	    if(config.enableAdd && config.enableAdd === true) {
			var obj = {
					"text": 'My button',
		            "action": function () {
		            	showNotification("error", "Button activated");
		            }
			};
			buttons.push(obj);
		}
	    if($(config.html +"_wrapper").find(".columnSearch").length !== 0)
		{
	    	$(config.html +"_wrapper").find(".columnSearch").remove();
		}
	    var showtoolbar="<'col-sm-2 col-xs-2 hidden-xs hideShowHideDiv pull-right 'C>";
	    if(config.fieldMapping && config.fieldMapping.disableshow){
	    	showtoolbar="";
	    }
	    var orderBy = [];
	    if(config.data.orderby) {
	    		var strOrderBy = '' + config.data.orderby;
	    		strOrderBy.split(',').map(function(orderColumn) {
	    			var arr = [ parseInt(orderColumn), config.data.ordertype || "desc" ]
	    			orderBy.push(arr);
	    		});
	    }
	    var filterdBy = "<'col-sm-4 col-xs-4 filterdByDiv'>"
	    var searchBox = "f";
	    if(config.hideSearch){
	    	searchBox ="";
	    }
	    var dom =  "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'"+searchBox+">";
	    if(config.hideShowHide && config.hideNoOfRows){
	    	dom =  "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'"+searchBox+">";
	    }
	    if(config.hideShowHide && config.hideNoOfRows){
	    	filterdBy = "<'col-sm-9 col-xs-9 filterdByDiv'>"
	    }
	    dom+=filterdBy;
	    if(!config.hideShowHide || config.hideShowHide !== true) {
	    	 dom+=showtoolbar;
	    	
	    }
	    if(!config.hideNoOfRows || config.hideNoOfRows !== true) {
	    	dom+="<'col-sm-1 col-xs-1 hidden-xs noRowDiv pull-right'l>";
	    }
	    dom+=
		"r<\"toolbar\">>"
			+ "t"
			+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>"
			 if(!config.hidePagination || config.hidePagination !== true) {
				 dom+="<'col-xs-12 col-sm-5'p><'col-xs-12 col-sm-1 inputboxdiv'>" 
			 }
			 
	    dom+=">";
		var dataTableInstance = $(config.html)
			.DataTable(
						{
							"dom" :dom,
									
							"oColVis": { "aiExclude": config.data.exclude},		
							"processing" : true,
							"lengthChange" : true,
							"serverSide" : (!config.data.serverType || config.data.serverType === "serverSide")?true:false,
							"sort" : true,
							"info" : true,
							"conditionalPaging": true,
							"oLanguage" : {
								"sProcessing" : '<span id="loader" > Preparing Data for Display</span>',
								"sInfoEmpty" : "No Data to Display"
							},
							"responsive": true,
							"jQueryUI" : false,
							"order" : orderBy,// [ [ config.data.orderby,
												// config.data.ordertype ||
												// "desc" ] ],
							"scrollX" : true,
							"scrollY": '65vh',
							"scrollCollapse": true,
							"pageLength":pageLength,
							"autoWidth" : false,
							 "bFilter": true,
							"initComplete": function(){
							
								//$(".dataTables_wrapper").css({"max-height":"560px","overflow-x":"auto","overflow-y":"scroll"});
								$(".dataTables_filter").find("span").css({"border-radius":"16px 0px 0px 16px"});
								$(".dataTables_scroll").css({"transform":"translate(0, 0)"});
								//$(".columnSearch").find("ul").css({"position":"fixed","margin-top":"128px","width":"17%"});
								 $(config.html +"_wrapper").find(".inputboxdiv").append('<label class="font-bold">Page:</label>&nbsp<input type="number" class="datatable-pagejump"  value="1"/>');
								 $(config.html +"_wrapper").find(".datatable-pagejump").keyup(function(event){
									 if(event.keyCode != 13)
										 return;
									 
									 var inputvalue = $(config.html +"_wrapper").find(".datatable-pagejump").val();
									 var totalpages = dataTableInstance.page.info().pages;
									 if(inputvalue < 0)
										 inputvalue = 1;
									 if (inputvalue > totalpages)
										 inputvalue = totalpages;
									 
									 $(config.html +"_wrapper").find(".ColVis_Button ").attr("disabled", true);
									 dataTableInstance.page(inputvalue-1);
									 dataTableInstance.ajax.reload(null,false);
								});
								
								if(profileTableMap[this.selector].enableAdd && profileTableMap[this.selector].enableAdd === true) {								  
						          $(this.selector + "_wrapper div.toolbar").html('<button data-show="'+profileTableMap[this.selector].enableAdd+'" data-sequence="'+profileTableMap[this.selector].html+'" class="btn btn-primary dataTableRowAdd border-round pull-right" type="button"><i class="fa fa-plus"></i> Create</button>');
						         /* $(".dataTableRowAdd").on('click', function(){
						        	  var dataTableEditor = new DataTableEditor();
						        	  if(config.isOverride&&config.data.overrideFunction) {
						        		  //dataTableEditor.addRow(actionCallbackHandler.createObjectPopUp(this),config.data.formName); 
						        		  "javascript:actionCallbackHandler."+ config.data.overrideFunction+ "(this)"
						        	  }
						        	  else{
						        		  dataTableEditor.addRow(this,config.data.formName);
						        	  }
						          });	*/	
						          if(config.isOverride&&config.data.overrideFunction) {
						            $('.dataTableRowAdd').attr({'onClick' : "javascript:actionCallbackHandler."+ config.data.overrideFunction+ "()"});
						          }
						          else{
						        	  $(".dataTableRowAdd").on('click', function(){
						        	  var dataTableEditor = new DataTableEditor();
						        		  dataTableEditor.addRow(this,config.data.formName,config.editType);
						          });
						          }
								}	
								setTimeout(function(){
									$(config.html +"_wrapper").find("th").find("i").popover({
							    		  placement: 'auto',
							    		  container: 'body',
							    		  delay :  { "show": 100, "hide": 100 },
							    		  trigger : 'hover'
							    	  });
									$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
								}, 1000);
								dataAnalyser.setViewConfigInWidget($(config.html +"_wrapper").parents(".jarviswidget").prop("id"));
						    }, 
							"ajax" : {
								"url" : url,
								"global": false,
								"type" : "POST",
								"data" : function(d) {
									$(config.html).find('tbody').empty();
									// enableLoading('loader');
									if(Object.keys(d).length >0 && d.search && d.search.value && d.search.value!="" ){
							                  d.search.value =d.search.value.replace(/'/g, "\\'");
							                }
											d.source = config.data.source,
// d.order = if()
											d.database = config.data.database,
											d.table = config.data.table,
											d.query = config.data.dataQuery?filterQueryWithFiltersAndCharts(config.data.dataQuery,THIS):'',
											d.columns = config.data.columns,
											d.columnConfigurations = JSON.stringify(columnData),
											d.profilekey = location.hash.replace(/^#/, ''),
											d.order = config.data.isSelectAll?[{column : d.order[0].column-1,dir:d.order[0].dir}]:d.order,
											d.filter =  function(){ 
												var rtnFilters = localStorage["datatablefilter_"+config.html.replace("#",'')] + " AND " + localStorage["datatablecolumnfilter_"+config.html.replace("#",'')];
												if(localStorage["datatablefilter_"+config.html.replace("#",'')] && localStorage["datatablefilter_"+config.html.replace("#",'')]){
													rtnFilters = localStorage["datatablefilter_"+config.html.replace("#",'')] + " AND " + localStorage["datatablecolumnfilter_"+config.html.replace("#",'')];
												}else if(localStorage["datatablefilter_"+config.html.replace("#",'')]){
													rtnFilters = localStorage["datatablefilter_"+config.html.replace("#",'')];
												}else if(localStorage["datatablecolumnfilter_"+config.html.replace("#",'')]){
													rtnFilters = localStorage["datatablecolumnfilter_"+config.html.replace("#",'')];
												}
												if(config.data.dataQuery){
													var dataQuery = filterQueryWithFiltersAndCharts(config.data.dataQuery,THIS);
													var filters = dataQuery.split(",");
													if(filters && filters.length>0){
														for(var i=0;i<filters.length;i++){
															if(filters[i].indexOf(" as ")>0){
																var alias = filters[i].split(" as ");
																if((alias && alias.length>1)&& rtnFilters.indexOf(alias[1].trim())>-1){
																	var regex = new RegExp(alias[1].trim(), "g");
																	rtnFilters = rtnFilters.replace(regex, alias[0].trim());
																}
															}
														}
													}
												}
												return rtnFilters.replace(" 1=1 AND","").replace(" AND 1=1","");
											},
											d.defaultFilter = config.data.filter?config.data.filter.replace(/@/g, "'").replace(" 1=1 AND","").replace(" AND 1=1",""):'',
											d.groupbyenabled = config.data.groupbyenabled,
											d.summaryenabled = config.data.summaryenabled,
											d.fieldMapping = config.fieldMapping
								},
								"dataSrc" : function(json) { 
									var inputval = $(config.html).DataTable().page();
									$(config.html +"_wrapper").find(".datatable-pagejump").val(inputval+1);
									
									if (json && json.isException) {
										showNotification("error", json.message);
										return [];
									}
									if (json !== undefined && json !== null) {
										// Getting list of colums having summary Aggregate as N/A.
										NaColumnsList = {}
										if(config && config.columns){
											for(var x = 0; x < config.columns.length; x++){
												if(config.columns[x].summaryName){
													NaColumnsList[(config.columns[x].data)] = "N/A"
												}
											  }
											}
										var rtnData =JSON.parse(json.data);
										for(n in rtnData )
										{
											if(config.isFilterNotapplicable){
											  for(i in rtnData[n])
											  {
												  if(rtnData[n][i]=="NULL"||rtnData[n][i]=="null"||rtnData[n][i]==null||rtnData[n][i]=="")
												  {
													  rtnData[n][i]="N/A";
												  }
												  else{
													  rtnData[n][i]= rtnData[n][i];
												  }
											     
											  }
											}
											if(localStorage["isMultipleProfile"]){
												if(Jsonparse(dataAnalyser.additionalconfig)["isFilterNotapplicable"]){
													  for(i in rtnData[n])
													  {
														  if(rtnData[n][i]=="NULL"||rtnData[n][i]=="null"||rtnData[n][i]==null||rtnData[n][i]=="")
														  {
															  rtnData[n][i]="N/A";
														  }
														  else{
															  rtnData[n][i]= rtnData[n][i];
														  }
													     
													  }
													
												}
											}
										}
										
										if(json.summaryData && json.summaryData !=="[]" && rtnData && rtnData.length>0){
											summaryData = JSON.parse(json.summaryData);
											summaryData[0][config.columns[0].data] = "Total : ";
											if(NaColumnsList)
												summaryData[0] = $.extend(summaryData[0], NaColumnsList);
											rtnData.push(summaryData[0]);
											$(config.html).addClass("summaryDataTable");
										}
										return rtnData;
									}
								},
								error : function() {
									// showNotification("error", 'Error while
									// Processing..' );
								}
							},
							callBack:function(){
								
								setTimeout(function(){
									console.log("0k")
									$($.fn.dataTable.tables(true)).DataTable()
								      .columns.adjust();
								},1000)
							},
							"fixedColumns":   {
					            leftColumns: config.fixedLeftColumn,
					            rightColumns: config.fixedRightColumn
					        },
							"destroy" : true,
							"columns" : config.columns, 
							"fnHeaderCallback": function( nHead) {
								$(nHead).find("[name=checkboxselectall]").unbind('click').bind('click',this,function(e){
									$('input[name=checkboxinline]',$(e.data)).prop('checked', $(this).is(":checked"));
								});
								if(config.data.history){
									$(nHead).parents("[id^='datatable']").data("history",JSON.stringify(config.data.history));
									var tableDetails = {
										source:config.data.source,
										database:config.data.database,
										table:config.data.table,
									};
									$(nHead).parents("[id^='datatable']").data("tabledetail",JSON.stringify(tableDetails));
								}
								if (!config.fixedLeftColumn && !config.fixedRightColumn && (!config.fieldMapping || !config.fieldMapping.disablefilter) ){
									var stringAlias;
									if ( config.data.source.toLowerCase()  === "hive"){
										stringAlias = "string";
									}else{
										stringAlias = "char";
									}
									
									if(dataTableInstance && dataTableInstance.columns && $(config.html +"_wrapper").find(".columnSearch").length === 0 ){
										if(config.hideFilter){
											var columnSearchTR = $("<tr/>").addClass("columnSearch");
											var distinctFilterDiv = $("<div/>").addClass("distinctFilter"); 
											$(nHead).parent().append(columnSearchTR);
											dataTableInstance.columns().every( function () {
												if (this.visible())
												{
													var column = this;
													var title = $(this.header()).text();
													var columnName = this.dataSrc();
													if (columnName === null && title === "Audit Trail"){
														$(columnSearchTR).append($("<th/>"));
													}
													else if (columnName === null && title === "Action"){
														$(columnSearchTR).append($("<th/>"));
													}
													else{
														$(columnSearchTR).append($("<th/>").append($('<input placeholder=" &#xF002; " class="fontAwesome" />')))
														$(columnSearchTR).append($("<th/>").append($('<input placeholder=" &#xF002; " class="fontAwesome" />').on( 'keyup', function () {
															var searchValue = $(this).val();
															var scrollLeft = $(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft();
															$(config.html).data("scrollleft",scrollLeft);
															localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";
															$($(this).parents("tr")[0]).find("input").each(function(){
																if ($(this).val()){ var searchFilter =$(this).val().toLowerCase();
																	localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += " AND LOWER(COALESCE(NULLIF(CAST("+ addTildInColumnName($(this).attr("column")) + " AS "+stringAlias+" ),''),'')) like '%"+searchFilter.replace(/'/g, "\\'")+"%'";
																}
															});
															dataTableInstance.settings()[0].jqXHR.abort();
															if(!config.data.serverType || config.data.serverType === "serverSide"){
																//dataTableInstance.ajax.reload();
																dataTableInstance.ajax.reload(
																	function(){
																		var scrLeft = $(config.html).data("scrollleft");
																		$(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft(scrLeft);
																	});
															}else{
																column.search( searchValue ? '^'+searchValue.toLowerCase().replace(/'/g, "\\'") : '', true, false ).draw();
															}
												        } ).css({"border-radius": "4px"}).attr({"column":columnName,"type":"text"})));
													}
												}
											});	
										}
										else{
										$(config.html +"_wrapper").parent().parent().find('.filterdByDiv').css({'margin-left':'-150px','margin-top':'5px'}).html("");
										if($(config.html +"_wrapper").parent().parent().find('.hideShowHideDiv').length!=0 && $(config.html +"_wrapper").parent().parent().find('.noRowDiv').length!=0){
											$(config.html +"_wrapper").parent().parent().find('.noRowDiv').css({'margin-right':'-58px'})
										}
										if($(config.html +"_wrapper").parent().parent().parent().width()<1241){
										   $(config.html +"_wrapper").parent().parent().find('.filterdByDiv').css({'margin-left':'-26px','margin-top':'5px'}).html("");
										   $(config.html +"_wrapper").parent().parent().find('.noRowDiv').css({'margin-right':'75px'})
										}
										var filterbyDiv = '<div data-toggle="tooltip" id="innerFilterDiv"  class="text-truncate"><label style="max-width: 95ch;overflow: hidden;text-overflow: ellipsis;white-space: nowrap ;"><b>Filtered by: </b>'
										var tooltipsString = '<div style="word-break: break-all;" >'
										var filterStr = '';
										var filtersApplied = [];
										if($(config.html +"_wrapper").parent().parent().parent().width()<1241){
											var filterbyDiv = '<div data-toggle="tooltip" id="innerFilterDiv"  class="text-truncate"><label style="max-width: 28ch;overflow: hidden;text-overflow: ellipsis;white-space: nowrap ;"><b>Filtered by: </b>'
										}
										try{
										if(profileAnalyser != undefined && profileAnalyser && profileAnalyser != null && profileAnalyser.filters)
											filtersApplied = profileAnalyser.filters;
										else
											filtersApplied = dataAnalyser.filters;
										
										filtersApplied.forEach(function(filter){
											if(!filter.invisible && filter.invisible != true){
												if(filter.fieldType === "dynamicDate" || filter.fieldType === "DATE" || filter.fieldType === "dateRange"){
													var filterval = "";
												if(filter.fieldType === "dateRange"){
													
													if(filter["defaultvalue"].currentRangeType){
														btweenFrom = filter["defaultvalue"].selectedRangeDate.from
														betweenTo = filter["defaultvalue"].selectedRangeDate.to
														if(btweenFrom && betweenTo)
															btweenFrom =btweenFrom*-1;
														filterval = getManipulatedDate(filter["defaultvalue"].currentRangeType,btweenFrom) +' To '+getManipulatedDate(filter["defaultvalue"].currentRangeType,betweenTo)
													}
												}
												else{
													if(filter["from-defaultvalue"].indexOf("getManipulatedDate") >= 0)
														var filtervalfrom = eval(filter["from-defaultvalue"]);
													else
														var filtervalfrom = filter["from-defaultvalue"];
													
													if(filter["to-defaultvalue"].indexOf("getManipulatedDate") >= 0)
														var filtervalto = eval(filter["to-defaultvalue"]);
													else
														var filtervalto = filter["to-defaultvalue"];
													if(filtervalfrom==""||filtervalto=="")
														filterval = filtervalfrom + ""+filtervalto;
													else
													filterval = filtervalfrom + "-"+filtervalto;
												}
											}else{
												filterval = filter["defaultvalue"];
											}
											if(filterval){
												filterStr+='<span style="color: chocolate;font-size: 12px;margin-left:4px;margin-right:2px">'+filter.displayName+':</span><span id="filterLastLabel" style="color: #7868d0;font-size: 11px;">'+filterval+'</span>';
												tooltipsString+='<label style="font-family: monospace; font-weight: bold; color: #ccc;font-size: 12px;margin-left:4px;margin-right:2px"><br>'+filter.displayName+':</label><label style="color: #fff;font-size: 11px;">'+filterval+'</label>';
											}
											
											}
										});
										
										if(filterStr != ''){
											filterStr=filterbyDiv+filterStr+'</label></div>'
											
										}
											
										//$(config.html +"_wrapper").parent().parent().find('.filterdByDiv').html(filterStr);
										
										if($(config.html +"_wrapper").parent().parent().find('.filterdByDiv').find("#innerFilterDiv").find('label').width() >= 706){
											$(config.html +"_wrapper").parent().parent().find('#innerFilterDiv').attr('title',tooltipsString+'</div>')
											$(function() {
												$(config.html +"_wrapper").parent().parent().find('#innerFilterDiv').tooltip({html:true,container: 'body'})
											});
										}
										if($(config.html +"_wrapper").parent().parent().parent().width()<1241){ 
										    if($(config.html +"_wrapper").parent().parent().find('.filterdByDiv').find("#innerFilterDiv").find('label').width() >= 200){
										    	$(config.html +"_wrapper").parent().parent().find('#innerFilterDiv').attr('title',tooltipsString+'</div>')
										    	$(function() {
												$(config.html +"_wrapper").parent().parent().find('#innerFilterDiv').tooltip({html:true,container: 'body'})
										    	});
										    }
										}
										}catch(ex){
											console.log(ex)
										}
										
										var columnSearchTR = $("<tr/>").addClass("columnSearch");
										
										$(nHead).parent().append(columnSearchTR);
										dataTableInstance.columns().every( function () {
										
											var colName = this.dataSrc();
											var distinctFilterDiv = $("<div/>").addClass("distinctFilter"+colName); 
											var searchList = {
													'Does not Contain':"not like '%<Q>%'",
													'Starts with':"like '<Q>%'",
													'Ends with':"like '%<Q>'",
													'Equals':" = '<Q>'",
													'Not Equals':" != '<Q>'",
											};
											$.each(config.columns, function(index, val){ 
												var stringDatatypes = [ "string", "character varying", "varchar",
													"character", "char", "text" , "longtext"];
												if(val.data == colName){
													if (!stringDatatypes.contains(val.datatype)) {
														if(val.datatype=="timestamp"){
															searchList['More than']= " > '<Q>'";
														}
														else{
														searchList['More than']= " > <Q>";
														}
														if(val.datatype=="timestamp"){
															searchList['Less than']= " < '<Q>'"
														}
														else{
															searchList['Less than']= " < <Q>"
														}
														if(val.datatype=="timestamp"){
															searchList['More or equal']= " >= '<Q>'";
														}
														else{
															searchList['More or equal']= " >= <Q>";
														}
														if(val.datatype=="timestamp"){
															searchList['Less or equal']= " <= '<Q>'";
														}
														else{
															searchList['Less or equal']= " <= <Q>";
														}
														if(val.datatype=="timestamp"){
															searchList['Range']= " between '<Q1>' and '<Q2>'";
														}
														else{
															searchList['Range']= " between <Q1> and <Q2>";
														}
														/*searchList['Less than']= " < <Q>";
														searchList['More or equal']= " >= <Q>";
														searchList['Less or equal']= " <= <Q>";
														searchList['Range']= " between <Q1> and <Q2>";*/
														
													}
												}
											});
											if(config.isFilterNotapplicable){ 
												 searchList['Not Available'] = "N/A";
												 
											}
											if(!config.isFilterNotapplicable){ 
											searchList['Blank'] = "Blank";
											}
											if(!config.isFilterNotapplicable){
											searchList['Exclude Blank'] = "Exclude Blank";
											}
											//searchList['N/A'] = "N/A";
											searchList['Clear Filter'] = "Clear Filter";
											searchList['Clear all Filters'] = "Clear all Filters";
											var containval = "like '%<Q>%'"
											var optionContent = '<option value= "' +searchList[i]+ '">Contains</options>'
											for (var i in searchList){
												if(i == 'Clear Filter')
													optionContent = optionContent + '<option style="font-size: 0.05em; background-color:#809fff;" disabled=""></option>'
												optionContent = optionContent + '<option value= "' +searchList[i]+ '">'+i+'</options>'
											}
											if (this.visible())
											{	
												var column = this;
												var title = $(this.header()).text();
												
												var columnName = this.dataSrc();
												var columnType = "text";
												var stringDatatype = [ "string", "character varying", "varchar",
													"character", "char", "text" , "longtext"];
												$.each(config.columns, function(index, val){
													if(val.data == columnName){
														if(!stringDatatype.contains(val.datatype) && val.datatype != 'timestamp'){
															 columnType = "number";
														}
													}
												})
												
												if (columnName === null && title === "Audit Trail"){
													$(columnSearchTR).append($("<th/>"));
												}
												else if (columnName === null && title === "Action"){
													$(columnSearchTR).append($("<th/>"));
												}
												else{
													//$(columnSearchTR).append($("<div/>").append($('<input/>')))
													$(columnSearchTR).append($("<th/>").append(distinctFilterDiv)
															.append($('<select/>').addClass('serchContain'+columnName+'').attr('id', 'serchContain').css({'background-color':'#eee','border':'0px solid #eee','float':'left', 'font-size':'11px','max-width': '125px','color':'color: #444','width':'100%'}).on('change',function(){
														var scrollLeft = $(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft();
														$(config.html).data("scrollleft",scrollLeft);
														var columnType = "text";
														var stringDatatype = [ "string", "character varying", "varchar",
															"character", "char", "text" , "longtext"];
														$.each(config.columns, function(index, val){
															var columnList = $(columnSearchTR).find('input');
															for(var x = 0; x< columnList.length; x++){
																var columnName = $($(columnSearchTR).find('input')[x]).attr('column')
																if(val.data == columnName){
																	if(!stringDatatype.contains(val.datatype) && val.datatype != 'timestamp'){
																		$($(columnSearchTR).find('input')[x]).attr('type','number')
																	}
																}
															}
															
														})
														$(this).parent().parent().prevObject.find('input').keyup();
														if($(this).parent().parent().prevObject.find('select').val() == " between <Q1> and <Q2>"){
															$(this).parent().parent().prevObject.find('input').attr('type','text')
															$(this).parent().parent().prevObject.find('input').val("0 to 0")
														}
															
										        	}).append(optionContent)).append($('<input placeholder=" &#xF002; " class="fontAwesome"/>').on( 'keyup', function () {
														var searchValue = $(this).val();
														var clearall = $($(this).parent().parent().prevObject.find('select')[1]).val()
														var scrollLeft = $(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft();
														$(config.html).data("scrollleft",scrollLeft);
														if(clearall == 'Clear all Filters'){
															$($(this).parents("tr")[0]).find("input.fontAwesome").each(function(){
																	$(this).val("")
																	filterval = "like '%<Q>%'"
																	$(this).parent().parent().prevObject.find('select').find('option:contains("Contains")').prop('selected',true);
																	$(this).parent().parent().prevObject.find('select').find("option[selected]").val("like '%<Q>%'")
															})

														}
														localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";
														$($(this).parents("tr")[0]).find("input.fontAwesome").each(function(){
															var searchFilter = "";
															var filterval =$($(this).parent().parent().prevObject.find('select')[1]).val()
															if(filterval == 'Blank'){ 
																   if(config.isFilterNotapplicable){ 
																	$(this).attr('type','text')
	 																$(this).val(" ");
																	filterval = "=''"
																	
																	filterval = "=''"	
																   }
																   else{
																	   $(this).attr('type','text')
																	   $(this).val(" ");
																		filterval = "=''"	
																   }
	 															}
															
															if(filterval == 'Exclude Blank'){
																$(this).attr('type','text')
																$(this).val(" ");
																filterval = "!=''"
															}
																
															if(filterval == 'N/A'){
																if(config.isFilterNotapplicable){
															    $(this).attr('type','text')
																$(this).val(" ");
																filterval = "=''"
															}
															}
															if(filterval == 'Clear Filter'){
																searchFilter = ""
																$(this).val("")
																filterval = "like '%<Q>%'"
																$(this).parent().parent().prevObject.find('select').find('option:contains("Contains")').prop('selected',true);
																$(this).parent().parent().prevObject.find('select').find("option[selected]").val("like '%<Q>%'")
															}
															if ($(this).val() && $(this).val() !== "0 to 0"){ 
																	var searchFilter =$(this).val().toLowerCase();
																	if(filterval == " between <Q1> and <Q2>"){
																		var selectedRange = $(this).val().toLowerCase().split('to');
																		var startRange = parseInt(selectedRange[0])
																		var endRange = 	parseInt(selectedRange[1])
																	}

																	if($(this).val() && filterval == "undefined")
																		filterval = "like '%<Q>%'"
																	if(filterval.trim() === "> <Q>" || filterval.trim() === "< <Q>" || filterval.trim() === ">= <Q>" || filterval.trim() === "<= <Q>" || filterval === " between <Q1> and <Q2>"){
																		if(filterval && filterval  === " between <Q1> and <Q2>"){
																			if(startRange && endRange){
																				searchFilter = filterval.replace('<Q1>', startRange);
																				searchFilter = searchFilter.replace('<Q2>', endRange);
																			}else{
																				searchFilter = " between 0 and 0"
																			}
																		}
																		if(filterval && filterval  !== " between <Q1> and <Q2>")
																			
																			searchFilter = filterval.replace('<Q>', searchFilter);
																		
																		localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += " AND "+addTildInColumnName($(this).attr("column"))+" "+searchFilter;
																	}else{
																		if(filterval)
																			searchFilter = filterval.replace('<Q>', searchFilter);
																		localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += " AND LOWER(COALESCE(NULLIF(CAST("+ addTildInColumnName($(this).attr("column")) + " AS "+stringAlias+" ),''),'')) "+searchFilter+"";
																	}
															}
															
														});
														dataTableInstance.settings()[0].jqXHR.abort();
														if(!config.data.serverType || config.data.serverType === "serverSide"){
															/*dataTableInstance.ajax.reload(
																	function(){
																		var scrLeft = $(config.html).data("scrollleft");
																		console.log(scrLeft);
																		$(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft(scrLeft);
																	},false);*/
															
															setTimeout( function () {
																var scrLeft = $(config.html).data("scrollleft");
																dataTableInstance.ajax.reload(function() {
																	console.log(scrLeft);
																	$(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft(scrLeft);
															    },false);
															},1000 );
															
															
														}else{
															column.search( searchValue ? '^'+searchValue.toLowerCase().replace(/'/g, "\\'") : '', true, false ).draw();
														}
													
											        } ).css({"border-radius": "4px"}).attr({"column":columnName,"type":columnType})));
												
												}
												
											}
										console.log(config.data)
										console.log(colName)
										function on(colName){
											/*console.log($('.distinctFilter'+colName).find('button')[0].title)
											 var filterValue = [];
										     filterValue.push($('.distinctFilter'+colName).find('button')[0].title);
										     dataTableInstance.search(filterValue).draw();*/
											
											
//											$('[id*="checkbox"]:checked').map(function() {
//											return $(this).val().toString(); 
//										    } ).get().join(",");
										
//										     var vals = $('input[type=checkbox]:checked').map(function(index, element) {
//									            return $.fn.dataTable.util.escapeRegex($(element).val());
//									          }).toArray().join('|');
//											dataTableInstance
//								            .search(vals.length > 0 ?  vals  : '', true, false)
//								            .draw();

											var checkboxValues = [];
										   localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";
										$.each(config.columns, function(index, val){
											var filter=('.distinctFilter'+val.data)
												
										//$(''+filter+' :checkbox:checked').map(function() {
											if($(''+filter+' :checkbox:checked').length>0){
												checkboxValues = [];
											 for(var i=0;i<$(''+filter+' :checkbox:checked').length;i++){
										            checkboxValues.push($(''+filter+' :checkbox:checked')[i].value);
										            console.log(checkboxValues)
											 }
										    var searchValue = $('.distinctFilter'+colName).find('button')[0].title
											var filterQuery = "";
											filterQuery = "1=1";
											filterQuery += " AND (";
											var operator='OR'
											var	searchtype='like'
										 
											if(checkboxValues.length===0){
												localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";	
											}
											else{
											for(var x=0;x<checkboxValues.length;x++){
											
												if(x === 0){
													//filterQuery+= addTildInColumnName(colName)+" "+searchtype + " '%" + checkboxValues[x] + "%'";
												   localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += "AND ("
												   localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += addTildInColumnName(val.data)+" "+searchtype + " '%" + checkboxValues[x] + "%'";
												  /* if(checkboxValues.length===1){
													   localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += ") AND 1=1"  
												   }*/
												}
												else {              
													  localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += operator +" "+ addTildInColumnName(val.data)+" "+searchtype + " '%" + checkboxValues[x] + "%'";
											    }
												
											}	
											localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] +=")"
											  /* if(checkboxValues.length>1){
												 localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += ") AND 1=1"
									           }*/
											 
											}
											
										
											 }
										//})
										})
											localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += "AND 1=1"  
										dataTableInstance.settings()[0].jqXHR.abort();
										if(!config.data.serverType || config.data.serverType === "serverSide"){
											dataTableInstance.ajax.reload();
										}else{
											column.search( searchValue ? '^'+searchValue.toLowerCase().replace(/'/g, "\\'") : '', true, false ).draw();
										}
									
										if(checkboxValues.length===0){
											 localStorage["datatablecolumnfilter_"+config.html.replace("#",'')]="1=1"
											dataTableInstance.ajax.reload();
										}
										
								}
										
									
										var lookupquery = "select distinct "+colName+", '' from "+config.data.table+" where lower("+colName+") like '%<Q>%' LIMIT 10";
										//var lookupquery ="select distinct "+colName+" from "+config.data.table+" where lower("+colName+") like '%<Q>%' order by 1"
										console.log(lookupquery)
										 constructDropDown(distinctFilterDiv,lookupquery,true,'',on,'' ,config.data.source,config.data.database,colName)
										 	var dropdownWidth=0
										 $.each(config.columns, function(index, val){
										 		var dropdownDiv=('.distinctFilter'+val.data)
										 		console.log($(dropdownDiv).find('button').parent().parent().parent().width())
										 		
										 		
										 			console.log(dropdownWidth)
										 			if(index===0){
										 				$(dropdownDiv).find("ul").css({"position":"fixed","margin-top":"128px","width":"17%"})
														$("#content").css("min-height","600px")
														//$(".dataTables_info").css("display","none")
														$("#wid-id-datatable_1-temp").css("z-index","1")
														
										 			}
										 			else{
										 			$(dropdownDiv).find("ul").css({"position":"fixed","margin-top":"128px","width":"17%","margin-left":dropdownWidth})
													$("#content").css("min-height","600px")
														//$(".dataTables_info").css("display","none")
														$("#wid-id-datatable_1-temp").css("z-index","1")
										            }
											dropdownWidth+=	$(dropdownDiv).find('button').parent().parent().parent().width()
										    })
										     
										});
										}
										//constructDropDown(distinctFilter)
									}
									/*if(dataTableInstance && dataTableInstance.columns && $(config.html +"_wrapper").find(".columnSearch").length === 0 ){
										var columnSearchTR = $("<tr/>").addClass("columnSearch");
										$(nHead).parent().append(columnSearchTR);
										dataTableInstance.columns().every( function () {
											if (this.visible())
											{
												var column = this;
												var title = $(this.header()).text();
												var columnName = this.dataSrc();
												if (columnName === null && title === "Audit Trail"){
													$(columnSearchTR).append($("<th/>"));
												}else{
													$(columnSearchTR).append($("<th/>").append($('<input placeholder=" &#xF002; " class="fontAwesome" />').on( 'keyup', function () {
														var searchValue = $(this).val();
														localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";
														$($(this).parents("tr")[0]).find("input").each(function(){
															if ($(this).val()){ var searchFilter =$(this).val().toLowerCase();
																localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += " AND LOWER(COALESCE(NULLIF(CAST("+ addTildInColumnName($(this).attr("column")) + " AS "+stringAlias+" ),''),'')) like '%"+searchFilter.replace(/'/g, "\\'")+"%'";
															}
														});
														dataTableInstance.settings()[0].jqXHR.abort();
														if(!config.data.serverType || config.data.serverType === "serverSide"){
															dataTableInstance.ajax.reload();
														}else{
															column.search( searchValue ? '^'+searchValue.toLowerCase().replace(/'/g, "\\'") : '', true, false ).draw();
														}
											        } ).css({"border-radius": "4px"}).attr({"column":columnName,"type":"text"})));
												}
											}
										});
										
									}*/
									// adding condition for fixed col
									else if( $(config.html +"_wrapper").find(".DTFC_LeftWrapper").find(".DTFC_LeftHeadWrapper").length > 0 && $(config.html +"_wrapper").find(".columnSearch").length ===2 ){
										var columnSearchTR = $("<tr/>").addClass("columnSearch"); 
										var distinctFilterDiv = $("<div/>").addClass("distinctFilter"); 
										setTimeout(function(){
											$(config.html +"_wrapper").find(".DTFC_LeftHeadWrapper").find("thead tr[role=row]").css("height","auto");
											$(config.html +"_wrapper").find(".DTFC_LeftHeadWrapper").find("thead").append(columnSearchTR);
											var len=$(config.html +"_wrapper").find(".DTFC_LeftHeadWrapper").find("thead").find("th").length;
											dataTableInstance.columns().every( function (){
												var column = this;
												var title = $(this.header()).text();
												var columnName = this.dataSrc();
												if(len>0){
													$(columnSearchTR).append($("<th/>").append($("<input/>").on( 'keyup', function () {
														var searchValue = $(this).val();
														localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] = "1=1";
														$($(this).parents("tr")[0]).find("input").each(function(){
															if ($(this).val()){
																localStorage["datatablecolumnfilter_"+config.html.replace("#",'')] += " AND LOWER(COALESCE(NULLIF(CAST("+ $(this).attr("column") + " AS "+stringAlias+" ),''),'')) like '%"+searchValue.toLowerCase().replace(/'/g, "\\'")+"%'";
															}
														});
														dataTableInstance.settings()[0].jqXHR.abort();
														if(!config.data.serverType || config.data.serverType === "serverSide"){
															dataTableInstance.ajax.reload(
																	function(){
																		var scrLeft = $(config.html).data("scrollleft");//alert(scrLeft);
																		$(config.html+"_wrapper").find(".dataTables_scrollBody").scrollLeft(scrLeft);
																	},false);
														}else{
															column.search( searchValue ? '^'+searchValue.toLowerCase().replace(/'/g, "\\'") : '', true, false ).draw();
														}
												   }).attr({"placeholder": "Search "+title,"column":columnName})));
												  len--;
												}
									    });
									    },300);
									 }
								}
								if($(nHead).parents("[id^='datatable']").parent().find("[name='infodiv']").length>0 && $(nHead).find("i").length === 0){
									var data = $(nHead).parents("[id^='datatable']").parent().find("[name='infodiv']").data();
									$(nHead).find("th").each(function(index){
										var attribute = {};
										if(data.columns[index].columndesc){
											attribute["data-content"] = data.columns[index].columndesc;
											attribute["data-placement"] = "top";
											attribute["data-toggle"] = "popover";
											attribute["data-html"] = true;
										}
										var val = $(this).html();
										$(this).html($("<div/>").css({"display":"inline-flex"}).html(val)
											.prepend(data.columns[index].columninfo?$("<i/>").attr(attribute)
											.css({"margin":"4px 5px 0px 0px"}).addClass("fa fa-info-circle txt-color-blue")
										.unbind('click').bind('click',data.columns[index],function(e){
											e.preventDefault();
											e.stopPropagation();
											getColumnInformation(e.data,$(this).parents("[id^='datatable']").next(),{source:config.data.source,database:config.data.database,table:config.data.table});
										}):""));
									});
									$(nHead).find("th").find("i").popover({
							    		  placement: 'auto',
							    		  container: 'body',
							    		  delay :  { "show": 100, "hide": 100 },
							    		  trigger : 'hover'
							    	  });
								}
							  },
							"fnRowCallback" : function(nRow, aData,rowIndex) { 
								if (config.data.drilldowncallback && config.data.drilldowncallback !== null && config.data.drilldowncallback !== "") {
									$('td', nRow).attr({'onClick' : "javascript:dataTableDrillDown."+ config.data.drilldowncallback+ "(this)"});
									$(nRow).data("request",JSON.stringify(aData));
									if(config.data.pivotSettings) {
										$(nRow).data("dataSettings",JSON.stringify(config.data));
										$(nRow).data("columnSettings",JSON.stringify(config.columns));
									} else if(config.data.rule) {
										$(nRow).data("rule",JSON.stringify(config.data.rule));
									}
								}else if (config.editType === "form" || config.editType === "inline" || config.editType === "actionForm"){
									$(nRow).data("request",JSON.stringify(aData));
									$(nRow).data("columnSettings",JSON.stringify(config.columns));
								}
								else if(config.data.isSelectAll){
									$(nRow).data("request",JSON.stringify(aData));
								}
								if (primaryKeys && primaryKeys.length > 0){
									for (var p = 0; p < primaryKeys.length; p++){
										primaryKeys[p].newValue =aData[primaryKeys[p].fieldName];
									}
									$(nRow).data("primarykeys",JSON.stringify(primaryKeys));
								}
							},
							"drawCallback": function (osettings) {
								var selectedObj = $($(this).children('tbody').children('tr')[0]).data().request,
									columnForDiff = '', columnValuesForDiff = [];
								if(selectedObj){
									rowObj = JSON.parse(selectedObj);
									if(rowObj['allowBackgroundColor']){
										$(this).removeClass('table-hover table-striped');
										columnNameForDiff = rowObj['columnNameForDiff'];
										columnValuesForDiff = rowObj['columnValuesForDiff'];
										$(this).children('tbody').children('tr').each(function (index) {
											var rowElmnt = this;
											$(rowElmnt).find('td').css('cursor', 'default');
											if(JSON.parse($(rowElmnt).data().request)[columnNameForDiff]) {
												var value = JSON.parse($(rowElmnt).data().request)[columnNameForDiff].toString().toLowerCase();
												columnValuesForDiff.forEach(function(obj, index){
													if(obj.name.toString().toLowerCase() === value) {
														$(rowElmnt).css({'background': obj.color, 'color': '#fff'});
													}
												});
											}
										});
									}
								}
								
							}
						});
		try{
			if(profileAnalyser && profileAnalyser.dataTable && dataTableInstance.context && dataTableInstance.context.length>0 && dataTableInstance.context[0].sTableId){
				profileAnalyser.dataTable[dataTableInstance.context[0].sTableId] = dataTableInstance;
			}
		}catch(ex){
			console.log(ex);
		}
		this.dataTable.push(dataTableInstance);	
		if (config.resetHandler) {
			$(config.resetHandler).on('click', function(e) {
				e.preventDefault();
				this.resetGraphs();
			}.bind(this));
		}
		setTimeout(function(){
			console.log("0kk")
			$($.fn.dataTable.tables(true)).DataTable()
		      .columns.adjust();
		},3000)
		

	};
	function getColumnInformation(column,div,soucreInfo){
		var request = {
			source:soucreInfo.source,
			database:soucreInfo.database,
			table:soucreInfo.table,
			isDetails:true,
			column:column.data
		};
		enableLoading();
		callAjaxService("getcolumnInfo", function(response){callBackGetColumnInfo(response,div,column)},callBackFailure, request, "POST");
	}
	function callBackGetColumnInfo(response,div,column){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			div.addClass("activate").removeClass("hide");
			div.find("legend").find("span").html(column.title+" Details");
			var chartDiv = $("<div/>").attr({"id":"columnchartdiv"+div.prev().attr("id")});
			div.find("section").html('').append($("<div/>").addClass("panel-group acc-v1 margin-bottom-40")
					.append(column.columndesc?$("<label/>").addClass("").append("<strong>Description : </strong><div>"+column.columndesc+"</div>"):'')
					.append($("<label/>").addClass("").append("<strong>No Of Records :</strong> <b class='badge  bounceIn animated bg-color-greenLight'>"+response.noOfRecords+"</b>"))
					.append($("<label/>").addClass("")
							.append("<strong>Distinct : </strong><b class='badge  bounceIn animated bg-color-greenLight'>"+response.distinct+"</b>"))
					.append(response.isNumaric?$("<label/>").addClass("")
							.append("<strong>Min : </strong><b class='badge  bounceIn animated bg-color-greenLight'>"+response.min+"</b>"):"")
					.append(response.isNumaric?$("<label/>").addClass("")
							.append("<strong>Max : </strong><b class='badge  bounceIn animated bg-color-greenLight'>"+response.max+"</b>"):"")
					.append(response.isNumaric?$("<label/>").addClass("")
							.append("<strong>Average :</strong> <b class='badge  bounceIn animated bg-color-greenLight'>"+response.avg.toFixed(2)+"</b>"):"")		
					.append(response.isNumaric?$("<label/>").addClass("")
							.append("<strong>Median : </strong><b class='badge  bounceIn animated bg-color-greenLight'>"+response.median+"</b>"):"")
					.append(response.isNumaric?$("<label/>").addClass("")
							.append("<strong>Standard Deviation : </strong><b class='badge  bounceIn animated bg-color-greenLight'>"+response.stdDeviation.toFixed(2)+"</b>"):"")
					.append($("<div/>").addClass("panel panel-default").append(chartDiv)));
			var data = response.columnDetails;
			var ndx = crossfilter(data);
			var dimFun = new Function('d', "return d['key'] || 'Others' ;");
			var dim = ndx.dimension(dimFun);
			var grp = dim.group().reduce(function(p,v){return v;},function(){},function(){});
			var element = "#columnchartdiv"+div.prev().attr("id");
			var chart = dc.rowChart(element);
			chart.width($(element).parent().width()).height((data.length*20)+20).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(grp).dimension(dim).valueAccessor(function(d){
				return d.value.count;
			}).title(function (d) { 
				return d.key+" : "+d.value.count; 
				})
			.elasticX(true).xAxis().tickFormat(d3.format("~s")).ticks(4);
			chart.ordering(function(a) {
				return a.value.count *-1;
			});
			chart.render();
		}
	}
	Graphing.prototype.drawTimeLineChart = function(config) {
		var THIS = this;
		var dateFormat = d3.timeFormat("%m/%d/%Y");
		var tlac = dc.lineChart(config.lineHTML);
		var tlc = dc.barChart(config.barHTML);
		var tldFun;
		var minDate;
		var maxDate;
		var xScaleRound;
		var xScaleUnits;
		var timelineDimension;
		if (config.timelinetype === "days") {
			tldFun = new Function('d', 'return d["date-' + config.field + '"];');
			timelineDimension = this.ndx.dimension(tldFun);
			minDate = timelineDimension.bottom(1)[0]["date-" + config.field];
			maxDate = timelineDimension.top(1)[0]["date-" + config.field];
			xScaleRound = d3.timeMonth.round;
			xScaleUnits = d3.timeMonths;

		} else {
			tldFun = new Function('d', 'return d["month-' + config.field + '"];');
			timelineDimension = this.ndx.dimension(tldFun);
			minDate = timelineDimension.bottom(1)[0]["month-" + config.field];
			maxDate = timelineDimension.top(1)[0]["month-" + config.field];

			xScaleRound = d3.timeDay.round;
			xScaleUnits = d3.timeDays;
		}
        
		if(maxDate){
			maxDate.setMonth(maxDate.getMonth());	
		}
		if(minDate){
			minDate.setMonth(minDate.getMonth());
		}
		var fields = [];
		var calculatedField = [],labels = [];
		if (config.calculatedfield && config.calculatedfield !== null
				&& $.trim(config.calculatedfield) !== "") {
			calculatedField = config.calculatedfield.split(",");
		}
		fields.push(config.group);
		fields = $.merge(fields, calculatedField);
		if(config.colorfield){
			labels.push(config.colorfield);
		}
		var grp = timelineDimension.group().reduce(
				this.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue), this.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
				this.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
		var aggField;
		if (config.aggregate === "calculated"){
			aggField = config.calculatedformula;
		}else{
			aggField =config.group;
		}
		var grpAggre = this.getValueByAgrregate(config.aggregate, aggField,config.filtervalue,config.absolute);
		var tipLine = d3.tip().offset([-10, 0]).attr('class', 'd3-tip').html(function(e){
			return THIS.getTitleAgrregate(e.data,config,dateFormat);
		});
		var barWidth;
		if (config.chartwidth.toLowerCase() === "auto") {
			barWidth = parseInt($(config.html).width());
		} else {
			barWidth = parseInt(config.chartwidth || 180);
		}
		var barHeight;
		if (config.chartheight.toLowerCase() === "auto") {
			barHeight = parseInt($(config.html).width());
			barHeight = barHeight > this.defaultHeight ? this.defaultHeight
					: barHeight;
		} else {
			barHeight = parseInt(config.chartheight);
		}
		if(config.hidechart){
			if(config.hidechart === 'barChart'){
				$(config.barHTML).hide();
			}else if(config.hidechart === 'lineChart'){
				$(config.lineHTML).hide();
			}
		}
		if(config.hideyaxisline && config.hideyaxisline === 'yes'){
			$(config.lineHTML).addClass('no-yaxis');
		}
		if(config.hideyaxisbar && config.hideyaxisbar === 'yes'){
			$(config.barHTML).addClass('no-yaxis');
		}
		if(config.autoplay && config.autoplay === 'yes'){
			config.maxDate = maxDate;
			config.minDate = minDate;
			$(config.barHTML).parent().next().hide();
			if(!config.defaultFiltervalueFrom){
				config.defaultFiltervalueFrom = getFormattedDate(minDate);
			}
			if(!config.defaultFiltervalueTo){
				config.defaultFiltervalueTo = getManipulatedDate(config.autoplaytype,config.playunit,getFormattedDate(config.minDate));
			}
			$(config.barHTML).prepend($("<a/>").attr({"name":"autoplay"}).data("playmode",false).css({"position":"absolute","top":"25%"})
					.addClass("btn btn-primary btn-circle btn-lg fa fa-play cursor-pointer").unbind('click').bind('click',function(){
				$(this).toggleClass("fa-play fa-pause").data("playmode",!$(this).data("playmode"));				
				if($(this).data("playmode")){
					startPlay();
				}else{
					clearInterval(autoplay);
				}
			}));
		}
		var autoplay;
		function startPlay(){
			if(tlc.filter() && tlc.filter()[1] && config.maxDate <= tlc.filter()[1]){	
				tlc.filter(null);
				tlc.filter(dc.filters.RangedFilter(config.minDate,new Date(getManipulatedDate(config.autoplaytype,config.playunit,getFormattedDate(config.minDate)))));
			}
			autoplay = setInterval(function(){
				var from = getFormattedDate(tlc.filter()[0]);				
				var to = getFormattedDate(tlc.filter()[1]);
				if(config.maxDate <= tlc.filter()[1]){
					$(config.barHTML).find("a[name='autoplay']").addClass("fa-play").removeClass("fa-pause").data("playmode",false);
					clearInterval(autoplay);
					return;
				}
				from = getManipulatedDate(config.autoplaytype,config.playunit,from);
				to = getManipulatedDate(config.autoplaytype,config.playunit,to);
				tlc.filter(null);
				tlc.filter(dc.filters.RangedFilter( new Date(from),new Date(to)));
			}, config.animation);
		}
			tlac.renderArea(true).width(barWidth).height(barHeight)
			.transitionDuration(1000).margins(
					{
						top : 20,
						left : 60,
						right : 10,
						bottom : config.xaxisorientation
								|| config.xaxislabel ? 60 : 20,
					}).dimension(timelineDimension).mouseZoomable(true)
			.rangeChart(tlc)
			.x(d3.scaleTime().domain([ minDate, maxDate ])).round(
					xScaleRound).xUnits(xScaleUnits)
			.renderHorizontalGridLines(true).brushOn(false).group(grp,
					"Vendor Count").valueAccessor(grpAggre).title(function () { return ""; }).xAxisLabel(
					config.xaxislabel ? config.xaxislabel : '').yAxisLabel(
					config.yaxislabel ? config.yaxislabel : '')
				.on('filtered', function() {
				resetAllCharts(THIS);
				});
		tlac.yAxis().ticks(10).tickFormat(d3.format("~s"));
		tlc.width(barWidth).height(barHeight / 10 * 7).margins({
			top : 20,
			left : 60,
			right : 10,
			bottom : config.xaxisorientation || config.xaxislabel ? 60 : 20,
		})
		
		.xAxisLabel(config.xaxislabel ? config.xaxislabel : '').yAxisLabel(
				config.yaxislabel ? config.yaxislabel : '').dimension(
				timelineDimension).group(grp, "Vendor Count").valueAccessor(
				grpAggre).title(function () { return ""; })/* .group(timelineGroup) */
				
		.centerBar(true).gap(1).x(d3.scaleTime().domain([ minDate, maxDate ]))
		// .filter(dc.filters.RangedFilter( new
		// Date(config.defaultFiltervalueFrom),new
		// Date(config.defaultFiltervalueTo)))
				.round(xScaleRound).alwaysUseRounding(true).elasticY(false)
				.xUnits(xScaleUnits)/* .title(config.title) */;
		tlc.yAxis().ticks(10).tickFormat(d3.format("~s"));
		
		if (config.colorcodes) {
			var keyColorCodes;
			keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
			tlc.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
			tlc.colorAccessor(function(d) { 
			 	return d.key;
			});
		}		
		
		$(".reset", $(config.html)).on('click', function() {
			tlac.filterAll();
			tlc.filterAll();
		}.bind(this));
		this.drawnGraph.push(tlac);
		this.drawnGraph.push(tlc);
		tlac.render();
		tlc.render();
		if(config.defaultFiltervalueFrom && config.defaultFiltervalueTo)
			tlc.filter(dc.filters.RangedFilter( new Date(config.defaultFiltervalueFrom),new Date(config.defaultFiltervalueTo)));
		tlac.on('filtered', function() {
			resetAllCharts(THIS);
		}.bind(this));
		if (viewSettings && viewSettings.length > 0){
			$.each(dataAnalyser.viewSettings,function(){
				if (this.anchorName ===  "timeline-"+config.field && this.filterValues && this.filterValues[0]){
					config.defaultFiltervalueFrom = this.filterValues[0][0];
					config.defaultFiltervalueTo = this.filterValues[0][1];
					return;
				}
			});
			tlc.filter(dc.filters.RangedFilter( new Date(config.defaultFiltervalueFrom),new Date(config.defaultFiltervalueTo))); 
		}
		/*
		 * d3.selectAll(config.lineHTML).selectAll(".dot").call(tipLine);
		 * d3.selectAll(config.lineHTML).selectAll(".dot").on('mouseover',
		 * tipLine.show);
		 */
	};

	Graphing.prototype.drawCompositeChart = function(config) {
	var THIS = this;
	$(config.html).parent().find('[name=transposeTable]').empty();
	var dimFun = new Function('d', "return d['" + config.field + "'] || 'Others' ;");
	var dim1 = this.ndx.dimension(dimFun);
	$(".reset", $(config.html)).data('isReset',true);
	$(config.html).data('chartReseted',false);
	$(".axisLabel", $(config.html)).attr({
		'data-numformat':config.numFormat,
		"data-prefix":config.prefix,
		"data-showpercentage":config.showpercentage,
		"data-aggregate":config.aggregate,
		"data-calculatedformula":config.calculatedformula,
		"data-suffix":config.suffix
		});
	var barWidth;
	if (config.chartwidth.toLowerCase() === "auto") {
		barWidth = parseInt($(config.html).width());
	} else {
		barWidth = config.chartwidth;
	}
	var barHeight;
	if (config.chartheight.toLowerCase() === "auto") {
		barHeight = parseInt($(config.html).width());
		barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
				: barHeight;
	} else {
		barHeight = config.chartheight;
	}
	config.legendxposition = barWidth * config.legendxposition;
	config.legendyposition = barHeight * config.legendyposition;
	if (config.filtervalues && $(config.html).find('select').length === 0) {
		this.constructSelect(config, dim1);
		$(config.html).find('select').unbind('change').bind('change',
				function() {
					config.filterdefaultval = $(this).val();
					renderCompositeChart(config, dim1);
				});
		if(config.filterfrom === "bottom"){
			$(config.html).find('input').attr("checked","checked");
		}
		$(config.html).find("input").unbind('click').bind("click",function(){
			if($(this).is(":checked")){
				config.filterfrom = "bottom";
			}else{
				config.filterfrom = "top";
			}
			renderCompositeChart(config, dim1);
		});
	} else {
		$(config.html).find('select').val(config.filterdefaultval);
		if(config.filterfrom === "bottom"){
			$(config.html).find('input').attr("checked","checked");
		}
	}
	function renderCompositeChart(config, dim) {
		try {
			if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
				for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
					if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
						dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
					}
				}
			}
			var composite = dc.compositeChart(config.html);
			var fields = [];
			var stacks = [];
			if (config.charts && config.charts !== null
					&& config.charts.length > 0) {
				for ( var x = 0; x < config.charts.length; x++) {
					if(config.charts[x].type === 'stack'){
						stacks = config.charts[x].group.split(",");
						THIS.stackMeasures= stacks;
					}else if (fields.indexOf(config.charts[x]["group"]))
					{
					fields.push(config.charts[x]["group"]);
					}
				}
			};
			var calculatedField = [],labels = [];
			if (config.calculatedfield && config.calculatedfield !== null
					&& $.trim(config.calculatedfield) !== "") {
				calculatedField = config.calculatedfield.split(",");
			}
			
			if(stacks.length >0){
				
				fields = $.merge(fields, stacks,calculatedField);
			}else{
				fields = $.merge(fields, calculatedField);
			}
			if(config.colorfield){
				labels.push(config.colorfield);
			}
			var grp = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
					THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
					THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
			if (config.filtertype) {
				var defaultVal = config.filterdefaultval;
				if (config.filtertype === "percentage") {
					defaultVal = Math.round(grp.size()
							* (config.filterdefaultval / 100));
				}
				grp.all().sort(THIS.sortGroup(config.group, config.aggregate,config.filterfrom === "bottom"?0:1));
				composite.data(function(group) {
					return group.top(defaultVal);
				});
			}
			
			if(config.defaultFiltervalue){
				var defaultFiltervalue = config.defaultFiltervalue.split(",");
				defaultFiltervalue.forEach(function(d) { 
					composite.filter(d);
				});
			}	

			if(config.runningtotal ==='yes'){
					grp = accumulate_group(grp);	
			}
			
			function sel_stack(i,x) {
				return function(d) {
					var stackValue;
					if (config.charts[x].aggregate.toLowerCase() === "sum"){
						stackValue = d.value[i];
					}else if (config.charts[x].aggregate.toLowerCase() === "count"){
						stackValue = d.value.count;
					}else{
						stackValue = d.value[i] / d.value.count;
					}
					return stackValue;
				};
			}
			
			composite.title(function () { return ""; }).margins({
				top : config.margintop,
				left : config.marginleft,
				right : config.marginright,
				bottom : config.marginbottom
			}).renderHorizontalGridLines(true)
			        .renderVerticalGridLines(true)
    ._rangeBandPadding(1) // workaround to fix visual stuff (dc js issues)


					.xAxisLabel(config.xaxislabel ? config.xaxislabel : '')
					.yAxisLabel(config.yaxislabel ? config.yaxislabel : '')
					.group(grp).brushOn(false)
					.elasticY(true)
					.width(barWidth).height(barHeight)
					._rangeBandPadding(1)
					.x(d3.scaleOrdinal()).xUnits(dc.units.ordinal);
			var charts = [];
			var keyColorCodes,colors;
			if (config.colorcodes) {					
				keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
			}
			if (config.charts && config.charts !== null
					&& config.charts.length > 0) {
				for ( var x = 0; x < config.charts.length; x++) {
					if(keyColorCodes){
						colors = d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes));
					}else{
						colors = config.charts[x].color;
					}
					if (config.charts[x].type && config.charts[x].type === "bar") {
						charts.push(dc.barChart(composite).dimension(dim)
								.gap(3).centerBar(true).colors(config.charts[x].color)
								.group(grp,x.toString())
								.colorAccessor(function(d) { 
									return d.key;
								})
								.valueAccessor(THIS.getValueByAgrregate(config.charts[x].aggregate,config.charts[x].group,config.filtervalue,config.absolute)));							
					}else if (config.charts[x].type && config.charts[x].type === "stack") {
						var stackTotal = {};
						var bar = dc.groupedBarChart(composite).dimension(dim).group(grp,stacks[0]).renderType(config.charts[x].type)
						.centerBar(true).gap(13).valueAccessor(THIS.getValueByAgrregate(config.charts[x].aggregate,stacks[0],config.filtervalue,config.absolute));
						
						for ( var i = 1; i < stacks.length; i++) {
							var stack = stacks[i];
							bar.stack(grp, stack, sel_stack(stack,x));
						}
						charts.push(bar);
					} else {
						charts.push(dc.lineChart(composite).dimension(dim)
								.colors(config.charts[x].color).group(grp,x.toString())
										 .renderDataPoints(config.renderdatapoints)
										 .renderArea(config.renderarea)
										 .interpolate(config.interpolate)
								.valueAccessor(
										THIS.getValueByAgrregate(
												config.charts[x].aggregate,
												config.charts[x].group,config.filtervalue,config.absolute))).title(function(d) { return d.key + ': ' + d.value; });;
					}
				}
			}
			composite.compose(charts);
			composite.legend(dc.legend().x(config.legendxposition).horizontal(config.legendhorizontal).autoItemWidth(true).y(config.legendyposition).itemHeight(12)
					.gap(5).legendText(function (d) {
						var legendName;
						if (config.measurelabel && config.measurelabel[config.charts[d.name].group]){
							legendName = config.measurelabel[config.charts[d.name].group];
						}else if(config.charts[d.name] !=undefined){
							legendName = config.charts[d.name].name || config.charts[d.name].group;
						}else{
							legendName = d.name;
						}
						if(config.colorcodes && config.colorcodes[d.name]){
							$(this).prev().attr("fill",config.colorcodes[d.name]);
					    }
						return legendName;
				    }));
			if(config.orderby === "2" || config.orderby === "3"){
				config.maxKeyLength = 0;
				grp.all().map(function(o){
					if(config.maxKeyLength < o.key.length){
						config.maxKeyLength = o.key.length;
					}
				});						
			}
			composite.ordering(function(a) {
				return THIS.sortbyGroup(config, a);
			});
			if(config.showtable =='yes'){
				var drawTable = table_transpose(grp,fields,config.measurelabel);
				$(config.html).after(drawTable);
			}
			var tipLine;
			composite.yAxis().ticks(10).tickFormat(d3.format("~s"));// for
																	// formating
																	// numbers
																	// of y
																	// axis.
			charts.forEach(function(chart){
				
				composite.on("renderlet",function(chart){;
					if(config.colorcodes){
						chart.selectAll("rect.bar").attr("fill", function(d){
							  return config.colorcodes[d.layer];
							});
					}
					if (config.showtotal === "yes") {
						moveGroupNames(chart);
					}
					});
				
				chart.on('filtered', function(chart,value) {
					composite.filter(value);
					resetAllCharts(THIS, config.html);
					resetAllProfiles(arguments[0]);
					$(".reset", $(config.html)).data('isReset',true);
				}.bind(this));
				
				
			});
			
			if(config.showtable ==='yes'){

					composite.on("postRedraw", function(){
						$(config.html).parent().find('[name=transposeTable]').empty();
						var drawTable = table_transpose(grp,fields,config.measurelabel);
						$(config.html).after(drawTable);
					})
							
			}
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					$(config.html).data('chartReseted',true);
				charts.forEach(function(chart){
					chart.filterAll();
				});
				composite.filterAll();
				$(e.currentTarget).removeData("isReset");
				resetAllCharts(THIS);
				$(config.html).data('chartReseted',false);
			}.bind(this));
			THIS.drawnGraph.push(composite);
			
			var numberFormat = d3.format(",f");
				tipLine = d3.tip().offset([-10, 0]).attr('class', 'd3-tip').html(function(e) {
					var returnString;
					var keyName;
					var key;
					if (config.showonlybardata && config.showonlybardata === "yes"){
						key = e.x;
						returnString = "<table class='d3-tip-table'><tr><th class='d3-tip-header'>"+ key + "</th></tr>";
						keyName = config.measurelabel[e.layer]||e.layer;
						returnString += "<tr><td class='d3-tip-key'>"+keyName + " </td><td class='d3-tip-value'>"
						+ numberFormat(e.y) + "</td></tr></table>";
					}else{

						if($(this).attr("class").split(' ')[0] ==='dot' || $(this).attr("class").split(' ')[0] ==='bar'){
									if (config.charts[e.layer]){
										config.group =  config.charts[e.layer].group;
										config.aggregate =  config.charts[e.layer].aggregate;
									}
									return THIS.getTitleAgrregate(e.data,config);
						}else{
							var d = e.data;
							var allkeys = Object.keys(d.value);
					       var keys = $.grep(allkeys, function(element) {
					    return $.inArray(element, THIS.stackMeasures ) !== -1;
					});
							var keyValue;
							var totalvalue = 0;
							key = d.key;
							if(config.tooltipkey === "code"){
								key = d.key;
							}else if(config.tooltipkey === "description"){
								if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
									key = THIS.masterData[config.fieldname][key];
								}
							}else{
								if ($.trim(config.description) !== "" && THIS.masterData[config.fieldname][key]) {
									key = d.key+"-" + THIS.masterData[config.fieldname][key];
								}
							}
							returnString ="<table class='d3-tip-table'><tr><th class='d3-tip-header'>"+ key + "</th></tr>";
							for ( var x = 0; x < keys.length; x++) {
								if (config.aggregate === "sum")
									keyValue = d.value[keys[x]];
								else if (config.aggregate === "count")
									keyValue = d.value.count;
								else
									keyValue = d.value[keys[x]]
											/ d.value.count;
								
								if (config.measurelabel && config.measurelabel[keys[x]])
									keyName = config.measurelabel[keys[x]]||keys[x];
								else 
									keyName = keys[x];
								
								if (keyName !== "count" && keyValue !== 0){
									totalvalue+= keyValue;
									returnString += "<tr><td class='d3-tip-key'>"+keyName + " </td><td class='d3-tip-value'>"
											+ numberFormat(keyValue) + "</td></tr>";
								}
							}
							returnString += "<tr><td class='d3-tip-key'>Total</td><td class='d3-tip-value'>"
							+ numberFormat(totalvalue) + "</td></tr>";
							returnString += "</table>";}
					}
					if (config.linklabel && Object.keys(config.linklabel).length>0){
						returnString += "<hr class='no-margin-bottom'/>";
						if(config.linklabel[e.layer]){
							var actions = config.linklabel[e.layer].split(",");
							for (var i = 0;i<actions.length;i++){
								returnString += "<div class='tooltip-action' title='Go to "+ actions[i]+ "' onclick='dataAnalyser.tooltipActionClick(this)'>"+actions[i]+"</div>";
							}
						}
					}
					else if (config.linkaction){
						returnString += "<hr class='no-margin-bottom'/>";
						var actions = config.linkaction.split(",");
						for (var i = 0;i<actions.length;i++){
							returnString += "<div class='tooltip-action' title='Go to "+ actions[i]+ "' onclick='dataAnalyser.tooltipActionClick(this)'>"+actions[i]+"</div>";
						}
					}
					returnString += "<div class='tooltip-toolbar'><a class='tooltip-toolbar-action' onclick=System.filterChart('"+ config.html +"','"+key+"')><i class='fa fa-check txt-color-greenLight'></i> Keep Only</a><a class='tooltip-toolbar-action' onclick=System.filterChart('"+config.html+"','"+key+"',true)><i class='fa fa-times txt-color-red'></i> Exclude</a><\div>";
					return returnString;
				});	
			
			
			
			composite.render();
			applyDefaultFilter(config,composite);
			d3.selectAll(config.html).selectAll(".dot,.bar").call(tipLine);
			d3.selectAll(config.html).selectAll(".dot,.bar").on('mouseover', tipLine.show);
		} catch (e) {
			console.log('Error Creating composite Chart' + e);

		}
	}
	function moveGroupNames(chart) {
		var gLabels = chart.select(".labels");
		if (gLabels.empty()) {
			gLabels = chart.selectAll(".sub").select(".chart-body").append('g').classed(
					'labels', true);
		}
		var gLabelsData = gLabels.selectAll("text").data(chart.selectAll(".bar"));

		gLabelsData.exit().remove(); // Remove unused elements

		gLabelsData.enter().append("text"); // Add new elements

		gLabelsData
				.attr('text-anchor', 'middle')
				.attr('fill', 'white')
				.text(
						function(e) {
							var obj = d3.select(e).data()[0];
							var chartConfig = config.charts[obj.layer];
							var d = obj.data;
							var tempValue;
							
							if (chartConfig.aggregate.toLowerCase() === "sum")
								tempValue = d.value[chartConfig.group];
							else if (chartConfig.aggregate.toLowerCase() === "count")
								tempValue = d.value.count;
							else if (chartConfig.aggregate.toLowerCase() === "calculated")
								tempValue = eval(config.calculatedformula);
							else
								tempValue = d.value[chartConfig.group]
										/ d.value.count;
							var prefix = d3.formatPrefix(tempValue);// d3.select(d).data()[0].data.value
							var prefixScale = Math.round(prefix
									.scale(tempValue), 0);
							return prefixScale + prefix.symbol;
						}).attr(
						'x',
						function(d) {
							return +d.getAttribute('x')
									+ (d.getAttribute('width') / 2);
						}).attr('y', function(d) {
					return +d.getAttribute('y') + 15;
				}).attr('style', function(d) {
					if (+d.getAttribute('height') < 18)
						return "display:none";
				});

	}
	renderCompositeChart(config, dim1);
};

	Graphing.prototype.resetAllFilters = function() {
		var THIS = this;
		$(".dc-data-count").unbind("click").bind("click", function() {
			$('input[name=checkboxinline],[name=checkboxselectall]').prop('checked', false);
			resetAllCustomCharts(THIS);
		});
	};

	Graphing.prototype.drawHierarchie = function(config) {
		try {
			var THIS = this;
			var numberFormat = d3.format(config.numFormat);
			var dimFun = new Function('d', "return d['" + config.field + "'];");

			var dim = this.ndx.dimension(dimFun);

			if (config['level-1']) {
				var dim1 = this.ndx.dimension(function(d) {
					return d[config['level-1']];
				});
			}
			if (config['level-2']) {
				var dim2 = this.ndx.dimension(function(d) {
					return d[config['level-2']];
				});
			}
			if (config['level-3']) {
				var dim3 = this.ndx.dimension(function(d) {
					return d[config['level-3']];
				});
			}
			if (config['level-4']) {
				var dim4 = this.ndx.dimension(function(d) {
					return d[config['level-4']];
				});
			}
			this.hierarchieCharts.push({
				"config" : config,
				"dim" : dim,
				"dim1" : dim1,
				"dim2" : dim2,
				"dim3" : dim3,
				"dim4" : dim4,
			});
			var renderHierarchie = function(config, dim, dim1, dim2, dim3, dim4) {
				var measures = [];
				var labels = [];
				var depthHierarchie = 1;
				var children = d3.nest();
				if (config['level-1']) {
					labels.push(config['level-1']);
					depthHierarchie++;
					children.key(function(d) {
						return d.value[config["level-1"]];
					});
				}
				if (config['level-2']) {
					labels.push(config['level-2']);
					depthHierarchie++;
					children.key(function(d) {
						return d.value[config["level-2"]];
					});
				}
				if (config['level-3']) {
					labels.push(config['level-3']);
					depthHierarchie++;
					children.key(function(d) {
						return d.value[config["level-3"]];
					});
				}
				if (config['level-4']) {
					labels.push(config['level-4']);
					depthHierarchie++;
					children.key(function(d) {
						return d.value[config["level-4"]];
					});
				}
				measures.push(config.group);
				if(config.colorfield){
					labels.push(config.colorfield);
				}
				var grp = dim.group().reduce(
						THIS.reduceFieldsAdd(measures,labels),
						THIS.reduceFieldsRemove(measures,labels),
						THIS.reduceFieldsInitial(measures,labels));
				var groupAll = grp.all();

				children = children.entries(groupAll);
				function reSortRoot(root, value_key) {
					for ( var key in root) {
						if (key === "key") {
							root.name = root.key;
							delete root.key;
						}
						if (key === "values") {
							root.children = [];
							for (item in root.values) {
								root.children.push(reSortRoot(
										root.values[item], value_key));
							}
							delete root.values;
						}
						if (key === "value") {
							root.size = parseFloat(root.value[config.group]);
						}
					}
					return root;
				}
				var root = {
					"key" : "Total",
					"values" : children
				};
				root = reSortRoot(root, "size");
				var width;
				if (config.chartwidth.toLowerCase() === "auto") {
					width = parseInt($(config.html).width());
					width = width > this.defaultHeight ? this.defaultHeight
							: width;
				} else {
					width = parseInt(config.chartwidth);
				}

				var height = width, radius = (Math.min(width, height) / 2) - 10;

				var x = d3.scaleLinear().range([ 0, 2 * Math.PI ]);

				var y = d3.scale.pow().range([ 0, radius ]);

				var partition = d3.layout.partition().value(function(d) {
					return d.size;
				});

				var arc = d3.svg.arc().startAngle(function(d) {
					return Math.max(0, Math.min(2 * Math.PI, x(d.x)));
				}).endAngle(function(d) {
					return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx)));
				}).innerRadius(function(d) {
					return Math.max(0, y(d.y));
				}).outerRadius(function(d) {
					return Math.max(0, y((d.y + d.dy)));
				});
				$(config.html).find("svg").remove();
				var svg = d3.select(config.html).append("svg").attr("width",
						width).attr("height", height).attr("class",
						"hierarchie").append("g").attr("transform",
						"translate(" + width / 2 + "," + (height / 2) + ")");
				var keyColorCodes = THIS.getKeyColorCodeByRange(grp, config,'name');
				if(!keyColorCodes.Total)
					keyColorCodes.Total = randomColor();
				svg.selectAll("path")
						.data(partition.nodes(root))
						.enter()
						.append("path")
						.attr("d", arc)
						.style("fill",
								function(d) { 
								 	return keyColorCodes[d.name];
							    })
						.on("click", click)
						.attr("level", function(d) {
							return d.depth;
						})
						.append("title")
						.text(
								function(d) {
									var key = d.name;
									var fieldname = config.fieldname;
									var description = config.description;
									if (d.depth !== 0) {
										if (config["level-" + d.depth]
												&& config["level-" + d.depth] !== "") {
											fieldname = config["level-"
													+ d.depth];
											description = config["level-"
													+ d.depth + "-desc"];
										}
										if (description !== null
												&& $.trim(description) !== "") {
											key += "-"
													+ THIS.masterData[fieldname][key];
										}
									}
									return key + "\n" + numberFormat(d.value);
								});

				function click(d) {

					svg.transition().duration(750).tween(
							"scale",
							function() {
								var xd = d3.interpolate(x.domain(), [ d.x,
										d.x + d.dx ]), yd = d3.interpolate(y
										.domain(), [ d.y, 1 ]), yr = d3
										.interpolate(y.range(), [ d.y ? 20 : 0,
												radius ]);
								return function(t) {
									x.domain(xd(t));
									y.domain(yd(t)).range(yr(t));
								};
							}).selectAll("path").attrTween("d", function(d) {
						return function() {
							return arc(d);
						};
					});
					var filterValues = [];
					filterValues.push(d.name);
					dim.filterAll();
					if (dim1)
						dim1.filterAll();
					if (dim2)
						dim2.filterAll();
					if (dim3)
						dim3.filterAll();
					if (dim4)
						dim4.filterAll();
					var dimHierarchie = {};
					if (d.depth === depthHierarchie) {
						$(config.html).data({
							"fieldname" : config.fieldname,
							"fieldvalue" : d.name
						});
						dimHierarchie = dim;
					} else if (d.depth === 1) {
						$(config.html).data({
							"fieldname" : config['level-1'],
							"fieldvalue" : d.name
						});
						dimHierarchie = dim1;
					} else if (d.depth === 2) {
						$(config.html).data({
							"fieldname" : config['level-2'],
							"fieldvalue" : d.name
						});
						dimHierarchie = dim2;
					} else if (d.depth === 3) {
						$(config.html).data({
							"fieldname" : config['level-3'],
							"fieldvalue" : d.name
						});
						dimHierarchie = dim3;
					} else if (d.depth === 4) {
						$(config.html).data({
							"fieldname" : config['level-4'],
							"fieldvalue" : d.name
						});
						dimHierarchie = dim4;
					}
					if (d.y !== 0) {
						$(config.html).find("a.reset").show();
						dimHierarchie.filter(function(d) {
							return filterValues.indexOf(d) > -1;
						});
					} else {
						$(config.html).data({
							"fieldname" : null,
							"fieldvalue" : null
						});
					}
					resetAllCharts(THIS, config.html);
				}

				d3.select(self.frameElement).style("height", height + "px");
			};
			renderHierarchie(config, dim, dim1, dim2, dim3, dim4);
			this.refreshHierarchieChart = function(currentConfigHtml) {
				for ( var x = 0; x < this.hierarchieCharts.length; x++) {
					if (!currentConfigHtml
							|| this.hierarchieCharts[x]["config"].html !== currentConfigHtml) {
						renderHierarchie(this.hierarchieCharts[x]["config"],
								this.hierarchieCharts[x]["dim"],
								this.hierarchieCharts[x]["dim1"],
								this.hierarchieCharts[x]["dim2"],
								this.hierarchieCharts[x]["dim3"],
								this.hierarchieCharts[x]["dim4"]);
					}
				}
			};
			$(".reset", $(config.html)).on('click', function() {
				dim.filterAll();
				if (dim1)
					dim1.filterAll();
				if (dim2)
					dim2.filterAll();
				if (dim3)
					dim3.filterAll();
				if (dim4)
					dim4.filterAll();
				$(config.html).find("a.reset").hide();
				$(config.html).data({
					"fieldname" : null,
					"fieldvalue" : null
				});
				resetAllCharts(THIS);
			}.bind(this));

		} catch (e) {
			console.log('Error Creating Pie Chart' + e);

		}
	};
	Graphing.prototype.drawSlider = function(config) {
		var THIS = this;
		try {
			if (viewSettings && viewSettings.length > 0){
				$.each(dataAnalyser.viewSettings,function(){
					if (this.anchorName ===  config.field && this.filterValues){
						config.defaultFilterFromvalue = this.filterValues[0];
						config.defaultFilterTovalue = this.filterValues[1];
						return;
					}
				});
			}
			config.defaultFilterFromvalue = parseFloat(config.defaultFilterFromvalue) || 0;
			config.defaultFilterTovalue = parseFloat(config.defaultFilterTovalue) || 100;
			var label = "d['" + config.field + "'] || 'Others'";
			var dimFun = new Function('d', 'return ' + label + ';');
			var dim = this.ndx.dimension(dimFun);
			var fields = [];
			var calculatedField = [];
			if (config.calculatedfield && config.calculatedfield !== null
					&& $.trim(config.calculatedfield) !== "") {
				calculatedField = config.calculatedfield.split(",");
			}
			fields.push(config.group);
			fields = $.merge(fields, calculatedField);
			this.sliders.push({
				"config" : config,
				"dim" : dim
			});
			var renderSlider = function(config, dim) {
				$(config.html).find("input").remove();
				$(config.html).find(".irs").remove();
				$(config.html).append($("<input/>").attr({
					"type" : "text",
					"name" : "range_1",
					"value" : ""
				}));
				if ($(config.html).find("input").ionRangeSlider().length)
					$(config.html).find("input").ionRangeSlider("remove");
				$(config.html).find("input").ionRangeSlider({
					min : 0,
					max : 100,
					from : parseInt(config.defaultFilterFromvalue),
					to : parseInt(config.defaultFilterTovalue),
					type : 'double',
					step : 1,
					multiply : 1,
					prettify : false,
					hasGrid : true,
					onFinish : function() {
						filterSlider(config, dim);
					}
				});
				filterSlider(config, dim);
			};
			var filterSlider = function(config, dim) {
				var fromPercentage = 0;
				var toPercentage = 100;
				if ($(config.html).find(".irs-from").length > 0
						&& !isNaN($(config.html).find(".irs-from").html())) {
					fromPercentage = $(config.html).find(".irs-from").html();
				}
				if ($(config.html).find(".irs-to").length > 0
						&& !isNaN($(config.html).find(".irs-to").html())) {
					toPercentage = $(config.html).find(".irs-to").html();
				}
				var rowGrp = dim.group().reduce(THIS.reduceFieldsAdd(fields),
						THIS.reduceFieldsRemove(fields),
						THIS.reduceFieldsInitial(fields));
				var grp = rowGrp.all().sort(
						THIS.sortGroup(config.group, config.aggregate, 1));
				if (grp && grp.length > 0) {
					var percentageCount = grp.length / 100;
					var fromCount = parseInt((100 - fromPercentage)
							* percentageCount) + 1;
					var toCount = parseInt((100 - toPercentage)
							* percentageCount);
					var selectedValues = grp.slice(Math.round(toCount), Math
							.round(fromCount));
					var selectedKey = [];
					selectedValues.forEach(function(d) {
						selectedKey.push(d.key);
					});
					$(config.html).data("fieldvalue", selectedKey);
					dim.filter(function(d) {
						return selectedKey.indexOf(d) > -1;
					});
					resetAllCharts(THIS);
				}
			};
			renderSlider(config, dim);
		} catch (e) {
			console.log('Error Creating bubble Chart' + e);
		}
	};
	this.resetAllProfiles = function(chart,chartType){
		resetAllProfiles(chart,chartType);
	};
	function resetAllProfiles(chart,chartType){
		if (chartType === "world"){

			profileAnalyser.onChartFilter(chart,chartType);
		} else{
			var anchorName = chart.anchorName().split('container')[0];
			if($('#'+anchorName).data("profileid") && profileAnalyser){
				profileAnalyser.onChartFilter(chart);
			}
		}
		
	}
	function resetAllCharts(THIS, currentConfigHtml) {
		$(".jarviswidget> div").removeClass("hide");
		$(".jarviswidget-toggle-btn").find("i").addClass("fa-minus").removeClass("fa-plus");
		currentConfigHtml = currentConfigHtml || null;
		if(THIS.renderTopNBarCharts){
			THIS.renderTopNBarCharts(currentConfigHtml);
		}
		if(THIS.renderTopNRowCharts){
			THIS.renderTopNRowCharts(currentConfigHtml); 			
		}
		dc.redrawAll();		
		if (THIS.refreshTable)
			THIS.refreshTable();
		if (THIS.refreshImageGrid)
			THIS.refreshImageGrid();
		if (THIS.refreshUSChart)
			THIS.refreshUSChart(currentConfigHtml);
		if (THIS.refreshSummary)
			THIS.refreshSummary(currentConfigHtml);
		if (THIS.refreshGroupedRowChart)
			THIS.refreshGroupedRowChart(currentConfigHtml);
		if (THIS.refreshMultiLevelRowChart)
			THIS.refreshMultiLevelRowChart(currentConfigHtml);
		if (THIS.refreshTreeMapChart)
			THIS.refreshTreeMapChart(currentConfigHtml);
		if (THIS.refreshHierarchieChart)
			THIS.refreshHierarchieChart(currentConfigHtml);
		if (THIS.refreshGroupedBarLine)
			THIS.refreshGroupedBarLine(currentConfigHtml);
		dataAnalyser.setViewConfigInWidget();
		/*
		 * if (THIS.refreshCompositeBrush)
		 * THIS.refreshCompositeBrush(currentConfigHtml);
		 */
	}
	function resetAllCustomCharts(THIS) {
		if ($(event.target).hasClass("fa-undo")) {
			dc.filterAll();
			if (THIS.refreshUSChart) {
				for ( var x = 0; x < THIS.usCharts.length; x++) {
					if (THIS.usCharts[x]["dim"])
						THIS.usCharts[x]["dim"].filterAll();
				}
			}
			if (THIS.refreshHierarchieChart) {
				for ( var x = 0; x < THIS.hierarchieCharts.length; x++) {
					if (THIS.hierarchieCharts[x]["dim"])
						THIS.hierarchieCharts[x]["dim"].filterAll();
				}
			}
			resetAllCharts(THIS);
			$(".reset").hide();
		}
	}
	;
	Graphing.prototype.drawCalendar = function(config) {
		var THIS = this;
		var filter = filterMapping(config.filters,null,THIS);
		localStorage["calendarfilter"] = filter;
		var url = directoryName + "/rest/dataAnalyser/getCalendarData";
		$(config.html).fullCalendar('destroy');
		$(config.html).fullCalendar(
				{
					header : {
						left : 'prev,next today',
						center : 'title',
						right : 'month,basicWeek,basicDay'
					},
					defaultDate : config.defaultdate
							|| System.getFormattedDate(new Date()),
					editable : true,
					eventLimit : true, // allow "more" link when too many
					height : config.chartheight,
					width : config.chartwidth,
					events : {
						url : url,
						type : 'POST',
						data : {
							source : config.source,
							database : config.database,
							table : config.table,
							eventid : config.eventid,
							eventname : config.eventname,
							eventstart : config.eventstart,
							eventend : config.eventend,
							eventcolor : config.eventcolor,
							filters : localStorage["calendarfilter"]
						},
						error : function() {
							alert('there was an error while fetching events!');
						}
					},
					callback:calendarDrillDown.callBack(),
					eventClick : function(calEvent, jsEvent, view) {
						/*
						 * $('td', nRow) .attr( { 'onClick' :
						 * "javascript:dataTableDrillDown." +
						 * config.data.drilldowncallback + "(this)" });
						 * 
						 * $(nRow).data("request", JSON.stringify(aData));
						 */
						calendarDrillDown.showDetails(
								config.eventclickcallback, calEvent, jsEvent,
								view);
						$(this).css('border-color', 'red');

					}

				});
	};
	ngramDatas = null;
	Graphing.prototype.drawNGram = function(config, makeAjaxCall) {
		var THIS = this, filter = "";
		filter = filterMapping(config.filters,null,THIS);
		if(config.whereCondition) {
			filter = config.whereCondition;
		}
		if(makeAjaxCall || makeAjaxCall == undefined){
			makeAjaxCall = true;
			var request = {};
			var ngramConfig = {
				"gram": config.ngramType,
				"source" : config.source,
				"database" : config.database,
				"table" : config.table,
				"field": config.tableField,
				"whereCondition": "WHERE "+(filter? filter: '1=1')
			};
			request.config = JSON.stringify(ngramConfig);
			enableLoading();
			if(dataAnalyser.ngramFilters.lemmatization) {
				callAjaxService("nGramAnalyserNLPPost", function(response){nGramAnalyserCallBackSucess(response, makeAjaxCall)}, callBackFailure, request, "POST", null, true);
			} else {
				callAjaxService("nGramAnalyserPost", function(response){nGramAnalyserCallBackSucess(response, makeAjaxCall)}, callBackFailure, request, "POST", null, true);
			}
		} else {
			enableLoading();
			var newObject = jQuery.extend(true, {}, ngramDatas); // creating
																	// deep copy
																	// of data
			nGramAnalyserCallBackSucess(newObject, makeAjaxCall);
		}
	}

	var nGramAnalyserCallBackSucess = function(response, makeAjaxCall){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(makeAjaxCall){
			ngramDatas = response;
		}
		generateWordCloud(response, makeAjaxCall);
	};
	
	generateWordCloud = function(response, makeAjaxCall){
		var response = jQuery.extend(true, {}, response);
		var data = filterNgramData(response["nGramAnalyser"]);
		$("#cloud").empty();
		var width = $("#cloud").width();
		var height = 600;
		var svg = d3.select('#cloud').append('svg').attr('width', width).attr('height', height)
					.attr('id', 'cloudSVG').append('g').attr('transform', 'translate('+width/2+', '+height/2+')');
		function drawCloud(words) {
			var vis = svg.selectAll('text').data(words);
			var clickEvent = 'onclick';
			if($("#ribbon").length && $("#ribbon").find('.breadcrumb').text().toLowerCase().includes('raw data ngram')){
				clickEvent = 'null';
			}
			vis.enter().append('text')
				.style('font-size', function(d) { return d.size + 'px'; })
				.style('font-family', function(d) { return d.font; })
				.style('fill', function(d, i) { return colors(i); })
				.attr('text-anchor', 'middle')
				.attr('class', "icon-hover")
				.attr(clickEvent, function(d){return "gramClick('"+d.text+"')";})
				.attr('transform', function(d) {
				  return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
				}).html(function(d) { return "<title>"+d.text+": "+Math.round(d.size/minFontSize)+"</title>"+ d.text; });
		}
		
		if(data.length > 0) {
			var totalsize = 1000;
			for (var i=0;i<data.length;i++){
				
				var temp =1250/(data[i].size*data[i].text.length);
				if (totalsize > temp)
					totalsize = temp;
			}
		} else {
			showNotification("error", "No Records Found!");
			return;
		}
		
		var minFontSize = totalsize;
		var typeFace = 'Gorditas';
		var colors = d3.scale.category20b();
			d3.layout.cloud()
				.size([width, height])
				.words(data)
				.rotate(function() { return ~~(Math.random()*2) * 0;}) // 0 or
																		// 90deg
				.font(typeFace)
				.fontSize(function(d) { return d.size * minFontSize; })
				.on('end', drawCloud)
				.start();

		$(".ngram-onoffswitch-checkbox").unbind("change").bind("change", function() {
			enablePanZoom();
		});
		
		if ($(".ngram-onoffswitch-checkbox").is(":checked")){
			panZoomForward = PanZoomsvg('treeviewidforward');
			if (panZoomForward && panZoomForward != null){
				panZoomForward.enableZoom();
				panZoomForward.enablePan();
			}
		}
		if($("#ribbon").length && $("#ribbon").find('.breadcrumb').text().toLowerCase().includes('raw data ngram')){
			var tip = d3.tip().direction('s').offset([10, 20]).html(function(e) {   
				var returnstr='<div id="ngramtooltip" class="tooltip-toolbar" style="padding-top:3px;"><a  class="tooltip-toolbar-action" onclick="System.filtertag(0,\''+e.text+'\')"><i class="fa fa-check txt-color-greenLight" ></i> Filter</a>';
				if($("#keyngramStopwords").length>0 && $("#firstBtnngramStopwords").length>0) {	
					returnstr+='<a class="tooltip-toolbar-action" onclick="System.filtertag(2,\''+e.text+'\')"><i class="fa fa-times txt-color-red"></i> Exclude</a>';				
				}
				returnstr+='<\div>';
				
				return returnstr;
			});
			d3.selectAll('#cloud').select("svg").selectAll(".icon-hover").call(tip);
			d3.selectAll('#cloud').select("svg").selectAll(".icon-hover").on('mousedown', tip.show);
			$(document).mouseup(function(e) {
			    var container = $("#cloudSVG");
			    // if the target of the click isn't the container nor a
				// descendant of the container
			    if (!container.is(e.target) && container.has(e.target).length === 0) {
			    	$(".tooltip-toolbar").hide();
			    }
			});
		}
	}
	
	filterNgramData = function(data) {
		if(dataAnalyser.ngramFilters) {
			if(dataAnalyser.ngramFilters.synonyms.length > 0){
				dataAnalyser.ngramFilters.synonyms.forEach(function(element1) {
					data.forEach(function(element2, index) {
						if(element1.ngramKey.toLowerCase() == element2.text.toLowerCase()){
							data[index].text = element1.ngramValue;
						}
					});
				});
			}
			if(dataAnalyser.ngramFilters.stopwords.length > 0){
				dataAnalyser.ngramFilters.stopwords.forEach(function(element1) {
					data.forEach(function(element2, index) {
						if(element1.ngramKey.toLowerCase() == element2.text.toLowerCase()){
							data.splice(index, 1);
						}
					});
				});
			}
			if(dataAnalyser.ngramFilters.hideSpecialChars){
				data.forEach(function(element2, index) {
					data[index].text = data[index].text.replace(/[^a-zA-Z0-9\s]/g, '');				
				});
			}
			if(dataAnalyser.ngramFilters.hideNumbers){
				data.forEach(function(element2, index) {
					data[index].text = data[index].text.replace(/[0-9]/g, '');	
				});
			}	
		}
		return data;
	}
	
	enablePanZoom = function(){
		if ($(".ngram-onoffswitch-checkbox").is(":checked")){
			panZoomForward = PanZoomsvg('cloudSVG');
			if (panZoomForward && panZoomForward != null){
				panZoomForward.enableZoom();
				panZoomForward.enablePan();
			}
		} else{
			if (panZoomForward && panZoomForward != null){
				panZoomForward.disableZoom();
				panZoomForward.disablePan();
			}
		}
	};
	gramClick = function(text){
		// searchKey = text.replace(" ","+");
	};
	
	Graphing.prototype.drawIFrame = function(config) {
		$(config.html)
			.append($('<iframe>').attr({"src": config.iframe, "scrolling": "yes"})
					.css({"position": "relative", "height": "500px", "width": "100%"}));
	};
	
	Graphing.prototype.drawHTMLTemplate = function(config) {
		if(config.layout){
			$(config.html).parent().parent().parent().find('header').hide();
			$(config.html).parent().parent().css({"border": "none", "overflow": "auto"});
		}
		$(config.html).empty();
		$(config.html).append(config.template);
	};
	
	this.getChartFilters = function(){
		var filters = [];
		if (dc.chartRegistry.list()) {
			$.each(dc.chartRegistry.list(),function(index, chart) {
				var filter = {
						anchorName:chart.anchorName(),
						fieldname: $(chart.anchor()).data('fieldname'),
						dynamicDimension:$(chart.anchor()).find("[name='dimensionselect']").val(),
						resetedDefaultVal:$(chart.anchor()).data('resetedValue')
					};
				if (chart.hasFilter()) {
					 filter.filterValues = chart.filters();
				}else if(chart.filterValue && chart.filterValue.length>0){
					filter.filterValues = chart.filterValue;
				}
				if($(chart.anchor()).data("profileid")){
					filter.profileid = $(chart.anchor()).data("profileid");
				}
				filters.push(filter);
			});
			if (this.refreshUSChart) {
				$(".datamap").each(
					function(index, ele) {
						var anchorName = $(ele).parent("div").attr("id");
						var pathSelected = $(ele).find("path.selected");
						var stateSelected = [];
						if (pathSelected.length > 0) {
								pathSelected.each(function(
									pathIndex, pathEle) {
								stateSelected.push($(pathEle)
										.attr("state"));
							});
							var filter = {
								"anchorName": anchorName,
								"filterValues": stateSelected,
								"resetedDefaultVal":$(chart.anchor()).data('resetedValue')
							};
							filters.push(filter);
						}
				});
			}
			if (this.refreshSummary) {
				$('[id^=summary]').children().each(
						function(index, ele) {
							var dataMapField = $(ele).attr("id");
							var filterby = $(ele).data("filterby");
							if (filterby && filterby != null){
								var filter = {
									"anchorName": dataMapField,
									"filterValues": filterby,
									"resetedDefaultVal":$(chart.anchor()).data('resetedValue')
								};
								filters.push(filter);
							}
						});
			}
			if (this.refreshHierarchieChart) {
				$(".hierarchie").each(
						function(index, ele) {
							var dataHierarchieField = $(ele).parent("div").attr("id");
							var dataHierarchieValue = $(ele).parent("div").data("fieldvalue");
							if (dataHierarchieValue
									&& dataHierarchieValue !== null
									&& dataHierarchieValue !== "null") {
								var filter = {
									"anchorName": dataHierarchieField,
									"filterValues": dataHierarchieValue,
									"resetedDefaultVal":$(chart.anchor()).data('resetedValue')
								};
								filters.push(filter);
							}
						});
			}
			if (this.refreshMultiLevelRowChart) {
				$(".multilevel").each(
						function(index, ele) {
							var filtervalue = $(ele).parent().data("filtervalue");
							for(var field in filtervalue) {
								filters.push({"anchorName":field,"filterValues":filtervalue[field]});
							}
						});
			}
			if (this.refreshGroupedBarLine) {
				$(".groupedbarline").each(
						function(index, ele) {
							var fieldName = $(ele).parent().data("fieldname");
							var filtervalue = $(ele).parent().data("filtervalue");
							if(filtervalue && filtervalue.length>0){
								filters.push({"anchorName":fieldName,"filterValues":filtervalue[field]});
							}
							
						});
			}
			if (this.refreshTreeMapChart) {
				$(".treemap").each(
						function(index, ele) {
							var filtervalue = $(ele).parent().data("filtervalue");
							for(var field in filtervalue) {
								filters.push({"anchorName":field,"filterValues":filtervalue[field]});
							}
						});
			}
			if (this.sliders && this.sliders.length > 0) {
				$(".irs-with-grid").each(
						function(index, ele) {
							var dataSliderField = $(ele).parent("div").attr("id");
							var dataSliderFromValue = $(ele).parent("div").find(".irs-from").html() || 0;
							var dataSliderToValue = $(ele).parent("div").find(".irs-to").html() || 0;
							if (dataSliderFromValue !== 0 || dataSliderToValue !== 100) {
								var filter = {
										"anchorName": dataSliderField,
										"filterValues": [dataSliderFromValue,dataSliderToValue],
										"resetedDefaultVal":$(chart.anchor()).data('resetedValue')
									};
									filters.push(filter);
							}
						});
			}
		}
		return filters;
	};
	
	this.renderTopNCharts = function(currentConfigHtml,renderFunction,chartType){
		for(var index=0; index<this.topNBarCharts.length; index++){
			if ((!currentConfigHtml || this.topNBarCharts[index]["config"].html !== currentConfigHtml) && this.topNBarCharts[index]["type"] === chartType) {
				var chartIndex = this.drawnGraph.indexOf(this.topNBarCharts[index]["chart"]);
				if(chartIndex > -1){
					this.drawnGraph.splice(chartIndex, 1);
					this.topNBarCharts[index]["config"].isRedraw=true;	
				}
				this.topNBarCharts[index]["config"].defaultFiltervalue= this.topNBarCharts[index]["chart"].filters().toString();
			var renderedChart =	renderFunction(this.topNBarCharts[index]["config"],this.topNBarCharts[index]["dim"]);
			this.topNBarCharts[index]["chart"] = renderedChart;
			}
		}
	};

	// added code for tree map
	Graphing.prototype.drawTreeMap = function(config) {
		var THIS = this;
		var filtervalue = {};
		var dimFun = new Function('d', "return d['" + config.field + "'];");
		var dim = this.ndx.dimension(dimFun);
		$(".reset", $(config.html)).data('isReset', true);
		var numberFormat = d3.format(config.numFormat);
		var tip = d3
				.tip()
				.offset([ -8, 0 ])
				.attr('class', 'd3-tip')
				.html(
						function(d) {
							var nameObj = {};
							var count = config.prefix + (numberFormat(d.count))
									+ config.suffix;
							if (config.charts[d.depth - 2]
									&& config.charts[d.depth - 2].level) {
								nameObj = THIS.masterData[config.charts[d.depth - 2].level];
							} else if (config.fieldname) {
								nameObj = THIS.masterData[config.fieldname];
							}
							return ((nameObj && nameObj[d.name]) ? nameObj[d.name]
									: d.name)
									+ " : " + count;
						});

		$(config.html).data({
			"chartindex" : this.groupedTreeMapCharts.length
		});
		this.groupedTreeMapCharts.push({
			"config" : config,
			"dim" : dim
		});

		var renderTreeMap = function(config, dim) {
			$(config.html).find(".treemap").remove();
			var defaultVal = 1000;
			var barWidth;
			if (config.chartwidth.toLowerCase() === "auto") {
				barWidth = parseInt($(config.html).width());
			} else {
				barWidth = config.chartwidth;
			}
			var barHeight;
			if (config.chartheight.toLowerCase() === "auto") {
				barHeight = parseInt($(config.html).width());
				barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
						: barHeight;
			} else {
				barHeight = config.chartheight;
			}
			var chartwidth;
			if (config.barwidth && config.barwidth === "auto") {
				chartwidth = barWidth;
			} else {
				chartwidth = config.marginleft + config.marginright
						+ (defaultVal * config.barwidth);
			}
			var aggField;
			if (config.aggregate === "calculated") {
				aggField = config.calculatedformula;
			} else {
				aggField = config.group;
			}
			var x = d3.scaleLinear().range([ 0, barWidth ]), y = d3.scale
					.linear().range([ 0, barHeight ]), color = d3.scale
					.category20c(), root, node;

			var measures = [];
			var labels = [];
			var dimentionData = [];
			measures.push(config.group);
			if (config.colorfield) {
				labels.push(config.colorfield);
			}
			var data = {
				labels : [],
				series : []
			};
			if (config.charts && config.charts !== null
					&& config.charts.length > 0) {
				for (var x = 0; x < config.charts.length; x++) {
					if (config.charts[x]["level"]) {
						dimentionData.push(config.charts[x]["level"]);
					}
				}
			}
			;
			var grp = dim.group().reduce(
					THIS.reduceMultipleFieldsAdd(config.group, dimentionData),
					THIS
							.reduceMultipleFieldsRemove(config.group,
									dimentionData),
					THIS.reduceMultipleFieldsInitial(config.group,
							dimentionData));
			var groupAll = grp.all();

			var root = {}
			var levelObj = {};
			for (var i = 0; i < groupAll.length; i++) {
				var obj = groupAll[i];
				levelObj[obj.key] = obj.value;
				if (!levelObj[config.group])
					levelObj[config.group] = 0;
				levelObj[config.group] += obj.value[config.group];
			}
			constructRootObj(root, levelObj, config.rootname, config.group);
			function constructRootObj(root, levelObj, name, group) {
				root.name = name;
				if (!root.count)
					root.count = 0;
				root.count += levelObj[group];
				if (!root.children)
					root.children = [];
				if (d3.keys(levelObj).length > 0) {
					var i = 0;
					for ( var k in levelObj) {
						if (k !== group) {
							root.children[i] = {};
							constructRootObj(root.children[i++], levelObj[k],
									k, group);
						}
					}
				}
			}
			var svg = d3.select(config.html).append("div").attr("class",
					"treemap").style("width", barWidth + "px").style("height",
					barHeight + "px").append("svg:svg").attr("width", barWidth)
					.attr("height", barHeight).append("svg:g").attr(
							"transform", "translate(.5,.5)");
			node = root;
			var treemap = d3.layout.treemap().round(false).size(
					[ barWidth, barHeight ]).sticky(true).sort(function(a, b) {
				return a.value - b.value;
			}).value(function(d) {
				return d.count;
			});

			var nodes = treemap.nodes(root).filter(function(d) {
				return !d.children;
			});

			var cell = svg.selectAll("g").data(nodes).enter().append("svg:g")
					.attr("class", "cell").attr("transform", function(d) {
						return "translate(" + d.x + "," + d.y + ")";
					}).on("click", function(d) {
						return zoom(node == d.parent ? root : d.parent);
					});

			cell.append("svg:rect").attr("width", function(d) {
				return d.dx;
			}).attr("height", function(d) {
				return d.dy;
			})
			.attr("class", function(d) {
				return d.children ? "parent" : "child";
			});
			d3.selectAll(config.html).selectAll(".parent,.child").call(tip);
			d3.selectAll(config.html).selectAll(".parent,.child").on(
					'mouseover', tip.show).on('mouseout', tip.hide).style(
					"fill", function(d) {
						return d.children ? null : color(d.parent.name);
					});

			cell.append("svg:text").attr("x", function(d) {
				return d.dx / 2;
			}).attr("y", function(d) {
				return d.dy / 2;
			}).attr("dy", ".35em").attr("text-anchor", "middle").text(
					function(d) {
						return d.name + " : " + d.count;
					}).style("opacity", function(d) {
				d.w = this.getComputedTextLength();
				return d.dx > d.w ? 1 : 0;
			});

			d3.select(config.html).on("click", function() {
				zoom(root);
			});

			d3.select("select").on("change", function() {
				treemap.value(this.value == "size" ? size : count).nodes(root);
				zoom(node);
			});

			function size(d) {
				return d.size;
			}

			function count(d) {
				return 1;
			}
			// added code for resetting the chart
			$(".reset", $(config.html)).unbind('click').bind('click',
					function() {
						zoom(root);
					});

			function zoom(d) {
				var x = d3.scaleLinear().domain([ 0, barWidth ]).range(
						[ 0, barWidth ]);

				var y = d3.scaleLinear().domain([ 0, barHeight ]).range(
						[ 0, barHeight ]);

				var kx = barWidth / d.dx, ky = barHeight / d.dy;
				x.domain([ d.x, d.x + d.dx ]);
				y.domain([ d.y, d.y + d.dy ]);

				var t = svg.selectAll("g.cell").transition().duration(
						d3.event.altKey ? 7500 : 750).attr("transform",
						function(d) {
							return "translate(" + x(d.x) + "," + y(d.y) + ")";
						});

				t.select("rect").attr("width", function(d) {
					return kx * d.dx;
				}).attr("height", function(d) {
					return ky * d.dy;
				})

				t.select("text").attr("x", function(d) {
					return kx * d.dx / 2;
				}).attr("y", function(d) {
					return ky * d.dy / 2;
				}).style("opacity", function(d) {
					return kx * d.dx > d.w ? 1 : 0;
				});

				node = d;
				// added code for resetting the chart
				if (d.depth) {
					$(config.html).find('.reset').data('isReset', false).show();
					var tempObj = d, label;
					if (dim) {
						label = (config.measurelabel && config.measurelabel[config.fieldname]) ? config.measurelabel[config.fieldname]
								: config.fieldname;
						filtervalue[config.fieldname] = tempObj.name;
						dim.filterAll();
						dim.filter(function(value) {
							return tempObj.name === value;
						});
					}
					tempObj = tempObj.parent;

					$(config.html).data("filtervalue", filtervalue);
					resetAllCharts(THIS, config.html);
					d3.event.stopPropagation();
				} else {
					if (dim) {
						dim.filterAll();
					}
					$(config.html).find('.reset').data('isReset', true).hide();
					$(config.html).data("filtervalue", {});
					resetAllCharts(THIS);
				}

			}
		}
		renderTreeMap(config, dim);
		this.refreshTreeMapChart = function(currentConfigHtml) {
			for (var x = 0; x < this.groupedTreeMapCharts.length; x++) {
				if (!currentConfigHtml
						|| this.groupedTreeMapCharts[x]["config"].html !== currentConfigHtml) {
					renderTreeMap(this.groupedTreeMapCharts[x]["config"],
							this.groupedTreeMapCharts[x]["dim"]);
				}
			}
		};
	};
	Graphing.prototype.drawCompositeBrush = function(config) {
		var THIS = this;
		var dimX;
		var dimXZ;
		var dimXY;
		var dimDate = null;
		var measures = [];
		var labels = [];
		labels.push(config.field);		
		$(config.html).data('resetedValue',false);
		var barWidth;
		if (config.chartwidth.toLowerCase() === "auto") {
			barWidth = parseInt($(config.html).width());
		} else {
			barWidth = config.chartwidth;
		}
		var barHeight;
		if (config.chartheight.toLowerCase() === "auto") {
			barHeight = parseInt($(config.html).width());
			barHeight = barHeight > THIS.defaultHeight ? THIS.defaultHeight
					: barHeight;
		} else {
			$(config.html).css({"min-height":config.chartheight+"px"});
			barHeight = config.chartheight;
		}

		if (config.xaxistype === "date"){
			$(config.html).data("xaxistype",config.xaxistype);
			dimX = this.ndx.dimension(function(d) {
				return d[config.field]?d[config.field]+"-"+d[config.xAxis]:'Others';
			});
			dimDate = this.ndx.dimension(function(d) {
				return d["date-"+config.xAxis];
			});
			labels.push("date-"+config.xAxis);
		}else{
			dimX = this.ndx.dimension(function(d) {
				return d[config.field]?d[config.field]:'Others';
			});
			dimXY = this.ndx.dimension(function (d) {
		        return [d[config.xAxis], d[config.yAxis]];
		    });
		    dimXZ = this.ndx.dimension(function (d) {
		        return [d[config.xAxis], d[config.radius]];
		    });
			measures.push(config.xAxis); 
		}
		var dims = {};
		if (config.charts && config.charts !== null
				&& config.charts.length > 0) {
			for ( var x = 0; x < config.charts.length; x++) {
				if(config.charts[x].type === 'scatter'){
					dims[config.charts[x].group] = this.ndx.dimension(function (d) {
				        return [d[config.field], d[config.charts[x].group],config.charts[x].color];
				    });
				}
			}
		};
		if(config.colorfield){
			labels.push(config.colorfield);
		}
		if (measures.indexOf(config.yAxis)===-1)
			measures.push(config.yAxis);
		if (measures.indexOf(config.radius)===-1)
			measures.push(config.radius);
		$(".reset", $(config.html)).data('isReset',true);
		$(".axisLabel", $(config.html)).attr({
			'data-numformat':config.numFormat,
			"data-prefix":config.radiusPrefix,
			"data-showpercentage":config.showpercentage,
			"data-aggregate":config.aggregate,
			"data-calculatedformula":config.calculatedformula,
			"data-suffix":config.radiusSuffix
			});
		if (config.filtervalues && $(config.html).find('select').length === 0) {
			this.constructSelect(config, dim1);
			$(config.html).find('select').unbind('change').bind('change',
					function() {
						config.filterdefaultval = $(this).val();
						renderBubble(config, dim1);
					});
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
			$(config.html).find("input").unbind('click').bind("click",function(){
				if($(this).is(":checked")){
					config.filterfrom = "bottom";
				}else{
					config.filterfrom = "top";
				}
				renderBubble(config, dim1);
			});
		} else {
			$(config.html).find('select').val(config.filterdefaultval);
			if(config.filterfrom === "bottom"){
				$(config.html).find('input').attr("checked","checked");
			}
		}
		this.compositeBrushchart.push({
			"config" : config,
			"dimX" : dimX,
			"dims":dims
		});
		var renderCompositeBrush = function(config, dim,dims) {
			function sel_stack1(i) {
	              return function(d) {
	                  return d.value[i];
	              };
	          }
			function remove_bins(source_group, limit,fields) { // (source_group,
				// bins...}
				return {
					all : function(d) {
						if(limit){
							return source_group.top(limit);
						}else{
							return source_group.all().filter(function(d) {
							var isNonZero = false;
							if(fields && fields.length>0){
								for(var i=0;i<fields.length;i++){
									if(d.value[fields[i]] != 0){
										isNonZero = true;
									}										
								}										
							}
							return isNonZero;
							});								
						}
					}
				};
			}
			 try {
				if(dc.chartRegistry.list() && dc.chartRegistry.list().length>0){
					for(var chartIndex=0;chartIndex<dc.chartRegistry.list().length;chartIndex++){
						if(dc.chartRegistry.list()[chartIndex].anchor() === config.html){
							dc.chartRegistry.deregister(dc.chartRegistry.list()[chartIndex]);
						}
					}
				}
				var fields = [];
				var stacks = [];
				var groupXy = dimXY.group();
				if (config.charts && config.charts !== null
						&& config.charts.length > 0) {
					for ( var x = 0; x < config.charts.length; x++) {
						if(config.charts[x].type === 'stack'){ 
							stacks = config.charts[x].group.split(",");
							THIS.stackMeasures= stacks;
						}else if(config.charts[x].type === 'scatter'){							
							fields.push(config.charts[x]["group"]);
						}
						else if (fields.indexOf(config.charts[x]["group"])){
							fields.push(config.charts[x]["group"]);
						}
					}
				};
				var calculatedField = [],labels = [];
				if (config.calculatedfield && config.calculatedfield !== null
						&& $.trim(config.calculatedfield) !== "") {
					calculatedField = config.calculatedfield.split(",");
				}
				if(stacks.length >0){					
					fields = $.merge(fields, stacks,calculatedField);
				}else{
					fields = $.merge(fields, calculatedField);
				}
				var grp1 = dim.group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
						THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue)); 
				var grp = remove_bins(grp1,null,fields);
				var defaultValue = grp.all().length;
				if(config.barwidth && config.barwidth === "auto"){
					chartwidth  =  barWidth;
				}else{
					chartwidth  = config.marginleft+config.marginright+(defaultValue*config.barwidth*fields.length);
				}
				
				if(chartwidth < barWidth)
					chartwidth  =  barWidth-(config.marginleft+config.marginright);
				
				function sel_stack(i,x) {
					return function(d) {
						var stackValue;
						if (config.charts[x].aggregate.toLowerCase() === "sum"){
							stackValue = d.value[i];
						}else if (config.charts[x].aggregate.toLowerCase() === "count"){
							stackValue = d.value.count;
						}else{
							stackValue = d.value[i] / d.value.count;
						}
						return stackValue;
					};
				}
				var charts = [];				
				var compositeBrushchart = dc.compositeChart(config.html);
				if (config.charts && config.charts !== null
						&& config.charts.length > 0) {
					for ( var x = 0; x < config.charts.length; x++) {
						/*
						 * if(keyColorCodes){ colors =
						 * d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes));
						 * }else{ colors = config.charts[x].color; }
						 */
						
						var colors = config.charts[x].color;
						if (config.charts[x].type && config.charts[x].type === "bar") {
							var keyColorCodes;
							config.codetype = "standard";
							config.colorcodes = config.charts[x].color;
							keyColorCodes = THIS.getKeyColorCodeByRange(grp, config);
							var bar = dc.barChart(compositeBrushchart);
							bar.charttype = config.charts[x].type;
							var gap = 5;
                            /*var grpLength = grp.all().length;
                            var rectWidth = (chartwidth-(config.marginleft+config.marginright))/grpLength;
                            if(grpLength<10){
                           	 if(grpLength === 2){
                           		 gap = parseInt(rectWidth/1.5);
                           	 }else{
                           		 gap = parseInt(rectWidth/2);
                           	 }
                            } 
                            if(gap<5)
                           	 gap  =5;*/
							charts.push(bar.dimension(dim)
									.gap(5)
									.colors(config.charts[x].color)
									//.barPadding(0.5)
									/*.clipPadding(20)
									.x(d3.scaleOrdinal())
									.xUnits(dc.units.ordinal).brushOn(false)
									.elasticX(true)*/
									.group(grp,config.charts[x].group).centerBar(true)
					                .serieGap(3)
					                .controlsUseVisibility(true)
									.colors(config.charts[x].color)
									.valueAccessor(THIS.getValueByAgrregate(config.charts[x].aggregate,config.charts[x].group,config.filtervalue,config.absolute))
									// .colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)))
									.colorAccessor(function(d) { 
									 	return d.key;
								    })
									// .ordinalColors([config.charts[x].color])
									);
							
							bar.on('filtered', function(chart,value) {
								$(".reset", $(config.html)).data('isReset',true);
								// compositeBrushchart.filter(value);
								var filters = chart.filters();
								if(filters.length>0){
									$(".reset", $(config.html)).show();
								}else{
									$(".reset", $(config.html)).hide();
								}
								compositeBrushchart.filterValue = filters;
								if(!config.isRedraw){
									$(config.html).data('resetedValue',true);						
									resetAllCharts(THIS,config.html);
									// resetAllProfiles(arguments[0]);
								}
								/*
								 * if(config.defaultFiltervalue.length !==
								 * filters.length){ if(config.renderFunction !==
								 * undefined){ var renderFun =
								 * config.renderFunction.replace(/data/g,
								 * arguments[0].filters().slice()); var
								 * chartRenderFun = new Function( "return
								 * "+renderFun); chartRenderFun(); }
								 * //resetAllProfiles(arguments[0]); }
								 */
							}.bind(this));
							// bar.colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)));
							// if (config.colorcodes) {
							
							/*
							 * .colors(d3.scaleOrdinal().domain(d3.keys(keyColorCodes)).range(d3.values(keyColorCodes)))
							 * .colorAccessor(function(d) { return d.key; }));
							 */
							// }
						}else if (config.charts[x].type && config.charts[x].type === "stack") { 
	                         var stackTotal = {};
                             var gap = 5;
                             /*var grpLength = grp.all().length;
                             var rectWidth = (chartwidth-(config.marginleft+config.marginright))/grpLength;
                             if(grpLength<7){
                            	 if(grpLength === 2){
                            		 gap = parseInt(rectWidth/1.5);
                            	 }else{
                            		 gap = parseInt(rectWidth/2);
                            	 }
                             } 
                             if(gap<13)
                            	 gap  =13;*/
                             var stackGroup = config.charts[x].group.split(",");
                             var bar = dc.barChart(compositeBrushchart).dimension(dim).group(grp,stackGroup[0], sel_stack1(stackGroup[0]))
                             .centerBar(true).gap(5).valueAccessor(THIS.getValueByAgrregate(config.charts[x].aggregate,stackGroup,config.filtervalue,config.absolute))
                             .colors([config.charts[x].color]);
                             bar.charttype = config.charts[x].type;
                             // bar.barPadding()
                             for ( var i = 1; i < stacks.length; i++) {
                                 var stack = stacks[i];
                                 bar.stack(grp, stackGroup[i], sel_stack1(stackGroup[i]));
                             }
                             bar.on('filtered', function(chart,value) { 
             					$(".reset", $(config.html)).data('isReset',true);
             					var filters = chart.filters();
             					if(filters.length>0){
             						$(".reset", $(config.html)).show();
             					}else{
             						$(".reset", $(config.html)).hide();
             					}
             					compositeBrushchart.filterValue = filters;
             					if(!config.isRedraw){
             						$(config.html).data('resetedValue',true);						
             						resetAllCharts(THIS,config.html);
             						 // resetAllProfiles(arguments[0]);
             					}
             					/*
								 * if(config.defaultFiltervalue.length !==
								 * filters.length){ if(config.renderFunction !==
								 * undefined){ var renderFun =
								 * config.renderFunction.replace(/data/g,
								 * arguments[0].filters().slice()); var
								 * chartRenderFun = new Function( "return
								 * "+renderFun); chartRenderFun(); }
								 * //resetAllProfiles(arguments[0]); }
								 */
             				}.bind(this));
                             charts.push(bar);
						} else if (config.charts[x].type && config.charts[x].type === "scatter") {
							/*
							 * var stackTotal = {}; // var group =
							 * dims[config.charts[x].group].group(); var group =
							 * dims[config.charts[x].group].group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
							 * THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
							 * THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
							 * 
							 * var scatterChart =
							 * dc.scatterPlot(compositeBrushchart).dimension(dims[config.charts[x].group]).group(remove_bins(group,null,config.charts[x].group),config.charts[x].group)
							 * .symbol(d3[config.charts[x].symbol])
							 * .ordinalColors([config.charts[x].color])
							 * .symbolSize(config.charts[x].symbolsize).excludedOpacity(0.5); //
							 * .valueAccessor(THIS.getValueByAgrregate(config.charts[x].aggregate,config.charts[x].group,config.filtervalue,config.absolute));
							 * charts.push(scatterChart);
							 */
							
							var stackTotal = {};
							// var group = dims[config.charts[x].group].group();
							var group = dims[config.charts[x].group].group().reduce(THIS.reduceFieldsAdd(fields,labels,config.filter,config.filtervalue),
									THIS.reduceFieldsRemove(fields,labels,config.filter,config.filtervalue),
									THIS.reduceFieldsInitial(fields,labels,config.filter,config.filtervalue));
							
							var scatterChart = dc.scatterPlot(compositeBrushchart).dimension(dims[config.charts[x].group]).group(remove_bins(group,null,[config.charts[x].group]),config.charts[x].group)
							.symbol(d3[config.charts[x].symbol]).ordinalColors([config.charts[x].color])
							.symbolSize(config.charts[x].symbolsize).excludedOpacity(0.5);
							// .valueAccessor(THIS.getValueByAgrregate(config.charts[x].aggregate,config.charts[x].group,config.filtervalue,config.absolute));
							charts.push(negativeMin(scatterChart,config.charts[x].group));
						} else {
						function add_origin(group) { 
							  return {
							    all: function() {
							      return (grp1.all());
							      
							    }
							  };
							}
						    grp=add_origin(grp1);
							charts.push(dc.lineChart(compositeBrushchart).dimension(dim)
									.colors(config.charts[x].color).group(grp,config.charts[x].group)
											.interpolate(config.interpolate)
											// .renderDataPoints({radius: 2,
											// fillOpacity: 0.8, strokeOpacity:
											// 0.0})
											 .renderDataPoints(config.renderdatapoints)
											 .renderArea(config.renderarea)
											 .renderDataPoints({radius: config.charts[x].symbolsize})
											 .dashStyle(config.charts[x].dash.split(","))
									.valueAccessor(
											THIS.getValueByAgrregate(
													config.charts[x].aggregate,
													config.charts[x].group,config.filtervalue,config.absolute)));
						}
					}
				}
				
				 var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){  
					  var keyValue,symbolValue;
					  var data;
					  if(d.data){
						  data = d.data.value;
						  keyValue = d.data.key;
					  }else if(d.value){ 
						  data = d.value;
						  if(d.key && d.key.length>0){
							  keyValue = d.key[0];
							  if(d.key[1])
								  symbolValue = parseInt(d.key[1]);
						  }
					  }
					  var toolTip= $("<div/>");
					  var table = $('<table/>').attr({"class":"d3-tip-table"}).append($('<tr/>').append($('<td/>').html( keyValue +':').attr({"class":"d3-tip-key"}))
							   .append($('<td/>').html("").attr({"class":"font-bold"})));
					   
					   if(config.charts && config.charts.length>0){
						   for(var j=0;j<config.charts.length;j++){
								// if(data[config.charts[j].group] !==
								// undefined){
							   		var totalValue = 0;
									var className = "";
									if (config.charts[j].type == "stack" && config.charts[j].group){
										var stackGroup = config.charts[j].group.split(",");
										var measureName = JSON.parse(config.charts[j].name)
										for (var x=0; x< stackGroup.length; x++){
											if(data[stackGroup[x]] !== undefined && measureName[stackGroup[x]] !=="."){
												if (d.layer === stackGroup[x]){
													color = "#ffd700";
												} else{
													color = "#fff";
												}
												var value = data[stackGroup[x]];
												totalValue = parseInt(totalValue) + parseInt(value) ;
												if(config.numFormat && config.numFormat.trim() !== "" && value){
													value = d3.format(config.numFormat)(data[stackGroup[x]]);
													value+=config.suffix? config.suffix:"";
												}
												table.append($("<tr/>")
														.append($("<td/>").html(measureName[stackGroup[x]] || stackGroup[x]).css("color",color))
														.append($("<td/>").html(value).css("color",color)));
											}
										}
										
										if(config.showtotal == "yes"){
											table.append($("<tr/>")
													.append($("<td/>").html("Total").css("color",color))
													.append($("<td/>").html(d3.format(config.numFormat)(totalValue)).css("color",color)));
										}
									} else if(data[config.charts[j].group] !== undefined){
										if(symbolValue && symbolValue === data[config.charts[j].group]){
											className = "label-primary";
										}
										if (this.getAttribute('fill') === config.charts[j].color || d.layer === config.charts[j].group){
											color = "#ffd700";
										} else{
											color = "#fff";
										}
										var value = data[config.charts[j].group];
										if(config.numFormat && config.numFormat.trim() !== "" && value){
											value = d3.format(config.numFormat)(data[config.charts[j].group]);
											value+=config.suffix? config.suffix:"";
										}
										table.append($("<tr/>")
												.append($("<td/>").html(config.charts[j].name || config.charts[j].group).css("color",color))
												.append($("<td/>").html(value).css("color",color)));
									}
								// }
							}
					   };
						if (config.linkaction){
							var actionDiv = $("<div/>");
							actionDiv.append($('<hr />').attr({"class":"no-margin-bottom"}));
							var actions = config.linkaction.split(",");
							for (var i = 0;i<actions.length;i++){
								actionDiv.append($('<div/>').html(actions[i]).attr({'class':'tooltip-action','title':'Go to "'+ actions[i]+'"','onclick':'dataAnalyser.tooltipActionClick(this)'}));
							}
							toolTip.append(actionDiv);
						}
						toolTip.append(table);
						/* changing code here */
						var anchorTag=$('<a/>').html('keep-only').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-check txt-color-greenLight"}));
						$(anchorTag).attr('onclick', 'System.filterChart("'+ config.html +'","'+keyValue+'")');
						var excludeAnchor=$('<a/>').html('Exclude').attr({"class":"tooltip-toolbar-action"}).append($('<i/>').attr({"class":"fa fa-times txt-color-red" }));
						$(excludeAnchor).attr('onclick','System.filterChart("'+ config.html +'","'+ keyValue +'",true)');
						var divTag=$('<div/>').attr({"class":"tooltip-toolbar"});
						divTag.append(anchorTag).append(excludeAnchor);
						toolTip.append(divTag);
						return toolTip[0].outerHTML;
					});
				 
				
				/*
				 * compositeBrushchart.title(function () { return "";
				 * }).width(barWidth).margins({ top : config.margintop, left :
				 * config.marginleft, right : config.marginright, bottom :
				 * config.marginbottom }) .group(grp,"dummy") //
				 * .x(d3.scaleLinear().domain([-1000000,35000000]))
				 * //.xUnits(function(){return 30;})
				 * .renderHorizontalGridLines(true) .height(barHeight)
				 * .x(d3.scaleBand()) .xUnits(dc.units.ordinal)
				 * //.x(d3.scaleOrdinal()).xUnits(dc.units.ordinal)
				 * ._rangeBandPadding(1) // workaround to fix visual stuff (dc
				 * js // issues) .xAxisLabel(config.xaxislabel ?
				 * config.xaxislabel : '') .yAxisLabel(config.yaxislabel ?
				 * config.yaxislabel : '') .clipPadding(10) .dimension(dim)
				 * .group(grp) .brushOn(false) .elasticY(true) //
				 * .yAxis().ticks(10).tickFormat(d3.format(",~s"))
				 * .compose(charts);
				 */
				 compositeBrushchart.width(chartwidth)
				 ._rangeBandPadding(config.rangebandpadding == 'no'?0:1)
				 .title(function () { return ""; }).width(chartwidth).margins({
						top : config.margintop,
						left : config.marginleft,
						right : config.marginright,
						bottom : config.marginbottom
					})
					.clipPadding(0)
				  .height(barHeight)
				   .renderHorizontalGridLines(true)
				  .x(d3.scaleOrdinal()).xUnits(dc.units.ordinal)
				  .shareColors(true)
				  .brushOn(false)
				  .xAxisLabel(config.xaxislabel ? config.xaxislabel : '')
				.yAxisLabel(config.yaxislabel ? config.yaxislabel : '')
				  .dimension(dim)
				   .elasticY(true)
				  // .elasticX(true)
				  .group(grp, 'Dummy group to make ordinal composite charts work')
				  .compose(charts)
				  .yAxis().ticks(10).tickFormat(function(d) {
					    return d % 1 ? null : d3.format(",~s")(d);
				  });
				 // .tickFormat(d3.format(",~s"))
				  /*
					 * compositeBrushchart.on('filtered', function() {
					 * resetAllCharts(THIS, config.html); $(".reset",
					 * $(config.html)).data('isReset',true);
					 * resetAllProfiles(arguments[0]); }.bind(this));
					 */
				/*
				 * charts.forEach(function(chart){ chart.on('filtered',
				 * function(chart,value) { //compositeBrushchart.filter(value);
				 * resetAllCharts(THIS, config.html);
				 * resetAllProfiles(arguments[0]); $(".reset",
				 * $(config.html)).data('isReset',true); }.bind(this)); });
				 */
					compositeBrushchart.on("renderlet",function(chart){						
						chart.selectAll('rect.bar').on('click.custom', function(d) {
							if(charts && charts.length>0){
								for(var i=0;i<charts.length;i++){
									if(charts[i].charttype === "bar" || charts[i].charttype === "stack"){
										resetAllProfiles(charts[i]);
										break;
									}
								}
							}
					    	
					    });
							chart.selectAll("rect.bar").attr("fill", function(d){
								var color;
								var selectedBarChart = config.charts.filter(function(o){return o.type === "bar"});
								var selectedStackChart = config.charts.filter(function(o){return o.type === "stack"});
								var selectedChart;
								if(selectedBarChart.length>0){
									selectedChart = config.charts.filter(function(o){return o.group === d.layer});
									if(selectedChart.length>0){
										color = selectedChart[0].color;
									}
								}else if(selectedStackChart.length>0){
									var colorCodes;
									for(var i=0;i<config.charts.length;i++){
										if(config.charts[i].type === "stack"){
											if(config.colorcodes && config.colorcodes["color"+(i+1)]){
												colorCodes = config.colorcodes["color"+(i+1)].split(",");
											}
											var stackGroup = config.charts[i].group.split(",");
											if(stackGroup && stackGroup.length>0){
												for(var j=0;j<stackGroup.length;j++){
													if(stackGroup[j] === d.layer){
														if(colorCodes && colorCodes.length>0 && colorCodes[j]){
															color = colorCodes[j];
														}else{
															color = config.charts[i].color;
														}
													}
												}
											}
										}
									}
								}
								return color;
							});
							chart.selectAll(".dc-legend-item").selectAll("path").remove();
							chart.selectAll(".dc-legend-item").append("path")
							  .attr("d",function(d,i){
								  var selectedScatter = config.charts.filter(function(o){return o.type === "scatter" && o.group === d.name});
								  if(selectedScatter && selectedScatter.length>0){
									  $(this.parentElement).find("rect").hide();
									  return d3.symbol().size(100).type(d3[selectedScatter[0].symbol])(i);
								  }
							  } )
							  .attr("transform", function(d,y) {
								  var selectedScatter = config.charts.filter(function(o){return o.type === "scatter" && o.group === d.name});
								  if(selectedScatter && selectedScatter.length>0){
									  $(this).parent().find("line").hide();
									  y = 9;
									  return "translate(" + x + "," + y + ")";
								  }	
							  	})
							  .style('fill', function(d,i) {
								  var selectedScatter = config.charts.filter(function(o){return o.type === "scatter" && o.group === d.name});
								  if(selectedScatter && selectedScatter.length>0){
									  return selectedScatter[0].color;
								  }
							  });
							chart.selectAll(".line").attr("stroke", function(d){
								var selectedlineChart = config.charts.filter(function(o){return o.group === d.name});
								var color;
								if(selectedlineChart && selectedlineChart.length>0){
									color = selectedlineChart[0].color;
								}
								else{
									color = "#fff";
								}
								 return color;
							});
							chart.selectAll(".dot").attr("fill", function(d){
								var selectedlineChart = config.charts.filter(function(o){return o.group === d.layer});
								var color;
								if(selectedlineChart && selectedlineChart.length>0){
									color = selectedlineChart[0].color;
								}
								else{
									color = "#fff";
								}
								 return color;
							});
							chart.selectAll(".symbol").attr("fill", function(d){
								var color;
								if(d && d.key && d.key.length>0 && d.key[2])
									color = d.key[2];
								else{
									color = "#fff";
								}
								 return color;
							});
							if (config.showtotal === "yes") {
								moveGroupNames(chart);
							}
						});
				
				 compositeBrushchart.legend(dc.legend().x(parseFloat(config.legendxposition))
						 .legendWidth(chartwidth).horizontal(config.legendhorizontal)
						 .autoItemWidth(true).y(parseFloat(config.legendyposition)).itemHeight(12)
				  .gap(15).legendText(function (d) { 
				  var legendName;
				  for (var i=0;i<config.charts.length;i++){
					  if (config.charts[i].type == "stack" && config.charts[i].group){
							var measureName = JSON.parse(config.charts[i].name);
							var stackGroup = config.charts[i].group.split(",");
							var colorCodes;
							if(config.colorcodes && config.colorcodes["color"+(i+1)]){
								colorCodes = config.colorcodes["color"+(i+1)].split(",");
							}
							for (var x=0; x< stackGroup.length; x++){
								if( d.name === stackGroup[x]){
									legendName = measureName[stackGroup[x]] || stackGroup[x];
									 if (config.charts[i].type != "line"){
										 $(this).prev().attr("strokeheight",12);
										 $(this).prev().attr("strokewidth",12);
										 if(colorCodes && colorCodes.length>0 && colorCodes[x]){
											 $(this).prev().attr({"stroke":colorCodes[x],"fill":colorCodes[x]});
										 }
									 }
									 if (legendName==="."){
										 $(this).prev().remove();
									 }
									  return legendName==="."?"":legendName;
								}
							}
						} else if (d.name === config.charts[i].group){
						  legendName = config.charts[i].name;
						  $(this).prev().attr("fill",config.charts[i].color);
						  $(this).prev().attr("stroke",config.charts[i].color);
						  if (config.charts[i].type === "scatter"){
							  $(this).prev().attr("stroke-height",12);
							  $(this).prev().attr("stroke-width",12);
						  }
						  return legendName==="."?"":legendName;
					  }
					  
				  }
				  }));
				
				$(".reset", $(config.html)).off('click').on('click', function(e) {
					compositeBrushchart.filterAll();
					compositeBrushchart.filterValue = [];
					charts.forEach(function(chart){						
						if(chart.filters() && chart.filters().length>0){
							chart.filterAll();
						}						
					});
					for (var scateerDim in dims) {
						dims[scateerDim].filterAll()
					}
					$(e.currentTarget).removeData("isReset");
					$(config.html).data('resetedValue',true);
					resetAllCharts(THIS);
					resetAllProfiles(compositeBrushchart);
				}.bind(this));
				THIS.drawnGraph.push(compositeBrushchart);
				compositeBrushchart.render();
				if(config.defaultFiltervalue){
					charts.forEach(function(chart){	
						var defaultFiltervalue = config.defaultFiltervalue.split(",");
						defaultFiltervalue.forEach(function(d) {
							d= d.replace('`',',')
							chart.filter(d);
						});				
					});
				}
				// if(config.defaultFiltervalue){
					// applyDefaultFilter(config,compositeBrushchart);
				// }
				if (d3.selectAll(config.html).selectAll(".bar,.symbol,.dot") && d3.selectAll(config.html).selectAll(".bar,.symbol,.dot")._groups[0].length){ 
					d3.selectAll(config.html).selectAll(".bar,.symbol,.dot").call(tip);
					d3.selectAll(config.html).selectAll(".bar,.symbol,.dot").on('mouseover', tip.show);
				}
				/*
				 * if (d3.selectAll(config.html).selectAll(".symbol")){
				 * d3.selectAll(config.html).selectAll(".symbol").call(tip);
				 * d3.selectAll(config.html).selectAll(".symbol").on('mouseover',
				 * tip.show); } if
				 * (d3.selectAll(config.html).selectAll(".dot")){
				 * d3.selectAll(config.html).selectAll(".dot").call(tip);
				 * d3.selectAll(config.html).selectAll(".dot").on('mouseover',
				 * tip.show); }
				 */
				compositeBrushchart.on('renderlet.add-tip', function(chart) {
				    chart.selectAll('.bar,.symbol,.dot')
				        .call(tip)
				        .on('mouseover', tip.show);
				       
				});
			  } catch (e) { 
				  console.log('Error Creating Composite Brush Chart' + e); 
			  }
			 
		};
		function negativeMin(chart,groupName) {
		    dc.override(chart, 'yAxisMin', function () {
		         var min = d3.min(chart.data(), function (d) {
		        	 return d.value[groupName];
		         });
		         // if(min<0){
			         return dc.utils.subtract(min*1.5, chart.yAxisPadding());		        	 
		         // }
		    });
		    return chart;
		}
		function moveGroupNames(chart) {
			var gLabels = chart.select(".labels");
			if (gLabels.empty()) {
				gLabels = chart.selectAll(".sub").select(".chart-body").append('g').classed(
						'labels', true);
			}
			var gLabelsData = gLabels.selectAll("text").data(chart.selectAll(".bar"));
			gLabelsData.exit().remove(); // Remove unused elements
			gLabelsData.enter().append("text"); // Add new elements
			gLabelsData
					.attr('text-anchor', 'middle')
					.attr('fill', 'white')
					.text(
							function(e) {
								var obj = d3.select(e).data()[0];
								var chartConfig = config.charts[obj.layer];
								var d = obj.data;
								var tempValue;
								
								if (chartConfig.aggregate.toLowerCase() === "sum")
									tempValue = d.value[chartConfig.group];
								else if (chartConfig.aggregate.toLowerCase() === "count")
									tempValue = d.value.count;
								else if (chartConfig.aggregate.toLowerCase() === "calculated")
									tempValue = eval(config.calculatedformula);
								else
									tempValue = d.value[chartConfig.group]
											/ d.value.count;
								var prefix = d3.formatPrefix(tempValue);// d3.select(d).data()[0].data.value
								var prefixScale = Math.round(prefix
										.scale(tempValue), 0);
								return prefixScale + prefix.symbol;
							}).attr(
							'x',
							function(d) {
								return +d.getAttribute('x')
										+ (d.getAttribute('width') / 2);
							}).attr('y', function(d) {
						return +d.getAttribute('y') + 15;
					}).attr('style', function(d) {
						if (+d.getAttribute('height') < 18)
							return "display:none";
					});	
		}
		this.refreshCompositeBrush = function(currentConfigHtml) {
			for ( var x = 0; x < this.compositeBrushchart.length; x++) {
				if (!currentConfigHtml || this.compositeBrushchart[x]["config"].html !== currentConfigHtml) {
					renderCompositeBrush(this.compositeBrushchart[x]["config"],this.compositeBrushchart[x]["dimX"],this.compositeBrushchart[x]["dims"]);
				}
			}
		};
		renderCompositeBrush(config,dimX,dims);
		
	};
};