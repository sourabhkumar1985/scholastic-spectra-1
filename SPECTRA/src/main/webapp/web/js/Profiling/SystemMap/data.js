
/**
 *
 * DATA SOURCE:  http://www.fec.gov/data/index.jsp/
 *
 */

/**
 * Daisy Chain Data Fetches to ensure all data is loaded prior to updates (async calls)
 */

// var dataDispatch=d3.dispatch("end");
var dataCalls=[];
var numCalls=0;

function fetchData() {
    dataCalls=[];
//    dataDispatch.on("end",onDataFetched)
    addStream("data/innernode.csv", onFetchInnerNode);
    //addStream("data/Candidates_Senate.csv", onFetchCandidatesSenate);
    addStream("data/links.csv", onFetchLinks);
    //addStream("data/Contributions_Senate.csv", onFetchContributionsSenate);
    addStream("data/outernode.csv", onFetchOuterNode);
    //addStream("data/Pacs_Senate.csv", onFetchPacsSenate);
    startFetch();
}

function onFetchInnerNode(csv) {
    for (var i=0; i < csv.length; i++) {
        var r=csv[i];
        r.value=Number(r.Amount);
        cns[r.name]=r;
        house.push(r);
            h_reps.push(r);
            total_hReps+= r.value;
    }
    log("onFetchCandidatesHouse()");
    endFetch();
}

function onFetchLinks(csv) {
    var i=0;
    csv.forEach(function (d) {
        d.Key="H"+(i++);
        contributions.push(d);
        c_house.push(d);
    });

    log("onFetchContributionsHouse()");
    endFetch();

}

function onFetchOuterNode(csv) {

    pacsHouse=csv;
    for (var i=0; i < pacsHouse.length; i++) {
        pacsById["house_" + pacsHouse[i].sender]=pacsHouse[i];
    }

    log("onFetchPacsHouse()");
    endFetch();


}


function addStream(file,func) {
    var o={};
    o.file=file;
    o.functio=func;
    dataCalls.push(o);
}

function startFetch() {
    numCalls=dataCalls.length;
    dataCalls.forEach(function (d) {
        d3.csv(d.file, d.functio);
    })
}

function endFetch() {
    numCalls--;
    if (numCalls==0) {
       // dataDispatch.end();
        main();
    }
}
