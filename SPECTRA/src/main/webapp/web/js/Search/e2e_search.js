pageSetUp();
$("#searchterm").focus();
var searchSolr;

var searchSolrInstance = function() {
	searchSolr = new searchSolrObj();
};
var searchSolrObj = function() {
	$('.dropdown-menu input, .dropdown-menu label').click(function(e) {
		e.stopPropagation();
	});
	$("[id*=count]").html(0);
	var THIS = this;
	$("#searchForm").submit(function(e) {
		e.preventDefault();
	});
	$("#search,#searchAdv").click(function() {
		searchSolr.Load();
	});
	$("#entities li").unbind('click').bind('click', function() {
		searchSolr.entitySearch(this);
	});
	var tableColumns = {
		"ebkn" : "banfn",
		"kna1" : "kunnr",
		"lfa1" : "lifnr",
		"mara" : "matnr",
		"makt" : "matnr",
		"eban" : "banfn",
		"marc" : "matnr",
		"knb1" : "kunnr",
		"f03012" : "aian8",
		"f0401" : "a6an8",
		"f4101" : "imlitm",
		"pm00200" : "vendorid",
		"rm00101" : "custnmbr",
		"iv00101" : "itemnmbr",
		"f0006" : "mcmcu",
		"f0010" : "ccco",
		"f0101" : "aban8",
		"f4801" : "wadoco",
		"afko" : "aufnr",
		"afpo" : "aufnr",
		"t001w" : "werks",
		"sy01500" : "cmpanyid",
		"iv40700" : "locncode"
	};
	THIS.datasource = "SAP";
	THIS.facet = "ekko";
	this.Load = function(source) {
		THIS.solrCollection = {
			GP : "SOLR_SERVER_GP_INSTANCE_NAME",
			JDE : "SOLR_SERVER_JDE_INSTANCE_NAME",
			SAP : "SOLR_SERVER_SAP_INSTANCE_NAME"
		};
		$("#entitySelectMsg").addClass("hide");
		$("#entities").empty();
		$("#resultGrid div[id^=faq]").remove();
		THIS.source = source || $("#myTab").find("li.active").data("source")
				|| "SAP";
		enableLoading();
		THIS.key = $("#searchterm").val();
		$.ajaxQ.abortAll();
		$("[id*=count]").html(0);
		$(".facets-tabs").show();
		$(".searchkey").empty();
		$(".searchkey").append(THIS.key);
		$(".totalSearchCount").html('');
		$("#mainDivId").addClass("hide");
		$("#wid-id-data").removeClass("hide");
		var searchInDisplaycolumns = false;
		searchInDisplaycolumns = $("[name=checkbox-inline]").is(':checked');
		var request = {
			"key" : THIS.key,
			"source" : THIS.source,
			"solrcollectionname" : THIS.solrCollection[THIS.source],//'SOLR_SERVER_'+THIS.source+'_INSTANCE_NAME',
			"searchInDisplaycolumns" : searchInDisplaycolumns
		};
		callAjaxService("search", callBackSearchResult, callBackFailure,
				request, "POST");
	};

	var callBackSearchResult = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		$("#entitySelectMsg,#mainDivId").removeClass("hide");
		$("#wid-id-20, #resultGrid").show();
		$
				.each(
						response,
						function(key, data) {
							key = key.toLowerCase();
							var keyDisplayName = lookUpTable.get(key);
							if (keyDisplayName == undefined
									|| keyDisplayName == null
									|| keyDisplayName == "")
								keyDisplayName = key;
							var liEntity = $("<li/>").attr({
								id : "li" + key
							}).data({
								'facet' : key
							}).unbind('click').bind('click', function() {
								searchSolr.entitySearch(this);
							}).appendTo("#entities");
							liEntity.append($("<a/>").attr({
								"href" : "#faq-" + key,
								"data-toggle" : "tab"
							}).append($("<span/>").attr({
								"id" : "loading-" + key
							})).append(
									keyDisplayName + " (<span class='" + key
											+ "count' >" + data + "</span>)"));

							var tabPaneDiv = $("<div/>").addClass(
									"tab-pane in fade").attr({
								"id" : "faq-" + key
							}).appendTo("#resultGrid");
							tabPaneDiv
									.append($("<div/>")
											.addClass("panel-group")
											.attr({
												"id" : "accordion" + key
											})
											.append(
													$("<div/>")
															.addClass(
																	"panel panel-default panel-faq")
															.append(
																	$("<div/>")
																			.addClass(
																					"panel-body")
																			.append(
																					$(
																							"<div/>")
																							.addClass(
																									"teaser")
																							.append(
																									$(
																											"<span/>")
																											.attr(
																													{
																														"id" : "tittle"
																													})
																											.html(
																													"<span id='statusMsg'>Loading </span> Results For"
																															+ "<span class='searchkey'>"
																															+ THIS.key
																															+ "</span> from <span class='totalSearchCount'>"
																															+ keyDisplayName
																															+ "</span>")))
																			.append(
																					$(
																							"<table/>")
																							.attr(
																									{
																										"id" : key
																												+ "Table"
																									})
																							.addClass(
																									"display  table table-hover table-striped")
																							.append(
																									$(
																											"<thead/>")
																											.append(
																													$("<tr/>")))))));

							/*
							 * $("#"+key+"Table") .dataTable({ "aaData" : null,
							 * "bDestroy" : true, "oLanguage" : { "sZeroRecords" :
							 * "No records to display" }, "bFilter" : false });
							 */
						});
		/*
		 * if ($("#"+THIS.facet+"count") != 0){ var request = { "key": THIS.key,
		 * "facet": THIS.facet.toUpperCase() }; //
		 * callAjaxService("searchEntity",
		 * callBackEntitySearchResult,callBackFailure,request,"POST"); }
		 */
	};
	this.entitySearch = function(obj) {
		$("#statusMsg").html("Loading");
		$("#entitySelectMsg").addClass("hide");
		THIS.datasource = $("#myTab").find("li.active").data("source");
		THIS.facet = $(obj).data("facet");
		var count = $("." + THIS.facet + "count").html();
		var searchInDisplaycolumns = false;
		searchInDisplaycolumns = $("[name=checkbox-inline]").is(':checked');
		if (count != "0") {
			enableLoading();
			var request = {
				"key" : THIS.key,
				"facet" : THIS.facet.toUpperCase(),
				"source" : THIS.source,
				"solrcollectionname" : THIS.solrCollection[THIS.source],
				"searchInDisplaycolumns" : searchInDisplaycolumns
			};
			callAjaxService("searchEntity", callBackEntitySearchResult,
					callBackFailure, request, "POST");
		} else {
			$("#" + THIS.facet + "Table thead tr").append("<th/>");
			$("#" + THIS.facet + "Table").dataTable({
				"data" : null,
				"destroy" : true,
				"language" : {
					"zeroRecords" : "No records to display"
				},
				"filter" : false
			});
		}
	};

	var callBackEntitySearchResult = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		$("#statusMsg").html("Showing");
		try {
			var colosFunction = [];
			var exportColumn = [];
			var coloum_config = Object.keys(response[0]);
			for ( var i = 0, idx = coloum_config.length; i < idx; i++) {

				var fun = new Function('obj', 'return obj.data.'
						+ coloum_config[i].trim());
				var colTitle = lookUpTable.get(THIS.facet + "-"
						+ coloum_config[i].trim());
				if (colTitle == null) {
					colTitle = coloum_config[i].trim();
				}
				var showCol = true;

				if (coloum_config[i].trim() == "key")
					showCol = false;
				else
					exportColumn.push(i);

				colosFunction.push({
					"data" : coloum_config[i].trim(),
					"defaultContent" : "",
					"sortable" : true,
					"title" : colTitle,
					"visible" : showCol,
					"render" : function(data, type, full) {
						if (data != null && data != "")
							var highlightData = highlight(data, THIS.key);
						var temp = $("<td/>").html(highlightData);
						return temp.html();
					}
				});
			}
			var facetTable = $("#" + THIS.facet + "Table")
					.dataTable(
							{
								"language" : {
									"zeroRecords" : "No records to display"
								},
								"dom" : "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"
										+ "t"
										+ "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"filter" : false,
								"info" : true,
								"data" : response,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ {
											"sExtends" : "pdf",
											"mColumns" : exportColumn
										}, {
											"sExtends" : "csv",
											"mColumns" : exportColumn
										} ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"rowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									var key = "";
									if (aData.key) {
										key = aData.key.split("-");
										if (key.length > 2)
											key = key[2];
										else if (key.length > 1)
											key = key[1];
									}
									if (THIS.facet == "ekko"
											|| THIS.facet == "ekpo"
											|| THIS.facet == "f4311"
											|| THIS.facet == "f4301"
											|| THIS.facet == "pop30110"
											|| THIS.facet == "pop30100") {
										$(nRow)
												.attr(
														{
															'onClick' : "javascript:thirdLevelDrillDown('po', '','"
																	+ key
																	+ "','"
																	+ THIS.datasource
																	+ "')",
															'data-target' : '#myModalthredlevel',
															'data-toggle' : 'modal'
														});
									} else if (THIS.facet == "vbap"
											|| THIS.facet == "vbak"
											|| THIS.facet == "f4201"
											|| THIS.facet == "f4211"
											|| THIS.facet == "sop30200"
											|| THIS.facet == "sop30300") {
										$(nRow)
												.attr(
														{
															'onClick' : "javascript:thirdLevelDrillDownSO( '','"
																	+ key
																	+ "','1','"
																	+ THIS.datasource
																	+ "')",
															'data-target' : '#myModalthredlevel',
															'data-toggle' : 'modal'
														});
									} else {
										var criteria = tableColumns[THIS.facet]
												+ ':' + key;
										var key = "",criteria;
										if (aData.key){
											key = aData.key.split("|");
											if (key.length > 2){
												key.splice(0, 2);
												criteria = key.toString();
											}	
											else if (key.length > 1){
												key.splice(0, 1);
												criteria = key.toString();
											}
										}
										var payload = "source=hive&database=" + System.dbName + "&table="
										+ THIS.facet + "&criteria=" + criteria;
										/*var payload = "entitytype="
												+ System.dbName + "."
												+ THIS.facet + "&criteria="
												+ criteria;*/
										$(nRow)
												.attr(
														{
															'onClick' : "javascript:detailDrillDown('"
																	+ payload
																	+ "', '"
																	+ THIS.facet
																	+ "', '"
																	+ criteria
																	+ "','"
																	+ THIS.facet
																	+ "')",
															'data-target' : '#myModalthredlevel',
															'data-toggle' : 'modal'
														});
									}

								},
								"destroy" : true,
								"columns" : colosFunction
							});
		} catch (err) {

		}
	};
};
searchSolrInstance();