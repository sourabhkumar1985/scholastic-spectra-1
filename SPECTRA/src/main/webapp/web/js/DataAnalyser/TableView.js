var TableView = function() {
	var table = null, filterCol = null,vendorCol=null;
	var init = function() {
		$("#vendor").hide();
		pageSetUp();
		constructDropDown(
				"[name='SelectEmFile']",
				"select distinct em_interface_type,'' from lpfg_stg.EM_VENDOR_FILE_MAPPING where lower(em_interface_type) like '%<Q>%' order by em_interface_type",
				"", "", function(e) {
					onChangeOfFile(e);
				}, "Select File", 'hive', 'EMEA_STG');
	};
	
	function onChangeOfFile(e) {
		$("#filterDiv").hide();
		$('#tableDiv').hide();
		$("#vendor").show();
		var filename = $("[name='SelectEmFile']").find("select").val() ? $(
				"[name='SelectEmFile']").find("select").val() : '';
		constructDropDown(
				"[name='SelectVendor']",
				"select distinct mapping.vendor_id,pl.plant_name,pl.plant_city from lpfg_stg.EM_VENDOR_FILE_MAPPING  mapping left outer join lpfg_stg.plant pl ON ( sap_plantcode = concat('US-',vendor_id)) where vendor_id like '%<Q>%'  and lower(em_interface_type)='"
						+ filename.toLowerCase() + "'order by vendor_id", "",
				"", function(e) {
					onChangeOfVendor(e);
				}, "Select Vendor", 'hive', 'EMEA_STG');
		$("#callProfile").hide();
		$("#proceed").show();
	}

	function onChangeOfVendor(e) {
		var filename = $("[name='SelectEmFile']").find("select").val() ? $(
				"[name='SelectEmFile']").find("select").val() : '';
		var vendorId = $("[name='SelectVendor']").find("select").val() ? +$(
				"[name='SelectVendor']").find("select").val() : '';
		var request = {
			source : "hive",
			database : "EMEA_STG",
			table : "EM_VENDOR_FILE_MAPPING",
			"columns" : "em_base_table,creation_date_column,fg_mfg_site_column",
			"filters" : "lower(em_interface_type)='" + filename.toLowerCase()
			+ "'  and vendor_id='" + vendorId + "'"
		};
		enableLoading();
		callAjaxService("getData", callBackSuccessGetData, callBackFailure,
				request, "POST", null, true);
	}

	var callBackSuccessGetData = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			if (response.data.length > 0) {

				table = response.data[0].em_base_table;
				filterCol = response.data[0].creation_date_column;
				vendorCol = response.data[0].fg_mfg_site_column;
				var vendorId = $("[name='SelectVendor']").find("select").val() ? +$(
				"[name='SelectVendor']").find("select").val() : '';
				if(vendorCol && vendorCol !== undefined){
				if (table) {
					$("#filterDiv").show();
					constructDropDown("[name='SelectDate']", "select distinct "
							+ filterCol + ",'' from lpfg_stg." + table
							+ " where "+vendorCol+" like '%"+vendorId+"%' and lower(" + filterCol.toLowerCase()
							+ ") like '%<Q>%' order by " + filterCol +" Desc", true, "",
							"", "Select Date", 'hive', 'EMEA_STG');
					$("#callProfile").show();
				}
			}else{
				$("#filterDiv").hide();
			$('#tableDiv').hide();
				showNotification("error", "Problem in selected filter");
				return;
			}
			}
		}
	}
	$("#proceed")
			.unbind("click")
			.bind(
					"click",
					function() {
					
						if (table) {
							$('#tableDiv').show();
							$('#tableDiv').empty();
							enableLoading();
							var actionCallback = "downloadExcel()";
							var str = '<header role="heading"> <h2>Data Table</h2> <div class="btn-group floatRight" id="actionPlaceHolder"><button type="button" id="downloadExcel" title="Download data in XLS format" class="btn marginRight10 btn-success"  data-header="excel" data-datatable="#datatable_1"><i class=@fa fa-download@></i> Excel</button></div> </header>';// </header>';
							str += '<table id="datatable_1" class="table table-striped table-hover dc-data-table smart-form dataTable no-footer" style="width: 100%"></table>';
							$('#tableDiv').append(str);
							getColumn(table);

						}
					});

	var downloadExcel = function(event) {

		if ($(event.target).attr("data-datatable") != undefined) {
			var tableSorting = $($(event.target).attr("data-datatable"))
					.dataTable().fnSettings().aaSorting;
			var tableData = event.data.tableconfig.data;
			var columnObj = tableData.columns;
			columnObj = columnObj.filter(function(o) {
				return !o.hidecoloumn || o.hidecoloumn === false
			});
			if (tableData.isSelectAll) {
				columnObj = columnObj.filter(function(o) {
					return !o.name || o.name !== "selectAll"
				});
			}
			var columns = "";
			var groupBy = "";
			var filter = " 1=1";
			if (localStorage["datatablefilter"])// localStorage["datatablefilter_"+event.data.tableconfig.alias.replace("#",'')])
				filter += ' And ' + localStorage["datatablefilter"];
			var aggCheck = false;
			for (var col = 0; col < columnObj.length; col++) {
				if (!columnObj[col].data)
					continue;
				columnObj[col].title = columnObj[col].title.replace(/'/g, "")
						.replace(/"/g, "");
				if (col !== 0) {
					columns += ",";
				}

				if (columnObj[col].groupbyaggregate
						&& columnObj[col].groupbyaggregate !== null) {
					columns += columnObj[col].groupbyaggregate + '('
							+ addTildInColumnName(columnObj[col].data)
							+ ') AS "' + columnObj[col].title + '"';
					aggCheck = true;
				} else {
					columns += addTildInColumnName(columnObj[col].data)
							+ ' AS "' + columnObj[col].title + '"';
					if (groupBy !== "")
						groupBy += ",";
					groupBy += addTildInColumnName(columnObj[col].data);
				}
			}
			;
			var fileName = ($(event.target).attr('data-header') && $(
					event.target).attr('data-header').trim() !== '') ? $(
					event.target).attr('data-header').trim() : $(event.target)
					.text().trim();
			fileName = (fileName.length > 250) ? 'Report' : fileName;
			var fileAttr = $(event.target).find("i").attr("filename");
			if (fileAttr)
				fileName = fileAttr;
			
			var request = {
				"columns" : columns,
				"databaseColumns" : tableData.dataColumns,
				"orderField" : tableData.isSelectAll ? tableSorting[0][0] - 1
						: tableSorting[0][0],
				"orderType" : tableSorting[0][1],
				"searchValue" : $('.dataTables_filter input').val(),
				"source" : tableData.source,
				"database" : tableData.database,
				"table" : tableData.table,
				"query" : tableData.dataQuery,
				"filter" : filter,
				"displayName" : fileName
			};
			if (tableData.dataQuery) {
				var filters = tableData.dataQuery.split(",");
				if (filters && filters.length > 0) {
					for (var i = 0; i < filters.length; i++) {
						if (filters[i].indexOf("as") > 0) {
							var alias = filters[i].split("as");
							if ((alias && alias.length > 1)
									&& request.filter.indexOf(alias[1].trim()) > -1) {
								var regex = new RegExp(alias[1].trim(), "g");
								request.filter = request.filter.replace(regex,
										alias[0].trim());
							}
						}
					}
				}
			}
			if (aggCheck)
				request.groupBy = groupBy;

			var $preparingFileModal = $("#preparing-file-modal");
			$preparingFileModal.dialog({
				modal : true
			});
			var downloadURL;
			if (event.data.type === "excel")
				downloadURL = 'rest/dataAnalyser/datatableExcelDownload';
			else
				downloadURL = 'rest/dataAnalyser/datatableCSVDownload';
			$.fileDownload(downloadURL, {
				successCallback : function() {
					$preparingFileModal.dialog('close');
				},
				failCallback : function(responseHtml) {
					$preparingFileModal.dialog('close');
					$("#error-modal").html($(responseHtml).text());
					$("#error-modal").dialog({
						modal : true
					});
				},
				httpMethod : "POST",
				data : {
					"data" : encodeURI(JSON.stringify(request)),
					"isEncoded" : "true"
				}
			});

		}

	}

	var getColumn = function(table) {
		var request = {
			source : "hive",
			database : "EMEA_STG",
			table : table
		};
		callAjaxService("getColumnList", function(response) {
			callBackSuccessQueryData(response, table);
		}, callBackFailure, request, "POST", null, true);
	}
	var callBackSuccessQueryData = function(response, table) {
		var date = $("[name='SelectDate']").find("select").length > 0
				&& $("[name='SelectDate']").find("select").val() ? $(
				"[name='SelectDate']").find("select").val() : '';
		var vendorId = $("[name='SelectVendor']").find("select").val() ? +$(
				"[name='SelectVendor']").find("select").val() : '';
				var filterQuery ='';
				if(date && date.length>0){
					filterQuery =filterCol + " IN('" + date.join("','") + "')  AND ";
				}
		filterQuery += vendorCol +" like'%" +vendorId +"%'";  
		console.log('date filters' + date+'vendorID'+vendorId);
		disableLoading();
		var columnArr = [];
		var graphConfig = {};
		this.grapher = null;
		for (var j = 0; j < response.length; j++) {
			var obj = {};
			obj.data = response[j].columnName;
			obj.title = response[j].columnName;
			columnArr.push(obj);
		}

		var config = {
			html : '#datatable_1',
			url : serviceCallMethodNames["getDataPagination"].url,
			source : 'hive',
			database : 'EMEA_STG',
			table : table,
			groupbyenabled : true,
			order : [ {
				column : 1,
				dir : 'desc'
			} ],
			dataColumns : columnArr.map(function(o) {
				return o.data
			}).toString(),
			columns : columnArr,
			filter : filterQuery
		};
		constructGrid(config);
		graphConfig.data = config;

		$("#downloadExcel").unbind("click").bind("click", {
			"tableconfig" : graphConfig,
			"type" : "excel"
		}, downloadExcel);

	}
	init();
}

var TableView = new TableView();