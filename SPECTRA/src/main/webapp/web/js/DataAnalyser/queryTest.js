(function() {
	var THIS = {};
	THIS.configData = [];
	THIS.totalSelectedQuery=" "; 
	
	var textarea = $("#queryDataSource");
	$("#saveProfile").unbind('click').bind('click', function() {
		saveProfile();
	});
	$("#createProfile").unbind('click').bind('click',function(){
	    $('#profilename').val("");
	    $('#description').val("");
		$("#myModalsave").modal('show');
		THIS.queryEditor.setValue("");
		$('#expectedOutput').val("");
		$('#rangeOutput').find('[name="from-defaultvalue"]').val("");
		$('#rangeOutput').find('[name="to-defaultvalue"]').val("");
		$('#myModalsave').find('.checkbox').show();
		//constructSelect2("#appProfile","select app as id, roles.displayname as name from profile_analyser inner join roles on profile_analyser.app = roles .name where lower(roles.displayname) || lower(roles.name) like '%<Q>%' group by app,roles.displayname",false,null,"name");
		THIS.profile = null;
		setTimeout(function() {
			THIS.queryEditor.refresh();
		}, 300);
	});
	$("#frmSaveDataSource").submit(function(e) {
		e.preventDefault();
	});
	$("#frmSaveDataSource").validate({
		ignore : [],
		rules : {
			profilename : {
				required : true
			},
			description : {
				required : true
			}
		},
		messages : {
			profilename : {
				required : 'Please enter profile name'
			},
			description : {
				required : 'Please describe profile'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	var sourceData = [ {
		"id" : "hive",
		"text" : "HIVE"
	}, {
		"id" : "postgreSQL",
		"text" : "PostgreSQL"
	} ];
	
	$("#database").select2({
		placeholder : "Select Database",
		data : []
	});
	$("#source").select2({
		placeholder : "Select Source",
		data : sourceData
	}).on("change", function(e) {
		sourceSelect(e.val);
	});
	
	var dataType = [ {
		"id" : "text",
		"text" : "text"
	}, {
		"id" : "range",
		"text" : "range"
	} ];
	
	$("#datatype").select2({
		placeholder : "Select Data Type",
		data : dataType
	}).on("change", function(e) {
		selectDataType(e.val);
	});
	var selectDataType = function(obj) {
		var type = obj.outputType ? obj.outputType:obj;
		var expectedOutput;
		if(THIS.profile && THIS.profile.config.outputType === type ){
			expectedOutput =THIS.profile.config.expectedOutput !=undefined ?  THIS.profile.config.expectedOutput :""
		}
		if (type === "range") {
			$('#outputDiv').hide();
			$('#rangeOutput').show();
			$('#rangeOutput').find('[name="defaultvaluelabel"]').empty();
			$('#rangeOutput').find('[name="defaultvaluelabel"]').append(
					$('<span/>').addClass('col-sm-2 text-right').html('From:'))
					.append($('<input/>').addClass('col-sm-3 input-xs').attr("name", "from-defaultvalue"))
					.append($('<span/>').addClass('col-sm-1 text-right').html('To:')).append(
							$('<input/>').addClass('col-sm-3 input-xs').attr("name", "to-defaultvalue"));;
			if(expectedOutput !=null || expectedOutput !=undefined){
				var arr =expectedOutput.split(' to '); 
				$('#rangeOutput').find('[name="from-defaultvalue"]').val(arr[0]);
				$('#rangeOutput').find('[name="to-defaultvalue"]').val(arr[1]);
			}
		}else{
			$('#rangeOutput').hide();
			$('#outputDiv').show();
			var output =THIS.profile && THIS.profile.config.outputType !="range"?(THIS.profile.config.expectedOutput ? THIS.profile.config.expectedOutput: obj.expectedOutput):"";
			$('#expectedOutput').val(output);
		}
	};
	
	THIS.queryEditor = CodeMirror.fromTextArea(textarea[0], {
		lineNumbers : true,
		mode : "text/x-sql",
		autofocus : true,
		matchBrackets : true,
		styleActiveLine : true,
		showCursorWhenSelecting : true
	});
	
	var getAllProfiles = function(){
		$('#myprofilediv').show();
		$('#allprofilediv').hide();
		var requiredCol = [ {
			"sWidth": '13%',
			"mDataProp" : "displayname",
			"title":"Name",
			"sDefaultContent" : ""
		},  {
			"mDataProp" : "source",
			"sWidth": '13%',
			"title":"Source",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "database",
			"sWidth": '10%',
			"title":"Data Base",
			"sDefaultContent" : ""
		},  {
			"mDataProp" : "lastmodifiedby",
			"sWidth": '10%',
			"title":"Modified By",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "app",
			"sWidth": '13%',
			"title":"App",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "remarks",
			"sWidth": '35%',
			"title":"Description",
			"sDefaultContent" : ""
		}];
		
		var profileParam = {
				"getAll" : true,
				"profileType" : "savedQuery",
		        "columns" : requiredCol,
		};
		enableLoading();
		var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){mydataSourceSelect(e,this);});
		profileUtitlyObj.getMyProfile();
		
	};
	
	
	var getAllRoles = function(){
		if(!THIS.Roles || Object.keys(THIS.Roles).length === 0)
			callAjaxService("GetAllRoles",callBackSuccessGetRole,callBackFailure,null,"POST");
	};
	var callBackSuccessGetRole = function(data){
		THIS.Roles = [];
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined && data !== null){
			for (var i=0; i<data.length ;i++){
				THIS.Roles.push({"id":data[i].name,"text":data[i].displayName});
			}
			$("#appName").select2({
				placeholder : "Select Project",
				data : THIS.Roles,
				allowClear: true
			});
		}
		getAllProfiles();
	};
	
	var mydataSourceSelect = function(obj) {
		var id = $(obj).data("refid");
		$('#myModalsave').find('.checkbox').hide();
		var request = {
			"id" : id
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};

	var callBackGetProfileConfig = function(response) {
		var data = JSON.parse(response["config"])[0];
		if (data) {
			THIS.profile = {};
			THIS.profile.id = data.id;
			THIS.profile.createdBy = data.createdby;
			THIS.profile.queryName = data.displayname;
			THIS.profile.appName = data.app;
			$("#profilename").val(data.displayname);
			$("#description").val(data.remarks.replace(/@/g, "'"));
			if(data.app && data.app !== "null"){
				var selectedApp = THIS.Roles.filter(function(obj){ return obj.id === data.app});
				$("#appName").select2("data",selectedApp[0]); 
			}else{
				$("#appName").select2("data",null); 
			}
			THIS.profile.remarks = data.remarks.replace(/@/g, "'");
			THIS.profile.displayname = data.displayname;
			if (data.config) {
				var config = JSON.parse(data.config.replace(/(\r\n|\n|\r)/gm," "));
				config.query = config.query.replace(/@/g, "'");
				THIS.queryEditor.setValue(config.query);
				THIS.profile.config = config;
					selectDataType(config);
					var outputType = config.outputType !=undefined ? config.outputType :'text';
					$("#datatype").select2("val", outputType);
				//$('#expectedOutput').val(config.expectedOutput);
				$("#myModalsave").modal('show');
			      setTimeout(function() {
					THIS.queryEditor.refresh();
				}, 300);
			}
			$("#source").select2("val", data.source);
			sourceSelect(data.source, data.database);
		}
	};
	var sourceSelect = function(source, selectedDatabase) {
		if (source === "hive" || source === "postgreSQL") {
			var request = {
				"source" : source
			};
			enableLoading();
			callAjaxService("getDatabaseList", function(response) {
				callBackGetDatabaseList(response, selectedDatabase);
			}, callBackFailure, request, "POST");
		}
	};
	var callBackGetDatabaseList = function(response, selectedDatabase) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var databaseList = [];
			for ( var i = 0; i < response.length; i++) {
				databaseList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				allowClear : true,
				data : databaseList
			});
			if (selectedDatabase != undefined && selectedDatabase != null
					&& selectedDatabase != "") {
				$("#database").select2("val", selectedDatabase);
			}

		}
	};

   var saveProfile = function() { //for saving profile
		if (!$("#frmSaveDataSource").valid())
			return;
			var displayName, remarks;
			var query = THIS.queryEditor.getValue();
			var id = 0;
			var createdBy;
			var expectedOutput; 
			if($("#datatype").select2("val") ==='range'){
				expectedOutput = $('[name="from-defaultvalue"]').val() +' to '+$('[name="to-defaultvalue"]').val();
			}else{
				expectedOutput = $('#expectedOutput').val();
			}
			
			var config = {
				query : query,
				expectedOutput : expectedOutput,
				outputType : $("#datatype").select2("val")
			};
			callBackSuccessHandler = function() {
				disableLoading();
				if($('#createOther').prop('checked') != true){
					$('#myModalsave').modal('hide');	
				}
				 $('#profilename').val("");
				    $('#description').val("");
					THIS.queryEditor.setValue("");
					$('#expectedOutput').val("");
				showNotification("success", "Query saved successfully..");
				getAllProfiles();
			};
			if (THIS.profile && THIS.profile.id) {
				id = THIS.profile.id;
				createdBy = THIS.profile.createdby;
			}
				remarks = $("#myModalsave #description").val().replace(/'/g,"@");
				displayName = $("#myModalsave #profilename").val();
			
				var request = {
				"source" : $("#source").select2("val"),
				"database" : $("#database").select2("val"),
				"config" : JSON.stringify(config),
				"displayname" : displayName,
				"remarks" : remarks,
				"type" : "savedQuery",
				"app" : $("#appName").select2("val"),
				"id" : id
			};
			enableLoading();
			callAjaxService("saveProfile", callBackSuccessHandler,
					callBackFailure, request, "POST");
	};
	getAllRoles();
})();