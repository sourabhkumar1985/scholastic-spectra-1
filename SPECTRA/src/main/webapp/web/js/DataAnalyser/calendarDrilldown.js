var calendarDrillDownObj = function(){
	var THIS = this;
	this.callBack = function(){
		$('[id^=summary]').addClass("cursor-pointer").unbind('click').bind('click',function(){
			if($(this).find("span").text().toLowerCase() !== 'overdue'){
				localStorage["activeTab"] = 2;
				localStorage["status"] = $(this).find("span").text();
				location.href = "#OrderDetails";
			}
		});	
		$("#summary5").removeClass("cursor-pointer");
	};
	this.showDetails = function(screen,calEvent,jsEvent,view){
		switch (screen) {
		case 'eventdetails':
			eventDetails(calEvent,jsEvent,view);
			break;
		default:
			//throw new Error('Chart Type not Found');
		}
	};
	var eventDetails = function(calEvent,jsEvent,view){
		callAjaxService("eventDetailsView", function(response){callBackSuccessEventDetailsView(calEvent.eventid,response);},
				function() {
					alert("error");
				}, null, "GET", "html");
	};
	var callBackSuccessEventDetailsView = function(eventid,response){
		$("#formView").empty().html(response);	
		eventDetailsObj.Load(eventid);
	};
	var procedeAction = function(obj){
		$("#myModalDrilldown").modal('show');
		$("#poDocView").css("min-height","auto");
		$("#myModalDrilldown > .modal-dialog").addClass("width-500");
		$("#btnCreate").data({"entity-id":$(obj).data("entity-id"),"entity-status":$(obj).html()}).unbind("click").bind("click",function(){
			submitProcedeAction(this);
		});
	};
	var submitProcedeAction = function(obj){
		var request = {
				"entityid":$(obj).data("entity-id"),
				"user":"mani",
				"role": "admin",
				"action":$(obj).data("entity-status"),
				"comments":$("[name=remarks]").val()
		};
		callAjaxService("applyWorkFlowAction", callBackSuccessApplyWorkFlowAction,callBackFailure,request,"POST",null,true);
	};
	var callBackSuccessApplyWorkFlowAction = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalDrilldown').modal('hide');
			$("#frmSankeyFilter input").val("");
			showNotification("success",response["message"]);
			THIS.getEventDetails(THIS.eventID);
		};
	};
};
var calendarDrillDown =  new calendarDrillDownObj();