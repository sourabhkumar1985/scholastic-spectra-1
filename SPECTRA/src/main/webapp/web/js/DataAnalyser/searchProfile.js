pageSetUp();
$("#searchterm").focus();
//var searchSolr;
//
//var searchSolrInstance =  function(){
//	searchSolr = new searchProfile();
//};
var searchData = [];
var currentFacet = '';
var configurationData = [];
var viewMode = 'Content';
var filterKeys = [];
var filteredKeyDataMap = {};
var currentRow = 0;
var searchProf;
var searchSuggestionsObject = {};
var recordsCounts = {};
var isNearest = false;
var secondaryRoles = [];
var searchUrls = {};
var viewType = 'search';
var bomIds = [];

//Process Secondary Roles
if (System.secondaryRoles && System.secondaryRoles.length > 0) {
    for (var i in System.secondaryRoles) {
        if (!!System.secondaryRoles[i].dataSource && System.secondaryRoles[i].dataSource.trim() !== '') {
            secondaryRoles.push(System.secondaryRoles[i].dataSource.replace('QFIND_', '').replace('_', ' '));
        }
    }
}

$.fn.myElevateZoom = function(id, opts) {
    $('#' + id + ' img').removeData('elevateZoom');
    $('.zoomWrapper img.zoomed').unwrap();
    $('.zoomContainer').remove();

    $(this).elevateZoom(opts);
};

var openModal = function(key, index) {

    var criteria = "",
        k = key;
    if (key) {
        key = key.split("|");
        if (key.length > 2) {
            key.splice(0, 2);
            criteria = key.toString();
        } else if (key.length > 1) {
            key.splice(0, 1);
            criteria = key.toString();
        }
    }

    var payload = "source=" + configurationData[0].source + "&database=" + (configurationData[0].config ? configurationData[0].config : System.dbName) + "&table=" + currentFacet + "&criteria=" + criteria;
    //detailDrillDown(payload, currentFacet, criteria, currentFacet);
    $("#view-search-data-modal .modal-dialog").addClass("modal-lg");
    var valMap = { catalogdesc1: 'Description', catalognumber1: 'Item No', uom_code: 'UOM Code', itemtype: 'Item Type', itemstatus: 'Status', component_sku_number: 'SKU', coo: 'Origin' };
    var contents = ['Country', 'country', '"label "', 'coodesc', 'approveddate', 'manufacturer', 'catalognumber1', 'itemtype', 'itemstatus', 'uom_code', 'component_sku_number', 'coo'];

    var obj = filteredKeyDataMap[filterKeys.indexOf(k)];
    var str = '';
    for (var i in obj) {
        if (contents.indexOf(i.toString()) !== -1 && valMap[i]) {
            str += '<div class="col-sm-6">'; //<div class="row">
            str += '<strong>' + valMap[i] + '</strong> : ';
            str += obj[i];
            str += '</div>'; //</div>	
        }

    }
//    $('#view-search-data-modal-data').html('');
//    $('#view-search-data-modal-img').html('');
//
    $('#view-search-data-modal-data').append(str);
    // $('#view-search-data-modal-img').append('<img style="width: 100%;" src="https://i.ytimg.com/vi/K6SmVMtMmEU/hqdefault.jpg" alt="NO photo" />');
    //$('#view-search-data-modal-img').append('<img style="width: 100%;" src="'+ obj.filepath + '" data-zoom-image="large/image1.jpg" alt="No photo" />');
    //$('#view-search-data-modal-img').append('<img id="view-search-data-modal-img-img" style="width: 100%;" src="http://192.168.101.46:8282/INTEGRA/img/qfind_images/RR2509.png" data-zoom-image="http://192.168.101.46:8282/INTEGRA/img/qfind_images/RR2509.png" alt="No photo" />');
    //$('#view-search-data-modal-img').append('<img onerror="System.imgError(this);" id="view-search-data-modal-img-img" style="width: 100%;" src="' + obj.filepath + '" data-zoom-image="' + obj.filepath + '" alt="No photo" />');
    // $('#view-search-data-modal-img-img').elevateZoom({
    //     scrollZoom: true,
    //     lensShape: "round",
    //     lensSize: 200
    // });
    try {
        $('#view-search-data-modal-img-img').myElevateZoom('view-search-data-modal-img-img', {
            zoomType: "lens",
            lensShape: "round",
            lensSize: 200
        });
    } catch (e) {

    }

    $("#view-search-data-modal").modal("show");
}

$('#view-search-data-modal').on('hidden.bs.modal', function() {
    $(".zoomImg").each(function() {
        //$('#'+ this.id).zoom({ url: this.src });
        $('#' + this.id).myElevateZoom(this.id, {
            scrollZoom: true,
            zoomType: "inner",
            cursor: "crosshair"
        });
    });
})

var appendSearchProfileView = function(viewType, newData) {
    if (!viewType || viewType.trim() === '' || !!!newData || newData === {}) {
        return;
    }
    //Override the variable to reuse the code....
    var searchData = newData;
    if (viewType.toLowerCase() === 'card') {
        enableLoading();
        var img = ['revesion', 'primaryUOM'];
        var valMap = { catalogdesc1: 'Description', catalognumber1: 'Item No', uom_code: 'UOM Code', itemtype: 'Item Type', itemstatus: 'Status', component_sku_number: 'SKU', coo: 'Origin' };
        var front = ['catalogdesc1', 'Description'];
        var back = ['"label "', 'coodesc', 'approveddate', 'manufacturer', 'catalognumber1', 'itemtype', 'itemstatus', 'uom_code', 'component_sku_number', 'coo'];
        var str = '<div class="row"><div class="col-sm-12"><div style="margin-left : 4%">';
        for (var i = 0; i < searchData.length; i += 1) {
            var index = Object.keys(filteredKeyDataMap).length + i;
            var cardTemplate = '<div class="card effect__EFFECT">';
            cardTemplate += '<div class="card__front">';
            cardTemplate += '<span><a style="cursor: pointer;" onClick="openModal(\'' + searchData[i].catalognumber1 + '\' , \'' + i + '\');"><i class="fa fa-lg fa-info-circle" aria-hidden"true"=""></i></a></span>';

            for (var j = 0; j < front.length; j++) {
                if (searchData[i][front[j]]) {
                    cardTemplate += '<span class="card__text"><strong>' + valMap[front[j]] + '</strong> : <label>' + searchData[i][front[j]] + '</label></span><br />';
                }
            }
            //cardTemplate += '<img class="card-img" src="https://i.ytimg.com/vi/K6SmVMtMmEU/hqdefault.jpg"/>';
            cardTemplate += '<img onerror="System.imgError(this);" src="' + searchData[i].filepath + '" alt="No Preview Available" class="card-img" />';
            cardTemplate += '</div>';
            cardTemplate += '<div class="card__back">';
            cardTemplate += '<span><a style="cursor: pointer;" onClick="openModal(\'' + searchData[i].catalognumber1 + '\' , \'' + i + '\');"><i class="fa fa-lg fa-info-circle" aria-hidden"true"=""></i></a></span>';
            for (var j = 0; j < back.length; j++) {
                if (searchData[i][back[j]] && valMap[back[j]]) {
                    cardTemplate += '<span class="card__text"><strong>' + valMap[back[j]] + '</strong> : <label>' + searchData[i][back[j]] + '</label></span><br />';
                }
            }
            cardTemplate += '</div>'
            cardTemplate += '</div>';
            str += cardTemplate;
        }
        str += "</div></div></div>";
        //$('#' + currentFacet + 'Card').html('');
        $('#' + currentFacet + 'Card').append(str);
        //$('#' + currentFacet + 'Table').fadeOut('slow');
        $('#resultGrid').fadeOut('slow');
        $('#' + currentFacet + 'Content').fadeOut('slow');
        var cards = document.querySelectorAll(".card.effect__EFFECT");
        for (var i = 0, len = cards.length; i < len; i++) {
            var card = cards[i];
            clickListener(card);
        }

        function clickListener(card) {
            card.addEventListener("click", function() {
                var c = this.classList;
                c.contains("flipped") === true ? c.remove("flipped") : c.add("flipped");
            });
        }
        $('#' + currentFacet + 'Card').fadeIn('slow');
        viewMode = 'Card';
        disableLoading();
    } else if (viewType.toLowerCase() === 'grid') {
        $('#' + currentFacet + 'Content').fadeOut('slow');
        $('#' + currentFacet + 'Card').fadeOut('slow');
        $('#resultGrid').fadeIn('slow');
        viewMode = 'Grid';
    } else if (viewType.toLowerCase() === 'content') {
        var valMap = { catalogdesc1: 'Description', catalognumber1: 'Item No', uom_code: 'UOM Code', itemtype: 'Item Type', itemstatus: 'Status', component_sku_number: 'SKU', coo: 'Origin' };
        var contents = ['Country', 'country', '"label "', 'coodesc', 'approveddate', 'manufacturer', 'catalognumber1', 'itemtype', 'itemstatus', 'uom_code', 'component_sku_number', 'coo'];
        var title = ['catalogdesc1'];
        var str = '<div style="margin-top: 15px;"></div><div class="row"><div class="col-sm-12">';
        var filteredLength = Object.keys(filteredKeyDataMap).length;
        for (var i = 0; i < searchData.length; i += 1) {
            var index = filteredLength + i;
            var cardTemplate = '<div class="row content-view">';
            cardTemplate += '<div class="col-xs-12 col-md-12 col-lg-12">';
            cardTemplate += '<a class="thumbnail" onClick="openModal(\'' + searchData[i].catalognumber1 + '\' , \'' + i + '\');">';
            cardTemplate += '<div class="col-md-12">';
            cardTemplate += '<span class="con-img-zoom" style="position: relative; overflow: hidden;"><span>Hover on the Image to Zoom</span>';
            //cardTemplate += '<img class="zoomImg" src="http://www.jacklmoore.com/img/daisy.jpg" alt="Generic placeholder thumbnail" />';
            //cardTemplate += '<img class="zoomImg"  id="' + currentFacet + 'con-img-zoom-' + index + '" src="http://192.168.101.46:8282/INTEGRA/img/qfind_images/RR2509.png" data-zoom-image="http://192.168.101.46:8282/INTEGRA/img/qfind_images/RR2509.png" alt="Generic placeholder thumbnail" /></span>';
            cardTemplate += '<img onerror="System.imgError(this);" class="zoomImg"  id="' + currentFacet + 'con-img-zoom-' + index + '" src="' + searchData[i].filepath + '" data-zoom-image="' + searchData[i].filepath + '" alt="No Preview" /></span>';
            //cardTemplate += '<img class="zoomImg" id="' + currentFacet + 'con-img-zoom-' + index + '"  src="http://192.168.101.46:8282/INTEGRA/' + searchData[i].filepath + '" data-zoom-image="' + searchData[i].filepath + '" alt="No Image to Preview" /></span>';
            // if (searchData[i].filepath) {
            //     cardTemplate += '<img class="zoomImg" src="' + searchData[i].filepath + '" alt="No Preview Available" />';
            // }
            cardTemplate += '</div>';
            cardTemplate += '<div class="col-md-12">';
            cardTemplate += '<ul>';
            for (var j = 0; j < title.length; j++) {
                if (searchData[i][title[j]]) {
                    // cardTemplate += '<span class="card__text"></span><br />';
                    cardTemplate += '<li style="border-bottom: 1px solid #FF6347"><h4><b>' + searchData[i][title[j]] + '</b></h4></li>';
                }
            }
            for (var j = 0; j < contents.length; j++) {
                if (searchData[i][contents[j]] && valMap[contents[j]]) {
                    // cardTemplate += '<span class="card__text"></span><br />';
                    cardTemplate += '<li><strong>' + valMap[contents[j]] + '</strong> : <label>' + searchData[i][contents[j]] + '</label></li>';
                }
            }
            cardTemplate += '</ul>';
            cardTemplate += '</div>';
            cardTemplate += '</a>';
            cardTemplate += '</div></div>';
            str += cardTemplate;
        }
        str += "</div></div>";
        //$('#' + currentFacet + 'Content').html('');
        $('#' + currentFacet + 'Content').append(str);
        $(".zoomImg").each(function() {
            //$('#'+ this.id).zoom({ url: this.src });
            $('#' + this.id).myElevateZoom(this.id, {
                scrollZoom: true,
                zoomType: "inner",
                cursor: "crosshair"
            });
        });
        //$('#' + currentFacet + 'Table').fadeOut('slow');
        $('#resultGrid').fadeOut('slow');
        $('#' + currentFacet + 'Content').fadeIn('slow');
        $('#' + currentFacet + 'Card').fadeOut('slow');
        viewMode = 'Content';
    }
}

var searchProfile = function() {
    $('body').toggleClass("minified");
    $("#savebookmark").unbind("click").bind("click", function() { THIS.bookmark(); });
    var THIS = this;
    $('#searchProfile').show();
    $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
        e.stopPropagation();
    });
    $("[id*=count]").html(0);
    $("form").submit(function(e) {
        e.preventDefault();
    });

    $("#entities li").unbind('click').bind('click', function() { THIS.entitySearch(this); });
    THIS.datasource = "SAP";
    THIS.facet = "ekko";
    THIS.qfindURL = lookUpTable.get("qfindURL");
    THIS.request = {};
    THIS.integraSearchUrls = {
        search: 'integraSearch',
        searchEntity: 'integraSearchEntity',
        bookmarksearch: 'integraBookmarksearch',
        getShortlists: 'integraGetShortLists',
        shortlistDocument: 'integraShortlistDocument'
    }

    THIS.mode = 'search';

    THIS.nonIntegraSearchUrls = {
        search: 'search',
        searchEntity: 'searchEntity',
        bookmarksearch: 'bookmarksearch'
    };

    THIS.resultKvp = {};
    THIS.compareKvp = {};

    this.resetPage = function() {
        THIS.compareClose();
        var icons = {
            JARIT: { src: THIS.qfindURL + 'img/qfind_images/jarit2.png', count: '', c: '1' },
            MILTEX: { src: THIS.qfindURL + 'img/qfind_images/miltex2.png', count: '', c: '0' },
            All_Others: { src: THIS.qfindURL + 'img/qfind_images/allothers.png', count: '', c: '0' }
        };
        $('.typeahead').typeahead('val', '');
        $('#searchModalDrilldown').hide();
        $("#searchContent").show();
        $('#searchProfile').show();
        $("#searchContent").empty();
        $('#searchterm').empty()
        $('#searchterm').val('').focus();
        THIS.key = '';
        var clinicalSpecialityDiv = $("#searchContent");
        var clinicalSpecialityItemsDiv = $("<div/>").addClass("col-md-12 no-padding").attr({ "id": "clinicalSpecialityItemsDiv" });
        if (icons != null && !$.isEmptyObject(icons)) {
            $.each(icons, function(key, data) {
                clinicalSpecialityItemsDiv.append($("<div/>").addClass("metro col-md-2 no-padding padding-left-10 cat").data({ "type": "integra", "key": key, "count": data.c })
                    .append($("<a/>").addClass("tile wide imagetext wideimage bg-color-green")
                        .append($("<img/>").attr({ "src": data.src, "onerror": "System.imgError(this);", "style": "height: 120px; min-width: 100%;" }))
                        .append($("<div/>").addClass("textover-wrapper transparent")
                            .append($("<div/>").addClass("text2").html(key))
                            .append($("<div/>").addClass("text2 text-right txt-color-gold").html(data.count))
                        )
                    )
                );
            });
        }
        $("#searchDivId").removeClass().addClass("search-active");
        clinicalSpecialityDiv.append(
            $("<div/>").addClass("col-md-12")
            .append($("<div/>").addClass("title-panel").attr({ "href": "clinicalSpecialityItemsDiv", "title": "click to collapse" }).html("Brands")));
        clinicalSpecialityDiv.append(clinicalSpecialityItemsDiv);

        $('.cat').unbind('click').bind('click', function() {
            if ($(this).data() && !!$(this).data().key && $(this).data().count > 0) {
                THIS.LoadFreeFrom();
            }
        });
    };
    $("#breadcrumbHome").unbind('click').bind("click", this.resetPage);

    this.shortlistPage = function() {
        if (location.hash.replace(/^#/, '') !== "integrasearch") {
            viewType = 'shortlist';
            window.location.hash = '#integrasearch';
        }
        THIS.mode = 'shortlist';
        callAjaxService(searchUrls["getShortlists"], function(response) {
            THIS.ShortlistItems = new Array();
            var key = '',
                count = 0;
            if (response.length > 0) {
                THIS.ShortlistItems = [].concat(response.map(function(a) {
                    if (!!a.refid) {
                        if (count == 0) {
                            key = a.refid;
                        } else {
                            key += ' OR ' + a.refid;
                        }
                        count += 1;
                        return a.refid;
                    }
                }));
            }
            if (THIS.ShortlistItems.length === 0) {
                return;
            }
            THIS.configData = configurationData;
            var request = {
                "key": key,
                "facet": "All",
                "solrcollectionname": THIS.configData[0].database,
                "searchInDisplaycolumns": false,
                "start": 0,
                "rows": 100,
                "type": "detailed"
            };
            enableLoading();
            callAjaxService(searchUrls['search'], callBackFacetFreeFormSearchResult,
                callBackFailure, request, "POST");
        }, callBackFailure, {}, "POST");
        return;
    }

    $("#shortlist").unbind('click').bind("click", this.shortlistPage);

    (function() {
        //TODO: Find a new way execute the method...
    	if(localStorage["lastusedapp"] == "ROLE_PAKFIND" || localStorage["lastusedapp"] == "ROLE_QFIND") {
    		
    	} else {
    		searchUrls = JSON.parse(JSON.stringify(THIS.nonIntegraSearchUrls));
            return;
    	}        
        searchUrls = JSON.parse(JSON.stringify(THIS.integraSearchUrls));
        THIS.ShortlistItems = [];
        if (location.hash.replace(/^#/, '') == "shortlist" || viewType === 'shortlist') {
            THIS.mode = 'shortlist';
            THIS.shortlistPage();
            return;
        }

        callAjaxService(searchUrls["getShortlists"], function(response) {
            THIS.ShortlistItems = new Array();
            if (response.length > 0) {
                THIS.ShortlistItems = [].concat(response.map(function(a) {
                    if (!!a.refid) {
                        return a.refid;
                    }
                }));
            }
        }, callBackFailure, {}, "POST");
        if (searchKey && searchKey !== '') {
            $('#searchterm').val(searchKey);
            //$('body').toggleClass("minified");
            searchKey = '';
            THIS = searchProf;
            THIS.LoadFreeFrom();
            return;
        }
        var icons = {
            JARIT: { src: THIS.qfindURL + 'img/qfind_images/jarit2.png', count: '', c: '1' },
            MILTEX: { src: THIS.qfindURL + 'img/qfind_images/miltex2.png', count: '', c: '0' },
            All_Others: { src: THIS.qfindURL + 'img/qfind_images/allothers.png', count: '', c: '0' }
        };
        var clinicalSpecialityDiv = $("#searchContent");
        var clinicalSpecialityItemsDiv = $("<div/>").addClass("col-md-12 no-padding").attr({ "id": "clinicalSpecialityItemsDiv" });
        if (icons != null && !$.isEmptyObject(icons)) {
            $.each(icons, function(key, data) {
                clinicalSpecialityItemsDiv.append($("<div/>").addClass("metro col-md-2 no-padding padding-left-10 cat").data({ "type": "integra", "key": key, "count": data.c })
                    .append($("<a/>").addClass("tile wide imagetext wideimage bg-color-green")
                        .append($("<img/>").attr({ "src": data.src, "onerror": "System.imgError(this);", "style": "height: 120px; min-width: 100%;" }))
                        .append($("<div/>").addClass("textover-wrapper transparent")
                            .append($("<div/>").addClass("text2").html(key))
                            .append($("<div/>").addClass("text2 text-right txt-color-gold").html(data.count))
                        )
                    )
                );
            });
        }
        $("#searchDivId").removeClass().addClass("search-active");
        clinicalSpecialityDiv.append(
            $("<div/>").addClass("col-md-12")
            .append($("<div/>").addClass("title-panel").attr({ "href": "clinicalSpecialityItemsDiv", "title": "click to collapse" }).html("Brands")));
        clinicalSpecialityDiv.append(clinicalSpecialityItemsDiv);

        $('.cat').unbind('click').bind('click', function() {
            if ($(this).data() && !!$(this).data().key && $(this).data().count > 0) {
                THIS.LoadFreeFrom();
            }
        });
    })();

    var enableSearchSuggestion = function() {
        $('.typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 0
        }, {
            name: 'states',
            displayKey: 'value',
            source: function(query, process) {
                $.ajaxQ.abortAll();
                query = $.trim(query);
                if (query !== "") {
                    return $.get('rest/integrasearch/searchSuggestions', { query: query, solrcollectionname: "Integra" }, function(data) {
                        var temp = [];
                        if (data && data.length > 0 && data[0].alternatives && data[0].alternatives.length > 0) {
                            for (var i = 0; i < data[0].alternatives.length; i++) {
                                temp.push({ "value": data[0].alternatives[i] });
                            }
                        }
                        return process(temp);
                    });
                } else {
                    $(".tt-dropdown-menu").hide();
                    $(".tt-hint").val("");
                }
            }
        }).bind("typeahead:selected", function() {
            THIS.LoadFreeFrom(configurationData);
        });
        $('.typeahead').focus(function() {
            $(".btn-bookmark").css({ "border": "2px solid #0097cf", "border-left": "0px" });
        });
        $('.typeahead').blur(function() {
            $(".btn-bookmark").css({ "border": "2px solid #ccc", "border-left": "0px" });
        });
    };
    /*var onDrawingClick = function(event){
    	var drawiningConfig = event.data.config;
    	$("#myModalDrilldown").modal("show");
    	$("#myModalLabel","#myModalDrilldown").html(drawiningConfig.catalognumber1 +" - " +drawiningConfig.catalogdesc1);
    	$(".modal-body","#myModalDrilldown").empty();
    	var drawingDiv = $("<div/>").addClass("tabs-left");
    	var tabs = $("<ul/>").addClass("nav nav-tabs tabs-left");
    	tabs.append($("<li/>").addClass("active").append(drawiningConfig.datasource));
    	tabs.append($("<li/>").append("Doc"));
    	drawingDiv.append(tabs);
    	$(".modal-body","#myModalDrilldown").append(drawingDiv);
    };*/
    var callBackLink3Result = function(response) {
        if (!!!response) {
            return;
        }
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        // var silderDiv = $("<section/>").addClass("regular");
        // $("#poDocView").append($("<hr/>"));
        // $("#poDocView").append($("<h3/>").addClass("txt-color-red").html("Similar Items"));
        // $("#poDocView").append($("<div/>").addClass("row slick-parent").append(silderDiv));
        // for (var i = 0; i < response.length; i++) {
        //     silderDiv.append($("<div/>")
        //         .append($("<img/>").attr({ "src": response[i].filepath, height: "120px" }))
        //         .append($("<a/>").attr("href", "#").html(response[i].catalognumber1 + " - " + response[i].catalogdesc1))
        //     );
        // }
        //$('#searchModalDrilldown-title').html('<h4 class="alert alert-success fade in margin-bottom-10"> Similar Items </h4>');
        $('#searchModalDrilldown-title').html('<hr /><h3 class="txt-color-red">Similar Items</h3>');
        var slider = '<section id="link3_slider">';
        for (var i = 0; i < response.length; i++) {
            slider += '<div style="padding: 0px;width: 100%" class="suggest-a">';
            slider += '<img onerror="System.imgError(this);" data-obj=\'' + JSON.stringify(response[i]) + '\' src="' + THIS.qfindURL + response[i].filepath + '" class="img-thumbnail thumb suggest" style="width: 100%; height: 120px; cursor: pointer;" />';
            slider += '<a class="suggest" data-obj=\'' + JSON.stringify(response[i]) + '\' href="javascript:void(0);" style="color: blue">' + response[i].catalognumber1 + " - " + response[i].catalogdesc1 + '';
            slider += '</a>';
            slider += '</div>';
        }
        slider += '</section>';
        $('#searchModalDrilldown-suggestion').html(slider);
        $("#link3_slider").slick({
            dots: false,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3
        });

        $('.suggest').unbind('click').bind('click', function() {
            callBackDetailViewResult([JSON.parse('' + $(this).attr('data-obj'))]);
        });

        // $('.link3_slider').fadeIn('slow');

        // $('.slick-next').click(function() {
        //     $('.regular').slick('unslick').slick('reinit');
        //     $(".regular").append('<div><img src="Pics/1.png"><a href="#">Title 11</a></div>');
        //     $(".regular").slick({
        //         dots: true,
        //         infinite: true,
        //         slidesToShow: 3,
        //         slidesToScroll: 3
        //     });

        //     console.log('Next Button Clicked');
        // });

        // $('.slick-prev').click(function() {
        //     console.log('Previous Button Clicked');
        // });
    }
    var callBackDetailViewResult = function(response) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        var drawiningConfig = response[0];
        $('#searchModalDrilldown-suggestion').empty();
        $('#searchModalDrilldown-title').empty();

        $("#searchContent").fadeOut('fast');
        $('#searchModalDrilldown').fadeIn('slow');
        $('#searchProfile').fadeOut('fast');
        $("#myModalLabel", "#myModalDrilldown").html(drawiningConfig.catalognumber1 + " - " + drawiningConfig.catalogdesc1);
        $('#searchModalLabel').html(drawiningConfig.catalognumber1 + " - " + drawiningConfig.catalogdesc1);
        $('#searchBack').bind('click', function() {
            $('.zoomContainer').remove();
            $('#searchModalDrilldown-image-img').remove();
            $("#searchContent").fadeIn('fast');
            $('#searchProfile').fadeIn('fast');
            $('#searchModalDrilldown').fadeOut('slow');

            $(".elevateZoom-window").elevateZoom({
                zoomWindowPosition: 2,
                //zoomWindowWidth : $("#content").width() * 8 / 12,
                //zoomWindowHeight : $("#content").height() * 1 / 12,
                scrollZoom: true,
                tint: true,
                tintColour: 'red',
                tintOpacity: 0.3,
                minZoomLevel: 0.3
            });
            $(".elevateZoom-lens").elevateZoom({
                zoomType: "lens",
                lensShape: "round",
                lensSize: 150,
                //zoomWindowWidth : $("#content").width() * 8 / 12,
                //zoomWindowHeight : $("#content").height() * 1 / 12,
                scrollZoom: true,
                minZoomLevel: 0.05,
                /*tint : true,
                tintColour : 'red',
                tintOpacity : 0.3,
                minZoomLevel:0.3*/
            });
        });
        $(".zoomContainer").addClass("hide");
        $('html, body').animate({ scrollTop: 0 }, 0);
        var images = [];
        for (var i = 0; i < response.length; i++) {
            images.push(response[i].filepath);
        }
        var imagepath = THIS.qfindURL + response[0].filepath;
        $('#searchModalDrilldown-image').html('<img onerror="System.imgError(this);" class="searchModalDrilldown-image-img" id="searchModalDrilldown-image-img" class="elevateZoom" src="' + imagepath + '" alt="' + drawiningConfig.catalognumber1 + ' - ' + drawiningConfig.catalogdesc1 + '" />');
        var imgDimension;
        var isItemMasterAppended = false,
            isPCAppended = false,
            isAgileAppended = false,
            isLabelAppended = false;

        //imgDimension = { "height": $(window).width() - 900 + "px" };
        imgDimension = { "width": "100%", "height": "100%" };
        $('#searchModalDrilldown-image-img').attr(imgDimension)
        $('#searchModalDrilldown-image-img').myElevateZoom('searchModalDrilldown-image-img', {
            zoomType: "lens",
            lensShape: "round",
            lensSize: 200,
            scrollZoom: true
        });
        //Append details to id: searchModalDrilldown-content
        var str = '<div class="row">';
        //Append Item Master, Agile, Label, Product Catalog
        // str += '<h4 class="alert alert-success fade in margin-bottom-10"> Item Master </h4>';
        // str += '<div class="row">';
        // str += '<div class="col-sm-4">'; //Content
        // str += '<dl class="dl-horizontal">';
        // str += '<dt>Country of Origin</dt><dd>' + drawiningConfig.coo_desc + '</dd>';
        // str += '<dt>Item Type</dt><dd>' + drawiningConfig.itemtype + '</dd>';
        // str += '<dt>Manufacturer</dt><dd>' + drawiningConfig.manufacturer + '</dd>';
        // str += '</dl>';
        // str += '</div>';
        // str += '</div>';
        // str += '</div>';

        var str = '<li>';
        str += '<div class="rpd-feature-set">';
        str += '<div class="rpd-feature-set-header line"><div data-obj="item" class="rpd-feature-circle fk-float-left js-rpd-feature-circle expand">-</div>';
        str += '<h3 class="rpd-feature-set-title">Details</h3></div>';
        str += '<div class="rpd-feature-set-body bpadding10 item-expand">';
        str += '<div class="js-rpd-feature-set-innerbody line item-expand1">';
        //Content-BEGIN
        str += '<div class="row">';
        str += '<dl class="dl-horizontal">';
        str += '<dt>Country of Origin</dt><dd>' + drawiningConfig.coo_desc + '</dd>';
        str += '<dt>Item Type</dt><dd>' + drawiningConfig.itemtype + '</dd>';
        str += '<dt>Manufacturer</dt><dd>' + drawiningConfig.manufacturer + '</dd>';
        str += '</dl>';
        str += '</div>';
        //Content-END
        str += '</div>'
        str += '</div>'
        str += '</li>';
        isItemMasterAppended = true;

        // var imageDiv;
        // if (images.length > 1) {
        //     imageDiv = $('<div/>');
        //     for (var imgIndex = 0; imgIndex < images.length; imgIndex++) {
        //         var imageUrl = THIS.qfindURL + images[imgIndex];
        //         imageDiv.append($('<div/>').addClass('padding-2').append($('<small/>').css({ 'margin': '5px 10px', 'font-weight': 'bold' }).html(imgIndex + 1))
        //             .append($('<img/>').addClass('img-thumbnail').css({ 'width': '100px', 'background-color': imgIndex === 0 ? '#3276b1' : '' })
        //                 .attr({ 'src': imageUrl }).unbind('click').bind('click', function() {
        //                     var imageIndex = parseInt($(this).parent().find('small').html()) - 1;
        //                     var imageUrl = THIS.qfindURL + response[imageIndex].filepath;
        //                     $("#myModalLabel", "#myModalDrilldown").html(" - " + response[imageIndex].catalognumber1 + " - " + response[imageIndex].catalogdesc1).prepend($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).html(sourceName));
        //                     $("#poDocView img").css('background-color', '');
        //                     $(this).css('background-color', '#3276b1');
        //                     $("#poDocView img:last").attr({ 'src': imageUrl });
        //                     setTimeout(function() { $("#poDocView img:last").elevateZoom({ zoomType: "lens", minZoomLevel: 0.1, lensShape: "round", lensSize: 250, scrollZoom: true, customClass: "detailImg" }); }, 500);
        //                 })));
        //     }
        // }

        // var imageContent = $("<div/>").addClass("padding-top-10");
        // imageContent.append($("<h4/>").addClass("alert alert-success fade in margin-bottom-10").append($("<strong/>").attr({ "href": "javascript:void(0);" }).html("Item Master")));
        // var sectionDL = $("<dl/>").addClass("dl-horizontal");
        // imageContent.append(sectionDL);
        // sectionDL.append($("<dt/>").html("Country of Origin")).append($("<dd/>").html(drawiningConfig.coo_desc));
        // sectionDL.append($("<dt/>").html("Item Type")).append($("<dd/>").html(drawiningConfig.itemtype));
        // sectionDL.append($("<dt/>").html("Item Status")).append($("<dd/>").html(drawiningConfig.itemstatus));
        // sectionDL.append($("<dt/>").html("Manufacturer")).append($("<dd/>").html(drawiningConfig.manufacturer));
        // imageContent.append("<br/>");
        // 

        if (drawiningConfig.datasource === "AGILE" || (drawiningConfig.components && $.trim(drawiningConfig.components) !== '')) {
            // imageContent.append($("<h4/>").addClass("alert alert-success fade in margin-bottom-10").append($("<strong/>").attr({ "href": "javascript:void(0);" }).html("AGILE")));
            // var agileDL = $("<dl/>").addClass("dl-horizontal");
            // agileDL.append($("<dt/>").html("Material")).append($("<dd/>").html(drawiningConfig.components));
            // imageContent.append(agileDL);
            // str += '<div class="row">';
            // str += '<h4 class="alert alert-success fade in margin-bottom-10"> Agile </h4>';
            // str += '<dl class="dl-horizontal">';
            // str += '<dt>Material</dt><dd>' + drawiningConfig.components + '</dd>';
            // // str += '<dt>Item Type</dt><dd>' + drawiningConfig.itemtype + '</dd>';
            // // str += '<dt>Manufacturer</dt><dd>' + drawiningConfig.manufacturer + '</dd>';
            // str += '</dl>';
            // str += '</div>';
            // str += '<div class="row" id="searchModalDrilldown-content-agile-img">';
            // str += '<img class="img-thumbnail thumb" style="width: 100px; cursor: pointer;" src="' + imagepath + '" />';
            // str += '</div>';

            str += '<li>';
            str += '<div class="rpd-feature-set">';
            str += '<div class="rpd-feature-set-header line"><div data-obj="agile" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
            str += '<h3 class="rpd-feature-set-title">Agile</h3></div>';
            str += '<div class="rpd-feature-set-body bpadding10 agile-expand">';
            str += '<div class="js-rpd-feature-set-innerbody line agile-expand1">';
            //Content-BEGIN
            str += '<div class="row">';
            str += '<div class="col-sm-12" id="searchModalDrilldown-content-agile-img">';
            str += '<img onerror="System.imgError(this);" class="img-thumbnail thumb" style="width: 100%; height: 90%; cursor: pointer;" src="' + imagepath + '" />';
            str += '</div>';
            str += '<div class="row">';
            str += '<div class="col-sm-12">';
            str += '<div>';
            str += '<strong>Material : </strong><label>' + drawiningConfig.components + '</label><br />';
            str += '<strong>Finish : </strong><label>' + drawiningConfig.finished_goods_number + '</label><br />';
            str += '</div>';
            str += '</div>';
            str += '</div>';
            //Content-END
            str += '</div>'
            str += '</div>'
            str += '</li>';
            isAgileAppended = true;
        }

        if (drawiningConfig.datasource === "PRODUCT_CATALOG") {
            // imageContent.append($("<h4/>").addClass("alert alert-success fade in margin-bottom-10").append($("<strong/>").attr({ "href": "javascript:void(0);" }).html("PRODUCT CATALOG")));
            // var categoryDL = $("<dl/>").addClass("dl-horizontal");
            // categoryDL.append($("<dt/>").html("Category")).append($("<dd/>").html($.trim(drawiningConfig.category)));
            // imageContent.append($("<i/>").addClass("padding-left-25").html("Category: ").append($("<a/>").attr("href", "javascript:void(0);").append($.trim(drawiningConfig.category))));
            // if (drawiningConfig.subcategory && $.trim(drawiningConfig.subcategory) !== "") {
            //     categoryDL.append($("<dt/>").html("Subcategory")).append($("<dd/>").html($.trim(drawiningConfig.subcategory)));
            // }
            // imageContent.append(categoryDL);
            // str += '<div class="row">';
            // str += '<h4 class="alert alert-success fade in margin-bottom-10"> PRODUCT CATALOG </h4>';
            // str += '<dl class="dl-horizontal">';
            // str += '<dt>Category</dt><dd>' + drawiningConfig.category + '</dd>';
            // str += '<dt>Item Type</dt><dd>' + drawiningConfig.itemtype + '</dd>';
            // if (drawiningConfig.subcategory && $.trim(drawiningConfig.subcategory) !== "") {
            //     str += '<dt>Subcategory</dt><dd>' + $.trim(drawiningConfig.subcategory) + '</dd>';
            // }
            // str += '</dl>';
            // str += '</div>';
            // str += '<div class="row" id="searchModalDrilldown-content-pc-img">';
            // str += '<img class="img-thumbnail thumb" style="width: 100px; cursor: pointer;" src="' + imagepath + '" />';
            // str += '</div>';
            isPCAppended = true;

            str += '<li>';
            str += '<div class="rpd-feature-set">';
            str += '<div class="rpd-feature-set-header line"><div data-obj="pc" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
            str += '<h3 class="rpd-feature-set-title">Product Catalog</h3></div>';
            str += '<div class="rpd-feature-set-body bpadding10 pc-expand">';
            str += '<div class="js-rpd-feature-set-innerbody line pc-expand1">';
            //Content-BEGIN
            str += '<div class="row">';
            str += '<div class="col-sm-12">';
            str += '<div id="searchModalDrilldown-content-pc-img">';
            str += '<img onerror="System.imgError(this);" class="img-thumbnail thumb" style="width: 100%; cursor: pointer;" src="' + imagepath + '" />';
            str += '</div>';
            str += '<div class="row">';
            str += '<div class="col-sm-12">';
            str += '<div>';
            str += '<strong>Category : </strong><label>' + drawiningConfig.category + '</label><br />';
            str += '<strong>Item Type : </strong><label>' + drawiningConfig.itemtype + '</label><br />';
            if (drawiningConfig.subcategory && $.trim(drawiningConfig.subcategory) !== "") {
                str += '<strong>Subcategory : </strong><label>' + $.trim(drawiningConfig.subcategory) + '</label><br />';
            }
            str += '</div>';
            str += '</div>';
            str += '</div>';
            //Content-END
            str += '</div>'
            str += '</div>'
            str += '</li>';
        }

        if (drawiningConfig.datasource.toLowerCase() === 'label') {
            str += '<li>';
            str += '<div class="rpd-feature-set">';
            str += '<div class="rpd-feature-set-header line"><div data-obj="labels" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
            str += '<h3 class="rpd-feature-set-title">Label</h3></div>';
            str += '<div class="rpd-feature-set-body bpadding10 labels-expand">';
            str += '<div class="js-rpd-feature-set-innerbody line labels-expand1">';
            //Content-BEGIN
            str += '<div class="row">';
            str += '<div class="col-sm-12" id="searchModalDrilldown-content-label-img">';
            str += '<img onerror="System.imgError(this);" class="img-thumbnail thumb" style="width: 100%; height: 90%; cursor: pointer;" src="' + imagepath + '" />';
            str += '<section>';
            str += '</section>';
            str += '</div>';
            str += '</div>';
            //Content-END
            str += '</div>'
            str += '</div>'
            str += '</li>';

        }

        //Iterate through link1 and link2 for images of various Entities
        var labels = [];
        var agiles = [];
        var pCatalogs = [];
        var items = [];
        if (!!drawiningConfig.link1 && drawiningConfig.link1.length > 0) {
            var link1Arr = drawiningConfig.link1.split(',');
            for (var i = 0; i < link1Arr.length; i += 1) {
                var linkSourceName = link1Arr[i];
                if (linkSourceName.indexOf("ITM_") !== -1 && secondaryRoles.contains("Item Master".toUpperCase())) {
                    items.push(linkSourceName);
                } else if (linkSourceName.indexOf("AGILE_") !== -1 && secondaryRoles.contains("Agile".toUpperCase())) {
                    agiles.push(linkSourceName);
                } else if (linkSourceName.indexOf("CAT_") !== -1 && secondaryRoles.contains("Product Catalog".toUpperCase())) {
                    pCatalogs.push(linkSourceName);
                } else if (linkSourceName.indexOf("LABEL_") !== -1 && secondaryRoles.contains("Label".toUpperCase())) {
                    labels.push(linkSourceName);
                }
            }
        }
        if (!!drawiningConfig.link2 && drawiningConfig.link2.length > 0) {
            var link2Arr = drawiningConfig.link2.split(',');
            for (var i = 0; i < link2Arr.length; i += 1) {
                var linkSourceName = link2Arr[i];
                if (linkSourceName.indexOf("ITM_") !== -1 && secondaryRoles.contains("Item Master".toUpperCase())) {
                    items.push(linkSourceName);
                } else if (linkSourceName.indexOf("AGILE_") !== -1 && secondaryRoles.contains("Agile".toUpperCase())) {
                    agiles.push(linkSourceName);
                } else if (linkSourceName.indexOf("CAT_") !== -1 && secondaryRoles.contains("Product Catalog".toUpperCase())) {
                    pCatalogs.push(linkSourceName);
                } else if (linkSourceName.indexOf("LABEL_") !== -1 && secondaryRoles.contains("Label".toUpperCase())) {
                    labels.push(linkSourceName);
                }
            }
        }
        var a = { lbls: labels, agls: agiles, itms: items, cats: pCatalogs, link3s: null };
        if (drawiningConfig.link3 !== null && drawiningConfig.link3 !== "") {
            // var request = {
            //     "key": drawiningConfig.link3.split(",").join(" OR "),
            //     "facet": "All",
            //     "solrcollectionname": THIS.configData[0].database,
            //     "searchInDisplaycolumns": false,
            //     "start": 0,
            //     "rows": 100,
            //     "type": "OR"
            // };
            a.link3s = drawiningConfig.link3.split(",").join(" OR ");
            // callAjaxService(searchUrls["searchEntity"], callBackLink3Result, callBackFailure, request, "POST", null, false);
        }
        THIS.request = {
            "key": '',
            "facet": "All",
            "solrcollectionname": THIS.configData[0].database,
            "searchInDisplaycolumns": false,
            "start": 0,
            "rows": 100,
            "type": "detailed"
        };
        $('#searchModalDrilldown-content').html(str);
        THIS.link3Request = undefined;
        THIS.labelRequest = undefined;
        THIS.agileRequest = undefined;
        THIS.catalogRequest = undefined;
        synchronousAppend('', a);
        //Call Service and append them to the respective entities.        
        // if (agiles.length > 0) {
        //     if (!isAgileAppended) {
        //         // str += '<div class="row">';
        //         // str += '<h4 class="alert alert-success fade in margin-bottom-10"> Agile </h4>';
        //         // str += '<dl class="dl-horizontal">';
        //         // str += '<dt>Material</dt><dd id="AgileAppendComponent"></dd>';
        //         // // str += '<dt>Item Type</dt><dd>' + drawiningConfig.itemtype + '</dd>';
        //         // // str += '<dt>Manufacturer</dt><dd>' + drawiningConfig.manufacturer + '</dd>';
        //         // str += '</dl>';
        //         // str += '</div>';
        //         // str += '<div class="row" id="searchModalDrilldown-content-agile-img"></div>';

        //         str += '<li>';
        //         str += '<div class="rpd-feature-set">';
        //         str += '<div class="rpd-feature-set-header line"><div data-obj="agile" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
        //         str += '<h3 class="rpd-feature-set-title">Agile</h3></div>';
        //         str += '<div class="rpd-feature-set-body bpadding10 agile-expand">';
        //         str += '<div class="js-rpd-feature-set-innerbody line agile-expand1">';
        //         //Content-BEGIN
        //         str += '<div class="row">';
        //         str += '<div class="col-sm-12" id="searchModalDrilldown-content-agile-img"></div>';
        //         str += '</div>';
        //         str += '<div class="row">';
        //         str += '<div>';
        //         str += '<strong>Material : </strong><label id="AgileAppendComponent"></label><br />';
        //         str += '<strong>Finish : </strong><label id="AgileAppendFinish"></label>';
        //         str += '</div>';
        //         str += '</div>';
        //         str += '</div>';
        //         //Content-END
        //         str += '</div>'
        //         str += '</div>'
        //         str += '</li>';
        //         isAgileAppended = true;
        //         var key = '';
        //         for (var i = 0; i < agiles.length; i += 1) {
        //             if (i < (agiles.length - 1)) {
        //                 key += agiles[i] + ' OR ';
        //             } else {
        //                 key += agiles[i];
        //             }
        //         }
        //         var req = {
        //             "key": key,
        //             "facet": "All",
        //             "solrcollectionname": THIS.configData[0].database,
        //             "searchInDisplaycolumns": false,
        //             "start": 0,
        //             "rows": 100,
        //             "type": "OR"
        //         }
        //         callAjaxService(searchUrls["searchEntity"], appendAgile, function(err, statMsg, status) {
        //             setTimeout(function() {
        //                 callAjaxService(searchUrls["searchEntity"], appendAgile, callBackFailure, req, "POST", null, false);
        //             }, 3000);
        //         }, req, "POST", null, false);
        //     }
        // }

        // if (pCatalogs.length > 0) {
        //     if (!isPCAppended) {
        //         // str += '<div class="row">';
        //         // str += '<h4 class="alert alert-success fade in margin-bottom-10"> PRODUCT CATALOG </h4>';
        //         // str += '<dl class="dl-horizontal">';
        //         // str += '<dt>Category</dt><dd id="PCAppendCat"></dd>';
        //         // str += '<dt>Item Type</dt><dd id="PCAppendType"></dd>';
        //         // str += '<dt id="SC"></dt><dd id="PCAppendSCat"></dd>';
        //         // str += '</dl>';
        //         // str += '</div>';
        //         // str += '<div class="row" id="searchModalDrilldown-content-pc-img"></div>';

        //         str += '<li>';
        //         str += '<div class="rpd-feature-set">';
        //         str += '<div class="rpd-feature-set-header line"><div data-obj="pc" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
        //         str += '<h3 class="rpd-feature-set-title">Product Catalog</h3></div>';
        //         str += '<div class="rpd-feature-set-body bpadding10 pc-expand">';
        //         str += '<div class="js-rpd-feature-set-innerbody line pc-expand1">';
        //         //Content-BEGIN
        //         str += '<div class="row">';
        //         str += '<div class="col-sm-12" id="searchModalDrilldown-content-pc-img"></div>';
        //         str += '</div>';
        //         str += '<div class="row">';
        //         str += '<div>';
        //         str += '<strong>Category : </strong><label id="PCAppendCat"></label><br />';
        //         str += '<strong>Item Type : </strong><label id="PCAppendType"></label><br />';
        //         str += '<strong id="SC"></strong><label id="PCAppendSCat"></label><br />';
        //         str += '</div>';
        //         str += '</div>';
        //         str += '</div>';
        //         //Content-END
        //         str += '</div>'
        //         str += '</div>'
        //         str += '</li>';

        //         isPCAppended = true;
        //         var key = '';
        //         for (var i = 0; i < pCatalogs.length; i += 1) {
        //             if (i < (pCatalogs.length - 1)) {
        //                 key += pCatalogs[i] + ' OR ';
        //             } else {
        //                 key += pCatalogs[i];
        //             }
        //         }
        //         var request = {
        //             "key": key,
        //             "facet": "All",
        //             "solrcollectionname": THIS.configData[0].database,
        //             "searchInDisplaycolumns": false,
        //             "start": 0,
        //             "rows": 100,
        //             "type": "OR"
        //         }
        //         callAjaxService(searchUrls["searchEntity"], appendProductCatalogs, function(err, statMsg, status) {
        //             setTimeout(function() {
        //                 callAjaxService(searchUrls["searchEntity"], appendProductCatalogs, callBackFailure, request, "POST", null, false);
        //             }, 5000);
        //         }, request, "POST", null, false);

        //     }
        // }


        $('.thumb').unbind('click').bind('click', function() {
            $('.zoomContainer').remove();
            var imgDimension = { "width": "100%", "height": "100%" };
            if ($('#searchModalDrilldown-image-img').length === 0) {
                $('#searchModalDrilldown-image').html('<img onerror="System.imgError(this);" style="border-style: solid;" id="searchModalDrilldown-image-img" class="elevateZoom" src="' + $(this).attr('src') + '" />');
            } else {
                $('#searchModalDrilldown-image-img').attr('src', $(this).attr('src'));
            }
            $('#searchModalDrilldown-image-img').attr(imgDimension)
            $('#searchModalDrilldown-image-img').myElevateZoom('searchModalDrilldown-image-img', {
                zoomType: "lens",
                lensShape: "round",
                lensSize: 200,
                scrollZoom: true
            });
        });
        $('.expand').unbind('click').bind('click', function() {
            var type = '' + $(this).attr('data-obj');
            if ($(this).html() === '+') {
                $(this).html('-');
                $('.' + type + '-expand').removeClass('minhieght200');
                // $('.' + type + '-expand').addClass('feature-folded');
                $('.' + type + '-expand1').fadeIn('fast');
            } else {
                $(this).html('+');
                $('.' + type + '-expand').removeClass('feature-folded');
                // $('.' + type + '-expand').addClass('minhieght200');
                $('.' + type + '-expand1').fadeOut('fast');
            }
        });
        // $("#poDocView").css({ "height": $(window).height() - 150 }).empty()
        //     .append(
        //         $("<div/>").addClass("row").css("height", "250px")
        //         .append($("<div/>").addClass("col-sm-7").append($("<div/>").addClass("text-center padding-10")
        //             .append($("<img/>").addClass("detail-img").attr(imgDimension))))
        //         .append($("<div/>").addClass("col-sm-5").append(imageContent))
        //     );

        // $('.detailImg').remove();
        // $('#poDocView').find('.col-sm-2').css({ 'max-height': $('#poDocView').height(), "max-width": "200px", "overflow": "auto" });
        // setTimeout(function() { $("#poDocView img:last").elevateZoom({ zoomType: "lens", minZoomLevel: 0.9, lensShape: "round", lensSize: 250, scrollZoom: true, customClass: "detailImg" }); }, 500);
    };

    var synchronousAppend = function(appendedSourceName, sources) {
        var sourceSequence = ['Label', 'Agile', 'Item', 'PRODUCT_CATALOG', 'link3'];
        var index = -1;
        if (!!sources) {
            if (!!sources.lbls && sources.lbls !== [] && sources.lbls.length > 0) {
                THIS.labelPresent = true;
                var labels = sources.lbls;
                index = 0;
                var str = '';
                str += '<li>';
                str += '<div class="rpd-feature-set">';
                str += '<div class="rpd-feature-set-header line"><div data-obj="labels" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
                str += '<h3 class="rpd-feature-set-title">Labels</h3></div>';
                str += '<div class="rpd-feature-set-body bpadding10 labels-expand">';
                str += '<div class="js-rpd-feature-set-innerbody line labels-expand1">';
                //Content-BEGIN
                str += '<div class="row">';
                str += '<div class="col-sm-12" id="searchModalDrilldown-content-label-img"></div>';
                str += '</div>';
                //Content-END
                str += '</div>'
                str += '</div>'
                str += '</li>';

                // str += '<div class="row">';
                // str += '<h4 class="alert alert-success fade in margin-bottom-10"> LABELS </h4>';
                // str += '</div>';
                // str += '<div class="row" id="searchModalDrilldown-content-label-img"></div>';
                var key = '';

                for (var i = 0; i < labels.length; i += 1) {
                    if (i < (labels.length - 1)) {
                        key += labels[i] + ' OR ';
                    } else {
                        key += labels[i];
                    }
                }
                var req = JSON.parse(JSON.stringify(THIS.request));
                req.key = key;
                THIS.labelRequest = req;
                $('#searchModalDrilldown-content').append(str);
            }
            if (!!sources.cats && sources.cats !== [] && sources.cats.length > 0) {
                THIS.catalogPresent = true;
                if (index === -1) {
                    index = 3;
                }
                var pCatalogs = sources.cats;
                var str = '';
                str += '<li>';
                str += '<div class="rpd-feature-set">';
                str += '<div class="rpd-feature-set-header line"><div data-obj="pc" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
                str += '<h3 class="rpd-feature-set-title">Product Catalog</h3></div>';
                str += '<div class="rpd-feature-set-body bpadding10 pc-expand">';
                str += '<div class="js-rpd-feature-set-innerbody line pc-expand1">';
                //Content-BEGIN
                str += '<div class="row">';
                str += '<div class="col-sm-12" id="searchModalDrilldown-content-pc-img"></div>';
                str += '</div>';
                str += '<div class="row">';
                str += '<div>';
                str += '<strong>Category : </strong><label id="PCAppendCat"></label><br />';
                str += '<strong>Item Type : </strong><label id="PCAppendType"></label><br />';
                str += '<strong id="SC"></strong><label id="PCAppendSCat"></label><br />';
                str += '</div>';
                str += '</div>';
                str += '</div>';
                //Content-END
                str += '</div>'
                str += '</div>'
                str += '</li>';

                isPCAppended = true;
                var key = '';
                for (var i = 0; i < pCatalogs.length; i += 1) {
                    if (i < (pCatalogs.length - 1)) {
                        key += pCatalogs[i] + ' OR ';
                    } else {
                        key += pCatalogs[i];
                    }
                }

                var req = JSON.parse(JSON.stringify(THIS.request));
                req.key = key;
                THIS.catalogRequest = req;

                $('#searchModalDrilldown-content').append(str);
            }
            if (!!sources.agls && sources.agls !== [] && sources.agls.length > 0) {
                THIS.agilePresent = true;
                if (index === -1) {
                    index = 1;
                }
                var agiles = sources.agls;
                var str = '';
                str += '<li>';
                str += '<div class="rpd-feature-set">';
                str += '<div class="rpd-feature-set-header line"><div data-obj="agile" class="expand rpd-feature-circle fk-float-left js-rpd-feature-circle">-</div>';
                str += '<h3 class="rpd-feature-set-title">Agile</h3></div>';
                str += '<div class="rpd-feature-set-body bpadding10 agile-expand">';
                str += '<div class="js-rpd-feature-set-innerbody line agile-expand1">';
                //Content-BEGIN
                str += '<div class="row">';
                str += '<div class="col-sm-12" id="searchModalDrilldown-content-agile-img"></div>';
                str += '</div>';
                str += '<div class="row">';
                str += '<div>';
                str += '<strong>Material : </strong><label id="AgileAppendComponent"></label><br />';
                str += '<strong>Finish : </strong><label id="AgileAppendFinish"></label>';
                str += '</div>';
                str += '</div>';
                str += '</div>';
                //Content-END
                str += '</div>'
                str += '</div>'
                str += '</li>';
                isAgileAppended = true;
                var key = '';
                for (var i = 0; i < agiles.length; i += 1) {
                    if (i < (agiles.length - 1)) {
                        key += agiles[i] + ' OR ';
                    } else {
                        key += agiles[i];
                    }
                }
                var req = JSON.parse(JSON.stringify(THIS.request));
                req.key = key;
                THIS.agileRequest = req;
                $('#searchModalDrilldown-content').append(str);
            }
            if (!!sources.link3s && sources.link3s !== '') {
                THIS.link3Present = true;
                if (index === -1) {
                    index = 4;
                }
                var req = JSON.parse(JSON.stringify(THIS.request));
                req.key = sources.link3s;
                THIS.link3Request = req;
            }
            if (index === 0) {
                callAjaxService(searchUrls["searchEntity"], appendLabels, callBackFailure, THIS.labelRequest, 'POST');
            } else if (index === 1) {
                callAjaxService(searchUrls["searchEntity"], appendAgile, callBackFailure, THIS.agileRequest, 'POST')
            } else if (index === 2) {
                callAjaxService(searchUrls["searchEntity"], function(response) {
                    //Intentionally do nothing.
                }, callBackFailure, THIS.request, 'POST')
            } else if (index === 3) {
                callAjaxService(searchUrls["searchEntity"], appendProductCatalogs, callBackFailure, THIS.catalogRequest, 'POST')
            } else if (index === 4) {
                callAjaxService(searchUrls["searchEntity"], callBackLink3Result, callBackFailure, THIS.link3Request, 'POST')
            }
        } else {
            if (!!appendedSourceName && appendedSourceName !== '') {
                index = (!!!appendedSourceName || appendedSourceName === '') ? 0 : sourceSequence.indexOf(appendedSourceName);
                if (index >= sourceSequence.length) {
                    return;
                } else {
                    index += 1;
                    if (sourceSequence[index] === 'Item') {
                        index += 1;
                    }
                    if (index === 0) {
                        if (!THIS.labelRequest) {
                            index += 1;
                        }
                    }
                    if (index === 1) {
                        if (!THIS.agileRequest) {
                            index += 1;
                        }
                    }
                    if (index === 2) {
                        if (!THIS.itemRequest) {
                            index += 1;
                        }
                    }
                    if (index === 3) {
                        if (!THIS.catalogRequest) {
                            index += 1;
                        }
                    }
                    if (index === 4) {
                        if (!THIS.link3Request) {
                            return;
                        }
                    }
                }
            }
            if (index > -1) {
                if (index === 0) {
                    if (THIS.labelRequest) {
                        callAjaxService(searchUrls["searchEntity"], appendLabels, callBackFailure, THIS.labelRequest, 'POST');
                    }
                } else if (index === 1) {
                    if (THIS.agileRequest) {
                        callAjaxService(searchUrls["searchEntity"], appendAgile, callBackFailure, THIS.agileRequest, 'POST')
                    }
                } else if (index === 2) {
                    if (THIS.itemRequest) {
                        callAjaxService(searchUrls["searchEntity"], function(response) {
                            //Intentionally do nothing.
                        }, callBackFailure, THIS.request, 'POST')
                    }
                } else if (index === 3) {
                    if (THIS.catalogRequest) {
                        callAjaxService(searchUrls["searchEntity"], appendProductCatalogs, callBackFailure, THIS.catalogRequest, 'POST')
                    }
                } else if (index === 4) {
                    if (THIS.link3Request) {
                        callAjaxService(searchUrls["searchEntity"], callBackLink3Result, callBackFailure, THIS.link3Request, 'POST')
                    }
                }
            }
        }
    }

    var appendProductCatalogs = function(response) {
        var str = '<section class="regular slider" id="pc_regular">';
        if (!!!response || response.isException) {
            return;
        }
        if (response.length > 0) {
            $('#PCAppendCat').html(response[0].category);
            $('#PCAppendType').html(response[0].itemtype);
            if (response[0].subcategory && $.trim(response[0].subcategory) !== "") {
                $('#SC').html('Subcategory : ');
                $('#PCAppendSCat').html($.trim(response[0].subcategory));
            }
            $('#PCAppendCat').html(response[0].category);
            for (var i = 0; i < response.length; i += 1) {
                str += '<div>';
                str += '<img onerror="System.imgError(this);" class="img-thumbnail thumb" style="width: 100%; cursor: pointer;" src="' + THIS.qfindURL + response[i].filepath + '" />';
                str += '</div>';
            }
            var link3s = '';
            for (var i = 0; i < response.length; i += 1) {
                if (!!response[i].link3 && response[i].link3 !== '') {
                    if (i < (response.length - 1)) {
                        link3s += response[i].link3 + ',';
                    } else {
                        link3s += response[i].link3;
                    }
                }
            }
            if (link3s && link3s !== '') {
                var request = {
                    "key": link3s.split(",").join(" OR "),
                    "facet": "All",
                    "solrcollectionname": THIS.configData[0].database,
                    "searchInDisplaycolumns": false,
                    "start": 0,
                    "rows": 100,
                    "type": "detailed"
                };
                callAjaxService(searchUrls["searchEntity"], callBackLink3Result, callBackFailure, request, "POST", null, false);
            }
        }
        str += '</section>';
        $('#searchModalDrilldown-content-pc-img').html(str);

        $("#pc_regular").slick({
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1
        });
        $('.thumb').unbind('click').bind('click', function() {
            $('.zoomContainer').remove();
            var imgDimension = { "width": "100%", "height": "100%" };
            if ($('#searchModalDrilldown-image-img').length === 0) {
                $('#searchModalDrilldown-image').html('<img onerror="System.imgError(this);" style="border-style: solid;" id="searchModalDrilldown-image-img" class="elevateZoom" src="' + $(this).attr('src') + '" />');
            } else {
                $('#searchModalDrilldown-image-img').attr('src', $(this).attr('src'));
            }
            $('#searchModalDrilldown-image-img').attr(imgDimension)
            $('#searchModalDrilldown-image-img').myElevateZoom('searchModalDrilldown-image-img', {
                zoomType: "lens",
                lensShape: "round",
                lensSize: 200,
                scrollZoom: true
            });
        });
        synchronousAppend('PRODUCT_CATALOG');
    }

    var appendLabels = function(response) {
        var str = '<section class="regular slider" id="label_regular">';
        if (!!!response || response.isException) {
            return;
        }
        if (response.length > 0) {
            for (var i = 0; i < response.length; i += 1) {
                str += '<div>';
                str += '<img onerror="System.imgError(this);" class="img-thumbnail thumb" style="height: 100px; cursor: pointer;" src="' + THIS.qfindURL + response[i].filepath + '" />';
                str += '</div>';
            }
        }
        str += '</section>';
        $('#searchModalDrilldown-content-label-img').html(str);

        $("#label_regular").slick({
            dots: false,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 3
        });
        $('.thumb').unbind('click').bind('click', function() {
            $('.zoomContainer').remove();
            var imgDimension = { "width": "100%", "height": "100%" };
            if ($('#searchModalDrilldown-image-img').length === 0) {
                $('#searchModalDrilldown-image').html('<img onerror="System.imgError(this);" style="border-style: solid;" id="searchModalDrilldown-image-img" class="elevateZoom" src="' + $(this).attr('src') + '" />');
            } else {
                $('#searchModalDrilldown-image-img').attr('src', $(this).attr('src'));
            }
            $('#searchModalDrilldown-image-img').attr(imgDimension)
            $('#searchModalDrilldown-image-img').myElevateZoom('searchModalDrilldown-image-img', {
                zoomType: "lens",
                lensShape: "round",
                lensSize: 200,
                scrollZoom: true
            });
        });
        synchronousAppend('Label');
    }

    var appendAgile = function(response) {
        var str = '<section class="regular slider" id="agile_regular">';
        if (!!!response || response.isException) {
            return;
        }
        if (response.length > 0) {
            $('#AgileAppendComponent').html(response[0].components);
            $('#AgileAppendFinish').html(response[0].finished_goods_number);
            for (var i = 0; i < response.length; i += 1) {
                str += '<div>';
                str += '<img onerror="System.imgError(this);" class="img-thumbnail thumb" style="width: 100%; height: 90%; cursor: pointer;" src="' + THIS.qfindURL + response[i].filepath + '" />';
                str += '</div>';
            }
        }
        str += '</section>';
        $('#searchModalDrilldown-content-agile-img').html(str);

        $("#agile_regular").slick({
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1
        });
        $('.thumb').unbind('click').bind('click', function() {
            $('.zoomContainer').remove();
            var imgDimension = { "width": "100%", "height": "100%" };
            if ($('#searchModalDrilldown-image-img').length === 0) {
                $('#searchModalDrilldown-image').html('<img onerror="System.imgError(this);" style="border-style: solid;" id="searchModalDrilldown-image-img" class="elevateZoom" src="' + $(this).attr('src') + '" />');
            } else {
                $('#searchModalDrilldown-image-img').attr('src', $(this).attr('src'));
            }
            $('#searchModalDrilldown-image-img').attr(imgDimension)
            $('#searchModalDrilldown-image-img').myElevateZoom('searchModalDrilldown-image-img', {
                zoomType: "lens",
                lensShape: "round",
                lensSize: 200,
                scrollZoom: true
            });
        });
        synchronousAppend('Agile');
    }

    var linkClick = function(obj, sourceName, buttonColor) {
        $("#myModalLabel", "#myModalDrilldown").empty();
        $("#poDocView", "#myModalDrilldown").empty();

        enableLoading();
        var refId = $(obj).attr("refid");
        var request = {
            "key": refId.replace(',', ' OR '),
            "facet": "All",
            "solrcollectionname": THIS.configData[0].database,
            "searchInDisplaycolumns": false,
            "start": 0,
            "rows": 100,
            "type": "detailed"
        };
        if (sourceName) {
            callAjaxService(searchUrls["searchEntity"], function(response) { callBackLinkClickResult(response, sourceName, buttonColor) }, callBackFailure, request, "POST");
            $("#myModalDrilldown").modal("show");
        } else {
            request.type = 'detailed';
            callAjaxService(searchUrls["searchEntity"], callBackDetailViewResult, callBackFailure, request, "POST");
        }
    };

    var callBackLinkClickResult = function(response, sourceName, buttonColor) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        var drawiningConfig = response[0];
        if (sourceName) {
            $("#myModalLabel", "#myModalDrilldown").html(" - " + drawiningConfig.catalognumber1 + " - " + drawiningConfig.catalogdesc1).prepend($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).html(sourceName));
        }
        $(".zoomContainer").addClass("hide");
        var images = [];
        for (var i = 0; i < response.length; i++) {
            images.push(response[i].filepath);
        }
        var imagepath = THIS.qfindURL + response[0].filepath;
        var imageDiv;
        if (images.length > 1) {
            imageDiv = $('<div/>');
            for (var imgIndex = 0; imgIndex < images.length; imgIndex++) {
                var imageUrl = THIS.qfindURL + images[imgIndex];
                imageDiv.append($('<div/>').addClass('padding-2').append($('<small/>').css({ 'margin': '5px 10px', 'font-weight': 'bold' }).html(imgIndex + 1))
                    .append($('<img/>').addClass('img-thumbnail').css({ 'width': '100px', 'background-color': imgIndex === 0 ? '#3276b1' : '' })
                        .attr({ 'src': imageUrl }).unbind('click').bind('click', function() {
                            var imageIndex = parseInt($(this).parent().find('small').html()) - 1;
                            var imageUrl = THIS.qfindURL + response[imageIndex].filepath;
                            $("#myModalLabel", "#myModalDrilldown").html(" - " + response[imageIndex].catalognumber1 + " - " + response[imageIndex].catalogdesc1).prepend($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).html(sourceName));
                            $("#poDocView img").css('background-color', '');
                            $(this).css('background-color', '#3276b1');
                            $("#poDocView img:last").attr({ 'src': imageUrl });
                            setTimeout(function() { $("#poDocView img:last").elevateZoom({ zoomType: "lens", minZoomLevel: 0.1, lensShape: "round", lensSize: 250, scrollZoom: true, customClass: "detailImg" }); }, 500);
                        })));
            }
        }
        var imgDimension;
        if (drawiningConfig.datasource === "PRODUCT_CATALOG") {
            imgDimension = { "width": $(window).width() - 600 + "px", "src": imagepath, "alt": title };
        } else {
            imgDimension = { "height": $(window).height() - 150 + "px", "src": imagepath, "alt": title };
        }
        var imageContent = $("<div/>").addClass("padding-top-10");

        imageContent.append($("<h4/>").addClass("alert alert-success fade in margin-bottom-10").append($("<strong/>").attr({ "href": "javascript:void(0);" }).html("Details")));
        var sectionDL = $("<dl/>").addClass("dl-horizontal");
        imageContent.append(sectionDL);
        sectionDL.append($("<dt/>").html("Country of Origin")).append($("<dd/>").html(drawiningConfig.coo_desc));
        sectionDL.append($("<dt/>").html("Item Type")).append($("<dd/>").html(drawiningConfig.itemtype));
        sectionDL.append($("<dt/>").html("Item Status")).append($("<dd/>").html(drawiningConfig.itemstatus));
        sectionDL.append($("<dt/>").html("Manufacturer")).append($("<dd/>").html(drawiningConfig.manufacturer));

        if (drawiningConfig.datasource === "AGILE") {
            var agileDL = $("<dl/>").addClass("dl-horizontal");
            agileDL.append($("<dt/>").html("Material")).append($("<dd/>").html(drawiningConfig.components));
            agileDL.append($("<dt/>").html("Finish")).append($("<dd/>").html(drawiningConfig.finished_goods_number));
            imageContent.append(agileDL);
        }

        if (drawiningConfig.category && $.trim(drawiningConfig.category) !== "") {
            imageContent.append($("<h4/>").addClass("alert alert-success fade in margin-bottom-10").append($("<strong/>").attr({ "href": "javascript:void(0);" }).html("PRODUCT CATALOG")));
            var categoryDL = $("<dl/>").addClass("dl-horizontal");
            categoryDL.append($("<dt/>").html("Category")).append($("<dd/>").html($.trim(drawiningConfig.category)));
            if (drawiningConfig.subcategory && $.trim(drawiningConfig.subcategory) !== "") {
                categoryDL.append($("<dt/>").html("Subcategory")).append($("<dd/>").html($.trim(drawiningConfig.subcategory)));
            }
            imageContent.append(categoryDL);
        }
        /*if (drawiningConfig.category && $.trim(drawiningConfig.category) !== ""){
        	imageContent.append("<br/>");
        	imageContent.append($("<i/>").addClass("padding-left-25").html("Category: ").append($("<a/>").attr("href","javascript:void(0);").append($.trim(drawiningConfig.category))));
        }
        if (drawiningConfig.subcategory && $.trim(drawiningConfig.subcategory) !== ""){
        	imageContent.append("<br/>");
        	imageContent.append($("<i/>").addClass("padding-left-25").html("Category: ").append($("<a/>").attr("href","javascript:void(0);").append($.trim(drawiningConfig.subcategory))));
        }*/
        if (drawiningConfig.datasource === "ITEM_MASTER") {
            $("#poDocView").css({ "height": $(window).height() - 150 }).empty()
                .append(
                    $("<div/>").addClass("row")
                    .append($("<div/>").addClass("col-sm-12").append(imageContent))
                );
            $('.detailImg').remove();
            $('#poDocView').find('.col-sm-2').css({ 'max-height': $('#poDocView').height(), "max-width": "200px", "overflow": "auto" });
            setTimeout(function() { $("#poDocView img:last").elevateZoom({ zoomType: "lens", minZoomLevel: 0.9, lensShape: "round", lensSize: 250, scrollZoom: true, customClass: "detailImg" }); }, 500);
        } else if (drawiningConfig.datasource === "LABEL") {
            if (imageDiv) {
                $("#poDocView").css({ "height": $(window).height() - 150 }).empty()
                    .append(
                        $("<div/>").addClass("row")
                        .append($("<div/>").addClass("col-sm-2").append(imageDiv ? imageDiv : ''))
                        .append($("<div/>").addClass("col-sm-6").append($("<div/>").addClass("text-center padding-10")
                            .append($("<img/>").addClass("detail-img").attr(imgDimension))))
                        .append($("<div/>").addClass("col-sm-4").append(imageContent))
                    );
            } else {
                $("#poDocView").css({ "height": $(window).height() - 150 }).empty()
                    .append(
                        $("<div/>").addClass("row")
                        .append($("<div/>").addClass("col-sm-8").append($("<div/>").addClass("text-center padding-10")
                            .append($("<img/>").addClass("detail-img").attr(imgDimension))))
                        .append($("<div/>").addClass("col-sm-4").append(imageContent))
                    );
            }
            $('.detailImg').remove();
            $('#poDocView').find('.col-sm-2').css({ 'max-height': $('#poDocView').height(), "max-width": "200px", "overflow": "auto" });
            setTimeout(function() { $("#poDocView img:last").elevateZoom({ zoomType: "lens", minZoomLevel: 0.9, lensShape: "round", lensSize: 250, scrollZoom: true, customClass: "detailImg" }); }, 500);
        } else {
            $("#poDocView").css({ "height": $(window).height() - 150 }).empty()
                .append(
                    $("<div/>").addClass("row")
                    .append($("<div/>").addClass("col-sm-8").append($("<div/>").addClass("text-center padding-10")
                        .append($("<img/>").addClass("detail-img").attr(imgDimension))))
                    .append($("<div/>").addClass("col-sm-4").append(imageContent))
                );
            $('.detailImg').remove();
            $('#poDocView').find('.col-sm-2').css({ 'max-height': $('#poDocView').height(), "max-width": "200px", "overflow": "auto" });
            setTimeout(function() { $("#poDocView img:last").elevateZoom({ zoomType: "lens", minZoomLevel: 0.9, lensShape: "round", lensSize: 250, scrollZoom: true, customClass: "detailImg" }); }, 500);
        }
    };

    var callBackFacetFreeFormSearchResult = function(response) {
        disableLoading();
        if (!response) {
            return;
        }
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        if (currentRow > 0 && response.length === 0) {
            currentRow -= 10;
            return;
        }
        if (THIS.mode !== 'shortlist') {
            var serachResults = $("<div/>").addClass("tab-pane fade in active");

            if (!currentRow > 0) {
                $("#searchContent").append($("<div/>").attr("id", "freeFormTabContent").addClass("tab-content padding-10").append(serachResults));
                if (THIS.type === "similar") {
                    serachResults.append($("<h1/>").addClass("font-md").html("<span class='text-danger'>No results found for</span> ").append($("<span/>").addClass("semi-bold").html(THIS.key)).append($("<br/>")).append($("<h3/>").addClass("font-normal").html("However these are Simliar Results.")));
                } else {
                    serachResults.append($("<h1/>").addClass("font-md").html("Search Results for ").append($("<span/>").addClass("semi-bold").html(THIS.key)).append($("<small/>").addClass("text-danger").html(" &nbsp;&nbsp;(" + recordsCounts[THIS.facet] + " results)")));
                }
            } else {
                $("#freeFormTabContent").append(serachResults);
            }
        } else {
            $("#searchContent").empty();
            var serachResults = $("<div/>").addClass("tab-pane fade in active").append('<div style="border-bottom: 1px solid #ccc"><div class="pull-right"><a class="btn btn-warning pull-right backToProduct" href="javascript:void(0);" title="Back to Search Results"><i class="fa fa-mail-reply"></i> Back</a></div><div><h1 class="txt-color-blueDark"><i class="fa-fw fa fa-star-o"></i> My Shortlist</h1></div></div>');

            $('#searchDivId').hide();
            $("#searchContent").append($("<div/>").attr("id", "freeFormTabContent").addClass("tab-content padding-10").append(serachResults));
            $("#freeFormTabContent").append(serachResults);
            $('.backToProduct').unbind('click').bind('click', function() {
                $('#searchDivId').show();
                THIS.compareClose();
                THIS.mode = 'search';
                THIS.resetPage();
            });
            if ($('.dropdown-backdrop').length > 0) {
                $('.dropdown-backdrop').remove();
            }
        }

        for (var i = 0; i < response.length; i++) {
            var elevateZoomType;
            var systemName;
            var systemNameClass;
            var j = currentRow + i;
            if (response[i].datasource === "AGILE") {
                elevateZoomType = "elevateZoom-window";
                systemName = "Agile";
                systemNameClass = "btn-outline";
            } else if (response[i].datasource === "LABEL") {
                elevateZoomType = "elevateZoom-window";
                systemName = "Label";
                systemNameClass = "btn-outline";
            } else if (response[i].datasource === "ITEM_MASTER") {
                elevateZoomType = "elevateZoom-window";
                systemName = "Item Master";
                systemNameClass = "btn-outline";
            } else {
                elevateZoomType = "elevateZoom-lens";
                systemName = "Product Catalog";
                systemNameClass = "btn-outline";
            }
            var imagePath = THIS.qfindURL + response[i].filepath;
            THIS.resultKvp[response[i].id] = response[i];
            var text = 'Shortlist';
            var cls = '';
            if (THIS.ShortlistItems.indexOf(response[i].id) !== -1) {
                text = 'Shortlisted';
                cls = ' shortlisted';
            }
            if (response[i].catalognumber1 !== "") {
            	var bomDisplay = 'none';
            	var bomDisplay = (bomIds.indexOf(response[i].catalognumber1) > -1) ? 'true' : 'none';
                var searchRecord = $("<div/>").addClass("search-results clearfix")
                    /*.bind( "click", {
                            				  config: response[i]
                            			}, function( event) {
                            			  onDrawingClick(event);
                            			})*/
                    .append($("<div/>").addClass("pull-right")
                        // .append($("<p/>").addClass("note")
                        .append($("<div/>").addClass("padding-10")
                            .append($("<a/>").attr({
                                "id": "shortlist" + j,
                                "data-solrid": response[i].id
                            }).unbind("click").bind("click", function() { THIS.shortlist(this); }).addClass("action-btn" + cls).append($("<span/>").addClass("fa fa-lg fa-fw fa-star-o margin-3 txt-color-darken")).append($("<span/>").addClass("text-mouseover").html(text)))
                            .append($("<a/>").attr({
                                "id": "compare" + j,
                                "data-solrid": response[i].id,
                                "data-sequence": j
                            }).addClass("action-btn").unbind("click").bind("click", function() { THIS.addToCompare(this); }).append($("<span/>").addClass("fa fa-lg fa-fw fa-plus-square margin-3 txt-color-darken")).append($("<span/>").addClass("text-mouseover").html("Add to Compare")))
                            .append($("<a/>").attr({
                                "id": "bom" + j,
                                "data-solrid": response[i].catalognumber1,
                                "data-sequence": j
                            }).addClass("action-btn").css("display", bomDisplay).unbind("click").bind("click", function() { THIS.loadBOMCUSA(this); }).append($("<span/>").addClass("fa fa-lg fa-fw fa-sitemap margin-3 txt-color-darken")).append($("<span/>").addClass("text-mouseover").html("BOM CUSA")))
                            // .append($("<a/>").attr("href", "javascript:void(0);").append($("<i/>").addClass("fa fa-thumbs-up").prepend(" Like this link&nbsp;&nbsp;")))
                            // .append($("<a/>").attr("href", "javascript:void(0);").append($("<i/>").addClass("fa fa-chain").prepend(" Share this link&nbsp;&nbsp;")))
                            // .append($("<a/>").attr("href", "javascript:void(0);").append($("<i/>").addClass("fa fa-star txt-color-yellow").prepend(" Favorite&nbsp;&nbsp;")))
                        ));
                searchRecord.append($("<h4/>").append($("<a/>").attr({ "href": "javascript:void(0);", "refid": response[i].id }).addClass("valid-document").html(response[i].catalognumber1 + " - " + response[i].catalogdesc1)));

                if (response[i].datasource !== "ITEM_MASTER") {
                    searchRecord.append($("<img/>").attr({
                        "src": imagePath,
                        "data-zoom-image": imagePath,
                        "onerror": "System.imgError(this);"
                    }).addClass("elevateZoom " + elevateZoomType));
                }


                //searchRecord.append($("<br/>"));
                serachResults.append(searchRecord);
                var imageContent = $("<div/>").addClass("padding-top-10");
                searchRecord.append(imageContent);

                imageContent.append($("<i/>").addClass("padding-left-25").html("Data Source: ").append($("<a/>").attr("href", "javascript:void(0);").append($("<button/>").addClass(systemNameClass + " btn margin-right-2").attr("href", "javascript:void(0);").html(systemName))));
                imageContent.append($("<i/>").addClass("padding-left-25").html("Country of Origin: ").append($("<a/>").attr("href", "javascript:void(0);").append(response[i].coo_desc)));
                imageContent.append($("<i/>").addClass("padding-left-25").html("Item Type: ").append($("<a/>").attr("href", "javascript:void(0);").append(response[i].itemtype)));
                if (response[i].datasource === "PRODUCT_CATALOG") {
                    imageContent.append("<br/>");
                    imageContent.append("<br/>");
                }
                imageContent.append($("<i/>").addClass("padding-left-25").html("Item Status: ").append($("<a/>").attr("href", "javascript:void(0);").append(response[i].itemstatus)));
                imageContent.append($("<i/>").addClass("padding-left-25").html("Manufacturer: ").append($("<a/>").attr("href", "javascript:void(0);").append(response[i].manufacturer)));
                if ((response[i].category && $.trim(response[i].category) !== "") || (response[i].components && $.trim(response[i].components) !== "")) {
                    imageContent.append("<br/>");
                    imageContent.append("<br/>");
                }
                if (response[i].components && $.trim(response[i].components) !== "") {
                    imageContent.append($("<i/>").addClass("padding-left-20").html("Material: ").append($("<a/>").attr("href", "javascript:void(0);").append(response[i].components)));
                }
                if (response[i].components && $.trim(response[i].finished_goods_number) !== "") {
                    imageContent.append($("<i/>").addClass("padding-left-20").html("Finish: ").append($("<a/>").attr("href", "javascript:void(0);").append(response[i].finished_goods_number)));
                }
                if (response[i].category && $.trim(response[i].category) !== "") {
                    imageContent.append($("<i/>").addClass("padding-left-20").html("Category: ").append($("<a/>").attr("href", "javascript:void(0);").append($.trim(response[i].category))));
                }
                if (response[i].subcategory && $.trim(response[i].subcategory) !== "") {
                    imageContent.append($("<i/>").addClass("padding-left-20").html("Subcategory: ").append($("<a/>").attr("href", "javascript:void(0);").append($.trim(response[i].subcategory))));
                }


                var links = response[i].link1 || "";
                var links2 = response[i].link2 || "";
                links = links.split(",");
                if (links.length > 0) {
                    imageContent.append("<br/>");
                    imageContent.append("<br/>");
                    var linkButtons = $("<a/>").attr("href", "javascript:void(0);");
                    var ITM = [];
                    var AGILE = [];
                    var CAT = [];
                    var LABEL = [];
                    for (var j = 0; j < links.length; j++) {
                        var linkSourceName = links[j];
                        if (linkSourceName.indexOf("ITM_") !== -1) {
                            ITM.push(linkSourceName);
                        } else if (linkSourceName.indexOf("AGILE_") !== -1) {
                            AGILE.push(linkSourceName);
                        } else if (linkSourceName.indexOf("CAT_") !== -1) {
                            CAT.push(linkSourceName);
                        } else if (linkSourceName.indexOf("LABEL_") !== -1) {
                            LABEL.push(linkSourceName);
                        }
                    }
                    var sourceName;
                    var buttonColor;
                    var isRelatedPresent = false;
                    if (ITM.length > 0 && secondaryRoles.contains("Item Master".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName = "Item Master";
                        buttonColor = "btn-info";
                        linkButtons.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).attr({ "href": "javascript:void(0);", "refid": ITM }).html(sourceName).click(function() { linkClick(this, "Item Master", "btn-info"); }));
                    }
                    if (AGILE.length > 0 && secondaryRoles.contains("Agile".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName = "Agile";
                        buttonColor = "btn-info";
                        linkButtons.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).attr({ "href": "javascript:void(0);", "refid": AGILE }).html(sourceName).click(function() { linkClick(this, "Agile", "btn-info"); }));
                    }
                    if (CAT.length > 0 && secondaryRoles.contains("Product Catalog".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName = "Product Catalog";
                        buttonColor = "btn-success";
                        linkButtons.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).attr({ "href": "javascript:void(0);", "refid": CAT }).html(sourceName).click(function() { linkClick(this, "Product Catalog", "btn-success"); }));
                    }
                    if (LABEL.length > 0 && secondaryRoles.contains("Label".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName = "Label";
                        buttonColor = "btn-warning";
                        linkButtons.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor).attr({ "href": "javascript:void(0);", "refid": LABEL.join(" OR ") }).html(sourceName).click(function() { linkClick(this, "Label", "btn-warning"); }));
                    }
                    if (isRelatedPresent) {
                        imageContent.append($("<i/>").addClass("padding-left-25").html("Related Documents: ").append(linkButtons));
                    }
                }
                links2 = links2.split(",");
                if (links2.length > 0 && links2[0] !== "") {
                    var linkButtons2 = $("<a/>").attr("href", "javascript:void(0);");
                    var ITM2 = [];
                    var AGILE2 = [];
                    var CAT2 = [];
                    var LABEL2 = [];
                    for (var j = 0; j < links2.length; j++) {
                        var linkSourceName2 = links2[j];
                        if (linkSourceName2.indexOf("ITM_") !== -1) {
                            ITM2.push(linkSourceName2);
                        } else if (linkSourceName2.indexOf("AGILE_") !== -1) {
                            AGILE2.push(linkSourceName2);
                        } else if (linkSourceName2.indexOf("CAT_") !== -1) {
                            CAT2.push(linkSourceName2);
                        } else if (linkSourceName2.indexOf("LABEL_") !== -1) {
                            LABEL2.push(linkSourceName2);
                        }
                    }
                    var sourceName2;
                    var buttonColor2;
                    var isRelatedPresent = false;
                    if (ITM2.length > 0 && secondaryRoles.contains("Item Master".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName2 = "Item Master";
                        buttonColor2 = "btn-info";
                        linkButtons2.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor2).attr({ "href": "javascript:void(0);", "refid": ITM2 }).html(sourceName2).click(function() { linkClick(this, "Item Master", "btn-info"); }));
                    }
                    if (AGILE2.length > 0 && secondaryRoles.contains("Agile".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName2 = "Agile";
                        buttonColor2 = "btn-info";
                        linkButtons2.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor2).attr({ "href": "javascript:void(0);", "refid": AGILE2 }).html(sourceName2).click(function() { linkClick(this, "Agile", "btn-info"); }));
                    }
                    if (CAT2.length > 0 && secondaryRoles.contains("Product Catalog".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName2 = "Product Catalog";
                        buttonColor2 = "btn-success";
                        linkButtons2.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor2).attr({ "href": "javascript:void(0);", "refid": CAT2 }).html(sourceName2).click(function() { linkClick(this, "Product Catalog", "btn-success"); }));
                    }
                    if (LABEL2.length > 0 && secondaryRoles.contains("Label".toUpperCase())) {
                        isRelatedPresent = true;
                        sourceName2 = "Label";
                        buttonColor2 = "btn-warning";
                        linkButtons2.append($("<button/>").addClass("btn btn-xs margin-right-2 " + buttonColor2).attr({ "href": "javascript:void(0);", "refid": LABEL2.join(" OR ") }).html(sourceName2).click(function() { linkClick(this, "Label", "btn-warning"); }));
                    }
                    if (isRelatedPresent) {
                        imageContent.append($("<i/>").addClass("padding-left-25").html("Closest Documents: ").append(linkButtons2));
                    }
                }
                //<i class="text-ellipsis">Clinical specialty: <a data-type="Clinical speciality" href="javascript:void(0);" id="cs0"><button class="btn-outline btn margin-right-2" href="javascript:void(0);"><em>spine</em></button></a></i>
                /*
				<div class="url text-success">
					http://www.wrapbootstrap.com <i class="fa fa-caret-down"></i>
				</div>
				<p class="description">
					It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here...
				</p>
			</div>
		</div>*/
            } else {
                var ids = response[i].id.split("_");
                var fileName;
                if (ids[0] === "CAT") {
                    fileName = "CV Catalog PDF Page - " + ids[2];
                } else if (ids[0].startsWith('F-')) {
                    fileName = "Packaging PDF Page - " + ids[2];
                }

                var str = '<div class="search-results clearfix absoluteCenterWrapper">';

                if (imagePath && imagePath !== '') {
                    str += '<br /><img onerror="System.imgError(this);" src="' + imagePath.replace('.pdf', '.png') + '" data-zoom-image="' + imagePath.replace('.pdf', '.png') + '" onerror="System.imgError(this);" style="vertical-align: middle;" class="elevateZoom elevateZoom-window absoluteCenter" />';
                    // pdfLink.append($("<img/>").attr({
                    //     "src": imagePath,
                    //     "data-zoom-image": imagePath,
                    //     "onerror": "System.imgError(this);"
                    // }).css('vertical-align', 'middle').addClass("elevateZoom " + 'elevateZoom-window'));
                }
                str += '<a style="float: right" id="shortlist' + j + '" data-solrid="' + response[i].id + '" class="action-btn slist ' + cls + '"><span class="fa fa-lg fa-fw fa-star-o margin-3 txt-color-darken"></span><span class="text-mouseover">' + text + '</span></a>';
                str += '<a style="float: right" id="compare' + j + '" data-sequence= "' + j + '" data-solrid="' + response[i].id + '" class="action-btn cmpr"><span class="fa fa-lg fa-fw fa-plus-square margin-3 txt-color-darken"></span><span class="text-mouseover">Add to Compare</span></a>';

                str += '<a style="margin-left: 10em;" target="_blank" href="web/pdfviewer/viewer.html?file=' + THIS.qfindURL + response[i].filepath + '?#search=' + THIS.key + '"><strong style="font-size: large; text-decoration: underline;"> ' + fileName + '</strong></a>';
                str += '<p>';
                if (response[i].title && response[i].title !== '') {
                    response[i].title = response[i].title.replace(THIS.key, '<b>' + THIS.key + '</b>');
                    str += '<i>' + response[i].title + '</i>';
                }
                str += '</p>';
                str += '</div>';
                // var pdfLink = $("<div/>").addClass("search-results clearfix")
                //     .append($("<h4/>").append($("<a/>").attr({ "target": "_blank", "href": "web/pdfviewer/viewer.html?file=" + THIS.qfindURL + response[i].filepath + "?#search=" + THIS.key }).html(fileName)));

                // if (imagePath && imagePath !== '') {
                //     pdfLink.append($("<img/>").attr({
                //         "src": imagePath,
                //         "data-zoom-image": imagePath,
                //         "onerror": "System.imgError(this);"
                //     }).css('vertical-align', 'middle').addClass("elevateZoom " + 'elevateZoom-window'));
                // }
                serachResults.append(str);
                $('.slist').unbind('click').bind('click', function() {
                    THIS.shortlist($(this));
                });
                $('.cmpr').unbind('click').bind('click', function() {
                    THIS.addToCompare($(this));
                })
            }
        }
        $(".elevateZoom-window").elevateZoom({
            zoomWindowPosition: 2,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            tint: true,
            tintColour: 'red',
            tintOpacity: 0.3,
            minZoomLevel: 0.3
        });
        $(".elevateZoom-lens").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lensSize: 150,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            minZoomLevel: 0.05,
            /*tint : true,
            tintColour : 'red',
            tintOpacity : 0.3,
            minZoomLevel:0.3*/
        });
        $(".search-results > h4 > a.valid-document").click(function() { linkClick(this); })
    };
    var facetFreeFormSearch = function(facet, start, rows, isAppend) {
        if (!!!isAppend) {
            $("#freeFormTabContent").empty();
            $("#freeFormTabContent").remove();
        }
        start = start || 0;
        rows = rows || 10;
        THIS.facet = facet;
        enableLoading();
        var request = {
            "key": THIS.key,
            "facet": THIS.facet,
            "solrcollectionname": THIS.configData[0].database,
            "searchInDisplaycolumns": false,
            "start": start,
            "rows": rows
        };
        THIS.request = request;
        if (THIS.type === "similar") {
            request.type = "similar";
        }
        callAjaxService(searchUrls["searchEntity"], callBackFacetFreeFormSearchResult, callBackFailure, request, "POST");
    };

    this.addToCompare = function(obj) {
        //New Logic using KVP
        var drawingObj = {};
        var isAdd = false,
            title = '';
        if (!!obj) {
            if (Object.keys(THIS.compareKvp).length === 2 && !THIS.compareKvp[$(obj).attr('data-solrid')]) {
                showNotification("error", "You can compare only two items. Remove existing item to add new item.");
                return;
            }



            if (!!THIS.compareKvp[$(obj).attr('data-solrid')]) {
                //delete THIS.compareKvp[$(obj).attr('data-solrid')];
                $(obj).toggleClass("addedToCompare");
                $(obj).find(".text-mouseover").html($(obj).find(".text-mouseover").text() == 'Add to Compare' ? 'Added to Compare' : 'Add to Compare');
                title = THIS.compareKvp[$(obj).attr('data-solrid')].catalogdesc1;
            } else if ($(obj).attr('data-solrid') && $(obj).attr('data-solrid') !== '' && !!THIS.resultKvp[$(obj).attr('data-solrid')]) {
                THIS.compareKvp[$(obj).attr('data-solrid')] = JSON.parse(JSON.stringify(THIS.resultKvp[$(obj).attr('data-solrid')]));
                if (!THIS.compareKvp[$(obj).attr('data-solrid')].catalogdesc1 || THIS.compareKvp[$(obj).attr('data-solrid')].catalogdesc1 === '') {
                    var ids = THIS.compareKvp[$(obj).attr('data-solrid')].id.split("_");
                    var fileName;
                    if (ids[0] === "CAT") {
                        fileName = "CV Catalog PDF Page - " + ids[2];
                    } else if (ids[0].startsWith('F-')) {
                        fileName = "Packaging PDF Page - " + ids[2];
                    }
                    THIS.compareKvp[$(obj).attr('data-solrid')].catalogdesc1 = fileName;
                    if (!!THIS.compareKvp[$(obj).attr('data-solrid')].filepath) {
                        THIS.compareKvp[$(obj).attr('data-solrid')].filepath = THIS.compareKvp[$(obj).attr('data-solrid')].filepath.replace('.pdf', '.png');
                    }
                }
                $(obj).toggleClass("addedToCompare");
                $(obj).find(".text-mouseover").html($(obj).find(".text-mouseover").text() == 'Add to Compare' ? 'Added to Compare' : 'Add to Compare');
                isAdd = true;
            } else {
                return;
            }

            if (Object.keys(THIS.compareKvp).length > 0) {
                $(".compare-cart-wrapper .compare-items .compare-item").each(function(index) {
                    var key = Object.keys(THIS.compareKvp)[index];

                    var $compareElement = $(this);
                    if (isAdd) {
                        var str = (THIS.compareKvp[key] && !!THIS.compareKvp[key].catalogdesc1) ? THIS.compareKvp[key].catalogdesc1 : "Add another Item";
                        if ($compareElement.hasClass("empty_item")) {
                            $compareElement.removeClass("empty_item");
                            $compareElement.find(" .description").text(str);
                            return false;
                        }
                    } else {
                        //var str = (THIS.compareKvp[key] && !!THIS.compareKvp[key].catalogdesc1) ? THIS.compareKvp[key].catalogdesc1 : "_";
                        if ($compareElement.find(" .description").text() == title) {
                            $compareElement.addClass("empty_item");
                            $compareElement.find(" .description").text("Add another Item");
                            return false;
                        }
                    }

                });
                if ($(".compare-cart-wrapper .compare-items .compare-item.empty_item").length == 0) {
                    $(".compare-controls .compare-button").removeClass("disabled");
                } else {
                    $(".compare-controls .compare-button").addClass("disabled");
                }
                $("#compareHolder").removeClass("hide");
            }
            if (!isAdd) {
                delete THIS.compareKvp[$(obj).attr('data-solrid')];
            }
        } else {
            $("#compareHolder").addClass("hide");
        }
        return;
    };

    this.goToCompare = function() {
        THIS.compareColl = Object.keys(THIS.compareKvp);
        $("#itemOneDrawing").attr("src", THIS.qfindURL + THIS.compareKvp[THIS.compareColl[0]].filepath);
        //$("#itemOneDrawing").attr("data-zoom-image",THIS.compareColl[0].drawingZoomUrl);
        $("#itemTwoDrawing").attr("src", THIS.qfindURL + THIS.compareKvp[THIS.compareColl[1]].filepath);
        //$("#itemTwoDrawing").attr("data-zoom-image",THIS.compareColl[1].drawingZoomUrl);

        $("#itemOneTitle").text(THIS.compareKvp[THIS.compareColl[0]].catalogdesc1);
        $("#itemTwoTitle").text(THIS.compareKvp[THIS.compareColl[1]].catalogdesc1);

        $("#itemOneDocNum").text(THIS.compareKvp[THIS.compareColl[0]].catalognumber1);
        $("#itemTwoDocNum").text(THIS.compareKvp[THIS.compareColl[1]].catalognumber1);

        $("#itemOneCompany").text(THIS.compareKvp[THIS.compareColl[0]].itemtype);
        $("#itemTwoCompany").text(THIS.compareKvp[THIS.compareColl[1]].itemtype);

        $("#itemOneCategory").text(THIS.compareKvp[THIS.compareColl[0]].coo_desc);
        $("#itemTwoCategory").text(THIS.compareKvp[THIS.compareColl[1]].coo_desc);

        $("#itemOneSubCategory").text(THIS.compareKvp[THIS.compareColl[0]].itemstatus);
        $("#itemTwoSubCategory").text(THIS.compareKvp[THIS.compareColl[1]].itemstatus);

        $("#itemOneMaterialOrComponents").text(THIS.compareKvp[THIS.compareColl[0]].manufacturer);
        $("#itemTwoMaterialOrComponents").text(THIS.compareKvp[THIS.compareColl[1]].manufacturer);
        $(".zoomContainer").addClass("hide");
        $('.detailImg').remove();
        setTimeout(function() { $("#itemOneDrawing,#itemTwoDrawing").elevateZoom({ zoomType: "lens", minZoomLevel: 0.5, lensShape: "round", lensSize: 200, scrollZoom: true }); }, 500);
    };
    $('#myCompareModel').on('hidden.bs.modal', function() {
        $('.zoomContainer').remove();
        $('.detailImg').remove();
        $(".elevateZoom-window").elevateZoom({
            zoomWindowPosition: 2,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            tint: true,
            tintColour: 'red',
            tintOpacity: 0.3,
            minZoomLevel: 0.3
        });
        $(".elevateZoom-lens").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lensSize: 150,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            minZoomLevel: 0.05,
            /*tint : true,
            tintColour : 'red',
            tintOpacity : 0.3,
            minZoomLevel:0.3*/
        });
    });
    this.compareClose = function() {
        $('#compareHolder').addClass('hide');
        $("[id^=compare].addedToCompare").find(".text-mouseover").html("Add to Compare");
        $("[id^=compare].addedToCompare").removeClass("addedToCompare");
        THIS.compareColl = new Array();
        THIS.compareKvp = {};
        $(".compare-cart-wrapper .compare-items .compare-item").each(function(index) {
            var $compareElement = $(this);
            $compareElement.addClass("empty_item");
            $compareElement.find(" .description").text("Add another Item");
        });
        $(".compare-controls .compare-button").addClass("disabled");
    };
    this.bookmark = function() {
        var bookMarkName = $.trim($("#bookMarkName").val());
        if (bookMarkName == "")
            return;
        if (THIS.key == "") {
            showNotification("error", "No results to bookmark");
            return;
        };
        $("#savebookmark").addClass("disabled");
        var request = {};
        request.key = JSON.stringify(THIS.request);
        request.displayQuery = THIS.key;
        request.bookmarkname = bookMarkName;
        enableLoading();
        callAjaxService(searchUrls["bookmarksearch"], callBackBookmarkSearchResult,
            callBackFailure, request, "POST");
    };
    this.getDefaultBookmark = function() {
        $("#bookMarkName").val(THIS.key);
        $("#bookMarkValue").val(THIS.key);
    };
    var callBackBookmarkSearchResult = function(response) {
        $($("#searchBookmark").parent()).removeClass("open");
        $("#savebookmark").removeClass("disabled");
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        } else {
            showNotification("success", response.message);
        };
    };

    this.shortlist = function(obj) {
        var i = $(obj).data("sequence");
        var SolrId = $(obj).toggleClass("shortlisted").data("solrid");
        $(obj).find(".text-mouseover").html($(obj).find(".text-mouseover").text() == 'Shortlisted' ? 'Shortlist' : 'Shortlisted');
        var request = {
            "solrid": removeHTMLTags(SolrId)
        };
        callAjaxService(searchUrls['shortlistDocument'],
            shortlistCallback, callBackFailure, request,
            "POST");
    };

    var shortlistCallback = function(response) {
        //THIS.ShortlistResponse = response;
        THIS.ShortlistItems = new Array();
        for (var i = 0; i < response.length; i++) {
            THIS.ShortlistItems.push(response[i].refid);
        }
        //THIS.getShortlisted();
    };

    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
            // facetFreeFormSearch('All');
            if (!!!THIS.configData || THIS.configData.length === 0 || $("#freeFormTabContent").length === 0 || !($("#freeFormTabContent").is(':visible')) || parseInt(recordsCounts[THIS.facet]) < 10 || parseInt(recordsCounts[THIS.facet]) <= currentRow || THIS.mode === 'shortlist') {
                return;
            }
            currentRow += 10;
            facetFreeFormSearch(THIS.facet, currentRow, 10, true);
        }
    });

    var callBackFreeFormSearchResult = function(response) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        var searchNav = $("<ul/>").attr("id", "searchTab").addClass("nav nav-tabs bordered");
        searchNav.append($("<li/>").addClass("active").append($("<a>").attr("data-toggle", "tab").html("All")).click(function() {
            currentRow = 0;
            facetFreeFormSearch("All");
        }));
        var searchHits = 0;
        recordsCounts = JSON.parse(JSON.stringify(response)); //Deep Copy
        $.each(response, function(key, data) {
            if (key && data > 0) {
                var keyDisplayName = lookUpTable.get(key);
                if (keyDisplayName === undefined || keyDisplayName === null || keyDisplayName === "")
                    keyDisplayName = key;
                searchNav.append($("<li/>").append($("<a>").attr("data-toggle", "tab").html(keyDisplayName).append($("<span/>").addClass("badge bg-color-greenLight txt-color-white").html(data))).click(function() {
                    currentRow = 0;
                    facetFreeFormSearch(key);
                }));
                searchHits += +data;
            }
        });
        recordsCounts['All'] = searchHits;
        $("#searchContent").append(searchNav);
        if (searchHits > 0) {
            isNearest = false;
            if (!!searchSuggestionsObject[$("#searchterm").val()]) {
                delete searchSuggestionsObject[$("#searchterm").val()];
                isNearest = !isNearest;
            }
            facetFreeFormSearch("All");
        } else {

            // $("#searchContent").append('<div class="alert alert-danger"><strong><i class="fa fa-info-circle fa-th-large"></i></strong> No Results found for ' + $('#searchterm').val() + '.</div>'); 
            if (!!searchSuggestionsObject[$("#searchterm").val()]) {
                $('#searchContent').append('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">�</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No results found for ' + $('#searchterm').val() + '</p></div>');
                return;
            }
            //var nearSearch = searchAutoSuggest($("#searchterm").val());
            //searchSuggestionsObject[$("#searchterm").val()] = nearSearch;
            if (THIS.type == null) {
                $("#searchContent").empty();
                THIS.LoadFreeFrom(configurationData, true);
            } else {
                $('#searchContent').append('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">�</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No results found for ' + $('#searchterm').val() + '</p></div>');
            }
        }
    };

    var searchAutoSuggest = function(str) {
        if (!str || str.trim() === '') {
            return;
        }
        var searchArea = ['mm'];
        var s = '',
            sConstruct = '';
        for (var i = 0; i < searchArea.length; i += 1) {
            if (str.toLowerCase().indexOf(searchArea[i]) !== -1) {
                var index = (str.toLowerCase().indexOf(searchArea[i]) - 1);
                var beginIndex = 0;
                for (var j = index; j > 0; j--) {
                    if (str[j] === ' ') {
                        break;
                    }
                    if (!isNaN(str[j])) {
                        s += '' + str[j];
                        beginIndex = j;
                    }
                }
                if (beginIndex > 0 && !isNaN(s)) {
                    sConstruct = str.substr(0, beginIndex);
                    s = parseInt(s.split('').reverse().join(''));
                    //console.log(s);
                    //sConstruct = sConstruct.replace(' ', ' AND ');
                    sConstruct += '[0' + (s - 3) + searchArea[i] + ' TO ' + (s + 3) + searchArea[i] + ']';
                }
                break;
            }
        }
        return sConstruct;
    }

    $('#myModalDrilldown').on('hidden.bs.modal', function() {
        $('.zoomContainer').remove();
        $('.detailImg').remove();
        $(".elevateZoom-window").elevateZoom({
            zoomWindowPosition: 2,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            tint: true,
            tintColour: 'red',
            tintOpacity: 0.3,
            minZoomLevel: 0.3
        });
        $(".elevateZoom-lens").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lensSize: 150,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            minZoomLevel: 0.05,
            /*tint : true,
            tintColour : 'red',
            tintOpacity : 0.3,
            minZoomLevel:0.3*/
        });
    });

    this.LoadFreeFrom = function(configData, nearSearch) {
        $(".zoomContainer").remove();
        $("#searchDivId").removeClass().addClass("search-active");
        configurationData = (!!!THIS.configData) ? configurationData : THIS.configData;
        configData = (!!!configData) ? configurationData : configData;
        THIS.configData = configData ? configData : THIS.configData;
        currentRow = 0;
        $("#entitySelectMsg").addClass("hide");
        $("#entities").empty();
        $("#resultGrid div[id^=faq]").remove();
        enableLoading();
        THIS.key = $("#searchterm").val();
        $.ajaxQ.abortAll();
        $("[id*=count]").html(0);

        $(".facets-tabs").show();
        $(".searchkey").empty();
        $(".searchkey").append(THIS.key);
        $(".totalSearchCount").html('');
        $("#mainDivId").addClass("hide");
        $("#wid-id-data").removeClass("hide");
        $('#' + currentFacet + 'Card').fadeOut('slow');
        $('#' + currentFacet + 'Content').fadeOut('slow');
        var searchInDisplaycolumns = $("[name=checkbox-inline]").is(':checked');
        var request = {
            "key": THIS.key,
            //"source": THIS.source,
            "solrcollectionname": configData[0].database,
            "searchInDisplaycolumns": searchInDisplaycolumns
        };
        if (nearSearch) {
            request.type = "similar";
            THIS.type = "similar";
        } else {
            THIS.type = null;
        }
        callAjaxService(searchUrls["search"], callBackFreeFormSearchResult, callBackFailure, request, "POST");
        $("#searchContent").empty();
    };

    this.Load = function(configData) {
    	//Get BOM Ids
    	if(!(bomIds && bomIds.length > 0) && localStorage.getItem("lastusedapp") === "ROLE_QFIND") {
    		callAjaxService("getBOMIds", function(response){
    			//console.log(response);
    			if (response && response.isException) {
    	            showNotification("error", response.customMessage);
    	            return;
    	        }
    			bomIds = response;
    		}, callBackFailure, {}, "POST");
    	}
        configurationData = (!!!THIS.configData) ? configurationData : THIS.configData;
        if (!!!configData) {
            configData = configurationData;
        }
        if (configData && configData.length > 0 && configData[0]["configtable"] === "freeformsearch") {
            configurationData = configData;
            disableLoading();
            enableSearchSuggestion();
            $("#search,#searchAdv").click(function() { THIS.LoadFreeFrom(configData); });
            $('#searchClear').click(function() {
                $('#searchterm').val('');
                THIS.LoadFreeFrom(configData);
            });
        } else {
            $("#searchDivId").removeClass().addClass("search-active");
            THIS.configData = configData ? configData : THIS.configData;
            configurationData = THIS.configData;
            currentRow = 0;
            $("#entitySelectMsg").addClass("hide");
            $("#entities").empty();
            $("#resultGrid div[id^=faq]").remove();
            enableLoading();
            THIS.key = $("#searchterm").val();
            $.ajaxQ.abortAll();
            $("[id*=count]").html(0);

            $(".facets-tabs").show();
            $(".searchkey").empty();
            $(".searchkey").append(THIS.key);
            $(".totalSearchCount").html('');
            $("#mainDivId").addClass("hide");
            $("#wid-id-data").removeClass("hide");
            $('#' + currentFacet + 'Card').fadeOut('slow');
            $('#' + currentFacet + 'Content').fadeOut('slow');
            var searchInDisplaycolumns = $("[name=checkbox-inline]").is(':checked');
            var request = {
                "key": $('#searchterm').val(),
                //"source": THIS.source,
                "solrcollectionname": configData[0].database,
                "searchInDisplaycolumns": searchInDisplaycolumns
            };
            if (THIS.type === "similar") {
                request.type = "similar";
            }
            callAjaxService(searchUrls["search"], callBackSearchResult, callBackFailure, request, "POST");
            $("#search,#searchAdv").click(function() { THIS.Load(); });
            $('#searchClear').click(function() {
                $('#searchterm').val('');
                THIS.Load();
            });
        }
    };

    var callBackSearchResult = function(response) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        $("#entitySelectMsg,#mainDivId").removeClass("hide");
        $("#wid-id-20, #resultGrid").show();
        $('#resultCfg').html('<div class="pull-right"><a class="btn btn-default" id="searchChangeCard"><i class="fa fa-th-large fa-lg"></i>Card</a><a class="btn btn-default" id="searchChangeContent"><i class="fa fa-list-alt fa-lg"></i>Content</a><a id="searchChangeGrid" class="btn btn-default"><i class="fa fa-list-ul fa-lg"></i>Grid</a></div>');
        $('resultData').html('');
        $.each(response, function(key, data) {
            var action = window.location.hash.replace(/^#/, '');
            if (key.toLowerCase() === 'event_details') {
                lookUpTable.put(action + '_' + key.toLowerCase(), 'getEventDetails');
            }
            var keyDisplayName = lookUpTable.get(key);
            if (keyDisplayName === undefined || keyDisplayName === null || keyDisplayName === "")
                keyDisplayName = key;
            var liEntity = $("<li/>").attr({ id: "li" + key }).data({ 'facet': key }).unbind('click').bind('click', function() { THIS.entitySearch(this); }).appendTo("#entities");
            liEntity.append($("<a/>").attr({ "href": "#faq-" + key, "data-toggle": "tab" })
                .append($("<span/>").attr({ "id": "loading-" + key })).append(keyDisplayName + " (<span class='" + key + "count' >" + data + "</span>)"));

            var tabPaneDiv = $("<div/>").addClass("tab-pane in fade").attr({ "id": "faq-" + key }).appendTo("#resultGrid");

            tabPaneDiv.append($("<div/>").addClass("panel-group").attr({ "id": "accordion" + key })
                .append($("<div/>").addClass("panel panel-default panel-faq")
                    .append($("<div/>").addClass("panel-body")
                        .append($("<div/>").addClass("teaser")
                            .append($("<span/>").attr({ "id": "tittle" }).html("<span id='statusMsg'>Loading </span> Results For" + "<span class='searchkey'>" + THIS.key + "</span> from <span class='totalSearchCount'>" + keyDisplayName + "</span>"))
                        )
                        //.append('<div class="pull-right"><a class="btn btn-default" id="searchChangeCard"><i class="fa fa-th-large fa-lg"></i>Card</a><a class="btn btn-default" id="searchChangeContent"><i class="fa fa-list-alt fa-lg"></i>Content</a><a id="searchChangeGrid" class="btn btn-default"><i class="fa fa-list-ul fa-lg"></i>Grid</a></div>')
                        .append($("<div/>").attr({ 'id': key + '_Details' }))
                        .append($("<table/>").attr({ "id": key + "Table" }).addClass("display  table table-hover table-striped").append($("<thead/>").append($("<tr/>"))))
                        // .append(('<div class="row" id="' + key + 'Card"></div>'))
                        // .append(('<div class="row" id="' + key + 'Content"></div>'))
                    )
                ));
            $('#resultData').append(('<div class="row" id="' + key + 'Card"></div>'));
            $('#resultData').append(('<div class="row" id="' + key + 'Content"></div>'));
        });
        $('#searchChangeCard').bind('click', function() { changeView('card'); });
        $('#searchChangeContent').bind('click', function() { changeView('content'); });
        $('#searchChangeGrid').bind('click', function() { changeView('grid'); });
        $('#searchChangeGrid').hide();
    };
    this.entitySearch = function(obj) {
        $("#statusMsg").html("Loading");
        $("#entitySelectMsg").addClass("hide");
        THIS.datasource = $("#myTab").find("li.active").data("source");
        THIS.facet = $(obj).data("facet");
        var count = $("." + THIS.facet + "count").html();
        var searchInDisplaycolumns = false;
        currentRow = 0;
        searchInDisplaycolumns = $("[name=checkbox-inline]").is(':checked');
        if (count != "0") {
            enableLoading();
            var request = {
                "key": THIS.key,
                "facet": THIS.facet,
                //"source": THIS.source,
                "solrcollectionname": THIS.configData[0].database,
                "searchInDisplaycolumns": searchInDisplaycolumns
                    //"start": 21,
                    //"rows": 10
            };
            callAjaxService(searchUrls["searchEntity"], callBackEntitySearchResult, callBackFailure, request, "POST");
        } else {
            $("#" + THIS.facet + "Table thead tr").append("<th/>");
            $("#" + THIS.facet + "Table")
                .dataTable({
                    "data": null,
                    "destroy": true,
                    "language": {
                        "zeroRecords": "No records to display"
                    },
                    "filter": false
                });
        }
    };
    
    this.loadBOMCUSA = function(obj) {
    	searchKey = $(obj).attr('data-solrId');
    	window.location.hash = "#BOMIntegra";       
    }
    
    var changeView = function(viewType) {
        if (!viewType || viewType.trim() === '') {
            return;
        }
        if (viewType === 'card') {
            enableLoading();
            var img = ['revesion', 'primaryUOM'];
            var valMap = { catalogdesc1: 'Description', catalognumber1: 'Item No', uom_code: 'UOM Code', itemtype: 'Item Type', itemstatus: 'Status', component_sku_number: 'SKU', coo: 'Origin' };
            var front = ['catalogdesc1', 'Description'];
            var back = ['"label "', 'coodesc', 'approveddate', 'manufacturer', 'catalognumber1', 'itemtype', 'itemstatus', 'uom_code', 'component_sku_number', 'coo'];
            var str = '<div class="row"><div class="col-sm-12"><div style="margin-left : 4%">';
            for (var i = 0; i < searchData.length; i += 1) {
                var cardTemplate = '<div class="card effect__EFFECT">';
                cardTemplate += '<div class="card__front">';
                cardTemplate += '<span><a style="cursor: pointer;" onClick="openModal(\'' + searchData[i].catalognumber1 + '\' , \'' + i + '\');"><i class="fa fa-lg fa-info-circle" aria-hidden"true"=""></i></a></span>';

                for (var j = 0; j < front.length; j++) {
                    if (searchData[i][front[j]]) {
                        cardTemplate += '<span class="card__text"><strong>' + valMap[front[j]] + '</strong> : <label>' + searchData[i][front[j]] + '</label></span><br />';
                    }
                }
                //cardTemplate += '<img class="card-img" src="https://i.ytimg.com/vi/K6SmVMtMmEU/hqdefault.jpg"/>';
                cardTemplate += '<img onerror="System.imgError(this);" src="' + searchData[i].filepath + '" alt="No Preview Available" class="card-img" />';
                cardTemplate += '</div>';
                cardTemplate += '<div class="card__back">';
                cardTemplate += '<span><a style="cursor: pointer;" onClick="openModal(\'' + searchData[i].catalognumber1 + '\' , \'' + i + '\');"><i class="fa fa-lg fa-info-circle" aria-hidden"true"=""></i></a></span>';
                for (var j = 0; j < back.length; j++) {
                    if (searchData[i][back[j]] && valMap[back[j]]) {
                        cardTemplate += '<span class="card__text"><strong>' + valMap[back[j]] + '</strong> : <label>' + searchData[i][back[j]] + '</label></span><br />';
                    }
                }
                cardTemplate += '</div>'
                cardTemplate += '</div>';
                str += cardTemplate;
            }
            str += "</div></div></div>";
            $('#' + currentFacet + 'Card').html('');
            $('#' + currentFacet + 'Card').append(str);
            //$('#' + currentFacet + 'Table').fadeOut('slow');
            $('#resultGrid').fadeOut('slow');
            $('#' + currentFacet + 'Content').fadeOut('slow');
            var cards = document.querySelectorAll(".card.effect__EFFECT");
            for (var i = 0, len = cards.length; i < len; i++) {
                var card = cards[i];
                clickListener(card);
            }

            function clickListener(card) {
                card.addEventListener("click", function() {
                    var c = this.classList;
                    c.contains("flipped") === true ? c.remove("flipped") : c.add("flipped");
                });
            }
            $('#' + currentFacet + 'Card').fadeIn('slow');
            viewMode = 'Card';
            disableLoading();
        } else if (viewType === 'grid') {
            $('#' + currentFacet + 'Content').fadeOut('slow');
            $('#' + currentFacet + 'Card').fadeOut('slow');
            $('#resultGrid').fadeIn('slow');
            viewMode = 'Grid';
        } else if (viewType === 'content') {
            var valMap = { catalogdesc1: 'Description', catalognumber1: 'Item No', uom_code: 'UOM Code', itemtype: 'Item Type', itemstatus: 'Status', component_sku_number: 'SKU', coo: 'Origin' };
            var contents = ['Country', 'country', '"label "', 'coodesc', 'approveddate', 'manufacturer', 'catalognumber1', 'itemtype', 'itemstatus', 'uom_code', 'component_sku_number', 'coo'];
            var title = ['catalogdesc1'];
            var str = '<div style="margin-top: 15px;"></div><div class="row"><div class="col-sm-12">';
            for (var i = 0; i < searchData.length; i += 1) {
                var cardTemplate = '<div class="row content-view">';
                cardTemplate += '<div class="col-xs-12 col-md-12 col-lg-12">';
                cardTemplate += '<a class="thumbnail" onClick="openModal(\'' + searchData[i].catalognumber1 + '\' , \'' + i + '\');">';
                cardTemplate += '<div class="col-md-12">';
                cardTemplate += '<span class="con-img-zoom" style="position: relative; overflow: hidden;"><span>Hover on the Image to Zoom</span>';
                //cardTemplate += '<img class="zoomImg" src="http://www.jacklmoore.com/img/daisy.jpg" alt="Generic placeholder thumbnail" />';
                //cardTemplate += '<img class="zoomImg"  id="' + currentFacet + 'con-img-zoom-' + i + '" src="https://i.ytimg.com/vi/K6SmVMtMmEU/hqdefault.jpg" data-zoom-image="https://i.ytimg.com/vi/K6SmVMtMmEU/hqdefault.jpg" alt="Generic placeholder thumbnail" /></span>';
                cardTemplate += '<img onerror="System.imgError(this);" class="zoomImg" id="' + currentFacet + 'con-img-zoom-' + i + '"  src="' + searchData[i].filepath + '" data-zoom-image="' + searchData[i].filepath + '" alt="No Image to Preview" /></span>';
                // if (searchData[i].filepath) {
                //     cardTemplate += '<img class="zoomImg" src="' + searchData[i].filepath + '" alt="No Preview Available" />';
                // }
                cardTemplate += '</div>';
                cardTemplate += '<div class="col-md-12">';
                cardTemplate += '<ul>';
                for (var j = 0; j < title.length; j++) {
                    if (searchData[i][title[j]]) {
                        // cardTemplate += '<span class="card__text"></span><br />';
                        cardTemplate += '<li style="border-bottom: 1px solid #FF6347"><h4><b>' + searchData[i][title[j]] + '</b></h4></li>';
                    }
                }
                for (var j = 0; j < contents.length; j++) {
                    if (searchData[i][contents[j]] && valMap[contents[j]]) {
                        // cardTemplate += '<span class="card__text"></span><br />';
                        cardTemplate += '<li><strong>' + valMap[contents[j]] + '</strong> : <label>' + searchData[i][contents[j]] + '</label></li>';
                    }
                }
                cardTemplate += '</ul>';
                cardTemplate += '</div>';
                cardTemplate += '</a>';
                cardTemplate += '</div></div>';
                str += cardTemplate;
            }
            str += "</div></div>";
            $('#' + currentFacet + 'Content').html('');
            $('#' + currentFacet + 'Content').append(str);
            $(".zoomImg").each(function() {
                //$('#'+ this.id).zoom({ url: this.src });
                $('#' + this.id).elevateZoom({
                    scrollZoom: true,
                    zoomType: "inner",
                    cursor: "crosshair"
                });
            });
            //$('#' + currentFacet + 'Table').fadeOut('slow');
            $('#resultGrid').fadeOut('slow');
            $('#' + currentFacet + 'Content').fadeIn('slow');
            $('#' + currentFacet + 'Card').fadeOut('slow');
            viewMode = 'Content';
        }
    }

    var callBackEntitySearchResult = function(response) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
//        $('#' + currentFacet + 'Content').fadeOut('slow');
//        $('#' + currentFacet + 'Card').fadeOut('slow');


        searchData = JSON.parse(JSON.stringify(response)); //deep copy
        currentFacet = THIS.facet;
        $("#statusMsg").html("Showing");
        $('#resultOpt').show();
        filterKeys = [];
        filteredKeyDataMap = {};
        try {
//            for (var i = 0; i < response.length; i += 1) {
//                filterKeys.push(response[i].catalognumber1);
//                filteredKeyDataMap[i] = response[i];
//            }
//            changeView('content');
//            return;
            var colosFunction = [];
            var exportColumn = [];
            var coloum_config = Object.keys(response[0]);
            for (var i = 0, idx = coloum_config.length; i < idx; i++) {

                var fun = new Function('obj', 'return obj.data.' + coloum_config[i].trim());
                var colTitle = lookUpTable.get(THIS.facet + "-" + coloum_config[i].trim());
                if (colTitle == null) {
                    colTitle = coloum_config[i].trim();
                }
                var showCol = true;

                if (coloum_config[i].trim() == "key")
                    showCol = false;
                else
                    exportColumn.push(i);

                colosFunction.push({
                    "data": coloum_config[i].trim(),
                    "defaultContent": "",
                    "sortable": true,
                    "title": colTitle,
                    "visible": showCol,
                    "render": function(data, type, full) {
                        if (data != null && data != "")
                            var highlightData = highlight(data, THIS.key);
                        var temp = $("<td/>")
                            .html(highlightData);
                        return temp.html();
                    }
                });
            }
            var facetTable = $("#" + THIS.facet + "Table")
                .dataTable({
                    "language": {
                        "zeroRecords": "No records to display"
                    },
                    "dom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "filter": false,
                    "info": true,
                    "scrollX": true,
                    "autoWidth": false,
                    "scrollCollapse": true,
                    "scrollY": "400px",
                    "data": response,
                    "tableTools": {
                        "aButtons": [{
                            "sExtends": "collection",
                            "sButtonText": 'Export <span class="caret" />',
                            "aButtons": [{
                                "sExtends": "pdf",
                                "mColumns": exportColumn
                            }, {
                                "sExtends": "csv",
                                "mColumns": exportColumn
                            }]
                        }],
                        "sSwfPath": "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "rowCallback": function(nRow, aData,
                        iDisplayIndex, iDisplayIndexFull) {
                        var key = "",
                            criteria;
                        if (aData.key) {
                            key = aData.key.split("|");
                            if (key.length > 2) {
                                key.splice(0, 2);
                                criteria = key.toString();
                            } else if (key.length > 1) {
                                key.splice(0, 1);
                                criteria = key.toString();
                            }
                        }

                        var payload = "source=" + THIS.configData[0].source + "&database=" + (THIS.configData[0].config ? THIS.configData[0].config : System.dbName) + "&table=" + THIS.facet + "&criteria=" + criteria;

                        var action = window.location.hash.replace(/^#/, ''),
                            fun;
                        // if (lookUpTable.get(action + '_' + THIS.facet)) {
                        //     $('td', nRow)
                        //         .attr({
                        //             'onClick': "javascript:searchEntityDetails." + lookUpTable.get(action + '_' + THIS.facet) + "(" + aData["Event_id"] + ");"
                        //         });
                        // } else {
                        //     $(nRow).attr({
                        //         'data-target': '#myModalDrilldown',
                        //         'data-toggle': 'modal'
                        //     });
                        //     $(nRow).bind('click', function(e) {
                        //         detailDrillDown(payload, THIS.facet, criteria, THIS.facet);
                        //     });
                        // }
                        $(nRow).bind('click', function(e) {
                            detailDrillDown(payload, THIS.facet, criteria, THIS.facet);
                            //openModal(aData.key, 0);
                        });

                    },
                    "destroy": true,
                    "columns": colosFunction
                });
        } catch (err) {

        }
    };
    searchProf = THIS;
};