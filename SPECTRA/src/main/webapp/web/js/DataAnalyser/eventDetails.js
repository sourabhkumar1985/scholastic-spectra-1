function eventDetails(){
	var THIS = this;
	this.Load = function(eventid){
		$("#wid-id-formView").removeClass("hide");
		$("#dataAnalyserProfile").addClass("hide");	
		getSelect2Data($("#eventDetailsForm").find("[name='physicians']"),"Enter physician's name",true,"select * from physicians where lower(name) like '%<Q>%'");
		getSelect2Data($("#eventDetailsForm").find("[name='reps']"),'Enter Sales Representative name',true,"select * from reps where lower(name) like '%<Q>%'");
		getSelect2Data($("#eventDetailsForm").find("[name='locations']"),'Enter Location',false,"select * from locations where lower(practice) like '%<Q>%'");
		$("#actionButtons").empty();
		$("#actionButtons").parent().find(".jarviswidget-ctrls").find("[data-original-title='Back']").unbind('click').bind('click',function(){
			$("#wid-id-formView").toggleClass("hide");
			$("#searchProfile").toggleClass("hide");
			$("#dataAnalyserProfile").toggleClass("hide");
		});
		$("#eventDetailsForm").submit(function(e) {
		    e.preventDefault();
		  //$(this).serializeArray()		   
		});
		
		$("#eventDetailsForm").find("[name='createbtn']").unbind("click").bind("click",function(){
			if($("#eventDetailsForm").valid()){		    	
				saveCase();
		    }
		});
		$("#eventDetailsForm").find("[name='status']").val('Confirmed');
		$("#eventDetailsForm").find("[name='startDate'],[name='endDate']").datepicker(
				{
					defaultDate : "+1w",
					changeMonth : true,
					dateFormat: 'mm-dd-yy',
					onClose : function(selectedDate) {
						if($(this).attr("name") == 'startDate'){
							$("[name='endDate']").datepicker(
									"option", "minDate", selectedDate);
						}else{
							$("[name='startDate']").datepicker(
									"option", "maxDate", selectedDate);
						}
						
					}
				});
			$("#eventDetailsForm").validate({
				rules : {
					caseName : {
						required : true
					},
					status : {
						required : true
					},
					startDate : {
						required : true
					},
					endDate : {
						required : true
					},
					eventDetails : {
						required : true
					},
					locations : {
						required : true
					},
					physicians : {
						required : true
					},
					reps : {
						required : true
					}
					
					/*part : {
						required : true
					},
					size : {
						required : true
					},
					quantity : {
						required : true
					}*/
				},
	
				messages : {},
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element);
				}
			});
		if(eventid){
			$("#actionButtons").show().append($('<div/>').addClass('actions')
					.append($("<button/>").attr({"name":"edit"}).html("Edit").unbind('click').bind('click',function(){
						editCase();						
					}).addClass("btn btn-warning").prepend($("<i/>").addClass("fa fa-pencil margin-right-5")))
					.append($("<button/>").attr({"name":"cancelCase"}).html("Cancel Case").addClass("btn btn-sm btn-danger hide").append($("<i/>").addClass("fa fa-close margin-left-5")))
					.append($("<button/>").attr({"name":"previous"}).html("Previous").addClass("btn btn-sm btn-primary btn-prev").prepend($("<i/>").addClass("fa fa-arrow-left margin-right-5")))
					.append($("<button/>").attr({"name":"next"}).html("Next").addClass("btn btn-sm btn-success btn-next").append($("<i/>").addClass("fa fa-arrow-right margin-left-5"))));
			THIS.eventID = eventid;
			$("[name='editCaseForm']").hide();
			$("[name='viewCaseForm']").show();
			getEventById(eventid);
		}else{	
			addImplantRow(1);
			$("#eventDetailsForm").find("[name='cancelbtn']").unbind("click").bind("click",function(){
				$("#wid-id-formView").toggleClass("hide");
				$("#searchProfile").toggleClass("hide");
				$("#dataAnalyserProfile").toggleClass("hide");
			});
			$("[name='editCaseForm']").show();
			$("[name='viewCaseForm']").hide();
		}
		
	};
	function closeCase(){
		$.confirm({
					text : "This case will be permanently cancelled. No further action (ship/ edit) will be possible for this case. Are you sure you want to cancel ?",
					title:'Cancel Case',
					confirmButton:"Cancel case",
					confirmButtonClass:"btn-danger",
					cancelButton: "Return",
					confirm : function() {
						var request = {
							caseid:	THIS.eventObj.eventId,
							status:"Cancelled",
							lastupdatedby:System.userName
						};
						enableLoading();
						callAjaxService("updatecasestatus", callBackSuccessUpdatecaseStatus,callBackFailure,request,"POST");						
					},
					cancel : function() {
						// nothing to do
					}
				});
	
	}
	function callBackSuccessUpdatecaseStatus(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response){
			if(dataAnalyser.profiling.grapher.refreshTable){
				dataAnalyser.profiling.grapher.refreshTable();
			}		
			if(response.isUpdated){
				showNotification("success","Case "+THIS.eventID+" has been closed");
			}
			$("#wid-id-formView").toggleClass("hide");
			$("#searchProfile").toggleClass("hide");
			$("#dataAnalyserProfile").toggleClass("hide");
		}
	}
	function editCase(){
		$("#eventDetailsForm").find("[name='cancelbtn']").unbind("click").bind("click",function(){
			$("[name='editCaseForm']").hide();
			$("[name='viewCaseForm']").show();
			$("#actionButtons").show();
			$("#eventDetailsForm").find("[name='createbtn']").html("Create");
		});
		$("#eventDetailsForm").find("[name='createbtn']").html("Update");
		$("#actionButtons").hide();
		$("[name='editCaseForm']").show();
		$("[name='viewCaseForm']").hide();
	}
	function getCaseObj(){
		var startDate = $("#eventDetailsForm").find("[name='startDate']").val();
		var endDate = $("#eventDetailsForm").find("[name='endDate']").val();
		var caseJson = {
			startDate: [startDate.split('-')[2],startDate.split('-')[0],startDate.split('-')[1]].join("-"),
			endDate: [endDate.split('-')[2],endDate.split('-')[0],endDate.split('-')[1]].join("-"),
			caseName: $("#eventDetailsForm").find("[name='caseName']").val(),
			status: $("#eventDetailsForm").find("[name='status']").val(),
			eventDetails: $("#eventDetailsForm").find("[name='eventDetails']").val(),
			attachments:[],
			locations:{
				id:$("#eventDetailsForm").find("[name='locations']").select2("data").id,
				practice:$("#eventDetailsForm").find("[name='locations']").select2("data").name
			},
			reps:$("#eventDetailsForm").find("[name='reps']").select2("data"),
			physicians:$("#eventDetailsForm").find("[name='physicians']").select2("data"),
			patients:[],
			equipment:{
				supplemental:{
					orientation:$("#eventDetailsForm").find("[name='orientation']").val()
				},
				implants:[]
			}
		};
		var implants = $("#eventDetailsForm").find("[name='implant']").find("tbody").find("tr");
		if(implants.length > 0){
			for(var i=0;i<implants.length;i++){
				var implantObj = {};
				if($(implants[i]).find("[name='part']").select2('data') && $(implants[i]).find("[name='part']").select2('data').id){
					implantObj = {
							part:$(implants[i]).find("[name='part']").select2('data').id,
							desc:$(implants[i]).find("[name='part']").select2('data').name,
							size:$(implants[i]).find("[name='size']").val(),
							quantity:parseInt($(implants[i]).find("[name='quantity']").val())
						};
					if($(implants[i]).find("[name='actualSize']").length>0 && $(implants[i]).find("[name='actualSize']").val()){
						implantObj.actualSize = $(implants[i]).find("[name='actualSize']").val();
					}
					caseJson.equipment.implants.push(implantObj);
				}
			}
		}
		return caseJson;
		
	}
	function saveCase(){
		var request = {},caseJson = getCaseObj();
		if(caseJson.status && caseJson.status.toLowerCase()=='confirmed' && caseJson.equipment.implants.length == 0){
			showNotification("error","Fill the data in implants and instruments section.");
			return;
		}
		if(THIS.eventObj){
			if(THIS.eventObj.source && THIS.eventObj.source == "manual" && THIS.eventObj.status.toLowerCase() != "shipped"){
				caseJson.eventId = THIS.eventObj.eventId;
				caseJson.lastUpdatedBy = System.userName;
				caseJson.source = THIS.eventObj.source;
				request.casejson  = JSON.stringify(caseJson);
				enableLoading();
				callAjaxService("updatecase", callBackSuccessSavecase,callBackFailure,request,"POST");
				
			}else{
				request.caseid =  THIS.eventObj.eventId;
				var partsjson = [];
				if(caseJson.equipment.implants && caseJson.equipment.implants.length>0){
					for(var i=0;i<caseJson.equipment.implants.length;i++){
						partsjson.push({
							part:caseJson.equipment.implants[i].part,
							actualSize:caseJson.equipment.implants[i].actualSize
						});
					}
					request.partsjson = JSON.stringify(partsjson);
					request.lastUpdatedBy = System.userName;;
				}
				enableLoading();
				callAjaxService("updateactualpartsize", callBackSuccessSavecase,callBackFailure,request,"POST");
			}
			
		}else{
			caseJson.createdBy = System.userName;
			request.newcasejson = JSON.stringify(caseJson);
			$("#eventDetailsForm").find("[name='createbtn']").addClass("disabled");
			enableLoading();
			callAjaxService("createcase", callBackSuccessSavecase,callBackFailure,request,"POST");
		}
		
	};
	
	function callBackSuccessSavecase(response){
		disableLoading();
		$("#eventDetailsForm").find("[name='createbtn']").removeClass("disabled");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response){
			if(dataAnalyser.profiling.grapher.refreshTable){
				dataAnalyser.profiling.grapher.refreshTable();
			}			
			if( response.caseid){
				showNotification("success","Case "+response.caseid+" created");
			}else if(response.isUpdated){
				showNotification("success","Case "+THIS.eventID+" updated");
			}			
			$("#wid-id-formView").toggleClass("hide");
			$("#searchProfile").toggleClass("hide");
			$("#dataAnalyserProfile").toggleClass("hide");
		}
	}
	function getSelect2Data(ele,placeholder,selectType,query){
		function formatResult(resp) {
			return resp.id + (resp.name?' - ' + resp.name:'');
		}
		function formatSelection(resp) {
			return resp.id + (resp.name?' - ' + resp.name:'');
		}
		$(ele).select2({
			placeholder : placeholder,
			minimumInputLength : 1,
			allowClear: true,
			multiple : selectType,
			width: 'resolve',
			ajax : {
				url : serviceCallMethodNames["suggestionsLookUp"].url,
				dataType : 'json',
				type : 'GET',
				data : function(term) {
					return {
						q : term,
						source : 'postgres',
						database : 'PSSC',
						lookupQuery : query
					};
				},
				results : function(data) {
					return {
						results : data
					};
				}
			},
			initSelection : function(element, callback) {
				callback($.map(element.val().split(','),
						function(id) {
							id = id.split(":");
							return {
								id : id[0],
								name : id[1]
							};
						}));
			},
			formatResult : formatResult,
			formatSelection : formatSelection,
			dropdownCsclass : "bigdrop",
			escapeMarkup : function(m) {
				return m;
			}
		});
	};
	function addImplantRow(rowNo,data,isActual){
		$("#eventDetailsForm").find("[name='implant']").find('.text-success').remove();
		$("#eventDetailsForm").find("[name='implant']").append($("<tr/>")
				.append($("<td/>").append($("<input/>").attr({"name":"part"})))
				.append($("<td/>").append($("<div/>").attr({"name":"description"}).html(data && data.desc ?data.desc:'')))				
				.append($("<td/>").append($("<input/>").attr({"name":"size"}).val(data&&data.size?data.size:'')))
				.append($("<td/>").append($("<input/>").attr({"name":"quantity","type":"number","min":"0"}).val(data && data.quantity ?data.quantity:'')))
				.append(isActual?$("<td/>").append($("<input/>").attr({"name":"actualSize"}).val(data && data.actualSize?data.actualSize:'')):'')
				.append(!isActual?$("<td/>").append($("<a/>").attr({"title":"Remove Row"}).unbind('click').bind('click',function(){					
					removeImplantRow(this);
				}).addClass('text-danger cursor-pointer').append($("<i>").addClass("fa fa-2x fa-trash")))
						.append($("<a/>").attr({"title":"Add Row","name":"addrow"}).unbind('click').bind('click',function(){
							addImplantRow(rowNo +1);
						}).addClass("text-success pull-right cursor-pointer").append($("<i>").addClass("fa fa-2x fa-plus-circle"))):''));
		getSelect2Data($("#eventDetailsForm").find("[name='implant']").find("tr:eq("+(rowNo)+")").find("[name='part']"),'Enter Part',false,"select * from implants where lower(description) like '%<Q>%'");
		$("#eventDetailsForm").find("[name='implant']").find("tr:eq("+(rowNo)+")").find("[name='part']").on('change',function(){			
			$(this).parents("tr").find("[name='description']").html($(this).select2('data').name);
		});
		if(data && data.part && data.desc){
			var part = {id:data.part,name:data.desc};
			$("#eventDetailsForm").find("[name='implant']").find("tr:eq("+(rowNo)+")").find("[name='part']").select2("data",part,true);
		}
	}
	function removeImplantRow(obj){
		if($(obj).parents("table").find('tr').length > 2){
			$(obj).parents("tr").remove();	
		}
		if($("#eventDetailsForm").find("[name='implant']").find("[name='addrow']").length == 0){
			$("#eventDetailsForm").find("[name='implant']").find('tr:last').find("td:last").append($("<a/>").attr({"title":"Add Row","name":"addrow"}).unbind('click').bind('click',function(){
				addImplantRow(2);
			}).addClass("text-success pull-right cursor-pointer").append($("<i>").addClass("fa fa-2x fa-plus-circle")));
		}
	}
	function fillDataInEventForm(response){
		var isActualSize = true;
		if(response.source && response.source == "manual" && response.status.toLowerCase() != "shipped"){
			isActualSize = false;		
		}
		if(response.status.toLowerCase() == "confirmed" || response.status.toLowerCase() == "pending"){
			$("#actionButtons").find("[name='cancelCase']").removeClass("hide").unbind('click').bind('click',function(){
				closeCase();
			});
		}else{
			$("#actionButtons").find("[name='cancelCase']").addClass("hide").unbind('click');
		}
		if(response.status.toLowerCase() == "cancelled"){
			$("#actionButtons").find("[name='edit']").addClass("hide");
		}else{
			$("#actionButtons").find("[name='edit']").removeClass("hide");
		}
		$.each( response, function( key, value ) {
			$("#eventDetailsForm").find("[name='"+key+"']").val(value);
		});
		if(response.startDate){
			var startDate = new Date(response.startDate);
			$("#eventDetailsForm").find("[name='startDate']").val((startDate.getMonth() + 1) + '-' + startDate.getDate() + '-' +  startDate.getFullYear());
		}
		if(response.endDate){
			var endDate = new Date(response.endDate);
			$("#eventDetailsForm").find("[name='endDate']").val((endDate.getMonth() + 1) + '-' + endDate.getDate() + '-' +  endDate.getFullYear());
		}
		if(response.physicians && response.physicians.length > 0){
			$("#eventDetailsForm").find("[name='physicians']").select2('data',response.physicians , true);	
		}		
		if(response.locations){
			var location = {
					id:response.locations.id,
					name:response.locations.practice
					};
			$("#eventDetailsForm").find("[name='locations']").select2('data',location , true);
		}
		if(response.reps && response.reps.length > 0){
			$("#eventDetailsForm").find("[name='reps']").select2('data',response.reps , true);	
		}		
		$("#eventDetailsForm").find("[name='implant']").find("tbody").empty();
		if(response.equipment){
			if(response.equipment.implants && response.equipment.implants.length>0){
				if(isActualSize){
					$("#eventDetailsForm").find("[name='implant']").empty().append($("<thead/>").append($("<tr/>")
							.append($("<th/>").html("Part").css("width","30%"))
							.append($("<th/>").html("Description").css("width","33%"))
							.append($("<th/>").html("Size").css("width","5%"))
							.append($("<th/>").html("Quantity").css("width","5%"))
							.append($("<th/>").html("Actual Size").css("width","20%"))));
				}else{
					$("#eventDetailsForm").find("[name='implant']").empty().append($("<thead/>").append($("<tr/>")
							.append($("<th/>").html("Part").css("width","30%"))
							.append($("<th/>").html("Description").css("width","53%"))
							.append($("<th/>").html("Size").css("width","5%"))
							.append($("<th/>").html("Quantity").css("width","5%"))
							.append($("<th/>").css("width","7%"))));
				}
				for(var i=0;i<response.equipment.implants.length;i++){
					addImplantRow(i+1,response.equipment.implants[i],isActualSize);
				}
				
			}else{
				addImplantRow(1);
			}
			if(response.equipment.supplemental && response.equipment.supplemental.orientation){
				$("#eventDetailsForm").find("[name='orientation']").val(response.equipment.supplemental.orientation);
			}
		}
		if(response.source && response.source == "manual" && response.status.toLowerCase() != "shipped"){
			$("#eventDetailsForm").find("input,select,textarea,a").removeAttr('disabled').css("background","");			
		}else{
			$("#eventDetailsForm").find("input,select,textarea,a").attr('disabled','disabled').css("background","#ddd");
			$("#eventDetailsForm").find("[name='actualSize']").removeAttr('disabled').css("background","");
		}
	}
	function getEventById(eventid){
		$("#actionButtons").find("[name='previous'],[name='next']").addClass("disabled");
		var request = {
				"eventid":eventid
		};
		enableLoading();
		callAjaxService("getEventByID", callBackSuccessGetEventByID,callBackFailure,request,"POST",null,true);
	}
	function callBackSuccessGetEventByID(response,mainDivId){
		if(!mainDivId){
			disableLoading();
			mainDivId = 'tab-r1';
			if(response && response.length >0){
				response = response[0];
				THIS.eventID = response.eventId;
				THIS.eventObj = response;
			}
			$("#workflow").css("padding-left","0px !important");
			$("#actionButtons").parent().find("h2").html("Case Details : "+response.eventId+' - '+response.caseName);
			 $("#tablecaseauditlogs").find("tbody").empty();
			callAjaxService("getAllWorkFlowStatus", callBackSuccessGetAllWorkFlowStatus,callBackFailure,null,"GET",null,true);
			fillDataInEventForm(response);			
			if(response.caseAudit && response.caseAudit.length>0){
				$("#tab-r3").find("[name='historyDiv']").show();
				$("#tab-r3").find("[name='emptyHistoryDiv']").hide();
				callBackSuccesGetcaseauditlogs(response.caseAudit);
			}else{
				$("#tab-r3").find("[name='historyDiv']").hide();
				$("#tab-r3").find("[name='emptyHistoryDiv']").show();
			}
			if(response.nextCaseId){
				$("#actionButtons").find("[name='next']").data("id",response.nextCaseId).unbind('click').bind('click',function(){					
					getEventById($(this).data("id"));
				}).removeClass('disabled');
			}else{
				$("#actionButtons").find("[name='next']").unbind('click').addClass('disabled');
			}
			if(response.prevCaseId){
				$("#actionButtons").find("[name='previous']").data("id",response.prevCaseId).unbind('click').bind('click',function(){					
					getEventById($(this).data("id"));
				}).removeClass('disabled');
			}else{
				$("#actionButtons").find("[name='previous']").unbind('click').addClass('disabled');
			}
		}
		$('#'+mainDivId).find('[name="eventId"]').html(response["eventId"]);
		$('#'+mainDivId).find('[name="eventCase"]').html(response["caseName"]);
		$('#'+mainDivId).find('[name="eventDetails"]').html(response["eventDetails"]);
		//$('#poDocView').find('[name="eventPhysiciansName"]').html(response["physicians"][0]["name"]);
		$('#'+mainDivId).find('[name="eventStart"]').html(response["startDate"]);
		$('#'+mainDivId).find('[name="eventEnd"]').html(response["endDate"]);
		$('#'+mainDivId).find('[name="orientation"]').html(response.equipment.supplemental.orientation);
		if (response["locations"] && response["locations"]["id"] && response["locations"]["id"]!= null){
			$('#'+mainDivId).find('[name="locationId"]').html(response["locations"]["id"]);
			$('#'+mainDivId).find('[name="locationAddress"]').html(response["locations"]["address"]);
			$('#'+mainDivId).find('[name="locationPractice"]').html(response["locations"]["practice"]);
		}
		if (response["physicians"] && response["physicians"].length > 0){
			$("#"+mainDivId +" #physicians").find("tbody").empty();
			for(var i=0;i<response["physicians"].length;i++){
				$("#"+mainDivId +" #physicians").find("tbody").append($("<tr/>")
						.append($("<td/>").html("Name"))
						.append($("<td/>").html(response["physicians"][i].name)));
			}
		}
		if (response["reps"] && response["reps"].length > 0){
			$("#"+mainDivId +" #reps").find("tbody").empty();
			for(var i=0;i<response["reps"].length;i++){
				$("#"+mainDivId +" #reps").find("tbody").append($("<tr/>")
						.append($("<td/>").html("Name"))
						.append($("<td/>").html(response["reps"][i].name)));
			}
		}
		if (response["patients"] && response["patients"].length > 0){
			$('#'+mainDivId).find('[name="patientId"]').html(response["patients"][0]["id"]);
			$('#'+mainDivId).find('[name="patientGender"]').html(response["patients"][0]["gender"]);
			$('#'+mainDivId).find('[name="patientAge"]').html(response["patients"][0]["age"]);
			$('#'+mainDivId).find('[name="patientHeight"]').html(response["patients"][0]["height"]);
			$('#'+mainDivId).find('[name="patientWeight"]').html(response["patients"][0]["weight"]);
		}
		if (response["equipment"] && response["equipment"] != null){
			if (response["equipment"]["implants"] && response["equipment"]["implants"] != null && response["equipment"]["implants"].length >  0){
				$("#"+mainDivId +" #implantAndInstruments").find("tbody").empty().append($('<tr/>')
						.append($('<th/>').css({"width":"20%"}).html("Part"))
						.append($('<th/>').css({"width":"40%"}).html("Description"))
						.append($('<th/>').css({"width":"15%"}).html("Size"))
						.append($('<th/>').css({"width":"10%"}).html("Quantity"))
						.append($('<th/>').css({"width":"15%"}).html("Actual Size")));				
				for(var i=0;i<response["equipment"]["implants"].length;i++){
					$("#"+mainDivId +" #implantAndInstruments").find("tbody").append($("<tr/>")
							.append($("<td/>").html(response["equipment"]["implants"][i].part || response["equipment"]["implants"][i].partNumber))
							.append($("<td/>").html(response["equipment"]["implants"][i].desc))
							.append($("<td/>").html(response.equipment.implants[i].size))
							.append($("<td/>").html(response.equipment.implants[i].quantity))
							.append($("<td/>").html(response.equipment.implants[i].actualSize)));
				}
			}
		}
	};
	var callBackSuccessGetAllWorkFlowStatus = function(response){	
		var steps = $("<ul/>").addClass("steps");
		for (var i=0; i< response.length ;i++){
			var workFlowli = $("<li/>").addClass((response[i].currentState.toLowerCase() == THIS.eventObj.status.toLowerCase())?'active':'');
			workFlowli.append($("<span/>").addClass("badge badge-info").html(i+1));
			workFlowli.append($("<span/>").html(response[i].currentState));
			//if (i != (response.length-1))
				workFlowli.append($("<span/>").addClass("chevron"));
			steps.append(workFlowli);
		};
		var wizardDiv = $("<div/>").addClass("wizard").append(steps);
		if(THIS.eventObj.status.toLowerCase() == "cancelled"){
			wizardDiv.append($("<div/>").addClass("col-md-2 col-md-offset-2").append($("<h1>").append($("<span/>").addClass("label red-bg").html("CANCELLED"))));
		}
		var workFlowDiv = $("<div/>").addClass("fuelux").append(wizardDiv);
		$("#workflow").empty().append(workFlowDiv);	
		var request = {
				"entityid":THIS.eventID
		};
		//callAjaxService("getWorkFlowStatus", callBackSuccessGetWorkFlowStatus,callBackFailure,request,"POST",null,true);
	};
	/*var callBackSuccessGetWorkFlowStatus = function(response){
		$(".wizard").find("li:contains('"+response["state"]+"')").addClass("active");
		var request = {
				"entityid":THIS.eventID,
				"role":"admin"
		};
		//callAjaxService("getActionButton", callBackSuccessGetActionButton,callBackFailure,request,"POST",null,true);
	};*/
	/*var callBackSuccessGetActionButton = function(response){
		$("#actionButtons").empty();
		for(var i=0; i< response.length;i++){
			$("#actionButtons").append($("<button/>").addClass("btn btn-primary btn-sm").html(response[i].action).data("entity-id",THIS.eventID).unbind("click").bind("click",function(){
				procedeAction(this);
			}));
		}
		$("#actionButtons").append($("<button/>").addClass("btn btn-danger btn-sm").html("Cancel").unbind("click").bind("click",function(){
			$("#wid-id-formView").toggleClass("hide");
			$("#searchProfile").toggleClass("hide");
			$("#dataAnalyserProfile").toggleClass("hide");
		}));
		disableLoading();
	};*/
	function callBackSuccesGetcaseauditlogs(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		$("#tablecaseauditlogs").find("tbody").empty();
		if(response && response.length>0){
			$('#tab-r3').find('[name="historyDiv"]').show();
			$("#demo-pill-nav").find("li:eq(2)").find("span").html(response.length);
			response.sort(function(a, b){
				return (new Date(a.updatedDate) < new Date(b.updatedDate))?1:(new Date(a.updatedDate) > new Date(b.updatedDate))?-1:0;
			});
			for(var i=0;i<response.length;i++){
				$("#tablecaseauditlogs").find("tbody").append($("<tr/>").data("auditId",response[i].auditId).css("cursor","pointer")
						.unbind('click').bind('click',function(e){
					getcaseAuditDetailsLogs($(this).data("auditId"),($(this).index()+1));
				}).append($("<td/>").addClass('circle-tile').html(i+1))
						.append($("<td/>").html(response[i].updatedDate))
						.append($("<td/>").html(response[i].changeLog)));
			}
			getcaseAuditDetailsLogs(response[0].auditId,1);
		}else{
			$('#tab-r3').find('[name="emptyHistoryDiv"]').show();
		}
	}
	function getcaseAuditDetailsLogs(auditid,index){
		$('#tab-r3').find('[name="generalinfo"]').html("Case Audit Logs Details : # "+index);
		callAjaxService("getcaseauditdetailslogs", callBackSuccesGetcaseauditDetails,callBackFailure,{auditid:auditid},"POST",null,true);
	}
	function callBackSuccesGetcaseauditDetails(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response){
			var caseDetails = Jsonparse(response.caseDetails);
			//$('#tab-r3').find('[name="generalinfo"]').html("Case Audit Logs Details :"+response.auditId+"-"+caseDetails.caseName);
			caseDetails.part = caseDetails.partNumber;
			callBackSuccessGetEventByID(caseDetails,'tab-r3');			
		}
	}
};
var eventDetailsObj =  new eventDetails();