(function() {
	var THIS = {};

	
	var textarea = $("#queryDataSource");
	$("#saveProfile").unbind('click').bind('click', function() {
		saveProfile();
	});
	$("#excuteQuery").unbind('click').bind('click', function() {
		excuteQuery();
	});
	$("#allProfiles").unbind("click").bind("click", function() {
		callBackGetMyProfiles();
	});
	$("#frmSaveDataSource").submit(function(e) {
		e.preventDefault();
	});
	$("#frmSaveDataSource").validate({
		ignore : [],
		rules : {
			profilename : {
				required : true
			},
			description : {
				required : true
			}
		},
		messages : {
			profilename : {
				required : 'Please enter profile name'
			},
			description : {
				required : 'Please describe profile'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	var sourceData = [ {
		"id" : "hive",
		"text" : "HIVE"
	}, {
		"id" : "postgreSQL",
		"text" : "PostgreSQL"
	} ];
	$("#database").select2({
		placeholder : "Select Database",
		data : []
	});
	$("#source").select2({
		placeholder : "Select Source",
		data : sourceData
	}).on("change", function(e) {
		sourceSelect(e.val);
	});
	THIS.queryEditor = CodeMirror.fromTextArea(textarea[0], {
		lineNumbers : true,
		mode : "text/x-sql",
		autofocus : true,
		matchBrackets : true,
		styleActiveLine : true,
		showCursorWhenSelecting : true
	});
	$(".col-sm-3", "#selectSource").unbind("click").bind("click", function() {
		widgetonClick(this);
	});
	$("#tableBack").unbind("click").bind("click", function() {
		$("#createDataSource").hide(300);
		$("#selectSource").show(300);
	});
	$("#tableBack1").unbind("click").bind("click", function() {
		$("#myprofiles").hide(300);
		$("#selectSource").show(300);
	});
	var widgetonClick = function(obj) {
		var widget = $(obj).data("source");
		if (widget === "create") {
			THIS.profile = null;
			$("#createDataSource").show(300);
			$("#selectSource").hide();
			$("#source").select2("val", "");
			$("#database").select2("val", "");
			THIS.queryEditor.setValue("");
			setTimeout(function() {
				THIS.queryEditor.refresh();
			}, 100);

		} else if (widget === "saved") {
			request = {
				"type" : "datasource"
			};
			enableLoading();
			callAjaxService("getMyProfiles", callBackGetMyProfiles,
					callBackFailure, request, "POST");
		}
	};
	var callBackGetMyProfiles = function() {
		/*disableLoading();
		$("#myprofiles").show(300);
		$("#selectSource").hide(300);
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			// response = JSON.parse(response);
			var data = response;
			// data = JSON.parse(data);
			$("#myProfileList").empty();
			for ( var i = 0; i < data.length; i++) {
				$("#myProfileList").append(
						$("<li/>").addClass("list-group-item").data({
							"refid" : data[i].id
						}).append(
								$("<a/>").append(
										$("<span/>").addClass(
												"text-left col-md-3").html(
												data[i].displayname)).append(
										$("<span/>").addClass(
												"text-left col-md-3").html(
												data[i].source)).append(
										$("<span/>").addClass(
												"text-left col-md-3").html(
												data[i].database)).append(
										$("<span/>").addClass(
												"badge text-center col-md-3")
												.html(data[i].createdon))*/
						/*
						 * .append($("<span/>").addClass("text-right
						 * col-md-1").html($("<button/>").addClass(btnClass+"
						 * btn") .attr({"href":"void(0)",'data-target' :
						 * '#profilePublishModal','data-toggle' :
						 * 'modal','data-refid':data[i].id,'data-displayname':data[i].displayname,'data-actionid':actionId,'data-action':data[i].profilekey}).html(publishStatus)))
						 */
						/*
						 * <button class="btn-outline btn margin-right-2"
						 * href="javascript:void(0);">spine</button>
						 */
						/*));
			}
			$("#myProfileList li").unbind("click").bind("click", function() {
				mydataSourceSelect(this);
			});
		}
		$("[data-target^=#profilePublishModal]").unbind("click").bind("click",
				function(e) {
					e.stopPropagation();
					previewPublishProfile(this);
				});*/
		//$('#myprofilediv').show();
		//$('#allprofilediv').hide();
	//	$('#myprofiles').show();
		$("#myprofiles").show(300);
		$("#selectSource").hide(300);
		var requiredCol = [ {
			"sWidth": '13%',
			"mDataProp" : "displayname",
			"title":"Name",
			"sDefaultContent" : ""
		},  {
			"mDataProp" : "source",
			"sWidth": '13%',
			"title":"Source",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "database",
			"sWidth": '13%',
			"title":"Data Base",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedby",
			"sWidth": '12%',
			"title":"Modified By",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedon",
			"sWidth": '13%',
			"title":"Modified On",
			"sDefaultContent" : ""
		}];
		var profileParam = {
				"getAll" : true,
				"profileType" : "datasource",
		        "columns" : requiredCol
		        
		};
		
		enableLoading();
		var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){mydataSourceSelect(e,this);});
		profileUtitlyObj.getMyProfile();
	};
	var mydataSourceSelect = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id
			
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};
	var callBackGetProfileConfig = function(response) {
		$("#createDataSource").show(300);
		$("#selectSource").hide(300);
		$("#myprofiles").hide(300);
		var data = JSON.parse(response["config"])[0];
		if (data) {
			THIS.profile = {};
			THIS.profile.id = data.id;
			THIS.profile.remarks = data.remarks;
			THIS.profile.displayname = data.displayname;
			if (data.config) {
				var config = JSON.parse(data.config);
				config.query = config.query.replace(/@/g, "'");
				THIS.queryEditor.setValue(config.query);
				setTimeout(function() {
					THIS.queryEditor.refresh();
				}, 100);
			}
			$("#source").select2("val", data.source);
			sourceSelect(data.source, data.database);
		}
	};
	var sourceSelect = function(source, selectedDatabase) {
		if (source === "hive" || source === "postgreSQL") {
			var request = {
				"source" : source
			};
			enableLoading();
			callAjaxService("getDatabaseList", function(response) {
				callBackGetDatabaseList(response, selectedDatabase);
			}, callBackFailure, request, "POST");
		}
	};
	var callBackGetDatabaseList = function(response, selectedDatabase) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var databaseList = [];
			for ( var i = 0; i < response.length; i++) {
				databaseList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				allowClear : true,
				data : databaseList
			});
			if (selectedDatabase != undefined && selectedDatabase != null
					&& selectedDatabase != "") {
				$("#database").select2("val", selectedDatabase);
			}

		}
	};

	var excuteQuery = function(){
		var request = {
			"source":$("#source").select2("val"),
			"database" : $("#database").select2("val"),
			"id" : THIS.profile.id
		};
		enableLoading();
		callAjaxService("excutequery", callBackGetExcuteQuery,callBackFailure, request, "POST");
	};
	var callBackGetExcuteQuery = function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if(response && response.length>0){
			for(var i=0;i<response.length;i++){
				if(response[i].indexOf("Excuted Succesfully")>-1){
					showNotification("success", response[i]);
				}else{
					showNotification("error", response[i]);
				}
			}
		}
		
	};
	var saveProfile = function() {
		var saveDataProfile = function() {
			var displayName, remarks;
			var query = THIS.queryEditor.getValue();
			var id = 0;
			var config = {
				query : query
			};
			callBackSuccessHandler = function() {
				disableLoading();
				$('#myModalsave').modal('hide');
				showNotification("success", "Data source saved successfully..");
			};
			if (THIS.profile) {
				id = THIS.profile.id;
				remarks = THIS.profile.remarks;
				displayName = THIS.profile.displayname;
			} else {
				remarks = $("#myModalsave #description").val();
				displayName = $("#myModalsave #profilename").val();
			}

			var request = {
				"source" : $("#source").select2("val"),
				"database" : $("#database").select2("val"),
				"config" : JSON.stringify(config),
				"displayname" : displayName,
				"remarks" : remarks,
				"type" : "datasource",
				"id" : id
				
			};
			enableLoading();
			callAjaxService("saveProfile", callBackSuccessHandler,
					callBackFailure, request, "POST");
		};
		if (THIS.profile && THIS.profile.id) {
			saveDataProfile();
		} else {
			$("#myModalsave #description").val("");
			$("#myModalsave #profilename").val("");
			$("#myModalsave").modal('show');
			$("#myModalsave #btnCreate").unbind("click").bind("click",
					function() {
						if (!$("#frmSaveDataSource").valid())
							return;
						saveDataProfile();
					});
		}
	};
})();