var salesDashboard=  function(filters){
	var This = this;
	this.load = function(){
		this.ndx = [];
		var request = {
			source:"hive",
			database:"APOTEXDB",//FORECASTINGDB
			table:"mst_sales_dashboard",
			/*query:"select qbrand_generic,strength,pack_size,qmanufacturer as manufacturer,year,month,'Total' as total,sales_unit," +
					"sales_value,avg(awp_usd) awp_usd,avg(wac_usd) wac_usd,avg(ims_at_sp) ims_at_sp from mst_sales_dashboard where <FILTER> " +
					"group by qbrand_generic,strength,pack_size,qmanufacturer,year,month,sales_unit,sales_value",*/
			columns:"qbrand_generic,strength,pack_size,qmanufacturer as manufacturer,year,month,'Total' as total,sales_unit,sales_value,awp_usd,wac_usd,ims_at_sp",
			groupByEnabled:true,
			filters:filters+" and year not in ('2011','2012')"
		};
		callAjaxService("getData", function(response){
			response.data.forEach(function(obj) { obj.date = d3.timeFormat("%m/%Y").parse(obj.month+"/"+obj.year); });
			This.ndx = crossfilter(response.data);
			var dimFun = new Function('d', 'return d["manufacturer"]|| "Others";');
			var dimFun2 = new Function('d', 'return d["qbrand_generic"]|| "Others";');
			var dimFun3 = new Function('d', 'return d["total"]|| "Others";');
			var dim = This.ndx.dimension(dimFun);
			var dim2 = This.ndx.dimension(dimFun2);
			var dim3 = This.ndx.dimension(dimFun3);
			This.trendDim = {
				dim:dim,
				dim2:dim2,
				dim3:dim3
			};
			//This.data = response.data;
			constructTrendChart("sales_value");
			constructBarChart({html:"#strengthChart",measure:"sales_value",field:"strength",xaxislabel:"",yaxislabel:""});
			constructBarChart({html:"#packsizeChart",measure:"sales_value",field:"pack_size",xaxislabel:"",yaxislabel:""});
			constructBarChart({html:"#yearChart",measure:"sales_value",field:"year",xaxislabel:"",yaxislabel:""});
		}, callBackFailure,request, "POST",null,true);
	};
	$("#salesDashboardMeasure").on("change",function(){
		constructTrendChart($(this).val());
	});
	var reduceFieldsAdd = function(measures,labels,filter,filterValue,dimension) {
		labels = labels || [];
		filterValue = filterValue || [];
		dimension = dimension || [];
		return function(p, v) {
			if (!filter || filterValue.length === 0 || (v[filter] && filterValue.indexOf(v[filter]) !== -1)){
				p.count += +v._count;
				measures.forEach(function(f) {
					if(v[f] && !isNaN(v[f]))
						p[f] += +v[f];
				});
			}else{
				measures.forEach(function(f) {
					p[f] += 0 ;
				});
			}
			labels.forEach(function(f) {
				p[f] = v[f];
			});
			dimension.forEach(function(f) {
				if(!p[f][v[f]])
					  p[f][v[f]] = 0;
				p[f][v[f]]++;
			});
			return p;
		};
	};
	var reduceFieldsRemove = function(measures,labels,filter,filterValue,dimension) {
		labels = labels || [];
		filterValue = filterValue || [];
		dimension = dimension || [];
		return function(p, v) {
			if (!filter || filterValue.length === 0 || (v[filter] && filterValue.indexOf(v[filter]) !== -1)){
				p.count -= +v._count;
				measures.forEach(function(f) {
					if(v[f] && !isNaN(v[f]))
						p[f] -= +v[f];
				});
			}else{
				measures.forEach(function(f) {
					p[f] -= 0 ;
				});
			}
			labels.forEach(function(f) {
				p[f] = v[f];
			});
			dimension.forEach(function(f) {
				if(!p[f][v[f]])
					  p[f][v[f]] = 0;
				p[f][v[f]]++;
			});
			return p;
		};
	};
	var reduceFieldsInitial = function(measures,labels,filter,filterValue,dimension) {
		labels = labels || [];
		dimension = dimension||[];
		return function() {
			var ret = {};
			ret["count"] = 0;
			measures.forEach(function(f) {
				ret[f] = 0;
			});
			labels.forEach(function(f) {
				ret[f] = "";
			});
			dimension.forEach(function(f) {
				ret[f] = {};
			});
			return ret;
		};
	};
	var reduceDatesFieldsAdd = function(meas,labels,filter,filterValue,dimension) {
		labels = labels || [];
		filterValue = filterValue || [];
		dimension = dimension || [];
		return function(p, v) {
			var value = v[meas]?parseFloat(v[meas]):0;
			//if(value && p){
				v._count = parseInt(v._count);
				p.count += +v._count;
				p.totalValue+= value;
				if(p.values.length === 0){
					p.values.push({
			    		date:v.date,//d3.timeFormat("%m/%Y").parse(v.month+"/"+v.year),//v.date,
			    		rating:value,
			    		count:v._count
			    	});
			    }else{
			    	var b = p.values.filter(function(o){return (o.date && v.date && o.date.getTime() === v.date.getTime())});
			    	if(b.length === 0){
				    	p.values.push({
				    		date:v.date,//d3.timeFormat("%m/%Y").parse(v.month+"/"+v.year),
				    		rating:value,
				    		count:v._count
				    	});
			    	}else{
			    		b[0].rating+=value;
			    		b[0].count+=+v._count;
			    	}
			    }
				return p;
			//}
		};
	};
	var reduceDatesFieldsRemove = function(meas,labels,filter,filterValue,dimension) {
		labels = labels || [];
		filterValue = filterValue || [];
		dimension = dimension || [];
		return function(p, v) {
			var value = v[meas]?parseFloat(v[meas]):0;
			//if(value && p){
				p.totalValue-= value;
				if(p.values.length === 0){
					p.values.push({
			    		date:v.date,//d3.timeFormat("%m/%Y").parse(v.month+"/"+v.year),
			    		rating:value
			    	});
			    }else{
			    	var b = p.values.filter(function(o){return (o.date && v.date && o.date.getTime() === v.date.getTime())});
			    	if(b.length === 0){
				    	p.values.push({
				    		date:v.date,//d3.timeFormat("%m/%Y").parse(v.month+"/"+v.year),
				    		rating:value
				    	});
			    	}else{
			    		b[0].rating-=value;
			    		b[0].count-=+v._count;
			    	}
			    }
				return p;
			//}
		};
	};
	var reduceDatedFieldsInitial = function(meas,labels,filter,filterValue,dimension) {
		labels = labels || [];
		dimension = dimension||[];
		return function() {
			return {
				count : 0,
				totalValue:0,
				values : []
			};
		};
	};
	var refreshTrendChart = function() {
		var measure = $("#salesDashboardMeasure").val();
		constructTrendChart(measure);
	};
	var constructTrendChart = function(measure){
		$("#trendchart").empty();
		var data = [];
		/*var dimFun = new Function('d', 'return d["manufacturer"]|| "Others";');
		var dimFun2 = new Function('d', 'return d["qbrand_generic"]|| "Others";');
		var dimFun3 = new Function('d', 'return d["total"]|| "Others";');
		var dim = This.ndx.dimension(dimFun);
		var dim2 = This.ndx.dimension(dimFun2);
		var dim3 = This.ndx.dimension(dimFun3);*/
		/*responseData.forEach(function(d) {
		    d.date = parseDate(d.month+"/"+d.year);
	    	var value = d[measure]?parseFloat(d[measure]):0;
		    if(value){
			    var obj= data.filter(function(o){return o.name === d.manufacturer});
			    var brandGeneric = data.filter(function(o){return o.name === d.qbrand_generic});
			    totalObj.totalValue+=value;
			    
			    var a = totalObj.values.filter(function(o){return (o.date && d.date && o.date.getTime() === d.date.getTime())});
			    if(a.length === 0){
			    	totalObj.values.push({
			    		date:d.date,
			    		rating:value
			    	});
			    }else{
			    	a[0].rating+=value;
			    	//totalObj.values.indexOf(a[0])
			    }
			    if(obj.length === 0){
			    	data.push({
			    		name:d.manufacturer,
			    		totalValue:0,
			    		values:[]
			    	});
			    }else{
			    	obj[0].totalValue+= value;
			    	var b = obj[0].values.filter(function(o){return (o.date && d.date && o.date.getTime() === d.date.getTime())});
			    	if(b.length === 0){
				    	obj[0].values.push({
				    		date:d.date,
				    		rating:value
				    	});
			    	}else{
			    		b[0].rating+=value;
			    	}
			    }
			    if(brandGeneric.length === 0){
			    	data.push({
			    		name:d.qbrand_generic,
			    		visible:true,
			    		totalValue:0,
			    		values:[]
			    	});
			    }else{
			    	var c = brandGeneric[0].values.filter(function(o){return (o.date && d.date && o.date.getTime() === d.date.getTime())});
			    	if(c.length === 0){
				    	brandGeneric[0].values.push({
				    		date:d.date,
				    		rating:value
				    	});
			    	}else{
			    		c[0].rating+=value;
			    	}
			    	brandGeneric[0].totalValue+= value;
			    }
		    }
		 });*/
		var groupedData = [];
		var grp = This.trendDim.dim.group().reduce(reduceDatesFieldsAdd(measure),reduceDatesFieldsRemove(measure),reduceDatedFieldsInitial(measure));
		var grp2 = This.trendDim.dim2.group().reduce(reduceDatesFieldsAdd(measure),reduceDatesFieldsRemove(measure),reduceDatedFieldsInitial(measure));
		var grp3 = This.trendDim.dim3.group().reduce(reduceDatesFieldsAdd(measure),reduceDatesFieldsRemove(measure),reduceDatedFieldsInitial(measure));
		groupedData = grp3.all();
		groupedData = grp2.all().concat(groupedData);
		groupedData.forEach(function(obj) { obj.visible = true; });
		groupedData = grp.all().concat(groupedData);
		for(var i=0;i<groupedData.length;i++){
			groupedData[i].value.values.sort(function(a,b){return a.date<b.date?-1:a.date>b.date?1:0});
			data.push({
				name:groupedData[i].key,
				totalValue:groupedData[i].value.totalValue,
				values:groupedData[i].value.values.filter(function(o){return o.rating >0}),
				visible:groupedData[i].visible
			});
		}
		var margin = {top: 20, right: 200, bottom: 100, left: 100},
	    margin2 = { top: 430, right: 10, bottom: 20, left: 40 },
	    width = 1100 - margin.left - margin.right,
	    height = 500 - margin.top - margin.bottom,
	    height2 = 500 - margin2.top - margin2.bottom;
	var bisectDate = d3.bisector(function(d) { return d.date; }).left;

	var xScale = d3.time.scale()
	    .range([0, width]),

	    xScale2 = d3.time.scale()
	    .range([0, width]); // Duplicate xScale for brushing ref later

	var yScale = d3.scaleLinear()
	    .range([height, 0]);

	// 40 Custom DDV colors
	var range = getRandomColors(data.length),domain = data.map(function(o){return o.name});
	range.unshift("#000000");
	domain.unshift("Total");
	var color = d3.scaleOrdinal().range(range);  
	 color.domain(domain);
	//data.unshift(totalObj);
	var xAxis = d3.svg.axis()
	    .scale(xScale)
	    .orient("bottom"),

	    xAxis2 = d3.svg.axis() // xAxis for brush slider
	    .scale(xScale2)
	    .orient("bottom");    

	var yAxis = d3.svg.axis()
	    .scale(yScale)
	    .orient("left");  

	var line = d3.svg.line()
	    .interpolate("basis")
	    .x(function(d) { 
	    	return xScale(d.date); 
	    })
	    .y(function(d) { 
	    	return yScale(d.rating);
	    })
	    .defined(function(d) {
	    	return d.rating; 
	    });  // Hiding line value defaults of 0 for missing data

	var maxY; // Defined later to update yAxis

	var svg = d3.select("#trendchart").append("svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom) //height + margin.top + margin.bottom
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	// Create invisible rect for mouse tracking
	svg.append("rect")
	    .attr("width", width)
	    .attr("height", height)                                    
	    .attr("x", 0) 
	    .attr("y", 0)
	    .attr("id", "mouse-tracker")
	    .style("fill", "white"); 

	//for slider part-----------------------------------------------------------------------------------
	  
	var context = svg.append("g") // Brushing context box container
	    .attr("transform", "translate(" + 0 + "," + 410 + ")")
	    .attr("class", "context");

	//append clip path for lines plotted, hiding those part out of bounds
	svg.append("defs")
	  .append("clipPath") 
	    .attr("id", "clip")
	    .append("rect")
	    .attr("width", width)
	    .attr("height", height); 

	//end slider part----------------------------------------------------------------------------------- 

	//d3.tsv("data.tsv", function(error, data) { 
		
	  /*color.domain(d3.keys(data[0]).filter(function(key) { // Set the domain of the color ordinal scale to be all the csv headers except "date", matching a color to an issue
	    return key !== "date"; 
	  }));*/

	 var categories = data.sort(function(a,b){return a.totalValue<b.totalValue?1:a.totalValue>b.totalValue?-1:0});
	 for(var i=0;i<categories.length;i++){
		 if(categories[i].visible)
			 categories[i].values.sort(function(a,b){return a.date<b.date?-1:a.date>b.date?1:0});
	 }
	 //categories[0].visible = true;
	  /*var categories = color.domain().map(function(name) { // Nest the data into an array of objects with new keys

	    return {
	      name: name, // "name": the csv headers except date
	      values: data.map(function(d) { // "values": which has an array of the dates and ratings
	        return {
	          date: d.date, 
	          rating: +(d[name]),
	          };
	      }),
	      visible: (name === "Unemployment" ? true : false) // "visible": all false except for economy which is true.
	    };
	  });*/
	  var AllDates = grp3.all()[0].value.values;
	  xScale.domain(d3.extent(AllDates, function(d) {
		  return d.date;
		})); // extent = highest and lowest points, domain is data, range is bouding box

	  yScale.domain([0, 
	    d3.max(categories, function(c) { return d3.max(c.values, function(v) { return v.rating; }); })
	  ]);

	  xScale2.domain(xScale.domain()); // Setting a duplicate xdomain for brushing reference later
	 
	 //for slider part-----------------------------------------------------------------------------------

	 var brush = d3.svg.brush()//for slider bar at the bottom
	    .x(xScale2) 
	    .on("brush", brushed);

	  context.append("g") // Create brushing xAxis
	      .attr("class", "x axis1")
	      .attr("transform", "translate(0," + height2 + ")")
	      .call(xAxis2);

	  var contextArea = d3.svg.area() // Set attributes for area chart in brushing context graph
	    .interpolate("monotone")
	    .x(function(d) { 
	    	return xScale(d.date); 
	    }) // x is scaled to xScale2
	    .y0(height2) // Bottom line begins at height2 (area chart not inverted) 
	    .y1(0); // Top line of area, 0 (area chart not inverted)

	  //plot the rect as the bar at the bottom
	  context.append("path") // Path is created using svg.area details
	    .attr("class", "area")
	    .attr("d", contextArea(categories[0].values)) // pass first categories data .values to area path generator 
	    .attr("fill", "#F1F1F2");
	    
	  //append the brush for the selection of subsection  
	  context.append("g")
	    .attr("class", "x brush")
	    .call(brush)
	    .selectAll("rect")
	    .attr("height", height2) // Make brush rects same height 
	      .attr("fill", "#0000ff");  
	  //end slider part-----------------------------------------------------------------------------------

	  // draw line graph
	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis);

	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("x", -10)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .text("total Value");

	  var issue = svg.selectAll(".issue")
	      .data(categories) // Select nested data and append to new svg group elements
	    .enter().append("g")
	      .attr("class", "issue");   

	  issue.append("path")
	      .attr("class", "line")
	      .style("pointer-events", "none") //Stop line interferring with cursor
	      .attr("id", function(d) {
	    	 // d.name.replace(/[%./#-/() ]/gi, '_')
	        return "line-" + d.name.replace(/[%./#-/() ]/gi, ''); // Give line id of line-(insert issue name, with any spaces replaced with no spaces)
	      })
	      .attr("d", function(d) { 
	        return d.visible ? line(d.values) : null; // If array key "visible" = true then draw line, if not then don't 
	      })
	      .attr("clip-path", "url(#clip)")//use clip path to make irrelevant part invisible
	      .style("stroke", function(d) { return color(d.name); });

	  // draw legend
	  var legendSpace = 450 / Object.keys(categories).length; // 450/number of issues (ex. 40)    

	  issue.append("rect")
	      .attr("width", 10)
	      .attr("height", 10)                                    
	      .attr("x", width + (margin.right/3) - 15) 
	      .attr("y", function (d, i) { return (legendSpace)+i*(legendSpace) - 8; })  // spacing
	      .attr("fill",function(d) {
	        return d.visible ? color(d.name) : "#F1F1F2"; // If array key "visible" = true then color rect, if not then make it grey 
	      })
	      .attr("class", "legend-box")

	      .on("click", function(d){ // On click make d.visible 
	        d.visible = !d.visible; // If array key for this data selection is "visible" = true then make it false, if false then make it true
	        d.values = d.values.filter(function(o){return o.rating>0});
	        d.values =  d.values.sort(function(a,b){return a.date<b.date?-1:a.date>b.date?1:0});
	        maxY = findMaxY(categories); // Find max Y rating value categories data with "visible"; true
	        yScale.domain([0,maxY]); // Redefine yAxis domain based on highest y value of categories data with "visible"; true
	        svg.select(".y.axis")
	          .transition()
	          .call(yAxis);   

	        issue.select("path")
	          .transition()
	          .attr("d", function(d){
	            return d.visible ? line(d.values) : null; // If d.visible is true then draw line for this d selection
	          })

	        issue.select("rect")
	          .transition()
	          .attr("fill", function(d) {
	          return d.visible ? color(d.name) : "#F1F1F2";
	        });
	      })

	      .on("mouseover", function(d){

	        d3.select(this)
	          .transition()
	          .attr("fill", function(d) { return color(d.name); });

	        d3.select("#line-" + d.name.replace(/[%./#-/() ]/gi, ''))// d.name.replace(/[%./#-/() ]/gi, '_')
	          .transition()
	          .style("stroke-width", 2.5);  
	      })

	      .on("mouseout", function(d){

	        d3.select(this)
	          .transition()
	          .attr("fill", function(d) {
	          return d.visible ? color(d.name) : "#F1F1F2";});

	        d3.select("#line-" + d.name.replace(/[%./#-/() ]/gi, ''))// d.name.replace(/[%./#-/() ]/gi, '_')
	          .transition()
	          .style("stroke-width", 1.5);
	      })
	      
	  issue.append("text")
	      .attr("x", width + (margin.right/3)) 
	      .attr("y", function (d, i) { return (legendSpace)+i*(legendSpace); })  // (return (11.25/2 =) 5.625) + i * (5.625) 
	      .text(function(d) { return d.name; }); 

	  // Hover line 
	  var hoverLineGroup = svg.append("g") 
	            .attr("class", "hover-line");

	  var hoverLine = hoverLineGroup // Create line with basic attributes
	        .append("line")
	            .attr("id", "hover-line")
	            .attr("x1", 10).attr("x2", 10) 
	            .attr("y1", 0).attr("y2", height + 10)
	            .style("pointer-events", "none") // Stop line interferring with cursor
	            .style("opacity", 1e-6); // Set opacity to zero 

	  var hoverDate = hoverLineGroup
	        .append('text')
	            .attr("class", "hover-text")
	            .attr("y", height - (height-40)) // hover date text position
	            .attr("x", width - 150) // hover date text position
	            .style("fill", "#ccc");//E6E7E8

	  var columnNames = d3.keys(data[0]); //grab the key values from your first data row
	                                     //these are the same as your column names
	                  //.slice(1); //remove the first column name (`date`);

	  var focus = issue.select("g") // create group elements to house tooltip text
	      .data(columnNames) // bind each column name date to each g element
	    .enter().append("g") //create one <g> for each columnName
	      .attr("class", "focus"); 

	  focus.append("text") // http://stackoverflow.com/questions/22064083/d3-js-multi-series-chart-with-y-value-tracking
	        .attr("class", "tooltip")
	        .attr("x", width + 20) // position tooltips  
	        .attr("y", function (d, i) { 
	        	return (legendSpace)+i*(legendSpace); 
	        	}); // (return (11.25/2 =) 5.625) + i * (5.625) // position tooltips       

	  // Add mouseover events for hover line.
	 /* d3.select("#mouse-tracker") // select chart plot background rect #mouse-tracker
	  .on("mousemove", mousemove) // on mousemove activate mousemove function defined below
	  .on("mouseout", function() {
	      hoverDate
	          .text(null) // on mouseout remove text for hover date

	      d3.select("#hover-line")
	          .style("opacity", 1e-6); // On mouse out making line invisible
	  });*/
//********************Mouse on hover*******************************
	  var mouseG = svg.append("g")
      .attr("class", "mouse-over-effects");

    mouseG.append("path") // this is the black vertical line to follow mouse
      .attr("class", "mouse-line")
      .style("stroke", "black")
      .style("stroke-width", "1px")
      .style("opacity", "0");
      
    var lines = document.getElementsByClassName('line');

    var mousePerLine = mouseG.selectAll('.mouse-per-line')
      .data(data)
      .enter()
      .append("g")
      .attr("class", "mouse-per-line");

    mousePerLine.append("circle")
      .attr("r", 7)
      .style("stroke", function(d) {
        return color(d.name);
      })
      .style("fill", "none")
      .style("stroke-width", "1px")
      .style("opacity", "0");

    mousePerLine.append("text")
      .attr("transform", "translate(10,3)");

    mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
      .attr('width', width) // can't catch mouse events on a g element
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .on('mouseout', function() { // on mouse out hide line, circles and text
        d3.select(".mouse-line")
          .style("opacity", "0");
        d3.selectAll(".mouse-per-line circle")
          .style("opacity", "0");
        d3.selectAll(".mouse-per-line text")
          .style("opacity", "0");
      })
      .on('mouseover', function() { // on mouse in show line, circles and text
        d3.select(".mouse-line")
          .style("opacity", "1");
        d3.selectAll(".mouse-per-line circle")
          .style("opacity", "1");
        d3.selectAll(".mouse-per-line text")
          .style("opacity", "1")
          .style("stroke", "steelblue")
          .style("stroke-width", "1px");
      })
      .on('mousemove', function() { // mouse moving over canvas
        var mouse = d3.mouse(this);
        d3.select(".mouse-line")
          .attr("d", function() {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });

        d3.selectAll(".mouse-per-line")
          .attr("transform", function(d, i) {
            //console.log(width/mouse[0])
            if(d.visible){
                var xDate = xScale.invert(mouse[0]),
                    bisect = d3.bisector(function(d) { return d.date; }).right;
                    idx = bisect(d.values, xDate);
                
                var beginning = 0,
                    end = lines[i].getTotalLength(),
                    target = null;

                while (true){
                  target = Math.floor((beginning + end) / 2);
                  pos = lines[i].getPointAtLength(target);
                  if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                      break;
                  }
                  if (pos.x > mouse[0])      end = target;
                  else if (pos.x < mouse[0]) beginning = target;
                  else break; //position found
                }
                
                d3.select(this).select('text')
                  .text(getManipulatedDate(null,0,xScale.invert(pos.x),"%d %B %Y")+" : "+yScale.invert(pos.y).toFixed(2));
                  
                return "translate(" + mouse[0] + "," + pos.y +")";
            }
          });
      });
//***************************************************	  
	 /* function mousemove() { 
	      var mouse_x = d3.mouse(this)[0]; // Finding mouse x position on rect
	      var graph_x = xScale.invert(mouse_x); // 

	      //var mouse_y = d3.mouse(this)[1]; // Finding mouse y position on rect
	      //var graph_y = yScale.invert(mouse_y);
	      //console.log(graph_x);
	      var mouse_y = d3.mouse(this)[1];
	      var graph_y = yScale.invert(mouse_y);
	      var format = d3.timeFormat('%b %Y'); // Format hover date text to show three letter month and full year
	      
	      // scale mouse position to xScale date and format it to show month and year
	      var yAxisValue = data.filter(function(o){return mouse_y>o.totalValue})
	      d3.select("#hover-line") // select hover-line and changing attributes to mouse position
	          .attr("x1", mouse_x) 
	          .attr("x2", mouse_x)
	          .style("opacity", 1); // Making line visible

	      // Legend tooltips // http://www.d3noob.org/2014/07/my-favourite-tooltip-method-for-line.html

	      var x0 = xScale.invert(d3.mouse(this)[0]),  d3.mouse(this)[0] returns the x position on the screen of the mouse. xScale.invert function is reversing the process that we use to map the domain (date) to range (position on screen). So it takes the position on the screen and converts it into an equivalent date! 
	      i = bisectDate(data, x0, 1), // use our bisectDate function that we declared earlier to find the index of our data array that is close to the mouse cursor
	      It takes our data array and the date corresponding to the position of or mouse cursor and returns the index number of the data array which has a date that is higher than the cursor position.
	      d0 = data[i - 1],
	      d1 = data[i],
	      d0 is the combination of date and rating that is in the data array at the index to the left of the cursor and d1 is the combination of date and close that is in the data array at the index to the right of the cursor. In other words we now have two variables that know the value and date above and below the date that corresponds to the position of the cursor.
	      d = x0 - d0.date > d1.date - x0 ? d1 : d0;
	      The final line in this segment declares a new array d that is represents the date and close combination that is closest to the cursor. It is using the magic JavaScript short hand for an if statement that is essentially saying if the distance between the mouse cursor and the date and close combination on the left is greater than the distance between the mouse cursor and the date and close combination on the right then d is an array of the date and close on the right of the cursor (d1). Otherwise d is an array of the date and close on the left of the cursor (d0).

	      //d is now the data row for the date closest to the mouse position
	      var value  = d.values.filter(function(o){return graph_x.getMonth()===o.date.getMonth() && graph_x.getFullYear()===o.date.getFullYear()});
	      hoverDate.text(format(graph_x)+" : "+value[0].rating); 
	      focus.select("text").text(function(columnName){
	         //because you didn't explictly set any data on the <text>
	         //elements, each one inherits the data from the focus <g>

	         return (d[columnName]);
	      });
	  };*/
	  var zoom = d3.behavior.zoom().x(xScale).y(yScale).scaleExtent([1,8]).on("zoom", zoomed);

	//call the zoom on the SVG
	 // svg.call(zoom);
	  function zoomed() {
		  
	    	svg.select(".x.axis").call(xAxis);
	    	svg.select(".y.axis").call(yAxis);

			svg.selectAll(".tipcircle")
				.attr("cx", function(d,i){return x(d.date)})
				.attr("cy",function(d,i){return y(d.value)});
				
			svg.selectAll(".line")
	    		.attr("class","line")
	        	.attr("d", function (d) { return line(d.values)});
		};
	  //for brusher of the slider bar at the bottom
	  function brushed() {

	    xScale.domain(brush.empty() ? xScale2.domain() : brush.extent()); // If brush is empty then reset the Xscale domain to default, if not then make it the brush extent 

	    svg.select(".x.axis") // replot xAxis with transition when brush used
	          .transition()
	          .call(xAxis);

	    maxY = findMaxY(categories); // Find max Y rating value categories data with "visible"; true
	    yScale.domain([0,maxY]); // Redefine yAxis domain based on highest y value of categories data with "visible"; true
	    
	    svg.select(".y.axis") // Redraw yAxis
	      .transition()
	      .call(yAxis);   

	    issue.select("path") // Redraw lines based on brush xAxis scale and domain
	      .transition()
	      .attr("d", function(d){
	          return d.visible ? line(d.values) : null; // If d.visible is true then draw line for this d selection
	      });
	    
	  };      

	//}); // End Data callback function
	  
	  function findMaxY(data){  // Define function "findMaxY"
	    var maxYValues = data.map(function(d) { 
	      if (d.visible){
	        return d3.max(d.values, function(value) { // Return max rating value
	          return value.rating; })
	      }
	    });
	    return d3.max(maxYValues);
	  }
	};
	var constructBarChart = function(config){
		$(config.html).find("svg").remove();
		var dimFun = new Function('d', "return d['" + config.field	+ "'] || 'Others' ;");
		var dim = This.ndx.dimension(dimFun);
		var grp = dim.group().reduce(reduceFieldsAdd([config.measure]),
				reduceFieldsRemove([config.measure]),
				reduceFieldsInitial([config.measure]));
		var bar = dc.barChart(config.html);
		bar.margins({
			top : 20,
			left : 40,
			right : 10,
			bottom : 30
		}).width(300).height(200).x(d3.scaleOrdinal())
				.xUnits(dc.units.ordinal).brushOn(false)
				.xAxisLabel(config.xaxislabel ? config.xaxislabel : '')
				.elasticX(true)
				.yAxisLabel(config.yaxislabel ? config.yaxislabel : '')
				.dimension(dim)
				.gap(5)
				// .barPadding(0.1)
				//.renderLabel(true)
				.title(function () { return ""; }).elasticY(true)
				.group(grp).valueAccessor(function(d){
					return d.value[config.measure];
				});
		$(".reset", $(config.html)).on('click', bar,function(e) {
			e.data.filterAll();
			dc.redrawAll();	
		}.bind(this));
		bar.yAxis().ticks(10).tickFormat(d3.format("s"));
		bar.colors(d3.scaleOrdinal().domain(grp.all().map(function(o){return o.key})).range(grp.all().map(function(o){return "#4169E1"})));
		bar.colorAccessor(function(d) { 
		 	return d.key;
	    });
		bar.render();
		bar.renderlet(function(chart) {
			moveGroupNames(chart,config.measure);
		});
		bar.on('filtered', function() {
			refreshTrendChart();
		}.bind(this));
	};
	
	function moveGroupNames(chart,measure) {
		var gLabels = chart.select(".labels");
		if (gLabels.empty()) {
			gLabels = chart.select(".chart-body").append('g').classed(
					'labels', true);
		}

		var gLabelsData = gLabels.selectAll("text").data(
				chart.selectAll(".bar")[0]);

		gLabelsData.exit().remove(); // Remove unused elements

		gLabelsData.enter().append("text"); // Add new elements

		gLabelsData
				.attr('text-anchor', 'middle')
				.attr('fill', 'white')
				.text(
						function(e) {
							var d = d3.select(e).data()[0].data;
							var tempValue;
							tempValue = d.value[measure];
							
							var prefix = d3.formatPrefix(tempValue);// d3.select(d).data()[0].data.value
							var prefixScale = Math.round(prefix
									.scale(tempValue), 0);
							return prefixScale + prefix.symbol;
						}).attr(
						'x',
						function(d) {
							return +d.getAttribute('x')
									+ (d.getAttribute('width') / 2);
						}).attr('y', function(d) {
					return +d.getAttribute('y') + 15;
				}).attr('style', function(d) {
					if (+d.getAttribute('height') < 18)
						return "display:none";
				});

	}
};