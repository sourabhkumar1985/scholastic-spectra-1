pageSetUp();
var DRL;
var drlInstance = function() {
	DRL = new drlObj();
	DRL.inti();
};
var drlObj = function() {
	var THIS = this;
	this.inti = function() {
		drawWorldMap();
		drawEasyPie();
		drawBarChart();
		drawMultiSelect();
		drawMultiSeriesLineChart();
	};

	var drawMultiSeriesLineChart = function() {

		THIS.moveChart = dc.lineChart("#monthly-move-chart");
		THIS.volumeChart = dc.barChart("#monthly-volume-chart");
		THIS.yearlyBubbleChart = dc.bubbleChart('#yearly-bubble-chart');
		d3.csv("web/data/DRL.csv", function(dataLine) {
			/* since its a csv file we need to format the data a bit */
			var dateFormat = d3.timeFormat("%Y");
			var numberFormat = d3.format(".2f");
console.log(dataLine);
			dataLine.forEach(function(d) {
				d.dd = dateFormat.parse(d.year);
				d.month = d3.time.month(d.dd); // pre-calculate month for
												// better performance
				d.salesVolume = +d.salesVolume; // coerce to number
				d.marketValue = +d.marketValue;
			});
console.log("ok1");
			var ndx1 = crossfilter(dataLine);
			var all1 = ndx1.groupAll();
			var moveMonths = ndx1.dimension(function(d) {
				return d.month;
			});
			console.log("ok3");
			var volumeByMonthGroup = moveMonths.group().reduceSum(function(d) {
				return d.salesVolume / 500000;
			});
			console.log("ok4");
			 var monthlyMoveGroup = moveMonths.group().reduceSum(function(d) {
				return Math.abs(d.salesVolume);
			});
			var yearlyDimension = ndx1.dimension(function (d) {
		        return d.Company;
		    });
			 var yearlyPerformanceGroup = yearlyDimension.group().reduce(
				        /* callback for when data is added to the current filter results */
				        function (p, v) {
				            ++p.count;
				            p.absGain += v.salesVolume;
				           /* p.fluctuation += Math.abs(v.close - v.open);
				            p.sumIndex += (v.open + v.close) / 2;
				            p.avgIndex = p.sumIndex / p.count;*/
				            p.percentageGain = d3.time.year(v.dd).getFullYear();
				            //p.fluctuationPercentage = (p.fluctuation / p.avgIndex) * 100;
				            p.fluctuationPercentage = v.salesVolume/10000000;
				            return p;
				        },
				        /* callback for when data is removed from the current filter results */
				        function (p, v) {
				            --p.count;
				            p.absGain -= v.salesVolume;
				           /* p.fluctuation -= Math.abs(v.salesVolume);
				            p.sumIndex -= (v.open + v.close) / 2;
				            p.avgIndex = p.sumIndex / p.count;*/
				            p.percentageGain = d3.time.year(v.dd).getFullYear();
				            //p.fluctuationPercentage = (p.fluctuation / p.avgIndex) * 100;
				            p.fluctuationPercentage =  v.salesVolume/1000000;
				            return p;
				        },
				        /* initialize p */
				        function () {
				            return {
				                count: 0,
				                absGain: 0/*,
				                fluctuation: 0,
				                fluctuationPercentage: 0,
				                sumIndex: 0,
				                avgIndex: 0,
				                percentageGain: 0*/
				            };
				        }
				    );
			var indexAvgByMonthGroup = moveMonths.group().reduce(
					function(p, v) {
						++p.days;
						p.total += (v.salesVolume) / 2;
						p.avg = Math.round(p.total / p.days);
						return p;
					}, function(p, v) {
						--p.days;
						p.total -= (v.salesVolume) / 2;
						p.avg = p.days ? Math.round(p.total / p.days) : 0;
						return p;
					}, function() {
						return {
							days : 0,
							total : 0,
							avg : 0
						};
					});

			
			THIS.yearlyBubbleChart
	        .width(500) // (optional) define chart width, :default = 200
	        .height(250)  // (optional) define chart height, :default = 200
	        .transitionDuration(1500) // (optional) define chart transition duration, :default = 750
	       // .margins({top: 10, right: 50, bottom: 30, left: 40})
	        .dimension(yearlyDimension)
	        .group(yearlyPerformanceGroup)
        .colors(colorbrewer.RdYlGn[9]) // (optional) define color function or array for bubbles
        .colorDomain([-500, 500])
         .colorAccessor(function (d) {
            return d.value.absGain;
        })
        .keyAccessor(function (p) {
            return  p.value.absGain;
        })
        .valueAccessor(function (p) {
            return  p.value.percentageGain;
        })
        .radiusValueAccessor(function (p) {
            return p.value.fluctuationPercentage;
        })
        
        .maxBubbleRelativeSize(0.1)
        //.xUnits(d3.time.months)
        .x(
							d3.time.scale().domain(
									[ new Date(2004, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.year.round).xUnits(d3.time.years)
       // .y(d3.time.scale().domain([new Date(2013, 0, 1), new Date(2015, 11, 31)]))
       // .xUnits(d3.time.years)
        .y(d3.scaleLinear().domain([0, 100]))
        //.yAxis().tickValues([0, 100, 200, 300])
        //.yUnits(d3.time.months)
        //.y(d3.scaleLinear().domain([2013, 2015]))
        .r(d3.scaleLinear().domain([0, 10000]))
        .elasticY(true)
        .elasticX(true)
        .yAxisPadding(1)
        .xAxisPadding(1)
        .renderHorizontalGridLines(true) // (optional) render horizontal grid lines, :default=false
        .renderVerticalGridLines(true) // (optional) render vertical grid lines, :default=false
        .xAxisLabel('Index Gain') // (optional) render an axis label below the x axis
        .yAxisLabel('Index Gain %') 
        .renderLabel(true) // (optional) whether chart should render labels, :default = true
        .label(function (p) {
            return p.key;
        })
        .renderTitle(true) // (optional) whether chart should render titles, :default = false
        .title(function (p) {
            return [
                p.key,
                'Index Gain: ' + numberFormat(p.value.absGain),
                'Index Gain in Percentage: ' + numberFormat(p.value.percentageGain) + '%',
                'Fluctuation / Index Ratio: ' + numberFormat(p.value.fluctuationPercentage) + '%'
            ].join('\n');
        })
         .yAxis()/*.tickFormat(function (v) {
            return v/1000000000000000 ;
        })*/;
			THIS.moveChart.renderArea(true).width($('#content').width()).height(200)
					.transitionDuration(1000).margins({
						top : 30,
						right : 50,
						bottom : 25,
						left : 40
					}).dimension(moveMonths).mouseZoomable(true).rangeChart(
							THIS.volumeChart).x(
							d3.time.scale().domain(
									[ new Date(2013, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true).legend(
							dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(indexAvgByMonthGroup,
							"Monthly Index Average").valueAccessor(function(d) {
						return d.value.avg;
					}).stack(monthlyMoveGroup, "Monthly Index Move",
							function(d) {
								return d.value;
							}).title(function(d) {
						var value = d.value.avg ? d.value.avg : d.value;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n" + numberFormat(value);
					});

			THIS.volumeChart.width($('#content').width()).height(180).margins({
				top : 0,
				right : 50,
				bottom : 20,
				left : 40
			}).dimension(moveMonths).group(volumeByMonthGroup).centerBar(true)
					.gap(1).x(
							d3.time.scale().domain(
									[ new Date(2013, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).alwaysUseRounding(true)
					.xUnits(d3.time.months);
			dc.dataCount(".dc-data-count").dimension(ndx1).group(all1);
			dc.renderAll();
			$("#monthly-volume-chart .axis.y").remove();
		});

	};

	var drawMultiSelect = function() {
		var demisionlist = $("#drllist");
		demisionlist.empty();
		var demisionData = new Array("Immunology", "Hepotology", "Hematology",
				"Healtly Volunteers", "Genertic Disease", "Gastroentrology",
				"Healtly Volunteers", "Endocrinology", "Devices",
				"Dermatology", "Dental and Oral Health",
				"Cardiology/Vascular Disease");
		var demisionFilters = new Array("Endocrinology", "Gastroentrology");
		for ( var i = 0; i < demisionData.length; i++) {
			var liValue = $.trim(demisionData[i]);
			var liObj = $("<li/>").addClass("color-333").appendTo(demisionlist);
			var isFiltered = false;
			for ( var j = 0; j < demisionFilters.length; j++) {
				if (demisionData[i] == demisionFilters[j]) {
					isFiltered = true;
					break;
				}
			}
			if (isFiltered) {
				liObj.addClass("filtered").append(
						$("<span/>").addClass("zd icon checkbox radio").append(
								$("<span/>").addClass("zd checked filtered")));
			} else {
				liObj.append($("<span/>").addClass("zd icon checkbox radio")
						.append($("<span/>").addClass("zd checked")));
			}
			liObj.append($("<div/>").addClass("attributeItem").append(
					$("<div/>").addClass("text").attr({
						"title" : liValue
					}).html(liValue)));
			// liObj.bind('click',
			// function(){System.demisionFilter(objsArray[0]+"."+objsArray[1],this);});
		}
	}
	var drawBarChart = function() {

		d3.json("web/data/wireframe.json", function(data) {
			/* since its a csv file we need to format the data a bit */
			THIS.fluctuationChart = dc.barChart("#fluctuation-chart");
			var dateFormat = d3.timeFormat("%m/%d/%Y");
			var numberFormat = d3.format(".2f");

			data.forEach(function(d) {
				d.dd = dateFormat.parse(d.ddate);
				d.month = d3.time.week(d.dd); // pre-calculate month for
												// better performance
				d.value = +d.value; // coerce to number
			});
			var ndx = crossfilter(data);
			// console.log(ndx);
			var all = ndx.groupAll();

			var fluctuation = ndx.dimension(function(d) {
				return d.month;
			});
			var fluctuationGroup = fluctuation.group().reduceSum(function(d) {
				return d.value;
			});

			THIS.fluctuationChart.width(1000).height(180)
			/* .margins({top: 10, right: 50, bottom: 30, left: 40}) */
			.dimension(fluctuation).group(fluctuationGroup).elasticY(true)
			// (optional) whether bar should be center to its x value. Not
			// needed for ordinal chart, :default=false
			.centerBar(true)
			// (optional) set gap between bars manually in px, :default=2
			.gap(1)
			// (optional) set filter brush rounding
			.round(dc.round.floor).alwaysUseRounding(true)
			/* .x(d3.scaleLinear().domain([-25, 25])) */
			.x(
					d3.time.scale().domain(
							[ new Date(2010, 0, 1), new Date(2012, 6, 10) ]))
					.round(d3.time.week.round).alwaysUseRounding(true).xUnits(
							d3.time.weeks).title(function(d) {
						return d.month;
					}).yAxis().ticks(0);
			/* .renderHorizontalGridLines(true) */
			// customize the filter displayed in the control span
			/*
			 * .filterPrinter(function (filters) { console.log(filters) var
			 * filter = filters[0], s = ""; s += numberFormat(filter[0]) + "% -> " +
			 * numberFormat(filter[1]) + "%"; return s; });
			 */

			// Customize axis
			/*
			 * fluctuationChart.xAxis().tickFormat( function (v) { return v +
			 * "%"; }); fluctuationChart.yAxis().ticks(0);
			 */
			dc.dataCount(".dc-data-count").dimension(ndx).group(all);
			dc.renderAll();
			$("#fluctuation-chart .axis.y").remove();
			$("#fluctuation-chart").scrollTo(
					$("#fluctuation-chart").width() / 2);
		});

	};
	var drawEasyPie = function() {
		$('.chart').easyPieChart({
			"barColor" : "#4D7686",
			"trackColor" : false,
			"scaleColor" : false,
			"size" : 100,
			"lineWidth" : 10
		});
	};
	var drawWorldMap = function() {
		var data_array = {
			"US" : 4977,
			"AU" : 4873,
			"IN" : 3671,
			"BR" : 2476,
			"TR" : 1476,
			"CN" : 146,
			"CA" : 134,
			"BD" : 100
		};
		$('#world-map').vectorMap({
			map : 'world_mill_en',
			backgroundColor : '#fff',
			regionStyle : {
				initial : {
					fill : '#c4c4c4'
				},
				hover : {
					"fill-opacity" : 1
				}
			},
			series : {
				regions : [ {
					values : data_array,
					scale : [ '#85a8b6', '#4d7686' ],
					normalizeFunction : 'polynomial'
				} ]
			},
			onRegionLabelShow : function(e, el, code) {
				if (typeof data_array[code] == 'undefined') {
					e.preventDefault();
				} else {
					var countrylbl = data_array[code];
					el.html(el.html() + ': ' + countrylbl + ' visits');
				}
			}
		});
	};
};

drlInstance();