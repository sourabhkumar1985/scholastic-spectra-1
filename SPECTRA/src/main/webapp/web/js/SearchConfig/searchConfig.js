pageSetUp();
var searchViewMode = '';
(function() {
    var This = this;
    This.dataBaseList = {};
    $("#frmSearchModal").submit(function(e) {
        e.preventDefault();
    });
    $('#btnSaveSearch').unbind('click').bind('click', function() { This.save(); });

    //$('#proceedSearch').unbind('click').bind('click', function() { This.proceed(); });

    $("#myprofiles").hide();
    $("#myProfiles").hide();
    $("#backToDesign").unbind("click").bind("click", function() {
        $("#myProfilesConfig").show(300);
        $("#myprofiles").hide(300);
        $("#myProfiles").hide(300);
        if (searchViewMode === 'configurator') {
            $("#profileConfig").show(300);
            return;
        }
        if (searchViewMode === 'queryBuilder') {
            $("#querybuilder").show(300);
            return;
        }
        This.currentId = null;
        $('#database').select2('val', '');
        $('#source').select2('val', '');
        $('#solrSchema').select2('val', '');
        $('#searchType').select2('val', '');
        $('#profilename').val('');
        $('#description').val('');
    });
    $("#database").select2({
        placeholder: "Choose Drilldown Database",
        data: new Array()
    }).on("change", function(e) {
        getTableNames();
    });

    $("#searchTable").select2({
        placeholder: "Choose Database Table",
        data: new Array()
    });
    // $("#database select").on('change', getColumnNames);

    $('#mySavedSearchProfile').unbind('click').bind('click', function() {
       callBackGetMyprofiles();
        $("#myProfilesConfig").hide(300);
        $("#myprofiles").show(300);
        $("#myProfiles").show(300);
        
        if ($('#profileConfig').length > 0) {
            $("#profileConfig").hide(300);
        }
        if($('#querybuilder').length > 0) {
            $('#querybuilder').hide(300);
        }
    });

    $("#frmSearchConfig").validate({
        rules: {
            database: {
                required: true
            },
            source: {
                required: true
            },
            solrSchema: {
                required: true
            },
            searchType: {
                required: true
            }
        },

        // Messages for form validation
        messages: {
            database: {
                required: 'Select database'
            },
            source: {
                required: 'Select source'
            },
            searchType: {
                required: 'Select searchType'
            },
            solrSchema: {
                required: 'Select Schema'
            }
        },

        // Do not change code below
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });
    $('#proceedSearch').unbind('click').bind('click', function() {
        if ($("#frmSearchConfig").valid()) {
            //$("#myModalSaveSearch").modal("show");
            This.proceed();
        }
    });

    $('#saveSearch').unbind('click').bind('click', function() {
        if ($("#frmSearchConfig").valid()) {
            $("#myModalSaveSearch").modal("show");
        }
    });
    this.proceed = function() {
        if (searchViewMode === 'indexer') {
            var indexer = getsolrIndexer(null, null, 'Config');
            indexer.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
            indexer.initConfig();
            $('#myProfilesConfig').fadeOut('slow');
            $('#profileConfig').fadeIn('slow');
            $('#profileEdit').fadeIn('slow');

        } else if (searchViewMode === 'configurator') {
            var configurator = getConfigurator(null, null, 'Config');
            configurator.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
            configurator.initConfig();
            $('#myProfilesConfig').fadeOut('slow');
            $('#profileConfig').fadeIn('slow');
            $('#profileEdit').fadeIn('slow');
        }
    };
    this.Load = function() {
        searchViewMode = (location.hash.replace(/^#/, '') === "solrIndexer") ? 'indexer' : (location.hash.replace(/^#/, '') === "queryBuilder") ? 'queryBuilder' : 'configurator';
        if (searchViewMode === 'configurator' || searchViewMode === 'queryBuilder') {
            getMyProfiles();
            return;
        }
        var sourceData = [{
            "id": "hive",
            "text": "HIVE"
        }, {
            "id": "postgreSQL",
            "text": "PostgreSQL"
        }];
        $("#source").select2({
            placeholder: "Choose Drilldow Source",
            data: sourceData
        }).on("change", function(e) {
            sourceSelect(e.val, 'database');
        });
        $("#searchType").select2({
            placeholder: "Choose Search Type",
            data: [{ 'id': 'facetsearch', 'text': 'Facet Search' }, { 'id': 'freeformsearch', 'text': 'Free Form Search' }]
        });
        synchronousLoad(null);
    };

    var synchronousLoad = function(loadedDatabase) {
        var databases = ['solr', 'hive', 'postgreSQL'];
        if (!loadedDatabase) {
            getDataBaseList(databases[0]);
        }
        if (databases.indexOf(loadedDatabase) < databases.length - 1) {
            getDataBaseList(databases[databases.indexOf(loadedDatabase) + 1]);
        }
        if (databases.indexOf(loadedDatabase) >= databases.length - 1) {
            getMyProfiles();
        }
    }

    var sourceSelect = function(source, selectId) {
        var databaseList = [];
        if (This.dataBaseList && This.dataBaseList[source] && This.dataBaseList[source].length > 0) {
            for (var i = 0; i < This.dataBaseList[source].length; i++) {
                databaseList.push({ "id": This.dataBaseList[source][i], "text": This.dataBaseList[source][i] });
            }
        }

        $("#" + selectId).select2({
            placeholder: selectId == 'solrSchema' ? "Choose Solar Schema" : "Choose Drilldown Database",
            data: databaseList
        });
    }
    var getDataBaseList = function(source) {
        var request = {};
        request.source = source;
        enableLoading();
        callAjaxService("getDatabaseList", function(response) { callBackGetDatabaseList(response, source) },
            callBackFailure, request, "POST");
    }
    var callBackGetDatabaseList = function(response, source) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        if (response !== undefined && response !== null) {
            This.dataBaseList[source] = response;
        }
        if (source == 'solr') {
            sourceSelect('solr', 'solrSchema');
        }
        synchronousLoad(source);
    };
    
    var callBackGetMyprofiles = function() { 
    	  var requiredCol = [ {
      		"sWidth": '13%',
      		"mDataProp" : "displayname",
      		"title":"Name",
      		"sDefaultContent" : ""
      	},  {
      		"mDataProp" : "source",
      		"sWidth": '13%',
      		"title":"Source",
      		"sDefaultContent" : ""
      	}, {
      		"mDataProp" : "database",
      		"sWidth": '13%',
      		"title":"Data Base",
      		"sDefaultContent" : ""
      	}, {
      		"mDataProp" : "lastmodifiedby",
      		"sWidth": '12%',
      		"title":"Modified By",
      		"sDefaultContent" : ""
      	}, {
      		"mDataProp" : "lastmodifiedon",
      		"sWidth": '13%',
      		"title":"Modified On",
      		"sDefaultContent" : ""
      	}];
      	var profileParam = {
      			"getAll" : true,
      			"profileType" : "search",
      	        "columns" : requiredCol,
      	        "isPublish" : true
      	        
      	};
      	
      	//enableLoading();
      	var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){getProfileConfig(e,this);});
      	profileUtitlyObj.getMyProfile();
    }
    var getMyProfiles = function(obj) {
        var id = $(obj).data("refid");
        var request = {
            "type": "search",
            "id" : id
        };
        enableLoading();
        callAjaxService("getMyProfiles", callBackGetMyProfiles,
            callBackFailure, request, "POST");
    }
    var callBackGetMyProfiles = function(response) { 

  
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
       if (response !== undefined && response !== null) {
            $("#myProfileList").empty();
            for (var i = 0; i < response.length; i++) {
                if (searchViewMode === 'indexer' && response[i].fact !== 'searchIndexer') {
                    continue;
                } else if (searchViewMode === 'configurator' && response[i].fact !== 'search') {
                    continue;
                } else if (searchViewMode === 'queryBuilder' && response[i].fact !== 'queryBuilder') {
                    continue;
                }
                var publishStatus = (searchViewMode === 'queryBuilder') ? '' : "Publish";
                var displayStatus = '';
                if(publishStatus === '') {
                    displayStatus = 'none';
                }
                var btnClass = "btn-outline";
                var actionId = 0;
                var canPublish = true;
                if (response[i].profilekey && response[i].profilekey != null && response[i].profilekey != "") {
                    publishStatus = "Published";
                    btnClass = "btn-outline-success";
                    actionId = response[i].actionid;
                }
                $("#myProfileList").append($("<li/>").unbind("click").bind("click", response[i], function(e) { 
                    // $("#myProfilesConfig").show(300);
                    // $("#myProfiles").hide(300);
                    // $('#database').select2('val', e.data.database);
                    // $('#searchType').select2('val', e.data.configtable);
                    // $('#profilename').val(e.data.displayname);
                    // $('#description').val(e.data.remarks);
                    This.currentId = e.data.id;

                    if (searchViewMode === 'indexer') {
                        var indexer = getsolrIndexer(null, { id: e.data.id }, 'id');
                        //indexer.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
                        indexer.initConfig();
                        $("#myprofiles").hide(300);
                        $('#myProfilesConfig').fadeOut('slow');
                        $('#profileConfig').fadeIn('slow');
                        $('#profileEdit').fadeIn('slow');

                    } else if (searchViewMode === 'configurator') {
                        var configurator = getConfigurator(null, { id: e.data.id }, 'id');
                        //configurator.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
                        configurator.initConfig(e.data.id);
                        $("#myprofiles").hide(300);
                        $("#myProfilesConfig").show(300);
                        $("#profileConfig").show(300);
                    } else if (searchViewMode === 'queryBuilder') {
                        var builder = getQueryBuilder(null, { id: e.data.id }, 'id');
                        //configurator.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
                        builder.initConfig(e.data.id);
                        $("#myprofiles").hide(300);   
                        $("#myProfilesConfig").show(300);                     
                        $("#querybuilder").show(300);
                    }

                }).addClass("list-group-item").data({ "id": response[i].id }).append(
                    $("<a/>")
                    .append($("<span/>").addClass("text-left col-md-2").html(response[i].displayname))
                    .append($("<span/>").addClass("text-left col-md-4").html(response[i].database))
                    .append($("<span/>").addClass("text-left col-md-2").html(response[i].configtable))
                    .append($("<span/>").addClass("text-left col-md-3").html(response[i].remarks))
                    .append($("<button/>").addClass(btnClass + " col-md-1").css('display', displayStatus).html(publishStatus).attr({
                        'data-target': '#profilePublishModal',
                        'data-toggle': 'modal',

                    }).unbind('click').bind('click', response[i], function(e) {
                        e.stopPropagation();
                        previewPublishProfile(e.data, this);
                    }))
                ));
            }
        }
        if (searchViewMode === 'configurator') {
            var configurator = getConfigurator(null, null, 'Config');
            configurator.initConfig();
        } else if (searchViewMode === 'queryBuilder') {
            var builder = getQueryBuilder(null, null, 'Config');
            builder.initConfig();
        }
      
    }

    var getTableNames = function() {
        var request = {
            "source": $('#source').val(),
            "database": $("#database").select2('val')
        };
        enableLoading();
        callAjaxService("getTableList", callBackGetTableNames, callBackFailure, request, "POST");
    }
    var callBackGetTableNames = function(response) {
        disableLoading();
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        if (response && response.length > 0) {
            This.dataBaseList['tbl'] = response;
            sourceSelect('tbl', 'searchTable');
        }
    }


    var getProfileConfig = function(obj) {
        var id = $(obj).data("refid");
        var request = {
          //  "profileKey": profilekey
        	  "id":id
            
        };
        enableLoading();
        callAjaxService("getProfileConfig", callBackGetProfileConfig,
            callBackFailure, request, "POST");
    }
   
    var callBackGetProfileConfig = function(response) {
       /* if (response && response[0]) {
            sourceSelect(response[0].source, 'database');
            $('#source').select2('val', response[0].source);
            $('#database').select2('val', response[0].config);
            $('#solrSchema').select2('val', response[0].database);
            $('#searchType').select2('val', response[0].configtable);
            $('#profilename').val(response[0].displayname);
            $('#description').val(response[0].remarks);
            // $("#database").unbind('change').bind('change', getColumnNames);
        }*/
    if (response !== undefined && response !== null) {
       // $("#myProfileList").empty();
       // for (var i = 0; i < response.length; i++) {
          /*  if (searchViewMode === 'indexer' && response[i].fact !== 'searchIndexer') {
                continue;
            } else if (searchViewMode === 'configurator' && response[i].fact !== 'search') {
                continue;
            } else if (searchViewMode === 'queryBuilder' && response[i].fact !== 'queryBuilder') {
                continue;
            }
            var publishStatus = (searchViewMode === 'queryBuilder') ? '' : "Publish";
            var displayStatus = '';
            if(publishStatus === '') {
                displayStatus = 'none';
            }
            var btnClass = "btn-outline";
            var actionId = 0;
            var canPublish = true;
            if (response[i].profilekey && response[i].profilekey != null && response[i].profilekey != "") {
                publishStatus = "Published";
                btnClass = "btn-outline-success";
                actionId = response[i].actionid;
            }
            //$("#myProfileList").append($("<li/>").unbind("click").bind("click", response[i], function(e) { 
*/                // $("#myProfilesConfig").show(300);
                // $("#myProfiles").hide(300);
                // $('#database').select2('val', e.data.database);
                // $('#searchType').select2('val', e.data.configtable);
                // $('#profilename').val(e.data.displayname);
                // $('#description').val(e.data.remarks);
              var data =JSON.parse(response["config"])[0]
                This.currentId = data.id;

                if (searchViewMode === 'indexer') {
                    var indexer = getsolrIndexer(null, { id: data.id }, 'id');
                    //indexer.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
                    indexer.initConfig();
                    $("#myProfiles").hide(300);
                    $('#myProfilesConfig').fadeOut('slow');
                    $('#profileConfig').fadeIn('slow');
                    $('#profileEdit').fadeIn('slow');

                } else if (searchViewMode === 'configurator') {
                    var configurator = getConfigurator(null, { id: data.id }, 'id');
                    //configurator.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
                    configurator.initConfig(data.id);
                    $("#myprofiles").hide(300);
                    $("#myProfilesConfig").show(300);
                    $("#profileConfig").show(300);
                } else if (searchViewMode === 'queryBuilder') {
                    var builder = getQueryBuilder(null, { id: data.id }, 'id');
                    //configurator.searchSetting($('#solrSchema').val(), $('#searchType').val(), $('#source').val(), $('#database').val(), $('#searchTable').val());
                    builder.initConfig(data.id);
                    $("#myProfiles").hide(300);   
                    $("#myProfilesConfig").show(300);                     
                    $("#querybuilder").show(300);
                }

                if (response && response[0]) {
                    sourceSelect(response[0].source, 'database');
                    $('#source').select2('val', data.source);
                    $('#database').select2('val', data.config);
                    $('#solrSchema').select2('val', data.database);
                    $('#searchType').select2('val', data.configtable);
                    $('#profilename').val(data.displayname);
                    $('#description').val(data.remarks);
                    // $("#database").unbind('change').bind('change', getColumnNames);
                }
           
        //}
    }
    /*if (searchViewMode === 'configurator') {
        var configurator = getConfigurator(null, null, 'Config');
        configurator.initConfig();
    } else if (searchViewMode === 'queryBuilder') {
        var builder = getQueryBuilder(null, null, 'Config');
        builder.initConfig();
    }*/

    };
    var publishProfile = function(data) {
        //action,actionId,refid
        console.log(data);
        if (data.aactionid) {

        }
    };
    this.save = function() {
        if (!$('#profilename').val()) {
            showNotification("error", 'Enter profile name');
            return;
        }
        enableLoading();
        var request = {
            source: $('#source').val(),
            database: $("#solrSchema").select2('val'),
            type: 'search',
            config: $("#database").select2('val'),
            table: $('#searchType').select2('val'),
            displayname: $('#profilename').val(),
            remarks: $('#description').val()
        };
        if (This.currentId) {
            request.id = This.currentId;
        }
        callAjaxService("saveProfile", callBackSaveProfile,
            callBackFailure, request, "POST");
    };
    var callBackSaveProfile = function(response) {
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        } else {
            showNotification("success", 'Search saved succesfully.');
            getMyProfiles();
        }
    };
    var previewPublishProfile = function(data, obj) {
        $("#userEnabled").removeAttr("checked");
        $("#smart-form-register input").removeAttr("readonly");
        if ($(obj).html() != "Publish") {
            publishedActiondetails(data, obj);
        } else {
            var publishDataObj = new Object();
            publishDataObj.action = data.displayname.replace(/ /g, '');
            publishDataObj.displayname = data.displayname;
            publishDataObj.refid = data.id;
            publishDataObj.source = "";
            //      publishDataObj.enabled = 0;

            var callBackPublishProfile = function(responseData) {
                $(obj).html("Published").removeClass("btn-outline").addClass("btn-outline-success");
                $(obj).attr("data-action", data.displayname.replace(/ /g, ''));
                $(obj).attr("data-actionid", responseData.actionId);
            };

            $("#smart-form-register input").val("");
            $("#frmSankeyFilter em").remove();
            $(".state-error").removeClass("state-error");

            templates.managePublishing(publishDataObj, "CREATE", callBackPublishProfile);
        }
    };

    var publishedActiondetails = function(data, obj) {
        enableLoading();

        var callbackSucessGetAction = function(response) {
            if (response && response.isException) {
                showNotification("error", response.customMessage);
                return;
            } else {
                if (response == null) {
                    disableLoading();
                    alert("Selected Profile is Published as Tab");
                    return;
                }

                response.refid = data.id;
                response.actionId = Number(data.actionid) || Number($(obj).attr("data-actionid"));
                templates.managePublishing(response, "VIEW", undefined);
            }
        };

        var request = {
            "action": data.profilekey || $(obj).attr("data-action")
        };
        callAjaxService("getAction",
            function(response) { callbackSucessGetAction(response); }, callBackFailure, request, "POST");
    };

    This.Load();
})();
