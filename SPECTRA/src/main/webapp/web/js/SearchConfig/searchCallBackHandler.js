/**8
 * Specifically used to get the callbacks from Search Results.
 * Utilize Modular Approach to achieve the separate handler. 
 * The common behaviors are to be placed in commonActionHandler.
 */

var searchActionHandler = function() {
	var THIS = this;
	var integraBomIds = [];
	var globalVar = {};
	THIS.load = function(preEvalStr) {
		localStorage.setItem("searchLocation", window.location.hash);
		if(preEvalStr) {
			try{
				eval(preEvalStr);
			} catch(e) {
				console.log(e);
			}
		}
	}
	
	THIS.prepareElevateZoom = function () {
   	 $('.zoomContainer').remove();
   	 $(".elevateZoom-window").elevateZoom({
            zoomWindowPosition: 2,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            tint: true,
            tintColour: 'red',
            tintOpacity: 0.3,
            minZoomLevel: 0.3
        });
        $(".elevateZoom-lens").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lensSize: 150,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            minZoomLevel: 0.05,
            /*tint : true,
            tintColour : 'red',
            tintOpacity : 0.3,
            minZoomLevel:0.3*/
        });
    }
	
	THIS.prepareSlick = function () {
         $(".slick_regular").not('.slick-initialized').slick({
	            dots: false,
	            infinite: false,
	            slidesToShow: 1,
	            slidesToScroll: 1,
	            prevArrow:'<button type="button" class="slick-prev prev-slick"></button>',
	            nextArrow:'<button type="button" class="slick-next next-slick"></button>'
	     });
         $('.prev-slick').on('click', function(){
        	 prepareElevateZoom();
         });
         $('.next-slick').on('click', function(){
        	 prepareElevateZoom();
         });
     }
	
	THIS.popUpGenericModal = function(data, title, modalObject, isModalLookUp, isElevateZoom, isSlick) {
		if(isModalLookUp) {
			modalObject = lookUpTable.get(modalObject);
		}
		$('.zoomContainer').remove();
		var res = templates.getResultView(modalObject, { serchResponse: data });
		var str = '<div aria-hidden="true" aria-labelledby="myModalLabel" class="searchConfigDrillDownModal modal fade in" id="searchConfigDrillDownModal" role="dialog" style="display: block; padding-right: 0px;" tabindex="-1">';
		str += '<div class="modal-dialog" style="width: 90%">';
		 str += '<div class="modal-content" id="printView">';
		   str += '<div class="modal-header modal-header-small">';
		     str += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>';
		     str += '<h4 class="modal-title" id="myModalLabel">'+title+'</h4>';
		   str += '</div>';
		   str += '<div class="modal-body">';
		    str += '<div class="poDocView">';
		     str += '<div class="row">';
		
		     str += '</div>';
		    str += '</div>';
		   str += '</div>';
		  str += '</div>';
		 str += '</div>';
		str += '</div>';
		$('#temp-view-construct-div').html(str);
		$('.poDocView').html(res);
		$('.poDocView').find('table').addClass('table');
		$('.searchConfigDrillDownModal').modal('show');
		$('.zoomContainer').remove();
//		if(isModalLookUp && isModalLookUp === 'true') {
//			THIS.prepareElevateZoom();
//		}
//		if(isSlick && isSlick === 'true') {
//			THIS.prepareSlick();
//		}
	}
	
	THIS.load();
}
var searchActionCallbackHandler = new searchActionHandler();