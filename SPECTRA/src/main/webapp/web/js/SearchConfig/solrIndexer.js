 var solrIndexConfig = {};
 var getsolrIndexer = function(cfg, obj, type) {
     //if (!!solrIndexConfig) {
     solrIndexConfig = new solrIndexer(cfg, obj, type);
     //}
     return solrIndexConfig;
 }

 var solrIndexer = function(cfg, obj, type) {

     var searchConfigTemplate = {
         title: '',
         advancedSearchs: [],
         data: [],
         dataViewConfig: {
             grid: false,
             card: false,
             content: false
         },
         defaultView: 'card',
         dataButtons: {
             edit: false,
             expBtn: false,
             info: false,
             compare: false,
             shortList: false,
             print: false,
             mail: false,
             download: false,
             obsolete: false
         },
         filters: {},
         findAndReplace: false,
         sortData: [],
         paginationStyle: 0,
         searchSettings: {
             schema: '',
             searchType: '',
             searchSource: '',
             searchDatabase: '',
             searchTable: ''
         },
         entities: [],
         searchFields: {},
         searchTypes: {},
         displayQuery: '',
         indexConfigObj: {},
         solrConfigFieldMap: {},
         key: '',
         description: '',
         id: 0
     };
     
     var filterCount = 0;
     var filterTemplate ={
    		 id: filterCount,
    		 field: '',
    		 displayName: '',
    		 type: '',
    		 defaultValue: '',
    		 lookUpQuery: '',
    		 invisible: false,
    		 mandatory: false,
    		 selectType: '',
    		 searchType: '',
    		 showType: ''
     };
     
     var searchConfigObject = {};
     var solrConfigFieldNameMap = {};
     var config, data;
     var searchTypes = [{id: 'in', text: 'In'}, {id: 'not in', text: 'Not In'}, {id: 'like', text: 'Like'}, {id: 'not like', text: 'Not Like'}];
     var selectTypes = [{id: 'multiple', text: 'Multiple'}, {id: 'single', text: 'Single'}];
     var solrFieldTypes = [{ id: 'string', text: 'string', index: 0 }, { id: 'number', text: 'number', index: 0 }, { id: 'float', text: 'float', index: 0 }, { id: 'date', text: 'Date', index: 0 }];
     var filterColumnType = [{ id: 'string', text: 'string' }, { id: 'int', text: 'number' }, { id: 'boolean', text: 'boolean' }, { id: 'boolean', text: 'boolean' }, ];
     var columns, solrColumns, columnsMap, solrConfigFieldMap, copyFields;
     var defaultSearchField = '',
         uniqueKeyField = '';
     var mode = (type && type !== '') ? type : 'View'; //Config , Preview, id
     var searchObj = obj;
     var searchData = [];     
     var filters = [];
     
     activate();

     function activate() {
         if (!!!cfg) {
             searchConfigObject = JSON.parse(JSON.stringify(searchConfigTemplate)); //Deep Copy             
         } else {
             searchConfigObject = JSON.parse(JSON.stringify(cfg)); //Deep Copy....
         }
         $('#searchBack').unbind('click').bind('click', function() {
             clearView();
             $('#myProfilesConfig').fadeIn('slow');
             $('#profileConfig').fadeOut('slow');
             $('#profileEdit').fadeOut('slow');
         });
     }

     function clearView() {
         solrIndexConfig = null;
         $('#advancedconfig-area').empty();
     }

     function getProfileById(id) {
         var request = {
             "id": id
         };
         enableLoading();
         callAjaxService("getProfileConfig", callBackGetProfileById, callBackFailure, request, "POST");
     }

     function callBackGetProfileById(response) {
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             disableLoading();
             return;
         }
         if (response) {
             if (response.config) {
                 var search = JSON.parse(response.config);
                 if (search.length > 0) {
                     search = search[0];
                 }
                 if (search && search.config) {
                     searchConfigObject = JSON.parse(search.config);
                     searchConfigObject.id = searchObj.id;
                     $('#profilename').val(searchConfigObject.title);
                     $('#description').val(searchConfigObject.description);
                     for (var i in searchConfigObject.solrConfigFieldMap) {
                         solrConfigFieldNameMap[searchConfigObject.solrConfigFieldMap[i].solrFieldName] = searchConfigObject.solrConfigFieldMap[i]
                     }
                     getSolrColumnList();
                 }
             }
         }
         disableLoading();
     }

     function getColumnList() {
         var request = {
             "source": searchConfigObject.searchSettings.schema,
             "database": searchConfigObject.searchSettings.searchDatabase,
             "table": searchConfigObject.searchSettings.searchTable
         };
         enableLoading();
         callAjaxService("getColumnList", callBackGetColumnNames, callBackFailure, request, "POST");
     }

     function callBackGetColumnNames(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         if (response && response.length > 0) {
             columns = response;
             columnsMap = [];
             for (var i = 0; i < columns.length; i += 1) {
                 var obj = { id: '', text: '', index: i };
                 obj.id = columns[i].columnName;
                 obj.text = columns[i].columnName;
                 columnsMap.push(obj);
             }
         }
         buildFieldMapTable();
     }

     function getSolrColumnList() {
         enableLoading();
         var request = { solrCollectionName: searchConfigObject.searchSettings.schema };
         callAjaxService("getSolrSchema", callBackGetSolrColumnList, callBackFailure, request, "POST");
     }

     function callBackGetSolrColumnList(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         if (response) {
             solrColumns = [];
             for (var i = 0; i < response.length; i += 1) {
                 if (copyFields && copyFields.length > 0 && copyFields.indexOf(response.schema.fields[i].name) !== -1) {
                     continue;
                 }
                 var obj = { id: '', text: '', index: i };
                 obj.id = response[i].Name;
                 obj.text = response[i].Name;
                 solrColumns.push(obj);
             }
         }
         getColumnList();
     }

     function buildFieldMapTable() {
         var str = '<div class="solrConfigDiv"><table class="table table-striped solrConfigTable">';
         str += '<thead>';
         str += '<tr> <td>Solr Field</td> <td>Database Field</td> <td>Solr Field Type</td> </tr>';
         str += '</thead>';
         str += '<tbody>';
         solrConfigFieldMap = {};
         if (!solrColumns) {
             return;
         }
         for (var i = 0; i < solrColumns.length; i += 1) {

             var curObj = solrColumns[i];
             var curFieldValue = '';
             var curTypeValue = 'string';
             if (solrConfigFieldNameMap[curObj.text]) {
                 curObj = solrConfigFieldNameMap[curObj.text];
                 curFieldValue = curObj.dbFieldName;
                 curTypeValue = curObj.solrFieldType;
             }
             solrConfigFieldMap[i] = { solrFieldName: solrColumns[i].text, dbFieldName: curFieldValue, solrFieldType: curTypeValue };
             str += '<tr>';
             str += '<td><label>' + solrColumns[i].text + '</label></td>'; //<input type="text" class="solrConfigTableText" data-type="solrFieldName" value="' + solrColumns[i].text + '" data-index="' + solrColumns[i].index + '" />
             str += '<td><input data-type="dbFieldName"  data-index="' + solrColumns[i].index + '" value="' + curFieldValue + '" type="text" class="solrConfigTableSelect solrConfigTableDbName" /></td>';
             str += '<td><input data-type="solrFieldType"  data-index="' + solrColumns[i].index + '" value="' + curTypeValue + '" type="text" class="solrConfigTableSelect solrConfigTableField"  /></td>';
             str += '</tr>';
         }
         str += '</tbody></table>';
         // str += '<div class="row">';
         // str += '<div class="col-sm-3"> <strong> Unique Key: </strong> </div><div class="col-sm-3"> <input style="width: 100%" type="text" class="solrConfigUKey" value="' + uniqueKeyField + '" /></div>';
         // str += '<div class="col-sm-3"> <strong> Default Search Field: </strong> </div><div class="col-sm-3"> <input style="width: 100%" type="text" class="solrConfigDKey" value="' + defaultSearchField + '" /></div>';
         // str += '</div>';
         
         $('#advancedconfig-area').html(str);
         str = '<div class="row">';
         str += '<div class="col-sm-5 pull-right"> <button class="btn btn-primary solrConfigSaveButton">Save Configuration</button> <button class="btn btn-primary solrConfigIndexButton">Solr Index</button> </div>';
         str += '</div>';
         $('#filter-btn-cfg').html(str);
         
         $('.solrConfigTable .solrConfigTableDbName').each(function() {
             var value = $(this).val();
             $(this).select2({
                 placeholder: 'ColumnName',
                 data: columnsMap,
                 allowClear: true
             }).on("change", function(e) {
                 if (e.target && $(e.target).length > 0) {
                     var data = $(e.target);
                     if (solrConfigFieldMap[data.attr('data-index')]) {
                         solrConfigFieldMap[data.attr('data-index')].dbFieldName = data.val();
                     }
//                     console.log(solrConfigFieldMap[data.attr('data-index')]);
                 }
             }).select2('val', value);
         });
        
         var value = '';
         if(searchConfigObject.indexConfigObj && searchConfigObject.indexConfigObj.filterColumn && searchConfigObject.indexConfigObj.filterColumn.columnName) {
        	 value = searchConfigObject.indexConfigObj.filterColumn.columnName;
         }
         $('#filter-column').select2({
             placeholder: 'ColumnName',
             data: columnsMap,
             allowClear: true
         }).select2('val', value);
         
         value = 'string';
         if(searchConfigObject.indexConfigObj && searchConfigObject.indexConfigObj.filterColumn && searchConfigObject.indexConfigObj.filterColumn.columnType) {
        	 value = searchConfigObject.indexConfigObj.filterColumn.columnType;
         }
         $('#filter-column-type').select2({
             placeholder: 'FieldType',
             data: filterColumnType
         }).select2('val', value);
         
         if(searchConfigObject.indexConfigObj && searchConfigObject.indexConfigObj.filterColumn && searchConfigObject.indexConfigObj.filterColumn.columnValue) {
        	 $('#filter-column-value').val(searchConfigObject.indexConfigObj.filterColumn.columnValue);
         }
         
         $('.solrConfigTable .solrConfigTableField').each(function() {
             var value = $(this).val();
             $(this).select2({
                 placeholder: 'FieldType',
                 data: solrFieldTypes
             }).on("change", function(e) {
                 if (e.target && $(e.target).length > 0) {
                     var data = $(e.target);
                     if (solrConfigFieldMap[data.attr('data-index')]) {
                         solrConfigFieldMap[data.attr('data-index')].solrFieldType = data.val();
                     }
                 }
             }).select2('val', value)
         })

         // $('.solrConfigTable .solrConfigTableText').on('keyup', function() {
         //     solrConfigFieldMap[$(this).attr('data-index')].solrFieldType = $(this).val();
         // });

         $('.solrConfigUKey').select2({
             placeholder: 'Unique Key',
             data: solrColumns,
             val: uniqueKeyField
         }).on("change", function(e) {
             if (e.target && $(e.target).length > 0) {
                 var data = $(e.target);
                 uniqueKeyField = data.val();
             }
         }).select2("val", uniqueKeyField);
         $('.solrConfigDKey').select2({
             placeholder: 'Search Field',
             data: solrColumns,
             val: defaultSearchField
         }).on("change", function(e) {
             if (e.target && $(e.target).length > 0) {
                 var data = $(e.target);
                 defaultSearchField = data.val();
             }
         }).select2("val", defaultSearchField);

         $('.solrConfigIndexButton').unbind('click').bind('click', function() {
             solrIndex();
         });
         $('.solrConfigSaveButton').unbind('click').bind('click', function() {
             if (mode !== 'view') {
                 $('#myModalSaveSearch').modal('show');
             } else {
                 saveIndex();
             }
         });
         $('#btnSaveSearch').unbind('click').bind('click', saveIndex);
     }
     
     function appendFilterColumn() {
    	 var str = '<div class="well col-sm-12 col-md-12 margin-bottom-10 padding-bottom-10">';
    	 str += '<div class="col col-2"><div class="note padding-2"><strong>Field</strong></div>';
    	 str += '<input type="text" class="width-100per filter-column-'+filterCount+'" data-index="'+filterCount+'" />';
    	 str += '<div class="col col-2"><div class="note padding-2"><strong>Display Name</strong></div>';
    	 str += '</div>';
    	 
    	 $('.filter-column' + filterCount).select2({
             placeholder: 'ColumnName',
             data: columnsMap,
             allowClear: true
         }).select2('val', value);
    	 
    	 filterCount += 1;
     }
     
     function solrIndex() {
         if (mode !== 'view') {
             if (!$('#profilename').val()) {
                 showNotification("error", 'Enter profile name');
                 return;
             }
             searchConfigObject.title = $('#profilename').val();
             searchConfigObject.description = $('#description').val();
         }
         if (searchConfigObject.title.trim() === '') {
             return;
         }
         var solrObj = { confList: [], filterColumn: {} };
         var isValid = true;
         var configObj = {
             name: searchConfigObject.searchSettings.searchTable,
             keywords: '',
             source: '',
             solrCollection: searchConfigObject.searchSettings.schema,
             dbName: searchConfigObject.searchSettings.searchDatabase,
             dbType: searchConfigObject.searchSettings.searchSource,
             columns: []
         };
         for (var i in solrConfigFieldMap) {
             var obj = { name: '', index: '', type: '' };
             if (solrConfigFieldMap[i].solrFieldName.toLowerCase() === '_version_') {
                 continue;
             }
             obj.name = solrConfigFieldMap[i].solrFieldName;
             obj.index = solrConfigFieldMap[i].dbFieldName;
             obj.type = solrConfigFieldMap[i].solrFieldType;
             // if (solrConfigFieldMap[i].dbFieldName.trim() === '') {
             //     showNotification('error', 'Please Map all the fields');
             //     isValid = false;
             //     break;
             // }
             configObj.columns.push(obj);
         }
         if (!isValid) {
             return;
         }
         solrObj.confList.push(configObj);
         solrObj.filterColumn['columnName'] = $('#filter-column').val();
         solrObj.filterColumn['columnType'] = $('#filter-column-type').val();
         solrObj.filterColumn['columnValue'] = $('#filter-column-value').val();
         
         var reqest = { config: JSON.stringify(solrObj), filterColumn: JSON.stringify(solrObj.filterColumn) }
         searchConfigObject.indexConfigObj = JSON.parse(JSON.stringify(solrObj)); //Deep Copy
         searchConfigObject.solrConfigFieldMap = JSON.parse(JSON.stringify(solrConfigFieldMap));
         // console.log(solrObj);
         // console.log(reqest);
         enableLoading();
         callAjaxService("indexSolr", callBackSolrIndex, callBackFailure, reqest, "POST");
     }

     function callBackSolrIndex(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         showNotification("success", "Solr Indexed Successfully");
         disableLoading();
     }

     function saveIndex() {
         disableLoading();
         var solrObj = { confList: [], filterColumn: {} };
         var configObj = {
                 name: searchConfigObject.searchSettings.searchTable,
                 keywords: '',
                 source: '',
                 solrCollection: searchConfigObject.searchSettings.schema,
                 dbName: searchConfigObject.searchSettings.searchDatabase,
                 dbType: searchConfigObject.searchSettings.searchSource,
                 columns: []
             };
         for (var i in solrConfigFieldMap) {
             var obj = { name: '', index: '', type: '' };
             if (solrConfigFieldMap[i].solrFieldName.toLowerCase() === '_version_') {
                 continue;
             }
             obj.name = solrConfigFieldMap[i].solrFieldName;
             obj.index = solrConfigFieldMap[i].dbFieldName;
             obj.type = solrConfigFieldMap[i].solrFieldType;
             // if (solrConfigFieldMap[i].dbFieldName.trim() === '') {
             //     showNotification('error', 'Please Map all the fields');
             //     isValid = false;
             //     break;
             // }
             configObj.columns.push(obj);
         }
         solrObj.confList.push(configObj);
         solrObj.filterColumn['columnName'] = $('#filter-column').val();
         solrObj.filterColumn['columnType'] = $('#filter-column-type').val();
         solrObj.filterColumn['columnValue'] = $('#filter-column-value').val();
         searchConfigObject.indexConfigObj = JSON.parse(JSON.stringify(solrObj)); //Deep Copy
         searchConfigObject.solrConfigFieldMap = JSON.parse(JSON.stringify(solrConfigFieldMap));
         searchConfigObject.title = $('#profilename').val();
         searchConfigObject.description = $('#description').val();
         var request = {
             "source": searchConfigObject.searchSettings.searchTable,
             "database": searchConfigObject.searchSettings.searchDatabase,
             "table": searchConfigObject.searchSettings.searchSource,
             "fact": 'searchIndexer',
             "customfacts": '',
             "config": JSON.stringify(searchConfigObject),
             "filters": JSON.stringify(searchConfigObject.indexConfigObj),
             "summary": JSON.stringify(searchConfigObject.solrConfigFieldMap),
             "remarks": searchConfigObject.description,
             "displayname": searchConfigObject.title,
             "additionalconfig": '',//searchConfigObject.searchSettings.schema,
             "type": "search",
             "id": searchConfigObject.id
         };
         enableLoading();
         callAjaxService("saveProfile", callBackSaveIndex, callBackFailure, request, "POST");
     }

     function callBackSaveIndex(response) {
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         showNotification("success", "Solr Profile Saved Successfully");
         $('#myModalSaveSearch').modal('hide');
     }

     return {
         searchSetting: function(schema, searchType, searchSource, searchDatabase, searchTable) {
             try {
                 if (!!schema) {
                     searchConfigObject.searchSettings.schema = schema;
                 }
                 if (!!searchType) {
                     searchConfigObject.searchSettings.searchType = searchType;
                 }
                 if (!!searchSource) {
                     searchConfigObject.searchSettings.searchSource = searchSource;
                 }
                 if (!!searchDatabase) {
                     searchConfigObject.searchSettings.searchDatabase = searchDatabase;
                 }
                 if (!!searchTable) {
                     searchConfigObject.searchSettings.searchTable = searchTable;
                 }
             } catch (e) {
                 //Intentionally do nothing
             }
             return searchConfigObject.searchSettings;
         },
         initConfig: function() {
             $('#advancedconfig').fadeIn('slow');
             if (mode === 'id' && obj && obj.id) {
                 getProfileById(obj.id);
             } else {
                 getSolrColumnList();
             }
         },
         initView: function(id) {
             mode = 'view';
             $('#solrIndexer').fadeIn('slow');
             $('#solrIndexer-advancedconfig').fadeIn('slow');
             obj = { id: id };
             getProfileById(obj.id);
         }
     }
 }
