 var sCart = {};
 var getCart = function() {
     //if (!!searchConfig) {
	 sCart = new shoppingCart();
     //}
     return sCart;
 }

var shoppingCart = function() {

	var carts = [];	
    var compares = [];    
    var compareLimit = 5;
    var compareElement = '.compareBtn';
    var cartElement = '.cartBtn';
    var cartImageElement = '#cart-image-area';
    var cartArea = '#cartArea';
    var cartsIndexKeyMap = {};
    
	function activate() {
		var str = '<div class="scroller-bottom cartContainer"><a href="#cart" class="'+cartElement.replace('.', '')+'">Cart <i class="label label-info compare-cart-count">0</i></a><div>'
		$('#searchDivId').append(str);
		if(localStorage.getItem('searchUser') && localStorage.getItem('carts') && localStorage.getItem('searchUser') !== System.userName) {
			localStorage.removeItem('carts');
		} else {
			localStorage.setItem('searchUser', System.userName);
		}		  
		if(localStorage.getItem('carts')) {
			carts = JSON.parse(localStorage.getItem('carts'));
			if(carts.length > 0) {				
				$('.compare-cart-count').text(carts.length);
			}			
		}
		if(window.location.hash === '#cart') {
			displayCart();
		}
		$('#shoppigCartli').show();
		$('#shoppingCart').on('click', function(){
       	 window.location.hash = '#cart';
        });
		mapCartIndexes();
		if(carts.length === 0) {
			$(cartElement).parent().hide();
		}
		Handlebars.registerHelper('checkForCart', function(key) {
			if(cartsIndexKeyMap[key] !== null && typeof cartsIndexKeyMap[key] !== 'undefined') {				
				return true;
			}
			return false;
		});
		var links = '';
		carts.map(function(obj) {
			 if(obj.emaillink) {
				 links += "\n" +  obj.emaillink;
			 }
		});
		var completeHref = 'mailto:?body=' + links;
		$('.sendEmail').prop('href', encodeURI(completeHref));
		$('.sendEmail').prop('target', '_top');
		$('.sendEmail').hide();
		$('#myModalEmail').on('hidden.bs.modal', function () {
		   prepareElevateZoom();
		})
		$('.printElement').on('click', function(){
			printImageArea($("#print-cartArea"), false, "");
		});
	}
	
	function alterCartItem(obj, displayKeyValPair, pKey, animateImage, headding, emaillink, sendImageAttachment, event){
	   	 if(!carts) {
	   		carts = [];
	   	 }
	   	 //Data Structure Pattern : carts = [{obj: {}, displayVal: {}, animateImage: ''}]
	   	 
	   	 if(carts.length >= 5) {
	   		 showNotification('error', 'Cart Queue is full');
	   		 if(event) {
	   			var target = event.target;	   			
	   			if($(target).is(':checkbox') || $(target).is(':checked')) {
	   				$(target).prop('checked', false);
	   			}
	   		 }
	   		 return;
	   	 }
	   	 if(carts !== []) {
	   		 var cartObjs = carts.map(function(a){ return a.obj });
	   		 for(var i = 0; i< cartObjs.length; i+=1) {
	   			 if(cartObjs[i][pKey] && cartObjs[i][pKey] === obj[pKey]) {
	   				 carts.splice(i, 1);
	   				 //break;
	   				 if(carts.length === 0) {
	   					$(cartElement).parent().hide();
	   				 }
	   				 $('.compare-cart-count').text('' + carts.length);	   				 
	   				 return;
	   			 }
	   		 }
	   		 var cart = {
	   				 "obj": {},
	   				 "displayVal": {},
	   				 "pKey": "",
	   				 "animateImage": '',
	   				 "headding": headding,
	   				 "emaillink" : emaillink
	   		 }   		 	   		
	   		cart["obj"] = obj;
	   		cart["displayVal"] = displayKeyValPair;
	   		cart["pKey"] = pKey;
	   		cart["animateImage"] = animateImage;
	   		cart["headding"] = headding;
	   		cart["emaillink"] = emaillink;
	   		cart["sendImageAttachment"] = sendImageAttachment;
	   		
	   		carts.push(cart);	  
	   		localStorage.setItem('carts', JSON.stringify(carts));	   		
	   		$(cartElement).parent().show();
	   		$('.compare-cart-count').text('' + carts.length);
	   	 }    	  
	    }
	
    function displayCart() {   	 
   	 $(cartImageElement).empty();
   	 $(cartArea).empty();
   	 if(carts.length === 0) {
   		$(cartArea).html('<div class="alert alert-warning fade in col-md-12"><h3 class="no-margin">Cart is Empty</h3></i></div>');
   		$('#print-cartArea').html('<div class="alert alert-danger fade in col-md-12"><h3 class="no-margin">Cart is Empty</h3></i></div>');
   		$('.printElement').hide();
   		return;
   	  }
   	  var elementWidth = Math.floor(10/Math.floor(carts.length));
   	  $('.printElement').show();
   	  var str = '<div class="col-md-2 text-center"><div>';
   	  var strPrintTable = '<table border="1">';
   	  var printImages = [];
   	  
   	  $(cartImageElement).append(str);
   	  for(var i = 0; i < carts.length; i += 1 ){
   		//Base Line Elemnet
   		str = '<div class="col-md-'+elementWidth+' text-center">';
   			str += '<div class="pull-right"><a style="cursor: pointer; display: none;" class="action-btn" data-solrid="{{link1}}" id="shortlist0"><span class="fa fa-lg fa-fw fa-star-o margin-3 txt-color-darken"></span><span class="text-mouseover">Shortlist</span></a>';
   		    str += '<span style="cursor: pointer" onclick="sCart.removeCartItem(\''+i+'\');">X</span></div><br /><br />';
   		if(carts[i].animateImage && carts[i].animateImage !== '') {
   			str += '<img style="height: 250px;" src="'+carts[i].animateImage+'" class="img-thumbnail thumb elevateZoom elevateZoom-lens" data-zoom-image="'+carts[i].animateImage+'" onerror="System.imgError(this);" />';
//     		str += '<img src="'+carts[i].animateImage+'" onerror="System.imgError(this);" class="img-thumbnail"/>';
   			printImages.push('<img src="'+carts[i].animateImage+'" onerror="System.imgError(this);" style="width: 500px; height: 500px;" />');
     	}
   		$(cartImageElement).append(str);
   	  }
   	  if(carts.length > 0) {
   		strPrintTable += '<thead><tr>';
   		str = '<div class="col-md-2 text-center">';
   	    str += '<ul class="list-group text-center">';
   	    str += '<li style="text-align: left" class="list-group-item"><div style="width: 100%;word-break:break-all;"><strong>Name</strong></div></li>';
   	    strPrintTable += '<td>Name</td>';
   	    for(var j in carts[0].displayVal) {
   	    	 str += '<li style="text-align: left" class="list-group-item"><div style="width: 100%;word-break:break-all;"><strong>'+carts[0].displayVal[j]+'</strong></div></li>';
   	    	 strPrintTable += '<td>'+carts[0].displayVal[j]+'</td>';
   	   	}
   	    strPrintTable += '<td>Image</td></tr></thead>';
   	    str += '</ul>';
   	    str += '</div>';
   	    $(cartArea).append(str);
   	  }
   	
   	for(var i = 0; i < carts.length; i+=1) {
   	   strPrintTable += '<tr style="page-break-after:always;">'; 
   	   str = '<div class="col-md-'+elementWidth+' text-center">';
   		str += '<ul class="list-group text-center">';
   		var headding = (carts[i].headding && carts[i].headding !== '') ? carts[i].headding : '-';    		
   			str += '<li style="text-align: left" class="list-group-item"><div style="width: 100%;word-break:break-all;"><strong>'+ carts[i].headding +'</strong></div></li>';     
   			strPrintTable += '<td>'+carts[i].headding+'</td>';
   		for(var j in carts[i].displayVal) {
   			var display = (!carts[i].obj[j]) ? '-' : carts[i].obj[j];
    		 str += '<li style="text-align: left" class="list-group-item"><div style="width: 100%;word-break:break-all;">'+ display +'</div></li>';
    		 strPrintTable += '<td>'+display+'</td>';
    	}
   		strPrintTable += '<td>'+printImages[i]+'</td>';
   		str += '</ul>';
       str += '</div>';
       strPrintTable += '</tr>'; 
       $(cartArea).append(str);
   	}
   	  //$('#cart-print-image-area').html($("#cart-image-area").html());
   	  $('#print-cartArea').html(strPrintTable);
   	  prepareElevateZoom();	
    }
    
    function prepareElevateZoom() {
   	 $('.zoomContainer').remove();
   	 $('.zoomWindowContainer').remove();
//   	 $(".elevateZoom-window").elevateZoom({
//            zoomWindowPosition: 2,
//            //zoomWindowWidth : $("#content").width() * 8 / 12,
//            //zoomWindowHeight : $("#content").height() * 1 / 12,
//            scrollZoom: true,
//            tint: true,
//            tintColour: 'red',
//            tintOpacity: 0.3,
//            minZoomLevel: 0.3
//        });
        $(".elevateZoom-lens").elevateZoom({
            zoomType: "lens",
            lensShape: "round",
            lensSize: 150,
            //zoomWindowWidth : $("#content").width() * 8 / 12,
            //zoomWindowHeight : $("#content").height() * 1 / 12,
            scrollZoom: true,
            minZoomLevel: 0.05,
            /*tint : true,
            tintColour : 'red',
            tintOpacity : 0.3,
            minZoomLevel:0.3*/
        });
        
    }
    
	activate();
	
	function removeCartItem(index) {
		if(!index) {
			return;
		}
		carts.splice(index, 1);
		localStorage.setItem('carts', JSON.stringify(carts));
		mapCartIndexes();
		displayCart();
	}
	
	function mapCartIndexes() {
		var count = 0;
		carts.map(function(a){ 
			cartsIndexKeyMap[a.obj[a.pKey]] = count;
			count += 1;
			});
		
	}		
	
	function printImageArea(elements, append, delimiter) {
		if(elements && elements.length > 0){ 
			for(var i = 0; i<elements.length; i+=1) {
				var elem = elements[i];
				if(elem.nodeName.toLowerCase() === 'img') {
//					$(elem).css('width', '20em');
//					$(elem).css('height', '10px');
				}
				var domClone = elem.cloneNode(true);

			    var $printSection = document.getElementById("printSection");

			    if (!$printSection) {
			        $printSection = document.createElement("div");
			        $printSection.id = "printSection";
			        document.body.appendChild($printSection);
			    }

			    if (append !== true) {
			        $printSection.innerHTML = "";
			    }

			    else if (append === true) {
			        if (typeof (delimiter) === "string") {
			            $printSection.innerHTML += delimiter;
			        }
			        else if (typeof (delimiter) === "object") {
			            $printSection.appendChlid(delimiter);
			        }
			    }

			    $printSection.appendChild(domClone);
			}
			window.print();
		}		
	}
	
	return {
		alterCartItem: alterCartItem,
		removeCartItem: removeCartItem,
		printImageArea: printImageArea,
		getCartCount: function() {
			return carts.length;
		}
	}	
}
getCart(); 