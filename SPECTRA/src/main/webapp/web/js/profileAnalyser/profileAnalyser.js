var ProfileAnalyser = function() {
	$("#actionDiv").empty();
	var THIS = this;
	THIS.profile = {};
	THIS.allProfiles = {};
	THIS.dataTable = {};
	THIS.data = '';
	THIS.chartFilters = [];
	THIS.defaultFilterValue = {};
	var previousChart = [];
	THIS.filterFields = [];
	THIS.viewSettings = [];
	var isEdit =false;
	var profilejsfiles = {
		profile : "js/DataAnalyser/DataAnalyser.js",
		sankey : "js/DataAnalyser/SankeyGenerator.js"
	};
	$("#actionDiv").prepend(
			$("<ul/>").addClass("nav nav-tabs pull-right in no-border").attr("id", "myTab"));

	$("#drawProfiles,#myDashboardProfilesDiv").hide();
	$("#saveProfiles").unbind("click").bind("click", function() {
		saveProfile();
	});
	
	$("#myProfilesBack1").unbind("click").bind("click", function() {
		$("#myDashBoard").show(300);
		$("#myProfiles").hide(300);
	});
	
	$("#offFilterMap").unbind("click").bind("click", function() {
		previewProfile("noFilterMap");
	});
	$("#previewProfiles").unbind("click").bind("click", function() {
		THIS.filters = dataAnalyser.getFilters();
		var config = getConfig();
		THIS.data.config = JSON.stringify(config);
		previewProfile(false,THIS.data);
	});
	$("#btnSaveFilter").unbind("click").bind("click", function() {
		saveFilter();
	});

	$("#btnSaveChartFilter").unbind("click").bind("click", function() {
		saveFilter('chartfilter');
	});

	$("#proceedDashboard").unbind("click").bind("click", function() {
		$("#drawProfiles").show(300);
		$("#myDashBoard").hide();
	});
	$("#editProfiles").unbind("click").bind("click",function() {
	isEdit=true;
						$("#dataAnalyserProfile").hide();
						$("#drawProfiles").show();
						$("#profileDashboardContainer").empty();
						var addConfig = THIS.data.additional_config ? Jsonparse(THIS.data.additional_config): ''
							if(addConfig && addConfig.filtermapping.length>0){
								drawFilters();
							}
						if (addConfig && addConfig.pdfDownload) {
							$('#enablePdf').prop('checked', true);
						} else {
							$('#enablePdf').prop('checked', false);
						}
						if (addConfig && addConfig.filterInterdependent) {
							$("#filterInterdependent").attr("checked", "checked");
						}
						if (addConfig && addConfig.isFilterNotapplicable) {
							$("#filterNotapplicable").attr("checked", "checked");
						}
						if (addConfig && addConfig.isFilterHide) {
							$('#filterShowHide').prop('checked', true);
						} else {
							$('#filterShowHide').prop('checked', false);
						}
					});
	$("#allProfiles").unbind("click").bind("click", function() {
		$("#myDashboardProfilesDiv").show();
		$(this).parent().parent().hide();
		getAllProfiles();
	});

	$("#myModalConfigFilter").find("[name='btnAddFilter']").unbind("click")
			.bind("click", function() {
				filtersMapping('', 'filters');
			});

	$("#myModalChartFilter").find("[name='btnAddFilter']").unbind("click")
			.bind("click", function() {
				filtersMapping();
			});

	$("#configDashboardFilter").unbind("click").bind("click", function() {
		configDashboardFilter();
	});

	$("#chartFilterMapping").unbind("click").bind("click", function() {
		configChartFilter();
	});

	$("#profileDashboardBack").unbind("click").bind("click", function() {
		$("#myDashboardProfilesDiv").show();
	});
	
	$("#profileDashboardBack").unbind("click").bind("click", function() {
		$("#myDashBoard").show(300);
		$("#formConf").hide(300);
	});

	$("#frmSaveProfiles").submit(function(e) {
		e.preventDefault();
	}).validate({
		ignore : [],
		rules : {
			profilename : {
				required : true
			},
			description : {
				required : true
			},
			appname : {
				required : true
			}
		},
		messages : {
			profilename : {
				required : 'Please enter profile name'
			},
			description : {
				required : 'Please describe profile'
			},
			appname : {
				required : 'Please select app name'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	;
	$("#addProfile").unbind("click").bind("click", function() {
		constructProfile();

	});
	var constructProfile = function(data) {
		var divLength = $("#widget-grid").find("[name='profilediv']").length;
		$("#widget-grid").append(
				$("<div/>").addClass("chart-div margin-20").attr({
					"name" : "profilediv"
				}).css({
					"background-color" : "#F5F5F5"
				}).append($("<div/>").addClass("col-sm-12").append($("<input/>").css({"width":"500px"}).attr({
					"name" : "profilename",
					"id" : "profileselect" + divLength
				}))));
		constructSelect2(
				"#profileselect" + divLength,
				"select id,displayname,filters from profile_analyser where (lower(displayname) like '%<Q>%' or lower(profilekey) like '%<Q>%') and type ='profile'",
				false, (data && Object.keys(data).length > 0 ? data : null),
				"name");
	};
		var getAllProfiles = function() {
	$('#myprofilediv').show();
	$('#allprofilediv').hide();
	$('#myProfiles').show();
//	var request = {
//		"type" : "dashboard"
//	};
//	
//	enableLoading();
//	
//	var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){myProfileSelect(e,this);});
//	profileUtitlyObj.getMyProfile();
//	
//	callAjaxService("getMyProfiles", callBackGetMyProfile, callBackFailure,
//			request, "POST");
//};
//var callBackGetMyProfile = function(response) {
//	disableLoading();
//	if (response && response.isException) {
//		showNotification("error", response.customMessage);
//		return;
//	}
//	if (response !== undefined && response !== null) {
		//var data = response;
		//profile analyser//
		var requiredCol = [ {
			"sWidth": '13%',
			"mDataProp" : "displayname",
			"title":"Name",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "profilekey",
			"sWidth": '13%',
			"title":"Action",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "source",
			"sWidth": '13%',
			"title":"Source",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "database",
			"sWidth": '13%',
			"title":"Data Base",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "configtable",
			"sWidth": '13%',
			"title":"Table",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedby",
			"sWidth": '12%',
			"title":"Modified By",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedon",
			"sWidth": '13%',
			"title":"Modified On",
			"sDefaultContent" : ""
		}];
		var profileParam = {
				"getAll" : true,
				"profileType" : "dashboard",
		        "columns" : requiredCol,
		        "isHistory" : true,
		        "isPublish" : true
		};
		
		enableLoading();
		var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){getProfileById(e,this);});
		profileUtitlyObj.getMyProfile();
//		$("#myDashboardProfiles").empty();
//		for (var i = 0; i < data.length; i++) {
//			var publishStatus = "Publish";
//			var btnClass = "btn-outline";
//			var actionId = 0;
//			if (data[i].profilekey && data[i].profilekey != null
//					&& data[i].profilekey != "") {
//				publishStatus = "Published";
//				btnClass = "btn-outline-success";
//				actionId = data[i].actionid;
//			}
//			$("#myDashboardProfiles")
//					.append(
//							$("<li/>")
//									.addClass("list-group-item")
//									.data({
//										"refid" : data[i].id
//									})
//									.append(
//											$("<a/>")
//													.append(
//															$("<span/>")
//																	.addClass(
//																			"text-left col-md-3")
//																	.html(
//																			data[i].displayname))
//													.append(
//															$("<span/>")
//																	.addClass(
//																			"text-left col-md-2")
//																	.html(
//																			data[i].profilekey))
//													.append(
//															$("<span/>")
//																	.addClass(
//																			"badge text-center col-md-3")
//																	.html(
//																			data[i].createdon))
//													.append(
//															$("<span/>")
//																	.addClass(
//																			"text-right col-md-1")
//																	.html(
//																			$(
//																					"<button/>")
//																					.addClass(
//																							btnClass
//																									+ " btn")
//																					.attr(
//																							{
//																								"href" : "void(0)",
//																								'data-target' : '#profilePublishModal',
//																								'data-toggle' : 'modal',
//																								'data-refid' : data[i].id,
//																								'data-displayname' : data[i].displayname,
//																								'data-actionid' : actionId,
//																								'data-action' : data[i].profilekey
//																							})
//																					.html(
//																							publishStatus)))
//									/*
//									 * <button class="btn-outline btn
//									 * margin-right-2"
//									 * href="javascript:void(0);">spine</button>
//									 */
//									));
//		}
//		$("#myDashboardProfiles li").unbind("click").bind("click",
//				function() {
//					getProfileById(this);
//				});
//	}
//	$("[data-target^=#profilePublishModal]").unbind("click").bind("click",
//			function(e) {
//				e.stopPropagation();
//				publishDashboardProfile(this);
//			});
	};
	this.onChartFilter = function(chart,chartType) {
		if (chartType === "world"){
			var chartid = chart["options"].element.id;
			var anchorName = '#' + chartid;
			var fieldName = $(anchorName).data("fieldname");
			var fieldValues = $(anchorName).data("filters");
			
			fieldValues  = fieldValues ? fieldValues: [];
			
			fieldValues = fieldValues.slice();
			var currentProfileId = $(anchorName).data("profileid");
			var chartreset = $(anchorName).data("chartReseted");
			callServiceWithFilter(fieldName, fieldValues, currentProfileId,
					chartid, chartreset, chart);
		}else{
			var chartid = chart.anchorName().split('container')[0];
			var anchorName = '#' + chartid;
			var fieldName = $(anchorName).data("fieldname");
			var fieldValues = chart.filters().slice();
			var currentProfileId = $(anchorName).data("profileid");
			var chartreset = $(anchorName).data("chartReseted");
			callServiceWithFilter(fieldName, fieldValues, currentProfileId,
					chartid, chartreset, chart);
		}
	};

	var callServiceWithFilter = function(fieldName, fieldValues,
			currentProfileId, chartid, chartreset, chart) {
		var profileToReload = [], profileids = [];
		var filterValues = dataAnalyser.getFiltersValue("divSankeyFilter");
		var allProfileData = $.extend(true, {}, THIS.allProfiles);
		THIS.defaultFilterValue = jQuery.extend(true, [], previousChart);
		// var defaultFilterValue ={};		
		delete allProfileData[currentProfileId];
		if (chartreset || fieldValues.length === 0) {
			var listToDelete = Object.keys(allProfileData);
			THIS.defaultFilterValue = THIS.defaultFilterValue.filter(function(item){return item.chartId !== chartid;});
			previousChart = THIS.defaultFilterValue.filter(function(item){return item.chartId !== chartid;});
			for (var i = 0; i < THIS.chartFilters.length; i++) {
				var obj = THIS.chartFilters[i];

				if (listToDelete.indexOf(obj.id) !== -1
						&& obj.fieldName === fieldName
						&& obj.chartid === chartid) {
					THIS.chartFilters.splice(i, 1);
					i--;
				}
			}
		}
		THIS.filterFields = THIS.filterFields.filter(function(item, pos, self) {
			return self.indexOf(item) == pos;
		});
		for (profileid in allProfileData) {			
			var chartfilter = {};
			var selectedFilter = {};
			var objectIsNew = jQuery.extend(true, {}, allProfileData[profileid]);
			var filters = [];
			var topFilter = THIS.filtersApplied ? THIS.filtersApplied[profileid]: [];
            for (var i = 0; i < topFilter.length; i++) {
				filters.push(topFilter[i]);
			}
			if (topFilter.length ===0) {

				var profile = $.extend(true, {}, allProfileData[profileid]);
				var topfilters = [], mappedFilter;
				var filterLength = filterValues.length > 0 ? filterValues.length
						: THIS.filters !==undefined ?THIS.filters.length:0;
				if(THIS.filterMapping !== undefined && THIS.filterMapping.length >0){
					for (var j = 0; j < filterLength; j++) {
						var filter = filterValues.length > 0 ? $.extend({},
								filterValues[j]) : $.extend({}, THIS.filters[j]);
						mappedFilter = THIS.filterMapping.filter(function(obj) {
							if (THIS.filters[j].fieldName === obj.fieldname)
								return obj;
						});
						if (mappedFilter && mappedFilter.length > 0
								&& mappedFilter[0].mappedProfiles[profileid]) {
							filter.fieldName = mappedFilter[0].mappedProfiles[profileid];
							filters.push(filter);
						}
					}
				}else{
					filterValues = dataAnalyser.getFiltersValue("divSankeyFilter");
					filters=filterValues;
				}
				
			}
			var count = 0;
			var chartFilVal = [];
			if (!chartreset && fieldValues.length > 0) {
				for (var field = 0; field < fieldValues.length; field++) {
					var fil = fieldValues[field];
					if (fil.indexOf(',') > -1) {
						fil = fil.replace(',', '`');
					}

					chartFilVal.push(fil);
				}
				var FilterObj = {};
				FilterObj.fieldName = fieldName;
				FilterObj.fieldVal = chartFilVal;
				FilterObj.chart = chart;
				FilterObj.chartId = chartid;
				if(previousChart.length>0){
					var isDuplicate = false;
					for(var n=0;n<previousChart.length;n++){
						if(previousChart[n].chartId === chartid){
							previousChart[n].fieldVal = chartFilVal;
							isDuplicate = true;
						}
					}
					if(!isDuplicate){
						previousChart.push(FilterObj);
					}
				}else{
					previousChart.push(FilterObj);
				}
				if (THIS.chartFilters.length > 0) {
					for (var k = 0; k < THIS.chartFilters.length; k++) {
						var obj = THIS.chartFilters[k];

						if (obj
								&& THIS.filterFields.indexOf(obj.fieldName) !== -1
								&& obj.fieldName === fieldName
								&& obj.id === profileid) {
							THIS.chartFilters[k].defaultvalue = fieldValues;
							count++;
						}
					}
				}

				if (count === 0) {
					THIS.filterFields.push(fieldName);
					chartfilter.fieldType = "select";
					chartfilter.fieldName = fieldName;
					chartfilter.defaultvalue = fieldValues;
					chartfilter.chartid = chartid;
					chartfilter.id = profileid;
					THIS.chartFilters.push(chartfilter);
				}
			}
			for (var j = 0; j < THIS.chartFilters.length; j++) {
				if (THIS.chartFilters[j].id === profileid) {
				var filter = $.extend({}, THIS.chartFilters[j]), mappedChartFilter;
				if(THIS.chartMapping !== undefined){
			if(THIS.chartMapping.length === 0 ){
				filters.push(filter);
			}else{
				mappedChartFilter = THIS.chartMapping
									.filter(function(obj) {
										if (THIS.chartFilters[j].fieldName === obj.mappedProfiles[profileid])
											return obj;
									});
				if (mappedChartFilter && mappedChartFilter.length > 0
								&& mappedChartFilter[0].mappedProfiles[profileid]) {
							filter.fieldName = mappedChartFilter[0].mappedProfiles[profileid];
							filters.push(filter);
						}
					}
				}else{
					filters.push(filter);
				}
				}
			}
			
			objectIsNew.filters = JSON.stringify(filters.length > 0 ? filters
					: THIS.filters);
			profileToReload.push(objectIsNew);
		}
		var result = THIS.defaultFilterValue.reduce(function(memo, e1) {
			var matches = memo.filter(function(e2) {
				return e1.fieldName == e2.fieldName;
			});
			if (matches.length == 0)
				memo.push(e1)
			return memo;
		}, [])
		if (profileToReload.length > 0) {
			dataAnalyser.callServiceByFilter(profileToReload, result);
		}
	};
	this.dynamicDashboard = function(profileConfig, data, filterActions) {
		var additionalconfig;
		THIS.profileId = data.id;
		this.isDynamic = true;
		if (data.additional_config) {
			additionalconfig = JSON.parse(data.additional_config);
			if (additionalconfig && additionalconfig.isFilterHide) {
				this.isFilterHide = true;
			}
			if (additionalconfig && additionalconfig.filtermapping) {
				this.filterMapping = additionalconfig.filtermapping;
			}
			if (additionalconfig && additionalconfig.chartmapping) {
				this.chartMapping = additionalconfig.chartmapping;
			}
		}
		this.filters = Jsonparse((data.filters).replace(/@/g, "'"), "CONFIG");
		this.profileView = profileConfig.views;
		templates.getWidget(null, function() {
			previewProfile(false, data);
		});

	};
	var getProfileById = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id,
			"viewId" : -1
		};
		enableLoading();
		$("#cloneProfile").show();
		callAjaxService("getProfileConfig", callBackGetProfileById,
				callBackFailure, request, "POST");
	};
	var callBackGetProfileById = function(response, profileConstruct) {
		disableLoading();
		$("#myDashboardProfilesDiv").hide(300);
		$("#drawProfiles").show(300);
		var profileconfig = JSON.parse(response.config);
		var data = profileconfig[0];
		THIS.data = data;
		THIS.profile.id = data.id;
		THIS.profile.displayname = data.displayname;
		THIS.profile.app = data.app;
		if (response.views !== undefined && response.views !== null) {
			THIS.profileView = response.views;
		}else if (profileconfig.views !== undefined && profileconfig.views !== null) {
			THIS.profileView = profileconfig.views;
		}
		if (data.additional_config) {
			var additionalconfig = JSON.parse(data.additional_config);
			if (additionalconfig && additionalconfig.isFilterInline) {
				$("#filterModalInlineswitch").attr("checked", "checked");
			}
			if (additionalconfig && additionalconfig.filteronTable) {
				$("#filteronTable").attr("checked", "checked");
			}
			if (additionalconfig && additionalconfig.filtermapping)
				THIS.filterMapping = additionalconfig.filtermapping;
			if (additionalconfig && additionalconfig.chartmapping)
				THIS.chartMapping = additionalconfig.chartmapping;
		}
		THIS.viewSettings = [];
		if (localStorage["viewSetting"] && localStorage["viewSetting"] !== null
				&& localStorage["viewSetting"] !== "null") {
			THIS.viewSettings = Jsonparse(localStorage["viewSetting"],
					"VIEW SETTING");
			localStorage["viewSetting"] = null;
		} else if (data["view_setting"] && data["view_setting"] !== null) {
			THIS.viewSettings = Jsonparse(data["view_setting"], "VIEW SETTING");
		}
		if (data["filters"] && $.trim(data["filters"]) !== ""
				&& data["filters"].length > 0 && data["filters"] !== "[]") {
			var arr = [], viewfilters;
			if (data["viewFilters"] != undefined) {
				viewfilters = Jsonparse(data["viewFilters"].replace(/@/g, "'"),
						"FILTER");
			}
			var profilefilters = Jsonparse(data["filters"].replace(/@/g, "'"),
					"FILTER");
			if (viewfilters && viewfilters.length > 0) {
				for (var i = 0; i < profilefilters.length; i++) {
					var countViewFilters = 0;
					for (var j = 0; j < viewfilters.length; j++) {
						if (profilefilters[i].fieldName === viewfilters[j].fieldName) {
							viewfilters[j].lookupquery = profilefilters[i].lookupquery;
							viewfilters[j].displayName = profilefilters[i].displayName;
							arr.push(viewfilters[j]);
							countViewFilters++
						}
					}
					if (countViewFilters === 0) {
						arr.push(profilefilters[i]);
					}
				}
				THIS.filters = arr;
			} else {
				THIS.filters = profilefilters;
			}
		}
		
		$('.profile-settings a.views').on('click', function() {
			$(this).parent().toggleClass("open");
		});
		$('body').on('click',function(e) {
							if (!$('.profile-settings').is(e.target)
									&& $('.profile-settings').has(e.target).length === 0
									&& $('.open').has(e.target).length === 0) {
								$('.profile-settings').removeClass('open');
							}
						});
		// THIS.filters = Jsonparse((data[0].filters).replace(/@/g,
		// "'"),"FILTER");
		if (!profileConstruct) {
			var profileConfig = JSON.parse(data.config);
			if (profileConfig && profileConfig.length > 0) {
				$("#widget-grid").find("[name='profilediv']").remove();
				for (var i = 0; i < profileConfig.length; i++) {
					constructProfile(profileConfig[i]);
				}
			}

		}
		previewProfile(false, data);
	};

	var drawFilters = function() {
		var fields = [], mappedFields = [];
		if (THIS.filterMapping && THIS.filterMapping.length > 0) {
			fields = profileAnalyser.filterMapping.map(function(obj) {
				return obj.fieldname;
			});
			for (var i = 0; i < fields.length; i++) {
				mappedFields.push({
					id : fields[i],
					text : fields[i]
				});
			}
		}
		dataAnalyser.drawFilters(THIS.filters, mappedFields);
	};

	var getConfig = function() {
		var currentConfig = [];
		$("#widget-grid").find("[name='profilediv']").each(
				function() {
					var currentObj = $(this);
					var configObj = {};
					var selectedProfile = currentObj
							.find("[id^=profileselect]").select2("data");
					if (selectedProfile && selectedProfile.id) {
						configObj.id = selectedProfile.id;
						configObj.name = selectedProfile.name;
						configObj.filters = selectedProfile.count;
						currentConfig.push(configObj);
					}
				});
		return currentConfig;
	};
	var addConfigFilter = function(data, isfilter) {
		var configs = getConfig();
		var profilesDiv = "";
		if (configs && configs.length > 0) {
			profilesDiv = $("<div/>").addClass("row");
			for (var i = 0; i < configs.length; i++) {
				var columnsSelect = $("<input/>").attr({
					"name" : "profilename",
					"profileid" : configs[i].id
				}).addClass("width-100per");
				profilesDiv.append($("<div/>").addClass("col-md-3").append(
						$("<strong/>").addClass("note").html(configs[i].name))
						.append(columnsSelect));
				$(columnsSelect)
						.select2({
							placeholder : "Select Column",
							allowClear : true,
							data : THIS.profileColumns[configs[i].id]
						})
						.select2(
								"val",
								(data && data.mappedProfiles[configs[i].id]) ? data.mappedProfiles[configs[i].id]
										: '');
			}
		}
		if (isfilter === 'filters') {
			$("#frmConfigFilter").find("[name='divConfigFilter']").append($("<section/>").addClass("row margin-10 well")
									.append($("<i/>").attr({"name" : "removefilter"}).addClass("fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right"))
									.append(
											$("<div/>")
													.addClass("col-md-2")
													.css({
														"margin-top" : "13px"
													})
													.append(
															$("<strong/>")
																	.addClass(
																			"note")
																	.html(
																			"Field Name"))
													.append(
															$("<input/>")
																	.attr(
																			{
																				"name" : "fieldname"
																			})
																	.addClass(
																			"form-control")
																	.val(
																			data
																					&& data.fieldname)))
									.append(
											$("<div/>").addClass("col-md-10")
													.append(profilesDiv)));
		} else {
			$("#frmChartFilter")
					.find("[name='divChartFilter']")
					.append(
							$("<section/>")
									.addClass("row margin-10 well")
									.append(
											$("<i/>")
													.attr({
														"name" : "removefilter"
													})
													.addClass(
															"fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right"))
									.append(
											$("<div/>").addClass("col-md-10")
													.append(profilesDiv)));
		}
		profilesDiv.parents("section").find("[name='removefilter']").unbind(
				"click").bind("click", function() {
			$(this).parent().remove();
		});
	};
	var configDashboardFilter = function() {
		$("#myModalConfigFilter").modal("show");
		$("#frmConfigFilter").find("[name='divConfigFilter']").empty();
		enableLoading();
		previewProfile('filters');
		// filtersMapping();
		// addConfigFilter();
	};

	var configChartFilter = function() {
		$("#myModalChartFilter").modal("show");
		$("#frmChartFilter").find("[name='divChartFilter']").empty();
		enableLoading();
		previewProfile("chartFilter");
		// filtersMapping();
		// addConfigFilter();
	};
	var saveFilter = function(arg) {
		if (arg === 'chartfilter') {
			THIS.chartMapping = [];
			$("#frmChartFilter").find("[name='divChartFilter']")
					.find("section").each(
							function() {
								var currentObj = $(this);
								var filter = {}
								filter.mappedProfiles = {};
								currentObj.find("[name='profilename']").each(
										function() {
											filter.mappedProfiles[$(this).attr(
													"profileid")] = $(this)
													.select2("val");
										});
								THIS.chartMapping.push(filter);
							});
			$("#myModalChartFilter").modal("hide");
		} else {
			THIS.filterMapping = [];
			$("#frmConfigFilter").find("[name='divConfigFilter']").find(
					"section").each(
					function() {
						var currentObj = $(this);
						var filter = {}
						filter.fieldname = currentObj
								.find("[name='fieldname']").val();
						filter.mappedProfiles = {};
						currentObj.find("[name='profilename']").each(
								function() {
									filter.mappedProfiles[$(this).attr(
											"profileid")] = $(this).select2(
											"val");
								});
						THIS.filterMapping.push(filter);
					});
			$("#myModalConfigFilter").modal("hide");
			drawFilters();
		}
	};
	
	var filtersWithAllCol = function(){
		
	}
	var saveProfile = function(isClone) {
		var saveDataProfile = function() {
			var config = getConfig();
			var filters = dataAnalyser.getFilters();
			if (!isClone) {
				var profileID = THIS.profile.id || 0;
				if ((profileID === 0)) { // Update values if saving on first
											// time
					var app = $("#appname").select2("data");
					THIS.profile.displayname = $(
							"#myModalthredlevel #description").val();
					THIS.profile.app = app.id;
				}
			} else {
				callBackSuccessHandler = function() {
					disableLoading();
					$('#myModalthredlevel').modal('hide');
					showNotification("success",
							"Data Profile cloned successfully..");
				};
				THIS.profile.displayname = $("#myModalthredlevel #profilename")
						.val();
				THIS.profile.app = $("#appname").select2("data").name;
			}
			var additionalconfig = {};
			var request = {
				"config" : JSON.stringify(config),
				"filters" : JSON.stringify(filters),
				// "remarks" : remarks,
				"displayname" : THIS.profile.displayname,
				"type" : "dashboard",
				"app" : THIS.profile.app,
				"id" : isClone === true ? 0 : THIS.profile.id
			};
			if (THIS.filterMapping) {
				additionalconfig.filtermapping = THIS.filterMapping;
			}
			if (THIS.chartMapping) {
				additionalconfig.chartmapping = THIS.chartMapping;
			}
			if ($("#filterNotapplicable").is(":checked")) {
				additionalconfig.isFilterNotapplicable = true;
			}
			if ($("#filterInterdependent").is(":checked")) {
				additionalconfig.filterInterdependent = true;
			}
			if ($("#filterModalInlineswitch").is(":checked")) {
				additionalconfig.isFilterInline = true;
			}
			if ($("#filteronTable").is(":checked")) {
				additionalconfig.filteronTable = true;
			}
			if ($("#filterShowHide").is(":checked")) {
				additionalconfig.isFilterHide = true;
			}
			if ($("#enablePdf").is(":checked")) {
				additionalconfig.pdfDownload = true;
			}
			if (Object.keys(additionalconfig).length > 0) {
				request.additionalconfig = JSON.stringify(additionalconfig);
			}
			enableLoading();
			callAjaxService("saveProfile", callBackSaveProfile,
					callBackFailure, request, "POST");
		};
		var profileID = THIS.profile.id || 0;
		if (isClone || profileID === 0) {
			$("#myModalthredlevel #description").val("");
			$("#myModalthredlevel #profilename").val("");
			constructSelect2(
					"#appname",
					"select name,displayname from roles where lower(displayname) like '%<Q>%'",
					false, null, "name");
			$("#myModalthredlevel").modal('show');
			$("#myModalthredlevel #btnCreate").unbind("click").bind("click",
					function() {
						if (!$("#frmSaveProfiles").valid())
							return;
						saveDataProfile();
					});
		} else {
			saveDataProfile();
		}
	};
	var constructFilters = function(source, database) {
		profileAnalyser.data.source = source;
		profileAnalyser.data.database = database;
		THIS.FiltersDeFault = "getManipulatedDate('month',-6,null)"
		dataAnalyser.filterHTML(JSON.parse(THIS.data.additional_config));
			$("[name='clearFilter']").unbind("click").bind("click",function() {
				$('#clearFilter').data("refineVal",true)
				var filters = [];
				
				profileAnalyser.filters.forEach(function(filter) {
					 if((filter.fieldType === "lookupdropdown" || filter.fieldType === "lookup") && !filter.invisible || filter.fieldType === "dynamicDate"){
						 filter.defaultvalue = "";
					 }
					 var filterval = $('#clearFilter').data("defaultVal")
					 if(filter.fieldType === "dynamicDate" || filter.fieldType === "DATE" || filter.fieldType === "dateRange"){
						 for(var y =0; y < filterval.length; y++ ){
							 if(filter['fieldName'] == filterval[y]['fieldName']){
								 if(filter.fieldType === "dateRange")
									 filter["defaultvalue"] = filterval[y]['defaultvalue']?filterval[y]['defaultvalue']:null
								 else{
									 filter["from-defaultvalue"] = filterval[y]['from-defaultvalue']?filterval[y]['from-defaultvalue']:null
									 filter["to-defaultvalue"] = filterval[y]['to-defaultvalue']?filterval[y]['to-defaultvalue']:null
								 }
							 }
						 }
					 }
					 filters.push(filter);
				});
				dataAnalyser.constructFilters(filters,"divSankeyFilter",profileAnalyser.data.source, profileAnalyser.data.database);
				$("#applyFilter").trigger('click');
				$("#filterModal").modal('hide');
			});
			$("#applyFilter").unbind("click").bind("click",function() {
				if($('#clearFilter').data("refineVal")!=true){
				if($(".refineClear").length==0){
					$(".profile-settings a:eq(0)").after($("<a/>").addClass("refineClear").prepend("<i class='fa fa-close' style='font-size:15px !important; color:red'></i>Clear Search").attr("title","Clear Search").attr("style","display: inline-block; margin-left: 6px;").unbind('click').bind('click', function() {
					$("#widget-inline-filter").show();
					$("#clearFilter").trigger('click');
					$(".refineClear").remove();
				}));
				}
				}
				$('#clearFilter').data("refineVal",false)
				$(".d3-tip").remove();
								THIS.filtersApplied = {};
								THIS.chartFilters = [];
								localStorage["isMultipleProfile"] = true;
								var profileToReload = [];
								var filterValues = dataAnalyser
										.getFiltersValue("divSankeyFilter");
								THIS.filters = filterValues;
								var index = THIS.allProfiles.length;
								for (profileid in THIS.allProfiles) {
									if (filterValues.length > 0) {
										var profileids = [];
										var profile = $.extend(true, {},
												THIS.allProfiles[profileid]);
										var topfilters = [], mappedFilter;
										// topfilters=JSON.parse(profile.filters);
										if(THIS.filterMapping && THIS.filterMapping.length>0){
											for (var j = 0; j < filterValues.length; j++) {
												var filter = $.extend({},filterValues[j]);
												mappedFilter = THIS.filterMapping
														.filter(function(obj) {
															if (THIS.filters[j].fieldName === obj.fieldname)
																return obj;
														});
												if (mappedFilter
														&& mappedFilter.length > 0
														&& mappedFilter[0].mappedProfiles[profileid]) {
													filter.fieldName = mappedFilter[0].mappedProfiles[profileid];
													topfilters.push(filter);
													profileids.push(filter);
												}
											}
										}else{
											topfilters = filterValues
										}
										previousChart=[];
										THIS.defaultFilterValue=[];
										THIS.chartFilters=[];
										THIS.filtersApplied[profileid] = profileids;
										profile.filters = JSON.stringify(topfilters);
										profileToReload.push(profile);
									}

								}
								if (profileToReload.length > 0)
									dataAnalyser
											.callServiceByFilter(profileToReload);
							});
			$('#frmSankeyFilter').find("#applyFilter").unbind("click").bind("click",function() {
				if($('#clearFilter').data("refineVal")!=true){
				if($(".refineClear").length==0){
					$(".profile-settings a:eq(0)").after($("<a/>").addClass("refineClear").prepend("<i class='fa fa-close' style='font-size:15px !important; color:red'></i>Clear Search").attr("title","Clear Search").attr("style","display: inline-block; margin-left: 6px;").unbind('click').bind('click', function() {
					$("#widget-inline-filter").show();
					$("#clearFilter").trigger('click');
					$(".refineClear").remove();
				}));
				}
				}
				$('#clearFilter').data("refineVal",false)
				THIS.filtersApplied = {};
				THIS.chartFilters = [];
				$(".d3-tip").remove();
				localStorage["isMultipleProfile"] = true;
				var profileToReload = [];
				var filterValues = dataAnalyser
						.getFiltersValue("divSankeyFilter");
				THIS.filters = filterValues;
				var index = THIS.allProfiles.length;
				for (profileid in THIS.allProfiles) {
					if (filterValues.length > 0) {
						var profileids = [];
						var profile = $.extend(true, {},
								THIS.allProfiles[profileid]);
						var topfilters = [], mappedFilter;
						// topfilters=JSON.parse(profile.filters);
						if(THIS.filterMapping && THIS.filterMapping.length>0){
							for (var j = 0; j < filterValues.length; j++) {
								var filter = $.extend({},
										filterValues[j]);
								mappedFilter = THIS.filterMapping
										.filter(function(obj) {
											if (THIS.filters[j].fieldName === obj.fieldname)
												return obj;
										});
								if (mappedFilter
										&& mappedFilter.length > 0
										&& mappedFilter[0].mappedProfiles[profileid]) {
									filter.fieldName = mappedFilter[0].mappedProfiles[profileid];
									topfilters.push(filter);
									profileids.push(filter);
								}
							}
						}else{
							topfilters = filterValues
						}
						previousChart=[];
						THIS.defaultFilterValue=[];
						THIS.chartFilters=[];
						THIS.filtersApplied[profileid] = profileids;
						profile.filters = JSON.stringify(topfilters);
						profileToReload.push(profile);
					}

				}
				$("#filterModal").modal('hide');
				if (profileToReload.length > 0)
					dataAnalyser
							.callServiceByFilter(profileToReload,undefined,true);
			});
//			if(JSON.parse(THIS.data.additional_config).isFilterHide){
//    			$('#divSankeyFilter').parents(".jarviswidget-sortable").hide();
//    		}
//		}
//
  };

	var callBackSaveProfile = function(response) {
		if ((response && response.isException)
				|| (response.message && response.message === "failure")) {
			showNotification("error", response.customMessage
					|| response.message);
			return;
		}
		if (response !== undefined && response !== null) {
			disableLoading();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Profile saved successfully.");
			$("#saveProfiles").data("id", response);
			$("#cloneProfiles").show();
		}
	};
	var previewProfile = function(isFilterConfig, data, viewName) {
		if (!isFilterConfig) {
			drawViews(THIS.profileView, data, viewName);
		}
		var configobj;
		if (data) {
			THIS.data = data;
			THIS.viewSettings = [];
			if (localStorage["viewSetting"]
					&& localStorage["viewSetting"] !== null
					&& localStorage["viewSetting"] !== "null") {
				THIS.viewSettings = Jsonparse(localStorage["viewSetting"],
						"VIEW SETTING");
				localStorage["viewSetting"] = null;
			} else if (data["view_setting"] && data["view_setting"] !== null) {
				THIS.viewSettings = Jsonparse(data["view_setting"],
						"VIEW SETTING");
			}
			/*if(THIS.viewSettings && THIS.viewSettings.length>0 && THIS.chartFilters.length === 0){
				for (profileid in THIS.allProfileData) {	
					for(var vs=0;vs<THIS.viewSettings.length;vs++){
						if(THIS.viewSettings[vs].profileid !== profileid){
							THIS.chartFilters.push({
								fieldType:"select",
								fieldName:THIS.viewSettings[vs].fieldname,
								defaultvalue:THIS.viewSettings[vs].filterValues,
								chartid:THIS.viewSettings[vs].anchorName,
								id:profileid
							});
						}
					}
				}
			}*/
			if (data["filters"] && $.trim(data["filters"]) !== ""
					&& data["filters"].length > 0 && data["filters"] !== "[]") {
				var arr = [], viewfilters;
				if (data["viewFilters"] != undefined) {
					viewfilters = Jsonparse(data["viewFilters"].replace(/@/g,
							"'"), "FILTER");
				}
				var profilefilters = Jsonparse(data["filters"].replace(/@/g,
						"'"), "FILTER");
				if (viewfilters && viewfilters.length > 0) {
					for (var i = 0; i < profilefilters.length; i++) {
						var countViewFilters = 0;
						for (var j = 0; j < viewfilters.length; j++) {
							if (profilefilters[i].fieldName === viewfilters[j].fieldName) {
								viewfilters[j].lookupquery = profilefilters[i].lookupquery;
								viewfilters[j].displayName = profilefilters[i].displayName;
								viewfilters[j].source = profilefilters[i].source;
								viewfilters[j].database = profilefilters[i].database;
								arr.push(viewfilters[j]);
								countViewFilters++
							}
						}
						if (countViewFilters === 0) {
							arr.push(profilefilters[i]);
						}
					}
					THIS.filters = arr;
				} else {
					THIS.filters = profilefilters;
				}
			}
			configobj = Jsonparse(data.config);
			var configFilters = THIS.filters;
		}
		enableLoading();
		var config = configobj || getConfig();
		if (config && config.length > 0) {
			var ids = config.map(function(o) {
				return o.id
			}).toString();
			var request = {
				"ids" : ids,
				"viewId" : 0
			};
			callAjaxService("getProfileConfig", function(response) {
				callBackGetProfileConfig(response, isFilterConfig, ids,
						configFilters)
			}, callBackFailure, request, "POST");
		}
		;

	};

	var filtersMapping = function(data, isfilter) {
		var configs = getConfig();
		if (configs && configs.length > 0) {
			profilesDiv = $("<div/>").addClass("row");
			for (var i = 0; i < configs.length; i++) {
				var columnsSelect = $("<input/>").attr({
					"name" : "profilename",
					"profileid" : configs[i].id
				}).addClass("width-100per");
				profilesDiv.append($("<div/>").addClass("col-md-3").append(
						$("<strong/>").addClass("note").html(configs[i].name))
						.append(columnsSelect));
				$(columnsSelect)
						.select2({
							placeholder : "Select Column",
							allowClear : true,
							data : THIS.profileColumns[configs[i].id]
						})
						.select2(
								"val",
								(data && data.mappedProfiles) ? data.mappedProfiles[configs[i].id]
										: '');
			}
		}
		if (isfilter === 'filters') {
			$("#frmConfigFilter")
					.find("[name='divConfigFilter']")
					.append(
							$("<section/>")
									.addClass("row margin-10 well")
									.append(
											$("<i/>")
													.attr({
														"name" : "removefilter"
													})
													.addClass(
															"fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right"))
									.append(
											$("<div/>")
													.addClass("col-md-2")
													.css({
														"margin-top" : "13px"
													})
													.append(
															$("<strong/>")
																	.addClass(
																			"note")
																	.html(
																			"Field Name"))
													.append(
															$("<input/>")
																	.attr(
																			{
																				"name" : "fieldname"
																			})
																	.addClass(
																			"form-control")
																	.val(
																			data
																					&& data.fieldname)))
									.append(
											$("<div/>").addClass("col-md-10")
													.append(profilesDiv)));
		} else {
			$("#frmChartFilter")
					.find("[name='divChartFilter']")
					.append(
							$("<section/>")
									.addClass("row margin-10 well")
									.append(
											$("<i/>")
													.attr({
														"name" : "removefilter"
													})
													.addClass(
															"fa fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right"))
									.append(
											$("<div/>").addClass("col-md-10")
													.append(profilesDiv)));
		}

		profilesDiv.parents("section").find("[name='removefilter']").unbind(
				"click").bind("click", function() {
			$(this).parent().remove();
		});

	}
	
	function calculateDynamicFilter(dataQuery,filters){
		var query = dataQuery;
		var dynamicFilters = query.match(/\[(.*?)\]/g)||[];
		$.each(dynamicFilters,function(){
			var compliedExpersion = this.toString();
			var validExp = true;
			var re = /(?:^|\W)#(\w+)(?!\w)/g, match;
			while (match = re.exec(this.toString())){
				 var tempVar = match[1];
				 if (tempVar.charAt(0) === "f" && filters && filters.length>0){
					 $.each(filters,function(){
						  if (this.id === tempVar){
							  firstEle = false;
							  if ((!this.defaultvalue || this.defaultvalue.length === 0) && compliedExpersion.indexOf("function()")=== -1)
							  {
								  validExp = false;
								  return;
							  }
							  if ((this.fieldType === "lookup") && this.isSingle === "true"){
								  if(this.isSingle)
									  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.defaultvalue && this.defaultvalue.id)?this.defaultvalue.id:'');
								  else
									  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.defaultvalue?this.defaultvalue.map(function(o){return "'"+o.id+"'"}).toString():'');
							  }else if(this.fieldType === "select"){
								  if(!this.isSingle && this.defaultvalue && this.defaultvalue.length>0){
									  this.defaultvalue = this.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
									  var index = this.defaultvalue.indexOf(" ");
									  if(index>-1)
										  this.defaultvalue.splice(index,1);
								  }else{
									  if(this.defaultvalue && typeof(this.defaultvalue) === "object" && this.defaultvalue.id){
										  this.defaultvalue = this.defaultvalue.id;
									  }
								  }
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.isSingle && this.defaultvalue.trim())?this.defaultvalue:this.defaultvalue.map(function(o){return "'"+o+"'"}).toString());	  
							  }
							  else if(this.fieldType === "lookupdropdown"){
								  if(!this.isSingle){
									  var index = this.defaultvalue.indexOf(" ");
									  if(index>-1)
										  this.defaultvalue.splice(index,1);
								  }
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,(this.isSingle && this.defaultvalue.trim())?this.defaultvalue:this.defaultvalue.map(function(o){return "'"+o+"'"}).toString());
							  }
							  else if(this.fieldType === "TEXT")
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.defaultvalue);
							  else
								  validExp = false;
							  return;
						  };
					  });
				 }else if(tempVar.charAt(0) === "c" && dc.chartRegistry.list().length>0){
					 var chartFilters = dc.chartRegistry.list();
					 $.each(chartFilters,function(){
						  if (this.anchorName() === tempVar){
							  if (!this.filterValues || this.filterValues.length === 0)
							  {
								  validExp = false;
								  return;
							  }
							  if (firstEle)
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,this.filterValues.join("','"));
							  else
								  compliedExpersion = compliedExpersion.replaceAll("#"+tempVar,"'"+this.filterValues.join("','")+"'");
							  
							  firstEle = false;
							  return;
						  }
					  });
				 }else{
					 validExp = false;
				 }
			}
			if (validExp){
				if(compliedExpersion.indexOf("function()")>-1){
					try {
						query = query.replace(this,eval(compliedExpersion.replace("[","").replace("]","")));
				    } catch (e) {
				    	query = query.replace(this,' ');
				    }
					
				}else{
					query = query.replace(this,compliedExpersion.replace("[","").replaceAll("]",""));
				}
			}else{
				query = query.replace(this,' ');
			}
		});
		return query;
	}
	
	var columnListForFil= function(configData, isFilterConfig,dataQ){
        enableLoading();
		if (configData && configData.length > 0) {
			var requests = [];
			var url = serviceCallMethodNames["getColumnList"].url;
			THIS.profileIds = [];
			THIS.profileColumns = {};
				var request = {
						"source" : configData[0].source,
						"database" : configData[0].database,
						"table" : configData[0].configtable
						//"query" : dataQuery
					};
				var dataQuery = null;
				if(dataQ !== ""){
					if (configData[0].additional_config) {
						var additionalconfig = JSON.parse(configData[0].additional_config);
						for(var q=0;q<dataQ.length;q++){
							if (additionalconfig.dataSourceId ===dataQ[q].profileID ) {
								dataQuery =calculateDynamicFilter(dataQ[q].query,configData[0].filters);
//								var status= THIS.data && Jsonparse(THIS.data.additional_config) ? Jsonparse(THIS.data.additional_config).filteronTable : false
//								if(status){
									if(dataQuery.toLowerCase().indexOf('where')>-1){
										var queryTable = dataQuery.toLowerCase().split('where');
										queryTable = queryTable[0].split('from');
										request.table = queryTable[1];
									}else if(dataQuery.toLowerCase().indexOf('from')>-1){
										var queryTable = dataQuery.toLowerCase().split('from');
										request.table = queryTable[1];
									}else{
										request.query =dataQuery;
										}
//								}
							}
						}
					}
				}
				
				var ajaxValue = {
					"type" : "POST",
					"url" : url,
					"data" : request,
					// "contentType": "application/x-www-form-urlencoded;
					// charset=UTF-8",
					"dataType" : "json",
					"error" : function() {
					},
					"cache" : true,
					"complete" : function() {

					}
				};
				requests.push($.ajax(ajaxValue));
			$.when.apply(this, requests).then(function() {
								disableLoading();
								var responseArgsArray = Array.prototype.slice.call(this, arguments);
								var data = [];
									if (arguments[0][0].length) {
										for (var j = 0; j < arguments[0][0].length; j++) {
											data.push({
														"id" : arguments[0][0][j].columnName,
														"text" : arguments[0][0][j].columnName
													});
										}
									} else {
										for (var j = 0; j < arguments[0].length; j++) {
											data.push({
														"id" : arguments[0][j].columnName,
														"text" : arguments[0][j].columnName
													});
										}
									}
									THIS.Columns = data;
									//if(isEdit)
									dataAnalyser.drawFilters(THIS.filters, data);
							});
		}
	
	}
	var getColumnListByTable = function(configData, isFilterConfig,dataQ) {
		enableLoading();
		if (configData && configData.length > 0) {
			var requests = [];
			var url = serviceCallMethodNames["getColumnList"].url;
			THIS.profileIds = [];
			THIS.profileColumns = {};
			// url = url.substring(url.indexOf("..") + 2, url.length);
			for (var i = 0; i < configData.length; i++) {
				var request = {
						"source" : configData[i].source,
						"database" : configData[i].database,
						"table" : configData[i].configtable
						//"query" : dataQuery
					};
				var dataQuery = null;
				if (configData[i].additional_config) {
					var additionalconfig = JSON.parse(configData[i].additional_config);
					for(var q=0;q<dataQ.length;q++){
						if (additionalconfig.dataSourceId ===dataQ[q].profileID ) {
							dataQuery =calculateDynamicFilter(dataQ[q].query,configData[i].filters);
							if(additionalconfig.filteronTable){
								if(dataQuery.toLowerCase().indexOf('where')>-1){
									var queryTable = dataQuery.toLowerCase().split('where');
									queryTable = queryTable[0].split('from');
									request.table = queryTable[1];
								}else if(dataQuery.toLowerCase().indexOf('from')>-1){
									var queryTable = dataQuery.toLowerCase().split('from');
									request.table = queryTable[1];
								}
							}else{
								request.query =dataQuery;
							}
						}
					}
				}
				THIS.profileIds.push(configData[i].id);
				
				var ajaxValue = {
					"type" : "POST",
					"url" : url,
					"data" : request,
					// "contentType": "application/x-www-form-urlencoded;
					// charset=UTF-8",
					"dataType" : "json",
					"error" : function() {
					},
					"cache" : true,
					"complete" : function() {

					}
				};
				requests.push($.ajax(ajaxValue));
			}
			$.when.apply(this, requests).then(
							function() {
								// Each argument is an array with the following
								// structure: [ data, statusText, jqXHR ]
								disableLoading();
								var responseArgsArray = Array.prototype.slice.call(this, arguments);
								for (var i = 0; i < arguments.length; i++) {
									var data = [];
									if (arguments[i][0].length) {
										for (var j = 0; j < arguments[i][0].length; j++) {
											data
													.push({
														"id" : arguments[i][0][j].columnName,
														"text" : arguments[i][0][j].columnName
													});
										}
									} else {
										for (var j = 0; j < arguments[i].length; j++) {
											data
													.push({
														"id" : arguments[i][j].columnName,
														"text" : arguments[i][j].columnName
													});
										}
									}
									THIS.profileColumns[THIS.profileIds[i]] = data;
								}
								if (isFilterConfig === 'filters'
										&& THIS.filterMapping
										&& THIS.filterMapping.length > 0) {
									for (var i = 0; i < THIS.filterMapping.length; i++) {
										addConfigFilter(THIS.filterMapping[i],
												isFilterConfig);
									}
								} else if (isFilterConfig === 'chartFilter'
										&& THIS.chartMapping
										&& THIS.chartMapping.length > 0) {
									for (var i = 0; i < THIS.chartMapping.length; i++) {
										addConfigFilter(THIS.chartMapping[i],
												isFilterConfig);
									}
								} else {
									addConfigFilter();
								}

							});
		}
	};

	var callBackGetProfileConfig = function(response, isFilterConfig, ids,configFilters, viewname) {
		disableLoading();
		dc.deregisterAllCharts();
		dc.chartRegistry.clear();
		ids = ids.split(",");
		
		var configData = JSON.parse(response["config"]);
		var dataQuery =  response["dataQuery"] !== undefined ?JSON.parse(response["dataQuery"]):'';
		configData.sort(function(obj1, obj2) {
			if (ids.indexOf(obj1.id) < ids.indexOf(obj2.id))
				return -1;
			if (ids.indexOf(obj1.id) > ids.indexOf(obj2.id))
				return 1;
			return 0;
		});
		
//		if(!isEdit){
//			columnListForFil(configData, isFilterConfig,dataQuery);
//		}
		
		var profileActions = response.profileActions;
		if (configData && configData.length > 0) {
			if (isFilterConfig === 'filters') {
				// getColumnListByfilter(configData);
				getColumnListByTable(configData, isFilterConfig,dataQuery);
			} else if (isFilterConfig === 'chartFilter') {
				getColumnListByTable(configData, isFilterConfig,dataQuery);
			}else if(isFilterConfig === 'noFilterMap'){
				columnListForFil(configData, isFilterConfig,dataQuery);
			} else {
				$("#drawProfiles").hide();
				var types = configData.map(function(o) {
					return o.type
				});
				var scripts = [];
				for (key in profilejsfiles) {
					if (types.indexOf(key) > -1) {
						scripts.push(profilejsfiles[key]);
					}
				}

				loadScripts(scripts,function() {
							var source = '';
							var database = '';
							THIS.allProfiles = {};
							if (!dataAnalyser) {
								dataAnalyser = new DataAnalyser();
							}
							for (var i = 0; i < configData.length; i++) {
								if (configData[i].additional_config) {
									var additionalconfig = JSON
											.parse(configData[i].additional_config);
									for(var q=0;q<dataQuery.length;q++){
										if (additionalconfig.dataSourceId ===dataQuery[q].profileID ) {
											additionalconfig["dataQuery"] = dataQuery[q].query;
											configData[i].additional_config = additionalconfig;
										}
									}
									
								}
								THIS.allProfiles[configData[i].id] = configData[i];
								source = configData[i].source;
								database = configData[i].database;
							}
							localStorage["isMultipleProfile"] = true;
							constructFilters(source, database);
							dataAnalyser.dynamicDashboardProfiles(configData,
									profileActions, configFilters,
									THIS.filterMapping, THIS.viewSettings);
							if (THIS.isDynamic)
								setTimeout(
										function() {
											if (THIS.data.additional_config
													&& Jsonparse(THIS.data.additional_config).pdfDownload) {
												$('#myModalDrilldown').on('hidden.bs.modal', function() {
													/*dc.chartRegistry.list().forEach(function(chart){
														var width = $(chart.anchor()).data("width");
														var height = $(chart.anchor()).data("height");
											            //var _bbox = chart.root().node().parentNode.getBoundingClientRect();
											            chart.width(width).height(height);
											          });
													dc.renderAll();*/
												});
												$(".profile-settings").append($("<a/>").prepend($("<i/>").addClass("fa fa-file-pdf-o")).attr("title","Print view of "+THIS.data.displayname).unbind('click').bind
														('click',{ "config": THIS.config }, function(e) {
															$('.dataTables_wrapper').find('.dataTables_length select').find('option[value=500]').attr('selected', 'selected');
																$('.dataTables_wrapper').find('.dataTables_length select').trigger('change');

																	var renderingDataTablesForPrint = function(){																		
																	var divContent = $('<div/>').attr({'id': 'modal-div-content'}).css({"z-index": "2147483647"});								
																	$("#widget-grid").each(function(index, _val1) {
																		$(_val1).find('.chart-parent-div').each(function(index, _val2) {
																			var _this = _val2;
																			if(!$(_this).hasClass('search-div') && $(_this).find(".jarviswidget  > div").is(":visible")){
																				var table = '';
																				var tableName = $(_this).find('header h2').text();
																				var tableHead = $(_this).find('.dataTables_wrapper .dataTables_scrollHead table thead tr[role="row"]');
																				var tableBody = $(_this).find('.dataTables_wrapper .dataTables_scrollBody table tbody');
																				if($(_this).hasClass('profile-summary-div')){
																					/*table = $('<div/>').html($(_this).html());
																					table.find(".col-md-2").css({"width":"16.66666%","float":"left","max-height":""});*/
																				}
																				else if(!$(_this).find('.widget-body .row div').hasClass('dataTables_wrapper')){
																					var str = $('<div/>')//.addClass($(_val2).attr('class'))
																								.append(tableName?'<table style="width:100% !important;border: 1px solid #ddd !important;padding-bottom:10px;margin-bottom:20px;border-collapse:collapse"><tr style="background: #585858;height:35px;font-size:20px ;color: #f2f2f2;-webkit-print-color-adjust:exact;"><td style="max-width: 140px; word-wrap:break-word;text-align:left">'+tableName+'</td></tr></table>':'')
																								//.append( $('<p/>').css({"font-size": "16px", "font-weight": "bolder","background": "#585858", "color": "#f2f2f2","-webkit-print-color-adjust": "exact"}).text(tableName));
																					table = str.append($(_this).find('.widget-body').clone());																					
																					table.find(".hide").remove();
																					table.find(".col-md-4").css({"width":"33.33333%","float":"left","max-height":""});
																					table.find(".grid-line").find("line").remove();
																					$("#myModalDrilldown").find(".dc-chart").css({ 'overflow' : '', 'width' : '' ,'max-height':''});
																					table.find(".reset").each(function(index,ele){
																						$(ele).parent().find("h4").remove();
																						if($(ele).parent().find("svg").length===0){
																							$(ele).parent().append($("<h4/>").addClass("alert-heading txt-color-orangeDark").html("No Data"));
																						}
																					});
																					table.css({"min-height":"100px"});
																					table.find("svg").find(".x").find("text").attr({"text-anchor": "end !important;"}).css({"transform": "translate(-10px, 2px) rotate(-30deg)","text-anchor": "end !important;","-webkit-print-color-adjust": "exact"});
																				} else {
																					var datatableId = $(_this).find(".dataTables_wrapper").attr("id").replace("_wrapper","");
																					var backendtablename = "";
																					if(profileAnalyser.dataTable && profileAnalyser.dataTable[datatableId] && profileAnalyser.dataTable[datatableId].ajax.params().table){
																						backendtablename = profileAnalyser.dataTable[datatableId].ajax.params().table;
																					}
																					
																					if(backendtablename === "crd_notes"){
																						var studyType = "CR&D";
																				        if(window.location.hash.indexOf("StudyScreen_IIT")>-1){
																				        	studyType = "IIT";
																				        }
																						var strHead = '<tr style="background: #585858;height:35px;font-size:20px ;color: #f2f2f2;-webkit-print-color-adjust: exact; "><td style="max-width: 140px; word-wrap:break-word;text-align:left" colspan="'+tableHead.find('th').length+'">'+tableName+'</td></tr>';
																						table = $('<table style="width:100% !important;border: 1px solid #ddd !important;margin-bottom:18px;border-collapse:collapse"/>')
																						.attr({"cellpadding":"2","cellspacing":"2"}).addClass('table table-bordered');
																						
																						var notesData = profileAnalyser.dataTable[datatableId].data();
																						if(notesData && notesData.length>0){
																							var noteContent = "";
																							table.append($('<thead/>').append(strHead));
																							for(var i=0;i<notesData.length;i++){
																								noteContent = "";
																								noteContent += '<tr><td>';
																								noteContent += '<table class="table table-bordered" style="width:100% !important;border: 1px solid #ddd !important;margin-bottom:18px;border-collapse:collapse">';
																								if(studyType === 'CR&D'){
																									noteContent +=	'<tr><td class="" style="width:50%"><b class="">Study :</b><span class="margin-left-5">'+notesData[i].protocol_num+'</span></td>';
																									noteContent +=	'<td class="" style="width:50%"><b class="">Modified By :</b><span class="margin-left-5">'+notesData[i].modified_by+'</span></td></tr>';
																									noteContent +=	'<tr><td class=""><b class="">Version :</b><span class="margin-left-5">'+notesData[i].version+'</span></td>';
																									noteContent +=	'<td class=""><b class="">Modified On :</b><span class="margin-left-5">'+getManipulatedDate(null,0,notesData[i].modified_on,"%d %b %Y %H:%M:%S")+'</span></td>';
																									noteContent +=	'</tr>';
																								}else{
																									noteContent +=	'<tr><td class="" style="width:40%"><b class="">Project :</b><span class="margin-left-5">'+(notesData[i].project_name || "")+'</span></td>';
																									noteContent +=	'<td class="" style="width:40%"><b class="">Study :</b><span class="margin-left-5">'+notesData[i].protocol_num+'</span></td>';
																									noteContent +=	'<td class="" style="width:20%"><b class="">Version :</b><span class="margin-left-5">'+notesData[i].version+'</span></td></tr>';
																									noteContent +=	'<tr><td class="" style="width:40%"><b class="">Modified By :</b><span class="margin-left-5">'+notesData[i].modified_by+'</span></td>';
																									noteContent +=	'<td class="" style="width:60%" colspan="2"><b class="">Modified On :</b><span class="margin-left-5">'+getManipulatedDate(null,0,notesData[i].modified_on,"%d %b %Y %H:%M:%S")+'</span></td>';
																									noteContent +=	'</tr>';
																								}
																								//noteContent += '<table class="table table-bordered">';
																								if(studyType === 'CR&D'){
																									noteContent +=	'<tr class=""><td colspan="2">';
																									noteContent +=	'<div><b class="">TMF Status :</b><div class="well" style="height:auto;padding:20px;margin-top:5px;margin-bottom:10px">'+notesData[i].study_summary+'</div></div>';
																									noteContent +=	'<div><b class="">Import License Status :</b><div class="well" style="height:auto;padding:20px;margin-top:5px;margin-bottom:10px">'+notesData[i].supply_summary+'</div></div>';
																									noteContent +=	'<div><b class="">Study Depot Summary :</b><div class="well" style="height:auto;padding:20px;margin-top:5px;margin-bottom:10px">'+notesData[i].depot_notes+'</div></div>';
																									noteContent +=	'<div><b class="">Study Assessment Summary :</b><div class="well" style="height:auto;padding:20px;margin-top:5px;margin-bottom:10px">'+notesData[i].remarks+'</div></div>';
																								}else{
																									noteContent +=	'<tr class=""><td colspan="3">';
																									noteContent +=	'<div><b class="">Study Assessment Summary :</b><div class="well" style="height:auto;padding:20px;margin-top:5px;margin-bottom:10px">'+notesData[i].remarks+'</div></div>';
																								}
																								noteContent +=	'</td></tr>';
																								noteContent +=	'</table>';
																								noteContent += '</td></tr>';
																								table.append(noteContent);
																							}
																							table.find("ul").css("padding-left",'40px');
																							//table.append($('<thead/>').append(strHead)).append($('<tbody/>').append(noteContent));
																						}
																					}else{
																						table = $('<table style="width:100% !important;border: 1px solid #ddd !important;margin-bottom:18px;border-collapse:collapse"/>')
																						.attr({"cellpadding":"2","cellspacing":"2"}).addClass('table table-bordered');
																						//table.find('caption').addClass('text-left').html('<p style="font-size: 16px; font-weight: bolder;">'+tableName+'</p>');
																						var strHead = '<tr style="background: #585858;height:35px;font-size:20px ;color: #f2f2f2;-webkit-print-color-adjust: exact; "><td style="max-width: 140px; word-wrap:break-word;text-align:left" colspan="'+tableHead.find('th').length+'">'+tableName+'</td></tr>';
																						strHead += '<tr style="background: #8e8c8c; color: #f2f2f2;-webkit-print-color-adjust: exact; ">';
																						tableHead.find('th').each(function(index, _val3) {
																							var _THISHEAD = _val3;
																							if(index < 12)
																								strHead += '<th style="max-width: 140px; word-wrap:break-word;border: 1px solid #ddd !important;">'+$(_THISHEAD).text()+'</th>';
																						});
																						strHead += '</tr>';
																						var thLength = tableHead.find('th').length;
																						var strBody = '';
																						tableBody.find('tr').each(function(index, _val4) {
																							var _THISBODY = _val4;
																							strBody += '<tr>';
																							var tdLength = $(_THISBODY).find('td').length;
																							$(_THISBODY).find('td').each(function(index, _val5) {
																								var _THISTD = _val5;
																								if(index < 12){
																									if(tdLength === 1){
																										strBody += '<td colspan='+thLength+' style="text-align:center;padding-left:10px !important;max-width: 140px; word-wrap:break-word;border: 1px solid #ddd !important;">'+$(_THISTD).text()+'</td>';
																									}else{
																										strBody += '<td style="padding-left:10px !important;max-width: 140px; word-wrap:break-word;border: 1px solid #ddd !important;">'+$(_THISTD).text()+'</td>';
																									}
																								}
																							});
																							strBody += '</tr>';
																						});
																						table.append($('<thead/>').append(strHead)).append($('<tbody/>').append(strBody));
																					}
																					
																				}
																				
																				divContent.append(table);
																			}
																		});
																	});
																	
																	$("#myModalLabel", "#myModalDrilldown").html(""); 
																	$("#myModalDrilldown .modal-header").hide();
																	$("#myModalDrilldown").css({"overflow": "auto"});
																	$("#myModalDrilldown .modal-dialog").css({"width": "auto", "max-width": "1200px"});
																	$("#printView").empty();
																	$("#printView").append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 20px; top: 12px;"><span aria-hidden="true">x</span> </button>');
																	$("#printView").css({"padding": "20px"}).append($('<h2/>').addClass('text-center').text($(".breadcrumb li").last().text().split("-")[0])
																			.css({"margin": "0px", "font-weight": "bold", "text-decoration": "underline"}))
																		.append($('<div/>').addClass('pull-right')
																		.append($('<button/>').addClass('btn btn-sm btn-primary').text('Print').css({"margin-right": "10px"})
																		.bind('click', function(e){
																		    printElement(document.getElementById("modal-div-content"));
																		})));
																	$("#printView").append(divContent);
																	$("#myModalDrilldown").modal("show");
																	divContent.find("svg").each(function(index,svg){
																		var width = svg.getAttribute("width") || 1000;
																		var height = parseInt(svg.getAttribute("height")) || 400;
																		svg.setAttribute("viewBox","0 0 "+width+" "+(height+100));
																		$(svg).parent().css({"max-height":""});
																		$(svg).removeAttr("width");
																		$(svg).removeAttr("height");
																	});
																}

																	
																function printTimeOut(){
																	setTimeout(function(){
																		var dataTablesProcessingDone = true;
																		$('.dataTables_processing').each(function(e){
																			if($(this).css('Display')!= 'none'){
																				dataTablesProcessingDone = false;
																			}
																		});
																		if(dataTablesProcessingDone){
																			renderingDataTablesForPrint();
																		}else{
																			printTimeOut();
																		}
																	}, 4000);
																};
																printTimeOut();
																
																function printElement(elem) {
																    var domClone = elem.cloneNode(true);
																    var printWindow = window.open('', '', 'height=1000,width=1200');
																    var $printSection = document.getElementById("printSection");
																    $("#left-panel").hide();
																    if (!$printSection) {
																        var $printSection = document.createElement("div");
																        $printSection.id = "printSection";
																        document.body.appendChild($printSection);
																    }
																    $printSection.innerHTML = "";
																    // $printSection.appendChild(domClone); /SPECTRA/src/main/webapp/web/css/bootstrap.min.css
																    $("#widget-grid svg").hide();
																    $("#printView").hide();
																    printWindow.document.write('<html><head><title>Study Analysis</title><link rel="stylesheet" type="text/css" href="css/your_style.css"><link rel="stylesheet" type="text/css" href="css/smartadmin-production_unminified.css"><link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"></head><body>');
																    printWindow.document.body.innerHTML = $(domClone).prop("outerHTML");
																    setTimeout(function(){
																	    printWindow.print();
																	}, 4000);
																    $("#widget-grid svg").show();
																    $("#printView").show();
																    $("#left-panel").show();
																}
															}));
											}
											
										}, 1000);
						}, "#dataAnalyserProfile");
			}

		}

	};

	var publishDashboardProfile = function(obj) {
		$("#userEnabled").removeAttr("checked");
		$("#smart-form-register input").removeAttr("readonly");
		if ($(obj).html() !== "Publish") {
			publishedActiondetails($(obj).attr("data-action"), $(obj).attr(
					"data-actionid"), $(obj).data("refid"));
		} else {
			var publishDataObj = {};
			publishDataObj.action = $(obj).data("displayname")
					.replace(/ /g, '');
			publishDataObj.displayname = $(obj).data("displayname");
			publishDataObj.refid = $(obj).data("refid");
			publishDataObj.source = "";

			var callBackPublishProfile = function(data) {
				$(obj).html("Published").removeClass("btn-outline").addClass(
						"btn-outline-success");
				$(obj).parent().parent().find(
						".text-left.col-md-2 col-xs-2 col-sm-2 col-lg-2").text(
						data.action);
				$(obj).parent().parent().find(
						".text-right col-md-1 col-xs-1 col-sm-1 col-lg-1")
						.attr("data-action", data.action);
			};

			$("#smart-form-register input").val("");
			$("#frmSankeyFilter em").remove();
			$(".state-error").removeClass("state-error");

			templates.managePublishing(publishDataObj, "CREATE",
					callBackPublishProfile);
		}
	};

	var publishedActiondetails = function(action, actionId, refid) {
		enableLoading();

		var callbackSucessGetAction = function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				if (response === null) {
					disableLoading();
					alert("Selected Profile is Published as Tab");
					return;
				}

				response.refid = refid;
				response.actionId = Number(actionId);
				templates.managePublishing(response, "VIEW", undefined);
			}
		};

		var request = {
			"action" : action
		};
		callAjaxService("getAction", function(response) {
			callbackSucessGetAction(response);
		}, callBackFailure, request, "POST");
	};

	var deleteView = function(obj) {
		var profileId = $(obj).parent().parent().data("profileId");
		var viewId = $(obj).parent().parent().data("viewId");
		var query = 'delete from profile_views where id =' + viewId;
		var updateRequest = {
			"source" : 'postgreSQL',
			"dataBase" : 'E2EMFPostGres',
			"queries" : btoa(query)
		}
		callAjaxService("integraGenericUpdate", function(response) {
			deleteCallBack(response, profileId, viewId);
		}, callBackFailure, updateRequest, "POST", "json", true);
	};

	var deleteCallBack = function(response, profileId, viewId) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}/*
			 * else if (views === undefined || views === null) {
			 * showNotification("error", "No Data avaiable.."); return; }
			 */else {
			showNotification("success", "view deleted successfully.");
			var request = {
				"id" : profileId,
				"viewId" : 0
			};
			$("#dataAnalyserProfile #widget-grid .widget-body.row").empty();
			$("#divSankeyFilter").empty();
			$(".dc-data-count").empty();
			callAjaxService("getProfileConfig", function(response) {
				callBackGetProfileById(response, true);
			}, callBackFailure, request, "POST");
		}
	}

	var viewClick = function() {
		var viewName = $($(this).find('label')[0]).text();
		var profileId = $(this).data("profileId");
		var viewId = $(this).data("viewId");
		var request = {
			"id" : profileId,
			"viewId" : viewId
		};
		$("#dataAnalyserProfile #widget-grid .widget-body.row").empty();
		$("#divSankeyFilter").empty();
		$(".dc-data-count").empty();
		enableLoading();// ,'',Object.keys(THIS.allProfiles).join(','),THIS.filters,viewName
		callAjaxService("getProfileConfig", function(response) {
			callBackGetProfileById(response, true);
		}, callBackFailure, request, "POST");
	};

	var getFiltersFromProfile = function(filterContentId) {
		filterContentId = filterContentId || "";
		if (filterContentId === "")
			return;
		var filterConfig = [];
		$('#' + filterContentId + ' section')
				.each(
						function() {
							var currentObj = $(this), isDefaultValue;
							var tempConfig = {
								"fieldName" : currentObj.data("field-name"),
								"displayName" : currentObj.data("display-name"),
								"fieldType" : currentObj.data("field-type"),
								"searchtype" : currentObj.data("searchtype"),
								"id" : $(this).attr("id"),
								"invisible" : currentObj.data("invisible")
							};
							if (currentObj.data("profileid"))
								tempConfig.profileid = currentObj
										.data("profileid");
							if (currentObj.find("[name='searchtype']").length > 0) {
								tempConfig.searchtype = currentObj.find(
										"[name='searchtype']").val();
							}
							if (tempConfig["fieldType"] === 'lookup'
									|| tempConfig["fieldType"] === 'select'
									|| tempConfig["fieldType"] === 'lookupdropdown') {
								tempConfig["lookupquery"] = currentObj
										.data("lookup-query");
								if (tempConfig["fieldType"] === 'lookupdropdown'
										|| tempConfig["fieldType"] === 'select') {
									tempConfig["defaultvalue"] = currentObj
											.find("[name='dropdowndiv']").find(
													"select").val();
								} else {
									tempConfig["defaultvalue"] = currentObj
											.find("input").select2('data');
								}
								tempConfig.isSingle = currentObj
										.data("isSingle");
								if (tempConfig.defaultvalue
										&& ((typeof o === 'object' && Object
												.keys(tempConfig.defaultvalue).length > 0) || tempConfig.defaultvalue.length > 0)) {
									isDefaultValue = true;
								}
							} else if (tempConfig["fieldType"] === 'dateRange') {
								tempConfig["defaultvalue"] = currentObj
										.data('defaultvalue');
								tempConfig["dateFormat"] = currentObj
										.data('dateformat');
								;
							} else if (tempConfig["fieldType"] === 'DATE'
									|| tempConfig["fieldType"] === 'dynamicDate'
									|| tempConfig["fieldType"] === 'NUM') {
								tempConfig["from-defaultvalue"] = currentObj
										.find("[name^=from-defaultvalue]")
										.val();
								tempConfig["to-defaultvalue"] = currentObj
										.find("[name^=to-defaultvalue]").val();
								tempConfig["dateFormat"] = currentObj
										.data('dateformat');
								;
								if (tempConfig["from-defaultvalue"]
										&& tempConfig["to-defaultvalue"]) {
									isDefaultValue = true;
								}
							} else if (tempConfig["fieldType"] === 'raw') {
								tempConfig["defaultvalue"] = currentObj.find(
										"textarea").val();
								if (tempConfig["from-ddefaultvalue"]
										&& tempConfig["to-defaultvalue"]) {
									isDefaultValue = true;
								}
							}

							else {
								tempConfig["defaultvalue"] = currentObj.find(
										"input").val();
								if (tempConfig.defaultvalue) {
									isDefaultValue = true;
								}
							}
							$(currentObj).find("em").remove();
							if (currentObj.data("isMandatory")
									&& !isDefaultValue) {
								$(currentObj)
										.find("label:eq(0)")
										.append(
												$("<em/>")
														.addClass("invalid")
														.html(
																currentObj
																		.data("display-name")
																		+ " can not be empty."));
								filterConfig = false;
								return false;
							}
							filterConfig.push(tempConfig);
						});
		// setTimeout(function () {$("#"+filterContentId).find("em").remove()},
		// 2000);
		return filterConfig;
	};

	var saveView = function() {
		var name = $("[name=viewname]", "#actionDiv").val();
		if (name === "")
			return;
		var isDefault = $("[name=default]", "#actionDiv").is(":checked");
		var isPublic = $("[name=public]", "#actionDiv").is(":checked");
		if (isDefault)
			isDefault = true;
		else
			isDefault = false;

		if (isPublic)
			isPublic = true;
		else
			isPublic = false;
		var config = {
			widget:{}	
		};
		$("#widget-grid").find(".jarviswidget").each(function(index,ele){
			var id = $(ele).attr("id");
			if(!$("#"+id+" > div").is(":visible")){
				if(!config.widget[id]){
					config.widget[id] = {};
				}
				config.widget[id].hidden = true;
			}
		});	
		var viewFilters = getFiltersFromProfile('divSankeyFilter');
		var request = {};
		var view = {
			"id" : 0,
			"profileId" : profileAnalyser.profileId,
			"name" : name,
			"viewFilters" : JSON.stringify(viewFilters),
			"viewConfig":JSON.stringify(config),
			"isDefault" : isDefault,
			"isPublic" : isPublic,
			"viewSetting" : JSON.stringify(dataAnalyser.profiling.grapher
					.getChartFilters()),
			"filterQuery" : dataAnalyser.profiling.grapher
					.constructFilterQuery()
		};
		request.view = JSON.stringify(view);
		callAjaxService("saveView", callBackSaveView, callBackFailure, request,
				"POST");
	};
	var callBackSaveView = function(views) {
		if (views && views.isException) {
			showNotification("error", views.customMessage);
			return;
		} else if (views === undefined || views === null) {
			showNotification("error", "No Data avaiable..");
			return;
		} else {
			showNotification("success", "view saved successfully.");
			var myViews = $("[name=myViews]", ".profile-settings").empty();
			var publicViews = $("[name=publicViews]", ".profile-settings")
					.empty();
			publicViews.append($("<article/>").addClass("row").data({
				"viewId" : -1,
				"profileId" : dataAnalyser.profileId
			}).click(viewClick).append(
					$("<section/>").addClass("col col-6").append(
							$("<label/>").addClass("label")
									.html("Default View"))).append(
					$("<section/>").addClass("col col-6").append(
							$("<label/>").addClass("label").html(
									"Administrator"))));
			var noViewsMessage = $("<section>").addClass("col col-12").append(
					$("<label/>").addClass("label")
							.html("No Views Available.."));
			var myViewsAvaiable = false;
			for (var v = 0; v < views.length; v++) {
				var defaultI = views[v].isDefault && views[v].isDefault !== "f" && views[v].isDefault !== false ? $(
						"<i/>").addClass("fa fa-check txt-color-green").attr({
					"title" : "Default view"
				})
						: $("<i/>");
				var publicI = views[v].isPublic && views[v].isPublic !== "f" &&  views[v].isPublic !== false ? $(
						"<i/>").addClass("fa fa-users").attr({
					"title" : "Public"
				})
						: $("<i/>").addClass("fa fa-user").attr({
							"title" : "Private"
						});
				if (views[v].viewType) {
					myViewsAvaiable = true;
					myViews.append($("<article/>").addClass("row").data({
						"viewId" : views[v].id,
						"profileId" : views[v].profileId
					}).click(viewClick).append(
							$("<section/>").addClass("col col-3").append(
									$("<label/>").addClass("label").html(
											views[v].name))).append(
							$("<section/>").addClass("col col-1").append(
									publicI)).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].createdOn))).append(
							$("<section/>").addClass("col col-1").append(
									defaultI)).append(
							$("<section/>").addClass("col col-2").append(
									$("<i/>").addClass("fa fa-trash").attr(
											"title", "delete view").unbind(
											"click").bind("click", function(e) {// e.preventDefault();
										e.stopPropagation();
										deleteView(this);
									}))));
				} else {
					publicViews.append($("<article/>").addClass("row").data({
						"viewId" : views[v].id,
						"profileId" : views[v].profileId
					}).click(viewClick).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].name))).append(
							$("<section/>").addClass("col col-2").append(
									$("<label/>").addClass("label").html(
											views[v].createdBy))).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].createdOn))).append(
							$("<section/>").addClass("col col-2").append(
									$("<i/>").addClass("fa fa-trash").attr(
											"title", "delete view").unbind(
											"click").bind("click", function(e) {// e.preventDefault();
										e.stopPropagation();
										deleteView(this);
									}))));
				}
			}
			if (!myViewsAvaiable)
				myViews.append(noViewsMessage);

			$(".profile-settings [name=emailNotification]").unbind("click")
					.bind("click", function() {
						drawEmailNotification(views, THIS.emailNotification);
					});
		}
	};
	var htmlToPdf = function(html, filename, cssStyle, pdfHeight) {
		  var filterString = '';
		  var loopCount = 0;
		  var str = '<table class="table" name="filterTable" style=" border: none;">';
		  var tableCol = '';
		  var filterCount= 0;
		  var colCount = 0;
		  var finalStr = '';
		  var tables = []
		  
		  for(var i=0; i<$('#divSankeyFilter').find('section').length;i++){
		   var filterValues ='';
		   var filterId =$('#divSankeyFilter').find('#f'+(i+1));
		   if(filterId.find('button').attr('data-toggle') ==='dropdown'){
		    if(filterId.find('[name="dropdowndiv"]').find('select').val()!=null){
		     filterValues ='<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('[name="dropdowndiv"]').find('button').attr('title') ;
		    }
		   }else if(filterId.find('.select2-container').length >0){
		    if(filterId.find('[name="dropdowndiv"]').select2('data').text().trim()!="" && filterId.find('[name="dropdowndiv"]').select2('data').text().trim()!="No matches found")
		    filterValues = '<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('[name="dropdowndiv"]').select2('data').text();
		   }else if(filterId.find('[name="from-defaultvalue"]').length !=0 && filterId.find('[name="to-defaultvalue"]').length !=0){
		    if(filterId.find('[name="from-defaultvalue"]').val() !="" && filterId.find('[name="to-defaultvalue"]').val()!="")
		    filterValues = '<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('[name="from-defaultvalue"]').val() +' to '+ filterId.find('[name="to-defaultvalue"]').val();
		   }else{
		    if( filterId.find('input').val() !="")
		    filterValues = '<b>' +filterId.find('strong').text() +'</b>' +' '+ filterId.find('input').val();
		   }
		   if(filterValues !=""){
		    filterCount++;
		     colCount ++;
		     tableCol += '<td style="font-size:15px">' +filterValues+  '</td>';
		   }
		   if(colCount > 1){
		      colCount =0;
		      str += '<tr class="trClass">' +tableCol; 
		      str += '</tr>';
		         tableCol =''; 
		   }
		  }
		  
		  if(filterCount %2 !=0){
		    str += '<tr class="trClass">' +tableCol; 
		    str += '</tr>';
		  }
		  str += '</table>';
		  finalStr = '<div style="margin-top:10px ;font-size:15px"><div style="padding-bottom:6px"><b><i> Filters Applied:</i></b></div> ' +str+'</div>';
		    
		  var request ={
		    "html": $(str).find('tr').hasClass('trClass')? finalStr : '' + html,
		    "css": cssStyle,
		    "fileName":filename,
		    "height":pdfHeight
		     };
		  
		  var downloadURL = 'rest/htmToPdf/getconvertedpdf';
		  $.fileDownload(downloadURL,{
		         httpMethod: "POST",
		         data: request,
		   successCallback: function() {
		    showNotification("success","Pdf downloaded successfully.");
		   
		            },
		            failCallback: function(responseHtml) {
		             showNotification("failure","Pdf downloaded failed.");  
		            }
		 });
	};
	var drawViews = function(views, profileConfig, viewName) {
		profileConfig.viewname = viewName || profileConfig.viewname;
		$(".profile-settings").remove();
		$("<div/>").addClass("profile-settings pull-right").append(
				$("<a/>").addClass("views").attr("title", "Views")/*
																	 * .on('click',
																	 * function(){
																	 * $(".profile-settings.pull-right.open
																	 * ").length
																	 * !=0 ?
																	 * $(".profile-settings").removeClass('open') :
																	 * $(".profile-settings").addClass('open'); })
																	 */
				.append($("<i/>").addClass("fa fa-ellipsis-v"))).append(
				$("<ul/>").css({"width":"450px"}).addClass("dropdown-menu dropdown-menu-right").attr(
						"id", "settingsOption")).prependTo("#actionDiv");
		var dropdownDiv = $("<div/>").addClass("smart-form").unbind("click").bind("click", function(e) {//e.preventDefault();
								e.stopPropagation();
							});
		$("#settingsOption").append($("<li/>").prepend(dropdownDiv));
		var noViewsMessage = $("<section>").addClass("col col-12").append(
				$("<label/>").addClass("label").html("No Views Available.."));
		var myViews = $("<fieldset/>").attr({
			"name" : "myViews"
		});
		var publicViews = $("<fieldset/>").attr({
			"name" : "publicViews"
		});
		var myViewsAvaiable = false;
		var isdefaultInput;
		if (profileConfig.isdefault && profileConfig.isdefault !== "f" && profileConfig.isdefault !== "0" ) {
			isdefaultInput = $("<input/>").addClass("input-xs").attr({
				"type" : "checkbox",
				"name" : "default",
				"checked" : true
			});
		} else {
			isdefaultInput = $("<input/>").addClass("input-xs").attr({
				"type" : "checkbox",
				"name" : "default"
			});
		}
		var ispublicInput;
		if (profileConfig.ispublic && profileConfig.ispublic !== "f" && profileConfig.ispublic !== "0") {
			ispublicInput = $("<input/>").addClass("input-xs").attr({
				"type" : "checkbox",
				"name" : "public",
				"checked" : true
			});
		} else {
			ispublicInput = $("<input/>").addClass("input-xs").attr({
				"type" : "checkbox",
				"name" : "public"
			});
		}
		publicViews.append($("<article/>").addClass("row").data({
			"viewId" : -1,
			"profileId" : profileConfig.id
		}).click(viewClick).append(
				$("<section/>").addClass("col col-6").append(
						$("<label/>").addClass("label").html("Default View")))
				.append(
						$("<section/>").addClass("col col-6").append(
								$("<label/>").addClass("label").html(
										"Administrator"))));
		var saveButtonDesc;
		if (profileConfig.viewtype && profileConfig.viewtype !== "t") {
			saveButtonDesc = " Save As";
		} else {
			saveButtonDesc = " Save";
		}
		$(".breadcrumb span.currentView").remove();
		var viewname = profileConfig.viewname || "Default";
		$(".breadcrumb li:last").append(
				$("<span/>").addClass("currentView").html(" - " + viewname));
		dropdownDiv.append($("<fieldset/>").addClass("padding-top-20").append(
				$("<section/>").addClass("col col-9").append(
						$("<label/>").addClass("input").append(
								$("<input/>").addClass("input-xs").attr({
									"type" : "text",
									"name" : "viewname"
								}).val(profileConfig.viewname).unbind("click")
										.bind("click", function(e) {//e.preventDefault();
											e.stopPropagation();
										})))).append(
				$("<section/>").addClass("col col-3").append(
						$("<button/>").addClass("btn btn-primary btn-xs")
								.append(
										$("<i/>").addClass("fa fa-floppy-o")
												.html(saveButtonDesc).click(
														function() {
															saveView();
														})))).append(
				$("<section/>").addClass("col col-6 padding-left-40").append(
						$("<label/>").addClass("checkbox").append(
								isdefaultInput).append($("<i/>")).append(
								$("<span/>").addClass("font-xs").html(
										"Make it my default")))).append(
				$("<section/>").addClass("col col-6").append(
						$("<label/>").addClass("checkbox")
								.append(ispublicInput).append($("<i/>"))
								.append(
										$("<span/>").addClass("font-xs").html(
												"Make it public")))));
		if (views) {
			for (var v = 0; v < views.length; v++) {
				var defaultI = views[v].isDefault && views[v].isDefault !== "f" && views[v].isDefault !== false ? $(
						"<i/>").addClass("fa fa-check txt-color-green").attr({
					"title" : "Default view"
				})
						: $("<i/>");
				var publicI = views[v].isPublic && views[v].isPublic !== "f" && views[v].isPublic !== false ? $(
						"<i/>").addClass("fa fa-users").attr({
					"title" : "Public"
				})
						: $("<i/>").addClass("fa fa-user").attr({
							"title" : "Private"
						});
				if (views[v].viewType) {
					myViewsAvaiable = true;
					myViews.append($("<article/>").addClass("row").data({
						"viewId" : views[v].id,
						"profileId" : views[v].profileId
					}).click(viewClick).append(
							$("<section/>").addClass("col col-3").append(
									$("<label/>").addClass("label").html(
											views[v].name))).append(
							$("<section/>").addClass("col col-1").append(
									publicI)).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].createdOn))).append(
							$("<section/>").addClass("col col-1").append(
									defaultI)).append(
							$("<section/>").addClass("col col-2").append(
									$("<i/>").addClass("fa fa-trash").attr(
											"title", "delete view").unbind(
											"click").bind("click", function(e) {//e.preventDefault();
										e.stopPropagation();
										deleteView(this);
									}))));
				} else {
					publicViews.append($("<article/>").addClass("row").data({
						"viewId" : views[v].id,
						"profileId" : views[v].profileId
					}).click(viewClick).append(
							$("<section/>").addClass("col col-3").append(
									$("<label/>").addClass("label").html(
											views[v].name))).append(
							$("<section/>").addClass("col col-2").append(
									$("<label/>").addClass("label").html(
											views[v].createdBy))).append(
							$("<section/>").addClass("col col-5").append(
									$("<label/>").addClass("label").html(
											views[v].createdOn))).append(
							$("<section/>").addClass("col col-2").append(
									$("<i/>").addClass("fa fa-trash").attr(
											"title", "delete view").unbind(
											"click").bind("click", function(e) {
										e.stopPropagation();
										deleteView(this);
										;
									}))));
				}
			}
		}

		if (!myViewsAvaiable)
			myViews.append(noViewsMessage);

		dropdownDiv.append($("<header/>").html("My Views"));
		dropdownDiv.append(myViews);
		dropdownDiv.append($("<header/>").html("Public Views"));
		dropdownDiv.append(publicViews);
		dropdownDiv.append($("<hr/>"));
	};
};
var profileAnalyser = new ProfileAnalyser();