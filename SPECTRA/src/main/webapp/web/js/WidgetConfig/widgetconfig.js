pageSetUp();
var WidgetConfig;
var THIS ; 
var WidgetConfigInstance = function() {
	WidgetConfig = new WidgetConfigObject();
};

var WidgetConfigObject = function(){
	THIS= this
	updateGroup = function(val){
				var selectedApp = $("#rolesName").find("select").val();
				var selectedfilterAPP = $("#appsName").find("select").val();
				 if(selectedApp == "" || selectedApp == null || selectedfilterAPP == "" || selectedfilterAPP == null){
					 showNotification("notify", "Please select Role and App ");
					 val.checked = false
				 }
				 else{
					 if ($(val).parents('div[id=widgetDiv]').find("#screenInput").is(':checked') == true|| val.name == "screen") {
							// db update
							var roles = selectedApp;
							var groupId = parseInt($(val).attr('groupid'));
							if (groupId) {
								dbupdateForGroup(val,groupId,null)
							} else {
								var actionName = $(val).attr('actionName');
								dbupdateForActionName(val,actionName);
							}
						} else {
							if($(val).is(':checked'))
								val.checked = false
							else
								val.checked = true
							showNotification("notify", "Please select Main action First ");
						}
					}
				}
	
	var dbupdateForGroup = function(val,groupId,enabledCheck){
				var roles = $("#rolesName").find("select").val();
				if(enabledCheck != undefined){
					var enabled = enabledCheck
				}else{
					var enabled = val.checked ? 1 : 0
				}
				
				var primaryKey = $(val).data('primaryKey') ? parseInt($(val)
						.data('primaryKey'))
						: null
				var columnList = [ 'group_id', 'role', 'enabled' ]
				var updateValue = []
				for (var x = 0; x < columnList.length; x++) {
					var newValue
					var fieldType
					if (columnList[x] == 'group_id') {
						newValue = groupId
						fieldType = 'int'
					} else if (columnList[x] == 'role') {
						newValue = roles
						fieldType = 'text'
					} else {
						newValue = enabled
						fieldType = 'boolean'
					}
					var data = {}
					data.fieldName = columnList[x]
					data.displayName = columnList[x]
					data.newValue = newValue
					data.oldValue = ""
					data.type = "p"
					updateValue.push(data)
				}
				var groups = $(val).parents('div[id=widgetDiv]').find('div[id=groupDiv]')
				var enabl = 0;
				for(var x= 0; x < groups.length;x++){ 
					var selcetedInput = $($(groups[x]).find('input'));
					var groupId = $($(groups[x]).find('input')).attr('groupid');
					if(! $($(groups[x]).find('input')).is(':checked')){
					   enabl+=1;
					}
					if(groups.length == enabl){
						var Parentval = $(val).parents('div[id=widgetDiv]').find("#screenInput");
						// prop false for action
						Parentval.prop('checked', false)
						var actionName = $(Parentval).attr('actionName');
						dbupdateForActionName(Parentval,actionName)
					}
				
				}
				var table = 'widget_config_logs';
				ajaxCallToUpDateDB(null,updateValue, primaryKey, table)
		
	}
	
	
	
	var dbupdateForGroupTemp = function(val,groupId,enabledCheck){
		var roles = $("#rolesName").find("select").val();
		if(enabledCheck != undefined){
			var enabled = enabledCheck
		}else{
			var enabled = val.checked ? 1 : 0
		}
		
		var primaryKey = $(val).data('primaryKey') ? parseInt($(val)
				.data('primaryKey'))
				: null
		var columnList = [ 'group_id', 'role', 'enabled' ]
		var updateValue = []
		for (var x = 0; x < columnList.length; x++) {
			var newValue
			var fieldType
			if (columnList[x] == 'group_id') {
				newValue = groupId
				fieldType = 'int'
			} else if (columnList[x] == 'role') {
				newValue = roles
				fieldType = 'text'
			} else {
				newValue = enabled
				fieldType = 'boolean'
			}
			var data = {}
			data.fieldName = columnList[x]
			data.displayName = columnList[x]
			data.newValue = newValue
			data.oldValue = ""
			data.type = "p"
			updateValue.push(data)
		}
		var table = 'widget_config_logs';
		var request ={
				 source:'postgreSQL',
				 database:'E2EMFPostGres',
				 table:table,
				 primaryKey:primaryKey?[{"fieldName":'id','displayName':'id','newValue':primaryKey,'oldValue':''}]:null,
				 values:updateValue,
				 create:false
		 	};
		 var request = {
					"request":JSON.stringify(request)
			};
		 return request;

}
	var dbupdateForActionName = function(val,actionName){
				var roles = $("#rolesName").find("select").val();
				var enabled = val.checked ? 1 : 0
				var primaryKey = $(val).data('primaryKey') ? parseInt($(val)
						.data('primaryKey'))
						: null
				var columnList = [ 'action', 'role', 'enabled' ]
				var updateValue = []
				for (var x = 0; x < columnList.length; x++) {
					var newValue
					var fieldType
					if (columnList[x] == 'action') {
						newValue = actionName
						fieldType = 'text'
					} else if (columnList[x] == 'role') {
						newValue = roles
						fieldType = 'text'
					} else {
						newValue = enabled
						fieldType = 'boolean'
					}
					var data = {}
					data.fieldName = columnList[x]
					data.displayName = columnList[x]
					data.newValue = newValue
					data.oldValue = ""
					data.type = "p"
					updateValue.push(data)
				}
				var table = 'widget_action_config_logs';
				ajaxCallToUpDateDB(val,updateValue, primaryKey, table)
		
		
		
	}
	var ajaxCallToUpDateDB = function(val,value, primaryKey,table){
		System['selectedAction'] = val;
		var request ={
				 source:'postgreSQL',
				 database:'E2EMFPostGres',
				 table:table,
				 primaryKey:primaryKey?[{"fieldName":'id','displayName':'id','newValue':primaryKey,'oldValue':''}]:null,
				 values:value,
				 create:false
		 	};
		 var request = {
					"request":JSON.stringify(request)
			};
		callAjaxService("updateToDB",function(response){
			var actionName = $(val).attr('actionName');
			if (response && !response.isException) {
				var groups = $(System.selectedAction).parents('div[id=widgetDiv]').find('div[id=groupDiv]')
				var requests = [];
				var apiCall = []
				for(var x= 0; x < groups.length;x++){
					var selcetedInput = $($(groups[x]).find('input'));
					var groupId = $($(groups[x]).find('input')).attr('groupid');
					var enableCheck = $(System.selectedAction).is(':checked')?1:0;
					var request = dbupdateForGroupTemp(selcetedInput, groupId,enableCheck)
					requests.push(request);
					apiCall.push('updateToDB');
				}
				callAjaxServices(apiCall,function(response){
					if (response && !response.isException) { 
						var groups = $(System.selectedAction).parents('div[id=widgetDiv]').find('div[id=groupDiv]')
						for(var x= 0; x < groups.length;x++){
							var selcetedInput = $($(groups[x]).find('input'));
							var groupId = $($(groups[x]).find('input')).attr('groupid');
							if($(System.selectedAction).is(':checked')) 
								$($(groups[x]).find('input')).prop('checked',true)
							else
								$($(groups[x]).find('input')).prop('checked',false)
						
						}
					
					}
					
				},callBackFailure,requests,"POST","json",true);
				showNotification("success", "Role updated");
				return;
			}
			
		},callBackFailure,request,"POST");
		onChangeFun();
	}
	
	 var callBackSuccessGetData = function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				
				disableLoading();
				var screenList = []
				 response.data
				 response.data.forEach(function(screens){
					 screenList.push(screens.screen)
				})
				var uniqueScreenList = [];
				  $.each(screenList, function(i, e) {
				    if ($.inArray(e, uniqueScreenList) == -1) uniqueScreenList.push(e);
				  });
				  
				  var WidgetData = {}
					for(var i = 0; i < uniqueScreenList.length; i++){
							var groupData = []
							response.data.forEach(function(screens){
								var groupDict = {}
								if(screens.screen == uniqueScreenList[i]){
									groupDict['ID'] = screens.id;
									groupDict['GROUP'] = screens.widget_group;
									groupDict['ACTION_NAME'] = screens.action;
									groupDict['WIDGET_NAME']=screens.widget_display_name;
									groupData.push(groupDict)
								}
								 })
								 WidgetData[uniqueScreenList[i]] = groupData;
							 }
				  
				  THIS.widgetGroupData = WidgetData;
				  // appending <tr> to table 
				  var str = '';
				  		$("#grouptablediv").html("")
				  		
				  			
				  			
				  	  /*str+= '<table id="widgetTable" class="table table-bordered smart-form"><thead><tr style="height: 45px;">'
					  str+='<th style="width:20%; background-color: rgb(50, 118, 177); color: rgb(255, 255, 255); border-bottom: 1px solid rgb(50, 118, 177); vertical-align: top; text-align: center;" colspan="3">SCREEN NAMES</th>'
					  str+='<th style="background-color: rgb(50, 118, 177); color: rgb(255, 255, 255); border-bottom: 1px solid rgb(50, 118, 177); vertical-align: top; text-align: center;" colspan="3">GROUP NAMES</th>'
					  str+='</tr></thead>'
					  str+='<tbody>'*/
				  $.each( WidgetData, function( key, value ) {
					  
					  		str+='<div  id="widgetDiv" class="col-md-3 col-xs-3 col-sm-3 col-lg-3">'
					  		str+='<div class="jarviswidget jarviswidget-sortable">'
					  		str+='<header class="smart-form" style="background:#eeeeee">'
					  		str+='<h2 id="screenName">'+key+'</h2>'
					  		str+='<label style="margin-top: 2px;margin-right: 13px;" class="toggle" ><input id="screenInput" type="checkbox" onclick="updateGroup(this)" name="screen" value="'+value[0].ACTION_NAME+'"  actionName="'+value[0].ACTION_NAME+'"><i data-swchon-text="On" data-swchoff-text="Off"></i></label>'
					  		str+='</header>'
					  		str+='<div style="background: #FFFFFF">'
					  		str+='<div class="jarviswidget-editbox">'
					  		str+='</div>'
					  		str+='<div class="widget-body">'
					  		
/*					  str += '<div id="tableDiv" class="row smart-form col-sm-12" style="padding-top:15px;padding-bottom:15px; ">';
*/	
/*					  str+= '<tr id="screenName"><th style="padding-top: 27px;padding-left: 26px;background-color: rgb(120, 191, 105);color: rgb(255, 255, 255);" colspan="3"> <span><label style="width: max-content;" class="" title="'+key+'"><p style="color: rgb(255, 255, 255);">'+key+'</p></span></th>';
*/					  // no checkboxfor screen now
					 /* str+= '<tr id="screenName"><th style="background-color: rgb(120, 191, 105); color: rgb(255, 255, 255);" colspan="3"> <span><label style="width: max-content;" class="checkbox" title="'+key+'"><input type="checkbox" onclick="updateGroup(this);" name="screen" value="'+value[0].ACTION_NAME+'"  actionName="'+value[0].ACTION_NAME+'"><i type="checkbox"></i><p style="color: rgb(255, 255, 255);">'+key+'</p></span></th>';
					  str+= '<td style="background-color: rgb(223, 241, 214);">';
					  str+='<div id="groupWidget" class="row smart-form">'
					  for(var gr =0; gr < value.length; gr++){
						  str+= '<div class="col-sm-3" style="padding-left: 25px;"><span> <label style="width: max-content;" id="" class="checkbox" title="'+value[gr].GROUP+'"><input  id="widgetGroup_'+value[gr].ID+'"  type="checkbox" onclick="updateGroup(this);" name="widgetGroup" value="'+value[gr].GROUP+'"  groupid="'+value[gr].ID+'"><i type="checkbox"></i>'+value[gr].GROUP+'<span></div>';
					  }
					  str+='</div></td></tr>'*/
					  			
					  			for(var gr =0; gr < value.length; gr++){ 
					  				if(value[gr].GROUP !=""){
					  					  var toolStr = ""
					  					  if(value[gr].WIDGET_NAME !=null){
					  						  var widget_display = value[gr].WIDGET_NAME.split('<>')
					  						  toolStr +="Widgets:<br/>"
					  						  var count = 1;
					  						  for(var x= 0; x<widget_display.length;x++){
					  							toolStr+=count+"."+"&nbsp;"+widget_display[x]+"<br/>"
					  							count+=1;
					  							  
					  						  }
					  					  }
					  						  
						  				  str+='<div id="groupDiv" style="margin-bottom: 12px;" class="row smart-form"><label class="toggle" id="" title="'+value[gr].GROUP+'"><input type="checkbox" id="widgetGroup_'+value[gr].ID+'"  onclick="updateGroup(this);" name="widgetGroup" value="'+value[gr].GROUP+'"  groupid="'+value[gr].ID+'"><i data-swchon-text="On" data-swchoff-text="Off"></i></label><div  class="note" data-content="'+toolStr+'" data-html="true" data-toggle="popover" rel="popover" style="width:80%;cursor: pointer"><strong style="color: steelblue !important;font-size: 13px;">'+value[gr].GROUP+'</strong></div></div>'
					  				}
					  				else{
					  					str+='<p style="font-size: 14px;padding-left: 48px;padding-top: 57px;"><b>No Widget Configuration</b></p>'
					  				}
								  }
					  			
					  			    str+='</div>'
							  		str+='</div>'
							  		str+='</div>'
							  		str+='</div>'
					});
				  /*str+='</tbody>';
				  str+='</table>';*/
				  $("#grouptablediv").html(str)
			  }
			 $("[class='widget-body']").find("div[rel='popover']").popover({ 
		  		  placement: 'auto',
		  		  container: 'body',
		  		  width:'300px',
		  		  delay :  { "show": 100, "hide": 100 },
		  		  trigger : 'hover',
		  		  html:true
	         })
			}
	 var roleCallBackSuccessGetData = function(response){
		 if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				var widgetData
				var allwidget = $($("div[id=widgetDiv]"))
				for(var x=0; x< allwidget.length;x++){
					$($("div[id=widgetDiv]")[x]).find('header').find('input').prop('checked', false);
					//all group in each widget
					var allGroup = $($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')
					for(var z = 0; z < allGroup.length; z++){
						$($($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')[z]).find('input').prop('checked', false);
					}
				}
				widgetActionData = Jsonparse(response["2"])
				widgetActionData.data.forEach(function(actionData){
					for(var x = 0 ; x< allwidget.length; x++){
							if($($("div[id=widgetDiv]")[x]).find('header').find('input').attr('actionName') == actionData.action){
								if(actionData.enabled == '1'){
									$($("div[id=widgetDiv]")[x]).find('header').find('input').prop('checked', true);
								}else{
									$($("div[id=widgetDiv]")[x]).find('header').find('input').prop('checked', false);
								}
								$($("div[id=widgetDiv]")[x]).find('header').find('input').data('primaryKey',actionData.id)
							}
						}
					
				})
				
				
				widgetGroupData = Jsonparse(response["1"])
				widgetGroupData.data.forEach(function(data){
					for(var x = 0 ; x< allwidget.length; x++){
						var allGroupId = $($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')
						for(var z = 0; z< allGroupId.length; z++ ){
							
							if($($($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')[z]).find('input').attr('groupid')== data.group_id ){
								if(data.enabled == '1'){
									$($($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')[z]).find('input').prop('checked', true);
								}
								else{
									$($($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')[z]).find('input').prop('checked', false);
								}
								$($($("div[id=widgetDiv]")[x]).find('div[id=groupDiv]')[z]).find('input').data('primaryKey',data.id)
							}
						}
					}
				})
				
				
				
				/*var allTd = $($("#grouptablediv").find("#widgetTable td"))
				for(var x = 0 ; x< allTd.length; x++){
						var allgroups = $($("#grouptablediv").find("#widgetTable td")[x]).find('input')
						for(var z = 0; z< allgroups.length; z++ ){
							
							$($("#grouptablediv").find("#widgetTable td")[x]).find('input')[z].checked=false;
						}
					}
				var allThInput = $($("#grouptablediv").find("#widgetTable th input"))
				for(var x = 0 ; x< allThInput.length; x++){
					$("#grouptablediv").find("#widgetTable th input")[0].checked=false
					}
				widgetActionData = Jsonparse(response["2"])
				widgetActionData.data.forEach(function(actionData){
					for(var x = 0 ; x< allThInput.length; x++){
							if($($("#grouptablediv").find("#widgetTable th input")[x]).attr('actionName') == actionData.action){
								if(actionData.enabled == '1'){
									$("#grouptablediv").find("#widgetTable th input")[x].checked=true;
								}else{
									$("#grouptablediv").find("#widgetTable th input")[x].checked=false;
								}
								$($("#grouptablediv").find("#widgetTable th input")[x]).data('primaryKey',actionData.id)
							}
						}
					
				})*/
				/*widgetGroupData = Jsonparse(response["1"])
				widgetGroupData.data.forEach(function(data){
					for(var x = 0 ; x< allTd.length; x++){
						var allIgroup = $($("#grouptablediv").find("#widgetTable td")[x]).find('input')
						for(var z = 0; z< allIgroup.length; z++ ){
							
							if($($($("#grouptablediv").find("#widgetTable td")[x]).find('input')[z]).attr('groupid')== data.group_id ){
								if(data.enabled == '1'){
									$($("#grouptablediv").find("#widgetTable td")[x]).find('input')[z].checked=true;
								}
								else{
									$($("#grouptablediv").find("#widgetTable td")[x]).find('input')[z].checked=false;
								}
								$($($("#grouptablediv").find("#widgetTable td")[x]).find('input')[z]).data('primaryKey',data.id)
							}
						}
					}
				})*/
				
			}
	 }
	 onSelectFun=function() {
		 var selectedApp = $("#appsName option:selected" ).text();
		 if(selectedApp == "" || selectedApp == "Select Apps")
			 selectedApp = "CASSA Dashboard"
		 if(selectedApp){
			 var filterQuery = " app = '"+selectedApp+"'"
			 var request ={
					 source:'postgreSQL',
					 database:'E2EMFPostGres',
					 //table:'widget_config',
					 query:'select id,action,screen,widget,widget_group,seq,enabled,widget_display_name from widget_config where <FILTER> ORDER BY seq asc',
					 columns:'id,action,screen,widget,widget_group,enabled,widget_display_name',
					 filters:filterQuery,
					 groupByEnabled:true,
					 cacheDisabled:true
			 	};
			 enableLoading();
			 callAjaxService("getData", callBackSuccessGetData, callBackFailure,
					 request, "POST",null,true);
		 }
		 onChangeFun()
	 }
	 onChangeFun=function() {
		 var selectedApp = $("#rolesName").find("select").val();
		 selectedApp = $("#appsName").find("select").val()?selectedApp:""
		 if(selectedApp){
			 var filterQuery = " role = '"+selectedApp+"'"
			 
			 var requests = [{
					filters:filterQuery,
					id:"1",
					profilekey:"widgetGroup",
					table:"widget_config_logs",
					columns:'id,group_id,enabled',
					source:"postgreSQL",
					database:"E2EMFPostGres",
					groupByEnabled:false,
					cacheDisabled:true
				},{
					filters:filterQuery,
					id:"2",
					profilekey:"actionGroup",
					table:"widget_action_config_logs",
					columns:"id,action,role,enabled",
					source:"postgreSQL",
					database:"E2EMFPostGres",
					groupByEnabled:false,
					cacheDisabled:true
				}];
				var request ={
						"requests":JSON.stringify(requests)
				};
				enableLoading();
				callAjaxService("getMultipleData",roleCallBackSuccessGetData,callBackFailure,request,"POST",null,true);
			/* var request ={
					 source:'postgreSQL',
					 database:'E2EMFPostGres',
					 table:'widget_config_logs',
					 columns:'id,group_id,enabled',
					 filters:filterQuery,
					 groupByEnabled:false,
					 cacheDisabled:true
			 	};
			 enableLoading();
			 callAjaxService("getData", roleCallBackSuccessGetData, callBackFailure,
					 request, "POST",null,true);*/
		 }
	 }
	 var object={};
	 object.buttonWidth = '200px';
	 
	 constructDropDown("[name='apps']",[],false, 'CASSA Dashboard', onSelectFun, "Select App","","",object);
	 var callBackSuccessGetBusinessRole = function(response){
	 		THIS.allapps = {};
	 		var allRoles = [];
	 		if(response.data && response.data.length>0){
	 			for(var i=0;i<response.data.length;i++){
	 				allRoles.push({
	 					label: response.data[i].role,
	 					value: response.data[i].role 
	 				})
	 				if(!THIS.allapps[response.data[i].role]){
	 					THIS.allapps[response.data[i].role] = []
	 				}
	 				var apps = [];
	 				if(response.data[i].apps){
	 					apps = response.data[i].apps.split(",");
	 				}
	 				if(apps.length>0){
	 					for(var j=0;j<apps.length;j++){
	 						THIS.allapps[response.data[i].role].push({
	 							 label: apps[j],
	 							 value: apps[j] 
	 						});
	 					}
	 				}
	 			}
	 		}
	 		
	 		 constructDropDown("[name='roles']",allRoles,false, '', onChangeofRole, "Select Role","","",object);

	 
	 }
	 var onChangeofRole = function(){
		 var selectedRole = $("[name='roles']").find("select").val();
		 
		 if(THIS.allapps[selectedRole] && THIS.allapps[selectedRole].length>0){
			 constructDropDown("[name='apps']",THIS.allapps[selectedRole],false, '', onSelectFun, "Select App","","",object);
		 }
		 onSelectFun()
	 }
	 var businessRoleRequest = {
				/*source : "postgreSQL",
				database : "E2EMFPostGres",
				table:"business_role",
				columns:"name as rolename"*/
				source : "postgreSQL",
				database : "E2EMFPostGres",
				table:"role_app_mapping",
				columns:"role_id,role,apps"
		};
		callAjaxService("getData", callBackSuccessGetBusinessRole,callBackFailure, businessRoleRequest, "POST");
	
	
	 
	 
	 onSelectFun()
}
WidgetConfigInstance();