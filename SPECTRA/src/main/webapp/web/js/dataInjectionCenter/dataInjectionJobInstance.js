var loadPage = function() {
	pageSetUp();
	var filterParam = Jsonparse(localStorage["filtersForwaded"]);
	var jobDetails = {};
	constructDropDown(
			"#jobInstanceId",
			"SELECT distinct id,name FROM di_job_instance where name like '%<Q>%'",
			false, filterParam.job_instance_id, function(){getJobInstancedetails();}, "Select Instance",
			"postgreSQL", "E2EMFPostGres");
	constructDropDown(
			"#jobName",
			"select distinct id, name from di_cfg_job where cast(id as char) like '%<Q>%' or name like '%<Q>%' ",
			false, filterParam.job_id, null, "Select Job", "postgreSQL",
			"E2EMFPostGres");
	delete localStorage["filtersForwaded"];
	var dom = "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'><'col-sm-9 col-xs-9 filterdByDiv'>r<\"toolbar\">>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>>";
	/*
	 * $('#tableJobInstanceDetails').dataTable({ "dom":dom, "data": [],
	 * "columns": [] } );
	 */
	$('#dataEdlFiles').dataTable({
		"dom" : dom,
		"data" : [],
		"columns" : [ {
			"data" : "name"
		}, {
			"data" : "size"
		}, {
			"data" : "last_modified_time"
		} ]
	});

	$(".jarviswidget-toggle-btn").unbind('click').bind('click', function() {
		$(this).find("i").toggleClass("fa-minus fa-plus");
		$(this).parents("header").next().toggleClass("hide");
	});
	$("#runJob").unbind("click").bind("click", function() {
		runJob();
	});
	$("#purge").unbind("click").bind("click", function() {
		purgeJob();
	});
	/*
	 * $("#staging").on( "blur", function(){ var staginginput =
	 * $("#staging").val(); if(staginginput === "hadoop/hdfs/one" ||
	 * staginginput === "hadoop/hdfs/multi"){ getStagingFiles(); } });
	 */
	var ribanName = lookUpTable.get("dataInjectionCenter");
	if (ribanName) {
		$("#ribbonname").html(ribanName);
	} else {
		$("#ribbonname").html("");
	}
	var progressBar;// setInterval(progressBarFunc, 1000);
	var logInterval;
	var logCount = 0;
	var baseUrl1 = "https://192.168.101.41:5005/";
	var baseUrl = lookUpTable.get("hadoopapi");// https://hadoopapi.relevancelab.com/
	var progressFrequency = 0;
	var prograssBarValue = 0;
	var purgeJob = function() {
		var ids = [];
		var request = {
			dir_name : "/sch/fscmtopmodelam_costtransactionam_costtransactionpvo"
		};
		$("#tableJobInstanceDetails").find("[name='jobinstance']").each(
				function() {
					if ($(this).is(":checked")) {
						ids.push($(this).attr("id"));
					}
				});
		$("#tableSourceLoadQueue").find("tr").each(
				function() {
					var data = $(this).data(), rowdata = {};
					if (data.request) {
						rowdata = Jsonparse(data.request);
					}
					if (rowdata && rowdata.target_folder_path
							&& ids.indexOf(rowdata.job_instance_id) > -1) {
						request.dir_name = rowdata.target_folder_path;
					}
				});
		enableLoading();
		$.ajax({
			type : "DELETE",
			crossDomain : true,
			url : baseUrl + "hadoop/hdfs",
			// staging: "https://192.168.101.41:5005/local/"+type
			data : JSON.stringify(request),
			processData : false,
			contentType : 'application/json'
		}).done(function(response) {
			disableLoading();
			//getLogsBYRequest(sourceFiles);
			progressBar = setInterval(getLogsBYRequest, 5000);
			//getFiles();
		});

	};
	var runJob = function() {
		enableLoading();
		var jobId = $("#jobName").find("select").val();
		var instanceId = $("#jobInstanceId").find("select").val();
		if (!jobId || !instanceId) {
			showNotification("notify", "Please Select a Job Instance.");
			return;
		}
		var targetObjects = [
				{
					"inputfile" : "file_fscmtopmodelam_costtransactionam_costtransactionpvo-batch2044181594-20200515_155342.zip",
					"suffix" : "",
					"prefix" : "",
					"loadtype" : "",
					"targetDir" : "/sch/test1/",
					"overwriteType" : "force",
					"cleanDir" : false
				},
				{
					"inputfile" : "file_fscmtopmodelam_costtransactionam_costtransactionpvo-batch2044181594-20200515_160052.zip",
					"suffix" : "",
					"prefix" : "",
					"loadtype" : "",
					"targetDir" : "/sch/test2/",
					"overwriteType" : "force",
					"cleanDir" : false
				} ];
		targetObjects = [];
		var ids = [];
		var sourceFiles = [];
		$("#tableJobInstanceDetails").find("[name='jobinstance']").each(
				function() {
					if ($(this).is(":checked")) {
						var data = $(this).parents("tr").data();
						data = Jsonparse(data.request);
						ids.push(data.source_datastore);
					}
				});
		$("#tableSourceLoadQueue").find("tr").each(
				function() {
					var data = $(this).data(), rowdata = {};
					if (data.request) {
						rowdata = Jsonparse(data.request);
					}
					if (rowdata && Object.keys(rowdata).length > 0
							&& ids.indexOf(rowdata.data_store_key) > -1) {
						sourceFiles.push(rowdata.data_store_key);
						targetObjects.push({
							"inputfile" : rowdata.source_file_name,
							"suffix" : "",
							"prefix" : "",
							"loadtype" : "",
							"targetDir" : rowdata.target_folder_path,
							"overwriteType" : "force",
							"cleanDir" : false
						});
					}
				});
		var request = {
			"instanceId" : instanceId,
			"jobId" : jobId,
			"targetObjects" : targetObjects
		};
		request = JSON.stringify(request);
		$.ajax({
			type : "POST",
			url : baseUrl + "job/oracle",
			data : request,
			processData : false,
			contentType : 'application/json'
		}).done(function(response) {
			disableLoading();
			jobDetails = response;
			//getLogsBYRequest(sourceFiles);
			progressBar = setInterval(getLogsBYRequest, 5000);
			//getFiles();
		}).fail(function(res) {
			disableLoading();
		});
	};
	var getLogsBYRequest = function(targetdata) {
		var jobId = $("#jobName").find("select").val();
		var instanceId = $("#jobInstanceId").find("select").val();
		enableLoading();
		$.ajax({
					type : "GET",
					url : baseUrl + "log/"+jobId+"/"+instanceId,
					data : {}
				})
				.done(
						function(response) {
							disableLoading();
							console.log(response);
							var taskStatus = "Completed";
							jobDetails = response;
							if (jobDetails && jobDetails.tasks.length > 0) {
								
								for (var i = 0; i < jobDetails.tasks.length; i++) {
									var sourceFileName = Object.keys(jobDetails.tasks[i]);
									sourceFileName = sourceFileName[0];
									if (jobDetails.tasks[i]&& jobDetails.tasks[i][sourceFileName]) {
										$("#logsdiv").html("");
										var responseData = jobDetails.tasks[i][sourceFileName];
										if (responseData.fileName) {
											$("#logsdiv")
													.append(
															$("<div/>")
																	.html(
																			"File Name : "
																					+ responseData.fileName));
										}
										if (responseData.request_id) {
											$("#logsdiv")
													.append(
															$("<div/>")
																	.html(
																			"Request Id : "
																					+ responseData.request_id));
										}
										if (responseData.task_status) {
											taskStatus = responseData.task_status;
											$("#logsdiv")
													.append(
															$("<div/>")
																	.html(
																			"Status : "
																					+ responseData.task_status));
										}
										if (responseData.log) {
											$("#logsdiv")
													.append(
															$("<div/>")
																	.html(
																			"Logs : "
																					+ responseData.log));
										}
										if (responseData.percent_complete) {
											$("#progressBarcompletion")
													.html(
															responseData.percent_complete
																	+ "% Completed");
											$(".progress")
													.find(".progress-bar")
													.css(
															{
																"width" : responseData.percent_complete
																		+ "%"
															})
													.attr(
															{
																"aria-valuenow" : responseData.percent_complete
															});
										}
										// $("#widgetMVAApis").find("h2").html()
									}
								}
								if ($("#logContainer").is(":visible")) {
									$("#labelText")
											.html(
													"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Hide Details</a>");
								} else {
									$("#labelText")
											.html(
													"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Show Details</a>");
								}
								$("#logs_div")
										.unbind('click')
										.bind(
												'click',
												function() {
													$("#logContainer").toggle();
													if ($(this).text() == "Show Details")
														$(this).text(
																"Hide Details");
													else
														$(this).text(
																"Show Details");
												});
							}
							if(taskStatus === "Completed" || taskStatus === "Failed"){
								clearInterval(progressBar);
								$(".progress").find(".progress-bar").css({"width" : "100%"}).attr({"aria-valuenow" : "100"});
								disableLoading();
							}else{
								//progressBar = setInterval(getLogsBYRequest, 5000);
							}
							
						});
	};
	var showLogs =function(sourceFileName){
		if (jobDetails && jobDetails.tasks.length > 0) {
			for (var i = 0; i < jobDetails.tasks.length; i++) {
				if (jobDetails.tasks[i]&& jobDetails.tasks[i][sourceFileName]) {
					$("#logsdiv").html("");
					var responseData = jobDetails.tasks[i][sourceFileName];
					if (responseData.fileName) {
						$("#logsdiv").append($("<div/>").html("File Name : "+ responseData.fileName));
					}
					if (responseData.request_id) {
						$("#logsdiv").append($("<div/>").html("Request Id : "+ responseData.request_id));
					}
					if (responseData.task_status) {
						taskStatus = responseData.task_status;
						$("#logsdiv").append($("<div/>").html("Status : "+ responseData.task_status));
					}
					if (responseData.log) {
						$("#logsdiv").append($("<div/>").html("Logs : "+ responseData.log));
					}
					if (responseData.percent_complete) {
						$("#progressBarcompletion").html(responseData.percent_complete+ "% Completed");
						$(".progress").find(".progress-bar").css({"width" : responseData.percent_complete+ "%"}).attr({"aria-valuenow" : responseData.percent_complete});
					}
					// $("#widgetMVAApis").find("h2").html()
				}
			}
			if ($("#logContainer").is(":visible")) {
				$("#labelText")
						.html(
								"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Hide Details</a>");
			} else {
				$("#labelText")
						.html(
								"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Show Details</a>");
			}
			$("#logs_div")
					.unbind('click')
					.bind(
							'click',
							function() {
								$("#logContainer").toggle();
								if ($(this).text() == "Show Details")
									$(this).text(
											"Hide Details");
								else
									$(this).text(
											"Show Details");
							});
		}
	};
	var getLogs = function(logId) {
		return;
		// /log/1761120200515011451_o
		$
				.ajax({
					type : "GET",
					url : baseUrl + "log/" + logId,
					data : {}
				})
				.done(
						function(response) {
							var logs = "";
							$("#logsdiv").html("");
							if (response.request_id) {
								$("#logsdiv").append(
										$("<div/>").html(
												"Request Id : "
														+ response.request_id));
							}
							if (response.start_date) {
								$("#logsdiv").append(
										$("<div/>").html(
												"Start Time : "
														+ response.start_date));
							}
							if (response.satus) {
								$("#logsdiv").append(
										$("<div/>").html(
												"Status : " + response.satus));
							}
							if (response.log) {
								$("#logsdiv").append(
										$("<div/>").html(
												"Logs : " + response.log));
							}
							if (response.satus === "completed") {
								clearInterval(logInterval);
							}
							if (logCount > 10) {
								clearInterval(logInterval);
							}
							if ($("#logContainer").is(":visible")) {
								$("#labelText")
										.html(
												"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Hide Details</a>");
							} else {
								$("#labelText")
										.html(
												"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Show Details</a>");
							}
							$("#logs_div").unbind('click').bind('click',
									function() {
										$("#logContainer").toggle();
										if ($(this).text() == "Show Details")
											$(this).text("Hide Details");
										else
											$(this).text("Show Details");
									});
						}).fail(function(res) {
					console.log(res);
				});
	};
	var progressBarFunc = function() {
		// value,frequency
		var value = parseFloat(prograssBarValue);
		var inputType = $("#staging").val().split('/')[3];
		/*
		 * if(inputType == 'multi') value = value+2; else value = value+15;
		 */
		value = value + progressFrequency;// 0.55;
		if (value > 100)
			$("#progressBarcompletion").html("100% Completed");
		else
			$("#progressBarcompletion").html(Math.round(value) + "% Completed");
		prograssBarValue = value;
		$(".progress").find(".progress-bar").css({
			"width" : value + "%"
		}).attr({
			"aria-valuenow" : value
		});

	};
	var callBackGetJobInstancedetails = function() {
		getSourcequeueData();
		$("#tableJobInstanceDetails").find("[name='jobinstance']").on("change",function() {
					// getSourcequeueData();
		});
		$("#tableJobInstanceDetails").find("[name='refreshimpala']").unbind("click").bind("click",function() {
			var data = $(this).parents("tr").data();
			data = Jsonparse(data.request);
			refreshImpala(data);
		});
		$("#tableJobInstanceDetails").find("[name='sourceDatstore']").unbind("click").bind("click",function() {
			var data = $(this).parents("tr").data();
			data = Jsonparse(data.request);
			getSourcequeueData(data);
		});
		
		/*
		 * $("#tableJobInstanceDetails").find("[name='jobinstance']").bind("click",function(){
		 * var data = $(this).parents("tr").data(); data =
		 * Jsonparse(data.request); //$("#staging").val(data.source_file_name);
		 * //$("#edlTarget").val(data.target_obj_name);
		 * getSourcequeueData(data); });
		 */
	};
	var getJobInstancedetails = function() {
		filterParam
		var filter =" 1=1 ";
		var instanceId = $("#jobInstanceId").find("select").val() || filterParam.job_instance_id;
		if(instanceId){
			filter +=" and job_instance_id ='"+instanceId+"'";
		}
		var columnConfig = [
				{
					"data" : "id",
					"title" : "Select",
					"render" : function(data) {
						return "<label class='checkbox'><input  id='"
								+ data
								+ "' name ='jobinstance'  type='checkbox' field='active' fieldtitle='Active'><i></i></label>"
					}
				}, {
					"data" : "refresh_type",
					"render": function(data) {
						return "<a name='refreshimpala' class='btn btn-success border-round'><i class='fa fa-refresh'></i></a>";
					},
					"title" : "Refresh"
				} , {
					"data" : "job_instance_id",
					"title" : "Job Instance Id"
				}, {
					"data" : "name",
					"title" : "Name"
				}, {
					"data" : "target_obj_id",
					"title" : "Target Obj Id"
				}, {
					"data" : "target_obj_name",
					"title" : "Target Obj Name"
				},{
					"data":"source_datastore",
					"title":"Source Datastore",
					"render" : function(data) {
						return data ? "<a name='sourceDatstore'>" + data + "</a>" : ""
					}
				},  {
					"data" : "source_file_name",
					"title" : "Source File Name"
				},{
					"data" : "source_checksum",
					"title" : "Source Checksum"
				}, {
					"data" : "source_status_json",
					"title" : "Source Status Json"
				}, {
					"data" : "source_mf_file",
					"title" : "Source MF File"
				}, {
					"data" : "source_extract_json",
					"title" : "Source Extract Json"
				}, {
					"data" : "log",
					"title" : "Log"
				}, {
					"data" : "status",
					"title" : "Status"
				}, {
					"data" : "source_last_refresh",
					"title" : "Last Refresh"
				}, {
					"data" : "active",
					"title" : "Active"
				}, {
					"data" : "source_row_count",
					"title" : "Source Row Count"
				}, {
					"data" : "target_last_load",
					"title" : "Last Load"
				}, {
					"data" : "target_row_count",
					"title" : "Target Row Count"
				}];

		var config = {
			html : '#tableJobInstanceDetails',
			url : serviceCallMethodNames["getDataPagination"].url,
			source : "postgreSQL",
			filter : filter,
			database : "E2EMFPostGres",
			table : "vw_job_instance_details",
			columns : columnConfig,
			callBack : callBackGetJobInstancedetails,
			// drilldowncallback:callBackGetDrillDown,
			dataColumns : $.map(columnConfig, function(val) {
				return val.data;
			}).join(',')
		};
		constructGrid(config);

	};
	var callBackGetSourcequeueData = function() {
		$("#tableSourceLoadQueue").find("tr").find("[name='sourcefilename']").bind("click", function() {
			var data = $(this).parents("tr").data();
			data = Jsonparse(data.request);
			showLogs(data.source_file_name);
			// $("#staging").val(data.data_store_key);
			// $("#edlTarget").val(data.target_obj_name);
			//getLogsBYRequest(data);
			//progressBar = setInterval(getLogsBYRequest, 1000,data);
		});
		$("#tableSourceLoadQueue").find("tr").find("[name='targetfolderpath']").bind("click", function() {
			var data = $(this).parents("tr").data();
			data = Jsonparse(data.request);
			if(data && data.target_folder_path){
				getFiles(data.target_folder_path);
			}
		});
	};
	var getSourcequeueData = function(request) {
		var ids = [];
		var filter = " 1=1 ";
		if(request && request.source_datastore){
			filter +=" and data_store_key='"+request.source_datastore+"'";
		}
		var instanceId = $("#jobInstanceId").find("select").val();
		if(instanceId){
			filter +=" and job_instance_id ='"+instanceId+"'";
		}
		var columnConfig = [ {
			"data" : "id",
			"title" : "Id"
		}, {
			"data" : "data_store_key",
			"title" : "Data  Store Key"
		}, {
			"data" : "source_job_id",
			"title" : "Source Job Id"
		}, {
			"data" : "source_batch_id",
			"title" : "Source Batch Id"
		}, {
			"data" : "source_ess_id",
			"title" : "Source Ess Id"
		}, {
			"data" : "source_file_name",
			"title" : "Source File Name",
			"render" : function(data) {
				return data ? "<a name='sourcefilename'>" + data + "</a>" : "";
			}
		}, {
			"data" : "target_folder_path",
			"title" : "Target Folder Path",
			"render" : function(data) {
				return data ? "<a name='targetfolderpath'>" + data + "</a>" : "";
			}
		}, {
			"data" : "source_checksum",
			"title" : "Source Checksum"
		}, {
			"data" : "source_status_json",
			"title" : "Source Status Json"
		}, {
			"data" : "source_mf_file",
			"title" : "Source MF File"
		}, {
			"data" : "source_extract_json",
			"title" : "Source Extract Json"
		}, {
			"data" : "status",
			"title" : "Status"
		}, {
			"data" : "job_instance_id",
			"title" : "Job Instance Id"
		} ];
		if (ids && ids.length > 0) {
			// filter = " job_instance_id in ("+ids.map(function(o){return
			// "'"+o+"'"}).toString()+") ";
		}
		var config = {
			html : '#tableSourceLoadQueue',
			url : serviceCallMethodNames["getDataPagination"].url,
			source : "postgreSQL",
			filter : filter,
			database : "E2EMFPostGres",
			table : "di_source_load_queue",
			columns : columnConfig,
			callBack : callBackGetSourcequeueData,
			// drilldowncallback:callBackGetDrillDown,
			dataColumns : $.map(columnConfig, function(val) {
				return val.data;
			}).join(',')
		};
		constructGrid(config);
	};
	function bytesToSize(bytes) {
		var sizes = [ 'Bytes', 'KB', 'MB', 'GB', 'TB' ];
		if (bytes == 0)
			return '0 Byte';
		var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
	var refreshImpala = function(data) {
		prograssBarValue = 0;
		progressFrequency = 100 / 180;
		//progressBar = setInterval(progressBarFunc, 1000);
		$("#labelText").html("Data Refresh In-Progress..");
		var request = {
			"table_name":"rlsf_stg."+data.target_obj_name
		};
		request = JSON.stringify(request);
		$.ajax({
					type : "PUT",
					crossDomain : true,
					url : baseUrl + "impala/refresh",
					data : request,
					processData : false,
					contentType : 'application/json'
				})
				.done(
						function(response) {
							var date = new Date("12/12/2020 "
									+ response.message);
							var timeTaken = "";
							if (date.getHours()) {
								timeTaken += date.getHours() + " Hour,";
							} else {
								// timeTaken += "00:";
							}
							if (date.getMinutes()) {
								timeTaken += date.getMinutes() + " Minute(s), ";
							} else {
								timeTaken += "0 Minute(s), ";
							}
							if (date.getSeconds()) {
								timeTaken += date.getSeconds() + " Second(s), ";
							} else {
								timeTaken += "0 Second(s), ";
							}
							if (date.getMilliseconds()) {
								timeTaken += date.getMilliseconds()
										+ " Millisecond(s).";
							} else {
								timeTaken += " 0 Millisecond(s).";
							}
							showNotification("success",
									"Time taken to refresh : " + timeTaken);
							$("#refreshMessage").html(
									"Time taken to refresh : " + timeTaken);
							$("#ingest,#purge").removeAttr("disabled");
							$(".progress").find(".progress-bar").css({
								"width" : "100%"
							}).attr({
								"aria-valuenow" : "100"
							});
							$("#progressBarcompletion").html("100% Completed");
							if ($("#logContainer").is(":visible")) {
								$("#labelText")
										.html(
												"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Hide Details</a>");
							} else {
								$("#labelText")
										.html(
												"Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Show Details</a>");
							}

							$("#logs_div").unbind('click').bind('click',
									function() {
										$("#logContainer").toggle();
										if ($(this).text() == "Show Details")
											$(this).text("Hide Details");
										else
											$(this).text("Show Details");
									});
							callAjaxService("clearCache",function(){},callBackFailure,null, "POST");
							//clearInterval(progressBar);
							// getFiles();
						}).fail(function() {

				});

	};
	var getFiles = function(tagetFolderName) {
		var dom = "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'><'col-sm-9 col-xs-9 filterdByDiv'>r<\"toolbar\">>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>>";
		var request = {
			dir_name : tagetFolderName.trim()//"/sch/fscmtopmodelam_costtransactionam_costtransactionpvo"
		};
		/*$("#tableJobInstanceDetails").find("[name='jobinstance']").each(
				function() {
					if ($(this).is(":checked")) {
						ids.push($(this).attr("id"));
					}
		});
		$("#tableSourceLoadQueue").find("tr").each(
				function() {
					var data = $(this).data(), rowdata = {};
					if (data.request) {
						rowdata = Jsonparse(data.request);
					}
					if (rowdata && rowdata.target_folder_path
							&& ids.indexOf(rowdata.job_instance_id) > -1) {
						request.dir_name = rowdata.target_folder_path;
					}
		});*/
		$.ajax({
					type : "POST",
					crossDomain : true,
					url : baseUrl + "hadoop/hdfs",
					// staging: "https://192.168.101.41:5005/local/"+type
					data : JSON.stringify(request),
					processData : false,
					contentType : 'application/json'
				})
				.done(
						function(response) {
							$("#edlFileDetails").find("h2").html("EDL File Details :("+tagetFolderName+")");
							if ($.fn.DataTable.isDataTable('#dataEdlFiles')) {
								$('#dataEdlFiles').DataTable().destroy();
							}
							$('#dataEdlFiles')
									.dataTable(
											{
												"dom" : dom,
												"data" : response,
												"columns" : [
														{
															"data" : "name"
														},
														{
															"data" : "size",
															"render" : function(
																	data) {
																return data ? bytesToSize(data)
																		: ""
															}
														},
														{
															"data" : "last_modified_time",
															"render" : function(
																	data) {
																return data ? getManipulatedDate(
																		null,
																		0,
																		data * 1000,
																		"%d %b %Y %H:%M:%S")
																		: ""
															}
														}, ]
											});
							// getTotalCount();
							// clearInterval(progressBar);

						});
	};
	var getTotalCount = function() {
		callAjaxService(
				"clearCache",
				function() {
					var request = {
						"source" : "hive",
						"database" : "FRV_DB",
						"groupByEnabled" : true,
						"filters" : " 1 =1 ",
						"query" : "select count(*) as _count from frv_tras_parq_final where <FILTER> "
					};
					callAjaxService("getData", function(response) {
						// getStagingFiles();
						if (response && response.isException) {
							showNotification("error", response.customMessage);
							return;
						} else {
							if (response && response.data
									&& response.data.length > 0) {
								var totalCount = response.data[0]["_count"];
								if (totalCount)
									$("#recordsValue").html(
											d3.format(",.0f")(totalCount));
								else
									$("#recordsValue").html("0");
							}
						}
					}, callBackFailure, request, "POST", null, true);
				}, callBackFailure, null, "POST");

	};
	getJobInstancedetails();
	// getFiles();
	// getStagingFiles();
}

loadPage();