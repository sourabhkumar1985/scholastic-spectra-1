var loadPage = function(){
	pageSetUp();
	var baseUrl = lookUpTable.get("hadoopapi");
	var dom = "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'><'col-sm-9 col-xs-9 filterdByDiv'>r<\"toolbar\">>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>>";
	$('#dataEdlFiles').dataTable({
		//"dom":dom,
	    "data": [],
	    "columns": [
		        // { "data": "Job Id" },
		        // { "data": "Schedule Id" },
		         { "data": "File Name" },
		         { "data": "File Size" },
		         {"data":"lastUpdateDate"}
	    ]
	} );
	constructDropDown("#container",[],false, "", getOracleCloudData, "Select Container", "postgreSQL", "QFIND_RA",{buttonWidth : "350px"});
	$("#filterbtn").unbind("click").bind("click",function(){
		getOracleCloudData();
	});
	
	function bytesToSize(bytes) {
		   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		   if (bytes == 0) return '0 Byte';
		   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
	////https://uscom-east-1.storage.oraclecloud.com/v1/Storage-schlprodpaas/BI_CON/file_fscmtopmodelam_costtransactionam_costtransactionpvo-batch2044181594-20200515_155342.zip
	var downloadFile = function(fileName){
		var url = baseUrl+"stream/";//BI_CON/EXTRACT_STATUS_DATA_22-4058231-20200520_060702.JSON
		var container = $("#container").find("select").val();
		url +=container+"/"+fileName;
		$.fileDownload(url)
	    .done(function () { alert('File download a success!'); })
	    .fail(function () { alert('File download failed!'); });
	};
	var onChangeOfContainer = function(){
		getOracleCloudData();
	};
	var getContainer = function(){
		/*var data = [{value: "BI_CON",label:  "BI_CON"},{value:"Fin_Recon_Storage",label:"Fin_Recon_Storage"}];
		constructDropDown("#container",data,false, "BI_CON", onChangeOfContainer, "Select Container", "postgreSQL", "QFIND_RA",{buttonWidth : "350px"});
		getOracleCloudData();*/
		enableLoading();
		$.ajax({
			type : "GET",
			crossDomain:true,
			url : baseUrl+"container/oracle",
			data :
			{ 
				}
		}).done(function(response) {
			disableLoading();
			console.log(response);
			var data = [];
			if(response && response.Containers && response.Containers.length>0){
				response.Containers[0] = response.Containers[0].replace("b'","");
				for(var i=0;i<response.Containers.length;i++){
					data.push({
						value: response.Containers[i] || '?',
                        label: response.Containers[i],
                        title: response.Containers[i]
					});
				}
			}
			constructDropDown("#container",data,false, "BI_CON", getOracleCloudData, "Select Container", "postgreSQL", "QFIND_RA",{buttonWidth : "350px"});
			getOracleCloudData();
		}).fail(function() {
		});
	};
	var getOracleCloudData = function(containerName){
		var containerName = $("#container").find("select").val();
		$("#widgetComponent").find("h2").html("Oracle Storage Cloud "+containerName);
		//var baseUrl = "https://hadoopapi.relevancelab.com/files/oracle";//"http://192.168.101.41:8080/api/oracle";
		enableLoading();
		$.ajax({
			type : "GET",
			crossDomain:true,
			url : baseUrl+"files/oracle/"+containerName,
			data :
			{ 
				}
		}).done(function(response) {
			disableLoading();
			if ( $.fn.DataTable.isDataTable('#dataEdlFiles') ) {
				  $('#dataEdlFiles').DataTable().destroy();
			}
			$('#dataEdlFiles').dataTable({
				//"dom" :dom,
			    "data": response,
			    "columns": [
				         { "data": "fileName","render":function(data){
				        	 return data?"<a filename='"+data+"' name='downloadfile'>"+data+"</a>":"" 
				        		 }},//,"render":function(data){return data?"<a >"+data+"</a>":"" }
				         { "data": "fileSize","render":function(data){return data?bytesToSize(data):"" }},
				         { "data": "lastUpdateDate"},
			    ]
			} );
			$('#dataEdlFiles').on('draw.dt', function() {
				$("[name='downloadfile']").unbind("click").bind("click",function(){
					var fileName = $(this).attr("filename");
					downloadFile(fileName);
				});
			});
			$("[name='downloadfile']").unbind("click").bind("click",function(){
				var fileName = $(this).attr("filename");
				downloadFile(fileName);
			});
		}).fail(function() {
		});
		
	};
	getContainer();
}

loadPage();