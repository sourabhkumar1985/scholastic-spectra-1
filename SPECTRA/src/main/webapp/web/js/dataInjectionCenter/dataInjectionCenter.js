var loadPage = function(){
	pageSetUp();
	var dom = "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'><'col-sm-9 col-xs-9 filterdByDiv'>r<\"toolbar\">>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>>";
	$('#dataEdlFiles').dataTable({
		"dom":dom,
	    "data": [],
	    "columns": [
		         { "data": "name" },
		         { "data": "size" },
		         { "data": "last_modified_time" }
	    ]
	} );
	$('#datastagingFiles').dataTable({
	    "data": [],
	    "dom":dom,
	    "columns": [
		         { "data": "name" },
		         { "data": "size" },
		         { "data": "last_modified_time" }
	    ]
	} );
	
	$(".jarviswidget-toggle-btn").unbind('click').bind('click',function(){
		$(this).find("i").toggleClass("fa-minus fa-plus");
		$(this).parents("header").next().toggleClass("hide");
	});
	
	$("#staging").on( "blur", function(){
		var staginginput = $("#staging").val();
		if(staginginput === "hadoop/hdfs/one" || staginginput === "hadoop/hdfs/multi"){
			getStagingFiles();
		}
	});
	var ribanName = lookUpTable.get("dataInjectionCenter");
	if(ribanName){
		$("#ribbonname").html(ribanName);
	}else{
		$("#ribbonname").html("");
	}
	var progressBar;//setInterval(progressBarFunc, 1000);
	var baseUrl1 = "https://192.168.101.41:5005/";
	var baseUrl = "https://hadoopapi.relevancelab.com/";
	var progressFrequency = 0;
	var prograssBarValue = 0;
	$("#purge").unbind("click").bind("click",function(){
		var staginginput = $("#staging").val();
		var edlTarget = $("#edlTarget").val();
		var tableObject = $("#tableObject").val();
		if(staginginput == ""){
			showNotification("warning","Please enter Staging folder Path");
			return;
		}	
		prograssBarValue = 0;
		progressFrequency = 100/60;
		progressBar = setInterval(progressBarFunc, 1000);
		$("#logContainer").hide();
		$("#ingest,#purge").attr("disabled","disabled");
		$("#labelText").html("Data Deletion In-Progress..");
		$.ajax({
			type : "DELETE",
			crossDomain:true,
			url : baseUrl+"hadoop/hdfs", //"http://192.168.101.41:5005/local/multi",
			data :
			{
				}
		}).done(function(response) {
			showNotification("success", "Time taken to delete : 0 Minute(s),1 Second(s),73 Millisecond(s).");
			$("#processMessage").html("Time taken to delete : 0 Minute(s),1 Second(s),73 Millisecond(s).");
			clearInterval(progressBar);
			$("#logsdiv").html("");
			$("#labelText").html("Data Deletion Completed.");
			$(".progress").find(".progress-bar").css({"width":"100%"}).attr({"aria-valuenow":"100"});
			$("#progressBarcompletion").html("100% Completed");
			$("#logsdiv").html("No Logs");
			refreshImapal();
		}).fail(function(res) {
			//showNotification("warning", "No Files To Delete.");
			clearInterval(progressBar);
			$("#labelText").html("No Files To Delete.");
			$(".progress").find(".progress-bar").css({"width":"0%"}).attr({"aria-valuenow":"0"});
			$("#progressBarcompletion").html("&nbsp;");
			$("#ingest,#purge").removeAttr("disabled");
			//refreshImapal();
		});	
	});
	
	$("#ingest").unbind("click").bind("click",function(){
		var staginginput = $("#staging").val();
		var edlTarget = $("#edlTarget").val();
		var tableObject = $("#tableObject").val();
		prograssBarValue = 0;
		if(staginginput == ""){
			showNotification("warning","Please enter Staging folder Path");
			return;
		}
		$("#ingest,#purge").attr("disabled","disabled");
		progressFrequency = 100/60;
		progressBar = setInterval(progressBarFunc, 1000);
		$("#logContainer").hide();
		$("#labelText").html("Data Ingestion and Processing In-Progress....");
		$.ajax({
			type : "PUT",
			url : baseUrl+staginginput,
			data :
			{
				}
		}).done(function(response) {
			var date = new Date("12/12/2020 "+response.message);
			var timeTaken = "";
			if(date.getHours()){
				timeTaken += date.getHours()+" Hour,";
			}else{
				//timeTaken += "00:";
			}
			if(date.getMinutes()){
				timeTaken += date.getMinutes()+" Minute(s), ";
			}else{
				timeTaken += "0 Minute(s), ";
			}
			if(date.getSeconds()){
				timeTaken += date.getSeconds()+" Second(s), ";
			}else{
				timeTaken += "0 Second(s), ";
			}
			if(date.getMilliseconds()){
				timeTaken += date.getMilliseconds()+" Millisecond(s).";
			}else{
				timeTaken += " 0 Millisecond(s).";
			}
			$("#processMessage").html("Time taken to ingest : "+ timeTaken);
			showNotification("success", "Time taken to ingest : "+ timeTaken);
			clearInterval(progressBar);
			var logs = "";
			$("#logsdiv").html("");
			$("#labelText").html("Data Ingestion and Processing completed.");
			$(".progress").find(".progress-bar").css({"width":"100%"}).attr({"aria-valuenow":"100"});
			$("#progressBarcompletion").html("100% Completed");
			if(response.put_time){
				logs = response.put_time.replaceAll("\\n","</br>");
				logs = logs.replaceAll("\\t","&nbsp;:&nbsp");
			}
			$("#logsdiv").html(logs);
			refreshImapal();
		}).fail(function(res) {
			console.log(res);
		});		
	});
	
	var progressBarFunc = function(){
		//value,frequency
		var value = parseFloat(prograssBarValue);
		var inputType = $("#staging").val().split('/')[3];
		/*if(inputType == 'multi')
			value = value+2;
		else
			value = value+15;*/
		value = value+progressFrequency;//0.55;
		if(value>100)
			$("#progressBarcompletion").html("100% Completed");
		else
			$("#progressBarcompletion").html(Math.round(value)+"% Completed");
		prograssBarValue = value;
		$(".progress").find(".progress-bar").css({"width":value + "%"}).attr({"aria-valuenow":value});
		
	};
	var getStagingFiles = function(){
		var staginginput = $("#staging").val();
		var type  = "multi";
		var dom = "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'><'col-sm-9 col-xs-9 filterdByDiv'>r<\"toolbar\">>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>>";
		if(staginginput.indexOf("one")>-1){
			type = "one";
		}
		$.ajax({
			type : "GET",
			crossDomain:true,
			url : baseUrl+"local/"+type,
			//staging: "https://192.168.101.41:5005/local/"+type
			data :
			{
				}
		}).done(function(response) {
			if ( $.fn.DataTable.isDataTable('#datastagingFiles') ) {
				  $('#datastagingFiles').DataTable().destroy();
			}
			$('#datastagingFiles').dataTable({
				"dom":dom,
			    "data": response,
			    "columns": [
				         { "data": "name" },
				         { "data": "size","render":function(data){return data?bytesToSize(data):""}},
				         { "data": "last_modified_time","render":function(data){return data?getManipulatedDate(null,0,data*1000,"%d %b %Y %H:%M:%S"):"" } },
			    ]
			} );
			// getpagination();
			//clearInterval(progressBar);
			
		}).fail(function(res) {
			console.log(res);
		});
	};
	var getFiles = function(){
		var staginginput = $("#staging").val();
		var type  = "multi";
		var dom = "<'dt-toolbar'<'col-xs-4 col-sm-4 searchBox'><'col-sm-9 col-xs-9 filterdByDiv'>r<\"toolbar\">>t<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i>>";
		if(staginginput.indexOf("one")>-1){
			type = "one";
		}
		$.ajax({
			type : "GET",
			crossDomain:true,
			url : baseUrl+"hadoop/hdfs",
			//staging: "https://192.168.101.41:5005/local/"+type
			data :
			{
				}
		}).done(function(response) {
			if ( $.fn.DataTable.isDataTable('#dataEdlFiles') ) {
				  $('#dataEdlFiles').DataTable().destroy();
			}
			$('#dataEdlFiles').dataTable({
				"dom" :dom,
			    "data": response,
			    "columns": [
				         { "data": "name" },
				         { "data": "size","render":function(data){return data?bytesToSize(data):"" }},
				         { "data": "last_modified_time","render":function(data){return data?getManipulatedDate(null,0,data*1000,"%d %b %Y %H:%M:%S"):"" } },
			    ]
			} );
			getTotalCount();
			//clearInterval(progressBar);
			
		});
	};
	function bytesToSize(bytes) {
		   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		   if (bytes == 0) return '0 Byte';
		   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
	var refreshImapal = function(){
		prograssBarValue = 0;
		progressFrequency = 100/180;
		progressBar = setInterval(progressBarFunc, 1000);
		$("#labelText").html("Data Refresh In-Progress..");
		$.ajax({
			type : "POST",
			crossDomain:true,
			url : baseUrl+"impala/refresh",
			data :
			{ 
				}
		}).done(function(response) {
			var date = new Date("12/12/2020 "+response.message);
			var timeTaken = "";
			if(date.getHours()){
				timeTaken += date.getHours()+" Hour,";
			}else{
				//timeTaken += "00:";
			}
			if(date.getMinutes()){
				timeTaken += date.getMinutes()+" Minute(s), ";
			}else{
				timeTaken += "0 Minute(s), ";
			}
			if(date.getSeconds()){
				timeTaken += date.getSeconds()+" Second(s), ";
			}else{
				timeTaken += "0 Second(s), ";
			}
			if(date.getMilliseconds()){
				timeTaken += date.getMilliseconds()+" Millisecond(s).";
			}else{
				timeTaken += " 0 Millisecond(s).";
			}
			showNotification("success", "Time taken to refresh : "+ timeTaken);
			$("#refreshMessage").html("Time taken to refresh : "+ timeTaken);
			$("#ingest,#purge").removeAttr("disabled");
			$(".progress").find(".progress-bar").css({"width":"100%"}).attr({"aria-valuenow":"100"});
			$("#progressBarcompletion").html("100% Completed");
			if($("#logContainer").is(":visible")){
				$("#labelText").html("Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Hide Details</a>");
			}else{
				$("#labelText").html("Data Refresh completed. <a href='#' class='logsDiv' id='logs_div'>Show Details</a>");
			}
			
			$("#logs_div").unbind('click').bind('click',function(){
				$("#logContainer").toggle();
				if ($(this).text() == "Show Details")
				   $(this).text("Hide Details");
				else
				    $(this).text("Show Details");
			});
			clearInterval(progressBar);
			getFiles();
		}).fail(function() {
			
		});
		
	};
	var getTotalCount = function(){
		callAjaxService("clearCache",function(){
			var request = {
					"source":"hive",
					"database":"FRV_DB",
					"groupByEnabled":true,
					"filters":" 1 =1 ",
					"query":"select count(*) as _count from frv_tras_parq_final where <FILTER> "
				};
				callAjaxService("getData", function(response){
					getStagingFiles();
					if (response && response.isException) {
						showNotification("error", response.customMessage);
						return;
					} else{
							if(response && response.data && response.data.length>0){
								var totalCount = response.data[0]["_count"];
								if(totalCount)
									$("#recordsValue").html(d3.format(",.0f")(totalCount)); 
								else
									$("#recordsValue").html("0"); 
							}
						}
				}, callBackFailure,request, "POST",null,true);
		},callBackFailure,null, "POST");
		
	};
	/*var getpagination = function(){
		callAjaxService("clearCache",function(){
			var columns = [{"data":"first_name","title":"First Name"},
			               {"data":"last_name","title":"Last Name"},
			               {"data":"company_name","title":"Company Name"},
			               ];
			var config = {
					html: '#datainjectionTable',
					url: serviceCallMethodNames["getDataPagination"].url,
					source:"HIVE",
					filter:'1=1',
					database:"FRV_DB",
					table:"frv_tras_parq_final",
					columns:columns,
					dataColumns:"first_name,last_name,company_name"
				};
			
			constructGrid(config);
			
			setTimeout(function(){ 
				var totalCount = $("#datainjectionTable_info").find("span:eq(2)").html();
				if(totalCount)
					$("#recordsValue").html(totalCount); 
				else
					$("#recordsValue").html("0"); 
				},3000);
		},callBackFailure,null, "POST");

	};*/
	getFiles();
	//getStagingFiles();
}

loadPage();