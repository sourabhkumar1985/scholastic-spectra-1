<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Select Application to Proceed</title>
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/font-awesome.min.css">
<!-- <link rel="stylesheet" type="text/css" media="screen"
	href="css/metro.css"> -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-production_unminified.css?v=${version}">
</head>
<link rel="icon" href="img/favicon/favicon-bullseye.ico" type="image/x-icon">
<body class="loaded apps-body" cz-shortcut-listen="true">
	<!-- <div id="content" class="default-apps">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 class="page-title txt-color-blueDark text-center well">
			
			PAGE HEADER
				Select Application to Proceed<br>
		</h1>
		
	</div>
	</div>
		<div class="row" id="defaultApps">		
		</div>
	</div> -->
        <div id="widget_scroll_container">
            <div class="widget_container full" data-num="0" id="defaultApps">
                <!-- <div class="widget widget2x2 widget_darkred" data-url="typography.php" data-theme="darkred" data-name="typography">
                    <div class="widget_content">
                        <div class="main">
                        <span class="image-helper"> </span>
                         <img src="img/logo/SmartFind.png"></img>
                        <i class="fa fa-search fa-5x"></i>
                            <span>Typography</span>
                        </div>
                    </div>
                </div>
                <div class="widget" data-url="metro_gallery.php" data-theme="green" data-name="metro_gallery.js">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_metro_gallery.png');">
                         <img src="img/logo_sea.png"></img>
                            <span>Metro Gallery</span>
                        </div>
                    </div>
                </div>
                <div class="widget widget2x2 widget_blue" data-url="sliding_menu.php" data-theme="blue" data-name="sliding_menu">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_sliding_menu.png');">
                            <span>Sliding Menu</span>
                        </div>
                    </div>
                </div>
                <div class="widget widget2x2 widget_darkblue" data-url="timeline.php" data-theme="darkblue" data-name="timeline">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_timeline.png');">
                            <span>timeline</span>
                        </div>
                    </div>
                </div>
                <div class="widget widget2x2 widget_blue" data-url="cube.php" data-theme="blue" data-name="gallery.js">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_gallery.png');">
                            <span>3D Cube Gallery</span>
                        </div>
                    </div>
                </div>
                <div class="widget widget2x2 widget_red" data-url="grid_slider.php" data-theme="red" data-name="grid_slider">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_chart.png');">
                            <span>grid slider</span>
                        </div>
                    </div>
                </div>
                <div class="widget widget4x2 widget_darkblue" data-url="royal_tab.php" data-theme="darkblue" data-name="royal_tab">
                    <div class="widget_content">
                        <div class="main" style="background-image:url('images/widget_tab.png');">
                            <span>Royal Tab</span>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script> if (!window.jQuery) { document.write('<script src="js/plugin/libs/jquery-2.0.2.min.js"><\/script>');} </script>
	<script src="js/Common/default-apps.js"></script>
</body>
</html>