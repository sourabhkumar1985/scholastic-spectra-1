
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en-us" class="${themename}">
<head>
<meta charset="utf-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title>Home</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap-multiselect.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="css/dc.css?v=<%=session.getAttribute("version")%>">

<!-- Material Styles : -->
<link rel="stylesheet" type="text/css" href="web/css/CASSA/materialize.css">
<link rel="stylesheet" type="text/css" href="web/css/CASSA/style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables-->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-production_unminified.css?v=<%=session.getAttribute("version")%>">

<link rel="stylesheet" type="text/css" media="print"
	href="css/smartadmin-production_unminified.css?v=<%=session.getAttribute("version")%>">

<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-skins.css?v=<%=session.getAttribute("version")%>">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/leaflet.css?v=<%=session.getAttribute("version")%>">

<link rel="stylesheet" type="text/css" media="print"
	href="css/markerCluster.css?v=<%=session.getAttribute("version")%>">

<link rel="stylesheet" type="text/css" media="screen"
	href="css/markerCluster.Default.css?v=<%=session.getAttribute("version")%>">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/dc-leaflet-legend.css?v=<%=session.getAttribute("version")%>">
<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

<!-- We recommend you use "your_style.css" to override SmartAdmin
		specific styles this will also ensure you retrain your customization
		with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css"> -->

<link rel="stylesheet" type="text/css" media="screen"
	href="css/your_style.css?v=<%=session.getAttribute("version")%>">
<link rel="stylesheet" type="text/css" media="print"
	href="css/your_style.css?v=<%=session.getAttribute("version")%>">
<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp 
<link rel="stylesheet" type="text/css" media="screen"
	href="css/demo.css">
-->
<!-- FAVICONS -->
<link rel="shortcut icon" href="${favicon}" type="image/x-icon">
<link rel="icon" href="${favicon}" type="image/x-icon">

<!-- GOOGLE FONT -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>
<body class="${themename}${menustyle}">
	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

	<!-- HEADER -->
	<header id="header" class="${widgetcolor} hidden">
		<div id="logo-group">

			<!-- PLACE YOUR LOGO HERE -->
			<c:choose>
				<c:when test="${not empty icon}">
					<span> <a id="logo" class='${icon} logoIcon'> </a>
					</span>
					<span class='logoIcon font-xl font25'> </span>
				</c:when>
				<c:otherwise>
					<span> <a id="logo"> 
					<%-- <img alt="SmartAdmin"
							src='${logo} ' id="imgLogo"
							onclick="window.location.reload(true)"> --%>
					</a>
					</span>
				</c:otherwise>
			</c:choose>
			<!-- END LOGO PLACEHOLDER -->

			<!-- Note: The activity badge color changes when clicked and resets the number to 0
				Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
			<!--	<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>-->

			<!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->

			<!-- END AJAX-DROPDOWN -->
		</div>
		<!-- projects dropdown -->

		<!-- end projects dropdown -->

		<!-- pulled right: nav area -->
		<%
			if (session.getAttribute("topMenuSettings") != null
					&& !session.getAttribute("topMenuSettings").toString()
							.isEmpty()) {
		%>
		${topMenuSettings}
		<%
			} else {
		%>
		<div id="topIconContent" class="pull-right">
			<div
				class="btn-header pull-right apps-content dropdown dropdown-toggle "
				data-toggle="dropdown">
				<div class="dropdown" id="mainNavigationMenu">
					<a href="javascript:void(0);" class="th-list-style" rel="popover"
						data-placement="bottom" data-content=""><i
						class="glyphicon glyphicon-th glyphicon-24px"></i></a>
				</div>
				<ul class="navigation-menu dropdown-menu user-menu z-index-max">
					<div class="padding-10">
						<div class="navigation-menu-tile" id="getProfile">
							<i class="fa fa-user navigation-menu-icon"></i>
							<div class="navigation-menu-text">Profile</div>
						</div>
						<div class="navigation-menu-tile" href="#shortlist"
							data-action="shortlist" id="shortlist">
							<i class="fa fa-star-o navigation-menu-icon"></i>
							<div class="navigation-menu-text">Favorites</div>
						</div>
						<div class="navigation-menu-tile" id="navagate_readingList">
							<i class="fa fa-book navigation-menu-icon"></i>
							<div class="navigation-menu-text">Reading List</div>
						</div>
						<div class="navigation-menu-tile" href="#searchHistory"
							data-action="searchHistory" id="searchHistory">
							<i class="fa fa-search navigation-menu-icon"></i>
							<div class="navigation-menu-text">Recently Viewed</div>
						</div>
						<div class="navigation-menu-tile" id="navagate_bookmarks">
							<i class="fa fa-bookmark-o navigation-menu-icon"
								href="#bookmarks" data-action="bookmarks"></i>
							<div class="navigation-menu-text">Saved Search</div>
						</div>
						<div class="navigation-menu-tile" id="navigate_faq">
							<i class="fa fa-question navigation-menu-icon"></i>
							<div class="navigation-menu-text">FAQs</div>
						</div>
						<a class="navigation-menu-tile" id="navigate-add-res"> <i
							class="fa fa-plus navigation-menu-icon"></i>
							<div class="navigation-menu-text">Additional Resources</div>
						</a>
						<div class="navigation-menu-tile" id="navigate_contact">
							<i class="fa fa-envelope-o navigation-menu-icon"></i>
							<div class="navigation-menu-text">Contact Us</div>
						</div>
						<div class="navigation-menu-tile" id="navigate_administrator">
							<i class="fa fa-user navigation-menu-icon"></i>
							<div class="navigation-menu-text">Admin</div>
						</div>
						<a class="navigation-menu-tile" href="${logoutUrl}"
							id="userLogout"> <i
							class="fa fa-sign-out navigation-menu-icon"></i>
							<div class="navigation-menu-text">Logout</div>
						</a>
					</div>
					<label id="username" style="display: none;"><%=request.getSession().getAttribute("userDisplayName")%></label>
					<%-- <li class="padding-10" style="text-align:center"><strong	id="username" class="text-transform-capitalize"
						title=<%=request.getSession().getAttribute("userDisplayName")%>><%=request.getSession().getAttribute("userDisplayName")%></strong></li>
					<li class="divider"></li>
					<li><a href="#searchHistory" data-action="searchHistory" class="padding-left-10 no-padding-top no-padding-bottom"
						id="searchHistory"><i class="fa fa-clock-o"></i> 
								My Search History</a></li>
								<li class="divider"></li>
					<li><a href="#bookmarks" data-action="bookmarks"  class="padding-left-10 no-padding-top no-padding-bottom"
						id="bookmarks"><i class="fa fa-bookmark-o"></i> 
								My Bookmarks</a></li>
								<li class="divider"></li>
					<li><a href="#shortlist" data-action="shortlist"  class="padding-left-10 no-padding-top no-padding-bottom"
						id="shortlist"><i class="fa fa-star-o"></i>  My
								Shortlist</a></li>
					<li class="divider"></li>
					<li id="shoppigCartli" style="display: none;"><a href="#cart" data-action="cart"  class="padding-left-10 no-padding-top no-padding-bottom"
						id="shoppingCart"><i class="fa fa-shopping-cart"></i>  Cart <i class="label label-info compare-cart-count-head"></i></a></li>			
								<li class="divider"></li>
					<li><a href="${logoutUrl}" data-action="userLogout" class="padding-left-10 padding-top-5 padding-bottom-5"
						id="userLogout"><i class="fa fa-sign-out"></i> <strong>Logout</strong></a>
					</li> --%>
				</ul>

			</div>
			<div id="hide-menu" class="btn-header pull-right">
				<span> <a href="javascript:void(0);" title="Collapse Menu"><i
						class="fa fa-reorder"></i></a>
				</span>
			</div>
			<div id="show-menu" class="btn-header pull-right"
				data-action="toggle" style="display: none;" data-side="right">
				<span> <a href="javascript:void(0);" title="help"><i
						class="fa fa-question"></i></a>
				</span>
			</div>
			<div id="show-apps" style="display: none;"
				class="btn-header pull-right apps-content" data-action="toggle"
				data-side="right">
				<span> <a href="javascript:void(0);" rel="popover"
					data-placement="bottom" data-content=" "><i
						class="glyphicon glyphicon-th"></i></a>
				</span>
			</div>
			<div id="sendmail" style="display: none;"
				class="btn-header pull-right apps-content" data-action="toggle"
				data-side="right">
				<span><a href="javascript:void(0);"
					title="Send email to support team"><i class="fa fa-support"></i></a>
				</span>
			</div>
			<!-- <ul class="header-dropdown-list hidden-xs">
					<li>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="img/favicon/jnjwebsitetheme_favicon.ico" class="flag flag-us" alt="United States"> <span> US</span> <i class="fa fa-angle-down"></i> </a>
						<ul class="dropdown-menu pull-right">
							<li class="active">
								<a href="javascript:void(0);"><img src="img/favicon/jnjwebsitetheme_favicon.ico" class="flag flag-us" alt="United States"> English (US)</a>
							</li>
							<li>
								<a href="javascript:void(0);"><img src="img/favicon/jnjwebsitetheme_favicon.ico" class="flag flag-fr" alt="France"> Français</a>
							</li>
						</ul>
					</li>
				</ul> -->
			<!-- logout button -->
			<%-- <div id="logout" class="btn-header transparent pull-right">
				<span> <a href="${logoutUrl}" title="Sign Out"><i
						class="fa fa-sign-out"></i></a>
				</span>
			</div> --%>
			<!-- end logout button -->

			<!-- search mobile button (this is hidden till mobile view port) -->
			<div id="search-mobile" class="btn-header transparent pull-right">
				<span> <a href="javascript:void(0)" title="Search"><i
						class="fa fa-search"></i></a>
				</span>
			</div>
			<!-- end search mobile button -->

			<!-- input: search field -->
			<form action="Search" class="header-search pull-right"
				id="frm-header-search">
				<!-- input type="text" placeholder="Search.." id="search-fld"> -->
				<!-- <button type="submit">
					<i class="fa fa-search"></i>
				</button> -->
				<a href="javascript:void(0);" id="cancel-search-js"
					title="Cancel Search"><i class="fa fa-times"></i></a>
			</form>
			<!-- end input: search field -->

			<!--multiple lang dropdown : find all flags in the image folder -->
			<!-- <ul class="header-dropdown-list hidden-xs hide">
				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="" src="img/flags/Canada.png"> <span> Canada</span>
						<i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu pull-right z-index-max">
						<li class="active"><a href="javascript:void(0);"><img
								alt="" src="img/flags/Canada.png"> Canada</a></li>
						<li><a href="javascript:void(0);"><img alt=""
								src="img/flags/Japan.png"> Japan</a></li>
						<li><a href="javascript:void(0);"><img alt=""
								src="img/flags/us.png"> US</a></li>
					</ul></li>
			</ul> -->
			<!-- end multiple lang -->

		</div>
		<%
			}
		%>

		<!-- end pulled right: nav area -->

	</header>
	<!-- END HEADER -->

	<!-- Left panel : Navigation area -->
	<!-- Note: This width of the aside area can be adjusted through LESS variables nav-lock-->
	<aside id="left-panel" style="display: none;"
		class="sidenav-main nav-collapsible sidenav-dark sidenav-active-rounded">
		<div class="brand-sidebar">
			<h1 class="logo-wrapper">
				<a class="brand-logo darken-1" id="brandlogo" href="javascript:void(0)"><img
					src="" style="height:43px;"
					alt="materialize logo"><span
					class="logo-text hide-on-med-and-down" style="display:none">${appname}</span></a><a
					class="navbar-toggler" href="#"><i class="logo-text material-icons" style="display:none">radio_button_unchecked</i></a>
			</h1>
		</div>
		
		<!-- User info -->

		<!-- end user info -->

		<!-- NAVIGATION : This navigation is also responsive
		'${logo} '
			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
		<!-- <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i>
		</span> -->
		<!-- <a class="batchNumberDecorator" id="batchNumberId" href="#" style=""></a> -->
		<!-- <nav> -->
		<!-- NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

		<ul
			class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow ps ps--active-y customscroll" style="overflow:auto;">
		</ul>

		<!-- </nav> -->

	</aside>
	<!-- END NAVIGATION -->

	<!-- MAIN PANEL -->
	<div id="main" role="main" class="hidden main-full">

		<!-- RIBBON -->
		<div id="ribbon" style="display: none;">
			<div class="col-md-15">
				<span class="ribbon-button-alignment"> <span id="refresh"
					class="btn btn-ribbon" data-title="refresh" rel="tooltip"
					data-placement="bottom"
					data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your applied filters."
					data-html="true"><i class="fa fa-refresh"></i></span>
				</span>
	
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<!-- This is auto generated -->
				</ol>
			</div>
			<div class="" style="text-align:center;width:50%;float:left;">
				<h2 class="breadcrumb row-seperator-header" id="ribbonname" style="text-align:center;color: #fff8dc !important;padding: 6px!important;font-weight: 500;font-size: 21px;"></h2>
			</div>
			<!-- <a href="#" class="pull-right padding10 text-underline-hover" data-target='#issueReport' data-toggle='modal' onclick="reportIssue.CreateView()"
							><span>Report issue</span></a> -->
			<!-- end breadcrumb -->
			<!--Help Button  -->
			<div class="col-md-20">
				<div class="profile-actions" id="actionDiv">
					<!-- <ul class="nav nav-tabs pull-right in" id="myTab">
					</ul>
					<span id="filterProfile" class="pull-right profileFilter"><i
						class="fa fa-filter">| Filter</i></span>
					<div class="dc-data-count pull-right">
						<a href="javascript:void(0)">Reset All</a>
					</div> -->
				</div>
			</div>
			<!-- <a href="javascript:void(0)" class="btn padding10  pull-right"
				align="right" data-action="toggle" data-side="right"><span>Help</span></a> -->
			<!--End help button  -->

			<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->
		</div>
		<!-- END RIBBON -->

		<!-- MAIN CONTENT -->
		<div id="content"></div>
		<div id="loading"></div>
		<!-- END MAIN CONTENT -->
		<div class="demo">
			<span id="demo-setting"> <!-- <i
				class="fa fa-question txt-color-blueDark"></i> -->
			</span>
			<form>
				<legend class="no-padding margin-bottom-10">
					Frequently Asked Questions
					<div class="pull-right">
						<a title="Close" id="closeFAQ"> <i
							class="fa fa-close txt-color-blueDark"></i>
						</a><a title="Open in new Window" id="openFAQ"><i
							class="fa fa-expand txt-color-blueDark"></i></a>
					</div>
				</legend>
				<section>
					<div class="input-group input-group-sm margin-bottom-20">
						<input class="form-control input-sm" type="text"
							placeholder="Search help..." id="search-faq-popup">
						<div class="input-group-btn">
							<button type="submit" class="btn btn-default">
								<i class="fa fa-fw fa-search fa-lg"></i>
							</button>
						</div>
					</div>
					<div class="panel-group acc-v1 margin-bottom-40" id="accordionFAQ">
					</div>
				</section>
			</form>
		</div>
		<div
			class="modal hide zd-infopopup attributePalette in docked zd-palettepopup"
			data-name="Group" aria-hidden="false"
			style="display: block; right: auto; bottom: auto; top: 120px; left: 750px;"
			id="demisionModalDiv">
			<div class="modal-header zd-infopopup-titlebar">
				<button type="button" class="close icon zd close"
					data-dismiss="modal" aria-hidden="true"
					onclick="$('#demisionModalDiv').addClass('hide')">&times;</button>
				<i class="fa fa-refresh padding-right10 pull-right cursor-pointer"></i>
				<h3 class="dialog-head zd-infopopup-header demisionTitle"></h3>
				<div class="clearfix"></div>
			</div>
			<div class="modal-body dialog-body zd-infopopup-body"
				style="max-height: 170px;">
				<ul class="nav nav-tabs nav-stacked" id="demisionlist">
					<!-- <li><span class="zd icon checkbox radio"><span
						class="zd checked "></span></span>
					<div class="attributeItem">
						<div class="text">1632-Burnaby:144</div>
					</div></li>
				 <li class="filtered"><span class="zd icon checkbox radio"><span
						class="zd checked  filtered"></span></span>
					<div class="attributeItem">
						<div class="text">Product Group</div>
					</div></li>
				 -->
				</ul>
			</div>
			<div class="zd-infopopup-background"></div>
			<div class="zd-infopopup-background small"></div>
			<div class="zd-infopopup-pointer"
				style="border-top-width: 14px; border-top-style: solid; border-top-color: transparent; border-bottom-width: 14px; border-bottom-style: solid; border-bottom-color: transparent; border-right-width: 14px; border-right-style: solid; border-right-color: rgba(50, 50, 50, 0.901961); top: 127px; left: -14px;">
				<div class="zd-infopopup-pointer-arrow"></div>
			</div>
			<div id="deleteDiv" class="hide">
				<div
					class="btn-group display-inline pull-right text-align-left hidden-tablet">
					<button class="btn btn-xs btn-default dropdown-toggle"
						data-toggle="dropdown">
						<i class="fa fa-cog fa-lg"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-xs pull-right">
						<li><a onclick="javascript:#.Delete(event,$)" id="deleteLink"><i
								class="fa fa-times fa-lg fa-fw txt-color-red"></i> <u>D</u>elete</a></li>
						<li class="divider"></li>
						<li class="text-align-center"><a
							onclick="javascript:#.CancelDelete(event)">Cancel</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="issueReport" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog  width-500">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Report Issue</h4>
				</div>
				<form id="frmSankeyFilter" class="">
					<div class="modal-body no-padding">
						<div action="" id="smart-form-register" class="smart-form "
							novalidate="novalidate">
							<fieldset>
								<section>
									<label class="input"> <i class="icon-append fa fa-user"></i>
										<input type="text" name="subject" placeholder="Issue subject">
										<b class="tooltip tooltip-bottom-right">Needed to enter
											the subject</b>
									</label>
								</section>
								<section>
									<label class="textarea"> <i
										class="icon-append fa fa-question-circle"></i> <textarea
											rows="3" placeholder="Issue descrption" name="description"></textarea>
										<b class="tooltip tooltip-bottom-right"><i
											class="fa fa-warning txt-color-teal"></i> Needed to enter the
											descrption</b>
									</label>
								</section>
								<section>
									<label for="file" class="input input-file">
										<div class="button">
											<input type="file" name="file"
												onchange="this.parentNode.nextSibling.value = this.value">Browse
										</div> <input type="text" placeholder="Attachment" readonly="">
									</label>
								</section>
							</fieldset>
							<footer>
								<button type="button" class="btn btn-warning"
									data-dismiss="modal" aria-hidden="true">Cancel</button>
								<button type="submit" class="btn btn-primary" id="btnCreate">Submit</button>
							</footer>
						</div>

					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="sidebars" id="divsidebar">
		<div class="sidebar right hide" id="page"
			style="max-height: auto; overflow-y: scroll">
			<div>

				<a href="javascript:void(0)" class="btn help-close"
					data-action="toggle" data-side="right"> <i class="fa fa-close"></i></a>

				<!--  <form action="help.html" id="gethelp" method="get"></form>   -->
				<a href="javascript:OpenURL('ajax/help.html');"
					class="btn-icon-help jarviswidget-fullscreen-btn" rel="tooltip"
					title="" data-placement="bottom" data-original-title="Fullscreen">
					<i class="fa fa-expand"></i>
				</a>
			</div>

			<div class="panel-group-faq" id="accordion">

				<div class="faqHeader">
					<h1 style="font-size: 20px">Frequently Asked Questions</h1>
					<input type="search" id="searching" placeholder="Search">
					<!-- <i class="fa fa-search"></i> -->
					<!-- <input type="button" id="searchs" value="Search">  -->

				</div>

				<div id="faqSection"></div>

			</div>
		</div>

	</div>

	<!-- END MAIN PANEL -->

	<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		
		<div id="shortcut">
			<ul>
				<li>
					<a href="#ajax/inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
				<li>
					<a href="#ajax/calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
				</li>
				<li>
					<a href="#ajax/gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
				</li>
				<li>
					<a href="#ajax/invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
				</li>
				<li>
					<a href="#ajax/gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
				</li>
				<li>
					<a href="javascript:void(0);" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
				</li>
			</ul>
		</div>-->
	<!-- END SHORTCUT AREA -->

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
	<script>
		if (!window.jQuery) {
			document
					.write('<script src="js/plugin/libs/jquery-2.0.2.min.js"><\/script>');
		}
	</script>
	<script
		src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script>
		if (!window.jQuery.ui) {
			document
					.write('<script src="js/plugin/libs/jquery-ui-1.10.3.min.js"><\/script>');
		}
	</script>




	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

	<!-- BOOTSTRAP JS -->
	<script
		src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script>
		if (typeof ($.fn.modal) === "undefined") {
			document
					.write('<script src="js/plugin/bootstrap/bootstrap.min.js"><\/script>');
		}
	</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.3/handlebars.min.js"></script>
	<script>
		if (typeof (Handlebars) === "undefined") {
			document
					.write('<script src="js/plugin/handelbars/handlebars.js"><\/script>');
		}
	</script>
	<!-- 
	<script src="js/spin.min.js"></script>
	<script src="js/crossfilter.min.js"></script>
	<script src="js/colorbrewer.js"></script>
	<script src="js/tipsy.min.js"></script>
	CUSTOM NOTIFICATION
	<script src="js/notification/SmartNotification.min.js"></script>

	JARVIS WIDGETS
	<script src="js/smartwidgets/jarvis.widget.min.js"></script>

	EASY PIE CHARTS
	<script src="js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

	SPARKLINES
	<script src="js/plugin/sparkline/jquery.sparkline.min.js"></script>

	JQUERY VALIDATE
	<script src="js/plugin/jquery-validate/jquery.validate.min.js"></script>

	JQUERY MASKED INPUT
	<script src="js/plugin/masked-input/jquery.maskedinput.min.js"></script>

	JQUERY SELECT2 INPUT
	<script src="js/plugin/select2/select2.min.js"></script>

	JQUERY UI + Bootstrap Slider
	<script src="js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

	browser msie issue fix
	<script src="js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

	SmartClick: For mobile devices
	<script src="js/plugin/smartclick/smartclick.js"></script> -->

	<!-- All above plugin into one file-->
	<script>
	var directoryName = "<%=request.getContextPath()%>";
	  sessionStorage.setItem("directoryName", directoryName);
		$("#batchNumberId").html("IDP Version : " + localStorage["buildInfo"]);
// 		var logoIcon = '${icon}';
// 		if(logoIcon && logoIcon!=""){
// 			$("#imgLogo").remove();
// 			$("#logo").addClass(logoIcon);
// 			$("#logo").addClass("fa-3x");
// 			$("#logo").addClass("logoIcon");
// 		}else{
// 			$("#imgLogo").attr("src",'${logo}');
// 		}
	</script>
	<script src="js/plugin/d3.v5.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/plugin/smartwidgets/jarvis.widget.js"></script>
	<script src="js/plugin/jquery_all_plugins.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/plugin/dc.grouped.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/Common/MetaData.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/Common/System.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/Common/Common.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/Common/templates.js?v=<%=session.getAttribute("version")%>"></script>
	<script>
		storeVersion(<%=session.getAttribute("version")%>);
	</script>
	<script src="js/plugin/datatables/jquery.dataTables-all.min.js"></script>
	<!-- <script src="js/plugin/datatables/jquery.dataTables-cust.min.js"></script>
	<script src="js/plugin/datatables/media/js/TableTools.min.js"></script>
	<script src="js/plugin/datatables/DT_bootstrap.js"></script>
	<script src="js/plugin/datatables/jquery.dataTables-cust.min.js"></script>
	<script src="js/plugin/datatables/media/js/TableTools.min.js"></script>
	<script src="js/plugin/datatables/DT_bootstrap.js"></script> -->
	<!-- <script src="js/plugin/jquery.PrintArea.js"></script>
	<script src="js/plugin/jquery.jsontotable.min.js"></script> -->
	<!-- <script src="js/plugin/jspdf.debug.js"></script> -->
	<script src="js/Profiling/Profiling.js?v=<%=session.getAttribute("version")%>"></script>
	<script src="js/Profiling/Graphing.js?v=<%=session.getAttribute("version")%>"></script>
	<!-- <script src="js/plugin/jquery-sidebar/jquery.sidebar.min.js"></script> -->
	<!-- <script src="js/WireFrame/MoleculeAnalysis1.js?v=${version}"></script> -->
	<!--[if IE 7]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

	<!-- Demo purpose only
		<script src="js/demo.js"></script> -->

	<!-- MAIN APP JS FILE -->
	<script src="js/Common/app.js?v=<%=session.getAttribute("version")%>"></script>
	<script>
	//console.log(${userDisplayName});
		System.lastUpdatedOn = '${lastUpdatedOn}';
		System.NotificationClosedOn = '${NotificationClosedOn}';
		System.userDisplayName = '${userDisplayName}';
		System.userName = '${userName}';
		System.businessRole = '${userBusinessRole}';
		System.pltFrmSplty = '${pltfrmSplty}';
		System.dept = '${dept}';
		System.region = '${region}';
		constructMenu(${userroleactions});
		storeLookupTable(${lookupvalues});
		System.currentRole = '${primaryRole}';
		System.readingList = '${rl}';
		<%session.setAttribute("rl", "");%>
		processEvidenceView('${primaryRole}');
		processRoles('${userrolesfordisplay}','${primaryRole}', '${userSecondaryRolesList}');
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {

				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),

			m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js',
				'ga');
		var trackingCode = lookUpTable.get("GoogleAnalyticsKey");
		ga('create', trackingCode, "auto");
		</script>

	<script id="tabInfo-partial" type="text/x-handlebars-template">
	<section  class="tabsection col-md-11">
 	 <label class="input col-md-1"> 
		<input type="text" name="taborder_{{index}}" value="{{value}}" >
		<b class="tooltip tooltip-bottom-right">Enter Tab Order</b>
		</label>
		<label class="input col-md-5"> 
			<input type="text" name="tabname_{{index}}" value="{{displayName}}"
			placeholder="Enter Tab Name"> <b
			class="tooltip tooltip-bottom-right">Enter Tab Name</b>
		</label>
		<label class="input col-md-6"> 
			<label class="input col-md-12" type="text" name="tabprofile_{{index}}" id="tabprofile_{{index}}" value="{{profilekey}}"
			placeholder="Select Tab Profile">
			</label> 
			<b	class="tooltip tooltip-bottom-right">Select Tab Profile</b>
		</label>
	</section>
	<i class="fa fa-minus-circle fa-2x col-md-1 btnRemoveTab" style="cursor: pointer;" title="Remove Tab"></i>
</script>


	<!-- Your GOOGLE ANALYTICS CODE Below -->
</body>

</html>