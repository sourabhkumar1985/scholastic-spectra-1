<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en-us">
<head>
<meta charset="utf-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title>Back To Basics | Login</title>
<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<script type="text/javascript">
msieversion();
function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0){ 
//    	console.log("---------> IE",parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        window.location = "web/ieErrorPage.html";
        return false;
    }   
}
</script>
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/font-awesome.min.css">

<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-production_unminified.css?v=${version}">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-skins.css?v=${version}">

<!-- SmartAdmin RTL Support is under construction
			<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp
		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css"> -->

<!-- FAVICONS -->
<link rel="shortcut icon" href="img/favicon/sea_favicon.ico"
	type="image/x-icon">
<link rel="icon" href="img/favicon/sea_favicon.ico"
	type="image/x-icon">

<!-- GOOGLE FONT -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<script>

</script>
</head>
<body id="login" class="animated fadeInDown ${themename}">
	<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
	<header id="header">
		<!--<span id="logo"></span>-->

		<div id="logo-group">
			<span id="logo"> <img style="height:100%;width:auto" src="<c:url value="img/logo/pssc.png" />" alt="Logo">
			</span>

			<!-- END AJAX-DROPDOWN -->
		</div>

		<!--	<span id="login-header-space"> <span class="hidden-mobile">Need an account?</span> <a href="register.html" class="btn btn-danger">Creat account</a> </span>-->

	</header>

	<div id="main" role="main">

		<!-- MAIN CONTENT -->
		<div id="content" class="container">

			<div class="row">
				<div
					class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
					<h1 class="txt-color-red login-header-big" style="margin-top:75px">Patient Specific Supply chain System</h1>
					<div class="hero">

						<div class="pull-left login-desc-box-l">
							<!-- <h3 class="paragraph-header ">Supply Chain - Business
								Insights</h3> -->
								<!-- <img src="img/logo_docspera.jpg" class="pull-right display-image" alt="" style="width:210px"> -->

						</div>

						<!--<img src="img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">-->

					</div>



				</div>
				
				 
				 


				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
				<div class="alert alert-danger fade in hide" id="errormsgdiv">
				<button class="close" data-dismiss="alert">
					�
				</button>
				<i class="fa-fw fa fa-times"></i>
				<strong>Error: </strong><span id="errormsg"> ${message}</span>
			</div>
					<div class="well no-padding">
						<form method="post" class="smart-form client-form" name="myform" id="login-form" action="<c:url value='j_spring_security_check' />">
							<header> Please Enter Your Information </header>

							<fieldset>
								<section>
									<label class="label">Username</label> <label class="input">
										<i class="icon-append fa fa-user"></i> <input type="text"
										name="username" id="username" autofocus> <b
										class="tooltip tooltip-top-right"><i
											class="fa fa-user txt-color-teal"></i> Please enter email
											address/username</b>
									</label>
								</section>

								<section>
									<label class="label">Password</label> <label class="input">
										<i class="icon-append fa fa-lock"></i> <input type="password"
										name="password" id="password"> <b
										class="tooltip tooltip-top-right"><i
											class="fa fa-lock txt-color-teal"></i> Enter your password</b>
									</label>
									<div class="note">
										<!--	<a href="javascript:void(0)">Forgot password?</a>-->
									</div>
								</section>


								<section>
									<label class="checkbox"> </label>
								</section>
							</fieldset>
							<footer>
								<button type="submit"
									class="btn btn-success btn-sm bounceIn animation-delay5 login-link pull-right">
									<i class="fa fa-sign-in"></i> Login
								</button>
							</footer>
						</form>
					</div>
					


				</div>
			</div>
		</div>
		<a id="batchNumberId"  style=" background: none repeat scroll 0 0 #eee;
		    border-radius: 5px;bottom: 10px;box-shadow: 0 1px 2px #999;padding: 0 5px 2px;position: absolute;
    		right: 10px;" href="#" style=""></a>
	</div>
	
	

	<!--================================================== -->

	<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

	<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script> if (!window.jQuery) { document.write('<script src="js/plugin/libs/jquery-2.0.2.min.js"><\/script>');} </script>

	<script
		src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script> if (!window.jQuery.ui) { document.write('<script src="js/plugin/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

	<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

	<!-- BOOTSTRAP JS -->
	<script src="js/plugin/bootstrap/bootstrap.min.js?v=${version}"></script>

	<!-- CUSTOM NOTIFICATION -->
	<script src="js/plugin/notification/SmartNotification.min.js?v=${version}"></script>

	<!-- JQUERY MASKED INPUT -->
	<script src="js/plugin/masked-input/jquery.maskedinput.min.js?v=${version}"></script>

	<!--[if IE 7]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

	<!-- MAIN APP JS FILE -->
	<script src="js/Common/app.js"></script>
	<script type="text/javascript">
	var directoryName ="/E2EMF" 

    /* function checkAuthentication() {
        sessionStorage.setItem("AuthenticationPass", " ");
        sessionStorage.setItem("user", " ");
        sessionStorage.clear();
    } */
    //window.onload = checkAuthentication;
    $(document).ready(function(){
    	loadBuildNo();
    });
	if ($("#errormsg").html() != " "){
		$("#errormsgdiv").removeClass("hide");
	}
		</script>

</body>
</html>