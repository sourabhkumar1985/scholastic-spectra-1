package com.relevance.ppa.data;

import java.util.Date;

public class TCurfView {

	private String exRtType;

	private String fromCurr;

	private String toCurr;

	private Date validFrom;

	private double ratioFrom;

	private double ratioTo;
	
	public String getExRtType() {
		return exRtType;
	}

	public void setExRtType(String exRtType) {
		this.exRtType = exRtType;
	}

	public String getFromCurr() {
		return fromCurr;
	}

	public void setFromCurr(String fromCurr) {
		this.fromCurr = fromCurr;
	}

	public String getToCurr() {
		return toCurr;
	}

	public void setToCurr(String toCurr) {
		this.toCurr = toCurr;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public double getRatioFrom() {
		return ratioFrom;
	}

	public void setRatioFrom(double ratioFrom) {
		this.ratioFrom = ratioFrom;
	}

	public double getRatioTo() {
		return ratioTo;
	}

	public void setRatioTo(double ratioTo) {
		this.ratioTo = ratioTo;
	}
		
}
