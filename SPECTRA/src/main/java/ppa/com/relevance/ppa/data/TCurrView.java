package com.relevance.ppa.data;

import java.util.Date;

public class TCurrView {

	private String exRtType;

	private String fromCurr;

	private String toCurr;

	private Date validFrom;

	private double exchRate;
	
	private double ratio;
	
	private String fromDateInString;
	
	private String toDateInString;
	
	public String getExRtType() {
		return exRtType;
	}

	public void setExRtType(String exRtType) {
		this.exRtType = exRtType;
	}

	public String getFromCurr() {
		return fromCurr;
	}

	public void setFromCurr(String fromCurr) {
		this.fromCurr = fromCurr;
	}

	public String getToCurr() {
		return toCurr;
	}

	public void setToCurr(String toCurr) {
		this.toCurr = toCurr;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public double getExchRate() {
		return exchRate;
	}

	public void setExchRate(double exchRate) {
		this.exchRate = exchRate;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}

	public String getFromDateInString() {
		return fromDateInString;
	}

	public void setFromDateInString(String fromDateInString) {
		this.fromDateInString = fromDateInString;
	}

	public String getToDateInString() {
		return toDateInString;
	}

	public void setToDateInString(String toDateInString) {
		this.toDateInString = toDateInString;
	}
	
	
}
