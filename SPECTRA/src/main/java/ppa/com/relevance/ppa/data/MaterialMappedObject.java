package com.relevance.ppa.data;

import com.relevance.prism.data.DistanceObject;

public class MaterialMappedObject {

	private Material sourceMaterial;
	private Material targetMaterial;
	private DistanceObject distance;
	public Material getSourceMaterial() {
		return sourceMaterial;
	}
	public void setSourceMaterial(Material sourceMaterial) {
		this.sourceMaterial = sourceMaterial;
	}
	public Material getTargetMaterial() {
		return targetMaterial;
	}
	public void setTargetMaterial(Material targetMaterial) {
		this.targetMaterial = targetMaterial;
	}
	public DistanceObject getDistance() {
		return distance;
	}
	public void setDistance(DistanceObject distance) {
		this.distance = distance;
	}
}
