package com.relevance.ppa.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.relevance.ppa.data.Material;
import com.relevance.ppa.data.PPAMaterialMapping;
import com.relevance.prism.util.Log;

public class ReversePlantPurchaseAnalyticsDao {
	

	public ReversePlantPurchaseAnalyticsDao() {	}

	public static int distance(String a, String b) {
		a = a.toLowerCase();
		b = b.toLowerCase();
		// i == 0
		int[] costs = new int[b.length() + 1];
		for (int j = 0; j < costs.length; j++)
			costs[j] = j;
		for (int i = 1; i <= a.length(); i++) {
			// j == 0; nw = lev(i - 1, j)
			costs[0] = i;
			int nw = i - 1;
			for (int j = 1; j <= b.length(); j++) {
				int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]),
						a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
				nw = costs[j];
				costs[j] = cj;
			}
		}
		return costs[b.length()];
	}
	public static int min(int a, int b, int c) {
	     if (a <= b && a <= c) return a;
	     if (b <= a && b <= c) return b;
	     return c;
	}

		public static ArrayList<PPAMaterialMapping>  compareBPCSToSSE(HashMap<String, Material> bpcsMaterialIDHashMap,
				HashMap<String, Material> sseMaterialIDHashMap,
				HashMap<String, Material> bpcsSpecCodeHashMap,
				HashMap<String, Material> sseSpecCodeHashMap,
				HashMap<String, Material> bpcsMaterialNameHashMap,
				HashMap<String, Material> sseMaterialNameHashMap,
				HashMap<String, List<Material>> bpcsPlantHashMap,
				HashMap<String, List<Material>> ssePlantHashMap,
				HashMap<String, String> sseSynonymHashMap,
				HashMap<String, String> bpcsSynonymHashMap, 
				HashMap<String, String> synonymWithOutNumbersHashMap, 
				ArrayList<PPAMaterialMapping> materialMappingList ,
				ArrayList<Material> bpcsMaterialNotFoundList ,
				List<Material> sseMaterialList
	){
			Material sseMaterial = new Material();
			Material bpcsMaterial = new Material();
			String targetPlant = "";
			Integer lvDistance = 100;
			Integer tempLVDistance = 100;
			Integer tempLVDistanceSupplier =100;
			/*Integer tempLVDistanceSynonym = 100;
			float priceDiff = 100;*/
			boolean removeDuplicated = true;
			try{
		Iterator<Map.Entry<String, List<Material>>> itPlant =  bpcsPlantHashMap.entrySet().iterator();
		while (itPlant.hasNext()) {
			Map.Entry<String, List<Material>> pairs = (Map.Entry<String, List<Material>>) itPlant.next();
			List<Material> sseMaterials =  new ArrayList<Material>();
			List<Material> bpcsMaterials =  new ArrayList<Material>();
			bpcsMaterials= pairs.getValue();
			lvDistance = 100;
			//priceDiff = 100;
			targetPlant = pairs.getKey().toLowerCase().trim();
			sseMaterials  = ssePlantHashMap.get(pairs.getKey());
			System.out.println("Starting BPCS to SSE Comparison");
			for (int j = bpcsMaterials.size(); j > 0 ; j--){
				try{
				boolean matchFound = false;
				bpcsMaterial = bpcsMaterials.get(j-1);					
				if (sseMaterialIDHashMap.containsKey(targetPlant+"-"+bpcsMaterial.getMaterialId().toLowerCase().trim().replaceFirst("^0+(?!$)", ""))){
					matchFound = true;
					sseMaterial = sseMaterialIDHashMap.get(targetPlant+"-"+sseMaterial.getMaterialId().toLowerCase().trim().replaceFirst("^0+(?!$)", ""));
					PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
					ppaMaterialMapping.setSourceSystem("BPCS");
					ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
					ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlant());
					ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
					ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
					ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
					ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());					
					ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                        ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
					ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
//					ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
					/*if (sseMaterial.getMaterialGroup().equals("A") || sseMaterial.getMaterialGroup().equals("P"))
						ppaMaterialMapping.setCategory("Chemial");
					else
						ppaMaterialMapping.setCategory("Packaging");	*/
					ppaMaterialMapping.setCategory(	bpcsMaterial.getCategory());				
					ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());								
					ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
					//ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
					//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
					//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
					//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
					//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
					//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
					ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
					ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
					ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
					ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
					ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
					//ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
					ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialGroup());
					ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
					ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
					ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
//					ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
//					ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
//					ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUom());
					ppaMaterialMapping.setDirection("planttodm");
					ppaMaterialMapping.setType("1");
					ppaMaterialMapping.setTypeDesc("Material Code");
					ppaMaterialMapping.setTypeCategory("Exact");
					ppaMaterialMapping.setWeightage(100);
				 //   System.out.println("SSSEgetMaterialName" + sseMaterial.getMaterialName() + "    BPCS : " + bpcsMaterial.getMaterialName());
					if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");
					if (bpcsMaterial.getMaterialName() == null)bpcsMaterial.setMaterialName("");
					sseSynonymHashMap.put( sseMaterial.getMaterialName().toLowerCase().trim(), 
					bpcsMaterial.getMaterialName().toLowerCase().trim());
					bpcsSynonymHashMap.put(bpcsMaterial.getMaterialName().toLowerCase().trim(), sseMaterial.getMaterialName().toLowerCase().trim());
					synonymWithOutNumbersHashMap.put( sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z ]", ""),
     				bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z ]", ""));
					
					materialMappingList.add(ppaMaterialMapping);
					if (removeDuplicated){
						if (sseMaterial.getPlant() != null ){
							if (ssePlantHashMap.get(sseMaterial.getPlant().trim().toLowerCase()) != null)
								ssePlantHashMap.get(sseMaterial.getPlant().trim().toLowerCase()).remove(sseMaterial);
						}
					}
				} 
				if ((!matchFound || !removeDuplicated) && sseSpecCodeHashMap.containsKey(targetPlant+"-"+bpcsMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", ""))){
					if (!bpcsMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", "").equalsIgnoreCase("tbc")){
						matchFound = true;
						sseMaterial = sseSpecCodeHashMap.get(targetPlant+"-"+bpcsMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", ""));
						PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
						ppaMaterialMapping.setSourceSystem("BPCS");
						ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
						ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlantDesc());
						ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
						ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
						ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
						ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());
						ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                                ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
						ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
						//ppaMaterialMapping.setSourceLatest(sseMaterial.getLatest());
//						ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
						ppaMaterialMapping.setCategory(	bpcsMaterial.getCategory());				
						ppaMaterialMapping.setSubcategory("Not found");
						ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
						//ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
						//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
						//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
						//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
						//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
						//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
						ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
						ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
						ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
						ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
						ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
						//ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
						ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialGroup());
						ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
						ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
						ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
//						ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
//						ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
//						ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUom());
						ppaMaterialMapping.setDirection("planttodm");
						ppaMaterialMapping.setType("2");
						ppaMaterialMapping.setTypeDesc("Spec Code");
						ppaMaterialMapping.setTypeCategory("Tentative");
						ppaMaterialMapping.setWeightage(90);						
						materialMappingList.add(ppaMaterialMapping);
						if (removeDuplicated){
							ssePlantHashMap.get(sseMaterial.getPlant().trim().toLowerCase()).remove(bpcsMaterial);
						}
					}
				} 
				if ((!matchFound || !removeDuplicated) && sseMaterialNameHashMap.containsKey(targetPlant+"-"+bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", ""))){
					matchFound = true;
					sseMaterial = sseMaterialNameHashMap.get(targetPlant+"-"+bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", ""));
					PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
					ppaMaterialMapping.setSourceSystem("BPCS");
					ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
					ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlantDesc());
					ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
					ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
					ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
					ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());
					ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                        ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
					ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
					//ppaMaterialMapping.setSourceLatest(sseMaterial.getLatest());
//					ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
					//ppaMaterialMapping.setMaterialGroup(sseMaterial.getMaterialGroup());
					ppaMaterialMapping.setCategory(	bpcsMaterial.getCategory());				
					ppaMaterialMapping.setSubcategory("Not Found");
					//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
					//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
					//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
					//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
					//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
					ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
					ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
					ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
					ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
					ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
					ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
					//ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
					ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialGroup());
					ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
					ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
					ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
//					ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
//					ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
//					ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUom());
					ppaMaterialMapping.setDirection("planttodm");
					ppaMaterialMapping.setType("3");
					ppaMaterialMapping.setTypeDesc("Exact Material Desc");
					ppaMaterialMapping.setTypeCategory("Tentative");
					ppaMaterialMapping.setWeightage(70);						
					materialMappingList.add(ppaMaterialMapping);
				}
				/*if (!matchFound || !removeDuplicated){
					PPAMaterialMapping ppaMaterialMapping= new PPAMaterialMapping();
					ppaMaterialMapping.setSourceSystem("BPCS");
					ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
					ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlantDesc());
					ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
					ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
					ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
					ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());
					ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                        ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
					ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
					//ppaMaterialMapping.setSourceLatest(sseMaterial.getLatest());
					ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
					ppaMaterialMapping.setCategory("Not Found");
					ppaMaterialMapping.setSubcategory("Not Found");
					//ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
					//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
					//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
					//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
					//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
					//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
					ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
					String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
					String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
					if (!sourceMaterialName.trim().equalsIgnoreCase("") && !sourceSupplierName.trim().equalsIgnoreCase("")  && !sourceMaterialName.trim().equalsIgnoreCase("na")
							&& sseMaterials != null){
						float sourcePrice = ppaMaterialMapping.getSourcePrice();
						for (int i = sseMaterials.size(); i > 0 ; i--){
							sseMaterial = sseMaterials.get(i-1);
							if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");
							String targetMaterialNameString = sseMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
							String targetSupplierString = sseMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
							if (!targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
								float targetPrice = bpcsMaterial.getPrice();
								tempLVDistance = distance(sourceMaterialName,targetMaterialNameString);
								tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
								float tempPriceDiff = 0;
								if (targetPrice != 0 && sourcePrice != 0){
									tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
									if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff) ){
										tempPriceDiff = 100;
									}
								}
								Boolean supplierMatch =false;
								if (tempLVDistanceSupplier < 6 || (targetSupplierString.length() > 1 && sourceSupplierName.length()>1 && (targetSupplierString.contains(sourceSupplierName) || sourceSupplierName.contains(targetSupplierString)))){
									supplierMatch = true;
								}
								if ( supplierMatch && ((lvDistance > tempLVDistance && tempLVDistance < 7) || (targetMaterialNameString.length() > 1 && sourceMaterialName.length()>1 && (targetMaterialNameString.contains(sourceMaterialName) || sourceMaterialName.contains(targetMaterialNameString))))){
									matchFound = true;
									lvDistance = tempLVDistance;
									priceDiff = tempPriceDiff;
									ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
									ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
									ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
									ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
									ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
									ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
									ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
									ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
									ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
									ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
									ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
									ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
									ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
									ppaMaterialMapping.setPriceDiff(tempPriceDiff);
									ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
									ppaMaterialMapping.setDirection("planttodm");
									ppaMaterialMapping.setType("5");
									ppaMaterialMapping.setTypeDesc("Close Description and Supplier");
									ppaMaterialMapping.setTypeCategory("Tentative");
									ppaMaterialMapping.setWeightage(40);
								} 	
							}
						}
					}
					if (matchFound)
						materialMappingList.add(ppaMaterialMapping);
				
			}*/
			if (!matchFound || !removeDuplicated){
				lvDistance = 100;
				//priceDiff = 100;
				PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
				ppaMaterialMapping.setSourceSystem("BPCS");
				ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
				ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlantDesc());
				ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
				ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
				ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
				ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());
				ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
				ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
				//ppaMaterialMapping.setSourceLatest(sseMaterial.getLatest());
//				ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
				ppaMaterialMapping.setCategory(	bpcsMaterial.getCategory());				
				ppaMaterialMapping.setSubcategory("Not Found");
				//ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
				//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
				//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
				//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
				//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
				//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
				ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
				if(ppaMaterialMapping.getSourceMaterialName()== null) ppaMaterialMapping.setSourceMaterialName("");
				String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				float sourcePrice = ppaMaterialMapping.getSourcePrice();
				if (!sourceMaterialName.trim().equalsIgnoreCase("")  && !sourceMaterialName.trim().equalsIgnoreCase("na") && sseMaterials != null){
					for (int i = sseMaterials.size(); i > 0 ; i--){
						sseMaterial = sseMaterials.get(i-1);
						if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");
						if (bpcsMaterial.getMaterialName() == null)bpcsMaterial.setMaterialName("");
						String targetMaterialNameString = sseMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						String targetSupplierString = sseMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						if (!targetMaterialNameString.trim().equalsIgnoreCase("")  && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
							float targetPrice = sseMaterial.getPrice();
							tempLVDistance = distance(sourceMaterialName,targetMaterialNameString);
							tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
							float tempPriceDiff = 0;
							if (targetPrice != 0 && sourcePrice != 0){
								tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
								if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff)){
									tempPriceDiff = 100;
								}
							}
							if ((lvDistance > tempLVDistance && tempLVDistance < 7) || (targetMaterialNameString.length() > 1 && sourceMaterialName.length()>1 && (targetMaterialNameString.contains(sourceMaterialName) || sourceMaterialName.contains(targetMaterialNameString)))){
								lvDistance = tempLVDistance;
								//priceDiff = tempPriceDiff;
								matchFound = true;
								ppaMaterialMapping.setTargetSystem("SSE");
								ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
								ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
								ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
								//ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
								ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialGroup());
								ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
								ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
								ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
								ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
//								ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
//								ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
//								ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUom());
								ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
								ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
								ppaMaterialMapping.setPriceDiff(tempPriceDiff);
								ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
								ppaMaterialMapping.setDirection("planttodm");
								ppaMaterialMapping.setType("6");
								ppaMaterialMapping.setTypeDesc("Close Description");
								ppaMaterialMapping.setTypeCategory("Tentative");
								ppaMaterialMapping.setWeightage(30);
							} 	
						}
					}
				}
				if (matchFound)
					materialMappingList.add(ppaMaterialMapping);
			}
			/*if (!matchFound || !removeDuplicated){
				PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
				ppaMaterialMapping.setSourceSystem("BPCS");
				ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
				ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlantDesc());
				ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
				ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
				ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
				ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());
				ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
				ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
				//ppaMaterialMapping.setSourceLatest(sseMaterial.getLatest());
				ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
				ppaMaterialMapping.setCategory("Not Found");
				ppaMaterialMapping.setSubcategory("Not Found");
				//ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
				//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
				//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
				//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
				//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
				//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
				ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
				String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				if (!sourceMaterialName.trim().equalsIgnoreCase("") && !sourceSupplierName.trim().equalsIgnoreCase("") && !sourceMaterialName.trim().equalsIgnoreCase("na")
						&& sseMaterials != null){
					float sourcePrice = ppaMaterialMapping.getSourcePrice();
					for (int i = sseMaterials.size(); i > 0 ; i--){
						
						sseMaterial = sseMaterials.get(i-1);
						if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");
						if (bpcsMaterial.getMaterialName() == null)bpcsMaterial.setMaterialName("");
						String targetMaterialNameString = sseMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						String targetSupplierString = sseMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						if (!targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
							float targetPrice = sseMaterial.getPrice();
							tempLVDistance = distance(sourceMaterialName,targetMaterialNameString);
							tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
							float tempPriceDiff = 0;
							if (targetPrice != 0 && sourcePrice != 0){
								tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
								if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff)){
									tempPriceDiff = 100;
								}
							}
							Boolean supplierMatch =false;
							if (tempLVDistanceSupplier < 6 || (targetSupplierString.length() > 1 && sourceSupplierName.length()>1 && (targetSupplierString.contains(sourceSupplierName) || sourceSupplierName.contains(targetSupplierString)))){
								supplierMatch = true;
							}
							if ( supplierMatch){
								matchFound = true; 
								lvDistance = tempLVDistance;
								priceDiff = tempPriceDiff;
								ppaMaterialMapping.setTargetSystem("SSE");
								ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
								ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
								ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
								ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
								ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
								ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
								ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
								ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
								ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
								ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
								ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
								ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
								ppaMaterialMapping.setPriceDiff(tempPriceDiff);
								ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
								ppaMaterialMapping.setDirection("planttodm");
								ppaMaterialMapping.setType("7");
								ppaMaterialMapping.setTypeDesc("Close Price");
								ppaMaterialMapping.setTypeCategory("Tentative");
								ppaMaterialMapping.setWeightage(20);
							} 	
						}
					}
				}
				if (matchFound)
					materialMappingList.add(ppaMaterialMapping);
			}*/
			if (!matchFound || !removeDuplicated){
				bpcsMaterialNotFoundList.add(bpcsMaterial);
			}
		}catch(Exception ex) {System.out.println(ex.getMessage());ex.printStackTrace();}
	   }
	}

	//List<Material> sseMaterials =  new ArrayList<Material>();
	boolean matchFound = false;
	for (int j= bpcsMaterialNotFoundList.size();j>0;j--){
		try{
		matchFound = false;
		lvDistance = 100;
		tempLVDistance = 100;
		tempLVDistanceSupplier =100;
		//tempLVDistanceSynonym = 100;
		bpcsMaterial = bpcsMaterialNotFoundList.get(j-1);
		PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
		ppaMaterialMapping.setSourceSystem("BPCS");
		ppaMaterialMapping.setSourcePlant(bpcsMaterial.getPlant());
		ppaMaterialMapping.setSourcePlantDesc(bpcsMaterial.getPlantDesc());
		ppaMaterialMapping.setSourceMaterialId(bpcsMaterial.getMaterialId());
		ppaMaterialMapping.setSourceSpecCode(bpcsMaterial.getSpecCode());
		ppaMaterialMapping.setSourceMaterialName(bpcsMaterial.getMaterialName());
		ppaMaterialMapping.setSourceSupplier(bpcsMaterial.getSupplier());
		ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
		ppaMaterialMapping.setSourcePrice(bpcsMaterial.getPrice());
		//ppaMaterialMapping.setSourceLatest(sseMaterial.getLatest());
		
		ppaMaterialMapping.setCategory(	bpcsMaterial.getCategory());				
		ppaMaterialMapping.setSubcategory("Not Found");
		//ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
		//ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
		//ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
		//ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
		//ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
		//ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
		ppaMaterialMapping.setSourceSAPPlant(bpcsMaterial.getSapPlant());
		if ( sseMaterialList!= null &&  sseMaterialList.size() >0){
		String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
		String sourceMaterialNameSynonym =bpcsSynonymHashMap.get(ppaMaterialMapping.getSourceMaterialName().toLowerCase().trim());
		String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
		if (!sourceMaterialName.trim().equalsIgnoreCase("") && !sourceSupplierName.trim().equalsIgnoreCase("")  && !sourceMaterialName.trim().equalsIgnoreCase("na")
				&& sseMaterialList!= null){
			float sourcePrice = ppaMaterialMapping.getSourcePrice();
			for (int i = sseMaterialList.size(); i > 0 ; i--){
				sseMaterial = sseMaterialList.get(i-1);
				if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");				
				String targetMaterialNameString = sseMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				String targetMaterialNameSynonym = sseSynonymHashMap.get(sseMaterial.getMaterialName().toLowerCase().trim());
				String targetSupplierString = sseMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				if (!targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
					float targetPrice = sseMaterial.getPrice();
					tempLVDistance =100;
					tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
					if (sourceMaterialNameSynonym != null && !sourceMaterialNameSynonym.equalsIgnoreCase("")){
						tempLVDistance = Math.min(tempLVDistance,distance(sourceMaterialNameSynonym,targetMaterialNameString));
						if (targetMaterialNameString.length() > 1 && sourceMaterialNameSynonym.length()>1 && (targetMaterialNameString.contains(sourceMaterialNameSynonym) || sourceMaterialNameSynonym.contains(targetMaterialNameString))){
							tempLVDistance =5;
						}
					}
					if (targetMaterialNameSynonym != null && !targetMaterialNameSynonym.equalsIgnoreCase("")){
						tempLVDistance = Math.min(tempLVDistance,distance(sourceMaterialName,targetMaterialNameSynonym));
						if (targetMaterialNameSynonym.length() > 1 && sourceMaterialName.length()>1 && (targetMaterialNameSynonym.contains(sourceMaterialName) || sourceMaterialName.contains(targetMaterialNameSynonym))){
							tempLVDistance =5;
						}
					}
					if (sourceMaterialNameSynonym != null && !sourceMaterialNameSynonym.equalsIgnoreCase("") && targetMaterialNameSynonym != null && !targetMaterialNameSynonym.equalsIgnoreCase("")){
						tempLVDistance = Math.min(tempLVDistance,distance(sourceMaterialNameSynonym,targetMaterialNameSynonym));
						if (targetMaterialNameSynonym.length() > 1 && sourceMaterialNameSynonym.length()>1 && (targetMaterialNameSynonym.contains(sourceMaterialNameSynonym) || sourceMaterialNameSynonym.contains(targetMaterialNameSynonym))){
							tempLVDistance =5;
						}
					}
					
				float tempPriceDiff = 0;
					if (targetPrice != 0 && sourcePrice != 0){
						tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
						if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff) ){
							tempPriceDiff = 100;
						}
					}
					/*Boolean supplierMatch =false;
					if (tempLVDistanceSupplier < 6 || (targetSupplierString.length() > 1 && sourceSupplierName.length()>1 && (targetSupplierString.contains(sourceSupplierName) || sourceSupplierName.contains(targetSupplierString)))){
						supplierMatch = true;
					}*/
					if  (lvDistance > tempLVDistance && tempLVDistance < 7){
						matchFound = true;
						lvDistance = tempLVDistance;
						//priceDiff = tempPriceDiff;
						ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
						ppaMaterialMapping.setTargetMaterialId(sseMaterial.getMaterialId());
						ppaMaterialMapping.setTargetPlant(sseMaterial.getPlant());
						ppaMaterialMapping.setTargetSpecCode(sseMaterial.getSpecCode());
						//ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialName());
						ppaMaterialMapping.setTargetMaterialName(sseMaterial.getMaterialGroup());
						ppaMaterialMapping.setTargetSupplier(sseMaterial.getSupplier());
						ppaMaterialMapping.setTargetSupplierId(sseMaterial.getSupplierId());
						ppaMaterialMapping.setTargetPrice(sseMaterial.getPrice());
						ppaMaterialMapping.setTargetSAPPlant(sseMaterial.getSapPlant());
//						ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
//						ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUom());
//						ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
//						ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
						ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
						ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
						ppaMaterialMapping.setPriceDiff(tempPriceDiff);
						ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
						ppaMaterialMapping.setDirection("planttodm");
						ppaMaterialMapping.setType("8");
						ppaMaterialMapping.setTypeDesc("Synonym Close Description");
						ppaMaterialMapping.setTypeCategory("Tentative");
						ppaMaterialMapping.setWeightage(20);
					} 	
				}
			}
		}
			if (!matchFound){
				ppaMaterialMapping.setDirection("planttodm");
				ppaMaterialMapping.setTargetPlant(ppaMaterialMapping.getSourcePlant());
				ppaMaterialMapping.setTargetSystem("SSE");
				ppaMaterialMapping.setType("4");
				ppaMaterialMapping.setTypeDesc("None");
				ppaMaterialMapping.setTypeCategory("None");
				ppaMaterialMapping.setWeightage(0);
			}
			materialMappingList.add(ppaMaterialMapping);
		}
		}catch(Exception ex){System.out.println(ex.getMessage());ex.printStackTrace();}
	}
			}catch(Exception ex ){
				ex.printStackTrace();
				Log.Error("Error on comparing BPCS to SSE. Error :" + ex.getMessage());
		   }
			if(materialMappingList != null ){
				System.out.println("Finished comparing BPCS to SSE");	
			}
			return materialMappingList;
	}
}
