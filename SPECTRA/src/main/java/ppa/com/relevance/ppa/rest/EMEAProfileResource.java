package com.relevance.ppa.rest;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.ppa.data.EMEAL2View;
import com.relevance.ppa.data.EMEAView;
import com.relevance.ppa.service.EMEAProfileService;
import com.relevance.prism.data.DataTableView;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;

@Path("/emea")
public class EMEAProfileResource {

	@Context
	ServletContext context;

	public EMEAProfileResource(@Context ServletContext value) {
		this.context = value;
		// System.out.println("Conext:" + this.context);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeapoprofileview/")
	public String getEMEAPOProfileDetails(	@FormParam("source") String source,
			@FormParam("materiallist") String materiallist,
			@FormParam("supplierlist") String supplierlist) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA PO Profile view to fetch EMEA PO Profile Details.");

		String emeaPOProfileViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		//String key = "emeapoprofileview";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PO Profile View.");
			//	emeaPOProfileViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaPOProfileViewList = emeaProfileService.getEMEAPOProfileViewObject(source,materiallist,supplierlist);

					emeaPOProfileViewJson = gson.toJson(emeaPOProfileViewList);
					emeaPOProfileViewJson = DataObfuscator.obfuscate(emeaPOProfileViewJson);
					endTime = System.currentTimeMillis();

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA PO Profile View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA PO Profile View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile View JSON created : " + emeaPOProfileViewJson);
		return emeaPOProfileViewJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeapoprofilel2view/")
	public String getEMEAPOProfileL2Details(@FormParam("plant") String plant,
											@FormParam("companycode") String companycode,
											@FormParam("currency") String currency,
											@FormParam("ssecontract") String ssecontract,
											@FormParam("compliant") String compliant,
											@FormParam("sseitemmmatch") String sseitemmatch,
											@FormParam("pricevariance") String pricevariance,
											@FormParam("pricevariancepercentage") String pricevariancepercentage,
											@FormParam("netvaluevariance") String netvaluevariance,
											@FormParam("poreqdate") String poreqdate,
											@FormParam("netvaluequartile") String netvaluequartile,
											@FormParam("pocostquartile") String pocostquartile,
											@FormParam("netvariancequartile") String netvariancequartile,
											@FormParam("itemtype") String itemtype,
											@FormParam("currencymismatch") String currencymismatch,
											@FormParam("leadtimemismatch") String leadtimemismatch,
											@FormParam("moqmismatch") String moqmismatch,
											@FormParam("unitmismatch") String unitmismatch,											
											@FormParam("incotermmismatch") String incotermmismatch,
											@FormParam("variance") String variance,
											@FormParam("gssstatus") String gssstatus,
											@FormParam("netvalue") String netvalue) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA PO Profile L2 View to fetch EMEA PO Profile L2 View Details.");

		String emeaPOProfileL2ViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeapoprofilel2view";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PO Profile L2 View.");
			//	emeaPOProfileL2ViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaPOProfileL2ViewList = emeaProfileService.getEMEAPOProfileL2ViewObject(plant, companycode, currency, ssecontract, compliant, sseitemmatch, pricevariance, netvaluevariance, poreqdate, netvaluequartile, pocostquartile, netvariancequartile, itemtype, currencymismatch, leadtimemismatch, moqmismatch, unitmismatch, incotermmismatch, variance, gssstatus, pricevariancepercentage, netvalue);
					EMEAL2View emeaPOProfileL2View = new EMEAL2View();
					emeaPOProfileL2View.setEmeaPurchaseOrderL2View(emeaPOProfileL2ViewList);
					emeaPOProfileL2ViewJson = gson.toJson(emeaPOProfileL2View);
					emeaPOProfileL2ViewJson = DataObfuscator.obfuscate(emeaPOProfileL2ViewJson);
					endTime = System.currentTimeMillis();

					if (emeaPOProfileL2ViewJson != null) {
						Log.Info("EMEA PO Profile L2 View JSON created having size : " + emeaPOProfileL2ViewJson.length());
						this.context.setAttribute(key,emeaPOProfileL2ViewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA PO Profile L2 View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA PO profile L2 View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile L2 View JSON created : " + emeaPOProfileL2ViewJson);
		return emeaPOProfileL2ViewJson;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/emeaquotesprofileview/")
	public String getEMEAQuotesProfileDetails() {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA Quotes Profile view to fetch EMEA Quotes Profile Details.");

		String emeaQuotesProfileViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeaquotesprofileview";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PO Profile View.");
			//	emeaPOProfileViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaQuotesProfileViewList = emeaProfileService.getEMEAQuotesProfileViewObject();

					emeaQuotesProfileViewJson = gson.toJson(emeaQuotesProfileViewList);
					emeaQuotesProfileViewJson = DataObfuscator.obfuscate(emeaQuotesProfileViewJson);
					endTime = System.currentTimeMillis();

					if (emeaQuotesProfileViewJson != null) {
						Log.Info("EMEA Quotes Profile View JSON created having size : " + emeaQuotesProfileViewJson.length());
						this.context.setAttribute(key,emeaQuotesProfileViewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA Quotes Profile View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA Quotes Profile View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile View JSON created : " + emeaPOProfileViewJson);
		return emeaQuotesProfileViewJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeaquotesprofilel2view/")
	public String getEMEAQuotesProfileL2Details(@FormParam("plantcode") String plantcode,
											@FormParam("currency") String currency,
											@FormParam("ssecontract") String ssecontract,
											@FormParam("compliant") String compliant,
											@FormParam("sseitemmmatch") String sseitemmatch,
											@FormParam("pricevariance") String pricevariance,
											@FormParam("netvaluevariance") String netvaluevariance,
											@FormParam("contractstartdate") String contractstartdate,
											@FormParam("pricevariancepercentage") String pricevariancepercentage,
											@FormParam("currencymismatch") String currencymismatch,
											@FormParam("leadtimemismatch") String leadtimemismatch,
											@FormParam("moqmismatch") String moqmismatch,
											@FormParam("unitmismatch") String unitmismatch,											
											@FormParam("incotermmismatch") String incotermmismatch,
											@FormParam("netvalue") String netvalue) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA Quotes Profile L2 View to fetch EMEA Quotes Profile L2 View Details.");

		String emeaQuotesProfileL2ViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeaquotesprofilel2view";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PO Profile L2 View.");
			//	emeaPOProfileL2ViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaQuotesProfileL2ViewList = emeaProfileService.getEMEAQuotesProfileL2ViewObject(plantcode, currency, ssecontract, compliant, sseitemmatch, pricevariance, netvaluevariance, contractstartdate, pricevariancepercentage, currencymismatch, leadtimemismatch, moqmismatch, unitmismatch, incotermmismatch, netvalue);
					EMEAL2View emeaL2View = new EMEAL2View();
					emeaL2View.setEmeaQuotesL2View(emeaQuotesProfileL2ViewList);
					emeaQuotesProfileL2ViewJson = gson.toJson(emeaL2View);
					emeaQuotesProfileL2ViewJson = DataObfuscator.obfuscate(emeaQuotesProfileL2ViewJson);
					endTime = System.currentTimeMillis();

					if (emeaQuotesProfileL2ViewJson != null) {
						Log.Info("EMEA Quotes Profile L2 View JSON created having size : " + emeaQuotesProfileL2ViewJson.length());
						this.context.setAttribute(key,emeaQuotesProfileL2ViewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA Quotes Profile L2 View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA Quotes profile L2 View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile L2 View JSON created : " + emeaPOProfileL2ViewJson);
		return emeaQuotesProfileL2ViewJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getEmeaDataPagination/")
	public String getEMEADataPagenation(@FormParam("table") String table,@FormParam("columns") String columns,@FormParam("search[value]") String searchValue,@FormParam("filter") String filter,@FormParam("order[0][column]") Integer orderBy,@FormParam("order[0][dir]") String orderType,@FormParam("start") String pagenumber,@FormParam("length") String pagesize,@FormParam("groupBy") String groupBy) throws AppException{

		Log.Info("Recieved request EMEA  Profile L2 View to fetch EMEA  Profile L2 View Details.");
		DataTableView view = null;
		String result = null;

				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					view = (DataTableView)emeaProfileService.getEMEADataPagination(table, columns, searchValue, filter, orderBy, orderType, pagenumber, pagesize,groupBy);
					DataObfuscator.obfuscate(view.getData(), "screen" + ".data");//do not remove .data
					
					result = view.toString();
					return result;

				}catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				}catch(Exception ex){
					Log.Error("Exception while retrieving the Database list " + ex.getMessage());
					Log.Error(ex);
					String failureMessage = "{\"message\":\"failure\"}";
					return failureMessage;
		}
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeappvprofileview/")
	public String getEMEAPPVProfileDetails(@FormParam("source") String source,
			@FormParam("materiallist") String materiallist,
			@FormParam("supplierlist") String supplierlist,
			@FormParam("specidlist") String specidlist) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA PPV Profile view to fetch EMEA PPV Profile Details.");

		String emeaPPVProfileViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeappvprofileview";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PPV Profile View.");
			//	emeaPPVProfileViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaPPVProfileViewList = emeaProfileService.getEMEAPPVProfileViewObject(source, materiallist, supplierlist, specidlist);

					emeaPPVProfileViewJson = gson.toJson(emeaPPVProfileViewList);
					emeaPPVProfileViewJson = DataObfuscator.obfuscate(emeaPPVProfileViewJson);
					endTime = System.currentTimeMillis();

					if (emeaPPVProfileViewJson != null) {
						Log.Info("EMEA PPV Profile View JSON created having size : " + emeaPPVProfileViewJson.length());
						this.context.setAttribute(key,emeaPPVProfileViewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA PPV Profile View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA PPV Profile View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile View JSON created : " + emeaPOProfileViewJson);
		return emeaPPVProfileViewJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeappvprofilel2view/")
	public String getEMEAPPVProfileL2Details(@FormParam("plantname") String plantname,
											@FormParam("postingyear") String postingyear,
											@FormParam("postingmonth") String postingmonth,
											@FormParam("postingdatedoc") String postingdatedoc,
											@FormParam("materialtype") String materiatype,
											@FormParam("purchasecurrency") String purchasecurrency,
											@FormParam("invoicecurrency") String documentcurrency,
											@FormParam("purchasinguom") String purchasinguom,
											@FormParam("stockinguom") String stockinguom,
											@FormParam("paymentterms") String paymentterms,
											@FormParam("materialcategory") String materialcategory,
											@FormParam("documentcategory") String documentcategory,
											@FormParam("totalinvoicepricequartile") String totalinvoicepricequartile,
											@FormParam("standardcostquartile") String standardcostquartile,
											@FormParam("variance") String variance,
											@FormParam("ppv2quartile") String ppv2quartile,
											@FormParam("ppvmismatch") String ppvmismatch,
											@FormParam("incotermcode") String incotermcode,
											@FormParam("gssstatus") String gssstatus,
											@FormParam("vendorfreqquantityquartile") String vendorfreqquantityquartile,
											@FormParam("vendorfreqposquartile") String vendorfreqposquartile,
											@FormParam("materialfreqquantityquartile") String materialfreqquantityquartile,
											@FormParam("materialfreqposquartile") String materialfreqposquartile,											
											@FormParam("netvalue") String netvalue,
											@FormParam("materialcode") String materialcode,
											@FormParam("vendornumber") String vendornumber,
											@FormParam("materialspecref") String materialspecref) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA PPV Profile L2 View to fetch EMEA Quotes Profile L2 View Details.");

		String emeaPPVProfileL2ViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeappvprofilel2view";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PO Profile L2 View.");
			//	emeaPOProfileL2ViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaPPVProfileL2ViewList = emeaProfileService.getEMEAPPVProfileL2ViewObject(plantname, postingyear, postingmonth, postingdatedoc, materiatype, purchasecurrency, documentcurrency, purchasinguom, stockinguom, paymentterms,  materialcategory, documentcategory, totalinvoicepricequartile, standardcostquartile, variance, ppv2quartile, ppvmismatch, incotermcode, gssstatus, vendorfreqquantityquartile, vendorfreqposquartile, materialfreqquantityquartile, materialfreqposquartile, netvalue, materialcode, vendornumber, materialspecref, "false");
					EMEAL2View emeaPPVL2View = new EMEAL2View();
					emeaPPVL2View.setEmeaPPVL2View(emeaPPVProfileL2ViewList);
					emeaPPVProfileL2ViewJson = gson.toJson(emeaPPVL2View);
					emeaPPVProfileL2ViewJson = DataObfuscator.obfuscate(emeaPPVProfileL2ViewJson);
					endTime = System.currentTimeMillis();

					if (emeaPPVProfileL2ViewJson != null) {
						Log.Info("EMEA PPV Profile L2 View JSON created having size : " + emeaPPVProfileL2ViewJson.length());
						this.context.setAttribute(key,emeaPPVProfileL2ViewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA PPV Profile L2 View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA PPV profile L2 View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile L2 View JSON created : " + emeaPOProfileL2ViewJson);
		return emeaPPVProfileL2ViewJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeapovarianceanalysisview/")
	public String getEMEAPOVarianceAnalysisDetails(@FormParam("sliderstart") String sliderstart,
			@FormParam("sliderend") String sliderend) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA PO Variance Analysis View to fetch EMEA PO Variance Analysis View Details.");

		String emeaPOVarianceAnalysisViewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeapovarianceanalysisview";
			//if (this.context.getAttribute(key) != null) {
			//	Log.Info("Reading object EMEA PO Profile L2 View.");
			//	emeaPOProfileL2ViewJson = this.context.getAttribute(key).toString();

			//} else {				
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaPOProfileL2ViewList = emeaProfileService.getEMEAPOVarianceAnalysisViewObject(sliderstart, sliderend);
					emeaPOVarianceAnalysisViewJson = gson.toJson(emeaPOProfileL2ViewList);
					emeaPOVarianceAnalysisViewJson = DataObfuscator.obfuscate(emeaPOVarianceAnalysisViewJson);
					endTime = System.currentTimeMillis();

					if (emeaPOVarianceAnalysisViewJson != null) {
						Log.Info("EMEA PO Variance Analysis View JSON created having size : " + emeaPOVarianceAnalysisViewJson.length());
						this.context.setAttribute(key,emeaPOVarianceAnalysisViewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA PO Profile L2 View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA PO Variance Analysis View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile L2 View JSON created : " + emeaPOProfileL2ViewJson);
		return emeaPOVarianceAnalysisViewJson;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/emeappvoverview/")
	public String getEMEAPPVOverview() {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved request EMEA PPV Overview View to fetch EMEA PPV Overview Details.");

		String emeaPPVOverviewJson = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String key = "emeappvoverview";
				try {
					EMEAProfileService emeaProfileService = new EMEAProfileService();
					ArrayList<EMEAView> emeaPPVOverviewList = emeaProfileService.getEMEAPPVOverviewObject();
					emeaPPVOverviewJson = gson.toJson(emeaPPVOverviewList);
					emeaPPVOverviewJson = DataObfuscator.obfuscate(emeaPPVOverviewJson);
					endTime = System.currentTimeMillis();

					if (emeaPPVOverviewJson != null) {
						Log.Info("EMEA PPV Overview JSON created having size : " + emeaPPVOverviewJson.length());
						this.context.setAttribute(key,emeaPPVOverviewJson);
					}

				} catch(AppException appe){
					Log.Error(appe);
					return appe.toString();
				} catch (Exception e) {
					Log.Error("Exception in EMEA PO Profile L2 View." + e.getMessage());
					Log.Error(e);
				}
			// }
		Log.Info("Returning EMEA PO Variance Analysis View,  total Time taken is " + E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		//Log.Info("EMEA PO Profile L2 View JSON created : " + emeaPOProfileL2ViewJson);
		return emeaPPVOverviewJson;
	}
}
