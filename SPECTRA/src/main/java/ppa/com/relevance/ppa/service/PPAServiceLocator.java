package com.relevance.ppa.service;

import com.relevance.ppa.dao.EMEAMasterDao;
import com.relevance.prism.dao.E2emfDao;
import com.relevance.prism.service.Service;

public class PPAServiceLocator {

	public static Service getServiceInstance(String serviceName) {
		Service service = null;
		 if(serviceName.equalsIgnoreCase("emeamaster")){
			service = new EMEAMasterService();
		}
		return service;
	}

	public static E2emfDao getServiceDaoInstance(String serviceName) {
		E2emfDao daoService = null;
		if(serviceName.equalsIgnoreCase("emeamaster")){
			daoService = new EMEAMasterDao();
		}		
		return daoService;
	}
}
