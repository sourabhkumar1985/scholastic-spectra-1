package com.relevance.ppa.data;

import java.util.ArrayList;

public class EMEAL2View {

	public EMEAL2View(){
		
	}
	
	private ArrayList<EMEAView> emeaPurchaseOrderL2View;
	private ArrayList<EMEAView> emeaQuotesL2View;
	private ArrayList<EMEAView> emeaPPVL2View;
	
	public ArrayList<EMEAView> getEmeaPurchaseOrderL2View() {
		return emeaPurchaseOrderL2View;
	}
	public void setEmeaPurchaseOrderL2View(
			ArrayList<EMEAView> emeaPurchaseOrderL2View) {
		this.emeaPurchaseOrderL2View = emeaPurchaseOrderL2View;
	}
	public ArrayList<EMEAView> getEmeaQuotesL2View() {
		return emeaQuotesL2View;
	}
	public void setEmeaQuotesL2View(ArrayList<EMEAView> emeaQuotesL2View) {
		this.emeaQuotesL2View = emeaQuotesL2View;
	}
	public ArrayList<EMEAView> getEmeaPPVL2View() {
		return emeaPPVL2View;
	}
	public void setEmeaPPVL2View(ArrayList<EMEAView> emeaPPVL2View) {
		this.emeaPPVL2View = emeaPPVL2View;
	}	
}
