package com.relevance.ppa.data;

import java.util.HashMap;

public class MaterialObject {
	
	public HashMap<String,Material> materialObject;
	public HashMap<String,String> materialIdHashMap;
	public HashMap<String,String> specCodeHashmap;
	public HashMap<String, Material> getMaterialObject() {
		return materialObject;
	}
	public void setMaterialObject(HashMap<String, Material> materialObject) {
		this.materialObject = materialObject;
	}
	public HashMap<String, String> getMaterialIdHashMap() {
		return materialIdHashMap;
	}
	public void setMaterialIdHashMap(HashMap<String, String> materialIdHashMap) {
		this.materialIdHashMap = materialIdHashMap;
	}
	public HashMap<String, String> getSpecCodeHashmap() {
		return specCodeHashmap;
	}
	public void setSpecCodeHashmap(HashMap<String, String> specCodeHashmap) {
		this.specCodeHashmap = specCodeHashmap;
	}
	
}
