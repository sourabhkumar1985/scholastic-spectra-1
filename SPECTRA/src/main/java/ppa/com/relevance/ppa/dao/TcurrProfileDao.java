package com.relevance.ppa.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.lang.time.DateUtils;

import com.relevance.ppa.data.TCurfView;
import com.relevance.ppa.data.TCurrView;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;

public class TcurrProfileDao extends BaseDao{
	
	PrismDbUtil dbUtil = null;

	Connection con = null;
	Statement stmt = null;
	
	String emea_db = null;
	
	public TcurrProfileDao(){
		emea_db = E2emfAppUtil.getAppProperty(E2emfConstants.emeaDB);
	}
	
	public ArrayList<TCurrView> getResults() throws AppException, SQLException{
		String tucrrSelectQuery = null;
		String tcurfSelectQuery = null;
		
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		ArrayList<TCurrView> tcurrViewList = new ArrayList<TCurrView>();
		HashMap<String, TCurfView> tcurfViewMap = new HashMap<String, TCurfView>();
		MultiHashMap tcurfViewTempMap = new MultiHashMap();
		MultiHashMap tcurrViewTempMap = new MultiHashMap();
		ArrayList<TCurrView> outputTcurfViewList = new ArrayList<TCurrView>();
		try {							
				Log.Info("Using EMEA HIVE Connection...");
				Log.Info("Fetching the TCURR Values for Profile ...");
								
				con = dbUtil.getHiveConnection(E2emfConstants.lpfgDB);
				stmt = con.createStatement();
				tucrrSelectQuery = "select exrt_type, from_curr, to_curr, valid_from, exch_rate from emea_l1_tcurr";
				rs = stmt.executeQuery(tucrrSelectQuery);
				if(rs != null){
					while(rs.next()){
						TCurrView tcurrView = new TCurrView();
						String exRtType = rs.getString("exrt_type");
						tcurrView.setExRtType(exRtType);
						String fromCurr = rs.getString("from_curr");
						tcurrView.setFromCurr(fromCurr);
						String toCurr = rs.getString("to_curr");
						tcurrView.setToCurr(toCurr);
						String dateInString = rs.getString("valid_from");
						String [] dateInArray = dateInString.split("/");
						int date = Integer.valueOf(dateInArray[1]).intValue();
						int month = Integer.valueOf(dateInArray[0]).intValue();
						int year = Integer.valueOf(dateInArray[2]).intValue();
						tcurrView.setValidFrom(new Date(year, month, date));
						String exchRate = rs.getString("exch_rate");
						//System.out.println("Exchange Rate : " + exchRate);
						//if(exchRate.contains(","))
						//	continue;
						tcurrView.setExchRate(Double.valueOf(exchRate).doubleValue());
						
						tcurrViewList.add(tcurrView);
						
						String tempKey = exRtType + "-" + fromCurr + "-" + toCurr;
						tcurrViewTempMap.put(tempKey, tcurrView);
					}
				}
								
				tcurfSelectQuery = "select exrt_type, from_curr, to_curr, valid_from, ratio_from, ratio_to from emea_l1_tcurf";
				rs2 = stmt.executeQuery(tcurfSelectQuery);
				if(rs2 != null){
					while(rs2.next()){
						TCurfView tcurfView = new TCurfView();
						String exRtType = rs2.getString("exrt_type");
						tcurfView.setExRtType(exRtType);
						String fromCurr = rs2.getString("from_curr");
						tcurfView.setFromCurr(fromCurr);
						String toCurr = rs2.getString("to_curr");
						tcurfView.setToCurr(toCurr);
						String dateInString = rs2.getString("valid_from");
						String [] dateInArray = dateInString.split("/");
						int date = Integer.valueOf(dateInArray[1]).intValue();
						int month = Integer.valueOf(dateInArray[0]).intValue();
						int year = Integer.valueOf(dateInArray[2]).intValue();
						tcurfView.setValidFrom(new Date(year, month, date));
						tcurfView.setRatioFrom(Double.valueOf(rs2.getString("ratio_from")).doubleValue());
						tcurfView.setRatioTo(Double.valueOf(rs2.getString("ratio_to")).doubleValue());
						
						//tcurfViewList.add(tcurfView);
						
						String key = exRtType + "-" + fromCurr + "-" + toCurr + "-" + dateInString.replace("/", "-"); 
						tcurfViewMap.put(key, tcurfView);
						
						String tempKey = exRtType + "-" + fromCurr + "-" + toCurr;
						tcurfViewTempMap.put(tempKey, tcurfView);
					}
				}
		}catch(SQLException sql){
			Log.Error("SQLException while Fetching Master Details for EMEA TCURR Profile." + sql.getStackTrace());
			Log.Error(sql);
			throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA TCURR Profile. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}finally{
			if(con != null)
				try {
					con.close();
					//stmt.close();
					//con = null;
					//stmt = null;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
				}
				
		}
		
		try{
			for(TCurrView tcurrView : tcurrViewList){
				String exRtType = tcurrView.getExRtType();
				String fromCurr = tcurrView.getFromCurr();
				String toCurr = tcurrView.getToCurr();
				Date validFrom = tcurrView.getValidFrom();
				double exchRate  = tcurrView.getExchRate();
				String dateFrom = "" + validFrom.getDate();
				String monthFrom = "";
				String yearFrom = "";
				int tempMonth = validFrom.getMonth();
				if(tempMonth == 0){
					monthFrom = "12";;
					yearFrom = "" + (validFrom.getYear()-1);
				}else{
					monthFrom = "" + validFrom.getMonth();
					yearFrom = "" + validFrom.getYear();
				}			
				double ratio = 0.0;
				String key = exRtType + "-" + fromCurr + "-" + toCurr + "-" + monthFrom + "-" + dateFrom + "-" + yearFrom;
				
				TCurfView tcurfView = tcurfViewMap.get(key);
				
				if(tcurfView != null){
					ratio = tcurfView.getRatioFrom() / tcurfView.getRatioTo();
					tcurrView.setExchRate(exchRate);
					tcurrView.setRatio(ratio);
					tcurrView.setFromDateInString(monthFrom+"/"+dateFrom+"/"+yearFrom);
					//-------------------------------------------------------------------
					String tempKey = exRtType + "-" + fromCurr + "-" + toCurr;
					ArrayList<TCurfView> tcurfTempViewList = (ArrayList<TCurfView>) tcurfViewTempMap.get(tempKey);
					ArrayList<Date> tcurrToDateList = new ArrayList<Date>();
					ArrayList<TCurrView> tcurrTempViewList = (ArrayList<TCurrView>) tcurrViewTempMap.get(tempKey);
					if(tcurfTempViewList != null){
						for(TCurrView tcurrTempView : tcurrTempViewList){
							Date date = tcurrTempView.getValidFrom();
							tcurrToDateList.add(date);
						}
						Collections.sort(tcurrToDateList);
						//TCurrDateView tcurrDateView = new TCurrDateView();
						//Date date = tcurfTempView.getValidFrom();
						//tcurrDateView.setValidFrom(date);
						int index = -1;
						Date fromDate = tcurrView.getValidFrom();
						for(Date dateToCompare : tcurrToDateList){
							index = index + 1;
							if(fromDate.getDate() ==  dateToCompare.getDate() & fromDate.getMonth() ==  dateToCompare.getMonth() & fromDate.getYear() ==  dateToCompare.getYear()){
								break;
							}
						}
						//int index = tcurrToDateList.indexOf((Date)date);
						if((index+1) < tcurrToDateList.size()){
							Date validTo = (Date) tcurrToDateList.get(index+1);
							Date daysAgo = DateUtils.addDays(validTo,-1);
							String dateTo = "" + daysAgo.getDate();
							String monthTo = "";
							String yearTo = "";
							tempMonth = daysAgo.getMonth();
							if(tempMonth == 0){
								monthTo = "12";;
								yearTo = "" + (daysAgo.getYear()-1);
							}else{
								monthTo = "" + daysAgo.getMonth();
								yearTo = "" + daysAgo.getYear();
							}	
							tcurrView.setToDateInString(monthTo+"/"+dateTo+"/"+yearTo);
						}else{
							tcurrView.setToDateInString(12+"/"+31+"/"+9999);
						}
					}		
					//-------------------------------------------------------------------
					outputTcurfViewList.add(tcurrView);
				}else{
					String tempKey = exRtType + "-" + fromCurr + "-" + toCurr;
					ArrayList<TCurfView> tcurfTempViewList = (ArrayList<TCurfView>) tcurfViewTempMap.get(tempKey);
					ArrayList<Date> tcurrToDateList = new ArrayList<Date>();
					ArrayList<TCurrView> tcurrTempViewList = (ArrayList<TCurrView>) tcurrViewTempMap.get(tempKey);
					if(tcurrViewTempMap != null){
						for(TCurrView tcurrTempView : tcurrTempViewList){
							Date date = tcurrTempView.getValidFrom();
							tcurrToDateList.add(date);
						}
						Collections.sort(tcurrToDateList);
						if(tcurfTempViewList != null){
							for(TCurfView tcurfTempView : tcurfTempViewList){
								int index = -1;
								Date fromDate = tcurrView.getValidFrom();
								for(Date dateToCompare : tcurrToDateList){
									index = index + 1;
									if(fromDate.getDate() ==  dateToCompare.getDate() & fromDate.getMonth() ==  dateToCompare.getMonth() & fromDate.getYear() ==  dateToCompare.getYear()){
										break;
									}
								}
								ratio = tcurfTempView.getRatioFrom() / tcurfTempView.getRatioTo();							
								tcurrView.setRatio(ratio);
								tcurrView.setFromDateInString(monthFrom+"/"+dateFrom+"/"+yearFrom);
								if((index+1) < tcurrToDateList.size()){
									Date validTo = (Date) tcurrToDateList.get(index+1);
									Date daysAgo = DateUtils.addDays(validTo,-1);
									String dateTo = "" + daysAgo.getDate();
									String monthTo = "";
									String yearTo = "";
									tempMonth = daysAgo.getMonth();
									if(tempMonth == 0){
										monthTo = "12";;
										yearTo = "" + (daysAgo.getYear()-1);
									}else{
										monthTo = "" + daysAgo.getMonth();
										yearTo = "" + daysAgo.getYear();
									}							
									tcurrView.setToDateInString(monthTo+"/"+dateTo+"/"+yearTo);
								}else{
									tcurrView.setToDateInString(12+"/"+31+"/"+9999);
								}
								outputTcurfViewList.add(tcurrView);
							}
						}else{
							//for(TCurfView tcurfTempView : tcurfTempViewList){
								int index = -1;
								Date fromDate = tcurrView.getValidFrom();
								for(Date dateToCompare : tcurrToDateList){
									index = index + 1;
									if(fromDate.getDate() ==  dateToCompare.getDate() & fromDate.getMonth() ==  dateToCompare.getMonth() & fromDate.getYear() ==  dateToCompare.getYear()){
										break;
									}
								}								
								ratio = 1;							
								tcurrView.setRatio(ratio);
								tcurrView.setFromDateInString(monthFrom+"/"+dateFrom+"/"+yearFrom);
								if((index+1) < tcurrToDateList.size()){
									Date validTo = (Date) tcurrToDateList.get(index+1);
									Date daysAgo = DateUtils.addDays(validTo,-1);
									String dateTo = "" + daysAgo.getDate();
									String monthTo = "";
									String yearTo = "";
									tempMonth = daysAgo.getMonth();
									if(tempMonth == 0){
										monthTo = "12";;
										yearTo = "" + (daysAgo.getYear()-1);
									}else{
										monthTo = "" + daysAgo.getMonth();
										yearTo = "" + daysAgo.getYear();
									}							
									tcurrView.setToDateInString(monthTo+"/"+dateTo+"/"+yearTo);
								}else{
									tcurrView.setToDateInString(12+"/"+31+"/"+9999);
								}
								outputTcurfViewList.add(tcurrView);
							//}
						}
					}					
				}
			}
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA TCURR Profile. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}
		return outputTcurfViewList;
	}
}
