package com.relevance.ppa.service;

import java.util.List;

import com.relevance.prism.dao.E2emfDao;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.Master;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.service.Service;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;

public class EMEAMasterService implements Service {
//E2emfAppUtil appUtil;
	
	public EMEAMasterService() {
		//appUtil = new E2emfAppUtil();		
	}

	@Override
	public String getJsonObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJsonObject(Object... obj) throws AppException{
		Log.Info("getJsonObject called to fetch Master Json Obj for Slob...");
		String masterSearchJson = null;
		try{

			E2emfDao masterDao = PPAServiceLocator.getServiceDaoInstance("emeamaster");
			E2emfBusinessObject master = masterDao.getBusinessObject(obj);
			masterSearchJson = generateJsonString(master);
			
			Log.Info("Slob MasterSearchJson generated is : " + masterSearchJson);

		}catch(AppException appe){
			throw appe;
		}catch(Exception e){
			Log.Error("getJsonObject called to fetch Master Json Obj for Slob..." + e.getStackTrace());
			e.printStackTrace(System.out);
		}


		return masterSearchJson;
	}

	@Override
	public E2emfBusinessObject getBusinessObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfBusinessObject getBusinessObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public E2emfView getViewObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GoodsHistoryList getGoodsHistoryViewObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public E2emfView getViewObject(E2emfBusinessObject material) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String generateJsonString(Object... obj) {
		StringBuilder sbNode = new StringBuilder();
		String masterDataJson = "";
		
		
		List<MasterItem> masterPojoList = null;
		if(obj != null && obj[0] != null){
			Master master = (Master) obj[0];
			masterPojoList = master.getMasterDataList();
	
		}else {
			Log.Error("Obj is null to creat json ..");
			return "";
		}
		
	

		try {
			sbNode.append("[");

			if (masterPojoList.size() != 0) {
				for (MasterItem masterPojo : masterPojoList) {
					sbNode.append("{");
					sbNode.append(E2emfAppUtil.getKeyValue("id", masterPojo.getId()));
					if(masterPojo.getName() != null){
						sbNode.append(",");
						sbNode.append(E2emfAppUtil.getKeyValue("name",
								masterPojo.getName()));
					}
					sbNode.append("}");
					sbNode.append(",");
				}
				masterDataJson = sbNode.toString();
				masterDataJson = masterDataJson.substring(0,
						masterDataJson.length() - 1);
			} else {
				masterDataJson = masterDataJson + "[";
			}

			masterDataJson = masterDataJson + "]";

		} catch (Exception e) {
			Log.Error("Exception creating JsonObject for Master..." + e.getStackTrace());
			e.printStackTrace();
		}

		return masterDataJson;
	}

	@Override
	public MaterialReportList getMaterialReportViewObject(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public E2emfMaterialFlowData getE2emfMaterialFlowData(String source)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public String getActions(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRoles(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPrimaryRoles(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSearchSuggestion(Object... obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

}
