package com.relevance.ppa.service;

import java.util.ArrayList;

import com.relevance.ppa.dao.PPAItemMasterDao;
import com.relevance.ppa.dao.PlantPurchaseAnalyticsDao;
import com.relevance.ppa.data.PPAMaterialMapping;
import com.relevance.ppa.data.PlantPurchaseAnalyticsView;
import com.relevance.prism.data.Master;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;

public class PlantPurchaseAnalyticsService {
	public PlantPurchaseAnalyticsView getMaterialMapping(Object... obj) throws AppException{
		PlantPurchaseAnalyticsView pantPurchaseAnalyticsView = new PlantPurchaseAnalyticsView();
		ArrayList<PPAMaterialMapping> materialMappingList = null;
		try{
			PlantPurchaseAnalyticsDao plantPurchaseAnalyticsDao = new PlantPurchaseAnalyticsDao();
			materialMappingList = plantPurchaseAnalyticsDao.getMaterialMapping(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching profile slob  in the Slob Profile Service " + e.getMessage());
			Log.Error(e);
		}
		pantPurchaseAnalyticsView.setMaterialMapping(materialMappingList);
		return pantPurchaseAnalyticsView;
	}
	
	public void generateMaterialMapping() throws AppException{
		PPAItemMasterDao pPAItemMasterDao = new PPAItemMasterDao();
		try{
			pPAItemMasterDao.truncateMatchingTable();
			
			if ("true".equalsIgnoreCase(E2emfAppUtil.getAppProperty(E2emfConstants.ppaEnableGenericMatch)))
			{	
				//New logic
				pPAItemMasterDao.findPPAMatches(null,"bpcs","bpcs");
				pPAItemMasterDao.findPPAMatches(null,"sse","bpcs");
			}
			else
			{
				//Old logic
				pPAItemMasterDao.itemMasterMapping(null,"bpcs","bpcs");
				pPAItemMasterDao.itemMasterMapping(null,"sse","bpcs");
			}
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching profile slob  in the generateMaterialMapping Service " + e.getMessage());
			Log.Error(e);
			throw new AppException(e.getMessage());
		}
	}
	
	/*public PlantPurchaseAnalyticsView getMaterialMappingBpcs(Object... obj) throws AppException{
		PlantPurchaseAnalyticsView pantPurchaseAnalyticsView = new PlantPurchaseAnalyticsView();
		ArrayList<PPAMaterialMapping> materialMappingList = null;
		try{
			ReversePlantPurchaseAnalyticsDao plantPurchaseAnalyticsDao = new ReversePlantPurchaseAnalyticsDao();
			materialMappingList = plantPurchaseAnalyticsDao.getMaterialMappingBPCS(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching profile slob  in the Slob Profile Service " + e.getMessage());
			Log.Error(e);
		}
		pantPurchaseAnalyticsView.setMaterialMapping(materialMappingList);
		return pantPurchaseAnalyticsView;
	}*/
	public Master getSearchMaterialMapping(Object... obj) throws AppException{
		Master pantPurchaseAnalyticsList = null;
		try{
			PlantPurchaseAnalyticsDao plantPurchaseAnalyticsDao = new PlantPurchaseAnalyticsDao();
			pantPurchaseAnalyticsList = plantPurchaseAnalyticsDao.getSearchMaterialMapping(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching profile slob  in the Slob Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return pantPurchaseAnalyticsList;
	}
	
	
		public boolean compareAndsaveMaterialMappingInDB(Object... obj) throws AppException{
		
		boolean compareSuccess = false;
		try{
			PlantPurchaseAnalyticsDao plantPurchaseAnalyticsDao = new PlantPurchaseAnalyticsDao();
			compareSuccess = plantPurchaseAnalyticsDao.compareAndsaveMaterialMappingInDB(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching profile slob  in the Slob Profile Service " + e.getMessage());
			Log.Error(e);
		}
		//pantPurchaseAnalyticsView.setMaterialMapping(materialMappingList);
		return compareSuccess;
	}
		/*public boolean compareAndsaveMaterialMappingInDBBPCS(Object... obj) throws AppException{
			
			boolean compareSuccess = false;
			try{
				ReversePlantPurchaseAnalyticsDao plantPurchaseAnalyticsDao = new ReversePlantPurchaseAnalyticsDao();
				compareSuccess = plantPurchaseAnalyticsDao.compareAndsaveMaterialMappingInDBBPCSToSSE(obj);
			}catch(AppException appe){
				throw appe;
			} catch(Exception e) {
				Log.Error("Exception fetching profile slob  in the Slob Profile Service " + e.getMessage());
				Log.Error(e);
			}
			//pantPurchaseAnalyticsView.setMaterialMapping(materialMappingList);
			return compareSuccess;
		}*/
	public ArrayList<PPAMaterialMapping>  compareAndDownloadMaterialMapping(Object... obj) throws AppException{
		
			ArrayList<PPAMaterialMapping> materialMappingList =new ArrayList<PPAMaterialMapping>();
			try{
				PlantPurchaseAnalyticsDao plantPurchaseAnalyticsDao = new PlantPurchaseAnalyticsDao();
				materialMappingList  = plantPurchaseAnalyticsDao.compareAndDownloadMaterialMapping(obj);
			}catch(AppException appe){
				throw appe;
			} catch(Exception e) {
				Log.Error("Exception fetching profile slob  in the Slob Profile Service " + e.getMessage());
				Log.Error(e);
			}
			//pantPurchaseAnalyticsView.setMaterialMapping(materialMappingList);
			return materialMappingList;
		}
		
}
