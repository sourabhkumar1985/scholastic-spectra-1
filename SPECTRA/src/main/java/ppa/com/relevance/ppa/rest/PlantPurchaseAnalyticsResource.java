package com.relevance.ppa.rest;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.ppa.data.PPAMaterialMapping;
import com.relevance.ppa.data.PlantPurchaseAnalyticsView;
import com.relevance.ppa.service.PlantPurchaseAnalyticsService;
import com.relevance.prism.data.Master;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;

@Path("/PlantPurchase")
@Consumes(MediaType.APPLICATION_JSON)
public class PlantPurchaseAnalyticsResource {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/materialMapping")
	public String materialMapping(@FormParam("plants") String plants,
			@FormParam("source") String source,@FormParam("target") String target) throws AppException {

		String materialMappingResponse = null;
		PlantPurchaseAnalyticsView plantPurchaseAnalyticsView = new PlantPurchaseAnalyticsView();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		try {
			plantPurchaseAnalyticsView = (PlantPurchaseAnalyticsView)plantPurchaseAnalyticsService.getMaterialMapping(plants, source,target);
			materialMappingResponse = gson.toJson(plantPurchaseAnalyticsView);
			materialMappingResponse = DataObfuscator.obfuscate(materialMappingResponse);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in getSearchParams for GBT Search " + e);
			Log.Error(e.getMessage());
		}
		return materialMappingResponse;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/generateMaterialMapping")
	public String generateMaterialMapping(@FormParam("plants") String plants,
			@FormParam("source") String source,@FormParam("target") String target) throws AppException {
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		try {
			plantPurchaseAnalyticsService.generateMaterialMapping();
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in getSearchParams for GBT Search " + e);
			Log.Error(e.getMessage());
		}
		return "{\"message\" : \"Successfully generated material mapping.\"}";
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/materialMappingDirection")
	public String materialMappingWithDirection(@FormParam("plants") String plants,
			@FormParam("source") String source,@FormParam("target") String target,@FormParam("direction") String direction) throws AppException {

		String materialMappingResponse = null;
		PlantPurchaseAnalyticsView plantPurchaseAnalyticsView = new PlantPurchaseAnalyticsView();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		try {
			plantPurchaseAnalyticsView = (PlantPurchaseAnalyticsView)plantPurchaseAnalyticsService.getMaterialMapping(plants, source,target,direction);
			materialMappingResponse = gson.toJson(plantPurchaseAnalyticsView);
			materialMappingResponse = DataObfuscator.obfuscate(materialMappingResponse);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in getSearchParams for GBT Search " + e);
			Log.Error(e.getMessage());
		}
		return materialMappingResponse;
	}
	/*@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/materialmappingbpcs")
	public String materialMappingBpcs(@FormParam("plants") String plants,
			@FormParam("source") String source,@FormParam("target") String target) throws AppException {

		String materialMappingResponse = null;
		PlantPurchaseAnalyticsView plantPurchaseAnalyticsView = new PlantPurchaseAnalyticsView();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		try {
			plantPurchaseAnalyticsView = (PlantPurchaseAnalyticsView)plantPurchaseAnalyticsService.getMaterialMappingBpcs(plants, source,target);
			materialMappingResponse = gson.toJson(plantPurchaseAnalyticsView);
			materialMappingResponse = DataObfuscator.obfuscate(materialMappingResponse);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in getSearchParams for GBT Search " + e);
			Log.Error(e.getMessage());
		}
		return materialMappingResponse;
	}*/
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchMaterialMapping")
	public String searchMaterialMapping(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam,
			@QueryParam("source") String source) {
		
		long startTime = System.currentTimeMillis();
		long endTime = 0L;
		
		Log.Info("Recieved request Material Mapping search Profile view to fetch Material Mapping Profile Details.");
		
		String materialMappingResponse = null;
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		try {
			PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
			Master  plantPurchaseAnalyticsList = plantPurchaseAnalyticsService.getSearchMaterialMapping(varX, searchParam,source);
			materialMappingResponse = gson.toJson(plantPurchaseAnalyticsList);
			endTime = System.currentTimeMillis();
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in getSearchParams for GBT Search " + e);
			Log.Error(e.getMessage());
		}
		
		Log.Info("Returning Material Mapping search View ,  total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return materialMappingResponse;
	}
	
	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/matmaptodb")
	public String materialMappingExportToDB(@QueryParam("plants") String plants,
			@QueryParam("source") String source,@QueryParam("target") String target) throws AppException {

		String materialMappingResponse = null;
		
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		try {
			boolean isSuccess = plantPurchaseAnalyticsService.compareAndsaveMaterialMappingInDB(plants, source,target);
			materialMappingResponse = gson.toJson(isSuccess);
			materialMappingResponse = DataObfuscator.obfuscate(materialMappingResponse);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception Material Mapping Export " + e);
			Log.Error(e.getMessage());
		}
		return materialMappingResponse;
	}
	
	
	/*@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	
	@Path("/exportmatmap")
	public void  downloadMaterialMapping(@QueryParam("plants") String plants,
			@QueryParam("source") String source,@QueryParam("target")   String target,HttpServletResponse response
			) throws AppException {

		String materialMappingResponse = null;
		
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		File file=new File("materialmappingresult.txt");
		ArrayList<PPAMaterialMapping> lstmapping=new ArrayList<PPAMaterialMapping>();
 		try {
 			lstmapping = plantPurchaseAnalyticsService.compareAndDownloadMaterialMapping(plants,source,target);
			FileOutputStream fout=new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fout));
			for (PPAMaterialMapping m : lstmapping) {
				bw.write(getRowData(m));
				bw.newLine();			 
				bw.close();
			}	
			
			final FileInputStream fin=new FileInputStream(file);			
			/*StreamingOutput streamOutput = new StreamingOutput(){
				public void write(OutputStream output) throws IOException, WebApplicationException {
					try {
						IOUtils.copy(fin,output);
					} catch (Exception e) {
						throw new WebApplicationException(e);
					}
				}
			};		
			ResponseBuilder response = Response.ok((Object) streamOutput, "text/plain");
			response.header("Content-Disposition", "attachment; filename=materialmappingresult.txt");
			return response.build();	
			response.setContentType("text/plain");
			response.setHeader("Content-Disposition", "attachment; filename=materialmappingresult.txt");
			ServletOutputStream out=response.getOutputStream();
			IOUtils.copy(fin, out);
			out.flush();		
			out.close();
		} catch (AppException appe) {
		
		} catch (Exception e) { 
			Log.Error("Exception Material Mapping Export " + e);
			Log.Error(e.getMessage());
			
		}
	
	}*/
	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/downloadmatmap")
	public Response  downloadMatMapping(@QueryParam("plants") String plants,
			@QueryParam("source") String source,@QueryParam("target")   String target,
			@QueryParam("filepath")   String filepath
			) throws AppException {

		/*String materialMappingResponse = null;
		
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();*/
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		System.out.println(filepath);
		
		ArrayList<PPAMaterialMapping> lstmapping=new ArrayList<PPAMaterialMapping>();
 		try {
 			lstmapping = plantPurchaseAnalyticsService.compareAndDownloadMaterialMapping(plants,source,target);
 			File file=new File(filepath);
			FileOutputStream fout=new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fout));
			for (PPAMaterialMapping m : lstmapping) {
				bw.write(getRowData(m));
				bw.newLine();			 
				
			}	
			//bw.write("testing");
			bw.close();
			final FileInputStream fin=new FileInputStream(file);			
		StreamingOutput streamOutput = new StreamingOutput(){
				public void write(OutputStream output) throws IOException, WebApplicationException {
					try {
						IOUtils.copy(fin,output);
					} catch (Exception e) {
						throw new WebApplicationException(e);
					}
				}
			};		
			ResponseBuilder responsebuilder = Response.ok((Object) streamOutput, "text/plain");
			responsebuilder.header("Content-Disposition", "attachment; filename=materialmappingresult.txt");
			return responsebuilder.build();	
			//response.setContentType("text/plain");
			/*response.setHeader("Content-Disposition", "attachment;filename=materialmappingresult.txt");
			ServletOutputStream out=response.getOutputStream();
			IOUtils.copy(fin, out);
			out.flush();		
			out.close();*/
		
		
		} catch (Exception e) { 
			Log.Error("Exception Material Mapping Export " + e);
			Log.Error(e.getMessage());
			e.printStackTrace();
			return null;
		}
	
	}
	/*@GET
	@Path("/downloadmatmap")
	public Response  downloadMatMap(@QueryParam("plants") String plants,
			@QueryParam("source") String source,@QueryParam("target")   String target,@QueryParam("filepath") String filepath
			) throws AppException {

		String materialMappingResponse = null;
		StringBuilder sb=new StringBuilder();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
	///	File file=new File("materialmappingresult.txt");
		ArrayList<PPAMaterialMapping> lstmapping=new ArrayList<PPAMaterialMapping>();
 		try {
 			lstmapping = plantPurchaseAnalyticsService.compareAndDownloadMaterialMapping(plants,source,target);	
		
 			File file=new File(filepath);
			FileOutputStream fout=new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fout));
		System.out.println("Exporting mapping result data to file process started");
		for (PPAMaterialMapping m : lstmapping) {
			//sb.append(getRowData(m));
			//sb.append("\n");
			bw.write(getRowData(m));
			bw.newLine();	
		}
		 bw.close();
			final FileInputStream fin=new FileInputStream(file);			
			StreamingOutput streamOutput = new StreamingOutput(){
					public void write(OutputStream output) throws IOException, WebApplicationException {
						try {
							IOUtils.copy(fin,output);
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};		
		ResponseBuilder responsebuilder = Response.ok((Object) streamOutput, "text/plain");
		responsebuilder.header("Content-Disposition", "attachment; filename=materialmappingresult.txt");
		return responsebuilder.build();	
		} catch (AppException appe) {
		return null;
		} catch (Exception e) { 
			Log.Error("Exception Material Mapping Export " + e);
			Log.Error(e.getMessage());
			return null;
		}
	//return sb.toString();
	}*/
		@GET
		@Path("/exportmatmap")
		public String  exportmatmap(@QueryParam("plants") String plants,
				@QueryParam("source") String source,@QueryParam("target")   String target
				) throws AppException {

			//String materialMappingResponse = null;
			StringBuilder sb=new StringBuilder();
			//Gson gson = new GsonBuilder().disableHtmlEscaping().create();
			PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		///	File file=new File("materialmappingresult.txt");
			ArrayList<PPAMaterialMapping> lstmapping=new ArrayList<PPAMaterialMapping>();
	 		try {
	 			lstmapping = plantPurchaseAnalyticsService.compareAndDownloadMaterialMapping(plants,source,target);	
				
				//File file=new File(filepath);
			//FileOutputStream fout=new FileOutputStream(file);
			//BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fout));
				
			System.out.println("Exporting mapping result data to file process started");
			for (PPAMaterialMapping m : lstmapping) {
				sb.append(getRowData(m));
				sb.append("\n");				
			}
			 // bw.close();	
				
			} catch (AppException appe) {
			return null;
			} catch (Exception e) { 
				Log.Error("Exception Material Mapping Export " + e);
				Log.Error(e.getMessage());
				return null;
			}
		return sb.toString();
		}

	/*
	/*@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/matmaptodbbpcs")
	public String materialMappingExportToDBReverse(@QueryParam("plants") String plants,
			@QueryParam("source") String source,@QueryParam("target") String target) throws AppException {

		String materialMappingResponse = null;
		
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		PlantPurchaseAnalyticsService plantPurchaseAnalyticsService = new PlantPurchaseAnalyticsService();
		try {
			boolean isSuccess = plantPurchaseAnalyticsService.compareAndsaveMaterialMappingInDBBPCS(plants, source,target);
			materialMappingResponse = gson.toJson(isSuccess);
			materialMappingResponse = DataObfuscator.obfuscate(materialMappingResponse);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception Material Mapping Export " + e);
			Log.Error(e.getMessage());
		}
		return materialMappingResponse;
	}*/
	
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
	
	public  String getRowData(PPAMaterialMapping m){
		String row="";
                m=RemoveEmptyValue(m);
		row =  m.getSourceSystem() + "|"+  
						 m.getSourcePlant()  + "|"+  
						m.getSourceSAPPlant()  + "|"+  
						m.getSourcePlantDesc()  + "|"+  
						m.getSourceSpecCode()  + "|"+  
						m.getSourceMaterialId()  + "|"+  
						m.getSourceMaterialName() +"|"+ ///.replace("\\","\\\\").replace("","\\")  + "|"+  // need to replace  " for hive
						m.getSubcategory() + "|"+ 
						m.getSourceContractStartDate()  + "|"+ 
						m.getSourceContractEndDate()  + "|"+ 
						m.getCategory() + "|"+   //.replace("","\\")  + ","+  
						m.getSourceSupplier() + "|"+   //.replace("","\\")  + ","+  
						m.getContractStatus()  + "|"+  
//						m.getSourceLatest()  + ","+  
					String.format("%.02f", m.getSourcePrice()) + "|" +
						m.getSourceCurrency()  + "|"+  
						m.getSourceIncoterm()  + "|"+  
						m.getSourceLeadTime()  + "|"+  
						String.valueOf((int)((double)(Double.valueOf(m.getSourceMinOrderQuantity() ) ))) + "|"+  
						//String.valueOf(m.getSourceMinOrderQuantity()  )+ "|"+  
						m.getTargetSystem()  + "|"+  
						m.getTargetPlant()  + "|"+  
						m.getTargetSAPPlant()  + "|"+  
						m.getTargetSpecCode()  + "|"+  
						m.getTargetMaterialId()  + "|"+  
						m.getTargetMaterialName()  + "|"+  
						m.getTargetSupplierId()  + "|"+  
						m.getTargetSupplier()  + "|"+  					
						String.format("%.02f", m.getTargetPrice()) + "|" +
						String.format("%.02f", m.getTargetPriceByPrice()) + "|" +					
						m.getType()  + "|"+  
						m.getTypeDesc()  + "|"+  
						m.getTypeCategory()  + "|"+  
						String.valueOf(m.getLvDistnaceDesc())  + "|"+  
						String.valueOf(m.getLvDistnaceSupplier())  + "|"+  						
						String.format("%.02f", m.getPriceDiff()) + "|" +
						String.format("%.02f", m.getPriceDiffByPrice()) + "|" +				
						String.valueOf(m.getWeightage())  + "|" +
						String.valueOf(m.getDirection()) + "|" +
						m.getPurchaseHistory() + "|" +
						m.getVendorNo() + "|" +
						m.getVendorCity() + "|" +  
						m.getMaterialCreationDate() + "|" +   
						m.getGalaxyUom();
		 				row = row.replace("\t", "");
		return  row;
	}
        public  PPAMaterialMapping RemoveEmptyValue(PPAMaterialMapping m) {
        if ((m.getSourcePlant() != null && m.getSourcePlant().equals("")) 
        		|| (m.getSourcePlant() != null && m.getSourcePlant().equals("NULL"))
        	|| m.getSourcePlant() == null)        {
            m.setSourcePlant("NA");
        }
        else
        	 m.setSourcePlant(m.getSourcePlant().trim());
        if ((m.getSourceSAPPlant() != null && m.getSourceSAPPlant().equals("")) 
        		|| (m.getSourceSAPPlant() != null && m.getSourceSAPPlant().equals("NULL")) 
        	|| m.getSourceSAPPlant() == null)        {
            m.setSourceSAPPlant("NA");
        }
        else
        	 m.setSourceSAPPlant(m.getSourceSAPPlant().trim());
        
        if ((m.getContractStatus() != null && m.getContractStatus().equals("")) 
        		|| (m.getContractStatus() != null && m.getContractStatus().equals("NULL")) 
        	|| m.getContractStatus() == null)        {
            m.setContractStatus("NA");
        }
        else
       	 	m.setContractStatus(m.getContractStatus().trim());
        	
        
        if ((m.getSourcePlantDesc() != null && m.getSourcePlantDesc().equals(""))
        		|| (m.getSourcePlantDesc() != null && m.getSourcePlantDesc().equals("NULL"))
        		|| m.getSourcePlantDesc() == null)        {
            m.setSourcePlantDesc("NA");
        }
        else
       	 	m.setSourcePlantDesc(m.getSourcePlantDesc().trim());
        if ((m.getSourceSpecCode() != null && m.getSourceSpecCode().equals("")) 
        		|| (m.getSourceSpecCode() != null && m.getSourceSpecCode().equals("NULL"))
         	|| m.getSourceSpecCode() == null)        {
            m.setSourceSpecCode("NA");
        }
        else
       	 	m.setSourceSpecCode(m.getSourceSpecCode().trim());
        if ((m.getSourceMaterialId() != null && m.getSourceMaterialId().equals(""))
        		|| (m.getSourceMaterialId() != null && m.getSourceMaterialId().equals("NULL"))
        	|| m.getSourceMaterialId() == null)        {
            m.setSourceMaterialId("NA");
        }
        else
       	 	m.setSourceMaterialId(m.getSourceMaterialId().trim());
        if ((m.getSourceMaterialName() != null && m.getSourceMaterialName().equals(""))
        		|| (m.getSourceMaterialName() != null && m.getSourceMaterialName().equals("NULL"))
        	|| m.getSourceMaterialName()== null){
            m.setSourceMaterialName("NA");
        }else{
        	m.setSourceMaterialName(m.getSourceMaterialName().trim().replaceAll("[?!$]+", ""));
        }
        
        if ((m.getSubcategory() != null && m.getSubcategory().equals(""))
        		|| (m.getSubcategory() != null && m.getSubcategory().equals("NULL"))
        	|| m.getSubcategory()== null){
            m.setSubcategory("NA");
        }  else{
        	m.setSubcategory(m.getSubcategory().trim().replaceAll("[?!$]+", ""));
        }
        if ((m.getSourceContractStartDate() != null && m.getSourceContractStartDate().equals(""))
        		|| (m.getSourceContractStartDate() != null && m.getSourceContractStartDate().equals("NULL")) 
        		|| m.getSourceContractStartDate() == null)        {
            m.setSourceContractStartDate("NA");
        }
        else
       	 	m.setSourceContractStartDate(m.getSourceContractStartDate().trim());
        if ((m.getSourceContractEndDate() != null && m.getSourceContractEndDate().equals(""))
        		|| (m.getSourceContractEndDate() != null && m.getSourceContractEndDate().equals("NULL")) 
        		|| m.getSourceContractEndDate() == null)        {
            m.setSourceContractEndDate("NA");
        }
        else
       	 	m.setSourceContractEndDate(m.getSourceContractEndDate().trim());
        if ((m.getCategory() != null && m.getCategory().equals(""))
        		|| (m.getCategory() != null && m.getCategory().equals("NULL")) 
        		|| m.getCategory() == null)        {
            m.setCategory("NA");
        }
        else
       	 	m.setCategory(m.getCategory().trim());

        if ((m.getSourceSupplier() != null && m.getSourceSupplier().equals(""))
        		|| (m.getSourceSupplier() != null && m.getSourceSupplier().equals("NULL"))
        	|| m.getSourceSupplier()== null){
            m.setSourceSupplier("NA");
        }else{
        	m.setSourceSupplier(m.getSourceSupplier().trim().replaceAll("[?!$]+", ""));
        }
//        if (m.getSourceLatest() != null && m.getSourceLatest().equals("")) {
//            m.setSourceLatest("NA");
//        }
        if ((m.getSourceCurrency() != null && m.getSourceCurrency().equals(""))
        		|| (m.getSourceCurrency() != null && m.getSourceCurrency().equals("NULL")) 
        	|| m.getSourceCurrency()== null){
            m.setSourceCurrency("NA");
        }
        else
       	 	m.setSourceCurrency(m.getSourceCurrency().trim());
        if ((m.getSourceIncoterm() != null && m.getSourceIncoterm().equals(""))
        		|| (m.getSourceIncoterm() != null && m.getSourceIncoterm().equals("NULL"))
         	|| m.getSourceIncoterm()== null){
            m.setSourceIncoterm("NA");
        }
        else
       	 	m.setSourceIncoterm(m.getSourceIncoterm().trim());
        if ((m.getSourceMinOrderQuantity() != null && m.getSourceMinOrderQuantity().equals(""))
        		|| (m.getSourceMinOrderQuantity() != null && m.getSourceMinOrderQuantity().equals("NULL"))
        		|| m.getSourceMinOrderQuantity() == null){
            m.setSourceMinOrderQuantity("0.0");
        }
        
        if ((m.getSourceLeadTime() != null && m.getSourceLeadTime().equals(""))
        		|| (m.getSourceLeadTime() != null && m.getSourceLeadTime().equals("NULL")) 
        	|| m.getSourceLeadTime()== null){
            m.setSourceLeadTime("NA");
        }
        else
       	 	m.setSourceLeadTime(m.getSourceLeadTime().trim());
        if ((m.getTargetSystem() != null && m.getTargetSystem().equals(""))
        		|| (m.getTargetSystem() != null && m.getTargetSystem().equals("NULL")) 
        	|| m.getTargetSystem()== null){
            m.setTargetSystem("NA");
        }
        else
       	 	m.setTargetSystem(m.getTargetSystem().trim());
        if ((m.getTargetPlant() != null && m.getTargetPlant().equals(""))
        		|| (m.getTargetPlant() != null && m.getTargetPlant().equals("NULL")) 
        		|| m.getTargetPlant()== null){
            m.setTargetPlant("NA");
        }
        else
       	 	m.setTargetPlant(m.getTargetPlant().trim());

        ///
        if ((m.getTargetSAPPlant() != null && m.getTargetSAPPlant().equals(""))
        		|| (m.getTargetSAPPlant() != null && m.getTargetSAPPlant().equals("NULL"))
        	|| m.getTargetSAPPlant()== null){
            m.setTargetSAPPlant("NA");
        }
        else
       	 	m.setTargetSAPPlant(m.getTargetSAPPlant().trim());
        if ((m.getTargetSpecCode() != null && m.getTargetSpecCode().equals(""))
        		|| (m.getTargetSpecCode() != null && m.getTargetSpecCode().equals("NULL")) 
        		|| m.getTargetSpecCode()== null){
            m.setTargetSpecCode("NA");
        }
        else
       	 	m.setTargetSpecCode(m.getTargetSpecCode().trim());
        if ((m.getTargetMaterialId() != null && m.getTargetMaterialId().equals(""))
        		|| (m.getTargetMaterialId() != null && m.getTargetMaterialId().equals("NULL")) 
        	|| m.getTargetMaterialId()== null){
            m.setTargetMaterialId("NA");
        }
        else
       	 	m.setTargetMaterialId(m.getTargetMaterialId().trim());
        if ((m.getTargetMaterialName() != null && m.getTargetMaterialName().equals(""))
        		|| (m.getTargetMaterialName() != null && m.getTargetMaterialName().equals("NULL"))
        	|| m.getTargetMaterialName()== null){
            m.setTargetMaterialName("NA");
        }
        else
       	 	m.setTargetMaterialName(m.getTargetMaterialName().trim());
        if ((m.getTargetSupplierId() != null && m.getTargetSupplierId().equals(""))
        		|| (m.getTargetSupplierId() != null && m.getTargetSupplierId().equals("NULL")) 
        		|| m.getTargetSupplierId()== null){
            m.setTargetSupplierId("NA");
        }else{
        	m.setTargetSupplierId(m.getTargetSupplierId().trim().replaceAll("[?!$]+", ""));
        }
        if ((m.getTargetSupplier() != null && m.getTargetSupplier().equals(""))
        		|| (m.getTargetSupplier() != null && m.getTargetSupplier().equals("NULL"))
        	|| m.getTargetSupplier()== null){
            m.setTargetSupplier("NA");
        }
        else
        	m.setTargetSupplier(m.getTargetSupplier().replaceAll("[?!$]+", ""));
        if (m.getType() != null && m.getType().equals("")) {
            m.setType("NA");
        }
        if (m.getTypeDesc() != null && m.getTypeDesc().equals("")) {
            m.setTypeDesc("NA");
        }

        if (m.getTypeCategory() != null && m.getTypeCategory().equals("")) {
            m.setTypeCategory("NA");
        }
    if ((m.getVendorNo() != null && m.getVendorNo().equals("")) 
    	|| (m.getVendorNo() != null && m.getVendorNo().equals("NULL")) 
		|| m.getVendorNo()== null){
            m.setVendorNo("NA");
        }
    else
   	 	m.setVendorNo(m.getVendorNo().trim());
    if ((m.getVendorCity() != null && m.getVendorCity().equals("")) 
    	|| (m.getVendorCity() != null && m.getVendorCity().equals("NULL")) 
		|| m.getVendorCity()== null){
            m.setVendorCity("NA");
        }
    else
   	 	m.setVendorCity(m.getVendorCity().trim());
    if ((m.getPurchaseHistory() != null && m.getPurchaseHistory().equals("")) 
    	|| (m.getPurchaseHistory() != null && m.getPurchaseHistory().equals("NULL")) 
		|| m.getPurchaseHistory()== null){
            m.setPurchaseHistory("NA");
        }
    else
   	 	m.setPurchaseHistory(m.getPurchaseHistory().trim());
    if ((m.getMaterialCreationDate() != null && m.getMaterialCreationDate().equals("")) 
            || (m.getMaterialCreationDate() != null && m.getMaterialCreationDate().equals("NULL")) 
         || m.getMaterialCreationDate()== null){
                   m.setMaterialCreationDate("NA");
               }
    else
   	 	m.setMaterialCreationDate(m.getMaterialCreationDate().trim());
return m;
    }
}
