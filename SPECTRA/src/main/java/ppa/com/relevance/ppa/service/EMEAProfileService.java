package com.relevance.ppa.service;

import java.util.ArrayList;

import com.relevance.ppa.dao.EMEAProfileDao;
import com.relevance.ppa.data.EMEAView;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.data.E2emfView;

public class EMEAProfileService {

	public ArrayList<EMEAView> getEMEAPOProfileViewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaViewList = null;
		try{
			EMEAProfileDao emeaProfileDao = new EMEAProfileDao();
			emeaViewList = emeaProfileDao.getEMEAPOProfileViewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA PO Profile in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPOProfileL2ViewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaPOProfileL2ViewList = null;
		try{
			EMEAProfileDao emeaProfileDao = new EMEAProfileDao();
			emeaPOProfileL2ViewList = emeaProfileDao.getEMEAPOProfileL2ViewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA PO Profile L2 View in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaPOProfileL2ViewList;
	}
	
	public ArrayList<EMEAView> getEMEAQuotesProfileViewObject() throws AppException{
		ArrayList<EMEAView> emeaQuotesProfileViewList = null;
		try{
			EMEAProfileDao emeaProfileDao = new EMEAProfileDao();
			emeaQuotesProfileViewList = emeaProfileDao.getEMEAQuotesProfileViewDetails();
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA Quotes Profile in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaQuotesProfileViewList;
	}
	
	public ArrayList<EMEAView> getEMEAQuotesProfileL2ViewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaQuotesProfileL2ViewList = null;
		try{
			EMEAProfileDao emeaQuotesProfileDao = new EMEAProfileDao();
			emeaQuotesProfileL2ViewList = emeaQuotesProfileDao.getEMEAQuotesProfileL2ViewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA Quotes Profile L2 View in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaQuotesProfileL2ViewList;
	}
	
	public E2emfView getEMEADataPagination(Object... obj) throws AppException{
		E2emfView dataView = null;
		try{
			EMEAProfileDao emeaProfileDao = new EMEAProfileDao();
			dataView = emeaProfileDao.getEMEADataPagination(obj);
		}catch(AppException appe){ 
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA  Profile L2 View in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return dataView;
	}
	
	public ArrayList<EMEAView> getEMEAPPVProfileViewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaPpvViewList = null;
		try{
			EMEAProfileDao emeaProfileDao = new EMEAProfileDao();
			emeaPpvViewList = emeaProfileDao.getEMEAPPVProfileViewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA PPV Profile in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaPpvViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPPVProfileL2ViewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaPPVProfileL2ViewList = null;
		try{
			EMEAProfileDao emeaPpvProfileDao = new EMEAProfileDao();
			emeaPPVProfileL2ViewList = emeaPpvProfileDao.getEMEAPPVProfileL2ViewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA Quotes Profile L2 View in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaPPVProfileL2ViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPOVarianceAnalysisViewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaPOVarianceAnalysisViewList = null;
		try{
			EMEAProfileDao emeaPpvProfileDao = new EMEAProfileDao();
			emeaPOVarianceAnalysisViewList = emeaPpvProfileDao.getEMEAPOVarianceAnalysisViewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA Quotes Profile L2 View in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaPOVarianceAnalysisViewList;
	}
	
	public ArrayList<EMEAView> getEMEAPPVOverviewObject(Object... obj) throws AppException{
		ArrayList<EMEAView> emeaPPVOverviewList = null;
		try{
			EMEAProfileDao emeaPPVOverviewDao = new EMEAProfileDao();
			emeaPPVOverviewList = emeaPPVOverviewDao.getEMEAPPVOverviewDetails(obj);
		}catch(AppException appe){
			throw appe;
		} catch(Exception e) {
			Log.Error("Exception fetching EMEA Quotes Profile L2 View in the EMEA Profile Service " + e.getMessage());
			Log.Error(e);
		}
		return emeaPPVOverviewList;
	}
}
