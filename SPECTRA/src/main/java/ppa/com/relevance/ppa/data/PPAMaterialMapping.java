package com.relevance.ppa.data;

public class PPAMaterialMapping {
	String sourceSystem;
	String sourcePlant;
	String sourceSAPPlant;
	String sourcePlantDesc;
	String sourceSpecCode;
	String sourceMaterialId;
	String sourceMaterialName;
	String sourceSupplierId;
	String subCategory;
	String sourceContractStartDate;
	String sourceContractEndDate;
	String category; //materialGroup
	String sourceSupplier;
	String contractStatus;
	float sourcePrice;
	String sourceCurrency;
	String sourceIncoterm;
	String sourceMinOrderQuantity;
	String sourceLeadTime;
	String targetSystem;
	String targetPlant;
	String targetSAPPlant;
	String targetPlantDesc;
	String targetSpecCode;
	String targetMaterialId;
	String targetMaterialName;
	String targetSupplierId;
	String targetSupplier;
	float targetPrice;
	String targetSystemByPrice;
	String targetPlantByPrice;
	String targetPlantDescByPrice;
	String targetSpecCodeByPrice;
	String targetMaterialIdByPrice;
	String targetMaterialNameByPrice;
	String targetSupplierIdByPrice;
	String targetSupplierByPrice;
	float targetPriceByPrice;
	String type;
	String typeDesc;
	String typeCategory;
	int lvDistnaceDesc;
	Boolean supplierPartialMatch;
	int lvDistnaceSupplier;
	float priceDiff;
	float priceDiffByPrice;
	int weightage;
	String direction;	
	String vendorNo;
	String vendorCity;
	String purchaseHistory;
	String materialCreationDate;
	String itemsCount;
	String materialDesc;
	String galaxyUom;
	
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourcePlant() {
		return sourcePlant;
	}
	public void setSourcePlant(String sourcePlant) {
		this.sourcePlant = sourcePlant;
	}
	public String getSourceSAPPlant() {
		return sourceSAPPlant;
	}
	public void setSourceSAPPlant(String sourceSAPPlant) {
		this.sourceSAPPlant = sourceSAPPlant;
	}
	public String getSourcePlantDesc() {
		return sourcePlantDesc;
	}
	public void setSourcePlantDesc(String sourcePlantDesc) {
		this.sourcePlantDesc = sourcePlantDesc;
	}
	public String getSourceSpecCode() {
		return sourceSpecCode;
	}
	public void setSourceSpecCode(String sourceSpecCode) {
		this.sourceSpecCode = sourceSpecCode;
	}
	public String getSourceMaterialId() {
		return sourceMaterialId;
	}
	public void setSourceMaterialId(String sourceMaterialId) {
		this.sourceMaterialId = sourceMaterialId;
	}
	public String getSourceMaterialName() {
		return sourceMaterialName;
	}
	public void setSourceMaterialName(String sourceMaterialName) {
		this.sourceMaterialName = sourceMaterialName;
	}
	public String getSourceSupplierId() {
		return sourceSupplierId;
	}
	public void setSourceSupplierId(String sourceSupplierId) {
		this.sourceSupplierId = sourceSupplierId;
	}
	public String getSourceContractStartDate() {
		return sourceContractStartDate;
	}
	public void setSourceContractStartDate(String sourceContractStartDate) {
		this.sourceContractStartDate = sourceContractStartDate;
	}
	public String getSourceContractEndDate() {
		return sourceContractEndDate;
	}
	public void setSourceContractEndDate(String sourceContractEndDate) {
		this.sourceContractEndDate = sourceContractEndDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSourceSupplier() {
		return sourceSupplier;
	}
	public void setSourceSupplier(String sourceSupplier) {
		this.sourceSupplier = sourceSupplier;
	}	
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public float getSourcePrice() {
		return sourcePrice;
	}
	public void setSourcePrice(float sourcePrice) {
		this.sourcePrice = sourcePrice;
	}
	public String getSourceCurrency() {
		return sourceCurrency;
	}
	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}
	public String getSourceIncoterm() {
		return sourceIncoterm;
	}
	public void setSourceIncoterm(String sourceIncoterm) {
		this.sourceIncoterm = sourceIncoterm;
	}
	public String getSourceMinOrderQuantity() {
		return sourceMinOrderQuantity;
	}
	public void setSourceMinOrderQuantity(String sourceMinOrderQuantity) {
		this.sourceMinOrderQuantity = sourceMinOrderQuantity;
	}
	public String getSourceLeadTime() {
		return sourceLeadTime;
	}
	public void setSourceLeadTime(String sourceLeadTime) {
		this.sourceLeadTime = sourceLeadTime;
	}
	public String getTargetSystem() {
		return targetSystem;
	}
	public void setTargetSystem(String targetSystem) {
		this.targetSystem = targetSystem;
	}
	public String getTargetPlant() {
		return targetPlant;
	}
	public void setTargetPlant(String targetPlant) {
		this.targetPlant = targetPlant;
	}
	public String getTargetSAPPlant() {
		return targetSAPPlant;
	}
	public void setTargetSAPPlant(String targetSAPPlant) {
		this.targetSAPPlant = targetSAPPlant;
	}
	public String getTargetPlantDesc() {
		return targetPlantDesc;
	}
	public void setTargetPlantDesc(String targetPlantDesc) {
		this.targetPlantDesc = targetPlantDesc;
	}
	public String getTargetSpecCode() {
		return targetSpecCode;
	}
	public void setTargetSpecCode(String targetSpecCode) {
		this.targetSpecCode = targetSpecCode;
	}
	public String getTargetMaterialId() {
		return targetMaterialId;
	}
	public void setTargetMaterialId(String targetMaterialId) {
		this.targetMaterialId = targetMaterialId;
	}
	public String getTargetMaterialName() {
		return targetMaterialName;
	}
	public void setTargetMaterialName(String targetMaterialName) {
		this.targetMaterialName = targetMaterialName;
	}
	public String getTargetSupplierId() {
		return targetSupplierId;
	}
	public void setTargetSupplierId(String targetSupplierId) {
		this.targetSupplierId = targetSupplierId;
	}
	public String getTargetSupplier() {
		return targetSupplier;
	}
	public void setTargetSupplier(String targetSupplier) {
		this.targetSupplier = targetSupplier;
	}
	public float getTargetPrice() {
		return targetPrice;
	}
	public void setTargetPrice(float targetPrice) {
		this.targetPrice = targetPrice;
	}
	public String getTargetSystemByPrice() {
		return targetSystemByPrice;
	}
	public void setTargetSystemByPrice(String targetSystemByPrice) {
		this.targetSystemByPrice = targetSystemByPrice;
	}
	public String getTargetPlantByPrice() {
		return targetPlantByPrice;
	}
	public void setTargetPlantByPrice(String targetPlantByPrice) {
		this.targetPlantByPrice = targetPlantByPrice;
	}
	public String getTargetPlantDescByPrice() {
		return targetPlantDescByPrice;
	}
	public void setTargetPlantDescByPrice(String targetPlantDescByPrice) {
		this.targetPlantDescByPrice = targetPlantDescByPrice;
	}
	public String getTargetSpecCodeByPrice() {
		return targetSpecCodeByPrice;
	}
	public void setTargetSpecCodeByPrice(String targetSpecCodeByPrice) {
		this.targetSpecCodeByPrice = targetSpecCodeByPrice;
	}
	public String getTargetMaterialIdByPrice() {
		return targetMaterialIdByPrice;
	}
	public void setTargetMaterialIdByPrice(String targetMaterialIdByPrice) {
		this.targetMaterialIdByPrice = targetMaterialIdByPrice;
	}
	public String getTargetMaterialNameByPrice() {
		return targetMaterialNameByPrice;
	}
	public void setTargetMaterialNameByPrice(String targetMaterialNameByPrice) {
		this.targetMaterialNameByPrice = targetMaterialNameByPrice;
	}
	public String getTargetSupplierIdByPrice() {
		return targetSupplierIdByPrice;
	}
	public void setTargetSupplierIdByPrice(String targetSupplierIdByPrice) {
		this.targetSupplierIdByPrice = targetSupplierIdByPrice;
	}
	public String getTargetSupplierByPrice() {
		return targetSupplierByPrice;
	}
	public void setTargetSupplierByPrice(String targetSupplierByPrice) {
		this.targetSupplierByPrice = targetSupplierByPrice;
	}
	public float getTargetPriceByPrice() {
		return targetPriceByPrice;
	}
	public void setTargetPriceByPrice(float targetPriceByPrice) {
		this.targetPriceByPrice = targetPriceByPrice;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTypeDesc() {
		return typeDesc;
	}
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	public String getTypeCategory() {
		return typeCategory;
	}
	public void setTypeCategory(String typeCategory) {
		this.typeCategory = typeCategory;
	}
	public int getLvDistnaceDesc() {
		return lvDistnaceDesc;
	}
	public void setLvDistnaceDesc(int lvDistnaceDesc) {
		this.lvDistnaceDesc = lvDistnaceDesc;
	}
	public Boolean getSupplierPartialMatch() {
		return supplierPartialMatch;
	}
	public void setSupplierPartialMatch(Boolean supplierPartialMatch) {
		this.supplierPartialMatch = supplierPartialMatch;
	}
	public int getLvDistnaceSupplier() {
		return lvDistnaceSupplier;
	}
	public void setLvDistnaceSupplier(int lvDistnaceSupplier) {
		this.lvDistnaceSupplier = lvDistnaceSupplier;
	}
	public float getPriceDiff() {
		return priceDiff;
	}
	public void setPriceDiff(float priceDiff) {
		this.priceDiff = priceDiff;
	}
	public float getPriceDiffByPrice() {
		return priceDiffByPrice;
	}
	public void setPriceDiffByPrice(float priceDiffByPrice) {
		this.priceDiffByPrice = priceDiffByPrice;
	}
	public int getWeightage() {
		return weightage;
	}
	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getSubcategory() {
		return subCategory;
	}
	public void setSubcategory(String subcategory) {
		this.subCategory = subcategory;
	}
	
	public String getVendorNo() {
		return vendorNo;
	}
	public void setVendorNo(String vendorNo) {
		this.vendorNo = vendorNo;
	}
	public String getVendorCity() {
		return vendorCity;
	}
	public void setVendorCity(String vendorCity) {
		this.vendorCity = vendorCity;
	}
	public String getPurchaseHistory() {
		return purchaseHistory;
	}
	public void setPurchaseHistory(String purchaseHistory) {
		this.purchaseHistory = purchaseHistory;
	}	
	
	public String getMaterialCreationDate() {
		return materialCreationDate;
	}
	public void setMaterialCreationDate(String materialCreationDate) {
		this.materialCreationDate = materialCreationDate;
	}	
	public String getItemsCount() {
		return itemsCount;
	}
	public void setItemsCount(String itemsCount) {
		this.itemsCount = itemsCount;
	}	
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public String getGalaxyUom() {
		return galaxyUom;
	}
	public void setGalaxyUom(String galaxyUom) {
		this.galaxyUom = galaxyUom;
	}	
	
}
