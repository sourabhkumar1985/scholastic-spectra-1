package com.relevance.ppa.data;

import java.util.Date;

public class TCurrDateView implements Comparable<TCurrDateView>{
	
	private Date validFrom;
		
	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	@Override
	public int compareTo(TCurrDateView o) {
		return getValidFrom().compareTo(o.getValidFrom());
	}
}
