package com.rl.controller;

import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.di.data.OracleDI;
import com.di.data.OracleObjects;

@RestController
@SpringBootApplication
@EnableAutoConfiguration
public class DiController {
	
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	
	@PreDestroy
    public void shutdonw() {
        // needed to avoid resource leak
        executor.shutdown(); 
    }

	
    @RequestMapping("/heartbeat")
    String home() {
        return "I am up";
    }
	


	@RequestMapping(value = "/api/load", method = RequestMethod.POST, produces = "application/json ; charset=utf-8")
	 public ResponseEntity<String> loadObject(@RequestBody String person) {
        
		executor.submit(() -> {
			try {
				doStuff(person);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}); 
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
           
	}
	
    private void doStuff(String json) throws Exception{
    	System.out.println(json);
    	OracleDI odi = new OracleDI();
    	odi.startload(json);
    }
    
    
    @RequestMapping(value = "/api/oracle/{containerName}", method = RequestMethod.GET, produces = "application/json ; charset=utf-8")
	 public ResponseEntity<Object> getObjectInfo(@PathVariable("containerName") String oContainerName)  {
    	System.out.println("Container Name Called is" + oContainerName);
    	String rs = "";
    	OracleObjects oObj = new OracleObjects();
    	ResponseEntity<Object> rse = null;
    	try {
    	 rs = oObj.getJson(oContainerName);
    	 rse = new ResponseEntity<>(rs,HttpStatus.OK);
    	}catch (Exception e) {
    		rse = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    		e.printStackTrace();
    	}
    	return rse;	
    }
   
    	
    	

	
	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(DiController.class);
		app.setDefaultProperties(Collections
		          .singletonMap("server.port", "7070"));
		app.run(args);
    }

}
