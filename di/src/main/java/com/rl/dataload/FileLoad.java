package com.rl.dataload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.rl.util.DiUtils;

import oracle.cloud.storage.CloudStorage;
import oracle.cloud.storage.CloudStorageConfig;
import oracle.cloud.storage.CloudStorageFactory;
import oracle.cloud.storage.model.Key;

public class FileLoad {
	static Properties prop = null;
	String propFileName = "conf/oracle.properties";
	static Logger log = Logger.getLogger(FileLoad.class.getName());
	 

	public static void main(String[] args) throws Exception {
		CloudStorage connection = null;
		FileLoad fileLoad = new FileLoad();
		fileLoad.readProperties();
		List<Key> fileList = null;
		FileHandler fh; 
		DiUtils diUtils = new DiUtils();
		String logFileName = "/tmp/" + diUtils.getHostName() + "_"+ diUtils.getFormattedDate();
		try {
			fh = new FileHandler(logFileName); 
			log.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);
			connection = fileLoad.getConnection();
			fileList = fileLoad.getListofFiles(connection);
			fileLoad.loadToMysql(fileList);
			  
		}catch (Exception e) {
			e.printStackTrace();
			diUtils.sendExceptionEmail(e, prop);
			throw e;
		}
		
		diUtils.sendSuccessEamil(prop,"File load Program completed Successfully + \n " + fileLoad.getSuccessMessage(logFileName) ,"FileLoad");
	}

private void readProperties() {
		
		log.info("Reading Properties File ...............");
		try (InputStream input = new FileInputStream(propFileName)) {
			prop = new Properties();
			prop.load(input);
			log.info("Reading Property File Complted");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private CloudStorage getConnection() throws Exception {
		CloudStorageConfig myConfig = new CloudStorageConfig();
		CloudStorage myConnection = null;
		try {
	        myConfig.setServiceName(prop.getProperty("servicename")).setUsername(prop.getProperty("username"))
	                        .setPassword(prop.get("password").toString().toCharArray()).setServiceUrl(prop.getProperty("serviceurl"));
	         myConnection = CloudStorageFactory.getStorage(myConfig);
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return myConnection;
		
	}
	
	
	private List<Key> getListofFiles(CloudStorage conn) {
		
		log.info("Getting List of Files in Container" + prop.getProperty("container"));
		List<Key> fList = conn.listObjects(prop.getProperty("container"),null);
		log.info("Number of Files in Container " + prop.getProperty("container") + " is " + fList.size());
		return fList;
		
	}
	
	private void loadToMysql(List<Key> fileList) throws ClassNotFoundException, SQLException {
		log.info("loading Files to Mysql");
		String mysql = prop.getProperty("mysql");
		String mysqlUsername = prop.getProperty("mysql_username");
		String mysqlPassword = prop.getProperty("mysql_password");
		Class.forName("com.mysql.jdbc.Driver");  
		Connection conn= null;
			
		HashMap<String, String> mysqlFileMap = new HashMap<>();
		HashMap<String, String> targetFileMap = new HashMap<>();
		ArrayList<String> targerKeyList = new ArrayList<>();
		PreparedStatement ps = null;
		Statement qst = null;
		
		String insertStmt = "insert into `"+prop.getProperty("table_name") +"`(file_name,add_date,src_file_size,target_dir) values(?,?,?,?)";
		
		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());

		try {
			conn = DriverManager.getConnection(  
					mysql,mysqlUsername,mysqlPassword);  
			
			
			Statement stmt= conn.createStatement();  
			qst = conn.createStatement();
			
			ResultSet qrs = qst.executeQuery("select * from target_mapping;");
			while(qrs.next()) {
				targetFileMap.put(qrs.getString("file_name"), qrs.getString("target_path"));
				targerKeyList.add(qrs.getString("file_name"));
			}
			
			ResultSet rs=stmt.executeQuery("select * from " + prop.getProperty("table_name") +";");  
			ps = conn.prepareStatement(insertStmt);
			
			while(rs.next()) {
				mysqlFileMap.put(rs.getString("file_name"), null);
			}
			
			for (Key k : fileList) {
				if (!mysqlFileMap.containsKey(k.getKey()) ) {
					if (k.getKey().endsWith(".zip")) {
						
					
					ps.setString(1, k.getKey());	
					ps.setDate(2, sqlDate);
					ps.setLong(3, k.getSize());
					ps.setString(4, this.getTargetPath(targetFileMap, k.getKey(), targerKeyList));
					ps.addBatch();
					log.info("Added File : " + k.getKey());
					}
					}
			}
			
		log.info("Batch Size " + ps.getFetchSize());
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			ps.executeBatch();
			if (conn != null)  conn.close();
		}
	}
	
	private String getTargetPath(HashMap<String, String> targetFileMap,String fileName,ArrayList<String> targerKeyList) {
		String retuenStr = null;
		for (String s:targerKeyList) {
			if (fileName.toLowerCase().contains(s.toLowerCase())) {
				retuenStr = targetFileMap.get(s);
				break;
			}	
		}
		
		if (retuenStr == null) {
			log.warning("File : " + fileName + "  Dosent have target Dir Specified");
		}
		return retuenStr;
	}

	private String getSuccessMessage(String logFileName) throws FileNotFoundException {
		
		String rt = "";
		File f = new File(logFileName);
		if (f.exists()) {
			rt = new Scanner(new File(logFileName)).useDelimiter("\\Z").next();

		}
		return rt;
	}
	
}

