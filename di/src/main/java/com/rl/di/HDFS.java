package com.rl.di;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;

/**
 * 
 * @author Bhuvan
 *Loads File to HADOOP
 */

public class HDFS {

FileSystem hdfs = null;
Map<String, String> deleteTrackerMap = null;
static Log log = LogFactory.getLog(HDFS.class);
String mySqlTableName = null;
/**
 * 
 * @param tableName mysql table name
 * @throws IOException
 * @throws URISyntaxException
 */
public HDFS(String tableName) throws IOException, URISyntaxException {

	Configuration conf = new Configuration();
	conf.addResource(new File("/etc/hadoop/conf/core-site.xml").toURI().toURL());
    conf.addResource(new File("/etc/hadoop/conf/hdfs-site.xml").toURI().toURL());
	
	hdfs = FileSystem.get(conf);
	deleteTrackerMap = new HashMap<String, String>();
	System.setProperty("HADOOP_USER_NAME", "hdfs");
	mySqlTableName = tableName;
}

/**
 * 
 * @param fconfg
 * @param oracleFileName
 * @param zis
 * @param con
 * @throws IllegalArgumentException
 * @throws IOException
 * @throws SQLException
 */
public void hdfsProcess(FileConfig fconfg,String oracleFileName,File zis,Connection con) throws IllegalArgumentException, IOException, SQLException {
		
	Path hdfsDirName = new Path(fconfg.getTargetDir());
	Path hdfsFileName = new Path(fconfg.getTargetDir() + oracleFileName);
	
	
	if (fconfg.getCleanDir() && !this.deleteTrackerMap.containsKey(fconfg.getTargetDir())) {
		this.cleanDir(hdfsDirName,fconfg.getTargetDir());
	}
	
	if (this.checkHdfsFileExists(hdfsFileName)) {
		if (fconfg.getOverwriteType().equalsIgnoreCase("force")) {
			this.deleteHdfsFile(hdfsFileName);
			this.writeToHdfs(hdfsFileName, zis,fconfg,con);
		}else {
			log.info("File " + oracleFileName + " Will not written to hdfs because file already exists and Overwrite is set to False");
			
		}
	}else {
		this.writeToHdfs(hdfsFileName, zis,fconfg,con);	
	}
}
/**
 * 
 * @param hdfsDirName
 * @param targetDir
 */

public void cleanDir(Path hdfsDirName,String targetDir) {
	this.deleteTrackerMap.put(targetDir, null);
	try {
		if (this.hdfs.exists(hdfsDirName)) {
			this.hdfs.delete(hdfsDirName,true);
			this.hdfs.mkdirs(hdfsDirName);	
		}else {
			this.hdfs.mkdirs(hdfsDirName, new FsPermission("777"));
		}
	}catch (IOException e) {
		e.printStackTrace();
	}
	
}
/**
 * 
 * @param hdfsFileName
 * @return
 */

private boolean checkHdfsFileExists(Path hdfsFileName) {
	boolean returnstatus  = false;
	
	try {
	if (this.hdfs.exists(hdfsFileName)) {
		
		returnstatus = true;
	}
	}catch (IOException e) {
		e.printStackTrace();
	}
	return returnstatus;
}
/**
 * 
 * @param hdfsFileName
 */

private void deleteHdfsFile(Path hdfsFileName) {
	try {
		this.hdfs.delete(hdfsFileName,true);
		
	}catch (IOException e) {
		e.printStackTrace();
	}	
}

@SuppressWarnings("unused")
private void writeToHdfs(Path hdfsFileName,ZipInputStream zis) throws IllegalArgumentException, IOException {
	
	FSDataOutputStream out = hdfs.create(hdfsFileName);
	byte[] buffer = new byte[256];
    int bytesRead;
    while ((bytesRead = zis.read(buffer)) != -1) {
        out.write(buffer, 0, bytesRead);
    }
	
}
/**
 * 
 * @param hdfsFileName
 * @param zipFilePath
 * @param fileConfig
 * @param conn
 * @throws IOException
 * @throws SQLException
 */

private void writeToHdfs(Path hdfsFileName,File zipFilePath,FileConfig fileConfig,Connection conn) throws IOException, SQLException {
	  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");  
	  Date date = new Date(); 
	  
	 String textDate = formatter.format(date);
	    
	 String readLine = "";
 	OutputStream os = hdfs.create( hdfsFileName);
 	int bufferSize = 1024*8;
 	BufferedWriter bw = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ),bufferSize );
 	int cnt  = -1; 
 	try (FileInputStream fis = new FileInputStream(zipFilePath);
             BufferedInputStream bis = new BufferedInputStream(fis);
             ZipInputStream stream = new ZipInputStream(bis)) {
     	while ((stream.getNextEntry()) != null) {
     	BufferedReader br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
     	    	        	
     	while ((readLine = br.readLine()) != null) {
         cnt ++;
     		if (cnt !=0) {
     		String[] splitStir = readLine.split("\",\"");
     		String[] splitStirp = readLine.split("\"|\"");
             StringBuilder sb = new StringBuilder();
             StringBuilder sbp = new StringBuilder();
             splitStir[0] = splitStir[0].replaceFirst("\"", "");
             splitStirp[0] = splitStirp[0].replaceFirst("\"", "");
         	if (splitStir[splitStir.length -1].endsWith("\"")) {
         		splitStir[splitStir.length -1] = splitStir[splitStir.length -1].substring(0,splitStir[splitStir.length -1].length() -1);   		
         	}
             for (int k = 0;k<splitStir.length;k++) {
             	sb.append(splitStir[k]);
             	//sb.append("\u0001");             	
             }
             if (splitStirp[splitStirp.length -1].endsWith("\"")) {
            	 splitStirp[splitStirp.length -1] = splitStirp[splitStirp.length -1].substring(0,splitStirp[splitStirp.length -1].length() -1);   		
          	}
              for (int j = 0;j<splitStirp.length;j++) {
              	sbp.append(splitStirp[j]);
              	//sb.append("\u0001");             	
              }
             //sb.append(textDate);
            // bw.write(readLine);
             bw.write(sb.toString());
             if(readLine.split("\"|\"").length > 1){
             bw.write(sbp.toString());
             }
             bw.write("\n");
             
     	}
     	}
     	}
      }
 	bw.flush();
 	bw.close();
 	this.updateMysql(conn, fileConfig.getId(),cnt);
 }

@SuppressWarnings("unused")
private String getmd5(FileInputStream fis) throws IOException {
	return  org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
}

/**
 * 
 * @param conn
 * @param fid
 * @param cnt
 * @throws UnknownHostException
 * @throws SQLException
 */
private void updateMysql(Connection conn,int fid,long cnt) throws UnknownHostException, SQLException {
	String hostname = this.getHostName();
    String sql = "update " + mySqlTableName + " set processed_by = '"+hostname + "',row_count = '"+cnt+"' where f_id =" +fid;
    	
    Statement st = conn.createStatement();
    st.execute(sql);
    
}
/**
 * 
 * @return
 * @throws UnknownHostException
 */

private String getHostName() throws UnknownHostException {
	return InetAddress.getLocalHost().getHostName();
}

}
	


