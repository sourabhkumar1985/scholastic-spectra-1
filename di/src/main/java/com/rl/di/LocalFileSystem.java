package com.rl.di;

import java.io.File;
import java.io.IOException;
import java.nio.file.StandardCopyOption;

import oracle.cloud.storage.model.StorageInputStream;

public class LocalFileSystem {

	public void processData() {
		
		
	}
	
	public void writetoLocal(StorageInputStream si,String filename) throws IOException {
		String targetBaseDir = "output/";
		File targetFile = new File(targetBaseDir + filename);
		java.nio.file.Files.copy(
			      si, 
			      targetFile.toPath(), 
			      StandardCopyOption.REPLACE_EXISTING);
			 
		
	}

}
