package com.rl.di;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileConfig {

	@SerializedName("inputfile")
	@Expose
	private String inputfile;
	@SerializedName("suffix")
	@Expose
	private String suffix;
	@SerializedName("prefix")
	@Expose
	private String prefix;
	@SerializedName("loadtype")
	@Expose
	private String loadtype;
	@SerializedName("targetDir")
	@Expose
	private String targetDir;
	@SerializedName("overwriteType")
	@Expose
	private String overwriteType;
	@SerializedName("cleanDir")
	@Expose
	private Boolean cleanDir;
	
	private int id;

	public String getInputfile() {
	return inputfile;
	}

	public void setInputfile(String inputfile) {
	this.inputfile = inputfile;
	}

	public String getSuffix() {
	return suffix;
	}

	public void setSuffix(String suffix) {
	this.suffix = suffix;
	}

	public String getPrefix() {
	return prefix;
	}

	public void setPrefix(String prefix) {
	this.prefix = prefix;
	}

	public String getLoadtype() {
	return loadtype;
	}

	public void setLoadtype(String loadtype) {
	this.loadtype = loadtype;
	}

	public String getTargetDir() {
	return targetDir;
	}

	public void setTargetDir(String targetDir) {
	this.targetDir = targetDir;
	}

	public String getOverwriteType() {
	return overwriteType;
	}

	public void setOverwriteType(String overwriteType) {
	this.overwriteType = overwriteType;
	}

	public Boolean getCleanDir() {
	return cleanDir;
	}

	public void setCleanDir(Boolean cleanDir) {
	this.cleanDir = cleanDir;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
