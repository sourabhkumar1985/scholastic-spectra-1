package com.rl.di;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.zip.ZipInputStream;

import com.google.gson.Gson;
import com.rl.email.Mail;

import oracle.cloud.storage.CloudStorage;
import oracle.cloud.storage.CloudStorageConfig;
import oracle.cloud.storage.CloudStorageFactory;
import oracle.cloud.storage.model.Key;
import oracle.cloud.storage.model.StorageInputStream;


/**
 * 
 * @author Bovine
 * Load the files from oracle cloud storage to hadoop cluster, Hadoop cluster config are read from /etc/hadoop
 * Files name must be loaded to mysql db using FileLoad.java, Files which are having locked = null in mysql will be laoded to hadoop
 *
 */
public class OracleDI {
	Properties prop = null;
	static String propFileName = "conf/oracle.properties";
	static Logger log = Logger.getLogger(OracleDI.class.getName());
	Map<String, FileConfig> fConfigMap  = null;
	static String fconfFile = "";
	
	
	/**
	 * 
	 * @param args
	 * @throws Exception
	 * Main Method 
	 */
	public static void main(String[] args) throws Exception {
		
		log.info("Program start time: " + new Date());
		
		OracleDI odi = new OracleDI();
		Connection mySqlConn = null;

		try {
			odi.readProperties();
			mySqlConn = odi.getMysqlConnection();
			CloudStorage conn = odi.getConnection();
			odi.processData(conn,mySqlConn);
		}catch (Exception e) {
			odi.sendExceptionEmail(e);
			e.printStackTrace();
			throw e;
		}finally {
			if (mySqlConn != null) mySqlConn.close();
			
		}
		log.info("Program End Date: " + new Date());
		odi.sendSuccessEamil();
	}
	
	
	private void sendSuccessEamil() {
		Mail mail = new Mail();
		try {	
			String message  = "Extraction Program completed successfully";
			mail.sendEmail(prop,message,"Success");
			
		}catch (Exception ep) {
			ep.printStackTrace();
		}
		
	}
	/**
	 * 
	 * @param exception
	 * send email
	 */
	
	private void sendExceptionEmail(Exception e) {
		Mail mail = new Mail();
		String p = "";
		for (int i = 0;i< e.getStackTrace().length;i++) {	
			 p =p +e.getStackTrace()[i] + " \n";
		}
		String s = e.toString() + "\n" +
				"Messgae :" + e.getMessage() + "\n" +
				"cause :" + e.getCause() + "\n" +
				"Stacktrace : " + p;
		
		try {
			
			mail.sendEmail(prop,s,"Failed");
			
		}catch (Exception ep) {
			ep.printStackTrace();
		}
		
		
	}
	
	/**
	 * 
	 * @return Mysql Connection 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * 
	 * Gets Mysql Connection
	 */
	
	
	private Connection getMysqlConnection() throws SQLException, ClassNotFoundException {
		String mysql = prop.getProperty("mysql");
		String mysqlUsername = prop.getProperty("mysql_username");
		String mysqlPassword = prop.getProperty("mysql_password");
		Class.forName("com.mysql.jdbc.Driver");  
		Connection conn = DriverManager.getConnection(  
				mysql,mysqlUsername,mysqlPassword);  
		return conn;
	}
	
	
	/**
	 * 
	 * @param MysqlConnection
	 * @return ArralyList of FileConfig Object
	 * @throws Exception
	 * Reads the mysql DB and create List of FIleConfig object, Also updates the locked = true and locked_by in mysql
	 * 
	 */
	private ArrayList<FileConfig> createConfiguration(Connection conn) throws Exception {
		
		String tableName = prop.getProperty("table_name");
		log.info("Reading properties file");
		String hostname = this.getHostName();

		ArrayList<FileConfig> fileConfigList = new ArrayList<>();
		
		FileConfig fileConfig = null;
		log.info("Limit is " + prop.getProperty("limit"));
		int upperBound = 999999999;
		int lowerBound = 0;
		if ((prop.getProperty("limit") != null) && (prop.getProperty("limit").length() > 0)) {
			 upperBound = Integer.parseInt(prop.getProperty("limit").split("-")[1]);
			 lowerBound = Integer.parseInt(prop.getProperty("limit").split("-")[0]);	
		}
		
		
		
		String selectStmt = "select * from " +  tableName + " where f_id >= " + lowerBound + " and f_id <=" + upperBound +" and locked is null ";
		
		java.sql.Statement st = null;
		ResultSet rs = null;
		Statement st2 = null;
		try {
			st =  conn.createStatement();
			st2 = conn.createStatement();
			rs = st.executeQuery(selectStmt);
			boolean processFile = true;
			while(rs.next()) {
				processFile = true;
			if (!rs.getBoolean("locked")) {	
				fileConfig = new FileConfig();
				fileConfig.setInputfile(rs.getString("file_name"));
				fileConfig.setSuffix("");
				fileConfig.setPrefix("");
				fileConfig.setLoadtype("");
				
				String targetDir = rs.getString("target_dir");
				if (targetDir == null) {
					log.warning("Target Dir is empty file will not be processed");
					processFile = false;
				}else {
					if (!targetDir.endsWith("/")) {
						targetDir = targetDir + "/";
					}
				}
				fileConfig.setTargetDir(targetDir);
				fileConfig.setOverwriteType("force");
				fileConfig.setCleanDir(false);
				fileConfig.setId(rs.getInt("f_id"));
				
				if (processFile) {
					fileConfigList.add(fileConfig);
					st2.execute("update " + tableName + " set locked = true,locaked_by='" + hostname + "' where f_id="+  rs.getInt("f_id"));
				}
			}
		}
			
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally {
			if (rs != null) rs.close();
		}
		
		return fileConfigList;
		
	}
	
	/**
	 * Reads properties file
	 */

	private void readProperties() {
		
		log.info("Reading Properties File ...............");
		try (InputStream input = new FileInputStream(propFileName)) {
			prop = new Properties();
			prop.load(input);
			log.info("Reading Property File Complted");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Un Used ...
	 * @throws IOException
	 */
	
	@SuppressWarnings("unused")
	private void readFileConfig() throws IOException {
		log.info("Reading Json Config File............");
		fConfigMap = new HashMap<String, FileConfig>();
        String content = new String(Files.readAllBytes(Paths.get(fconfFile)));
		Gson gson = new Gson();
		FileConfig[] fileConfigArray = gson.fromJson(content, FileConfig[].class);  

		for (int i = 0;i<fileConfigArray.length;i++ ) {
			fConfigMap.put(fileConfigArray[i].getInputfile().toLowerCase(), fileConfigArray[i]);
			
		}
	}
	
	/**
	 * 
	 * Unused
	 * 
	 * @param conn
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private void readFileConfig(Connection conn) throws Exception {
		
		log.info("Reading Json Config File............");
		fConfigMap = new HashMap<String, FileConfig>();
		
		for (FileConfig fileConfig:this.createConfiguration(conn)){              
			fConfigMap.put(fileConfig.getInputfile(), fileConfig);
			
		}
		
	}
	
	/**
	 * 
	 * @return Oracle Cloud Connection
	 * @throws Exception
	 * 
	 */
	
	private CloudStorage getConnection() throws Exception {
		CloudStorageConfig myConfig = new CloudStorageConfig();
		CloudStorage myConnection = null;
		try {
	        myConfig.setServiceName(prop.getProperty("servicename")).setUsername(prop.getProperty("username"))
	                        .setPassword(prop.get("password").toString().toCharArray()).setServiceUrl(prop.getProperty("serviceurl"));
	         myConnection = CloudStorageFactory.getStorage(myConfig);
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return myConnection;
		
	}
	
	/**
	 * 
	 *
	 * @param Orcale Cloud Storage Connection
	 * @param Mysql Connection
	 * @throws Exception
	 * 
	 * For each File in List<FileConfig> Download the file to location mentioned in Properties File, Unzip and pass to HDFS class to load
	 */
	
	private void processData(CloudStorage conn,Connection mySqlCon) throws Exception {
		HDFS hdfs = new HDFS(prop.getProperty("table_name"));	
		String zipFileDir = prop.getProperty("zipFileDir");
		if (!zipFileDir.endsWith("/")) {
			zipFileDir = zipFileDir + "/";
		}
		HashMap<String, String> oFileMap = this.getOracleFileMap(conn);
		
		StorageInputStream st = null;
		ZipInputStream zis = null;

		for (FileConfig f : this.createConfiguration(mySqlCon)) {
			if (oFileMap.containsKey(f.getInputfile())) {
				log.info("Processing For File " + f.getInputfile());
				try {
				st = conn.retrieveObject(prop.getProperty("container"), f.getInputfile());
				String formattedFileName = this.formatFileName(f.getInputfile());
								
				File targetZipFile = new File( zipFileDir + formattedFileName);
				this.createZipFileDirPaths(targetZipFile);
				
				java.nio.file.Files.copy(st, targetZipFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
				hdfs.hdfsProcess(f, targetZipFile.getName().replace(".zip", ""),targetZipFile,mySqlCon);
				}catch (Exception e) {
					e.printStackTrace();
					throw e;
				}finally {
					if (zis != null) zis.close();
				}
			}else {
				log.warning("Specified file is not present in the container:" + f.getInputfile());
			}
		}
	}
	
	/**
	 * 
	 * @param Oracle Connection
	 * @return List<Key> 
	 * Returns list of oracle file names 
	 */
	private List<Key> getListofFiles(CloudStorage conn) {
		log.info("Getting List of Files in Container" + prop.getProperty("container"));
		List<Key> fList = conn.listObjects(prop.getProperty("container"),null);
		return fList;
		
	}
	
	/**
	 * 
	 * @param conn
	 * @return HashMap<String,String>
	 * Returns HashMap<Oracle FIlename,null>
	 */
	private HashMap<String, String> getOracleFileMap(CloudStorage conn){
		HashMap<String, String> oFileMap = new HashMap<>();
		
		for (Key k: this.getListofFiles(conn)){
			oFileMap.put(k.getKey(), null);
			
		}
		
		return oFileMap;
	}
	
	/**
	 * 
	 * @return Hostname
	 * @throws UnknownHostException
	 */

	private String getHostName() throws UnknownHostException {
		return InetAddress.getLocalHost().getHostName();
	}
	
	/**
	 * 
	 * @param s
	 * @return
	 */
	private  String formatFileName(String s) {
		/*if (s.contains("/")) {
			s = s.replace("/", "_");
			log.info("File name contains / , changes to _");
		}*/
		return s;
	}
	
	/**
	 * 
	 * @param zipFileDirPath
	 * Creates ZIP file dir as per oracle cloud Storage
	 */
	
	private void createZipFileDirPaths(File zipFileDirPath) {
		
		File parentDir = new File(zipFileDirPath.getParent());
		
		if (!parentDir.exists()) {
			parentDir.mkdirs();
		}
	}
	
}

