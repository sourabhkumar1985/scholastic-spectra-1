package com.rl.email;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {
	
	  String host="localhost";  
	  final String user="oracle_extraction@localhost";
	  final String password=""; 
	  Properties props = null;
	  	
	  public Mail() {
		  	props = new Properties();
		  	props.put("mail.smtp.host",host);  
		  	props.put("mail.smtp.auth", "false");  
		  	 
	  }
	  
	  public void sendEmail(Properties mProp,String mailMessage,String status) throws AddressException {
		   
		  
		  String to = mProp.getProperty("email");
		  
		  String[] recipientList = to.split("\\;");
		  
		  InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
		  int counter = 0;
		  for (String recipient : recipientList) {
		      recipientAddress[counter] = new InternetAddress(recipient.trim());
		      counter++;
		  }
		  
		  
		  Session session = Session.getDefaultInstance(props,  
				    new javax.mail.Authenticator() {  
				      protected PasswordAuthentication getPasswordAuthentication() {  
				    return new PasswordAuthentication(user,password);  
				      }  
				    }); 
		  
		  try {  
			     MimeMessage message = new MimeMessage(session);  
			     message.setFrom(new InternetAddress(user));  
			     message.addRecipients(Message.RecipientType.TO,recipientAddress);  
			     message.setSubject( new Date()  + ":" + status);  
			     message.setText( mailMessage );  
			       
			     Transport.send(message);  
			  
			    
			     } catch (MessagingException e) {e.printStackTrace();}  
			 }   
	   }
	   

