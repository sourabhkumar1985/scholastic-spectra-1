package mock.test;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import oracle.cloud.storage.CloudStorage;
import oracle.cloud.storage.CloudStorageConfig;
import oracle.cloud.storage.CloudStorageFactory;
import oracle.cloud.storage.model.Key;

public class OracleTest {

	public static void main(String[] args) throws Exception {
	
		CloudStorageConfig myConfig = new CloudStorageConfig();
		CloudStorage myConnection = null;
		List<OraclePojo> oList = new ArrayList<OraclePojo>();
		try {
	        myConfig.setServiceName("Storage-schlprodpaas").setUsername("cloud_bi_user")
	                        .setPassword("TestCloud2020".toString().toCharArray()).setServiceUrl("https://uscom-east-1.storage.oraclecloud.com");
	        myConnection = CloudStorageFactory.getStorage(myConfig);
	        List<Key> fList= myConnection.listObjects("BI_CON", null); 
	        
	       for (Key k:fList) {
	    	   OraclePojo oPojo = new OraclePojo();
	    	   oPojo.setFileName(k.getKey());
	    	   oPojo.setFileSize(k.getSize());
	    	   oPojo.setLastUpdateDate(k.getLastModified().toString());
	    	   oList.add(oPojo);
	       }
	         
	       Gson gson = new Gson();
	       System.out.println(gson.toJson(oList));
	       
	       
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		
		

	}

}
