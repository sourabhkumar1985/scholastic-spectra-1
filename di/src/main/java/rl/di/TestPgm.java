package rl.di;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import oracle.cloud.storage.CloudStorage;
import oracle.cloud.storage.CloudStorageConfig;
import oracle.cloud.storage.CloudStorageFactory;
import oracle.cloud.storage.model.StorageInputStream;

public class TestPgm {

	public static void main(String[] args) throws IOException, URISyntaxException {
		// TODO Auto-generated method stub
		
		TestPgm tp = new TestPgm();
		tp.getFromOracle();
				
	}

	private void getFromOracle() throws IOException, URISyntaxException {
		CloudStorageConfig myConfig = new CloudStorageConfig();
        myConfig.setServiceName("Storage-schlprodpaas").setUsername("cloud_bi_user")
                        .setPassword("TestCloud2020".toCharArray()).setServiceUrl("https://uscom-east-1.storage.oraclecloud.com");
        CloudStorage myConnection = CloudStorageFactory.getStorage(myConfig);
       
		
		StorageInputStream st = myConnection.retrieveObject("BI_CON", "file_crmanalyticsam_partiesanalyticsam_customer-batch106802768-20200506_091307.zip");
	
		ZipInputStream zis2 = new ZipInputStream(st);
		zis2.getNextEntry();
		this.writeToHdfs(zis2);
		
		
		
		
	
	}
	
	private void writeToHdfs(ZipInputStream zis) throws IOException, URISyntaxException {
		Configuration config = new Configuration();  
		//config.addResource(new Path("conf/core-site.xml"));
		//config.addResource(new Path("conf/hdfs-site.xml"));
		//FileSystem fs = FileSystem.get(config);
		
		FileSystem hdfs = FileSystem.get(new URI("hdfs://rlmaster:8020"), config);
		
		 FSDataOutputStream out = hdfs.create(new Path("/frv/tmpew"));
		 
		 byte[] buffer = new byte[256];
		    int bytesRead;
		    while ((bytesRead = zis.read(buffer)) != -1) {
		        out.write(buffer, 0, bytesRead);
		    }
	}
}
